@php
    $page = 'startPage';
    $dateFrom = \Carbon\Carbon::today()->format('d-m-Y');
    $dateTo = \Carbon\Carbon::tomorrow()->format('d-m-Y');
@endphp

@extends('site.startpage')
@section('meta')
    <meta name="keywords" content="Ошибка 404 Страница не найдена">
    <meta name="description" content="Ошибка 404 Страница не найдена">
    <title>Ошибка 404 Страница не найдена</title>
@endsection
@section('content')

    <section id="error404" class="text-center">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="wrapper-404-error">
                        <img src="/images/404.jpg" alt="">
                        <h2>Страница не найдена</h2>
                        <p>Страница которую Вы искали не существует или временно не работает. <br> Перейдите на главную страницу сайта или воспользуйтесь поиском.</p>
                        <a href="{{ route('homepage') }}">На главную</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="search404">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    @include('site.blocks.search-hotel')
                </div>
            </div>
        </div>
    </section>


@endsection