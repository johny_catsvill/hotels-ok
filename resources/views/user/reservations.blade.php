@extends('site.startpage')

@section('content')
    @include('user.blocks.top-user', ['name' => 'Мои бронирования', 'background' => 'top-user.jpg'])
    <div class="container">
        <div class="row">
            @include('user.blocks.left-sidebar')
            <div class="col-lg-9">
                <div class="list-reservation-user">
                    <div class="sort-reservation-user">
                        <ul>
                            <li>
                                <span>{{ __('Сортировать по') }}:</span>
                            </li>
                            <li>
                                <div class="select-sort-reservation">
                                    <span><dz>{{ __('Текущий год') }}</dz> <i class="fa fa-caret-down" aria-hidden="true"></i></span>
                                </div>
                                <div class="dropdown-filter-reservation">
                                    <ul>
                                        <li><a href="" data-year="2018">{{ __('Текущий год') }}</a></li>
                                        <li><a href="" data-year="2017">2017 {{ __('год') }}</a></li>
                                        <li><a href="" data-year="2016">2016 {{ __('год') }}</a></li>
                                        <li><a href="" data-year="2015">2015 {{ __('год') }}</a></li>
                                    </ul>
                                </div>
                                <input type="hidden" name="year" value="2018">
                            </li>
                            <li>
                                <div class="select-sort-reservation">
                                    <span><dz>{{ __('Все бронирования') }}</dz> <i class="fa fa-caret-down" aria-hidden="true"></i></span>
                                </div>
                                <div class="dropdown-filter-reservation">
                                    <ul>
                                        <li><a href="" data-type="all">{{ __('Все бронирования') }}</a></li>
                                        <li><a href="" data-type="wait">{{ __('В ожидании') }}</a></li>
                                        <li><a href="" data-type="cancel">{{ __('Отмененные') }}</a></li>
                                        <li><a href="" data-type="active">{{ __('Активные') }}</a></li>
                                    </ul>
                                </div>
                                <input type="hidden" name="type" value="all">
                            </li>
                        </ul>
                    </div>
                    <span class="count-reservation-user">{{ __('Найдено') }} {{ $reservations->total() }} {{ __('бронирований') }}</span>
                    @if($reservations->total())
                        <div class="global-wrap-reservation">
                            <div class="wrap-resevation">
                                @include('user.blocks.reservation', ['reservations' => $reservations])
                            </div>
                            @include('global.preloader')
                        </div>

                        <input type="hidden" name="page" value="2">
                        @if($showMore)
                        <a href="javascript:void(0);" onclick="UserScripts.getReservations();" class="btn-show-more-reservation">{{ __('Показать еще') }}</a>
                        @endif
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

<!--
    fail
    success
    no-active

-->