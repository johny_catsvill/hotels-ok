@extends('site.startpage')

@section('content')
    @include('user.blocks.top-user', ['name' => 'Мой профиль', 'background' => 'top-user.jpg'])
    <div class="container">
        <div class="row">
            @include('user.blocks.left-sidebar')
            <div class="col-lg-9">
                <div class="list-menu-profile-user">
                    <ul>
                        <li><a class="active" href="javascript:void(0);" data-block="personal">{{ __('Персональна информация') }}</a></li>
                        <li><a href="javascript:void(0);" data-block="reservation-info-user">{{ __('Для бронирования') }}</a></li>
                        {{--<li><a href="javascript:void(0);">Кредитные карты</a></li>--}}
                        {{--<li><a href="javascript:void(0);">Предпочтения по оплате</a></li>--}}
                        {{--<li><a href="javascript:void(0);">Как вы путешествуете</a></li>--}}
                        <li><a href="javascript:void(0);" data-block="password-user">{{ __('Пароль и валюта') }}</a></li>
                        <li><a href="javascript:void(0);" data-block="social-info-user">{{ __('Социальные сети') }}</a></li>
                        <li><a href="javascript:void(0);" data-block="email-rassilka">{{ __('Получение рассылки') }}</a></li>
                        {{--<li><a href="javascript:void(0);">Получение SMS</a></li>--}}
                        {{--<li><a href="javascript:void(0);">Безопасность</a></li>--}}
                    </ul>
                </div>
                <div class="content-profile-user">
                    <div class="personal-info-user personal">
                        <form action="" enctype="multipart/form-data">
                            <h3>
                                <span>{{ __('Персональная информация') }}</span>
                                <a href="javascript:void(0);" onclick="newFunction.saveInfoUser(this);">{{ __('Сохранить') }}</a>
                            </h3>
                            {{--<div class="profile-img-block">--}}
                                {{--<ul>--}}
                                    {{--<li>--}}
                                        {{--<h4>Главная фотография</h4>--}}
                                        {{--<span>Ваша фотография - первое, что <br> видят люди, поэтому выберите лучшую.</span>--}}
                                        {{--<input type="file" name="photo" class="hidden-field" onchange="newFunction.showImgUser(this);">--}}
                                    {{--</li>--}}
                                    {{--<li>--}}
                                        {{--<ul>--}}
                                            {{--<li>--}}
                                                {{--<div class="block-img-profile">--}}
                                                    {{--<img src="/images/img-profile.png" alt="">--}}
                                                {{--</div>--}}
                                            {{--</li>--}}
                                            {{--<li>--}}
                                                {{--<span onclick="newFunction.clickByInputImg();">Нажмите, чтоб <br> загрузить фото</span>--}}
                                            {{--</li>--}}
                                        {{--</ul>--}}
                                    {{--</li>--}}
                                {{--</ul>--}}
                            {{--</div>--}}
                            <div class="form-info-user">
                                <div class="field-user">
                                    <label for="">{{ __('Ваш псевдоним') }}</label>
                                    <input type="text" name="nickname" value="{{ $client->nickname }}">
                                </div>
                                <div class="field-user">
                                    <label for="">{{ __('Дата рождения') }}</label>
                                    <select name="day">
                                        @for($i = 1; $i<32; $i++)
                                            @if($i < 10)
                                                <option @if($client->day == $i) selected @endif value="0{{ $i }}">0{{ $i }}</option>
                                            @else
                                                <option @if($client->day == $i) selected @endif value="{{ $i }}">{{ $i }}</option>
                                            @endif
                                        @endfor
                                    </select>
                                    <select name="month" id="">
                                        <option @if($client->month == '01') selected @endif value="01">{{ __('Январь') }}</option>
                                        <option @if($client->month == '02') selected @endif value="02">{{ __('Февраль') }}</option>
                                        <option @if($client->month == '03') selected @endif value="03">{{ __('Март') }}</option>
                                        <option @if($client->month == '04') selected @endif value="04">{{ __('Апрель') }}</option>
                                        <option @if($client->month == '05') selected @endif value="05">{{ __('Май') }}</option>
                                        <option @if($client->month == '06') selected @endif value="06">{{ __('Июнь') }}</option>
                                        <option @if($client->month == '07') selected @endif value="07">{{ __('Июль') }}</option>
                                        <option @if($client->month == '08') selected @endif value="08">{{ __('Август') }}</option>
                                        <option @if($client->month == '09') selected @endif value="09">{{ __('Сентябрь') }}</option>
                                        <option @if($client->month == '10') selected @endif value="10">{{ __('Октябрь') }}</option>
                                        <option @if($client->month == '11') selected @endif value="11">{{ __('Ноябрь') }}</option>
                                        <option @if($client->month == '12') selected @endif value="12">{{ __('Декабрь') }}</option>
                                    </select>
                                    <select name="year">
                                       @for($i = 1950; $i <= date('Y'); $i++)
                                            <option @if($client->year == $i) selected @endif value="{{ $i }}">{{ $i }}</option>
                                       @endfor
                                    </select>
                                </div>
                                <div class="field-user cities-countries-style">
                                    <label for="">{{ __('Страна/территория') }}</label>
                                    <select name="countryId">
                                        <option value="1">{{ __('Украина') }}</option>
                                    </select>
                                </div>
                                <div class="field-user cities-countries-style">
                                    <label for="">{{ __('Город') }}</label>
                                    <select name="cityId">
                                        @foreach($cities as $city)
                                            <option @if($client->cityId == $city->id) selected @endif value="{{ $city->id }}">{{ $city->name }}</option>
                                        @endforeach
                                    </select>
                                </div>

                            </div>
                            {{ csrf_field() }}
                        </form>
                    </div>

                    <div class="personal-info-user reservation-info-user">
                        <form action="">
                            {{ csrf_field() }}
                            <h3>
                                <span>{{ __('Для бронирования') }}</span>
                                <a href="javascript:void(0);" onclick="newFunction.saveInfoUser(this);">{{ __('Сохранить') }}</a>
                            </h3>
                            <div class="form-info-user">
                                <div class="field-user">
                                    <ul>
                                        <li>
                                            <label for="">{{ __('Имя') }}</label>
                                            <input type="text" name="name" value="{{ $client->name }}">
                                        </li>
                                        <li>
                                            <label for="">{{ __('Фамилия') }}</label>
                                            <input type="text" name="lastName" value="{{ $client->lastName }}">
                                        </li>
                                    </ul>
                                </div>

                                <div class="field-user sex-block-user">
                                    <label for="">{{ __('Пол') }}</label>
                                    <div class="wrap-sex-user-radio">
                                        <input type="radio" name="sex" @if($client->sex == 1) checked @endif value="1"> <span><img src="/images/man.png" alt="">{{ __('Мужчина') }}</span>
                                    </div>
                                    <div class="wrap-sex-user-radio">
                                        <input type="radio" name="sex" @if($client->sex == 2) checked @endif value="2"> <span><img src="/images/woman-icon.png" alt="">{{ __('Женщина') }}</span>
                                    </div>
                                    <div class="wrap-sex-user-radio">
                                        <input type="radio" name="sex" @if($client->sex == 0) checked @endif value="0"> <span>{{ __('Предпочитаю не указывать') }}</span>
                                    </div>
                                </div>

                                <div class="field-user">
                                    <ul>
                                        <li>
                                            <label for="">{{ __('Телефон') }}</label>
                                            <input type="text" id="phoneCallback" name="phone" value="{{ $client->phone }}">
                                        </li>
                                        <li>
                                            <label for="">{{ __('Почта') }}</label>
                                            <input type="text" name="email" value="{{ $client->email }}">
                                        </li>
                                    </ul>
                                </div>

                                {{--<div class="field-user address-info-user">--}}
                                {{--<label for="">Адреса</label>--}}
                                {{--<ul>--}}
                                {{--<li>--}}
                                {{--<div class="one-address-user">--}}
                                {{--<span><img src="/images/home-address.png" alt="">Домашний адрес</span>--}}
                                {{--<input type="text">--}}
                                {{--<a href="">Нажмите, чтоб изменить адрес</a>--}}
                                {{--</div>--}}
                                {{--</li>--}}
                                {{--<li>--}}
                                {{--<div class="one-address-user">--}}
                                {{--<span><img src="/images/work-address.png" alt="">Рабочий адрес</span>--}}
                                {{--<input type="text">--}}
                                {{--<a href="">Нажмите, чтоб изменить адрес</a>--}}
                                {{--</div>--}}
                                {{--</li>--}}
                                {{--</ul>--}}
                                {{--</div>--}}

                            </div>
                        </form>
                    </div>


                    <div class="personal-info-user password-user" >
                        <form action="">
                            {{ csrf_field() }}
                            <h3>
                                <span>{{ __('Пароль') }}</span>
                                <a href="javascript:void(0);" onclick="newFunction.saveInfoUser(this);">{{ __('Сохранить') }}</a>
                            </h3>
                            <div class="field-user address-info-user">
                                <ul>
                                    <li>
                                        <div class="one-address-user">
                                            <span>{{ __('Старый пароль') }}</span>
                                            <input type="password" name="oldPassword">
                                        </div>
                                    </li>
                                    <li>
                                        <div class="one-address-user">
                                            <span>{{ __('Новый пароль') }}</span>
                                            <input type="password" name="password">
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </form>
                    </div>

                    <div class="personal-info-user social-info-user">
                        <form action="">
                            {{ csrf_field() }}
                            <h3>
                                <span>{{ __('Социальные сети') }}</span>
                                {{--<a href="javascript:void(0);">Сохранить </a>--}}
                                <b>{{ __('Входите в аккаунт одним нажатием при подключении через Facebook или Google') }}</b>

                            </h3>
                            <div class="social-list">
                                <ul>
                                    <li>
                                        <div class="one-social-user">
                                            @if($socialAccountFacebook)
                                                <a><img src="/images/facebook.jpg" alt="">

                                                    <span>{{ $socialAccountFacebook->name }}</span>
                                                </a>
                                            @else
                                                <a href="{{ route('socialAuth', 'facebook') }}"><img src="/images/facebook.jpg" alt="">

                                                    <span>{{ __('Войти через') }}</span> <b>Facebook</b>
                                                </a>
                                            @endif
                                        </div>
                                    </li>
                                    <li>
                                        <div class="one-social-user">
                                            @if($socialAccountGoogle)
                                                <a><img src="/images/google.jpg" alt=""><span>{{ $socialAccountGoogle->name }}</span></a>
                                            @else
                                                <a href="{{ route('socialAuth', 'google') }}"><img src="/images/google.jpg" alt=""><span>{{ __('Войти через') }}</span> <b>Google</b></a>
                                            @endif
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </form>
                    </div>

                    <div class="personal-info-user email-rassilka">
                        <form action="">
                            {{ csrf_field() }}
                            <h3>
                                <span>{{ __('Получение рассылки') }}</span>
                                <a href="javascript:void(0);" onclick="newFunction.saveInfoUser(this);">{{ __('Сохранить') }}</a>
                                <b>{{ __('Выберите, как часто вы хотите получать от нас письма. Вы так же будете получать предложения,
                                    позволяющие вам экономить до 50%.') }}</b>
                            </h3>
                            <p>{{ $client->email }}</p>
                            <ul>
                                <li><input type="radio" name="distribution" @if($client->distribution == 1) checked @endif value="1" id="rassiklka1"><label for="rassiklka1">{{ __('Всегда') }}</label></li>
                                <li><input type="radio" name="distribution" value="2" @if($client->distribution == 2) checked @endif id="rassiklka2"><label for="rassiklka2">{{ __('Еженедельно') }}</label></li>
                                <li><input type="radio" name="distribution" value="3" @if($client->distribution == 3) checked @endif id="rassiklka3"><label for="rassiklka3">{{ __('Отписаться') }}</label></li>
                            </ul>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
@include('global.preloader')
    <div class="overlay-save hide">
    </div>

    <span class="info-save-message">{{ __('Информация успешно сохранена!') }}</span>
@endsection