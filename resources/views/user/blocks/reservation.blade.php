@foreach($reservations as $reservation)

        <div class="one-reservation-user @if($reservation->statusId == 3) success @elseif($reservation->statusId == 2) fail @elseif($reservation->statusId == 4) no-active @endif">
            <div class="reservation-hotel-image display-inline-block"
                 style="background: url(../images/hotels/{{ $reservation->object->id.'-'.$reservation->object->subdomain }}/{{ $reservation->object->image }}); background-size:cover;">
            </div>
            <div class="main-content-reservation display-inline-block">
                <h3><a target="_blank" href="{{ route('hotel', ['id' => $reservation->objectId, 'slug' => $reservation->object->subdomain]) }}">{{ $reservation->object->name }} <span>({{ $reservation->object->objectCategory->name }})</span></a>
                    <div class="reviews-hotel display-inline-block" title="Оценка: {{ $reservation->object->avgValue }}">
                        <ul class="display-inline-block stars-hotel">
                            @for($i = 0; $i < 5; $i++)
                                <i class="fa fa-star @if($reservation->object->stars > $i) active-star @endif" aria-hidden="true"></i>
                            @endfor

                        </ul>
                        <a target="_blank" href="{{ route('hotel', ['id' => $reservation->objectId, 'slug' => $reservation->object->subdomain]) }}#otzivi" class="display-inline-block">{{ $reservation->object->reviews->count() }}
                            {{ trans_choice('localization.reviews', $reservation->object->reviews->count()) }}</a>
                    </div>
                </h3>
                <span class="address-hotel-reservation">{{ $reservation->object->address }}</span>
                <div class="list-pluses-hotel-reservation">
                    <ul>
                        @foreach($reservation->object->objectPluses->slice(0, 10) as $pluse)
                            <li>{{ $pluse->name }}</li>
                        @endforeach
                    </ul>
                    <a href="javascript:void(0);" class="btn-more-info-reservation" onclick="newFunction.showPopupMoreInfo({{ $reservation->objectId }}, 1);">{{ __('Подробнее о броне') }}</a>
                </div>
            </div>

            <div class="info-price-reservation display-inline-block">
                <h4>{{ $reservation->price }} ₴</h4>
                <span>{{ \App\Helpers\Func::getDay($reservation->startReservationDate) }} - {{ \App\Helpers\Func::getDay($reservation->endReservationDate) }}</span>
            </div>
            <div class="status-reservation">
                <ul>
                    <li>
                        @if($reservation->statusId == 3)
                            <span>{!! __('Бронирование <br> подтвержденно') !!}</span>
                        @elseif($reservation->statusId == 2)
                            <span>{!! __('Бронирование <br>было отменено') !!}</span>
                        @elseif($reservation->statusId == 4)
                            <span>{{ __('В ожидании') }}</span>
                        @endif
                    </li>
                    <li>
                        @if(!$reservation->isComment)
                            @php
                                $codeReview = DB::table('code_reviews_user')->where('reservationId', $reservation->id)->first();
                                if($codeReview) {
                                    $link = url('/'). '/feedback/?email='.$reservation->visitorEmail.'&code='.$codeReview->code;
                                }
                                else {
                                    $link = false;
                                }

                            @endphp
                            @if($link)
                            <a href="{{ $link }}" target="_blank">{{ __('Оставить отзыв') }}</a>
                            @endif
                        @else
                            <a href="">{{ __('Посмотреть отзыв') }}</a>
                        @endif
                    </li>
                    <li>
                        <a href="{{ route('hotel', ['id' => $reservation->objectId, 'slug' => $reservation->object->subdomain]) }}" target="_blank">{{ __('Забронировать еще раз') }}</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="quick-view popup">

        </div>
@endforeach