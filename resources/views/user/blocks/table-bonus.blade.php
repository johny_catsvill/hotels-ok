@if(count($listReservations) > 0)
    <table>
        <tr>
            <th>Дата бронирования за которое был бонус</th>
            <th>Название бонуса</th>
            <th>Сумма бонуса</th>
            <th>Общая сумма</th>
        </tr>
        @foreach($listReservations as $reservation)
            <tr onclick="">
                <td>{{ date('d.m.Y', $reservation->startReservationDate) }}</td>
                <td>{{ $reservation->bonus->name }}</td>
                <td>{{ $reservation->price * $reservation->bonus->procent / 100 }} ₴</td>
                <td>{{ $reservation->price }} ₴
                    <i class="fa fa-angle-down"></i>
                </td>
            </tr>
        @endforeach
    </table>
@else
    <span>У Вас отсутствуют бронирования</span>
@endif