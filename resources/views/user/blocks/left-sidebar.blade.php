<style>
    body {
        background-color: #F9F8F8;
    }
</style>
<div class="col-lg-3">
    <div class="left-sidebar-iser">
        <div class="background-profile">
            <div class="user-img" style="background-image: url('/images/w512h5121347464802Personal.png')"></div>
            <img src="/images/w512h5121347464802Personal.png" alt="">
        </div>

        <div class="menu-user">
            <ul>
                <li>
                    @if(Request::is('account'))
                    <a class="active" href="{{ route('user.profile') }}">
                        <img src="/images/icon-user/profile.png" alt="">{{ __('Мой профиль') }}</a>
                    @else
                        <a href="{{ route('user.profile') }}">
                            <img src="/images/icon-user/profile-no-active.png" alt="">{{ __('Мой профиль') }}</a>
                    @endif
                </li>

                <li>
                    @if(Request::is('account/reservations'))
                    <a class="active"  href="{{ route('user.reservations') }}">
                        <img src="/images/icon-user/reservation.png" alt="">{{ __('Мои бронирования') }}</a>
                    @else
                        <a  href="{{ route('user.reservations') }}">
                            <img src="/images/icon-user/reservation-no-active.png" alt="">{{ __('Мои бронирования') }}</a>
                    @endif

                </li>
                <li>
                    @if(Request::is('account/bonus'))
                    <a  class="active"  href="{{ route('user.bonus') }}">
                        <img src="/images/icon-user/balans.png" alt="">{{ __('Баланс, бонусы') }}</a>
                    @else
                        <a href="{{ route('user.bonus') }}">
                            <img src="/images/icon-user/balans-no-active.png" alt="">{{ __('Баланс, бонусы') }}</a>
                    @endif

                </li>


                <li>
                    @if(Request::is('account/promotional-offers'))
                        <a class="active" href="{{ route('user.offers') }}"><img src="/images/icon-user/offer.png" alt="">{{ __('Акционные предложения') }}</a>
                    @else
                        <a href="{{ route('user.offers') }}"><img src="/images/icon-user/offer-no-active.png" alt="">{{ __('Акционные предложения') }}</a>
                    @endif
                </li>
                <li>
                    @if(Request::is('account/feedback'))
                        <a class="active" href="{{ route('user.feedback') }}"><img src="/images/icon-user/message.png" alt="">{{ __('Обратная связь') }}</a>
                    @else
                        <a href="{{ route('user.feedback') }}"><img src="/images/icon-user/message-no-active.png" alt="">{{ __('Обратная связь') }}</a>
                    @endif
                </li>
                {{--<li><a href=""><img src="/images/icon-user/partnerka-no-active.png" alt="">Партнерские программы</a></li>--}}
                <li><a href="{{ route('logout') }}"><img src="/images/icon-user/exit.png" alt="">{{ __('Выход') }}</a></li>
            </ul>
        </div>

    </div>
</div>