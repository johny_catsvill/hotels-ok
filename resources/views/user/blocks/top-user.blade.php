<section id="contacts" class="margin-bottom-50px" style="background-image: url(/images/{{ $background }});">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h1>{{ __($name) }}</h1>
                @include('site.blocks.breadcrumbs')
            </div>
        </div>
    </div>
</section>