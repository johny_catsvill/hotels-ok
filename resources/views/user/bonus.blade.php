@extends('site.startpage')

@section('content')
    @include('user.blocks.top-user', ['name' => 'Баланс / Бонусы', 'background' => 'top-user.jpg'])
    <div class="container">
        <div class="row">
            @include('user.blocks.left-sidebar')
            <div class="col-lg-9">
                <div class="bonus-content">
                    <h3>{{ __('Баланс / Бонусы') }}</h3>
                    <ul class="info-user">
                        <li><span>{{ __('Имя') }}</span><b class="name-user">{{ $user->client->name }} {{ $user->client->lastName }}</b></li>
                        <li><span>{{ __('Почта') }}</span><b>{{ $user->email }}</b></li>
                        <li><span>{{ __('Телефон') }}</span><b>{{ $user->client->phone }}</b></li>
                        <li><span>{{ __('Дата регистрации') }}</span><b>{{ date('d.m.Y', $user->creationDate) }}</b></li>
                        <li><span>{{ __('Баланс') }}</span><b class="balans-user">{{ $user->client->balans }} грн</b></li>
                    </ul>

                    <div class="filters-balans-user">
                        <ul>
                            <li><span>{{ __('Сортировать по') }}:</span></li>
                            <li>
                                <input type="hidden" name="date">
                                <b ><input type="text" class="date-range-picker" value="{{ $dateFrom }} - {{ $dateTo }}"></b>
                            </li>
                            <li>
                                <input type="hidden" name="city" value="" onchange="newFunction.getBonusList();">
                                <b onclick="UserScripts.showDropDown(this);">{{ __('Города') }}</b>
                                <div class="dropdown-filter-reservation dropdown-class">
                                    <ul>
                                        <li>
                                            <a href="javascript:void(0);" data-id="" >{{ __('Все бронирования') }}</a>
                                        </li>
                                        @foreach($cities as $city)
                                            <li>
                                                <a href="javascript:void(0);" data-id="{{ $city->id }}">{{ $city->name }}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>

                    <b class="caption-bonus">{{ __('Ваши начисления') }}</b>
                    <div class="list-tabs-balans-user">
                        <ul>
                            <li class="active">
                                <a href="javascript:void(0);" data-tab="1">
                                    <img src="/images/bonus-active.png" alt="">{{ __('Список') }}
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0);" data-tab="2">
                                    <img src="/images/graffic-noactive.png" alt="">{{ __('Графики') }}
                                </a>
                            </li>
                        </ul>
                    </div>

                    <div class="tabs-info-user">
                        <ul>
                            <li class="active" data-tab="1">
                                <div class="one-tab-info-user">
                                    <div class="wrap-table-bonus">
                                        @include('user.blocks.table-bonus')
                                    </div>
                                </div>
                            </li>
                            <li data-tab="2">
                                <div class="one-tab-info-user">
                                    <canvas id="myChartReservationLine"></canvas>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('javascript')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.bundle.min.js"></script>
    <script>
        var MONTHS = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

        window.chartColors = {
            red: 'rgb(255, 99, 132)',
            orange: 'rgb(255, 159, 64)',
            yellow: 'rgb(255, 205, 86)',
            green: 'rgb(75, 192, 192)',
            blue: 'rgb(54, 162, 235)',
            purple: 'rgb(153, 102, 255)',
            grey: 'rgb(201, 203, 207)'
        };

        var randomScalingFactor = function() {
            return Math.round(Math.random() * 20);
        };

        var config = {
            type: 'line',
            data: {
                labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
                datasets: [{
                    label: 'Бронирования',
                    backgroundColor: window.chartColors.red,
                    borderColor: window.chartColors.red,
                    data: [
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor()
                    ],
                    fill: false,
                }]
            },
            options: {
                responsive: true,
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Месяца'
                        }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Количество броней'
                        },
                        ticks: {
                            min: 0,
                            max: 20,

                            // forces step size to be 5 units
                            stepSize: 1
                        }
                    }]
                }
            }
        };

        window.onload = function() {
            var ctx = document.getElementById('myChartReservationLine').getContext('2d');
            window.myLine = new Chart(ctx, config);
        };

        var colorNames = Object.keys(window.chartColors);


    </script>

@endsection
