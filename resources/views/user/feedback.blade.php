@extends('site.startpage')
@section('content')
    @include('user.blocks.top-user', ['name' => 'Обратная связь', 'background' => 'top-user.jpg'])
    <div class="container">
        <div class="row">
            @include('user.blocks.left-sidebar')
            <div class="col-lg-9">
                <div class="feedback-account">
                    <ul>
                        <li>
                            <h3>{{ __('Звоните нам') }}</h3>
                            <p class="first-p">{{ __('Мы будем рады ответить на Ваш звонок в любое время суток (ежедневно).') }}</p>
                            <span>0 800 800-000</span>
                            <p class="two-p">{{ __('А хотите мы Вам перезвоним?') }}</p>
                            <a href="javascript:void(0);" onclick="Popup.showRecallPopup();">{{ __('Закажите обратный звонок') }}</a>

                            <div class="contact-us-feedback">
                                <b>{{ __('Свяжитесь с нами') }}</b>
                                <b>{{ __('Наш офис') }}: </b><span>г. Киев, ул. Иванова, 35</span> <br>
                                <b>{{ __('Электронный адрес') }}: </b><span>info@hotels-ok.com</span>
                            </div>
                        </li>
                        <li>
                            <h3>{{ __('Пишите нам') }}</h3>
                            <p class="first-p">{{ __('Мы ответим на ваше письмо очень быстро, как только сможем.') }} </p>
                            <form action="">
                                <input type="hidden" name="url" value="{{ url(Request::path()) }}">
                                {{ csrf_field() }}
                                <div class="field-feedback">
                                    <input type="text" name="name" placeholder="{{ __('Ваше имя') }}">
                                    <span class="red red-error-popup">{{ __('Укажите Ваше имя') }}</span>
                                </div>

                                <div class="field-feedback">
                                    <input type="text" name="email" placeholder="{{ __('Ваш') }} E-mail">
                                    <span class="red red-error-popup">{{ __('Неккоректный email') }}</span>
                                </div>

                                <textarea name="review" id="" cols="30" rows="10" placeholder="{{ __('Как мы можем вам помочь?') }}"></textarea>
                                <span class="red red-error-popup">{{ __('Укажите текст сообщения(не меньше 10 символов)') }}</span>
                                <input type="submit" name="submit" value="Отправить" onclick="newFunction.saveReviewSite(this, event);">
                                <input type="hidden" name="type" value="wishes">
                            </form>
                        </li>
                    </ul>
                </div>
                <div class="map-us">
                    <div id="map-feedback"></div>
                </div>
            </div>
        </div>
    </div>
    <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyAzqr9UAi_u2u9LDTKpQs9ufwDOrEOviuM&libraries=geometry"></script>
    @include('site.blocks.messages-phone')
@endsection
@section('javascript')
    @parent
    <script>
        function initMap() {
            var uluru = {lat: +50.4684391, lng: +30.5306431 };
            var map = new google.maps.Map(document.getElementById('map-feedback'), {
                zoom: 15,
                center: uluru
            });
            var marker = new google.maps.Marker({
                position: uluru,
                map: map
            });

            google.maps.event.trigger(map, "resize");
        }
        initMap();
    </script>
@endsection
