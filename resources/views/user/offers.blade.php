@extends('site.startpage')
@section('content')
    @include('user.blocks.top-user', ['name' => 'Акции', 'background' => 'top-user.jpg'])
    <div class="container">
        <div class="row">
            @include('user.blocks.left-sidebar')
            <div class="col-lg-9">
                <div class="offers-account">
                    <div class="sorts-offers-object">
                        <ul>
                            <li><span>{{ __('Сортировка') }}:</span></li>
                            <li class="active"><a href="javascript:void(0);">Рекомендованные</a></li>
                            <li><a href="javascript:void(0);">Сначала дешевые</a></li>
                            <li><a href="javascript:void(0);">По рейтингу</a></li>
                        </ul>
                    </div>
                    <div class="dates-and-name-hotel">
                        <span>Выберите даты проживания в отеле, чтобы просмотреть цены и номера</span>
                        <div class="main-info-search">
                            <ul>
                                <li>
                                    <input type="text" name="object" placeholder="Город или отель">
                                </li>
                                <li>
                                    <input type="text" name="dateFrom" placeholder="Заезд">
                                </li>
                                <li>
                                    <input type="text" name="dateFrom" placeholder="Отьезд">
                                </li>
                                <li>
                                    <input type="text" name="guest" placeholder="Гостей">
                                </li>
                                <li><a href="javascript:void(0);">Еще фильтры</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection