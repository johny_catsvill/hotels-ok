<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <link rel="stylesheet" href="/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/stub.css">
</head>
<body>
    <section id="wrapper">
        <div class="overlays"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="main-content">
                        <h2>Страница на обслуживании, зайдите позже!</h2>
                        <span>В настоящее время на странице ведутся технические работы. Мы сообщим Вам когда раздел будет функционировать.</span>
                        <hr>
                        <a href="/">Вернуться на главную</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</body>
</html>