
    <a href="javascript:void(0);" onclick="Popup.closePopup(this);"><img src="/images/close-popup-quick-view.png" alt=""></a>
    <div class="header-quick-view" style="background-image: url(/{{$oldMainImg}});">
        <div class="header-info">
            <ul>
                <li>
                    <div class="name-hotel-and-address">
                        <h3>{{ $object->objectCategory->name }} {{ $object->name }}</h3>
                        <span>{{ $object->address }}</span>
                    </div>
                </li>
                <li>
                    <a href="{{ route('hotel', ['id' => $object->id, 'slug' => $object->subdomain]) }}" class="btn-to-hotel" target="_blank">{{ __('На страницу отеля') }}</a>
                </li>
                <li>
                    <div class="price-hotel-and-reservation-btn">
                        <span>от {{ $minPrice }} грн {{ __('сутки') }}</span>
                        <a href="{{ route('hotel', ['id' => $object->id, 'slug' => $object->subdomain]) }}">{{ __('Забронировать') }}</a>
                    </div>
                </li>
            </ul>
        </div>
        <div class="overlay-header-quick-view"></div>
    </div>
    <div class="menu-popup">
        <ul>
            <li><a href="javascript:void(0);" class="active" data-tab="1">{{ __('Об отеле') }}</a></li>
            <li><a href="javascript:void(0);" data-tab="2">{{ __('Фотографии') }} ({{ count($objectImages) }})</a></li>
            <li><a href="javascript:void(0);" data-tab="3">{{ __('Номера') }}</a></li>
            <li><a href="javascript:void(0);" data-tab="4">{{ __('На карте') }}</a></li>
            <li><a href="javascript:void(0);" data-tab="5">{{ __('Отзывы') }}</a></li>
        </ul>
    </div>
    <div class="clear"></div>
    <div class="tabs-popup">
        <ul>
            <li data-tab="1">
                <div class="about-hotel-popup-block">
                    <ul class="list-images">
                        @foreach($objectImages as $img)
                            <li>
                                <img src="{{ $img }}" alt="">
                            </li>
                        @endforeach
                    </ul>
                    <div class="description-info-hotel">
                        <div class="service-hotel">
                            <h4 class="caption-hotel">{{ __('Услуги в гостинице') }}</h4>
                            <ul>
                                @foreach($object->objectPluses as $pluse)
                                    <li><img src="/images/icon-pluses/{{ $pluse->image }}" alt=""> <span>{{ __($pluse->name) }}</span></li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <div class="description-info-hotel">
                        <h4>{{ __('Описание') }}</h4>
                        <p>{{ $object->infoAboutObject }}</p>
                    </div>
                    <div class="description-info-hotel">
                        <h4>{{ __('Расположение отеля') }}</h4>
                        <p>{{ $object->locationObject }}</p>
                    </div>
                    <div class="description-info-hotel">
                        <h4>{{ __('Информация о проезде') }}</h4>
                        <p>{{ $object->infoAboutTravel }}</p>
                    </div>
                </div>
            </li>
            <li data-tab="2" class="active">
                <div class="photos-popup-block">
                    <div class="wrap-big-img-photos-popup">
                        <div class="main-img-photo">
                            <img src="../{{ $oldMainImg }}" alt="">
                        </div>
                        <div class="background-img-photo" style="background-image: url(../{{ $oldMainImg }})"></div>
                        <div class="arrow-popup-photos">
                            <ul>
                                <li><a onclick="Gallery.prevPhoto();" href="javascript:void(0);"><img src="/images/arrow-left-gallery.png" alt=""></a></li>
                                <li><a onclick="Gallery.nextPhoto();" href="javascript:void(0);"><img src="/images/arrow-right-gallery.png" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                    <ul class="list-images-quick-view">
                        @foreach($objectImages as $img)
                            <li @if($loop->first) class="active-img" @endif>
                                <img src="{{ $img }}" alt="">
                            </li>
                        @endforeach
                    </ul>
                </div>
            </li>
            <li data-tab="3">
                <div class="rooms-popup-block">
                    <div class="header-rooms-popup-block">
                        <ul>
                            <li>{{ __('Название') }}</li>
                            <li>{{ __('Условия') }}</li>
                            <li>{{ __('Цена за сутки') }}</li>
                        </ul>
                    </div>
                    <ul>
                        @foreach($rooms as $room)
                            <li>
                            <div class="one-room-popup-block">
                                <ul>
                                    <li>
                                        <div class="wrap-img-room-popup-block">
                                            <img src="/{{ preg_replace('/norm/', 'mini', $room->firstImage) }}" alt="">
                                        </div>
                                        <span class="name-room-popup-block">{{ $room->name }}</span>
                                    </li>
                                    <li>
                                        <div class="conditions-rooms-popup-block">
                                            <ul>
                                                @foreach($room->pluses()->where('image', '!=', '')->limit(6)->get() as $pluse)
                                                    <li><img src="/images/icon-pluses-room/{{ $pluse->image }}" alt=""> {{ __($pluse->name) }}</li>
                                                @endforeach

                                            </ul>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="price-room-popup-block">
                                            <span>от {{ $room->priceWithoutDiscount }} грн<b> / {{ __('сутки') }}</b></span>
                                            <a href="javascript:void(0);"
                                               data-active="off"><i class="fa fa-angle-down"></i> {{ __('Подробнее о номере') }}</a>
                                        </div>
                                    </li>
                                </ul>
                                <div class="clear"></div>
                            </div>

                            <div class="other-content-room-popup-block">
                                <ul>
                                    <li>
                                        <div class="photo-list-rooms-popup-block">
                                            <ul>
                                                @foreach($room->images as $img)
                                                    <li><img src="{{ $img }}" alt=""></li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="services-hotel-rooms-popup-block">
                                            <h4>{{ __('Услуги') }}</h4>
                                            <ul>
                                                @foreach($room->pluses as $pluse)
                                                    @if($loop->index == 6)
                                                        @break
                                                    @endif
                                                    <li><i class="fa fa-circle" aria-hidden="true"></i> <span class="display-inline-block">{{ __($pluse->name) }}</span></li>
                                                @endforeach
                                            </ul>
                                            <span><b>{{ __('Включено:') }}</b> {{ __('НДС в размере') }} 10%</span>
                                            <a href="{{ route('hotel', ['id' => $object->id, 'slug' => $object->subdomain]) }}">{{ __('Забронировать номер') }}</a>
                                        </div>

                                    </li>
                                </ul>
                                <div class="clear"></div>
                            </div>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </li>
            <li data-tab="4">
                <div class="map-popup-block">
                    <div class="wrapper-hotel-map">
                        <div id="map"></div>
                    </div>
                </div>
            </li>
            <li data-tab="5">
                <div class="reviews-popup-block">
                    @if(count($reviews))
                    <ul>
                        @foreach($reviews as $review)
                        <li>
                            <div class="one-review-popup-block" onclick="newFunction.showFullReviewPopup(this);">
                                <ul>
                                    <li>
                                        <div class="name-review-author">
                                            <strong>{{ $review->authorName }}</strong>
                                            <span>{{ $review->lastUpdater }}, {{ $review->typeReview->nameType }}</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="hotel-rating">
                                            <a href="javascript:void(0);">{{ __('Оценка отеля') }} <b>{{ $review->avgValue }}</b></a>
                                        </div>
                                    </li>
                                </ul>
                                <div class="content-review-popup">
                                    <div class="main-content-review-hotel">
                                        <ul>
                                            <li>
                                                <div class="average-raiting-current-review">
                                                    <span>{{ __('Оценки отеля') }}</span>
                                                    <ul>
                                                        <li><b>{{ __('Комфорт') }}</b><span>{{ $review->comfortValue }}</span></li>
                                                        <li><b>{{ __('Персонал') }}</b><span>{{ $review->employeeValue }}</span></li>
                                                        <li><b>{{ __('Услуги') }}</b><span>{{ $review->serviceValue }}</span></li>
                                                        <li><b>{{ __('Цена/качество') }}</b><span>{{ $review->qualityPriceValue }}</span></li>
                                                        <li><b>{{ __('Чистота') }}</b><span>{{ $review->cleanValue }}</span></li>
                                                        <li><b>{{ __('Местоположение') }}</b><span>{{ $review->locationValue }}</span></li>
                                                    </ul>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="text-current-review">
                                                    <ul>
                                                        <li>
                                                            <span><img src="/images/pluses-review.png" alt="">{{ __('Достоинства') }}</span>
                                                            <p>{{ $review->positiveText }}</p>
                                                        </li>
                                                        <li>
                                                            <span><img src="/images/limitations.png" alt="">{{ __('Недостатки') }}</span>
                                                            <p>{{ $review->negativeText }}</p>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </li>
                        @endforeach
                    </ul>
                    @else
                        <span class="message-without-reviews">{{ __('К сожалению у даного отеля еще нету отзывов.') }}</span>
                    @endif
                </div>
            </li>
        </ul>
    </div>
