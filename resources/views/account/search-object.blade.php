@extends('account.admin_layouts.index')
@section('content')

<section id="searchObject">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 style-page">
                <h2>Поиск обьектов</h2>
                <hr>
                <form action="">
                        <tr>
                    <table class="search-object-table">
                            <td class="search-field-td">
                               <i class="fa fa-search icon-search-string" aria-hidden="true"></i>
                                <input type="text" name="search" class="form-control search-field" autocomplete="off" onkeyup="MainFunction.startFunctionChangeFieldSearch(this);" onfocus="MainFunction.changeFieldSearch(this);">
                                <div class="result-search">
                                    <div class="wrapper-field-search wrapper-cities-search" data-category="city">
                                        <span class=" caption-search cities-search-result"><i class="fa fa-university" aria-hidden="true"></i> Города</span>
                                        <ul></ul>
                                    </div>
                                    <div class="wrapper-field-search wrapper-regions-search">
                                        <span class="caption-search regions-search-result"><i class="fa fa-pie-chart" aria-hidden="true"></i> Регионы</span>
                                        <ul></ul>
                                    </div>
                                    <div class="wrapper-field-search wrapper-hotels-search">
                                        <span class="caption-search hotels-search-result"><i class="fa fa-building" aria-hidden="true"></i> Отели</span>
                                        <ul></ul>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="wrapper-period">
                                    <span><b>Период:</b> с <input type="text" name="dateFrom" class="datepicker" id="datetimepicker1" value=""> по <input type="text" value="" name="dateTo" class="datepicker" id="datetimepicker2">
                                    </span>
                                    
                                </div>
                            </td>
                            <td>
                                <input type="number" name="amountPerson" class="person-field form-control" min="1" max="6" placeholder="2" required value="">
                            </td>
                            <td>
                                <div class="form-groups wrapper-submit-btn">
                                    <input type="submit" class="submit-btn-home-search form-control" value="Найти" onclick="MainFunction.showObjectForReservation(event);">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="wrapper-period">
                                    <span><b>Цена:</b> от <input type="text" class="datepicker" name="priceFrom" value=""> до <input type="text" value="" name="priceTo" class="datepicker">
                                    </span>
                                    
                                </div>
                            </td>
                            <td>
                                <label for=""><input type="checkbox" name="isDiscount"> Со скидкой</label>
                            </td>
                            <td></td>
                            <td></td>
                        </tr>
                    </table>
                    <input type="hidden" name="category" class="hide-field-category" value="">
                    <input type="hidden" name="" class="select-category-and-id" value="">
                    <input type="hidden" name="countryId" class="js-select-country-id">
                            <input type="hidden" name="page" value="1">
                </form>
                <hr>
                <div class="alert alert-danger">Не выбран ни один обьект.</div>
                <div class="list-search-objects">
                    
                </div>
                <a href="javascript:void(0);" onclick="" class="btn btn-primary hide btn-show-more-objects">Показать еще</a>
            </div>
        </div>
    </div>



</section>

<div class="popup-pluses">
    <a href="javaaript:void(0);" class="btn-close-popup" onclick="MainFunction.closePopup(this);">
        <img src="/images/close.png" alt="">
    </a>
</div>


@endsection