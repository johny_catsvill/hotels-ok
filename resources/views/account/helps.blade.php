@extends('account.index')
@section('content')

<section id="settings">
    <div class="container">
        <div class="row">
            <div class="wrapper-settings style-page">
               <div class="col-lg-9">
                   <h2>Помощь и поддержка</h2>
                   <hr>    
                   <div class="alert alert-info">Поддержка для отелей осуществляется круглосуточно по всему спектру вопросов.</div>
                   <div class="create-ticket-form">
                       <form action="{{ route('help', $subdomain) }}" method="post">
                           <div class="field">
                               <label for="">Тема:</label>
                               <input type="text" name="themeTicket">
                           </div>
                           <div class="field">
                               <label for="">Сообщение:</label>
                               <textarea name="message" id="" cols="30" rows="10"></textarea>
                           </div>
                           {{ csrf_field() }}
                           <input type="submit" name="submit" value="Отправить" class="btn btn-primary">
                           <div class="clear"></div>
                       </form>
                   </div>
                   <div class="clear"></div>
                   <div class="list-tickets">
                       <table>
                           <tr>
                               <th style="width:10%;">ID</th>
                               <th style="width:60%;">Тема</th>
                               <th style="width:25%;">Последнее сообщение</th>
                               <th style="width:5%;"></th>
                           </tr>
                           @if(count($tickets) > 0)
                               @foreach($tickets as $ticket)
                                   <tr>
                                       <td>{{ $ticket->id }}</td>
                                       <td><a href="{{ route('ticket', ['domain' => $subdomain, 'id' => $ticket->id]) }}"><i class="fa fa-envelope-o @if($ticket->reading == 0) red @endif" aria-hidden="true" ></i> {{ $ticket->themeTicket }}</a></td>
                                       <td><span>{{ $ticket->dateLastMessages }}</span></td>
                                       <td ><a href="{{ route('ticket-delete', ['domain' => $subdomain, 'id' => $ticket->id]) }}" style="color:red;"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td>
                                   </tr>
                               @endforeach
                           @endif
                           
                       </table>
                   </div>
                   <p>В любое время Вы также можете связаться с нами следующими способами:</p>
                   <ul class="list-telefons-supports">
                       <li><b>Email:</b> hotelsok25@gmail.com</li>
                       <li><b>Телефон:</b>0636108513</li>
<!--                       <li>0636108513</li>-->
                   </ul>
               </div>
                @include('account.blocks.right_sidebar')
                <div class="clear"></div>
            </div>
        </div>
    </div>
</section>


@endsection