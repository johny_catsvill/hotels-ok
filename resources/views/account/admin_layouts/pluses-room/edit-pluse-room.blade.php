@extends('account.admin_layouts.index')
@section('content')


<section id="EditServices">
    <div class="container">
        <div class="row">
            <div class="wrapper-edit-services style-page">
                <div class="col-lg-12">
                    <h2>Добавление плюса комнаты</h2>
                    <hr>
                    @include('auth.success_errors_block')
                    <form action="{{ route('edit-pluse-room') }}" method="post" class="style-form-edit-service" enctype="multipart/form-data">
                        <div class="field">
                            <label for="">Название плюса</label>
                            <input type="text" name="name" value="{{ $pluseRoom ? $pluseRoom->name : '' }}">
                        </div>
                        <div class="field">
                            <label for="">Категория плюса</label>
                            
                            <select name="roomCategoryId" id="">
                                <option value="">Выберите категорию</option>
                                @foreach($listCategoriesPlusesRoom as $category)
                                   @if($pluseRoom && $pluseRoom->roomCategoryId == $category->id)
                                        <option value="{{ $category->id }}" selected >{{ $category->name }}</option>
                                    @else
                                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                                    @endif
                                @endforeach
                            </select>
                            
                        </div>
                        <div class="field">
                            <label for="">Изображение</label>
                            <input type="file" name="image">
                            @if($pluseRoom->image)
                                <img class="img-pluse" src="/images/icon-pluses/{{ $pluseRoom->image }}" alt="">
                            @endif
                        </div>
                        <input type="hidden" name="pluseRoomId"  value="{{ $pluseRoom ? $pluseRoom->id : '' }}">
                        {{ csrf_field() }}
                        <input type="submit" value="Сохранить" name="submit" class="btn btn-success">
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>


@endsection