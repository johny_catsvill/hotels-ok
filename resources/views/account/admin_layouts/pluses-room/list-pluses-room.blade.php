@extends('account.admin_layouts.index')
@section('content')

<section id="listServices">
    <div class="container">
        <div class="row">
            <div class="wrapper-list-services style-page">
                <div class="col-lg-12">
                    <h2>Список плюсов комнат</h2>
                    <hr>
                    <a href="{{ route('edit-pluse-room') }}" class="btn btn-primary btn-add-services-style">Добавить плюс комнаты</a>
                    @include('auth.success_errors_block')
                    <div class="list-services-block">
                        <table>
                            <tr>
                                <th>Идентификатор</th>
                                <th>Название</th>
                                <th>Категория</th>
                                <th>Наличие изображения</th>

                                <th>Редактировать</th>
                                <th>Удалить</th>
                            </tr>
                            @foreach($listPlusesRoom as $pluse)
                                <tr>
                                    <td>{{ $pluse->id }}</td>
                                    <td>{{ $pluse->pluseName }}</td>
                                    <td>{{ $pluse->categoryName }}</td>
                                    <td>
                                        @if($pluse->image)
                                            Присутствует
                                        @else
                                            Отсутствует
                                        @endif
                                    </td>

                                    <td><a href="{{ route('edit-pluse-room', ['id' => $pluse->id]) }}" class="btn btn-primary">Редактировать</a></td>
                                    <td class="td-btn-delete"><a onclick="MainFunction.prepareAboutDeleteElement(this,event);" href="" class="btn btn-danger">Удалить</a>
                                        <div class="prepareBlockDelete">
                                            <span>Точно удалить?</span>
                                            <a href="{{ route('delete-pluse-room', ['id' => $pluse->id]) }}" class="btn btn-success">Да</a>
                                            <a href="javascript:void(0);" onclick="MainFunction.cancelDeleteElement(this);" class="btn btn-primary">Нет</a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </div>
</section>


@endsection