@extends('account.admin_layouts.index')
@section('content')


    <section id="EditServices">
        <div class="container">
            <div class="row">
                <div class="wrapper-edit-services style-page">
                    <div class="col-lg-12">
                        <h2>Добавление региона</h2>
                        <hr>
                        @include('auth.success_errors_block')
                        @if($page)
                            <form action="{{ route('edit-page', $page->id) }}" method="post" class="style-form-edit-service">
                        @else
                            <form action="{{ route('edit-page') }}" method="post" class="style-form-edit-service">
                        @endif
                            <div class="field">
                                <label for="">Страница</label>
                                <input type="text" name="name" value="{{ $page ? $page->name : '' }}">
                            </div>

                            <div class="field">
                                <label for="">Slug</label>
                                <input type="text" name="slug" value="{{ $page ? $page->slug : '' }}">
                            </div>

                            <div class="field">
                                <label for="">Title</label>
                                <input type="text" name="title" value="{{ $page ? $page->title : '' }}">
                            </div>

                            <div class="field">
                                <label for="">Keywords</label>
                                <input type="text" name="keywords" value="{{ $page ? $page->keywords : '' }}">
                            </div>

                            <div class="field">
                                <label for="">Description</label>
                                <textarea name="description" id="" cols="30" rows="10">{{ $page ? $page->description : '' }}</textarea>
                            </div>


                            {{ csrf_field() }}
                            <input type="submit" value="Сохранить" name="submit" class="btn btn-success">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection