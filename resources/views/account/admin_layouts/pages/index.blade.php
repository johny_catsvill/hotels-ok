@extends('account.admin_layouts.index')
@section('content')

    <section id="listServices">
        <div class="container">
            <div class="row">
                <div class="wrapper-list-services style-page">
                    <div class="col-lg-12">
                        <h2>Список страниц</h2>
                        <hr>
                        @include('auth.success_errors_block')
                        <div class="tabs-pages">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a data-toggle="tab" href="#panel1">Страницы</a></li>
                                <li><a data-toggle="tab" href="#panel2">Разметка(главная)</a></li>
                            </ul>
                            <div class="tab-content">
                                <div id="panel1" class="tab-pane fade in active">
                                    <a href="{{ route('edit-page') }}" class="btn btn-primary btn-add-services-style">Добавить страницу</a>

                                    <div class="list-services-block">
                                        <table>
                                            <tr>
                                                <th>Идентификатор</th>
                                                <th>Страница</th>
                                                <th>Title</th>
                                                <th></th>
                                            </tr>
                                            @foreach($pages as $page)
                                                <tr>
                                                    <td>{{ $page->id }}</td>
                                                    <td>{{ $page->name }}</td>
                                                    <td>{{ $page->title }}</td>
                                                    <td>
                                                        <a href="{{ route('edit-page', $page->id) }}" class="btn btn-primary">Редактировать</a>
                                                        <a href="{{ route('delete-page', $page->id) }}" class="btn btn-danger">Удалить</a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                </div>
                                <div id="panel2" class="tab-pane">
                                    <div class="razmetka">
                                        <form action="" method="post" style="width:600px;">
                                            <div class="field">
                                                <label for="">addressLocality</label>
                                                <input type="text" name="addressLocality">
                                            </div>

                                            <div class="field">
                                                <label for="">streetAddress</label>
                                                <input type="text" name="streetAddress">
                                            </div>

                                            <div class="field">
                                                <label for="">postalCode</label>
                                                <input type="text" name="postalCode">
                                            </div>

                                            <div class="field">
                                                <label for="">GeoCoordinates</label>
                                                <input type="text" name="GeoCoordinates">
                                            </div>

                                            <div class="field">
                                                <label for="">latitude</label>
                                                <input type="text" name="latitude">
                                            </div>

                                            <div class="field">
                                                <label for="">longitude</label>
                                                <input type="text" name="longitude">
                                            </div>

                                            <div class="field">
                                                <label for="">telephone</label>
                                                <input type="text" name="telephone">
                                            </div>

                                            <input type="submit" name="submit" value="Сохранить" class="btn btn-primary">

                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </section>


@endsection