@extends('account.admin_layouts.index')
@section('content')


<section id="EditServices">
    <div class="container">
        <div class="row">
            <div class="wrapper-edit-services style-page">
                <div class="col-lg-12">
                    <h2>Добавление ближайщего обьекта</h2>
                    <hr>
                    @include('auth.success_errors_block')
                    <form action="{{ route('edit-object-beside') }}" method="post" class="style-form-edit-service">
                        <div class="field">
                            <label for="">Название услуги</label>
                            <input type="text" name="name_typeObject" value="{{ $typeObject ? $typeObject->name : '' }}">
                        </div>
                        <input type="hidden" name="typeObjectId"  value="{{ $typeObject ? $typeObject->id : '' }}">
                        {{ csrf_field() }}
                        <input type="submit" value="Сохранить" name="submit" class="btn btn-success">
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>


@endsection