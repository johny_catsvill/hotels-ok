@extends('account.admin_layouts.index')
@section('content')
    <section>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 style-page">
                    <h2>Пользователи</h2>
                    <hr>
                    <a href="" class="btn btn-primary btn-add-services-style">Создать пользователя</a>
                    @include('auth.success_errors_block')
                    <div>
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Обьекты</a></li>
                            <li><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Профили</a></li>
                            <li><a href="#manager" aria-controls="successReservation" role="tab" data-toggle="tab">Управляющие (админка)</a></li>
                            <li><a href="#roles" aria-controls="successReservation" role="tab" data-toggle="tab">Роли</a></li>
                        </ul>

                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="home">
                                <div class="list-application-object">
                                    <table>
                                        <tr>
                                            <th>ID</th>
                                            <th>Имя</th>
                                            <th>Дата регистрации</th>
                                            <th>Телефон</th>
                                            <th>Email</th>
                                            <th></th>
                                        </tr>
                                        @foreach($objects as $object)
                                        <tr>
                                            <td>{{ $object->id }}</td>
                                            <td>{{ $object->object->name }}</td>
                                            <td>{{ date('d.m.Y h:i', $object->creationDate) }}</td>
                                            <td>{{ $object->phone }}</td>
                                            <td>{{ $object->email }}</td>
                                            <td><a href="" class="btn btn-danger">Заблокировать</a></td>
                                        </tr>
                                        @endforeach
                                    </table>
                                </div>
                            </div>

                            <div role="tabpanel" class="tab-pane" id="profile">
                                <div class="list-application-object">
                                    <table>
                                        <tr>
                                            <th>ID</th>
                                            <th>Имя</th>
                                            <th>Дата регистрации</th>
                                            <th>Количество броней</th>
                                            <th>Телефон</th>
                                            <th>Email</th>
                                            <th></th>
                                        </tr>
                                        @foreach($profiles as $profile)
                                            <tr>
                                                <td>{{ $profile->id }}</td>
                                                <td>{{ $profile->client->name }}</td>
                                                <td>{{ date('d.m.Y h:i', $profile->creationDate) }}</td>
                                                <td>18</td>
                                                <td>{{ $profile->phone }}</td>
                                                <td>{{ $profile->email }}</td>
                                                <td><a href="" class="btn btn-danger">Заблокировать</a></td>
                                            </tr>
                                        @endforeach
                                    </table>
                                </div>
                            </div>

                            <div role="tabpanel" class="tab-pane" id="manager">
                                Менеджеры
                            </div>

                            <div role="tabpanel" class="tab-pane" id="roles">
                                Роли
                            </div>


                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection