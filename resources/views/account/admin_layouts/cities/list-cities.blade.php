@extends('account.admin_layouts.index')
@section('content')

<section id="listServices">
    <div class="container">
        <div class="row">
            <div class="wrapper-list-services style-page">
                <div class="col-lg-12">
                    <h2>Список городов системы</h2>
                    <hr>
                    <a href="{{ route('edit-city') }}" class="btn btn-primary btn-add-services-style">Добавить город</a>
                    @include('auth.success_errors_block')
                    <div class="list-services-block">
                        <table>
                            <tr>
                                <th>Идентификатор</th>
                                <th>Название города</th>
                                <th>Статус</th>
                                <th>Редактировать</th>
                                <th>Удалить</th>
                            </tr>
                            @foreach($listCities as $city)
                                <tr>
                                    <td>{{ $city->id }}</td>
                                    <td>{{ $city->name }}</td>
                                    <td>ON</td>
                                    <td><a href="{{ route('edit-city', ['id' => $city->id]) }}" class="btn btn-primary">Редактировать</a></td>
                                    <td><a href="{{ route('delete-city', ['id' => $city->id]) }}" class="btn btn-danger">Удалить</a></td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </div>
</section>


@endsection