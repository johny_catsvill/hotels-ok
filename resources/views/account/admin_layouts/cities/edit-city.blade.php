@extends('account.admin_layouts.index')
@section('content')


<section id="EditServices">
    <div class="container">
        <div class="row">
            <div class="wrapper-edit-services style-page">
                <div class="col-lg-12">
                    <h2>Добавление города</h2>
                    <hr>
                    @include('auth.success_errors_block')
                    <form action="{{ route('edit-city') }}" method="post" class="style-form-edit-service">
                        <div class="field">
                            <label for="">Название города:</label>
                            <input type="text" name="name" id="address" value="{{ $city ? $city->name : '' }}" onkeyup="initMapTimer(event);">
                        </div>
                        <div class="field">
                            <label for="">Выберите страну:</label>
                            <select name="countryId" id="">
                                @foreach($listCountries as $country)
                                    <option value="{{ $country->id }}">{{ $country->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="field">
                            <label for="">Выберите регион</label>
                            <select name="regionId" id="">
                                    @foreach($listRegions as $region)
                                        <option value="{{ $region->id }}">{{ $region->name }}</option>
                                    @endforeach
                            </select>
                        </div>
                        <div class="field">
                            <label for="">Код города:</label>
                            <input type="text" name="phoneCode" value="{{ $city ? $city->phoneCode : '' }}">
                        </div>
                        <div class="field">
                            <label for="">Широта:</label>
                            <input type="text" name="latitudeGps" value="{{ $city ? $city->latitudeGps : ''}}" placeholder="Заполняются автоматически" readonly>
                        </div>
                        <div class="field">
                            <label for="">Долгота:</label>
                            <input type="text" name="longitudeGps" value="{{ $city ? $city->longitudeGps : '' }}" placeholder="Заполняются автоматически" readonly>
                        </div>
                        <div style=" width: 100%; height: 400px;" id="map_canvas"></div>
                        
                          
                        @include('account.admin_layouts.blocks.map_geocoder')
                        <input type="hidden" name="cityId"  value="{{ $city ? $city->id : '' }}">
                        {{ csrf_field() }}
                        <br>
                        <input type="submit" value="Сохранить" name="submit" class="btn btn-success" style="margin-bottom:20px;">
                    </form>
                </div>
                        <div class="clear"></div>
            </div>
        </div>
    </div>
</section>


@endsection