@extends('account.admin_layouts.index')
@section('content')


<section id="EditServices">
    <div class="container">
        <div class="row">
            <div class="wrapper-edit-services style-page">
                <div class="col-lg-12">
                    <h2>Добавление услуги для обьектов</h2>
                    <hr>
                    @include('auth.success_errors_block')
                    <form action="{{ route('edit-service') }}" enctype="multipart/form-data" method="post" class="style-form-edit-service">
                        <div class="field">
                            <label for="">Название услуги</label>
                            <input type="text" name="name_service" value="{{ $pluse ? $pluse->name : '' }}">
                        </div>
                        <div class="field">
                            <label for="">Изображение</label>
                            <input type="file" name="image">
                            @if($pluse->image)
                                <img class="img-pluse" src="/images/icon-pluses/{{ $pluse->image }}" alt="">
                            @endif    
                        </div>
                        <input type="hidden" name="service_id"  value="{{ $pluse ? $pluse->id : '' }}">
                        {{ csrf_field() }}
                        <input type="submit" value="Сохранить" name="submit" class="btn btn-success">
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>


@endsection