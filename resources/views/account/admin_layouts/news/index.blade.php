@extends('account.admin_layouts.index')
@section('content')

    <section id="listServices">
        <div class="container">
            <div class="row">
                <div class="wrapper-list-services style-page">
                    <div class="col-lg-12">
                        <h2>Список новостей</h2>
                        <hr>
                        <a href="{{ route('edit-news-admin') }}" class="btn btn-primary btn-add-services-style">Добавить новость</a>
                        @include('auth.success_errors_block')
                        <div class="list-services-block">
                            <table>
                                <tr>
                                    <th>Идентификатор</th>
                                    <th>Имя</th>
                                    <th>Категория</th>
                                    <th>Дата публикации</th>
                                    <th>Активность</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                                @foreach($news as $new)
                                    <tr>
                                        <td>{{ $new->id }}</td>
                                        <td>{{ $new->name }}</td>
                                        <td>{{ $new->category->name }}</td>
                                        <td>{{ $new->date_published }}</td>
                                        @if($new->active)
                                            <td>Да</td>
                                        @else
                                            <td>Нет</td>
                                        @endif
                                        <td>
                                            <a href="{{ route('edit-news-admin', $new->id) }}" class="btn btn-primary">Редактировать</a>
                                        </td>
                                        <td>
                                            <a href="{{ route('delete-news', $new->id) }}" class="btn btn-danger">Удалить</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </section>


@endsection