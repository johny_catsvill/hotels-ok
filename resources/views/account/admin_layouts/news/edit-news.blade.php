@extends('account.admin_layouts.index')
@section('content')


    <section id="EditServices">
        <div class="container">
            <div class="row">
                <div class="wrapper-edit-services style-page">
                    <div class="col-lg-12">
                        <h2>Добавление новости</h2>
                        <hr>
                        @include('auth.success_errors_block')
                        @if($news)
                            <form method="post" action="{{ route('edit-news-admin', $news->id) }}" class="style-form-edit-service" enctype="multipart/form-data">
                        @else
                            <form method="post" action="{{ route('edit-news-admin') }}" class="style-form-edit-service" enctype="multipart/form-data">
                        @endif
                            <div class="field">
                                <label for="">Название новости</label>
                                <input type="text" name="name" class="name_text" value="{{ isset($news->name) ? $news->name : '' }}">
                            </div>
                            <div class="field">
                                <label for="">Slug</label>
                                <input type="text" name="slug" class="name_text_translit" value="{{ isset($news->slug) ? $news->slug : '' }}">
                            </div>

                            <div class="field">
                                <label for="">Ключевики</label>
                                <textarea name="keywords" >{{ isset($news->keywords) ? $news->keywords : '' }}</textarea>
                            </div>

                            <div class="field">
                                <label for="">Мета описание</label>
                                <textarea name="description">{{ isset($news->description) ? $news->description : '' }}</textarea>
                            </div>


                            <div class="field">
                                <label for="">Категория</label>
                                <select name="category_id" id="">
                                    <option value="">Выберите категорию</option>
                                    @foreach($categories as $category)
                                        <option @if(isset($news->category_id) && $news->category_id == $category->id) selected @endif value="{{ $category->id }}">{{ $category->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="field">
                                <label for="">Описание</label>
                            </div>
                            <div class="clear"></div>
                            <textarea name="text" id="text-news" cols="30" rows="10">{{ isset($news->text) ? $news->text : '' }}</textarea>
                            <div class="field">
                                <label for="">Дата публикации</label>
                                <input type="text" name="date_published" id="datetimepicker-news" value="{{ isset($news->date_published) ? $news->date_published : '' }}">
                            </div>

                            <div class="field">
                                <label for="">Изображение</label>
                                <input type="file" name="image">
                            </div>
                             @if(isset($news->image))
                                    <img src="{{ '/images/news/'.$news->image }}" alt="" class="img-news-style">
                             @endif

                            <div class="field">
                                <label for="">Год публикации</label>
                                <input type="text" name="year" value="{{ isset($news->year) ? $news->year : '' }}">
                            </div>
                            <div class="field">
                                <label for="">Активность</label>
                                <input type="checkbox" name="active" @if(isset($news->active) && $news->active) checked @endif>
                            </div>
                            {{ csrf_field() }}
                                <input type="hidden" name="id" value="{{ isset($news->id) ? $news->id : '' }}">
                            <input type="submit" value="Сохранить" name="submit" class="btn btn-success">

                        </form>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </section>

@endsection