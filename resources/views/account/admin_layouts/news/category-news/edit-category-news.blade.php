@extends('account.admin_layouts.index')
@section('content')


    <section id="EditServices">
        <div class="container">
            <div class="row">
                <div class="wrapper-edit-services style-page">
                    <div class="col-lg-12">
                        <h2>Добавление категории новостей</h2>
                        <hr>
                        @include('auth.success_errors_block')
                        <form @if($category) action="{{ route('edit-category-news', $category->id) }}" @else action="{{ route('edit-category-news') }}"  @endif method="post" class="style-form-edit-service">
                            <div class="field">
                                <label for="">Название категории</label>
                                <input type="text" name="name" class="name_text" value="@if($category){{ $category->name }}@endif">
                            </div>
                            <div class="field">
                                <label for="">Slug категории</label>
                                <input type="text" name="slug" class="name_text_translit" value="@if($category){{ $category->slug }}@endif">
                            </div>

                            <div class="field">
                                <label for="">Ключевики</label>
                                <textarea name="keywords" >@if($category){{ $category->keywords }}@endif</textarea>
                            </div>

                            <div class="field">
                                <label for="">Мета описание</label>
                                <textarea  name="description">@if($category){{ $category->description }}@endif</textarea>
                            </div>


                            <div class="field">
                                <label for="">Позиция</label>
                                <input type="text" name="position" value="@if($category){{ $category->position }}@else 1 @endif">
                            </div>
                            <div class="field">
                                <label for="">Активность</label>
                                <input type="checkbox" name="active" @if($category && $category->active) checked @endif> Да
                            </div>
                            {{ csrf_field() }}
                            <input type="submit" value="Сохранить" name="submit" class="btn btn-success">
                        </form>
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </section>


@endsection