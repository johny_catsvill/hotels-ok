@extends('account.admin_layouts.index')
@section('content')

    <section id="listServices">
        <div class="container">
            <div class="row">
                <div class="wrapper-list-services style-page">
                    <div class="col-lg-12">
                        <h2>Список категорий новостей</h2>
                        <hr>
                        <a href="{{ route('edit-category-news') }}" class="btn btn-primary btn-add-services-style">Добавить категорию</a>
                        @include('auth.success_errors_block')
                        <div class="list-services-block">
                            <table>
                                <tr>
                                    <th>Идентификатор</th>
                                    <th>Имя</th>
                                    <th>Активность</th>
                                    <th>Позиция</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                                @foreach($categories as $category)
                                    <tr>
                                        <td>{{ $category->id }}</td>
                                        <td>{{ $category->name }}</td>
                                        @if($category->active)
                                        <td>Да</td>
                                        @else
                                            <td>Нет</td>
                                        @endif
                                        <td>{{ $category->position }}</td>
                                        <td><a href="{{ route('edit-category-news', $category->id) }}" class="btn btn-primary">Редактировать</a></td>
                                        <td><a href="{{ route('delete-category-news', $category->id) }}" class="btn btn-danger">Удалить</a></td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </section>


@endsection