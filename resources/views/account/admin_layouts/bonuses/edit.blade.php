@extends('account.admin_layouts.index')
@section('content')


    <section id="EditServices">
        <div class="container">
            <div class="row">
                <div class="wrapper-edit-services style-page">
                    <div class="col-lg-12">
                        <h2>Изменение бонуса</h2>
                        <hr>
                        @include('auth.success_errors_block')
                        <form action="{{ route('edit-discount') }}" method="post" class="style-form-edit-service">
                            <div class="field">
                                <label for="">Название</label>
                                <input type="text" name="name" value="{{ $bonus ? $bonus->name : '' }}">
                            </div>

                            <div class="field">
                                <label for="">Процент</label>
                                <input type="text" name="procent" value="{{ $bonus ? $bonus->procent : '' }}">
                            </div>
                            <input type="hidden" name="discountId" value="{{ $bonus ? $bonus->id : '' }}">


                            {{ csrf_field() }}
                            <input type="submit" value="Сохранить" name="submit" class="btn btn-success">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection