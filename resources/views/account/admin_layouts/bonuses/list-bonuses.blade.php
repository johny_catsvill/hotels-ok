@extends('account.admin_layouts.index')
@section('content')

    <section id="listServices">
        <div class="container">
            <div class="row">
                <div class="wrapper-list-services style-page">
                    <div class="col-lg-12">
                        <h2>Список бонусов</h2>
                        <hr>
                        <a href="{{ route('edit-discount') }}" class="btn btn-primary btn-add-services-style">Добавить бонус</a>
                        @include('auth.success_errors_block')
                        <div class="list-services-block">
                            @if($listBonuses)
                                <table>
                                    <tr>
                                        <th>Идентификатор</th>
                                        <th>Название</th>
                                        <th>Процент</th>
                                        <th>Изменить</th>
                                        <th>Удалить</th>
                                    </tr>

                                    @foreach($listBonuses as $bonus)
                                        <tr>
                                            <td>{{ $bonus->id }}</td>
                                            <td>{{ $bonus->name }}</td>
                                            <td>{{ $bonus->procent }}%</td>
                                            <td><a href="{{ route('edit-discount', $bonus->id) }}" class="btn btn-primary">Изменить статус</a></td>
                                            <td><a href="{{ route('delete-discount', ['id' => $bonus->id]) }}" class="btn btn-danger">Удалить</a></td>
                                        </tr>
                                    @endforeach

                                </table>
                            @else
                                <div class="alert alert-primary">
                                    Отсутствуют бонусы
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </section>


@endsection