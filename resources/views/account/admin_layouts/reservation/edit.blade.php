@extends('account.admin_layouts.index')
@section('content')

<section id="reservation">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 style-page">
                <h2>Создание бронирования</h2>
                <hr>
                
                <form action="{{ route('reservation-edit') }}" method="post" class="style-form-edit-service">
                    <div class="wrapper-standart-info">
                        <div class="field">
                        <label for="">Период</label>
                        <div class="wrapper-input-period">
                            <input type="text" name="dateFrom"
                                   @if($reservation) value="{{ date('d.m.Y', $reservation->startReservationDate) }}" @endif
                                   @if($dateFrom) value="{{ $dateFrom }}"  @endif
                                   id="datetimepicker1" onchange="MainFunction.recountPriceForRooms();"> - <input @if($dateTo) value="{{ $dateTo }}"  @endif @if($reservation) value="{{ date('d.m.Y', $reservation->endReservationDate) }}" @endif name="dateTo" type="text" id="datetimepicker2" onchange="MainFunction.recountPriceForRooms();">
                        </div>
                        </div>
                        <div class="field wrapper-list-objects-ajax">
                            <label for="">Название Отеля

                            </label>
                            <input type="text" name="objectName" @if($object)value="{{ $object->name }}" @endif @if($reservation) value="{{ $reservation->objectName }}" @endif
                            @if(!$reservation) onfocus="MainFunction.getObjectsName(event, this)" onkeyup="MainFunction.getObjectsName(event, this);" @else readonly @endif  autocomplete="off">
                            <input type="hidden" name="objectId" @if($reservation)value="{{ $reservation->objectId }}" @endif>
                            <div class="list-objects-ajax">
                                <ul>
                                    
                                </ul>
                            </div>           
                        </div>
                        
                        <div class="field wrapper-telefon-field">
                            <label for="">Телефон</label>
                            <input type="text" name="telefon" id="phoneCallback" autocomplete="off" onkeyup="MainFunction.getLoadClientByTelefon(event, this);" @if($reservation) value="{{ $reservation->visitorPhone }}"  @endif>
                            <input type="hidden" name="userId" @if($reservation)value="{{ $reservation->userId }}" @endif>
                            <div class="user-ajax">
                               <i class="fa fa-close style-for-close-popup-user" onclick="MainFunction.closePopupUser();" ></i>
                                <ul>
                                    
                                </ul>
                            </div>
                        </div>
                        
                        <div class="field">
                            <label for="">Почтовый адрес</label>
                            <input type="text" name="email" @if($reservation) value="{{ $reservation->visitorEmail }}" @endif>
                        </div>

                        <div class="field">
                            <label for="">Имя</label>
                            <input type="text" name="name" @if($reservation) value="{{ $reservation->name }}" @endif>
                        </div>

                    </div>
                    <hr>
                    
                    <div class="list-rooms">
                        @if($reservation && $roomsReservation)
                           @foreach($roomsReservation as $room)
                            <div class="display-block wrapper-room-field-js">
                               <div class="field peopleLive">
                                   <label for="">На кого</label>
                                   <input type="text" name="peopleFio" value="{{ $room->peopleFio }}">
                               </div>
                               <div class="field js-amountPerson">
                                   <label for="">Количество гостей</label>
                                   <div class="wrapper-person-style">
                                       <input type="text" onkeyup="MainFunction.checkedCountPerson(this);" value="{{ $room->roomAmountPerson }}">
                                       <div class="info-count-person-room"></div>
                                   </div>

                                   <input type="hidden" name="maxAmountPerson" value="{{ $room->amountPerson }}">
                               </div>
                                <div class="field">
                                    <label for="">Тип комнаты</label>
                                    <select name="roomId" class="js-type-room" onchange="MainFunction.changePriceForRoom(this);" >
                                        <option value="">Выберите комнату</option>
                                       @foreach($reservation->listRooms as $currentRoom)
                                           @if($currentRoom->id == $room->roomId)
                                               <option value="{{ $room->roomId }}" selected>{{ $currentRoom->name }}</option>
                                            @else
                                                <option value="{{ $currentRoom->id }}">{{ $currentRoom->name }}</option>
                                           @endif
                                        @endforeach
                                    </select>
                                    <input type="hidden" name="hiddenPrice" value="{{ $room->price }}">
                                    <input type="hidden" name="selectedRoomId" value="{{ $room->id }}">
                                </div>
                                <div class="field">
                                    <label for="">Тип предоплаты</label>
                                    <select name="prepayment" id="prepaymentSelect" onchange="MainFunction.reloadPrepaymentReservation();">
                                        <option value="1" @if($room->prepaymentId == 1) selected @endif >Без предоплаты</option>
                                        <option value="2" @if($room->prepaymentId == 2) selected @endif>За сутки</option>
                                        <option value="3" @if($room->prepaymentId == 3) selected @endif>Полная предоплата</option>
                                    </select>
                                </div>
                                <div class="field price-for-discount">
                                    <label for="">Цена со скидкой</label>
                                    <input type="text" name="" disabled value="{{ $room->price }}">
                                </div>
                                <div class="field price">
                                    <label for="">Цена без скидки</label>
                                    <input type="text" name="" disabled value="{{ $room->priceWithoutDiscount }}">
                                </div>

                                <div class="field all-price">
                                    <label for="">Общая цена за номер</label> <span class="val-all-price">{{ $room->price }}</span>

                                </div>
                                <h4>Примечание</h4>
                                <hr>
                                <textarea name="info" class="text-room" id="" cols="30" rows="10">{{ $room->info }}</textarea>
                                <input type="hidden" name="priceForDay" value="{{ $room->priceForDay }}">
                                <a href="javascript:void(0);" onclick="MainFunction.removeThatRoom(this);" class="btn btn-danger btn-remove-room-style">Удалить комнату</a>   
                                <hr>                                                                                    
                            </div>
                            @endforeach
                        @endif
                    </div>
                    
                    <a href="javascript:void(0);" class="btn btn-primary" onclick="MainFunction.getListRoomsByObjectId();">Добавить комнату</a>
                    
                    <div class="wrapper-room-field">
                       <div class="field peopleLive">
                           <label for="">На кого</label>
                           <input type="text" name="">
                       </div>
                       <div class="field js-amountPerson">
                           <label for="">Количество гостей</label>
                           <div class="wrapper-person-style">
                               <input type="text" onkeyup="MainFunction.checkedCountPerson(this);">
                               <div class="info-count-person-room"></div>
                           </div>
                           
                           <input type="hidden" name="maxAmountPerson">
                       </div>
                        <div class="field">
                            <label for="">Тип комнаты</label>
                            <select name="" id="roomId" onchange="MainFunction.changePriceForRoom(this);" >
                               
                                <option value="">Выберите комнату</option>
                            </select>
                            <input type="hidden" name="hiddenPrice">
                            <input type="hidden" name="selectedRoomId">
                        </div>
                        <div class="field">
                            <label for="">Тип предоплаты</label>
                            <select name="" id="prepaymentSelect" onchange="MainFunction.reloadPrepaymentReservation();">
                                <option value="1">Без предоплаты</option>
                                <option value="2">За сутки</option>
                                <option value="3">Полная предоплата</option>
                            </select>
                        </div>
                        <div class="field price-for-discount">
                            <label for="">Цена со скидкой</label>
                            <input type="text" name="" disabled>
                        </div>
                        <div class="field price">
                            <label for="">Цена без скидки</label>
                            <input type="text" name="" disabled>
                        </div>
                        
                        <div class="field all-price">
                            <label for="">Общая цена за номер</label> <span class="val-all-price">-</span>
                            
                        </div>
                        <h4>Примечание</h4>
                        <hr>
                        <textarea name="" class="text-room" id="" cols="30" rows="10"></textarea>
                        <input type="hidden" name="priceForDay">
                        <a href="javascript:void(0);" onclick="MainFunction.removeThatRoom(this);" class="btn btn-danger btn-remove-room-style">Удалить комнату</a>   
                        <hr>                                                                                    
                    </div>
                    <div class="field full-price-reservation">
                        <label for="">Общая стоимость</label> 
                        <span class="val-full-price-reservation">@if($reservation){{ $reservation->price }} @else 0 @endif</span>
                    </div>
                    <div class="field prepayment-style">
                        <label for="">Предоплата</label>
                        <span class="prepayment">@if($reservation){{ $reservation->prePayment }} @else 0 @endif</span>
                    </div>
                    <input type="hidden" name="reservationId" @if($reservation) value="{{ $reservation->id }}" @endif>
                    {{ csrf_field() }}
                    <input type="submit" name="submit" value="Сохранить" class="btn-save-room" onclick="MainFunction.validateInfoReservation(event, this);">
                    <a href="javascript:void(0);" class="btn-save-room btn-delete-reservation" onclick="MainFunction.deleteReservation(true);">Отменить бронирование</a>
                </form>
                
            </div>
        </div>
    </div>
</section>

<div class="message-reservation">
    <div class="alert alert-success">
        Бронь успешно помещена в черновики.
    </div>
</div>
<div class="message-reservation-error">
    <div class="alert alert-danger">
        <strong>Сообщение:</strong> Произошла ошибка при обновлении брони. Попробуй еще раз!
    </div>
</div>
<div class="message-reservation-error-count-people">
    <div class="alert alert-danger">
        <strong>Сообщение:</strong> На выбранную дату у данного обьекта нету свободных комнат!
    </div>
</div>
<script type="text/javascript">
  window.onbeforeunload = function() {
    var reservationId = document.getElementsByName('reservationId');
      if(reservationId[0].value) {
        return "Что-нибудь сообщить пользователю";
      }
    
  }
  
</script>
@endsection