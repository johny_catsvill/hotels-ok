@extends('account.admin_layouts.index')
@section('content')

<section id="reservation">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 style-page">
                <h2>Бронирование</h2>
                <hr>
                @include('auth.success_errors_block')
                <div>
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Брони</a></li>
                        <li><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Прошедшие брони</a></li>
                        <li><a href="#successReservation" aria-controls="successReservation" role="tab" data-toggle="tab">Подтвержденные брони</a></li>
                    </ul>
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="home">
                            <a href="{{ route('reservation-edit') }}" class="btn btn-primary btn-add-services-style">Создать бронь</a>
                            @if(count($reservations) > 0)
                               <div class="wrapper-table-reservation">
                                    <table>
                                        <tr>
                                            <th style="width:5%;">№</th>
                                            <th>Имя клиента</th>
                                            <th>Дата брони</th>
                                            <th>Телефон</th>
                                            <th>День заезда</th>
                                            <th>День отьезда</th>
                                            <th>Предоплата</th>
                                            <th>Общая цена</th>
                                            <th>Статус</th>
                                            <th>Менеджер</th>
                                            <th>Подробнее</th>
                                        </tr>
                                        @foreach($reservations as $reservation)
                                            <tr>
                                                <td>{{ $reservation->id }}</td>
                                                <td>{{ $reservation->name }}</td>
                                                <td>{{ $reservation->lastUpdater }}</td>
                                                <td>{{ $reservation->visitorPhone }}</td>
                                                <td>{{ date('d.m.Y', $reservation->startReservationDate) }}</td>
                                                <td>{{ date('d.m.Y', $reservation->endReservationDate) }}</td>
                                                <td>{{ $reservation->prePayment }} UAH</td>
                                                <td>{{ $reservation->price }} UAH</td>
                                                <td>{{ $reservation->statusName }}</td>
                                                <td>{{ $reservation->managerName }} {{ $reservation->managerLastName }} </td>
                                                <td><a href="{{ route('info-by-reservation', ['id' => $reservation->id]) }}">Подробнее</a></td>
                                            </tr>
                                        @endforeach
                                    </table>                       
                               </div>

                            @endif
                        </div>
                        <div role="tabpanel" class="tab-pane" id="profile">
                            <div class="list-objects-admin">
                                <table>
                                    <tr>
                                        <th>ID</th>
                                        <th>День заезда - день отьезда</th>
                                        <th>Имя пользователя</th>
                                        <th>Телефон</th>
                                        <th>Имя отеля</th>
                                        <th></th>
                                    </tr>
                                    @if(count($reservationsPast) > 0)
                                        @foreach($reservationsPast as $reservation)
                                            <tr>
                                                <td>{{ $reservation->id }}</td>
                                                <td>{{ date('d.m.Y', $reservation->startReservationDate) }} - {{ date('d.m.Y', $reservation->endReservationDate) }}</td>
                                                <td>{{ $reservation->name }}</td>
                                                <td>{{ $reservation->visitorPhone }}</td>
                                                <td>{{ $reservation->objectName }}</td>
                                                <td class="js-popup-reservation"><a href="javascript:void(0);" onclick="MainFunction.showChangeStatusReservation(this);" class="btn btn-primary">Статус посещения</a>
                                                    <div class="js-popup-past-reservation">
                                                        <ul>
                                                            <li><a href="javascript:void(0);" onclick="MainFunction.changeStatusReservation({{ $reservation->id }}, 1, this)">Проживал</a></li>
                                                            <li><a href="javascript:void(0);" onclick="MainFunction.changeStatusReservation({{ $reservation->id }}, 2, this)">Не проживал</a></li>
                                                        </ul>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </table>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="successReservation">
                            <div class="list-objects-admin">
                                <table>
                                    <tr>
                                        <th>ID</th>
                                        <th>День заезда - день отьезда</th>
                                        <th>Имя пользователя</th>
                                        <th>Телефон</th>
                                        <th>Имя отеля</th>
                                        <th></th>
                                    </tr>
                                    @if(count($reservationsActive) > 0)
                                        @foreach($reservationsActive as $reservation)
                                            <tr>
                                                <td>{{ $reservation->id }}</td>
                                                <td>{{ date('d.m.Y', $reservation->startReservationDate) }} - {{ date('d.m.Y', $reservation->endReservationDate) }}</td>
                                                <td>{{ $reservation->name }}</td>
                                                <td>{{ $reservation->visitorPhone }}</td>
                                                <td>{{ $reservation->objectName }}</td>
                                                <td class="js-popup-reservation">
                                                    <a href="{{ route('info-by-reservation', ['id' => $reservation->id]) }}">Подробнее</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</section>


@endsection