@extends('account.admin_layouts.index')
@section('content')

    <section id="listServices">
        <div class="container">
            <div class="row">
                <div class="wrapper-list-services style-page">
                    <div class="col-lg-12">
                        <h2>Список отзывов о проекте</h2>
                        <hr>
                        <a href="{{ route('add-review-project') }}" class="btn btn-primary btn-add-services-style">Добавить отзыв</a>
                        @include('auth.success_errors_block')
                        <div class="list-services-block">
                            <table>
                                <tr>
                                    <th>Идентификатор</th>
                                    <th>Дата обновления</th>
                                    <th>Имя</th>
                                    <th>Рейтинг</th>
                                    <th>Активность</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                                @foreach($reviews as $review)
                                    <tr>
                                        <td>{{ $review->id }}</td>
                                        <td>{{ $review->lastUpdater }}</td>
                                        <td>{{ $review->name }}</td>
                                        <td>{{ $review->raiting }}</td>
                                        <td>{{ $review->active }}</td>
                                        <td><a href="{{ route('add-review-project', $review->id) }}" class="btn btn-primary">Редактировать</a></td>
                                        <td><a href="{{ route('delete-review-project', $review->id) }}" class="btn btn-danger">Удалить</a></td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </section>


@endsection