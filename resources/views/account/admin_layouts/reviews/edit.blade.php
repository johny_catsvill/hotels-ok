@extends('account.admin_layouts.index')
@section('content')


    <section id="EditServices">
        <div class="container">
            <div class="row">
                <div class="wrapper-edit-services style-page">
                    <div class="col-lg-12">
                        <h2>Добавление отзыва проекта</h2>
                        <hr>
                        @include('auth.success_errors_block')
                        <form @if($review) action="{{ route('add-review-project', $review->id) }}" @else action="{{ route('add-review-project') }}"  @endif method="post" class="style-form-edit-service">
                            <div class="field">
                                <label for="">Имя</label>
                                <input type="text" name="name" value="@if($review){{ $review->name }}@endif">
                            </div>
                            <div class="field">
                                <label for="">Рейтинг(от 5 до 10)</label>
                                <input type="text" name="raiting" value="@if($review){{ $review->raiting}}@endif">
                            </div>

                            <div class="field">
                                <label for="">Мини текст</label>
                                <textarea name="text" >@if($review){{ $review->text }}@endif</textarea>
                            </div>

                            <div class="field">
                                <label for="">Активность</label>
                                <input type="checkbox" name="active" @if($review && $review->active) checked @endif> Да
                            </div>
                            {{ csrf_field() }}
                            <input type="hidden" name="reviewId" value="@if($review) {{ $review->id }} @endif">
                            <input type="submit" value="Сохранить" name="submit" class="btn btn-success">
                        </form>
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </section>


@endsection