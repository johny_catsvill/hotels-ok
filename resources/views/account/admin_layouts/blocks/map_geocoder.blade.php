<script>
        
    var timer = false;
    function initMapTimer(e) {
        if(e.keyCode < 32 && e.keyCode != 8) return null;
        clearTimeout(timer);
        timer = setTimeout(function(){
            initialize();
        }, 1000);       
    }
    
        function initialize () {
			var geocoder;
			var map;
			//var address = $('#address').val();
			var address = document.getElementById('address').value;
            var myOptions = {
                zoom: 16,
                center: latlng,
                mapTypeControl: true,
                mapTypeControlOptions: {
                    style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
                },
                navigationControl: true,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
			geocoder = new google.maps.Geocoder();
            var latlng = new google.maps.LatLng(-34.397, 150.644);
            if (geocoder) {
                geocoder.geocode({
                    'address': address
                }, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (status != google.maps.GeocoderStatus.ZERO_RESULTS) {
                            map.setCenter(results[0].geometry.location);
 
                            var infowindow = new google.maps.InfoWindow({
                                content: '<b>' + address + '</b>',
                                size: new google.maps.Size(150, 50)
                            });
 
                            var marker = new google.maps.Marker({
                                position: results[0].geometry.location,
                                map: map,
                                title: address
                            });
							$('input[name="latitudeGps"]').val(results[0].geometry.location.lat());
							$('input[name="longitudeGps"]').val(results[0].geometry.location.lng());
                            $('.wrapper-longitude').text(results[0].geometry.location.lng());
                            $('.wrapper-latitude').text(results[0].geometry.location.lat());
                            google.maps.event.addListener(marker, 'click', function() {
                                infowindow.open(map, marker);
                            });
 
                        } else {
                            console.log("No results found");
                        }
                    } else {
                        console.log("Geocode was not successful for the following reason: " + status);
                    }
                });
            }
google.maps.event.addDomListener(window, 'load', initialize);
        }
        
		//initialize();

 
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAphlVuZWUONV67Lckv28MXTaGTU0gbxDg&callback=initialize"></script>