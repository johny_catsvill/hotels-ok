@extends('account.admin_layouts.index')
@section('content')

<section id="home">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 style-page">
                <h2 class="caption-page">Главная</h2>
                <hr>
                <ul class="btns-chart-line">
                    <li><a href="javascript:void(0);">По дням</a></li>
                    <li><a href="javascript:void(0);">По месяцям</a></li>
                </ul>
                <select class="month-select-style" onchange="" name="" id="">
                    <option value="">Выбрать месяц</option>
                    <option @if($currentMonth == '01') selected @endif value="01">Январь</option>
                    <option @if($currentMonth == '02') selected @endif value="02">Февраль</option>
                    <option @if($currentMonth == '03') selected @endif  value="03">Март</option>
                    <option @if($currentMonth == '04') selected @endif value="04">Апрель</option>
                    <option @if($currentMonth == '05') selected @endif value="05">Май</option>
                    <option @if($currentMonth == '06') selected @endif value="06">Июнь</option>
                    <option @if($currentMonth == '07') selected @endif value="07">Июль</option>
                    <option @if($currentMonth == '08') selected @endif value="08">Август</option>
                    <option @if($currentMonth == '09') selected @endif value="09">Сентябрь</option>
                    <option @if($currentMonth == '10') selected @endif value="10">Октябрь</option>
                    <option @if($currentMonth == '11') selected @endif value="11">Ноябрь</option>
                    <option @if($currentMonth == '12') selected @endif value="12">Декабрь</option>
                </select>
                <canvas id="myChartReservationLine"></canvas>
                <div class="wrap-chart-reservation">
                    <canvas id="myChartReservation"></canvas>
                </div>
                <div class="wrap-chart-reservation">

                </div>

                <script>
                    var ctx = document.getElementById("myChartReservation").getContext('2d');
                    var line = document.getElementById("myChartReservationLine").getContext('2d');
                    var myChart = new Chart(ctx, {
                        type: 'bar',
                        data: {
                            labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
                            datasets: [{
                                label: 'Бронирования',
                                data: [12, 19, 3, 5, 2, 3],
                                backgroundColor: [
                                    'rgba(255, 99, 132, 0.2)',
                                    'rgba(54, 162, 235, 0.2)',
                                    'rgba(255, 206, 86, 0.2)',
                                    'rgba(75, 192, 192, 0.2)',
                                    'rgba(153, 102, 255, 0.2)',
                                    'rgba(255, 159, 64, 0.2)'
                                ],
                                borderColor: [
                                    'rgba(255,99,132,1)',
                                    'rgba(54, 162, 235, 1)',
                                    'rgba(255, 206, 86, 1)',
                                    'rgba(75, 192, 192, 1)',
                                    'rgba(153, 102, 255, 1)',
                                    'rgba(255, 159, 64, 1)'
                                ],
                                borderWidth: 1
                            }]
                        },
                        options: {
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        beginAtZero:true
                                    }
                                }]
                            }
                        }
                    });


                    new Chart(document.getElementById("myChartReservationLine"),
                        {"type":"line",
                            "data":{"labels":
                                [
                                    @for($i = 1; $i <= $countDay; $i++)
                                        "{{ $i }}",
                                    @endfor
                                ],
                        "datasets":[
                            {"label": "Количество броней на {{ $monthName }}",
                            "data":[
                                @foreach($arrayReservation as $reservation)
                                    {{ $reservation }},
                                @endforeach
                            ],
                            "fill":false,
                            "borderColor":"rgb(75, 192, 192)",
                            "lineTension":0.1}
                            ,
                            {{--{"label": "Количество броней на {{ $monthName }}",--}}
                                {{--"data":[75,64,14,256,51,5,18],--}}
                                {{--"fill":false,--}}
                                {{--"borderColor":"rgb(0, 0, 0)",--}}
                                {{--"lineTension":0.1}--}}
                        ]},

                            "options":{
                                scales: {
                                    xAxes: [{
                                        display: true,
                                        scaleLabel: {
                                            display: true,
                                            labelString: 'Month'
                                        }
                                    }],
                                    yAxes: [{
                                        display: true,
                                        ticks: {
                                            beginAtZero: true,
                                            steps: 5,
                                            stepValue: 10,
                                            max: 30
                                        }
                                    }]
                                }
                            }});


                </script>
            </div>
        </div>
    </div>
</section>

@endsection