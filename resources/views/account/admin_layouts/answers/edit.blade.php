@extends('account.admin_layouts.index')
@section('content')


    <section id="EditServices">
        <div class="container">
            <div class="row">
                <div class="wrapper-edit-services style-page">
                    <div class="col-lg-12">
                        <h2>Добавление ответа</h2>
                        <hr>
                        @include('auth.success_errors_block')
                        <form action="{{ route('edit-answer') }}" method="post" class="style-form-edit-service">
                            <div class="field">
                                <label for="">Правдивость ответа</label>
                                <input type="radio" name="isTrue" value="1"> Да
                                <input type="radio" name="isTrue" value="0"> Нет
                            </div>

                            <div class="field">
                                <label for="">Ответ</label>
                                <input type="text" name="answer" value="{{ $answer ? $answer->answer : '' }}">
                            </div>
                            <div class="field">
                                <label for="">Процент</label>
                                <input type="text" name="procent" value="{{ $answer ? $answer->procent : '' }}">
                            </div>

                            <div class="field">
                                <label for="">Правдивый текст</label>
                                <textarea name="good_text" id="" cols="30" rows="10">{{ $answer ? $answer->good_text : '' }}</textarea>
                            </div>

                            <div class="field">
                                <label for="">Неправильный текст</label>
                                <textarea name="bad_text" id="" cols="30" rows="10">{{ $answer ? $answer->bad_text : '' }}</textarea>
                            </div>


                            <div class="field">
                                <label for="">Выберите вопрос</label>
                                <select name="question_id" id="">
                                    @foreach($listQuestions as $question)
                                        <option
                                                @if($answer && $answer->question_id == $question->id)
                                                checked
                                                @endif
                                                value="{{ $question->id }}">{{ $question->name }}</option>
                                    @endforeach
                                </select>
                            </div>



                            <input type="hidden" name="answer_id"  value="{{ $answer ? $answer->id : '' }}">
                            {{ csrf_field() }}
                            <input type="submit" value="Сохранить" name="submit" class="btn btn-success">
                        </form>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </section>


@endsection