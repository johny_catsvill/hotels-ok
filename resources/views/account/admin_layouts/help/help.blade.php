@extends('account.admin_layouts.index')
@section('content')

<section id="home">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 style-page">
                <h2 class="caption-page">Список тикетов</h2>
                <hr>
                <div class="list-tickets-admin">
                    <table>
                        <tr>
                            <th>ID</th>
                            <th>Название</th>
                            <th>Имя обьекта</th>
                            <th>Последнее сообщение</th>
                        </tr>
                        @foreach($tickets as $ticket)
                            <td>{{ $ticket->id }}</td>
                            <td><a href="{{ route('ticket-admin', $ticket->id) }}">{{ $ticket->themeTicket }}</a></td>
                            <td>{{ $ticket->objectName }}</td>
                            <td>{{ date('d.m.Y H:i', $ticket->dateLastMessages) }}</td>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection