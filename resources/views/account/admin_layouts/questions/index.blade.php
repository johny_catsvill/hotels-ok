@extends('account.admin_layouts.index')
@section('content')

    <section id="listServices">
        <div class="container">
            <div class="row">
                <div class="wrapper-list-services style-page">
                    <div class="col-lg-12">
                        <h2>Список Вопросов</h2>
                        <hr>
                        <a href="{{ route('edit-question') }}" class="btn btn-primary btn-add-services-style">Добавить Вопрос</a>
                        @include('auth.success_errors_block')
                        <div class="list-services-block">
                            <table>
                                <tr>
                                    <th>Идентификатор</th>
                                    <th>Вопрос</th>
                                    <th>Количество ответов</th>
                                    <th>Редактировать</th>
                                    <th>Удалить</th>
                                </tr>
                                @foreach($listQuestions as $question)
                                    <tr>
                                        <td>{{ $question->id }}</td>
                                        <td>{{ $question->name }}</td>
                                        <td>{{ $question->answers_count }}</td>
                                        <td><a href="{{ route('edit-question', ['id' => $question->id]) }}" class="btn btn-primary">Редактировать</a></td>
                                        <td><a href="{{ route('delete-question', ['id' => $question->id]) }}" class="btn btn-danger">Удалить</a></td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </section>


@endsection