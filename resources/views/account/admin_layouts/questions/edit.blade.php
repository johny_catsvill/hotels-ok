@extends('account.admin_layouts.index')
@section('content')


    <section id="EditServices">
        <div class="container">
            <div class="row">
                <div class="wrapper-edit-services style-page">

                    <div class="col-lg-12">
                        <h2>Добавление Вопроса</h2>
                        <a href="{{ route('questions') }}" class="btn btn-primary">Вернуться к списку вопросов</a>
                        <hr>
                        @include('auth.success_errors_block')
                        <form action="{{ route('edit-question') }}" method="post" class="style-form-edit-service" enctype="multipart/form-data">
                            <div class="field">
                                <label for="">Текст вопроса</label>
                                <input type="text" name="name" value="{{ $question ? $question->name : '' }}">
                            </div>

                            <div class="field">
                                <label for="">Изображение вопроса</label>
                                <input type="file" name="image">
                            </div>
                            
                            @if($question)
                                <img style="margin-bottom: 20px;" src="/images/test/{{ $question->image }}" alt="">
                            @endif    


                            <input type="hidden" name="question_id"  value="{{ $question ? $question->id : '' }}">
                            {{ csrf_field() }}
                            <input type="submit" value="Сохранить" name="submit" class="btn btn-success">
                        </form>
                        <hr>


                            <h2>Список Ответов по этому вопросу</h2>
                            <hr>
                            @if($question)
                                <a href="{{ route('edit-answer') }}" class="btn btn-primary btn-add-services-style">Добавить ответ</a>
                            @endif
                            <div class="list-services-block">
                                <table>
                                    <tr>
                                        <th>Идентификатор</th>
                                        <th>Ответ</th>
                                        <th>Процент</th>
                                        <th>Наличие правильного текста</th>
                                        <th>Наличие неправильного текста</th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                    @if($answers)
                                        @foreach($answers as $answer)
                                            <tr>
                                                <td>{{ $answer->id }}</td>
                                                <td>{{ $answer->answer }}</td>
                                                <td>{{ $answer->procent }}</td>
                                                <td>{{ $answer->good_text ? 'Да' : 'Нет' }}</td>
                                                <td>{{ $answer->bad_text ? 'Да' : 'Нет' }}</td>
                                                <td><a href="{{ route('edit-answer', ['id' => $answer->id]) }}" class="btn btn-primary">Редактировать</a></td>
                                                <td><a href="{{ route('delete-answer', ['id' => $answer->id]) }}" class="btn btn-danger">Удалить</a></td>
                                            </tr>
                                        @endforeach
                                    @endif

                                </table>
                            </div>

                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </section>


@endsection