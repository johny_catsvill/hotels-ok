@extends('account.admin_layouts.index')
@section('content')

<section id="applicationObject">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 style-page">
                <h2>Заявки обьектам</h2>
                <hr>
                @include('auth.success_errors_block')
                <form action="{{ route('save-application-object') }}" class="application-form" method="post">

                    <div class="field">
                        <label for="">Имя</label>
                        <input type="text" name="name" value="{{ old('name') }}">
                    </div>

                    <div class="field">
                        <label for="">Название обьекта</label>
                        <input type="text" name="object_name" value="{{ old('object_name') }}">
                    </div>

                    <div class="field">
                        <label for="">Категория обьекта</label>
                        <select name="typeObject" id="">
                            @foreach($categoriesObjects as $categoryObject)
                                <option @if(old('typeObject') == $categoryObject->id) selected @endif value="{{ $categoryObject->id }}">{{ $categoryObject->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="field">
                        <label for="">Город</label>
                        <select name="city" id="">
                            @foreach($cities as $city)
                                <option @if(old('$city') == $city->id) selected @endif value="{{ $city->id }}">{{ $city->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="field">
                        <label for="">Email</label>
                        <input type="text" name="email" value="{{ old('email') }}">
                    </div>
                    <div class="field">
                        <label for="">Телефон</label>
                        <input type="text" name="phone" id="applicationPhone" value="{{ old('phone') }}">
                    </div>
                    {{ csrf_field() }}
                    <input type="submit" name="submitSave" class="btn btn-primary" value="Сохранить заявку">
                    <input type="submit" name="submitCreate" class="btn btn-primary" value="Создать обьект">
                </form>
                <hr>
                @if(count($listApplicationObject) > 0)
                    <table class="list-application-object">
                        <tr>
                            <th>ID</th>
                            <th>Как зовут</th>
                            <th>Название обьекта</th>
                            <th>Дата создания</th>
                            <th>Email</th>
                            <th>Телефон</th>
                            <th>Город</th>
                            <th></th>
                        </tr>
                        @foreach($listApplicationObject as $application)
                            <tr>
                                <td>{{ $application->id }}</td>
                                <td>{{ $application->name }}</td>
                                <td>{{ $application->object_name }}</td>
                                <td>{{ date('d.m.Y H.i', $application->updateDate) }}</td>
                                <td>{{ $application->email }}</td>
                                <td>{{ $application->phone }}</td>
                                <td>{{ $application->city }}</td>
                                @if(!$application->is_send)
                                    <td><a href="javascript:void(0);" class="btn btn-primary">Отправить впервые</a></td>
                                @else
                                    <td><a href="javascript:void(0);" class="btn btn-primary">Отправить еще раз</a></td>
                                @endif
                            </tr>
                        @endforeach
                    </table>
                @else
                    <div class="alert alert-danger">Заявок нету</div>
                @endif
                
            </div>
        </div>
    </div>
</section>

@endsection