@extends('account.admin_layouts.index')
@section('content')


    <section id="EditServices">
        <div class="container">
            <div class="row">
                <div class="wrapper-edit-services style-page">
                    <div class="col-lg-12">
                        <h2>Изменение статуса</h2>
                        <hr>
                        @include('auth.success_errors_block')
                        <form action="{{ route('edit-callback', $callback->id) }}" method="post" class="style-form-edit-service">
                            <div class="field">
                                <label for="">Телефон</label>
                                <input type="text" name="telefone" readonly value="{{ $callback->telefon }}">
                            </div>

                            <div class="field">
                                <label for="">Статус</label>
                                <select name="status" id="">
                                    <option @if($callback->status == 'noAnswered') selected @endif value="noAnswered">Без ответа</option>
                                    <option @if($callback->status == 'answered') selected @endif value="answered">Отвечено</option>
                                    <option @if($callback->status == 'callbackLater') selected @endif value="callbackLater">Перезвонить позже</option>
                                </select>
                            </div>

                            <div class="field">
                                <label for="">Дата</label>
                                <input type="text" readonly value="{{ date('d.m.Y H:i', $callback->creationDate) }}">
                            </div>

                            {{ csrf_field() }}
                            <input type="submit" value="Сохранить" name="submit" class="btn btn-success">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection