@extends('account.admin_layouts.index')
@section('content')

<section id="listServices">
    <div class="container">
        <div class="row">
            <div class="wrapper-list-services style-page">
                <div class="col-lg-12">
                    <h2>Список звонков</h2>
                    <hr>
                    
                    @include('auth.success_errors_block')
                    <div class="list-services-block">
                       @if($listCallbacks)
                        <table>
                            <tr>
                                <th>Идентификатор</th>
                                <th>Телефон</th>
                                <th>Статус</th>
                                <th>Изменить статус</th>
                                <th>Удалить</th>
                            </tr>
                            
                            @foreach($listCallbacks as $callback)
                                <tr>
                                    <td>{{ $callback->id }}</td>
                                    <td>{{ $callback->telefon }}</td>
                                    <td>{{ $callback->status }}</td>
                                    <td><a href="{{ route('edit-callback', $callback->id) }}" class="btn btn-primary">Изменить статус</a></td>
                                    <td><a href="{{ route('delete-callback', ['id' => $callback->id]) }}" class="btn btn-danger">Удалить</a></td>
                                </tr>
                            @endforeach
                            
                        </table>
                        @else
                            <div class="alert alert-primary">
                                Отсутствуют звонки
                            </div>
                        @endif
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </div>
</section>


@endsection