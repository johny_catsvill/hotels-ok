@extends('account.admin_layouts.index')
@section('content')

<section id="home">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 style-page">
                <h2 class="caption-page">Список обьектов</h2>
                <hr>
                @include('auth.success_errors_block')
                <div class="tab-content">
                    <div id="list" class="tab-pane fade in active">
                        <div class="list-objects-admin">
                            <table>
                                <tr>
                                    <th>ID</th>
                                    <th>Имя обьекта</th>
                                    <th>Дата создания</th>
                                    <th>Статус</th>
                                    <th>Дата проверки</th>
                                    <th>Имя организации</th>
                                    <th></th>
                                </tr>
                                @foreach($listObjects as $object)
                                    <tr>
                                        <td>{{ $object->id }}</td>
                                        <td>{{ $object->name }}</td>
                                        <td>{{ $object->lastUpdater }}</td>
                                        <td>
                                            @if($object->confirmedAccount)
                                                <i class="fa fa-check-circle-o fa-status-object" title="Подтвержден" style="color:green;" aria-hidden="true"></i>
                                            @elseif(is_null($object->confirmedAccount))
                                                <i class="fa fa-ban fa-status-object" style="color:red;" title="Нет заявки на проверку" aria-hidden="true"></i>
                                            @else
                                                <i class="fa fa-hourglass-start" style="color:orange;" title="В ожидании" aria-hidden="true"></i>
                                            @endif
                                        </td>
                                        <td>
                                            {{ date('d.m.Y H:i', $object->creationDate) }}
                                        </td>
                                        <td>{{ $object->organizationName }}</td>
                                        <td><a href="{{ route('checked-object-id', $object->id) }}" class="btn btn-primary">Проверить</a></td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

@endsection