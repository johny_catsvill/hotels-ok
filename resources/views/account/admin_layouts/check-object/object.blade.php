@extends('account.admin_layouts.index')
@section('content')

<section id="" onload="initialize();">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 style-page">
                <h2 class="caption-page">Обьект: {{ $object->name }}</h2>
                <hr>
                <a href="{{ route('confirmed-object-admin', $id ) }}" class="btn btn-success" style="margin-bottom:20px;">Подтвердить</a>
                <a href="javascript:void(0);" class="btn btn-danger" onclick="MainFunction.saveErrorsForObject();" style="margin-bottom:20px;">Сообщить о проблеме</a>
                <input type="hidden" name="objectId" value="{{ $id }}">
                <div class="alert alert-danger wrapper-for-errors">
                    @if($errors)
                        @foreach($errors as $error)
                            <span data-id="{{ $error->errorId }}">{{ $error->message }}</span>
                        @endforeach
                    @endif

                </div>
                <div>
                  <!-- Навигация -->
                  <ul class="nav nav-tabs" role="tablist">
                    <li class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Информация об обьекте</a></li>
                    <li><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Общие сведения</a></li>
                    <li><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Комнаты</a></li>
<!--                    <li><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Календарь</a></li>-->
                  </ul>
                  <!-- Содержимое вкладок -->
                  <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="home">
                       <table>
                           <tr>
                               <td style="width:15%;"><span data-number-problem="1" data-name-field="image">Изображение обьекта</span></td>
                               <td style="width:70%;"><img class="object-img" src="/images/hotels/{{ $id }}-{{ $object->subdomain }}/{{ \App\Traits\croppedPhotos::changeImg($object->image) }}" alt=""></td>
                               <td style="width:15%;">
                                   <a href="javascript:void(0);" onclick="MainFunction.successStatus(this);" class="btn btn-success btn-select-admin"><i class="fa fa-check"></i> Yes</a>
                                   <a href="javascript:void(0);" onclick="MainFunction.showPopupmessageForError(this);" class="btn btn-danger btn-select-admin">
                                       <i class="fa fa-ban" aria-hidden="true"></i> No</a>
                                   <a href="javascript:void(0);" class="btn btn-warning btn-reload-status display-none" onclick="MainFunction.changeStatusError(this);">Изменить статус</a>
                                   <span class="status-select">Статус: <zz></zz></span>
                               </td>
                           </tr>
                           <tr>
                               <td><span data-number-problem="2" data-name-field="">Галерея изображений</span></td>
                               <td>
                                 @foreach($galleryImages as $image)
                                  <div class="wrapper-for-img" style="background-image:url(/{{ $image }});"></div>
                                  @endforeach
                               </td>
                               <td>
                                   <a href="javascript:void(0);" onclick="MainFunction.successStatus(this);" class="btn btn-success btn-select-admin"><i class="fa fa-check"></i> Yes</a>
                                   <a href="javascript:void(0);" onclick="MainFunction.showPopupmessageForError(this);" class="btn btn-danger btn-select-admin"><i class="fa fa-ban" aria-hidden="true"></i> No</a>
                                   <a href="javascript:void(0);" class="btn btn-warning btn-reload-status display-none" onclick="MainFunction.changeStatusError(this);">Изменить статус</a>
                                   <span class="status-select">Статус: <zz></zz></span>
                               </td>
                           </tr>
                           <tr>
                               <td><span data-number-problem="3" data-name-field="">Количество звёзд</span></td>
                               <td>
                                   <input type="text" disabled value="{{ $object->stars }} звезды">
                               </td>
                               <td>
                                   <a href="javascript:void(0);" onclick="MainFunction.successStatus(this);" class="btn btn-success btn-select-admin"><i class="fa fa-check"></i> Yes</a>
                                   <a href="javascript:void(0);" onclick="MainFunction.showPopupmessageForError(this);" class="btn btn-danger btn-select-admin"><i class="fa fa-ban" aria-hidden="true"></i> No</a>
                                   <a href="javascript:void(0);" class="btn btn-warning btn-reload-status display-none" onclick="MainFunction.changeStatusError(this);">Изменить статус</a>
                                   <span class="status-select">Статус: <zz></zz></span>
                               </td>
                           </tr>
                           <tr>
                               <td><span data-number-problem="4">Регистрация заезда</span></td>
                               <td><input type="text" disabled value="{{ $object->checkIn }}"></td>
                               <td>
                                   <a href="javascript:void(0);" onclick="MainFunction.successStatus(this);" class="btn btn-success btn-select-admin"><i class="fa fa-check"></i> Yes</a>
                                   <a href="javascript:void(0);" onclick="MainFunction.showPopupmessageForError(this);" class="btn btn-danger btn-select-admin"><i class="fa fa-ban" aria-hidden="true"></i> No</a>
                                   <a href="javascript:void(0);" class="btn btn-warning btn-reload-status display-none" onclick="MainFunction.changeStatusError(this);">Изменить статус</a>
                                   <span class="status-select">Статус: <zz></zz></span>
                               </td>
                           </tr>
                           <tr>
                               <td><span data-number-problem="5">Регистрация отьезда</span></td>
                               <td><input type="text" disabled value="{{ $object->checkOut }}"></td>
                               <td>
                                   <a href="javascript:void(0);" onclick="MainFunction.successStatus(this);" class="btn btn-success btn-select-admin"><i class="fa fa-check"></i> Yes</a>
                                   <a href="javascript:void(0);" onclick="MainFunction.showPopupmessageForError(this);" class="btn btn-danger btn-select-admin"><i class="fa fa-ban" aria-hidden="true"></i> No</a>
                                   <a href="javascript:void(0);" class="btn btn-warning btn-reload-status display-none" onclick="MainFunction.changeStatusError(this);">Изменить статус</a>
                                   <span class="status-select">Статус: <zz></zz></span>
                               </td>
                           </tr>
                           <tr>
                               <td><span data-number-problem="6">Туристический сбор</span></td>
                               <td><input type="text" disabled value="0"></td>
                               <td>
                                  <a href="javascript:void(0);" onclick="MainFunction.successStatus(this);" class="btn btn-success btn-select-admin"><i class="fa fa-check"></i> Yes</a>
                                   <a href="javascript:void(0);" onclick="MainFunction.showPopupmessageForError(this);" class="btn btn-danger btn-select-admin"><i class="fa fa-ban" aria-hidden="true"></i> No</a>
                                   <a href="javascript:void(0);" class="btn btn-warning btn-reload-status display-none" onclick="MainFunction.changeStatusError(this);">Изменить статус</a>
                                   <span class="status-select">Статус: <zz></zz></span>
                               </td>
                           </tr>
                           <tr>
                               <td><span data-number-problem="7">Услуги в отеле</span></td>
                               <td>
                                   <div class="list-service-object">
                                       <ul>
                                            @if(count($currentObject->objectPluses) > 0)
                                               @foreach($currentObject->objectPluses as $pluse)
                                                   <li><img src="/images/icon-pluses/{{ $pluse->image }}" alt=""> <span>{{ $pluse->name }}</span></li>
                                               @endforeach
                                            @else
                                                <li>Плюсы отсутствуют</li>
                                            @endif
                                       </ul>
                                   </div>
                               </td>
                               <td>
                                   <a href="javascript:void(0);" onclick="MainFunction.successStatus(this);" class="btn btn-success btn-select-admin"><i class="fa fa-check"></i> Yes</a>
                                   <a href="javascript:void(0);" onclick="MainFunction.showPopupmessageForError(this);" class="btn btn-danger btn-select-admin"><i class="fa fa-ban" aria-hidden="true"></i> No</a>
                                   <a href="javascript:void(0);" class="btn btn-warning btn-reload-status display-none" onclick="MainFunction.changeStatusError(this);">Изменить статус</a>
                                   <span class="status-select">Статус: <zz></zz></span>
                               </td>
                           </tr>
                           <tr>
                               <td><span data-number-problem="8">Описание</span></td>
                               <td>
                                   <textarea name="" id="" cols="30" rows="10">{{ $object->infoAboutObject }}</textarea>
                               </td>
                               <td>
                                   <a href="javascript:void(0);" onclick="MainFunction.successStatus(this);" class="btn btn-success btn-select-admin"><i class="fa fa-check"></i> Yes</a>
                                   <a href="javascript:void(0);" onclick="MainFunction.showPopupmessageForError(this);" class="btn btn-danger btn-select-admin"><i class="fa fa-ban" aria-hidden="true"></i> No</a>
                                   <a href="javascript:void(0);" class="btn btn-warning btn-reload-status display-none" onclick="MainFunction.changeStatusError(this);">Изменить статус</a>
                                   <span class="status-select">Статус: <zz></zz></span>
                               </td>
                           </tr>
                           <tr>
                               <td><span data-number-problem="9">Ближайщие обьекты</span></td>
                               <td>
                                   <div class="list-about-object">
                                       <ul>
                                          @if(count($listBesideObject) > 0)
                                              @foreach($listBesideObject as $besideObject)
                                                  <li>{{ $besideObject->besideTypeName }} {{ $besideObject->name }} {{ $besideObject->distance }}</li>
                                              @endforeach
                                          @else
                                              <li>Нету ближайщих обьектов</li>
                                          @endif
                                   </div>
                               </td>
                               <td>
                                   <a href="javascript:void(0);" onclick="MainFunction.successStatus(this);" class="btn btn-success btn-select-admin"><i class="fa fa-check"></i> Yes</a>
                                   <a href="javascript:void(0);" onclick="MainFunction.showPopupmessageForError(this);" class="btn btn-danger btn-select-admin"><i class="fa fa-ban" aria-hidden="true"></i> No</a>
                                   <a href="javascript:void(0);" class="btn btn-warning btn-reload-status display-none" onclick="MainFunction.changeStatusError(this);">Изменить статус</a>
                                   <span class="status-select">Статус: <zz></zz></span>
                               </td>
                           </tr>
                           <tr>
                               <td><span data-number-problem="10">Месторасположение</span></td>
                               <td>
                                   <textarea name="" id="" cols="30" rows="10">{{ $object->locationObject }}</textarea>
                               </td>
                               <td>
                                   <a href="javascript:void(0);" onclick="MainFunction.successStatus(this);" class="btn btn-success btn-select-admin"><i class="fa fa-check"></i> Yes</a>
                                   <a href="javascript:void(0);" onclick="MainFunction.showPopupmessageForError(this);" class="btn btn-danger btn-select-admin"><i class="fa fa-ban" aria-hidden="true"></i> No</a>
                                   <a href="javascript:void(0);" class="btn btn-warning btn-reload-status display-none" onclick="MainFunction.changeStatusError(this);">Изменить статус</a>
                                   <span class="status-select">Статус: <zz></zz></span>
                               </td>
                           </tr>
                           <tr>
                               <td><span data-number-problem="11">Информация о проезде</span></td>
                               <td><textarea name=""  id="" cols="30" rows="10">{{ $object->infoAboutTravel }}</textarea></td>
                               <td>
                                   <a href="javascript:void(0);" onclick="MainFunction.successStatus(this);" class="btn btn-success btn-select-admin"><i class="fa fa-check"></i> Yes</a>
                                   <a href="javascript:void(0);" onclick="MainFunction.showPopupmessageForError(this);" class="btn btn-danger btn-select-admin"><i class="fa fa-ban" aria-hidden="true"></i> No</a>
                                   <a href="javascript:void(0);" class="btn btn-warning btn-reload-status display-none" onclick="MainFunction.changeStatusError(this);">Изменить статус</a>
                                   <span class="status-select">Статус: <zz></zz></span>
                               </td>
                           </tr>
                           <tr>
                               <td data-number-problem="12"><span>Размещение детей и предоставление дополнительных мест</span></td>
                               <td><textarea name=""  id="" cols="30" rows="10">{{ $object->infoExtraPlaces }}</textarea></td>
                               <td>
                                   <a href="javascript:void(0);" onclick="MainFunction.successStatus(this);" class="btn btn-success btn-select-admin"><i class="fa fa-check"></i> Yes</a>
                                   <a href="javascript:void(0);" onclick="MainFunction.showPopupmessageForError(this);" class="btn btn-danger btn-select-admin"><i class="fa fa-ban" aria-hidden="true"></i> No</a>
                                   <a href="javascript:void(0);" class="btn btn-warning btn-reload-status display-none" onclick="MainFunction.changeStatusError(this);">Изменить статус</a>
                                   <span class="status-select">Статус: <zz></zz></span>
                               </td>
                           </tr>
                           <tr>
                               <td><span data-number-problem="13">НДС</span></td>
                               <td><textarea name=""  id="" cols="30" rows="10">{{ $object->infoAdditionalPayments }}</textarea></td>
                               <td>
                                   <a href="javascript:void(0);" onclick="MainFunction.successStatus(this);" class="btn btn-success btn-select-admin"><i class="fa fa-check"></i> Yes</a>
                                   <a href="javascript:void(0);" onclick="MainFunction.showPopupmessageForError(this);" class="btn btn-danger btn-select-admin"><i class="fa fa-ban" aria-hidden="true"></i> No</a>
                                   <a href="javascript:void(0);" class="btn btn-warning btn-reload-status display-none" onclick="MainFunction.changeStatusError(this);">Изменить статус</a>
                                   <span class="status-select">Статус: <zz></zz></span>
                               </td>
                           </tr>
                           <tr>
                               <td><span data-number-problem="14">Рестораны и бары</span></td>
                               <td><textarea name=""  id="" cols="30" rows="10">{{ $object->restaurantAndBarsInfo }}</textarea></td>
                               <td>
                                   <a href="javascript:void(0);" onclick="MainFunction.successStatus(this);" class="btn btn-success btn-select-admin"><i class="fa fa-check"></i> Yes</a>
                                   <a href="javascript:void(0);" onclick="MainFunction.showPopupmessageForError(this);" class="btn btn-danger btn-select-admin"><i class="fa fa-ban" aria-hidden="true"></i> No</a>
                                   <a href="javascript:void(0);" class="btn btn-warning btn-reload-status display-none" onclick="MainFunction.changeStatusError(this);">Изменить статус</a>
                                   <span class="status-select">Статус: <zz></zz></span>
                               </td>
                           </tr>
                           <tr>
                               <td><span data-number-problem="15">Лечение</span></td>
                               <td><textarea name=""  id="" cols="30" rows="10">{{ $object->infoMedication }}</textarea></td>
                               <td>
                                   <a href="javascript:void(0);" onclick="MainFunction.successStatus(this);" class="btn btn-success btn-select-admin"><i class="fa fa-check"></i> Yes</a>
                                   <a href="javascript:void(0);" onclick="MainFunction.showPopupmessageForError(this);" class="btn btn-danger btn-select-admin"><i class="fa fa-ban" aria-hidden="true"></i> No</a>
                                   <a href="javascript:void(0);" class="btn btn-warning btn-reload-status display-none" onclick="MainFunction.changeStatusError(this);">Изменить статус</a>
                                   <span class="status-select">Статус: <zz></zz></span>
                               </td>
                           </tr>
                           <tr>
                               <td><span data-number-problem="16">Координаты</span></td>
                               <td>
                                   <input type="text" name="address"  id="address" value="{{ $object->street }} {{ $object->numberHouse }} город {{ $object->cityName }}">
                                    <div style=" width: 90%; height: 400px;" id="map_canvas"></div>
                                    <span>Координаты: lon: {{ $object->longitudeGps }}, lat:{{ $object->latitudeGps }}</span>
                               </td>
                               <td>
                                   <a href="javascript:void(0);" onclick="MainFunction.successStatus(this);" class="btn btn-success btn-select-admin"><i class="fa fa-check"></i> Yes</a>
                                   <a href="javascript:void(0);" onclick="MainFunction.showPopupmessageForError(this);" class="btn btn-danger btn-select-admin"><i class="fa fa-ban" aria-hidden="true"></i> No</a>
                                   <a href="javascript:void(0);" class="btn btn-warning btn-reload-status display-none" onclick="MainFunction.changeStatusError(this);">Изменить статус</a>
                                   <span class="status-select">Статус: <zz></zz></span>
                               </td>
                           </tr>
                       </table>
                        <input type="hidden" name="countErrors" value="0">
                        <hr>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="profile">
                        <table>
                            <tr>
                                <td style="width:15%;"><span data-number-problem="17">Телефон:</span></td>
                                <td style="width:70%;">
                                    <input type="text" disabled value="{{ $object->phone }}">
                                </td>
                                <td style="width:15%;">
                                    <a href="javascript:void(0);" onclick="MainFunction.successStatus(this);" class="btn btn-success btn-select-admin"><i class="fa fa-check"></i> Yes</a>
                                   <a href="javascript:void(0);" onclick="MainFunction.showPopupmessageForError(this);" class="btn btn-danger btn-select-admin"><i class="fa fa-ban" aria-hidden="true"></i> No</a>
                                   <a href="javascript:void(0);" class="btn btn-warning btn-reload-status display-none" onclick="MainFunction.changeStatusError(this);">Изменить статус</a>
                                   <span class="status-select">Статус: <zz></zz></span>
                                </td>
                            </tr>
                            <tr>
                                <td><span data-number-problem="18">Email:</span></td>
                                <td>
                                    <input type="text" disabled value="{{ $object->email }}">
                                </td>
                                <td>
                                    <a href="javascript:void(0);" onclick="MainFunction.successStatus(this);" class="btn btn-success btn-select-admin"><i class="fa fa-check"></i> Yes</a>
                                   <a href="javascript:void(0);" onclick="MainFunction.showPopupmessageForError(this);" class="btn btn-danger btn-select-admin"><i class="fa fa-ban" aria-hidden="true"></i> No</a>
                                   <a href="javascript:void(0);" class="btn btn-warning btn-reload-status display-none" onclick="MainFunction.changeStatusError(this);">Изменить статус</a>
                                   <span class="status-select">Статус: <zz></zz></span>
                                </td>
                            </tr>
                            <tr>
                                <td><span data-number-problem="19">Название обьекта:</span></td>
                                <td>
                                    <input type="text" disabled value="{{ $object->name }}">
                                </td>
                                <td>
                                    <a href="javascript:void(0);" onclick="MainFunction.successStatus(this);" class="btn btn-success btn-select-admin"><i class="fa fa-check"></i> Yes</a>
                                   <a href="javascript:void(0);" onclick="MainFunction.showPopupmessageForError(this);" class="btn btn-danger btn-select-admin"><i class="fa fa-ban" aria-hidden="true"></i> No</a>
                                   <a href="javascript:void(0);" class="btn btn-warning btn-reload-status display-none" onclick="MainFunction.changeStatusError(this);">Изменить статус</a>
                                   <span class="status-select">Статус: <zz></zz></span>
                                </td>
                            </tr>
                            <tr>
                                <td><span data-number-problem="20">URL:</span></td>
                                <td>
                                    <input type="text" disabled value="{{ $object->url }}">
                                </td>
                                <td>
                                    <a href="javascript:void(0);" onclick="MainFunction.successStatus(this);" class="btn btn-success btn-select-admin"><i class="fa fa-check"></i> Yes</a>
                                   <a href="javascript:void(0);" onclick="MainFunction.showPopupmessageForError(this);" class="btn btn-danger btn-select-admin"><i class="fa fa-ban" aria-hidden="true"></i> No</a>
                                   <a href="javascript:void(0);" class="btn btn-warning btn-reload-status display-none" onclick="MainFunction.changeStatusError(this);">Изменить статус</a>
                                </td>
                            </tr>
                            <tr>
                                <td><span data-number-problem="21">Тип обьекта:</span></td>
                                <td>
                                    <input type="text" disabled value="{{ $object->categoryName }}">
                                </td>
                                <td>
                                    <a href="javascript:void(0);" onclick="MainFunction.successStatus(this);" class="btn btn-success btn-select-admin"><i class="fa fa-check"></i> Yes</a>
                                   <a href="javascript:void(0);" onclick="MainFunction.showPopupmessageForError(this);" class="btn btn-danger btn-select-admin"><i class="fa fa-ban" aria-hidden="true"></i> No</a>
                                   <a href="javascript:void(0);" class="btn btn-warning btn-reload-status display-none" onclick="MainFunction.changeStatusError(this);">Изменить статус</a>
                                </td>
                            </tr>
                            <tr>
                                <td><span data-number-problem="22">Страна:</span></td>
                                <td>
                                    <input type="text" disabled value="{{ $object->countryName }}">
                                </td>
                                <td>
                                    <a href="javascript:void(0);" onclick="MainFunction.successStatus(this);" class="btn btn-success btn-select-admin"><i class="fa fa-check"></i> Yes</a>
                                   <a href="javascript:void(0);" onclick="MainFunction.showPopupmessageForError(this);" class="btn btn-danger btn-select-admin"><i class="fa fa-ban" aria-hidden="true"></i> No</a>
                                   <a href="javascript:void(0);" class="btn btn-warning btn-reload-status display-none" onclick="MainFunction.changeStatusError(this);">Изменить статус</a>
                                </td>
                            </tr>
                            <tr>
                                <td><span data-number-problem="23">Город:</span></td>
                                <td>
                                    <input type="text" disabled value="{{ $object->cityName }}">
                                </td>
                                <td>
                                    <a href="javascript:void(0);" onclick="MainFunction.successStatus(this);" class="btn btn-success btn-select-admin"><i class="fa fa-check"></i> Yes</a>
                                   <a href="javascript:void(0);" onclick="MainFunction.showPopupmessageForError(this);" class="btn btn-danger btn-select-admin"><i class="fa fa-ban" aria-hidden="true"></i> No</a>
                                   <a href="javascript:void(0);" class="btn btn-warning btn-reload-status display-none" onclick="MainFunction.changeStatusError(this);">Изменить статус</a>
                                </td>
                            </tr>
                            <tr>
                                <td><span data-number-problem="24">Индекс:</span></td>
                                <td>
                                    <input type="text" disabled value="{{ $object->indexMail }}">
                                </td>
                                <td>
                                    <a href="javascript:void(0);" onclick="MainFunction.successStatus(this);" class="btn btn-success btn-select-admin"><i class="fa fa-check"></i> Yes</a>
                                   <a href="javascript:void(0);" onclick="MainFunction.showPopupmessageForError(this);" class="btn btn-danger btn-select-admin"><i class="fa fa-ban" aria-hidden="true"></i> No</a>
                                   <a href="javascript:void(0);" class="btn btn-warning btn-reload-status display-none" onclick="MainFunction.changeStatusError(this);">Изменить статус</a>
                                </td>
                            </tr>
                            <tr>
                                <td><span data-number-problem="25">Улица:</span></td>
                                <td>
                                    <input type="text" disabled value="{{ $object->street }}">
                                </td>
                                <td>
                                    <a href="javascript:void(0);" onclick="MainFunction.successStatus(this);" class="btn btn-success btn-select-admin"><i class="fa fa-check"></i> Yes</a>
                                   <a href="javascript:void(0);" onclick="MainFunction.showPopupmessageForError(this);" class="btn btn-danger btn-select-admin"><i class="fa fa-ban" aria-hidden="true"></i> No</a>
                                   <a href="javascript:void(0);" class="btn btn-warning btn-reload-status display-none" onclick="MainFunction.changeStatusError(this);">Изменить статус</a>
                                </td>
                            </tr>
                            <tr>
                                <td><span data-number-problem="26">№ дома:</span></td>
                                <td>
                                    <input type="text" disabled value="{{ $object->numberHouse }}">
                                </td>
                                <td>
                                    <a href="javascript:void(0);" onclick="MainFunction.successStatus(this);" class="btn btn-success btn-select-admin"><i class="fa fa-check"></i> Yes</a>
                                   <a href="javascript:void(0);" onclick="MainFunction.showPopupmessageForError(this);" class="btn btn-danger btn-select-admin"><i class="fa fa-ban" aria-hidden="true"></i> No</a>
                                   <a href="javascript:void(0);" class="btn btn-warning btn-reload-status display-none" onclick="MainFunction.changeStatusError(this);">Изменить статус</a>
                                </td>
                            </tr>
                            <tr>
                                <td><span data-number-problem="27">№ квартиры:</span></td>
                                <td>
                                    <input type="text" disabled value="{{ $object->numberApartment }}">
                                </td>
                                <td>
                                    <a href="javascript:void(0);" onclick="MainFunction.successStatus(this);" class="btn btn-success btn-select-admin"><i class="fa fa-check"></i> Yes</a>
                                   <a href="javascript:void(0);" onclick="MainFunction.showPopupmessageForError(this);" class="btn btn-danger btn-select-admin"><i class="fa fa-ban" aria-hidden="true"></i> No</a>
                                   <a href="javascript:void(0);" class="btn btn-warning btn-reload-status display-none" onclick="MainFunction.changeStatusError(this);">Изменить статус</a>
                                </td>
                            </tr>
                            <tr>
                                <td><span data-number-problem="28">Факс:</span></td>
                                <td>
                                    <input type="text" disabled value="{{ $object->fax }}">
                                </td>
                                <td>
                                    <a href="javascript:void(0);" onclick="MainFunction.successStatus(this);" class="btn btn-success btn-select-admin"><i class="fa fa-check"></i> Yes</a>
                                   <a href="javascript:void(0);" onclick="MainFunction.showPopupmessageForError(this);" class="btn btn-danger btn-select-admin"><i class="fa fa-ban" aria-hidden="true"></i> No</a>
                                   <a href="javascript:void(0);" class="btn btn-warning btn-reload-status display-none" onclick="MainFunction.changeStatusError(this);">Изменить статус</a>
                                </td>
                            </tr>
                            <tr>
                                <td><span data-number-problem="29">Контактное лицо:</span></td>
                                <td>
                                    <input type="text" disabled value="{{ $object->contactName }}">
                                </td>
                                <td>
                                    <a href="javascript:void(0);" onclick="MainFunction.successStatus(this);" class="btn btn-success btn-select-admin"><i class="fa fa-check"></i> Yes</a>
                                   <a href="javascript:void(0);" onclick="MainFunction.showPopupmessageForError(this);" class="btn btn-danger btn-select-admin"><i class="fa fa-ban" aria-hidden="true"></i> No</a>
                                   <a href="javascript:void(0);" class="btn btn-warning btn-reload-status display-none" onclick="MainFunction.changeStatusError(this);">Изменить статус</a>
                                </td>
                            </tr>
                            
                        </table>
                        <hr>
                        <h2>Отдел бронирования</h2>
                        <hr>
                        <table>
                            <tr>
                                <td style="width:15%;"><span data-number-problem="30">Ф.И.О:</span></td>
                                <td style="width:70%;">
                                    <input type="text" disabled value="{{ $object->reservationPeopleFio }}">
                                </td>
                                <td style="width:15%;">
                                    <a href="javascript:void(0);" onclick="MainFunction.successStatus(this);" class="btn btn-success btn-select-admin"><i class="fa fa-check"></i> Yes</a>
                                   <a href="javascript:void(0);" onclick="MainFunction.showPopupmessageForError(this);" class="btn btn-danger btn-select-admin"><i class="fa fa-ban" aria-hidden="true"></i> No</a>
                                   <a href="javascript:void(0);" class="btn btn-warning btn-reload-status display-none" onclick="MainFunction.changeStatusError(this);">Изменить статус</a>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:15%;"><span data-number-problem="31">Должность:</span></td>
                                <td style="width:70%;">
                                    <input type="text" disabled value="{{ $object->reservationPeoplePosition }}">
                                </td>
                                <td style="width:15%;">
                                    <a href="javascript:void(0);" onclick="MainFunction.successStatus(this);" class="btn btn-success btn-select-admin"><i class="fa fa-check"></i> Yes</a>
                                   <a href="javascript:void(0);" onclick="MainFunction.showPopupmessageForError(this);" class="btn btn-danger btn-select-admin"><i class="fa fa-ban" aria-hidden="true"></i> No</a>
                                   <a href="javascript:void(0);" class="btn btn-warning btn-reload-status display-none" onclick="MainFunction.changeStatusError(this);">Изменить статус</a>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:15%;"><span data-number-problem="32">Телефон:</span></td>
                                <td style="width:70%;">
                                    <input type="text" disabled value="{{ $object->reservationPhone }}">
                                </td>
                                <td style="width:15%;">
                                    <a href="javascript:void(0);" onclick="MainFunction.successStatus(this);" class="btn btn-success btn-select-admin"><i class="fa fa-check"></i> Yes</a>
                                   <a href="javascript:void(0);" onclick="MainFunction.showPopupmessageForError(this);" class="btn btn-danger btn-select-admin"><i class="fa fa-ban" aria-hidden="true"></i> No</a>
                                   <a href="javascript:void(0);" class="btn btn-warning btn-reload-status display-none" onclick="MainFunction.changeStatusError(this);">Изменить статус</a>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:15%;"><span data-number-problem="33">Телефон для смс:</span></td>
                                <td style="width:70%;">
                                    <input type="text" disabled value="{{ $object->reservationPhoneSms }}">
                                </td>
                                <td style="width:15%;">
                                    <a href="javascript:void(0);" onclick="MainFunction.successStatus(this);" class="btn btn-success btn-select-admin"><i class="fa fa-check"></i> Yes</a>
                                   <a href="javascript:void(0);" onclick="MainFunction.showPopupmessageForError(this);" class="btn btn-danger btn-select-admin"><i class="fa fa-ban" aria-hidden="true"></i> No</a>
                                   <a href="javascript:void(0);" class="btn btn-warning btn-reload-status display-none" onclick="MainFunction.changeStatusError(this);">Изменить статус</a>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:15%;"><span data-number-problem="34">Факс:</span></td>
                                <td style="width:70%;">
                                    <input type="text" disabled value="{{ $object->reservationFax }}">
                                </td>
                                <td style="width:15%;">
                                    <a href="javascript:void(0);" onclick="MainFunction.successStatus(this);" class="btn btn-success btn-select-admin"><i class="fa fa-check"></i> Yes</a>
                                   <a href="javascript:void(0);" onclick="MainFunction.showPopupmessageForError(this);" class="btn btn-danger btn-select-admin"><i class="fa fa-ban" aria-hidden="true"></i> No</a>
                                   <a href="javascript:void(0);" class="btn btn-warning btn-reload-status display-none" onclick="MainFunction.changeStatusError(this);">Изменить статус</a>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:15%;"><span data-number-problem="35">Email:</span></td>
                                <td style="width:70%;">
                                    <input type="text" disabled value="{{ $object->reservationEmail }}">
                                </td>
                                <td style="width:15%;">
                                    <a href="javascript:void(0);" onclick="MainFunction.successStatus(this);" class="btn btn-success btn-select-admin"><i class="fa fa-check"></i> Yes</a>
                                   <a href="javascript:void(0);" onclick="MainFunction.showPopupmessageForError(this);" class="btn btn-danger btn-select-admin"><i class="fa fa-ban" aria-hidden="true"></i> No</a>
                                   <a href="javascript:void(0);" class="btn btn-warning btn-reload-status display-none" onclick="MainFunction.changeStatusError(this);">Изменить статус</a>
                                </td>
                            </tr>
                        </table>
                        <hr>
                        <h2>Бухгалтерия</h2>
                        <table>
                            <tr>
                                <td style="width:15%;"><span data-number-problem="36">Ответственный бухгалтер:</span></td>
                                <td style="width:70%;">
                                    <input type="text" disabled value="{{ $object->bookkeepingPeopleFio }}">
                                </td>
                                <td style="width:15%;">
                                    <a href="javascript:void(0);" onclick="MainFunction.successStatus(this);" class="btn btn-success btn-select-admin"><i class="fa fa-check"></i> Yes</a>
                                   <a href="javascript:void(0);" onclick="MainFunction.showPopupmessageForError(this);" class="btn btn-danger btn-select-admin"><i class="fa fa-ban" aria-hidden="true"></i> No</a>
                                   <a href="javascript:void(0);" class="btn btn-warning btn-reload-status display-none" onclick="MainFunction.changeStatusError(this);">Изменить статус</a>
                                </td>
                                
                            </tr>
<!--
                            <tr>
                                <td><span data-number-problem="37">Должность:</span></td>
                                <td>
                                    <input type="text" disabled value="">
                                </td>
                                <td>
                                    <a href="javascript:void(0);" onclick="MainFunction.successStatus(this);" class="btn btn-success btn-select-admin"><i class="fa fa-check"></i> Yes</a>
                                   <a href="javascript:void(0);" onclick="MainFunction.showPopupmessageForError(this);" class="btn btn-danger btn-select-admin"><i class="fa fa-ban" aria-hidden="true"></i> No</a>
                                   <a href="javascript:void(0);" class="btn btn-warning btn-reload-status display-none" onclick="MainFunction.changeStatusError(this);">Изменить статус</a>
                                </td>
                            </tr>
-->
                            <tr>
                                <td style="width:15%;"><span data-number-problem="37">Телефон:</span></td>
                                <td style="width:70%;"> 
                                    <input type="text" disabled value="{{ $object->bookkeepingPhone }}">
                                </td>
                                <td style="width:15%;">
                                    <a href="javascript:void(0);" onclick="MainFunction.successStatus(this);" class="btn btn-success btn-select-admin"><i class="fa fa-check"></i> Yes</a>
                                   <a href="javascript:void(0);" onclick="MainFunction.showPopupmessageForError(this);" class="btn btn-danger btn-select-admin"><i class="fa fa-ban" aria-hidden="true"></i> No</a>
                                   <a href="javascript:void(0);" class="btn btn-warning btn-reload-status display-none" onclick="MainFunction.changeStatusError(this);">Изменить статус</a>
                                </td>
                            </tr>
                            <tr>
                                <td><span data-number-problem="38">Факс:</span></td>
                                <td>
                                    <input type="text" disabled value="{{ $object->bookkeepingFax }}">
                                </td>
                                <td>
                                    <a href="javascript:void(0);" onclick="MainFunction.successStatus(this);" class="btn btn-success btn-select-admin"><i class="fa fa-check"></i> Yes</a>
                                   <a href="javascript:void(0);" onclick="MainFunction.showPopupmessageForError(this);" class="btn btn-danger btn-select-admin"><i class="fa fa-ban" aria-hidden="true"></i> No</a>
                                   <a href="javascript:void(0);" class="btn btn-warning btn-reload-status display-none" onclick="MainFunction.changeStatusError(this);">Изменить статус</a>
                                </td>
                            </tr>
                            <tr>
                                <td><span data-number-problem="39">Электронный адрес:</span></td>
                                <td>
                                    <input type="text" disabled value="{{ $object->bookkeepingEmail }}">
                                </td>
                                <td>
                                    <a href="javascript:void(0);" onclick="MainFunction.successStatus(this);" class="btn btn-success btn-select-admin"><i class="fa fa-check"></i> Yes</a>
                                   <a href="javascript:void(0);" onclick="MainFunction.showPopupmessageForError(this);" class="btn btn-danger btn-select-admin"><i class="fa fa-ban" aria-hidden="true"></i> No</a>
                                   <a href="javascript:void(0);" class="btn btn-warning btn-reload-status display-none" onclick="MainFunction.changeStatusError(this);">Изменить статус</a>
                                </td>
                            </tr>
                            
                        </table>
                        <hr>
                        <h2>Реквизиты для оплаты за проживания:</h2>
                        <table>
                            <tr>
                                <td style="width:15%;"><span data-number-problem="40">Юридическое название отеля:</span></td>
                                <td style="width:70%;">
                                    <input type="text" disabled value="{{ $object->legalName }}">
                                </td>
                                <td style="width:15%;">
                                    <a href="javascript:void(0);" onclick="MainFunction.successStatus(this);" class="btn btn-success btn-select-admin"><i class="fa fa-check"></i> Yes</a>
                                   <a href="javascript:void(0);" onclick="MainFunction.showPopupmessageForError(this);" class="btn btn-danger btn-select-admin"><i class="fa fa-ban" aria-hidden="true"></i> No</a>
                                   <a href="javascript:void(0);" class="btn btn-warning btn-reload-status display-none" onclick="MainFunction.changeStatusError(this);">Изменить статус</a>
                                </td>
                            </tr>
                            <tr>
                                <td><span data-number-problem="41">Форма собственности 1:</span></td>
                                <td>
                                    <input type="text" disabled value="{{ $object->typeOwn1 }}">
                                </td>
                                <td>
                                    <a href="javascript:void(0);" onclick="MainFunction.successStatus(this);" class="btn btn-success btn-select-admin"><i class="fa fa-check"></i> Yes</a>
                                   <a href="javascript:void(0);" onclick="MainFunction.showPopupmessageForError(this);" class="btn btn-danger btn-select-admin"><i class="fa fa-ban" aria-hidden="true"></i> No</a>
                                   <a href="javascript:void(0);" class="btn btn-warning btn-reload-status display-none" onclick="MainFunction.changeStatusError(this);">Изменить статус</a>
                                </td>
                            </tr>
                            <tr>
                                <td><span data-number-problem="42">Форма собственности 2:</span></td>
                                <td>
                                    <input type="text" disabled value="{{ $object->typeOwn2 }}">
                                </td>
                                <td>
                                    <a href="javascript:void(0);" onclick="MainFunction.successStatus(this);" class="btn btn-success btn-select-admin"><i class="fa fa-check"></i> Yes</a>
                                   <a href="javascript:void(0);" onclick="MainFunction.showPopupmessageForError(this);" class="btn btn-danger btn-select-admin"><i class="fa fa-ban" aria-hidden="true"></i> No</a>
                                   <a href="javascript:void(0);" class="btn btn-warning btn-reload-status display-none" onclick="MainFunction.changeStatusError(this);">Изменить статус</a>
                                </td>
                            </tr>
                            <tr>
                                <td><span data-number-problem="43">Расчетный счет-фактуры:</span></td>
                                <td>
                                    <input type="text" disabled value="{{ $object->checkingAccount }}">
                                </td>
                                <td>
                                    <a href="javascript:void(0);" onclick="MainFunction.successStatus(this);" class="btn btn-success btn-select-admin"><i class="fa fa-check"></i> Yes</a>
                                   <a href="javascript:void(0);" onclick="MainFunction.showPopupmessageForError(this);" class="btn btn-danger btn-select-admin"><i class="fa fa-ban" aria-hidden="true"></i> No</a>
                                   <a href="javascript:void(0);" class="btn btn-warning btn-reload-status display-none" onclick="MainFunction.changeStatusError(this);">Изменить статус</a>
                                </td>
                            </tr>
                            <tr>
                                <td><span data-number-problem="44">Дополнительный № счета:</span></td>
                                <td>
                                    <input type="text" disabled value="{{ $object->additionalAccount }}">
                                </td>
                                <td>
                                    <a href="javascript:void(0);" onclick="MainFunction.successStatus(this);" class="btn btn-success btn-select-admin"><i class="fa fa-check"></i> Yes</a>
                                   <a href="javascript:void(0);" onclick="MainFunction.showPopupmessageForError(this);" class="btn btn-danger btn-select-admin"><i class="fa fa-ban" aria-hidden="true"></i> No</a>
                                   <a href="javascript:void(0);" class="btn btn-warning btn-reload-status display-none" onclick="MainFunction.changeStatusError(this);">Изменить статус</a>
                                </td>
                            </tr>
                            <tr>
                                <td><span data-number-problem="45">Юридический адрес предприятия:</span></td>
                                <td>
                                    <input type="text" disabled value="{{ $object->legalAddress }}">
                                </td>
                                <td>
                                    <a href="javascript:void(0);" onclick="MainFunction.successStatus(this);" class="btn btn-success btn-select-admin"><i class="fa fa-check"></i> Yes</a>
                                   <a href="javascript:void(0);" onclick="MainFunction.showPopupmessageForError(this);" class="btn btn-danger btn-select-admin"><i class="fa fa-ban" aria-hidden="true"></i> No</a>
                                   <a href="javascript:void(0);" class="btn btn-warning btn-reload-status display-none" onclick="MainFunction.changeStatusError(this);">Изменить статус</a>
                                </td>
                            </tr>
                            <tr>
                                <td><span data-number-problem="46">ЕГРПОУ:</span></td>
                                <td>
                                    <input type="text" disabled value="{{ $object->egrpou }}">
                                </td>
                                <td>
                                    <a href="javascript:void(0);" onclick="MainFunction.successStatus(this);" class="btn btn-success btn-select-admin"><i class="fa fa-check"></i> Yes</a>
                                   <a href="javascript:void(0);" onclick="MainFunction.showPopupmessageForError(this);" class="btn btn-danger btn-select-admin"><i class="fa fa-ban" aria-hidden="true"></i> No</a>
                                   <a href="javascript:void(0);" class="btn btn-warning btn-reload-status display-none" onclick="MainFunction.changeStatusError(this);">Изменить статус</a>
                                </td>
                            </tr>
                            <tr>
                                <td><span data-number-problem="47">ИНН:</span></td>
                                <td>
                                    <input type="text" disabled value="{{ $object->identicationCodeLegal }}">
                                </td>
                                <td>
                                    <a href="javascript:void(0);" onclick="MainFunction.successStatus(this);" class="btn btn-success btn-select-admin"><i class="fa fa-check"></i> Yes</a>
                                   <a href="javascript:void(0);" onclick="MainFunction.showPopupmessageForError(this);" class="btn btn-danger btn-select-admin"><i class="fa fa-ban" aria-hidden="true"></i> No</a>
                                   <a href="javascript:void(0);" class="btn btn-warning btn-reload-status display-none" onclick="MainFunction.changeStatusError(this);">Изменить статус</a>
                                </td>
                            </tr>
                            <tr>
                                <td><span data-number-problem="48">НДС:</span></td>
                                <td>
                                    <input type="text" disabled value="{{ $object->numberNds }}">
                                </td>
                                <td>
                                    <a href="javascript:void(0);" onclick="MainFunction.successStatus(this);" class="btn btn-success btn-select-admin"><i class="fa fa-check"></i> Yes</a>
                                   <a href="javascript:void(0);" onclick="MainFunction.showPopupmessageForError(this);" class="btn btn-danger btn-select-admin"><i class="fa fa-ban" aria-hidden="true"></i> No</a>
                                   <a href="javascript:void(0);" class="btn btn-warning btn-reload-status display-none" onclick="MainFunction.changeStatusError(this);">Изменить статус</a>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="messages">test3</div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="wrapper-message-about-problem">
    <form action="">
        <textarea id="" cols="30" rows="10" name="messageError"></textarea>
        <input type="hidden" name="problemId">
        <input type="submit" value="Сохранить" class="btn btn-primary" onclick="MainFunction.saveError(event);">
            <i class="fa fa-times close-popup-problem" aria-hidden="true" onclick="MainFunction.closePopupMessageForError();"></i>
    </form>
</div>

@include('account.admin_layouts.blocks.map_geocoder')
    <script>
        document.addEventListener("DOMContentLoaded", initialize);

        window.onload = function() {
            MainFunction.startScriptErrors();
        };
    </script>

@endsection