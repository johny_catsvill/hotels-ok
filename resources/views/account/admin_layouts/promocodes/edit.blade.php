@extends('account.admin_layouts.index')
@section('content')


    <section id="EditServices">
        <div class="container">
            <div class="row">
                <div class="wrapper-edit-services style-page">
                    <div class="col-lg-12">
                        <h2>Изменение бонуса</h2>
                        <hr>
                        @include('auth.success_errors_block')
                        <form action="{{ route('edit-promocode') }}" method="post" class="style-form-edit-service">
                            <div class="field">
                                <label for="">Название</label>
                                <input type="text" name="name" value="{{ $promocode ? $promocode->name : '' }}">
                            </div>

                            <div class="field">
                                <label for="">Промокод</label>
                                <input type="text" name="code" value="{{ $promocode ? $promocode->code : '' }}">
                                <a href="javascript:void(0);" onclick="MainFunction.generateCode(this);" style="display: block;">Сгенерировать код</a>
                            </div>

                            <div class="field">
                                <label for="">Дата окончания публикации</label>
                                <input type="text" name="published_at" id="datetimepicker-news" value="{{ $promocode ? $promocode->published_at : '' }}">
                            </div>
                            <input type="hidden" name="codeId" value="{{ $promocode ? $promocode->id : '' }}">


                            {{ csrf_field() }}
                            <input type="submit" value="Сохранить" name="submit" class="btn btn-success">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection