@extends('account.admin_layouts.index')
@section('content')

    <section id="listServices">
        <div class="container">
            <div class="row">
                <div class="wrapper-list-services style-page">
                    <div class="col-lg-12">
                        <h2>Список промокодов</h2>
                        <hr>
                        <a href="{{ route('edit-promocode') }}" class="btn btn-primary btn-add-services-style">Добавить промокод</a>
                        @include('auth.success_errors_block')
                        <div class="list-services-block">
                            @if($listPromocodes)
                                <table>
                                    <tr>
                                        <th>Идентификатор</th>
                                        <th>Название</th>
                                        <th>Промокод</th>
                                        <th>До какого числа</th>
                                        <th>Изменить</th>
                                        <th>Удалить</th>
                                    </tr>

                                    @foreach($listPromocodes as $promocode)
                                        <tr>
                                            <td>{{ $promocode->id }}</td>
                                            <td>{{ $promocode->name }}</td>
                                            <td>{{ $promocode->code }}</td>
                                            <td>{{ $promocode->published_at }}</td>
                                            <td><a href="{{ route('edit-promocode', $promocode->id) }}" class="btn btn-primary">Изменить статус</a></td>
                                            <td><a href="{{ route('delete-promocode', ['id' => $promocode->id]) }}" class="btn btn-danger">Удалить</a></td>
                                        </tr>
                                    @endforeach

                                </table>
                            @else
                                <div class="alert alert-primary">
                                    Отсутствуют бонусы
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </section>


@endsection