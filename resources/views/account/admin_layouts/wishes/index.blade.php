@extends('account.admin_layouts.index')
@section('content')

    <section id="listServices">
        <div class="container">
            <div class="row">
                <div class="wrapper-list-services style-page">
                    <div class="col-lg-12">
                        <h2>Список пожеланий по сайту</h2>
                        <hr>
                        @include('auth.success_errors_block')
                        <div class="list-services-block">
                            <table>
                                <tr>
                                    <th>Идентификатор</th>
                                    <th>Дата создания</th>
                                    <th>Имя</th>
                                    <th>Email</th>
                                    <th>Отзыв</th>
                                    <th>Тип отзыва</th>
                                    <th>URL</th>
                                </tr>
                                @foreach($wishes as $wish)
                                    <tr>
                                        <td>{{ $wish->id }}</td>
                                        <td>{{ date('d.m.Y H:i', $wish->creationDate) }}</td>
                                        <td>{{ $wish->name }}</td>
                                        <td>{{ $wish->email }}</td>
                                        <td>
                                            <a href="javascript:void(0);" class="btn btn-primary" onclick="MainFunction.showWishes(this);">Смотреть текст</a>
                                            <p class="hide">{{ $wish->review }}</p>
                                        </td>
                                        <td>{{ $wish->type }}</td>
                                       <td>{{ $wish->url }}</td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </section>

    <div class="popup-wishes">
        <a href="javascript:void(0);" class="btn-close-popup" onclick="MainFunction.closePopup(this);">
            <img src="/images/close.png" alt="">
        </a>
        <p></p>
    </div>


@endsection