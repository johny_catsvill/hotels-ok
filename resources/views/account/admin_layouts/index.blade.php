<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{!! csrf_token() !!}" />
    <title>Панель управления</title>
    <link rel="stylesheet" href="{{ asset('css/reset.css') }}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap-theme.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/font-awesome.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style-admin.css') }}">
    <link rel="stylesheet" href="{{ asset('css/calendar.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="/css/bootstrap-datetimepicker.css">

    <script type="text/javascript" src="{{ asset('js/jquery-3.1.1.min.js') }}"></script>
    <script src="/js/moment-with-locales.min.js"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>
    <script src="{{ asset('js/script-admin.js') }}"></script>
    <script src="{{ asset('js/masonry.pkgd.min.js') }}"></script>
    <script src="{{ asset('js/jquery.liTranslit.js') }}"></script>
    <script src="{{ asset('js/calendar.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/bootstrap-select.min.js"></script>
    <script src="/js/validation.js"></script>
    <script src="{{ asset('js/jquery.maskedinput.min.js') }}"></script>
    <script src="{{ asset('js/ckeditor/ckeditor.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.bundle.min.js"></script>
<!--    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">-->
</head>
<body>
    

<div id="sidebar" data-power="1" onmouseenter="MainFunction.showRightSidebar(this);" onmouseleave="MainFunction.hideRightSidebar(this);">
        <div class="logo-sidebar">
            <i class="fa fa-bars"></i>
        </div>
 
        <div class="main-menu border-bottom-black">
           {{--<ul class="caption-list">--}}
               {{--<li><a href="javascript:void(0);">Меню <i class="fa fa-angle-down"></i></a></li>--}}
           {{--</ul>--}}
            <ul class="menu-hover"> 
               <li>
                    <a class="loop" href="{{ route('administrator-home') }}" class="active" data-description="Главная">
                        <i class="fa fa-home" aria-hidden="true" ></i> 
                        <span>Главная</span>
                    </a>
                </li>
                <li>
                    <a class="loop" href="{{ route('search-object') }}" class="active" data-description="Поиск обьектов">
                        <i class="fa fa-search" aria-hidden="true"></i>
                        <span>Поиск обьектов</span>
                    </a>
                </li>
                
                <li>
                    <a class="loop" href="{{ route('check-object') }}" class="active" data-description="Проверка обьектов">
                        <i class="fa fa-check-circle-o" aria-hidden="true"></i>
                        <span>Проверка обьектов</span>
                    </a>
                </li>
                
                <li>
                    <a class="loop" href="{{ route('reservation-admin') }}" class="active" data-description="Бронирования">
                        <i class="fa fa-address-book" aria-hidden="true"></i>
                        <span>Бронирования</span>
                    </a>
                </li>
                <li>
                    <a class="loop" href="{{ route('list-service-object') }}" data-description="Плюсы обьекта">
                       <i class="fa fa-chain-broken" aria-hidden="true"></i>         
                        <span>Плюсы обьекта</span>
                    </a>
                </li>
                <li>
                    <a class="loop" class="loop" href="{{ route('list-objects-beside') }}" data-description="Ближайщие обьекты">
                        <i class="fa fa-list-ul" aria-hidden="true"></i> 
                        <span>Ближайщие обьекты</span>
                    </a>
                </li>
                <li>
                    <a class="loop" href="{{ route('list-countries') }}" data-description="Страны">
                        <i class="fa fa-map-o" aria-hidden="true"></i>
                        <span>Страны</span>
                    </a>
                </li>
                <li>
                    <a class="loop" href="{{ route('list-regions') }}" data-description="Регионы">
                        <i class="fa fa-pie-chart" aria-hidden="true"></i> 
                        <span>Регионы</span>
                    </a>
                </li>
                <li>
                    <a class="loop" href="{{ route('list-cities') }}" data-description="Города">
                        <i class="fa fa-commenting-o" aria-hidden="true"></i> 
                        <span>Города</span>
                    </a>
                </li>
                <li>
                    <a class="loop" href="{{ route('category-pluses-room') }}" data-description="Категории плюсов" >
                        <i class="fa fa-puzzle-piece" aria-hidden="true"></i>
                        <span>Категории плюсов</span>
                    </a>
                </li>
                <li>
                    <a class="loop" href="{{ route('list-pluses-room') }}" data-description="Плюсы комнаты" >
                        <i class="fa fa-paper-plane" aria-hidden="true"></i>
                        <span>Плюсы комнаты</span>
                    </a>
                </li>
                <li>
                    <a class="loop" href="{{ route('list-callbacks') }}" data-description="Список звонков" >
                        <i class="fa fa-phone" aria-hidden="true"></i>
                        <span>Список звонков</span>
                    </a>
                </li>
                <li>
                    <a class="loop" href="{{ route('reviews-project') }}" data-description="Отзывы проекта" >
                       <i class="fa fa-comment-o" aria-hidden="true"></i>
                        <span>Отзывы проекта</span>
                    </a>
                </li>
                <li>
                    <a class="loop" href="{{ route('wishes') }}" data-description="Пожелания" >
                        <i class="fa fa-bookmark-o" aria-hidden="true"></i>
                        <span>Пожелания</span>
                    </a>
                </li>
                <li>
                    <a class="loop" href="{{ route('application-object') }}" data-description="Заявки обьектам" >
                       <i class="fa fa-window-restore" aria-hidden="true"></i>
                        <span>Заявки обьектам</span>
                    </a>
                </li>
                <li>
                    <a class="loop" href="{{ route('news-category-admin') }}" data-description="Категории новостей" >
                        <i class="fa fa-file-text-o" aria-hidden="true"></i>
                        <span>Категории новостей</span>
                    </a>
                </li>
                <li>
                    <a class="loop" href="{{ route('news-admin') }}" data-description="Новости проекта" >
                        <i class="fa fa-newspaper-o" aria-hidden="true"></i>
                        <span>Новости проекта</span>
                    </a>
                </li>

                <li>
                    <a class="loop" href="{{ route('pages') }}" data-description="Страницы">
                        <i class="fa fa-share-alt" aria-hidden="true"></i>
                        <span>Страницы</span>
                    </a>
                </li>

                <li>
                    <a class="loop" href="{{ route('discounts') }}" data-description="Скидки">
                        <i class="fa fa-percent" aria-hidden="true"></i>
                        <span>Скидки</span>
                    </a>
                </li>

                <li>
                    <a class="loop" href="{{ route('promocodes') }}" data-description="Промокоды">
                        <i class="fa fa-barcode" aria-hidden="true"></i>
                        <span>Промокоды</span>
                    </a>
                </li>

                <li>
                    <a class="loop" href="{{ route('users') }}" data-description="Пользователи">
                        <i class="fa fa-users" aria-hidden="true"></i>
                        <span>Пользователи</span>
                    </a>
                </li>

                <li>
                    <a class="loop" href="{{ route('questions') }}" data-description="Вопросы/Ответы">
                        <i class="fa fa-question" aria-hidden="true"></i>
                        <span>Опросник</span>
                    </a>
                </li>


            </ul>
        </div>
<!--
        <div class="main-menu">
           <ul class="caption-list">
               <li><a href="javascript:void(0);">Другое <i class="fa fa-angle-down"></i></a></li>
           </ul>
            <ul> 
                <li>
                    <a class="loop" href="" data-description="Настройки">
                        <i class="fa fa-cog" aria-hidden="true"></i> 
                        <span>Настройки</span>
                    </a>
                </li>
                <li>
                    <a class="loop" href="{{ route('logout') }}" data-description="Выход">
                        <i class="fa fa-sign-out" aria-hidden="true"></i> 
                        <span>Выход</span>
                    </a>
                </li>
            </ul>
        </div>               
-->
                        
    </div>
<div id="topMenu">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="col-lg-4">
                        <div class="logo">
                            <img src="/images/logo-our-admin.png" alt="">
                        </div>
                    </div>
                    <div class="col-lg-4">
<!--
                        <div class="telefons-contacts">
                            <span>Служба поддержки: </span> 
                            <ul>
                                <li><span>38 063-610-85-13 (LIFE)</span> </li>
                                <li><span>38 063-610-85-13 (KIEVSTAR)</span> </li>
                            </ul>
                        </div>
-->
                    </div>
                    <div class="col-lg-4">
                        <div class="mini-menu">
                            <ul>
                                <li><a href=""><i class="fa fa-cog" aria-hidden="true"></i></a></li>
                                <li><a href="{{ route('help-admin') }}"><i class="fa fa-envelope-o" aria-hidden="true"></i></a></li>
                                <li><a href="{{ route('logout') }}"><i class="fa fa-sign-out" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="wrapper">
        @yield('content')
    </div>
    <div id="overlay"></div>
    <div class="overlay-category"></div>
    <section id="wrapperCopyright">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="copyright">
                        Copyright <i class="fa fa-copyright" aria-hidden="true"></i> 2014-2017 hotels-ok.com. Все права защищены |  <span>Политика конфиденциальности</span>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="loader-inner hide margin-top--20px show-more-loader">
        <div class="loader-line-wrap">
            <div class="loader-line"></div>
        </div>
        <div class="loader-line-wrap">
            <div class="loader-line"></div>
        </div>
        <div class="loader-line-wrap">
            <div class="loader-line"></div>
        </div>
        <div class="loader-line-wrap">
            <div class="loader-line"></div>
        </div>
        <div class="loader-line-wrap">
            <div class="loader-line"></div>
        </div>
    </div>



    <script>
    $('.grid').masonry({
      // options
      itemSelector: '.grid-item',
      columnWidth: '.grid-item',
            horizontalOrder: true

    });

    $(function(){
        $('.name_text').liTranslit({
            elAlias: $('.name_text_translit')
        });
    });

    $(document).ready(function() {
        var date = new Date();
        var afterDate = new Date;
        afterDate.setDate(afterDate.getDate() + 1);
        $('#datetimepicker1').datetimepicker({language: 'ru', pickTime:false, minDate: date,defaultDate: date, format: 'DD-MM-YYYY'});
        $('#datetimepicker2').datetimepicker({language: 'ru', pickTime:false, minDate: afterDate, defaultDate: afterDate, format: 'DD-MM-YYYY'});                  
    });


     $(function(){
        $("#applicationPhone").mask("+38(999) 999-99-99");
        $("#phoneCallback").mask("+38(999) 999-99-99");
    });

    $('#datetimepicker-news').datetimepicker({language: 'ru', pickTime:true, format:'YYYY-MM-DD HH:mm:ss'});


    CKEDITOR.replace( 'text-news' );
</script>

   <div class="js-message-success">
       <div class="alert alert-success"></div>
   </div>
   
    
</body>
</html>