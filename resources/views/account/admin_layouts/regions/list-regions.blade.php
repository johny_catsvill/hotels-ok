@extends('account.admin_layouts.index')
@section('content')

<section id="listServices">
    <div class="container">
        <div class="row">
            <div class="wrapper-list-services style-page">
                <div class="col-lg-12">
                    <h2>Список регионов</h2>
                    <hr>
                    <a href="{{ route('edit-region') }}" class="btn btn-primary btn-add-services-style">Добавить регион</a>
                    @include('auth.success_errors_block')
                    <div class="list-services-block">
                        <table>
                            <tr>
                                <th>Идентификатор</th>
                                <th>Имя региона</th>
                                <th>Страна</th>
                                <th>Статус</th>
                                <th>Редактировать</th>
                                <th>Удалить</th>
                            </tr>
                            @foreach($listRegions as $region)
                                <tr>
                                    <td>{{ $region->id }}</td>
                                    <td>{{ $region->name }}</td>
                                    <td>{{ $region->countryName }}</td>
                                    <td>ON</td>
                                    <td><a href="{{ route('edit-region', ['id' => $region->id]) }}" class="btn btn-primary">Редактировать</a></td>
                                    <td><a href="{{ route('delete-region', ['id' => $region->id]) }}" class="btn btn-danger">Удалить</a></td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </div>
</section>


@endsection