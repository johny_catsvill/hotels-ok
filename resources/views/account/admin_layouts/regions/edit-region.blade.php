@extends('account.admin_layouts.index')
@section('content')


<section id="EditServices">
    <div class="container">
        <div class="row">
            <div class="wrapper-edit-services style-page">
                <div class="col-lg-12">
                    <h2>Добавление региона</h2>
                    <hr>
                    @include('auth.success_errors_block')
                    <form action="{{ route('edit-region') }}" method="post" class="style-form-edit-service">
                        <div class="field">
                            <label for="">Название региона</label>
                            <input type="text" name="name" value="{{ $region ? $region->name : '' }}">
                        </div>
                        
                        <div class="field">
                            <label for="">Выберите страну</label>
                            <select name="countryId" id="">
                                @foreach($listCountries as $country)
                                    <option
                                    @if(isset($region->countryId) && $region->countryId == $country->id)
                                        checked 
                                    @endif 
                                      value="{{ $country->id }}">{{ $country->name }}</option>
                                @endforeach    
                            </select>
                        </div>
                        
                        
                        
                        <input type="hidden" name="regionId"  value="{{ $region ? $region->id : '' }}">
                        {{ csrf_field() }}
                        <input type="submit" value="Сохранить" name="submit" class="btn btn-success">
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>


@endsection