@extends('account.admin_layouts.index')
@section('content')

<section id="listServices">
    <div class="container">
        <div class="row">
            <div class="wrapper-list-services style-page">
                <div class="col-lg-12">
                    <h2>Список категории плюсов</h2>
                    <hr>
                    <a href="{{ route('edit-category-pluses') }}" class="btn btn-primary btn-add-services-style">Добавить категорию</a>
                    @include('auth.success_errors_block')
                    <div class="list-services-block">
                        <table>
                            <tr>
                                <th>Идентификатор</th>
                                <th>Название категории</th>
                                <th>Количество плюсов</th>
                                <th>Статус</th>
                                <th>Редактировать</th>
                                <th>Удалить</th>
                            </tr>
                            @foreach($listCategoriesPluses as $category)
                                <tr>
                                    <td>{{ $category->id }}</td>
                                    <td>{{ $category->name }}</td>
                                    <td>{{ $category->countPluses }}</td>
                                    <td>ON</td>
                                    <td><a href="{{ route('edit-category-pluses', ['id' => $category->id]) }}" class="btn btn-primary">Редактировать</a></td>
                                    <td><a href="{{ route('delete-category-pluses', ['id' => $category->id]) }}" class="btn btn-danger">Удалить</a></td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </div>
</section>


@endsection