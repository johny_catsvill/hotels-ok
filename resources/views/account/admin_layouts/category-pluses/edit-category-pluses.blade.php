@extends('account.admin_layouts.index')
@section('content')


<section id="EditServices">
    <div class="container">
        <div class="row">
            <div class="wrapper-edit-services style-page">
                <div class="col-lg-12">
                    <h2>Добавление категории плюсов</h2>
                    <hr>
                    @include('auth.success_errors_block')
                    <form action="{{ route('edit-category-pluses') }}" method="post" class="style-form-edit-service">
                        <div class="field">
                            <label for="">Название категории</label>
                            <input type="text" name="name" value="{{ $categoryPluses ? $categoryPluses->name : '' }}">
                        </div>
                        <input type="hidden" name="categoryPlusesId"  value="{{ $categoryPluses ? $categoryPluses->id : '' }}">
                        {{ csrf_field() }}
                        <input type="submit" value="Сохранить" name="submit" class="btn btn-success">
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>


@endsection