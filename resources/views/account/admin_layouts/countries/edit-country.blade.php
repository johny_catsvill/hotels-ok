@extends('account.admin_layouts.index')
@section('content')


<section id="EditServices">
    <div class="container">
        <div class="row">
            <div class="wrapper-edit-services style-page">
                <div class="col-lg-12">
                    <h2>Добавление страны</h2>
                    <hr>
                    @include('auth.success_errors_block')
                    <form action="{{ route('edit-country') }}" method="post" class="style-form-edit-service more-style-page">
                        <div class="field">
                            <label for="">Название страны:</label>
                            <input type="text" name="name" value="{{ $country ? $country->name : '' }}">
                        </div>
                        <div class="field">
                            <label for="">Код страны:</label>
                            <input type="text" name="code" value="{{ $country ? $country->code : '' }}">
                        </div>
                        <div class="field">
                            <label for="">Код валюты:</label>
                            <input type="text" name="currencyCode" value="{{ $country ? $country->currencyCode : '' }}">
                        </div>
                        <div class="field">
                            <label for="">Имя валюты:</label>
                            <input type="text" name="currencyName" value="{{ $country ? $country->currencyName : '' }}">
                        </div>
                        <div class="field">
                            <label for="">Сокращенное имя валюты (пример "UAH, USD")</label>
                            <input type="text" name="currencyShortName" value="{{ $country ? $country->currencyShortName : '' }}">
                        </div>
                        <div class="field">
                            <label for="">Код телефона</label>
                            <input type="text" name="phoneCode" value="{{ $country ? $country->phoneCode : '' }}">
                        </div>
                        <input type="hidden" name="countryId"  value="{{ $country ? $country->id : '' }}">
                        {{ csrf_field() }}
                        <input type="submit" value="Сохранить" name="submit" class="btn btn-success">
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>


@endsection