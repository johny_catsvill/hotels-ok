@extends('account.index')
@section('content')

    <section id="reference">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 style-page">
                    <h2>Инструкция по регистрации объекта</h2>
                    <hr>
                    <div class="content-reference">
                        <ul>
                            <li><span class="alert alert-info caption-reference">1. Перейдите по ссылке "Информация об объекте" и заполните необходимые поля, правильными данными.</span>
                               
                                <img src="/images/reference/1.png" alt="">
                                <ul>
                                    <li><span class="alert alert-info caption-reference">1.1. В поле "Координаты отеля" введите точный адрес объекта. Например: г.Киев, ул.Туполева, 12</span>
                                        <img src="/images/reference/2.png" alt="">
                                    </li>
                                    <li><span class="alert alert-info caption-reference">2. Перейдите по ссылке "Список номеров" и добавьте все категории номеров продаваемых у Вас в отеле. Например: Люкс, Полулюкс, Стандарт и т.д.</span>
                                        <img src="/images/reference/3.png" alt="">
                                        <ul>
                                            <li><span class="alert alert-info caption-reference">2.1. Для того что бы добавить категорию номера перейдите по ссылке "Добавить новый номер" и заполните все необходимые поля.</span>
                                                <img src="/images/reference/4.png" alt="">
                                            </li>
                                        </ul>
                                    </li>
                                    <li><span class="alert alert-info caption-reference">2.2. Это действие необходимо сделать для каждой категории номера.</span></li>
                                    <li><span class="alert alert-info caption-reference">3. Перейдите по ссылке "Общие сведения" и заполните необходимые поля, правильными данными.</span>
                                    <img src="/images/reference/5.png" alt=""></li>
                                    <li><span class="alert alert-info caption-reference">4. После заполнение информации перейдите в календарь номеров. На каждой странице справой стороны есть кнопка "Открыть календарь". Заполните информацию в календаре для возможности находить и бронировать Ваши номера.</span>
                                    <img src="/images/reference/6.png" alt="">
                                        <ul>
                                            <li><span class="alert alert-info caption-reference">4.1 Вверху присутствуют фильтры для удобной работый с номерами. Вы можете выбрать любой номер, а так же конкретный месяц и год.</span>
                                                <img src="/images/reference/7.png" alt="">
                                            </li>
                                        </ul>
                                    </li>
                                    <li><span class="alert alert-info caption-reference">4.2. При нажатии на одну из дат в календаре Вы можете установить различные параметры номера на определенную дату, а так же цену, скидку или доступность номера. Обязательно указывайте количество доступных номеров.</span>
                                        <img src="/images/reference/8.png" alt="">
                                    </li>
                                    <li><span class="alert alert-info caption-reference">5. После заполнение информации. нажмите кнопку "Проверить объект" в самом верху. Это нужно для того что бы мы могли проверить Ваш отель, а так же улучшить возможность его поиска, или указать на неверно заполненные данные.</span>
                                    <img src="/images/reference/9.png" alt="">
                                    </li>
                                    <li><span class="alert alert-info caption-reference">6. В случае если Вам что то не понятно или Вы хотите задать вопрос администрации, справа вверху есть иконка сообщения. Перейдя по ней Вы можете нам написать и мы в кратчайщие сроки постараемся Вам помочь. А так же Вы можете нам позвонить по номерам которые указанны вверху.</span>
                                    <img src="/images/reference/10.png" alt=""></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection