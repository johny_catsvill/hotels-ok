<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{!! csrf_token() !!}" />
    <title>Панель управления</title>
    <link rel="stylesheet" href="{{ asset('css/reset.css') }}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap-theme.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/font-awesome.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style-admin.css') }}">
    <link rel="stylesheet" href="{{ asset('css/media-admin.css') }}">
    <link rel="stylesheet" href="{{ asset('css/calendar.css') }}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="/css/bootstrap-datetimepicker.css">
    <link rel="stylesheet" href="/css/animate.css">
    <link rel="stylesheet" href="/css/jquery.timepicker.css">
</head>
<body>

    <div id="sidebar" data-power="on">
        <div class="logo-sidebar">
            <i class="fa fa-bars" onclick="MainFunction.showRightSidebar(this);" data-power="0"></i>
        </div>
<!--
        <div class="profile-user">
            <img src="/images/ETa0e0dTFJM.jpg" alt="">
            <span>Jack <br> Wiilliams</span>
            <div class="clear"></div>
        </div>
-->
        
        <div class="main-menu border-bottom-black">
           <ul class="caption-list">
               <li><a href="javascript:void(0);">Меню <i class="fa fa-angle-down"></i></a></li>
           </ul>
            <ul class="menu-hover"> 
                <li>
                    <a class="loop" href="{{ route('domain-home', $subdomain) }}" class="active" data-description="Главная">
                        <img src="/images/icon-menu/home.png" alt="">
                        <span>Главная</span>
                    </a>
                </li>
                <li>
                    <a class="loop" href="{{ route('information-object', $subdomain) }}" data-description="Информация об объекте">
                        <img src="/images/icon-menu/info.png" alt="">
                        <span>Информация об объекте</span>
                    </a>
                </li>
                <li>
                    <a class="loop" class="loop" href="{{ route('rooms', $subdomain) }}" data-description="Список номеров">
                        <img src="/images/icon-menu/list-rooms.png" alt="">
                        <span>Список номеров</span>
                    </a>
                </li>
                <li>
                    <a class="loop" href="{{ route('reservations', $subdomain) }}" data-description="Бронирования">
                        <img src="/images/icon-menu/reservation.png" alt="">
                        <span>Бронирование</span>
                    </a>
                </li>
                <li>
                    <a class="loop" href="{{ route('general-information', $subdomain) }}" data-description="Общие сведения">
                        <img src="/images/icon-menu/svedenie.png" alt="">
                        <span>Общие сведения</span>
                    </a>
                </li>
                <li>
                    <a class="loop" href="{{ route('reviews', $subdomain) }}" data-description="Отзывы">
                        <img src="/images/icon-menu/reviews.png" alt="">
                        <span>Отзывы</span>
                    </a>
                </li>
<!--
                <li>
                    <a class="loop" href="" data-description="Управ. пользователями" >
                        <i class="fa fa-users" aria-hidden="true"></i> 
                        <span>Управ. пользователями</span>
                    </a>
                </li>
-->
            </ul>
        </div>
        <div class="main-menu">
           <ul class="caption-list">
               <li><a href="javascript:void(0);">Другое <i class="fa fa-angle-down"></i></a></li>
           </ul>
            <ul class="menu-hover"> 
                <li>
                    <a href="{{ route('analytics', $subdomain) }}" class="loop" data-description="Аналитика">
                        <img src="/images/icon-menu/analytics.png" alt="">
                    <span>Аналитика</span>
                </a></li>
                <li>
                    <a class="loop" href="{{ route('settings', $subdomain) }}" data-description="Настройки">
                        <img src="/images/icon-menu/settings.png" alt="">
                        <span>Настройки</span>
                    </a>
                </li>
                <li>
                    <a class="loop" href="{{ route('reference', $subdomain) }}" data-description="Cправка">
                        <img src="/images/icon-menu/spravka.png" alt="">
                        <span>Cправка</span>
                    </a>
                </li>
                <li>
                    <a class="loop" href="{{ route('logout') }}" data-description="Выход">
                        <img src="/images/icon-menu/exit.png" alt="">
                        <span>Выход</span>
                    </a>
                </li>
            </ul>
        </div>               
                        
    </div>
    <div id="topMenu">
        <a href="javascript:void(0);" class="menu-mobile">Меню</a>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="col-lg-4">
                        <div class="logo">
                            <img src="/images/logo-our-admin.png" alt="">
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="col-lg-5">
                        <div class="telefons-contacts">
                            <span>Служба поддержки: </span> 
                            <ul>
                                <li><span>38 063-863-08-43 (LIFE)</span> </li>
                                <li><span>38 098-081-42-43 (KIEVSTAR)</span> </li>
                                <li><span>38 099-407-20-43 (MTC)</span> </li>
                                <li><span>info@hotels-ok.com (Почта)</span> </li>
                            </ul>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="col-lg-3">
                        <div class="mini-menu">
                            <ul>
                                <li><a href="{{ route('settings', $subdomain) }}"><i class="fa fa-cog" aria-hidden="true"></i></a></li>
                                <li><a href="{{ route('help', $subdomain) }}"><i class="fa fa-envelope-o @if(@messages > 0) red @endif" aria-hidden="true" title="Помощь и поддержка"></i></a></li>
                                <li><a href="{{ route('logout') }}"><i class="fa fa-sign-out" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="wrapper">
       @include('account.blocks.breadcrumbs')
       <div class="container no-confirmed-account">
           <div class="row">
               <div class="col-lg-12" style="text-align:center;">
                   @if(!$isConfirmed)
                       <div class="alert alert-danger">Ваш аккаунт не подтвержден. Заполните полную информацию о Вашем отеле и комнатах, а     так     же выставьте их на продажу. И после этого нажмите кнопку "Проверить объект" для публикации объекта на сайте. <br>
                           @php
                                $errors = \Illuminate\Support\Facades\DB::table('messages_for_objects')->where('objectId',Auth::user()->objectId)->get();
                           @endphp

                           @if(count($errors))
                               <ul class="align-left" style="padding-left:10px; margin-top: 25px; text-align: left;">
                                   @foreach($errors as $error)
                                        <li>{{ $loop->iteration.'.'.$error->message }}</li>
                                   @endforeach
                               </ul>
                           @endif

                       </div>
                        <a href="{{ route('confirmed-account', $subdomain) }}"  class="btn btn-success">Проверить объект</a>
                   @elseif($isConfirmed == 2)
                       <div class="alert alert-warning">Заявка на одобрение отправлена. В течении нескольких часов мы сообщим Вам результат.</div>
                   @endif
               </div>
           </div>
       </div>
 
        @yield('content')
    </div>
    
    <section id="wrapperCopyright">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="copyright">
                        Copyright <i class="fa fa-copyright" aria-hidden="true"></i> 2014-2017 hotels-ok.com. Все права защищены |  <span>Политика конфиденциальности</span>
                    </div>
                </div>
            </div>
        </div>
    </section>



    <script type="text/javascript" src="{{ asset('js/jquery-3.1.1.min.js') }}"></script>
    <script src="/js/moment-with-locales.min.js"></script>    
    <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>
    <script src="{{ asset('js/script-admin.js') }}"></script>
    <script src="{{ asset('js/masonry.pkgd.min.js') }}"></script>
    <script src="{{ asset('js/jquery.liTranslit.js') }}"></script>
    <script src="{{ asset('js/calendar.js') }}"></script>
    <script src="{{ asset('js/jquery.maskedinput.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
    <script src="/js/jquery.timepicker.js"></script>

    <script type="text/javascript">
        $(function(){
            $("#phoneCallback").mask("+38(999) 999-99-99");
            $("#phone").mask("+38(999) 999-99-99");
            $("#fax").mask("+38(999) 999-99-99");
            $("#reservationPhone").mask("+38(999) 999-99-99");
            $("#reservationFax").mask("+38(999) 999-99-99");
            $("#bookkeepingPhone").mask("+38(999) 999-99-99");
            $("#bookkeepingFax").mask("+38(999) 999-99-99");
            $("#phoneSms").mask("+38(999) 999-99-99");
        });
    </script>
    <script src="/js/validation.js"></script>
    <script>
    $('.grid').masonry({
      // options
      itemSelector: '.grid-item',
      columnWidth: '.grid-item',
            horizontalOrder: true

    });

    $(function(){
        $('.name_text').liTranslit({
            elAlias: $('.name_text_translit')
        });
    });

    $('#datetimepickerReservation1').datetimepicker({language: 'ru', pickTime:false, format: 'DD-MM-YYYY'});
    $('#datetimepickerReservation2').datetimepicker({language: 'ru', pickTime:false,  format: 'DD-MM-YYYY'});


        $('input[name="checkIn"]').timepicker({'step': 60, 'scrollDefault': 'now', 'timeFormat': 'H:i' ,'minTime': '00.00', 'maxTime': '23.30', 'showDuration': true });
        $('input[name="checkOut"]').timepicker({'step':60, 'scrollDefault': 'now', 'timeFormat': 'H:i' ,'minTime': '00.00', 'maxTime': '23.30', 'showDuration': true });
        

</script>
    <div class="message-errors">
        <a href="{{ route('help', $subdomain) }}" class="">Сообщить об ошибке</a>    
    </div>
    
            
</body>
</html>