
@if($reservations)
                                    @foreach($reservations as $reservation)
                                        <table class="table-reservation">
                                        <tr class="reserv">
                                           <td>{{ $reservation->id }}</td>
                                            <td>
                                                <span>{{ $reservation->name }}</span>
                                            </td>
                                            <td>{{ date('d.m.Y', $reservation->startReservationDate) }}</td>
                                            <td>{{ date('d.m.Y', $reservation->endReservationDate) }}</td>
                                            <td>
                                                <span>{{ $reservation->roomsName }}</span>
                                            </td>
                                            <td>
                                                <span>{{ $reservation->visitorPhone }}</span>
                                            </td>
                                            <td>{{ date('d.m.Y', $reservation->creationDate) }}</td>
                                            <td>
                                               @if($reservation->statusId == 3)
                                                    <i class="fa fa-check-circle success-order" title="{{ $reservation->statusName }}"></i>
                                                @elseif($reservation->statusId == 2)
                                                    <i class="fa fa-close fail-order" title="{{ $reservation->statusName }}"></i>
                                                @endif
                                            </td>
                                            <td>UAH {{ $reservation->price }}</td>
                                            <td>UAH 181.20</td>
                                            <td>
                                                <a href="{{ route('get-info-reservation-by-id', ['domain' => $subdomain, 'id' => $reservation->id]) }}">Подробнее</a>
                                            </td>
                                        </tr>
                                        </table>
                                        <div class="reservation-style-mobile">
                                            <table>
                                                <tr>
                                                    <td>{{ $reservation->id }}</td>
                                                    <td>{{ $reservation->name }}</td>
                                                    <td>
                                                        @if($reservation->statusId == 3)
                                                            <i class="fa fa-check-circle success-order" title="{{ $reservation->statusName }}"></i>
                                                        @elseif($reservation->statusId == 2)
                                                            <i class="fa fa-close fail-order" title="{{ $reservation->statusName }}"></i>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <a href="javascript:void(0);" onclick="MainFunction.showMoreInfoReservation(this);" data-active="off">Подробнее</a>
                                                    </td>
                                                </tr>
                                                <tr class="description-reservation">
                                                    <td colspan="4">
                                                        <ul>
                                                            <li><b>Идент.брони</b><span>{{ $reservation->id }}</span></li>
                                                            <li><b>ФИО</b><span>{{ $reservation->name }}</span></li>
                                                            <li><b>Дата заезда</b><span>{{ date('d.m.Y', $reservation->startReservationDate) }}</span></li>
                                                            <li><b>Дата отьезда</b><span>{{ date('d.m.Y', $reservation->endReservationDate) }}</span></li>
                                                            <li><b>Название комнаты</b><span>{{ $reservation->roomsName }}</span></li>
                                                            <li><b>Телефон</b><span>{{ $reservation->visitorPhone }}</span></li>
                                                            <li><b>Дата создания</b><span>{{ date('d.m.Y', $reservation->creationDate) }}</span></li>
                                                            <li><b>Статус</b><span>
                                                                    @if($reservation->statusId == 3)
                                                                        Активна
                                                                    @elseif($reservation->statusId == 2)
                                                                        Отменено
                                                                    @endif
                                                                </span></li>
                                                            <li><b>Цена</b><span>{{ $reservation->price }}</span></li>
                                                            <li><b>Предоплата</b><span>UAH 181.20</span></li>
                                                            <li><b></b><span><a class="btn btn-primary" href="{{ route('get-info-reservation-by-id', ['domain' => $subdomain, 'id' => $reservation->id]) }}">Подробнее</a></span></li>
                                                        </ul>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    @endforeach
                                @endif
