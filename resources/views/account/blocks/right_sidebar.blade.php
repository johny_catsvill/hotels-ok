<div class="col-lg-3">
                    <div class="right-sidebar">
                        <div class="wrapper-calendar">
                            <a href="{{ route('calendar', $subdomain) }}" style="text-decoration:none;"><i class="fa fa-calendar" aria-hidden="true"></i></a>
                            <a href="{{ route('calendar', $subdomain) }}">Открыть общий календарь управления номерами</a>
                        </div>
                        <div class="wrapper-help">
                            <span class="caption"><i class="fa fa-share" aria-hidden="true"></i> Поможем. Подскажем.</span>
                            <h5>Отдел по работе с отелями:</h5>
                            <div class="list-contacts">
                                <ul>
                                    <li><i class="fa fa-phone" aria-hidden="true"></i>
                                        <ul>
                                            <li>063 863 08 43</li>
                                            <li>098 081 42 43</li>
                                            <li>099 407 20 43</li>
                                        </ul>
                                    </li>
                                    <li>
                                       <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                        <ul>
                                            <li>info@hotels-ok.com</li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>