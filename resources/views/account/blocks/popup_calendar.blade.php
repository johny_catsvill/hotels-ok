<div class="popup-calendar">
    <i class="fa fa-close close-popup-calendar" onclick="Calendar.closePopupCalendar(event);"></i>
    <div class="content-popup-calendar">
        <form action="">
        <div class="content-period">
            <span class="caption-period">Выбранный период</span>
            <span class="input-period-date"><input type="text" name="dateFrom" class="datepicker" id="datetimepicker1" value=""> - <input value="" name="dateTo" class="datepicker" type="text" id="datetimepicker2"></span>
        </div>
        <div class="period-checkbox">
            <table>
                <tr>
                    <td>Пн</td>
                    <td>Вт</td>
                    <td>Ср</td>
                    <td>Чт</td>
                    <td>Пт</td>
                    <td>Сб</td>
                    <td>Вс</td>
                </tr>
                <tr>
                    <td><input type="checkbox" name="dayWeek[]" value="1"></td>
                    <td><input type="checkbox" name="dayWeek[]" value="2"></td>
                    <td><input type="checkbox" name="dayWeek[]" value="3"></td>
                    <td><input type="checkbox" name="dayWeek[]" value="4"></td>
                    <td><input type="checkbox" name="dayWeek[]" value="5"></td>
                    <td><input type="checkbox" name="dayWeek[]" value="6"></td>
                    <td><input type="checkbox" name="dayWeek[]" value="0"></td>
                </tr>
            </table>
        </div>
        <div class="price-room"><span>Цена за номер (<strong>грн</strong>) <b><input type="text" name="price" value="{{ $price }}"></b></span><div class="clear"></div></div>
        <div class="clear"></div>
        <div class="offers-room-wrapper">
            <span>Скидка на номер (%) <b><input type="text" value="{{ $roomInfo->valuePercent }}" name="discount"></b></span>
        </div>
        <div class="clear"></div>

        <div class="price-room"><span>Свободных номеров <b><input type="text" value="{{ $roomInfo->countRoom or ''}}" name="countRoom"></b></span><div class="clear"></div></div>
        <div class="room-not-available">
            <label for=""><input type="checkbox" id="isAvailable" name="isAvailable" @if(isset($roomInfo->closeRoom) && $roomInfo->closeRoom) checked @endif><span><label
                            for="isAvailable">Номер недоступен</label></span></label>
        </div>
        <div class="wrapper-btn-popup">
            <input type="submit" value="Сохранить" class="btn btn-success" onclick="Calendar.sendDataByRoom(event);">
            <input type="submit" value="Отменить" class="btn btn-danger" onclick="Calendar.closePopupCalendar(event);">
        </div>
        </form>
    </div>
</div>

