<div class="list-tabs">
                                <ul>
                                    <li>
                                        <span class="new-reservation-color">
                                            <i class="fa fa-clock-o" aria-hidden="true"></i> <b>Новое бронирование</b>
                                            <div class="clear"></div>
                                        </span>
                                            
                                        <ul class="sublist-info">
                                           @if(count($reservations) > 0)
                                               @foreach($reservations as $reservation)
                                                   <li>
                                                        <table>
                                                            <tr>
                                                                <td class="user-review">{{ $reservation->name.' '. $reservation->lastName }}</td>
                                                                <td>
                                                                    <ul>
                                                                        <li><span>{{ $reservation->countRoom }}  {{ Lang::choice('validation.room', $reservation->countRoom) }}</span></li>
                                                                        <i class="fa fa-circle" aria-hidden="true"></i>
                                                                        <li><span>{{ $reservation->countPerson }} {{ Lang::choice('validation.guest', $reservation->countPerson) }}</a></li>
                                                                        <i class="fa fa-circle" aria-hidden="true"></i>
                                                                        <li><span>@countDayInPeriod($reservation->startReservationDate, $reservation->endReservationDate) {{ Lang::choice('validation.night', @countDayInPeriod($reservation->startReservationDate, $reservation->endReservationDate)) }}</span></li>
                                                                    </ul>
                                                                </td>
                                                                <td class="see-review-object">
                                                                    <a href="{{ route('get-info-reservation-by-id', ['domain' => $subdomain, 'id' => $reservation->id]) }}" class="btn btn-primary">Просмотреть</a>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </li>
                                               @endforeach
                                           @else
                                                <li>У вас нет ни каких бронирований.</li>
                                           @endif    
                                        </ul>
                                    </li>
                                    <li>
                                        <span class="comein-color"><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i> <b>Заезд</b>
                                        <div class="clear"></div>
                                        </span>
                                        <ul class="sublist-info">
                                           @if(count($reservationsArrival) > 0)
                                               @foreach($reservationsArrival as $reservation)
                                                   <li>
                                                        <table>
                                                            <tr>
                                                                <td class="user-review">{{ $reservation->name.' '. $reservation->lastName }}</td>
                                                                <td>
                                                                    <ul>
                                                                        <li><span>{{ $reservation->countRoom }} {{ Lang::choice('validation.room', $reservation->countRoom) }}</span></li>
                                                                        <i class="fa fa-circle" aria-hidden="true"></i>
                                                                        <li><span>{{ $reservation->countPerson }} {{ Lang::choice('validation.guest', $reservation->countPerson) }}</a></li>
                                                                        <i class="fa fa-circle" aria-hidden="true"></i>
                                                                        <li><span>@countDayInPeriod($reservation->startReservationDate, $reservation->endReservationDate) {{ Lang::choice('validation.night', @countDayInPeriod($reservation->startReservationDate, $reservation->endReservationDate)) }}</span></li>
                                                                    </ul>
                                                                </td>
                                                                <td class="see-review-object">
                                                                    <a href="{{ route('get-info-reservation-by-id', ['domain' => $subdomain, 'id' => $reservation->id]) }}" class="btn btn-primary">Просмотреть</a>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </li>
                                               @endforeach
                                           @else
                                                <li>У вас нет ни каких заездов.</li>
                                           @endif    
                                        </ul>
                                    </li>
                                    <li>
                                        <span class="comeout-color"><i class="fa fa-arrow-circle-o-left" aria-hidden="true"></i> <b>Отьезд</b>
                                        <div class="clear"></div>
                                        </span>
                                        <ul class="sublist-info">
                                           @if(count($reservationsDeparture) > 0)
                                               @foreach($reservationsDeparture as $reservation)
                                                   <li>
                                                        <table>
                                                            <tr>
                                                                <td class="user-review">{{ $reservation->name.' '. $reservation->lastName }}</td>
                                                                <td>
                                                                    <ul>
                                                                        <li><span>{{ $reservation->countRoom }} {{ Lang::choice('validation.room', $reservation->countRoom) }}</span></li>
                                                                        <i class="fa fa-circle" aria-hidden="true"></i>
                                                                        <li><span>{{ $reservation->countPerson }} {{ Lang::choice('validation.guest', $reservation->countPerson) }}</a></li>
                                                                        <i class="fa fa-circle" aria-hidden="true"></i>
                                                                        <li><span>@countDayInPeriod($reservation->startReservationDate, $reservation->endReservationDate) {{ Lang::choice('validation.night', @countDayInPeriod($reservation->startReservationDate, $reservation->endReservationDate)) }}</span></li>
                                                                    </ul>
                                                                </td>
                                                                <td class="see-review-object">
                                                                    <a href="{{ route('get-info-reservation-by-id', ['domain' => $subdomain, 'id' => $reservation->id]) }}" class="btn btn-primary">Просмотреть</a>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </li>
                                               @endforeach
                                           @else
                                                <li>У вас нет ни каких отъездов.</li>
                                           @endif    
                                        </ul>
                                    </li>
                                    <li>
                                        <span class="review-color"><i class="fa fa-comments-o" aria-hidden="true"></i> <b>Отзыв</b>
                                        <div class="clear"></div>
                                        </span>
                                        <div class="sublist-info">
                                           @if(count($reviews) > 0)
                                            <table>
                                               @foreach($reviews as $review)
                                                <tr>
                                                    <td class="user-review">
                                                        <span>{{ $review->authorName }}</span>
                                                        <br>
                                                        <span class="time-review">{{ date('h:m', $review->creationDate) }}</span>
                                                    </td>
                                                    <td class="user-review-raiting">Рейтинг: {{ $review->avgValue }}</td>
                                                    <td class="see-review-object">
                                                        <a href="{{ route('get-info-reservation-by-id', ['domain' => $subdomain, 'id' => $reservation->id]) }}" class="btn btn-primary">Просмотреть</a>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </table>
                                            @else
                                                <span>У вас нет ни каких отзывов.</span>
                                            @endif
                                        </div>
                                    </li>
                                    <li>
                                        <span><i class="fa fa-eye" aria-hidden="true"></i> <b>Просмотры</b>
                                        <div class="clear"></div>
                                        </span>
                                        <ul class="sublist-info">
                                            <table>
                                                <tr>
                                                    <td class="user-review">Количество просмотров</td>
                                                    <td class="user-review-raiting">0</td>
                                                    <td></td>
                                                </tr>
                                            </table>
                                        </ul>
                                    </li>
                                </ul>
                                
                            </div>