
<section id="breadcrumbs">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                @if( isset($breadcrumbs))

                    <div class="list-breadcrumbs">
                        <ul>
                            @foreach($breadcrumbs as $key => $breadcrumb)
                                <li>
                                    <a href="{{ $breadcrumb }}">{{ __($key) }}</a>
                                </li>
                                @if(!$loop->last)
                                    <li>/</li>
                                @endif

                            @endforeach
                        </ul>

                    </div>
                @endif
            </div>
        </div>
    </div>
</section>