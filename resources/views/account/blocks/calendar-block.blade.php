    <div class="list-month">
        <ul>
            @foreach($listMonths as $key => $month)
                <li><a href="javascript:void(0);" data-month="{{ $key }}" onclick="Calendar.reloadMonth({{ $loop->iteration }});" class="@if($needMonth == $loop->iteration) active  @endif">{{ $month  }}</a></li>
            @endforeach
        </ul>
    </div>

    <div class="style-calendar" id="calendarTable">
       @if($calendar)
        {!! $calendar !!}
        @else
            <div class="alert alert-danger message-danger-calendar">Для того что бы воспользоваться календарём, нужно создать комнаты для обьекта. <br>
                <a href="{{ route('edit-room', $subdomain) }}" class="btn btn-primary">Создать комнату</a>
            </div>
        @endif
    </div>



