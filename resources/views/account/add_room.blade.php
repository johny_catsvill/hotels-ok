@extends('account.index')
@section('content')

<section id="addRoom">
    <div class="container">
        <div class="row">
            <div class="wrapper-add-room style-page">
                <div class="col-lg-9">
                    <h2 class="caption-page">{{ $caption }}</h2>
                    <hr>
                    <div class="alert alert-danger alert-danger-js display-none">
                        <ul></ul>
                    </div>
                    @include('auth.success_errors_block')
                    <form action="{{ route('edit-room', $subdomain) }}" method="post" enctype="multipart/form-data">
                            <a href="javascript:void(0);" onclick="MainFunction.clickByFieldFile('gallery_img');" class="gallery_images btn btn-primary">Загрузить фотографии</a>
                            <input type="file" name="gallery_img" value="Выбрать изображение" accept="image/*" multiple class="hidden-file-field" onchange="MainFunction.editGalleryImgRoom(this);">
                            <div id="upload_image" class="@if(!$listImagesObject) display-none @endif">
                                @if($listImagesObject)
                                    @foreach($listImagesObject as $image)
                                        <div class="wrapper-for-img" style="background-image:url(../{{ $image }});">
                                            <i class="fa fa-close" title="Удалить изображение" onclick="MainFunction.removeImgObject(this);"></i>
                                            <input type="hidden" name="imgHidden" value="{{ $image }}"> 
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        <div class="field">
                            <label for="">Название номера:</label>
                            <input type="text" name="name" data-name="Название номера" value="@if($room){{ $room->name }}@endif">
                        </div>
                        <div class="field">
                            <label for="">Количество номеров / мест для продажи(цифрами):</label>
                            <input type="text" name="countRoom" data-name="Количество номеров" value="@if($room){{ $room->countRoom }}@endif">
                        </div>
                        <div class="field">
                            <label for="">Тип оплаты:</label>
                            <select name="typePayment" id="">
                                <option value="priceAboutRoom" @if($room) {{ $room->typePayment == 'priceAboutRoom' ? 'selected' : '' }} @endif>Цена за номер</option>
                                <option value="priceAboutPlace" @if($room) {{ $room->typePayment == 'priceAboutPlace' ? 'selected' : '' }} @endif>Цена за место</option>
                            </select>
                        </div>
                        <div class="field">
                            <label for="">Цена в грн(цифрами):</label>
                            <input type="text" value="@if($room){{ $room->price }}@endif" name="price" data-name="Цена в грн">
                        </div>

                        <div class="field">
                            <label for="">Цена за ?-местное проживание:</label>
                            <input type="text" name="morePlace" value="@if($room) {{ $room->morePlace }} @endif">
                        </div>
                        <h4>Кровати</h4>
                        <hr>
                        <div class="field">
                            <label for="">Количество мест в номере(цифрами):</label>
                            <input type="text" name="amountPerson" data-name="Количество мест в номере" value="@if($room){{ $room->amountPerson }}@endif">
                        </div>
                       <div class="wrapper-beds">
                           <span>Кровати какого типа доступны в этом номере?</span>
                           <table>
                               
                               @if($listBedsThatRoom) 
                                   @foreach($listBedsThatRoom as $bedThatRoom)
                                       <tr class="">
                                           <td>
                                               <select name="bedTypeId[]" id="">
                                                  @foreach($listBedsForRoom as $bed)
                                                       <option value="{{ $bed->id }}" @if($bedThatRoom->bedTypeId == $bed->id) selected @endif >{{ $bed->name }}</option>
                                                   @endforeach
                                               </select>
                                            </td>
                                           <td>x</td>
                                           <td>
                                               <select name="countBed[]" id="">
                                                   <option value="1" @if($bedThatRoom->countBed == 1) selected @endif >1</option>
                                                   <option value="2" @if($bedThatRoom->countBed == 2) selected @endif >2</option>
                                                   <option value="3" @if($bedThatRoom->countBed == 3) selected @endif >3</option>
                                               </select>
                                           </td>
                                           <td class="wrapper-btn-delete-for-room">
                                              @if (!$loop->first)
                                               <a href="javascript:void(0);" onclick="MainFunction.removeThisBed(this);" class="btn btn-danger style-for-btn-delete ">Удалить</a>
                                               @endif
                                           </td>
                                       </tr>
                                   @endforeach
                                   @endif
                                   <tr class="standart-field-bed display-none">
                                       <td>
                                           <select class="js-bed-type" name="" id="">
                                              @foreach($listBedsForRoom as $bed)
                                                   <option value="{{ $bed->id }}">{{ $bed->name }}</option>
                                               @endforeach
                                           </select>
                                        </td>
                                       <td>x</td>
                                       <td>
                                           <select class="js-count-bed" name="" id="">
                                               <option value="1">1</option>
                                               <option value="2">2</option>
                                               <option value="3">3</option>
                                           </select>
                                       </td>
                                       <td class="wrapper-btn-delete-for-room"></td>
                                   </tr>
                               
                           </table>
                           <a href="javascript:void(0);" class="btn btn-primary" onclick="MainFunction.addBedInTheRoom();"><i class="fa fa-plus-circle"></i> Добавить кровать</a>
                       </div>
                        <h4>Услуги в номере</h4>
                        <hr>            
                        <div class="list-services-room">
                            <ul class="grid">
                               @foreach($listCategoriesPlusesRoom as $category)
                                    <li class="grid-item">
                                        <div class="filter">
                                            <span class="caption-filter-block">{{ $category->name }}</span>
                                            <div class="list-checkbox">
                                                <table>
                                                   @foreach($category->pluses as $pluse)
                                                        <tr class="string-pluse-style" onclick="MainFunction.clickAboutCheckbox(this);">
                                                            <td class="string-checkbox-style">
                                                                <label for="">
                                                                    <input type="checkbox" @if(isset($pluse->active)) checked @endif onclick="MainFunction.clickAboutCheckbox(this);" name="pluse[]" value="{{ $pluse->id }}" >
                                                                </label>
                                                            </td>
                                                            <td><span>{{ $pluse->name }}</span></td>
                                                        </tr>
                                                    @endforeach
                                                </table>
                                            </div>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                        <h4>Описание номера на русском языке(минимально 30 знаков):</h4>
                        <textarea name="infoAboutRoom" onkeyup="MainFunction.countLetters(this, 'infoAboutRoomLetters');" data-name="Описание номера" id="" class="text-room" cols="30" rows="10">@if($room){{ $room->infoAboutRoom }}@endif</textarea>
                        Минимальное количество символов: 30. Количество символов: <b class="infoAboutRoomLetters"></b></span>
                        <br>
                        <input type="hidden" name="roomId" value="@if($room){{ $room->id }}@endif">
                        {{ csrf_field() }}
                        <input type="submit" name="submitRoom" value="Сохранить" class="btn-save-room" onclick="MainFunction.checkingDataRoom(event);">
                    </form>
                </div>
                @include('account.blocks.right_sidebar')
                <div class="clear"></div>
            </div>
        </div>
    </div>
</section>
<a href="javascript:void(0);" onclick="MainFunction.removeThisBed(this);" class="btn btn-danger style-for-btn-delete btn-delete-style-pattern">Удалить</a>
@include('account.admin_layouts.blocks.wrapper_for_img_gallery')
@endsection