@extends('account.index')
@section('content')


<section id="infoObject">
    <div class="container">
        <div class="row">
            
            <div class="info-object style-page">
                <div class="col-lg-9">
                    <h2 class="caption-page">Информация об отеле</h2>
                    <hr>
                    <div class="alert alert-danger alert-danger-js display-none">
                        <ul></ul>
                    </div>
                    @include('auth.success_errors_block')
                    <form action="{{ route('information-object', $subdomain) }}" method="post" enctype="multipart/form-data">
                     {{ csrf_field() }}
                      <h4>Основная фотография</h4>
                      <hr>
                       <a href="javascript:void(0);" class="btn btn-primary" onclick="MainFunction.clickByFieldFile('main_img');">Выбрать изображения</a>
                        <input type="file" value="Выбрать изображение" accept="image/*" multiple name="main_img" class="hidden-file-field" onchange="MainFunction.onchangeImgObject(this);">
                        <input type="hidden" name="old_img" value="{{ $objectHotel->image }}">
                        <img id="img-preview" src="@if($objectHotel->image) /images/hotels/{{ $objectHotel->id }}-{{ $subdomain }}/{{$objectHotel->image}} @endif" />
                       <h4>Дополнительные фотографии</h4>
                       <hr>
                        <a href="javascript:void(0);" class="gallery_images btn btn-primary" onclick="MainFunction.clickByFieldFile('gallery_img');">Загрузить фотографии</a>
                        <input type="file" name="gallery_img" value="Выбрать изображение" accept="image/*" multiple class="hidden-file-field" onchange="MainFunction.editGalleryImgObject(this);">
                        <div id="upload_image" class="@if(!$listImagesObject) display-none @endif">
                            @if($listImagesObject)
                                @foreach($listImagesObject as $image)
                                    <div class="wrapper-for-img" style="background-image:url(../{{ $image }});">
                                        <i class="fa fa-close" title="Удалить изображение" onclick="MainFunction.removeImgObject(this);"></i>
                                        <input type="hidden" name="imgHidden" value="{{ $image }}">
                                    </div>
                                @endforeach
                            @endif
                            
                        </div>
                        <h4>Информация</h4>
                        <hr>

                        <div class="field">
                            <label for="">Официальная категория объекта(кол-звезд):</label>
                            <select name="stars" data-name="Количество звёзд" id="" class="">
                                <option value="">Выберите категорию</option>
                                @foreach($listStars as $key => $star)
                                    <option value="{{ $key }}" @if($objectHotel->stars == $key) selected @endif>{{ $star }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="field">
                            <label for="">Количество номеров на объекте:</label>
                            <input type="text" name="countRooms" data-name="Количество номеров на объекте" value="{{ $objectHotel->countRooms }}">
                        </div>
                        <div class="field">
                            <label for="">Регистрация заезда(время):</label>
                            <input type="text" name="checkIn" data-name="Регистрация заезда" value="{{ $objectHotel->checkIn }}">
                        </div>
                        <div class="field">
                            <label for="">Регистрация отьезда(время):</label>
                            <input type="text" name="checkOut" data-name="Регистрация отьезда" value="{{ $objectHotel->checkOut }}">
                        </div>
                        <div class="field">
                            <label for="">Стойка регистрации работает круглосуточно:</label>
                            <select name="receptionRoundClock" data-name="Стойка регистрации" id="" class="">
                                <option value="1" @if($objectHotel->receptionRoundClock == 1) selected @endif>Да</option>
                                <option value="0" @if($objectHotel->receptionRoundClock == 2) selected @endif>Нет</option>
                            </select>
                        </div>
                        
                        <div class="field style-for-select-prepayment">
                            <label for="">Возможность поселения без предоплаты:</label>
                            за <select name="prePayment" data-name="Возможность поселения без предоплаты" id="" class="">
                                <option value="0" @if($objectHotel->prePayment == 0) selected @endif>0</option>
                                <option value="1" @if($objectHotel->prePayment == 1) selected @endif>1</option>
                                <option value="2" @if($objectHotel->prePayment == 2) selected @endif>2</option>
                                <option value="3" @if($objectHotel->prePayment == 3) selected @endif>3</option>
                            </select>
                            сут. до заезда
                        </div>
                        
<!--
                        <div class="field">
                            <label for="">Возможность отмены:</label>
                            <select name="" id="" onchange="MainFunction.changeTypeCancel(this);" class="">
                                <option value="">Бесплатная отмена</option>
                                <option value="">Отмена невозможна</option>
                                <option value="3">Расширенный порядок аннуляции</option>
                            </select>
                            <div class="wrapper-annulation">
                                <span class="style-for-select-annulation">
                                       До <select name="" id="" class="select-style-penalty">
                                        <option value="">1</option>
                                    </select>
                                    <span>сут. штраф</span> <select name="" id="" class="select-style-penalty">
                                        <option value="">0 %</option>
                                    </select>
                                </span>
                                <span class="style-for-select-annulation">
                                       До <select name="" id="" class="select-style-penalty">
                                        <option value=""></option>
                                    </select>
                                    <span>сут. штраф</span> <select name="" id="" class="select-style-penalty">
                                        <option value="">0 %</option>
                                    </select>
                                </span>
                                <span class="style-for-select-annulation">
                                       До <select name="" id="" class="select-style-penalty">
                                        <option value=""></option>
                                    </select>
                                    <span>сут. штраф</span> <select name="" id="" class="select-style-penalty">
                                        <option value="">0 %</option>
                                    </select>
                                </span>
                            </div>
                        </div>
-->
                        
                        <div class="field style-wrapper-radio-btn">
                            <label for="">Базовый способ оплаты за проживания клиента:</label>
                            <div class="wrapper-radio-btn">
                                <span class="style-radio-btn">
                                    <label for=""><input type="radio" id="payHotelsOk" name="typePayment" data-name="Базовый способ оплаты за проживания" value="payHotelsOk" @if($objectHotel->typePayment == 'payHotelsOk') checked @endif><span><label for="payHotelsOk">получить оплату через Hotels-ok.com</label></span></label>
                                </span>

<!--
                                <span class="style-radio-btn">
                                    <label for=""><input type="radio" name="typePayment" value="creditData" data-name="Базовый способ оплаты за проживания" @if($objectHotel->typePayment == 'creditData') checked @endif><span>получить кредитные данные гостя</span></label>
                                </span>
-->
                                <span class="style-radio-btn">
                                    <label for=""><input type="radio" id="payInHotel" name="typePayment" data-name="Базовый способ оплаты за проживания" value="payInHotel"  @if($objectHotel->typePayment == 'payInHotel') checked @endif><span><label for="payInHotel">оплата в отеле</label></span></label>
                                </span>
                            </div>
                            <div class="clear"></div>
                        </div>
                        
                        <div class="field style-wrapper-radio-btn">
                            <label for="">Туристический сбор:</label>
                            <div class="wrapper-radio-btn">
                                <span class="style-radio-btn">
                                    <label for=""><input type="radio" id="IncludedPrice" onchange="MainFunction.changeTouristTax(this);" name="touristTax" data-name="Туристический сбор" value="IncludedPrice"  @if($objectHotel->touristTax == 'IncludedPrice') checked @endif><span><label for="IncludedPrice">входит в стоимость или не взымается</label></span></label>
                                    <input type="hidden" name="touristTaxValue" value="0">
                                </span>
                                <span class="style-radio-btn">
                                    <label for=""><input type="radio" onchange="MainFunction.changeTouristTax(this);" data-name="Туристический сбор" name="touristTax" value="addPrice"  id="addPrice" @if($objectHotel->touristTax == 'addPrice') checked @endif>
                                      <span><label for="addPrice">Добавляется к стоимости за <br> проживания в размере:
                                           <select name="touristTaxValue" @if(!$objectHotel->touristTax == 'IncludedPrice' ) disabled @endif  id="" class="style-for-select-collection">
                                                <option value="0.1" @if($objectHotel->touristTaxValue && $objectHotel->touristTaxValue == 0.1) selected @endif>0.1</option>
                                                <option value="0.2" @if($objectHotel->touristTaxValue && $objectHotel->touristTaxValue == 0.2) selected @endif>0.2</option>
                                                <option value="0.3" @if($objectHotel->touristTaxValue && $objectHotel->touristTaxValue == 0.3) selected @endif>0.3</option>
                                                <option value="0.4" @if($objectHotel->touristTaxValue && $objectHotel->touristTaxValue == 0.4) selected @endif>0.4</option>
                                                <option value="0.5" @if($objectHotel->touristTaxValue && $objectHotel->touristTaxValue == 0.5) selected @endif>0.5</option>
                                                <option value="0.6" @if($objectHotel->touristTaxValue && $objectHotel->touristTaxValue == 0.6) selected @endif>0.6</option>
                                                <option value="0.7" @if($objectHotel->touristTaxValue && $objectHotel->touristTaxValue == 0.7) selected @endif>0.7</option>
                                                <option value="0.8" @if($objectHotel->touristTaxValue && $objectHotel->touristTaxValue == 0.8) selected @endif>0.8</option>
                                                <option value="0.9" @if($objectHotel->touristTaxValue && $objectHotel->touristTaxValue == 0.9) selected @endif>0.9</option>
                                                <option value="1" @if($objectHotel->touristTaxValue && $objectHotel->touristTaxValue == 1) selected @endif>1</option>
                                            </select> %
                                        </label></span>
                                    </label>

                                </span>
                            </div>
                            <div class="clear"></div>
                        </div>
                        
                        <h4>Услуги в отеле</h4>
                        <hr>
                        <table class="services-object">
                            
                               @foreach($listPluses as $pluse)
                                   <tr>
                                    <td><label for=""><input type="checkbox" {{ isset($pluse->active) ? 'checked' : '' }} value="{{ $pluse->id }}" name="pluses[]"></label></td>
                                    <td><span style="cursor:pointer;" onclick="MainFunction.selectCheckboxPluse(this);">{{ $pluse->name }}</span></td>
                                    </tr>
                                @endforeach
                            
                        </table>
                        
                        <h4>Питание</h4>
                        <hr>
                        <table class="services-object">
                           @foreach($listFoodTypes as $foodType)
                               <tr>
                                   <td><label for=""><input name="foodTypes[]" {{ isset($foodType->active) ? 'checked' : '' }} type="checkbox" value="{{ $foodType->id }}"></label></td>
                                   <td><span style="cursor:pointer;" onclick="MainFunction.selectCheckboxPluse(this);">{{ $foodType->name }}</span></td>
                               </tr>
                           @endforeach
                        </table>
                        
                        <h4>Описание объекта</h4>
                        <hr>
                        <textarea name="infoAboutObject" onkeyup="MainFunction.countLetters(this, 'infoAboutObjectLetters');" class="text-room" data-name="Описание объекта" id="" cols="30" rows="10">{{ $objectHotel->infoAboutObject }}</textarea>
                        <span>Минимальное количество символов: 100. Количество символов: <b class="infoAboutObjectLetters"></b></span>
                        <h4>Месторасположение</h4>
                        <hr>
                        <textarea name="locationObject" onkeyup="MainFunction.countLetters(this, 'locationObjectLetters');" class="text-room" id="" cols="30" rows="10">{{ $objectHotel->locationObject }}</textarea>
                        <span><b class="locationObjectLetters"></b></span>
                        <h4>Объекты рядом</h4>
                        <hr>
                        <a href="javascript:void(0);" class="btn btn-primary" onclick="MainFunction.addObjectAbout();"><i class="fa fa-plus"></i> Добавить еще объект</a>
                        
                        <table class="object-where-about">
                            <tr class="standart-string-add-object">
                                <td>
                                    <span>Тип объекта:</span>
                                    <select name="typeObject[]" id="">
                                        @foreach($listTypesObjects as $typeObject)
                                            <option value="{{ $typeObject->id }}">{{ $typeObject->name }}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td>
                                    <span>Название:</span>
                                    <input type="text" name="nameTypeObjectBeside[]" placeholder="Укажите название">
                                </td>
                                <td>
                                    <span>Расстояние до объекта:</span>
                                    <input type="text" name="distanceBesideObject[]" placeholder="35 км">
                                </td>
                                <td>
                                    <a href="javascript:void(0);" class="btn btn-danger btn-remove-object" onclick="MainFunction.deleteAboutObject(this);"><i class="fa fa-close"></i></a>
                                </td>
                            </tr>
                            @if($listObjectsBeside)
                               @foreach($listObjectsBeside as $objectBeside)
                                <tr>
                                    <td>
                                        <span>Тип объекта:</span>
                                        <select name="typeObject[]" id="">
                                            @foreach($listTypesObjects as $typeObject)
                                                <option value="{{ $typeObject->id }}" @if($typeObject->id == $objectBeside->typeBesideId ) selected @endif >{{ $typeObject->name }}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td>
                                        <span>Название:</span>
                                        <input type="text" name="nameTypeObjectBeside[]" value="{{ $objectBeside->name }}">
                                    </td>
                                    <td>
                                        <span>Расстояние до объекта:</span>
                                        <input type="text" name="distanceBesideObject[]" value="{{ $objectBeside->distance }}">
                                    </td>
                                    <td>
                                        <a href="javascript:void(0);" class="btn btn-danger btn-remove-object" onclick="MainFunction.deleteAboutObject(this);"><i class="fa fa-close"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                            @endif
                        </table>
                        
                       <h4>Информация о проезде:</h4>
                       <hr> 
                       <textarea name="infoAboutTravel" class="text-room" id="" cols="30" rows="10">{{ $objectHotel->infoAboutTravel }}</textarea> 
                        
                        <h4>Размещение детей и предоставление дополнительных мест:</h4>
                        <hr>
                        <textarea name="infoExtraPlaces" class="text-room" id="" cols="30" rows="10">{{ $objectHotel->infoExtraPlaces }}</textarea>
                        
                        <h4>НДС, муниципальный налог / налог на туристов:</h4>
                        <hr>
                        <textarea name="infoAdditionalPayments" class="text-room" id="" cols="30" rows="10">{{ $objectHotel->infoAdditionalPayments }}</textarea>
                        
                        <h4>Рестораны и бары:</h4>
                        <hr>
                        <textarea name="restaurantAndBarsInfo" class="text-room" id="" cols="30" rows="10">{{ $objectHotel->restaurantAndBarsInfo }}</textarea>
                        
                        <h4>Лечение:</h4>
                        <hr>
                        <textarea name="infoMedication" class="text-room" id="" cols="30" rows="10">{{ $objectHotel->infoMedication }}</textarea>
                        
                        <h4>Координаты объекта: <span>(для выбора координат объекта введите адрес)</span></h4>
                        <hr>
                        <input type="text" name="address" id="address" value="{{ $objectHotel->address }}" onkeyup="initMapTimer(event);">
                        <div style=" width: 100%; height: 400px;" id="map_canvas"></div>
                        <br>
                        <span><b>Текущие координаты:</b> <br>
                        lat: <span class="wrapper-latitude">
                            {{ $objectHotel->latitudeGps }}
                        </span>, lng: <span class="wrapper-longitude">
                            {{ $objectHotel->longitudeGps }}
                        </span>
                        </span>
                        <input type="hidden" name="longitudeGps" value="{{ $objectHotel->longitudeGps }}">
                        <input type="hidden" name="latitudeGps" value="{{ $objectHotel->latitudeGps }}">
                        <br>
                        
                        <input type="submit" name="submitRoom" value="Сохранить" onclick="MainFunction.checkingDataInfoObject(event);" class="btn-save-room">
                    </form>
                </div>
                @include('account.blocks.right_sidebar')
                <div class="clear"></div>
            </div>
            
        </div>
    </div>
</section>
@include('account.admin_layouts.blocks.wrapper_for_img_gallery')
@include('account.admin_layouts.blocks.map_geocoder')
@endsection