@extends('account.index')
@section('content')

<section id="generalInformation">
    <div class="container">
        <div class="row">
            <div class="wrapper-general-info style-page">
                <div class="col-lg-9">
                    <h2>Общие сведения</h2>
                    <hr>
                    <div class="alert alert-danger alert-danger-js display-none">
                        <ul></ul>
                    </div>
                   @include('auth.success_errors_block')
                    <div class="list-block-general-info">
                       <form action="{{ route('general-information', $subdomain) }}" method="post">
                        <ul>
                            <li class="item-info">
                                <h4>Общие сведения об объекте</h4>
                                <hr>
                                <div class="wrapper-field-form">
                                    <div class="field">
                                        <label for="">Название объекта:</label>
                                            <input type="text" name="name" data-name="Название объекта" value="{{ isset($object->name) ? $object->name : '' }}" class="name_text">
                                    </div>
                                    <div class="field">
                                        <label for="">URL объекта:</label>
                                        <input type="text" name="url" value="{{ isset($object->url) ? $object->url : '' }}" class="name_text_translit" disabled>
                                    </div>
                                    <div class="field">
                                        <label for="">Тип объекта:</label>
                                        <select name="objectCategoryId" data-name="Тип отеля" id="">
                                            <option value="">Выберите тип</option>
                                            @foreach($listCategoriesObject as $categoryObject)
                                                <option value="{{ $categoryObject->id }}" @if(isset($object->objectCategoryId) && $object->objectCategoryId == $categoryObject->id) selected @endif >{{ $categoryObject->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="field">
                                        <label for="">Страна:</label>
                                        <select name="countryId" data-name="Страна" id="">
<!--                                            <option value="">Выберите страну</option>-->
                                            @foreach($listCountries as $country)
                                                <option value="{{ $country->id }}" @if(isset($object->countryId) && $object->countryId == $country->id) selected @endif>{{ $country->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="field">
                                        <label for="">Область:</label>
                                        <select name="regionId" id="" data-name="Область" onchange="MainFunction.selectRegionAndLoadCity(this);">
                                            <option value="">Выберите область</option>   
                                            @foreach($listRegions as $region)
                                                <option value="{{ $region->id }}" @if(isset($object->regionId) && $object->regionId == $region->id) selected @endif>{{ $region->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="field">
                                        <label for="">Индекс / Город:</label>
                                        <input type="text" data-name="Индекс" class="style-for-field-index" name="indexMail" value="{{ isset($object->indexMail) ? $object->indexMail : '' }}"> / <select name="cityId" data-name="Город" id="" class="style-for-field-city" @if(!$listCities) disabled @endif>
                                            <option value="">Выберите город</option>
                                            @if($listCities)
                                                @foreach($listCities as $city)
                                                    <option value="{{ $city->id }}" @if(isset($object->cityId) && $object->cityId == $city->id) selected @endif>{{ $city->name }}</option>
                                                @endforeach
                                            @endif
                                        </select>    
                                    </div>
                                    <div class="field">
                                        <label for="">Улица:</label>
                                        <input type="text" name="street" data-name="Улица" value="{{ isset($object->street) ? $object->street : '' }}">
                                    </div>
                                    <div class="field">
                                        <label for="">№ дома:</label>
                                        <input type="text" name="numberHouse" data-name="Номер дома" value="{{ isset($object->numberHouse) ? $object->numberHouse : '' }}">
                                    </div>
                                    <div class="field">
                                        <label for="">№ квартиры:</label>
                                        <input type="text" name="numberApartment" value="{{ isset($object->numberApartment) ? $object->numberApartment : '' }}">
                                    </div>
                                    <div class="field">
                                        <label for="">Телефон (с кодом города):</label>
                                        <input type="text" id="phone" name="phone" data-name="Телефон" value="{{ isset($object->phone) ? $object->phone : '' }}">
                                    </div>
                                    <div class="field">
                                        <label for="">Факс (с кодом города):</label>
                                        <input type="text" id="fax" name="fax" value="{{ isset($object->fax) ? $object->fax : '' }}">
                                    </div>
                                    <div class="field">
                                        <label for="">Контактное лицо(Ф.И.О):</label>
                                        <input type="text" name="contactName" data-name="Контактное лицо" value="{{ isset($object->contactName) ? $object->contactName : '' }}">
                                    </div>
                                    <div class="field">
                                        <label for="">Почтовый адрес:</label>
                                        <input type="email" name="email" data-name="Почтовый адрес" value="{{ isset($object->email) ? $object->email : '' }}">
                                    </div>
                            </div>      
                                
                            </li>
                            <li class="item-info">
                                <h4>Отдел бронирования</h4>
                                <hr>
                                <div class="wrapper-field-form">
                                    <div class="field">
                                        <label for="">Ф.И.О:</label>
                                        <input type="text" name="reservationPeopleFio" data-name="Отдел бронирования -> ФИО" value="{{ isset($object->reservationPeopleFio) ? $object->reservationPeopleFio : '' }}">
                                    </div>
                                    <div class="field">
                                        <label for="">Должность:</label>
                                        <input type="text" name="reservationPeoplePosition" value="{{ isset($object->reservationPeoplePosition) ? $object->reservationPeoplePosition : '' }}">
                                    </div>
                                    <div class="field">
                                        <label for="">Телефон:</label>
                                        <input type="text" id="reservationPhone" name="reservationPhone" data-name="Отдел бронирования -> Телефон" value="{{ isset($object->reservationPhone) ? $object->reservationPhone : '' }}">
                                    </div>
                                    <div class="field">
                                        <label for="">Телефон для получения смс уведомления:</label>
                                        <input type="text" id="phoneSms" name="reservationPhoneSms" value="{{ isset($object->reservationPhoneSms) ? $object->reservationPhoneSms : '' }}">
                                    </div>
                                    <div class="field">
                                        <label for="">Факс:</label>
                                        <input type="text" id="reservationFax" name="reservationFax" value="{{ isset($object->reservationFax) ? $object->reservationFax : '' }}">
                                    </div>
                                    <div class="field">
                                        <label for="">Электронный адрес:</label>
                                        <input type="email" name="reservationEmail" value="{{ isset($object->reservationEmail) ? $object->reservationEmail : '' }}">
                                    </div>
                                </div>
                            </li>
                            <li class="item-info">
                                <h4>Бухгалтерия</h4>
                                <hr>
                                <div class="wrapper-field-form">
                                    <div class="field">
                                        <label for="">Ответственный бухгалтер по оплате коммисии (Ф.И.О):</label>
                                        <input type="text" name="bookkeepingPeopleFio" data-name="Бухгалтерия -> Ответственный бухгалтер" value="{{ isset($object->bookkeepingPeopleFio) ? $object->bookkeepingPeopleFio : '' }}">
                                    </div>
                                    <div class="field">
                                        <label for="">Телефон:</label>
                                        <input type="text" id="bookkeepingPhone" name="bookkeepingPhone" data-name="Бухгалтерия -> Телефон" value="{{ isset($object->bookkeepingPhone) ? $object->bookkeepingPhone : '' }}">
                                    </div>
                                    <div class="field">
                                        <label for="">Факс:</label>
                                        <input type="text" id="bookkeepingFax" name="bookkeepingFax" value="{{ isset($object->bookkeepingFax) ? $object->bookkeepingFax : '' }}">
                                    </div>
                                    <div class="field">
                                        <label for="">Электронный адрес:</label>
                                        <input type="email" name="bookkeepingEmail" value="{{ isset($object->bookkeepingEmail) ? $object->bookkeepingEmail : '' }}">
                                    </div>
                                </div>
                            </li>
                            <li class="item-info">
                                <h4>Реквизиты для оплаты за проживания</h4>
                                <hr>
                                <div class="wrapper-field-form">
                                    <div class="field">
                                        <label for="">Юридическое название отеля:</label>
                                        <input type="text" name="legalName" data-name="Юридическое название отеля" value="{{ isset($object->legalName) ? $object->legalName : '' }}">
                                    </div>
                                    <div class="field">
                                        <label for="">Форма собственности 1:</label>
                                        <select name="typeOwn1" id="" data-name="Форма собственности 1">
                                            <option value="">Выберите форму собственности</option>
                                            <option value="tov" @if(isset($object->typeOwn1) && $object->typeOwn1 == 'tov') selected  @endif>ТОВ</option>
                                            <option value="fop" @if(isset($object->typeOwn1) && $object->typeOwn1 == 'fop') selected  @endif>ФОП</option>
                                        </select>
                                    </div>
                                    <div class="field">
                                        <label for="">Форма собственности 2:</label>
                                        <select name="typeOwn2" id="">
                                            <option value="">Выберите форму собственности</option>
                                            <option value="tov" @if(isset($object->typeOwn2) && $object->typeOwn2 == 'tov') selected  @endif>ТОВ</option>
                                            <option value="fop" @if(isset($object->typeOwn2) && $object->typeOwn2 == 'fop') selected  @endif>ФОП</option>
                                        </select>
                                    </div>
                                    <div class="field">
                                        <label for="">Расчетный счет для выставления счет-фактуры:</label>
                                        <input type="text" name="checkingAccount" data-name="Расчетный счет для выставления счет-фактуры" value="{{ isset($object->checkingAccount) ? $object->checkingAccount : '' }}">
                                    </div>
                                    <div class="field">
                                        <label for="">Дополнительный № счета или карточки:</label>
                                        <input type="text" name="additionalAccount" value="{{ isset($object->additionalAccount) ? $object->additionalAccount : '' }}">
                                    </div>
                                    <h5 class="style-for-caption-textarea-address">Юридический адрес предприятия:</h5>
                                    <textarea name="legalAddress" data-name="Юридический адрес предприятия" id="" class="style-for-textarea-address" cols="30" rows="10">{{ isset($object->legalAddress) ? $object->legalAddress : '' }}</textarea>
                                    <div class="field">
                                        <label for="">ЕГРПОУ (идентификационный номер):</label>
                                        <input type="text" name="egrpou" data-name="ЕГРПОУ (идентификационный номер)" value="{{ isset($object->egrpou) ? $object->egrpou : '' }}">
                                    </div>
                                    <div class="field">
                                        <label for="">ИНН (для юридических лиц):</label>
                                        <input type="text" name="identicationCodeLegal" value="{{ isset($object->identicationCodeLegal) ? $object->identicationCodeLegal : '' }}">
                                    </div>
                                    <div class="field">
                                        <label for="">Номер свидетельства плательщика НДС:</label>
                                        <input type="text" name="numberNds" value="{{ isset($object->numberNds) ? $object->numberNds : '' }}">
                                    </div>
                                </div>
                            </li>
                        </ul>
                        {{ csrf_field() }}
                        <input type="submit" name="submitGeneralInfo" value="Внести изменения" onclick="MainFunction.checkingData(event);" class="btn-save-room">
                        </form>
                    </div>
                </div>
                @include('account.blocks.right_sidebar')
                <div class="clear"></div>
            </div>
        </div>
    </div>
</section>


@endsection