@extends('account.index')
@section('content')

    <section id="reservation">
        <div class="container">
            <div class="row">
                <div class="wrapper-reservation style-page">
                    <div class="col-lg-12">
                        <h2 class="caption-page">Бронирование <a onclick="MainFunction.excelDownload();" href="javascript:void(0);" class="btn btn-default btn-print">
                                <i class="fa fa-print"></i> Распечатать список броней</a> <a href="{{ route('calendar', $subdomain) }}" class="btn btn-primary">Календарь управления номерами</a></h2>
                        <hr>
                        <div class="data-reservation">
                            <table>
                                <tr>
                                    <td>
                                        <span>Статус</span>
                                        <select name="status" id="" onchange="MainFunction.filtersReservationCalendar();">
                                            <option value="">Выберите статус</option>
                                            <option value="3">Активна</option>
                                            
                                            <option value="2">Отменено</option>
                                        </select>
                                    </td>
                                    <td>
                                        <span>Тип брони</span>
                                        <select name="type" id="" onchange="MainFunction.filtersReservationCalendar();">
                                            <option value="">Выберите тип брони</option>
                                            <option value="1">Сайт</option>
                                            <option value="0">Менеджер</option>
                                        </select>
                                    </td>
                                    <td>
                                        <span>Предоплата</span>
                                        <select name="prepayment" id="" onchange="MainFunction.filtersReservationCalendar();">
                                            <option value="">Тип предоплаты</option>
                                            <option value="1">С предоплатой</option>
                                            <option value="0">Без предоплаты</option>
                                        </select>
                                    </td>
                                    <td>
<!--
                                       <label for="" class="checkbox-no-checked-query">
                                           <input type="checkbox"> Необработанные запросы
                                       </label>
-->
                                    </td>
                                </tr>
                            </table>
                            <div class="list-conditions">
                                <span>Бронирование:</span>
                                <ul>
                                    <li><a href="javascript:void(0);" date-value="{{ $yesterday }}" onclick="MainFunction.selectDateForReservation(this);">Вчера</a></li>
                                    <li><a href="javascript:void(0);" date-value="{{ $today }}" onclick="MainFunction.selectDateForReservation(this);">Сегодня</a></li>
                                    <li><a href="javascript:void(0);" date-value="{{ $tommorrow }}" onclick="MainFunction.selectDateForReservation(this);">Завтра</a></li>
                                    <li><a href="javascript:void(0);" date-value="" onclick="MainFunction.selectDateForReservation(this);">За все время</a></li>
                                </ul>
                                <div class="wrapper-period">
                                    <span><b>Период:</b> с <input type="text" name="dateFrom" onchange="MainFunction.filtersReservationCalendar();" id="datetimepickerReservation1" value=""> по <input type="text" value="" name="dateTo" onchange="MainFunction.filtersReservationCalendar();" id="datetimepickerReservation2">
                                    </span>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        
                    </div>
                    <div class="clear"></div>
                    <div class="list-reservation-content">
                            <table>
                                <tr class="caption-table-reservation">
                                   <th>
                                       <span>Иден. брони</span>
                                   </th>
                                    <th class="td-name">
                                        <span>Имя гостя</span>
                                    </th>
                                    <th>
                                        <span>Заезд</span>
                                    </th>
                                    <th>
                                        <span>Отьезд</span>
                                    </th>
                                    <th>
                                        <span>Название номера</span>
                                    </th>
                                    <th>
                                        <span>Контактный телефон</span>
                                    </th>
                                    <th>
                                        <span>Дата<br> бронирования</span>
                                    </th>
                                    <th>
                                        <span>Статус</span>
                                    </th>
                                    <th>
                                        <span>Итоговая<br> цена</span>
                                    </th>
                                    <th>
                                        <span>Комиссия</span>
                                    </th>
                                    <th></th>
                                </tr>

                            </table>
                        <div class="wrap-reservation">
                            @include('account.blocks.list-reservation')
                        </div>

                        <div class="wrapper-for-reloader"></div>
<!--
                            <div class="full-info-about-price">
                                <span><b>Комиссия:</b> UAH 344.41</span>
                                <span><b>Оплата:</b> UAH 2296.10</span>
                            </div>
-->
                        </div>
                        <div class="alert alert-danger messageErrorReservation display-none">
                            По выбранным фильтрам не найдено ни одной брони.
                        </div>

                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </section>


@endsection