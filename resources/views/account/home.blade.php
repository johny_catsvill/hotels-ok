@extends('account.index')
@section('content')

<section id="home">
    <div class="container">
        <div class="row">
            <div class="wrapper-home style-page">
                <div class="col-lg-9">
                    <h2>{{ $object->organizationName }}</h2>
                    <hr>
                    <div class="wrapper-tabs-global">
                       <h4>Актуальная информация</h4>
                       <div class="wrapper-tabs">
                            <div class="link-tab">
                                <ul>
                                    <li><a href="javascript:void(0);" onclick="MainFunction.changeDateInfo(this, '{{ $yesterday }}');">Вчера</a></li>
                                    <li><a class="active" href="javascript:void(0);" onclick="MainFunction.changeDateInfo(this, '{{ $currentDate }}');">Сегодня</a></li>
                                    <li><a href="javascript:void(0);" onclick="MainFunction.changeDateInfo(this, '{{ $tommorrow }}');">Завтра</a></li>
                                </ul>
                                <div class="clear"></div>
                            </div>
                            <div class="js-tabs">
                                @include('account.blocks.info-about-object')
                            </div>
                            
                        </div>    
                    </div>
                    <div class="clear"></div>
                </div>
                @include('account.blocks.right_sidebar')
                <div class="clear"></div>
            </div>
        </div>
    </div>
</section>

@endsection