@extends('account.index')
@section('content')

<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 style-page padding-0px">
                <h2 style="padding-left:10px;">Детали бронирования</h2>
                <hr>
                @include('auth.success_errors_block')
                <div class="col-lg-9">
                    @if($reservation)
                    <div class="wrapper-info-reservation">
                        <div class="col-lg-3">
                            <div class="info-about-reservation-date">
                               <ul>
                                  <li>
                                       <span class="caption">Название обьекта:</span>
                                       <span class="style-for-value-reservation">{{ $reservation->objectName }}</span>
                                   </li>
                                  <li>
                                       <span class="caption">Тип обьекта:</span>
                                       <span class="style-for-value-reservation">{{ $reservation->categoryName }}</span>
                                   </li>
                                   <li>
                                       <span class="caption">Заезд:</span>
<!--                                       <span class="style-for-value-reservation">Вт, 20 Июн 2017</span>-->
                                       <span class="style-for-value-reservation">{{ date('d.m.Y', $reservation->startReservationDate) }}</span>
                                   </li>
                                   <li>
                                       <span class="caption">Отьезд:</span>
                                       <span class="style-for-value-reservation">{{ date('d.m.Y', $reservation->endReservationDate) }}</span>
                                   </li>
                                   <li>
                                       <span class="caption">Общее количество гостей:</span>
                                       <span class="style-for-value-reservation">{{ $reservation->countPeople }}</span>
                                   </li>
                                   <li>
                                       <span class="caption">Общее количество номеров:</span>
                                       <span class="style-for-value-reservation">{{ $reservation->countRoom }}</span>
                                   </li>
                                   <li>
                                       <span class="caption">Общая стоимость:</span>
                                       <span class="style-for-value-reservation">UAH {{ $reservation->price }}</span>
                                   </li>
                                   <li>
                                       <span class="caption">Предоплата:</span>
                                       <span class="style-for-value-reservation">UAH {{ $reservation->prePayment }}</span>
                                   </li>
                               </ul>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="main-info-about-reservation">
                                <span class="caption">Имя гостя:</span>
                                <span class="style-for-value-reservation">{{ $reservation->guests }}</span>
                                <div class="wrapper-email-and-telefon">
                                    <a href="javascript:void(0);">{{ $reservation->visitorEmail }}</a>
                                    <a href="javascript:void(0);">+{{ $reservation->visitorPhone }}</a>
                                </div>
                                <div class="col-lg-12 padding-0px">
                                    <div class="main-info">
                                        <ul>
                                            <li>
                                                <span class="caption">Кем забронировано:</span>
                                                <span class="style-for-value-reservation">
                                                {{ $reservation->name }}</span>
                                            </li>
                                            <li>
                                                <span class="caption">Идентификационный номер бронирования:</span>
                                                <span class="style-for-value-reservation">{{ $reservation->id }}</span>
                                            </li>
                                            <li>
                                                <span class="caption">Дата бронирования:</span>
                                                <span class="style-for-value-reservation">{{ date('d.m.Y', $reservation->creationDate) }}</span>
                                            </li>
                                            <li>
                                                <span class="caption">Сумма, облагающаяся коммисией:</span>
                                                <span class="style-for-value-reservation">UAH {{ $reservation->price }}</span>
                                            </li>
                                            <li>
                                                <span class="caption">Коммисионное вознаграждение:</span>
                                                <span class="style-for-value-reservation">UAH 181, 20</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div class="col-lg-3"></div>
                        <div class="clear"></div>
                    </div>
                    <div class="rooms-by-reservation">
                        <ul>
                           @foreach($roomsReservation as $room)
                            <li>
                                <div class="room-reservation">
                                    <div class="caption-room">
                                        <span class=><b>Номер {{ $loop->iteration }}</b> - <span class="name-room">{{ $room->name }}</span></span>
                                        <ul class="date-reservation-room">
                                            <li><span><i class="fa fa-sign-in" aria-hidden="true"></i>{{ date('d.m.Y', $reservation->startReservationDate) }}</span></li>
                                            <li><span><i class="fa fa-sign-out" aria-hidden="true"></i>{{ date('d.m.Y', $reservation->endReservationDate) }}</span></li>
                                        </ul>
                                        <div class="price-for-that-room">
                                            <b>UAH {{ $room->price }}</b>
                                        </div>
                                    </div>
                                    <div class="main-info-room-reservation">
                                        <div class="col-lg-4">
                                            <div class="mini-info-room">
                                                <div class="main-info">
                                                    <ul>
                                                        <li>
                                                            <span class="caption">Имя гостя:</span>
                                                            <span class="style-for-value-reservation">{{ $room->peopleFio }}</span>
                                                        </li>
                                                        <li>
                                                            <span class="caption">Макс. вместимость:</span>
                                                            <span class="style-for-value-reservation">{{ $room->amountPerson }} гостя</span>
                                                        </li>
                                                        <li>
                                                            <span class="caption">План питания:</span>
                                                            <span class="style-for-value-reservation">Питание не входит в цену данного номера.</span>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="img-room-reservation">
                                                    <img src="/images/cheap-flights-to-madrid.jpg" alt="">
                                                </div>
                                            </div>
                                        </div>                                        
                                        <div class="col-lg-8">
                                            <div class="info-about-price-this-room">
                                                <ul>
                                                    <li>
                                                        <div class="left-sidebar-room sidebars-style">
                                                            <span><b>Дата</b></span>
                                                        </div>
                                                        <div class="right-sidebar-room sidebars-style">
                                                            <span><b>Стоимость</b></span>
                                                        </div>
                                                        <div class="clear"></div>
                                                    </li>
                                                    <li>
                                                        <div class="left-sidebar-room sidebars-style">
                                                            <span>{{ date('d.m.Y', $reservation->startReservationDate). ' '. date('d.m.Y', $reservation->endReservationDate)}}</span>
                                                        </div>
                                                        <div class="right-sidebar-room sidebars-style">
                                                            <span>UAH {{ $room->priceForDay }}</span>
                                                        </div>
                                                        <div class="clear"></div>
                                                    </li>
                                                    <li>
                                                        <div class="left-sidebar-room sidebars-style">
                                                            <span>Городской налог</span>
                                                        </div>
                                                        <div class="right-sidebar-room sidebars-style">
                                                            <span>UAH 5.07</span>
                                                        </div>
                                                        <div class="clear"></div>
                                                    </li>
                                                    <li>
                                                        <div class="left-sidebar-room sidebars-style">
                                                            <span><b>Общая стоимость номера</b></span>
                                                        </div>
                                                        <div class="right-sidebar-room sidebars-style">
                                                            <span>UAH {{ $room->price }}</span>
                                                        </div>
                                                        <div class="clear"></div>
                                                    </li>
                                                </ul>
                                                <span class="info-nds">В стоимость включено: НДС в размере 20%</span>
                                            </div>
                                        </div> 
                                        <div class="clear"></div>                                       
                                    </div>
                                </div>
                            </li>
                           @endforeach
                        </ul>
                    </div>
                @endif
                <div class="clear"></div>
                </div>
                <div class="col-lg-3">
                    <a href="{{ route('reservations', $subdomain) }}" class="btn btn-primary">Вернуться к списку броней</a>
                </div>    
            </div>
            
        </div>
    </div>
</section>

@endsection