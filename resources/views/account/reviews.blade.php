@extends('account.index')
@section('content')


<section id="reviews">
    <div class="container">
        <div class="row">
            <div class="wrapper-reviews style-page">
                <div class="col-lg-8">
                    <h2>Отзывы гостей</h2>
                    <hr>
                    @if(count($listReviews) > 0)
                    <div class="pagination">
                      <ul>
                        {!! $pagination !!}
                      </ul>
                      <select name="sort" onchange="MainFunction.sortReview(this);" id="" class="btn btn-default style-for-select-allreviews">
                          <option value="all">Все отзывы</option>
                          <option value="new">Новые</option>
                          <option value="positive">Позитивные</option>
                          <option value="negative">Негативные</option>
                      </select>
                    </div>
                    <div class="clear"></div>
                    
                    <div class="list-reviews">
                        <ul>
                            @foreach($listReviews as $review)
                                <li class="item-review">
                                    <div class="review-caption">
                                        <div class="raiting">
                                            <span class="number-raiting">{{round($review->avgValue, 1)}}</span> <br>
                                        </div>
                                        <span class="date-review-and-number-reservation">
                                            <span>{{ $funcModel->translateToDate($review->creationDate) }}</span> Номер бронирования: <a href="">{{$review->reservationId}}</a>
                                        </span><br>
                                        <a href="" class="style-link-object">Ваша страница отзывов на hotels-ok.com</a>
                                        <div class="clear"></div>
                                        <span class="name-user-review">{{ $review->authorName }}
                                         @if($review->countryName)
                                         (страна: {{$review->countryName}}) </span>
                                         @endif
                                         <br><span class="name-type-user">{{$review->nameType}}</span>
                                    </div>
                                    <div class="characteries-review">
                                        <ul>
                                            <li>
                                                <span class="name-charactery-review">Персонал</span>
                                                <span class="number-charactery-review">{{ $review->employeeValue }}</span>
                                                <div class="clear"></div>
                                                <div class="style-progress-bar">
                                                    <div class="style-progress" style="width:{{ $funcModel->countPercent($review->employeeValue, 10) }}%;"></div>
                                                </div>
                                            </li>
                                            <li>
                                                <span class="name-charactery-review">Сервис</span>
                                                <span class="number-charactery-review">{{ $review->serviceValue }}</span>
                                                <div class="clear"></div>
                                                <div class="style-progress-bar">
                                                    <div class="style-progress" style="width:{{ $funcModel->countPercent($review->serviceValue, 10) }}%;"></div>
                                                </div>
                                            </li>
                                            <li>
                                                <span class="name-charactery-review">Чистота</span>
                                                <span class="number-charactery-review">{{ $review->cleanValue }}</span>
                                                <div class="clear"></div>
                                                <div class="style-progress-bar">
                                                    <div class="style-progress" style="width:{{ $funcModel->countPercent($review->cleanValue, 10) }}%;"></div>
                                                </div>
                                            </li>
                                            <li>
                                                <span class="name-charactery-review">Комфорт</span>
                                                <span class="number-charactery-review">{{ $review->comfortValue }}</span>
                                                <div class="clear"></div>
                                                <div class="style-progress-bar">
                                                    <div class="style-progress" style="width:{{$funcModel->countPercent($review->comfortValue, 10)}}%;"></div>
                                                </div>
                                            </li>
                                            <li>
                                                <span class="name-charactery-review">Местоположение</span>
                                                <span class="number-charactery-review">{{ $review->locationValue }}</span>
                                                <div class="clear"></div>
                                                <div class="style-progress-bar">
                                                    <div class="style-progress" style="width:{{ $funcModel->countPercent($review->locationValue, 10) }}%;"></div>
                                                </div>
                                            </li>
                                            <li>
                                                <span class="name-charactery-review">Соотношение цена / качество</span>
                                                <span class="number-charactery-review">{{ $review->qualityPriceValue }}</span>
                                                <div class="clear"></div>
                                                <div class="style-progress-bar">
                                                    <div class="style-progress" style="width:{{ $funcModel->countPercent($review->qualityPriceValue, 10) }}%;"></div>
                                                </div>
                                            </li>
                                        </ul>
                                            <div class="clear"></div>
</div>
                                    <div class="text-this-user-about-object">
                                                            @if(isset($review->positiveText) && $review->positiveText)
                                                                <div class="positive-text">
                                                                    <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                                    <p>{{ $review->positiveText }}</p>
                                                                </div>
                                                            @endif
                                                            @if(isset($review->negativeText) && $review->negativeText)
                                                                <div class="negative-text">
                                                                    <i class="fa fa-minus-circle" aria-hidden="true"></i>
                                                                    <p>{{ $review->negativeText }}</p>
                                                                </div>
                                                            @endif</div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="pagination">
                      <ul>
                      {!! $pagination !!}
                      </ul>
                    </div>
                    @endif
                </div>
                <div class="col-lg-4">
                    <div class="right-sidebar">
                        <div class="wrapper-calendar">
                            <i class="fa fa-calendar" aria-hidden="true"></i>
                            <a href="{{ route('calendar', $subdomain) }}">Открыть общий календарь управления номерами</a>
                        </div>
                        <div class="wrapper-help">
                            <span class="caption"><i class="fa fa-share" aria-hidden="true"></i> Поможем. Подскажем.</span>
                            <h5>Отдел по работе с отелями:</h5>
                            <div class="list-contacts">
                                <ul>
                                    <li><i class="fa fa-phone" aria-hidden="true"></i>
                                        <ul>
                                            <li>067 433 00 14</li>
                                            <li>063 610 85 13</li>
                                            <li>067 236 22 58</li>
                                        </ul>
                                    </li>
                                    <li>
                                       <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                        <ul>
                                            <li>hotelsok25@gmail.com</li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="avg-reviews-block">
                            <h4>Ваше оценка по отзывам</h4>
                            <span><span class="count-reviews-object">{{ $averageRaiting }}</span> на основании {{ $countReviews }} отзывов</span>
                            
                            <div class="list-avg-value-reviews-object">
                                <ul>
                                    <li>
                                        <span>Персонал</span>
                                        <div class="value-avg-reviews">
                                            <div class="value-avg">{{ round($totalValueByCategoryForObject[0]->employeeValue,1) }}</div>
                                            <div class="line-status-value">
                                                <div class="line-progress" style="width:{{ $funcModel->countPercent($totalValueByCategoryForObject[0]->employeeValue, 10) }}%;"></div>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <span>Комфорт</span>
                                        <div class="value-avg-reviews">
                                            <div class="value-avg">{{ round($totalValueByCategoryForObject[0]->comfortValue,1) }}</div>
                                            <div class="line-status-value">
                                                <div class="line-progress"  style="width:{{ $funcModel->countPercent($totalValueByCategoryForObject[0]->comfortValue, 10) }}%;"></div>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <span>Сервис</span>
                                        <div class="value-avg-reviews">
                                            <div class="value-avg">{{ round($totalValueByCategoryForObject[0]->serviceValue,1) }}</div>
                                            <div class="line-status-value">
                                                <div class="line-progress" style="width:{{ $funcModel->countPercent($totalValueByCategoryForObject[0]->serviceValue, 10) }}%;"></div>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <span>Чистота</span>
                                        <div class="value-avg-reviews">
                                            <div class="value-avg">{{ round($totalValueByCategoryForObject[0]->cleanValue,1) }}</div>
                                            <div class="line-status-value">
                                                <div class="line-progress" style="width:{{ $funcModel->countPercent($totalValueByCategoryForObject[0]->cleanValue, 10) }}%;"></div>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <span>Цена / Качество</span>
                                        <div class="value-avg-reviews">
                                            <div class="value-avg">{{ round($totalValueByCategoryForObject[0]->priceValue,1) }}</div>
                                            <div class="line-status-value">
                                                <div class="line-progress" style="width:{{ $funcModel->countPercent($totalValueByCategoryForObject[0]->priceValue, 10) }}%;"></div>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <span>Местоположение</span>
                                        <div class="value-avg-reviews">
                                            <div class="value-avg">{{ round($totalValueByCategoryForObject[0]->locationValue,1) }}</div>
                                            <div class="line-status-value">
                                                <div class="line-progress" style="width:{{ $funcModel->countPercent($totalValueByCategoryForObject[0]->locationValue, 10) }}%;"></div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </div>
</section>

@endsection