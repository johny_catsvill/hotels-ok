@extends('account.index')
@section('content')

<section id="settings">
    <div class="container">
        <div class="row">
            <div class="wrapper-settings style-page">
               <div class="col-lg-9">
                   <h2>Настройки</h2>
                   <hr>
                   
                   @include('auth.success_errors_block')
                   
                   
                    <div>
                      <!-- Навигация -->
                      <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Общие настройки</a></li>
                        <li class=""><a href="#password" aria-controls="password" role="tab" data-toggle="tab">Изменить пароль</a></li>
                      </ul>
                      <!-- Содержимое вкладок -->
                      <div class="tab-content first-tab-content">
                        <div role="tabpanel" class="tab-pane active" id="home">
                            <form action="{{ route('settings', $subdomain) }}" method="post">
                                <div class="field">
                                    <label for="">Телефон</label>
                                    <input type="text" name="phone" value="{{ $infoUser->phone }}">
                                </div>
                                <div class="field">
                                    <label for="">Email</label>
                                    <input type="text" name="email" value="{{ $infoUser->email }}">
                                </div>
                                {{ csrf_field() }}
                                <input type="submit" name="submit" value="Сохранить" class="btn-save-room">
                           </form>
                        </div>
                        <div role="tabpanel" class="tab-pane " id="password">
                            <form action="{{ route('change-password', $subdomain) }}" method="post">
                                <div class="field">
                                    <label for="">Пароль</label>
                                    <input type="password" name="password">
                                </div>
                                <div class="field">
                                    <label for="">Повторите пароль</label>
                                    <input type="password" name="repeatPassword">
                                </div>
                                {{ csrf_field() }}
                                <input type="submit" name="submit" value="Сохранить" class="btn-save-room">
                            </form>
                        </div>
                      </div>
</div>
                    
                    
                   
               </div>
                @include('account.blocks.right_sidebar')
            </div>
        </div>
    </div>
</section>


@endsection