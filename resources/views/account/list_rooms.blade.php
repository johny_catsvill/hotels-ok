@extends('account.index')
@section('content')

<section id="listRooms">
    <div class="container">
        <div class="row">
            <div class="list-rooms style-page">
                <div class="col-lg-9">
                    <h2 class="caption-page">Список номеров</h2>
                    <hr>
                    @include('auth.success_errors_block')
                    <a href="{{ route('edit-room', $subdomain) }}" class="btn btn-primary"><i class="fa fa-plus"></i> Добавить новый номер</a>
                    @if($listRoomsObject)
                        <div class="wrapper-list-rooms">
                        <ul>
                           @foreach($listRoomsObject as $room)
                            <li class="room-object">
                               <a href="{{ route('edit-room', ['domain' => $subdomain, 'id' => $room->id]) }}">
                                    <div class="img-room" style="background:url(../{{$room->img}});">
<!--                                        <img src="/{{ $room->img }}" alt="">-->
                                        <div class="wrapper-caption">
                                           <div class="overlay-caption"></div>
                                            <span>{{ $room->name }}</span>
                                        </div>
                                    </div>
                                </a> 
                                <div class="content-room">
                                    <span>Стандартная вместимость: <b>{{ $room->amountPerson }} человека</b></span>
                                    <span>Количество номеров в наличии: <b>{{ $room->countRoom }} номера</b></span>
                                    <a href="{{ route('edit-room', ['domain' => $subdomain, 'id' => $room->id]) }}" class="btn btn-default">Редактировать</a>
                                    <a href="{{ route('delete-room', ['domain' => $subdomain, 'id' => $room->id]) }}" class="btn btn-default">Удалить</a>
                                    <a href="{{ route('calendar', ['domain' => $subdomain, 'id' => $room->id]) }}" class="btn btn-primary"><i class="fa fa-calendar"></i>  Установить цены</a>
                                    <a href="{{ route('activation-sale-room', ['domain' => $subdomain, 'id' => $room->id]) }}" class="btn btn-danger"> <!-- Единица включена , ноль выключена -->
                                        @if($room->activeSale == 1)
                                            Остановить продажу
                                        @else
                                            Включить продажу
                                        @endif
                                    </a>
                                </div>   
                            </li>
                            @endforeach
                        </ul>
                    </div>
                    @else
                        <div class="alert alert-success margin-top-20px">
                            У данного обьекта отсутствуют номера!
                        </div>
                    @endif
                </div>
                @include('account.blocks.right_sidebar')
                <div class="clear"></div>
            </div>
        </div>
    </div>
</section>

@endsection

