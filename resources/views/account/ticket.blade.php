@extends('account.index')
@section('content')

<section id="settings">
    <div class="container">
        <div class="row">
            <div class="wrapper-settings style-page">
               <div class="col-lg-9">
                   <h2>Помощь и поддержка</h2>
                   <hr>    
                   <a href="{{ route('help', $subdomain) }}" class="btn btn-primary" style="margin-bottom:15px;">Вернуться к списку тикетов</a>
                   <div class="create-ticket-form">
                      <h2 class="theme-style">Тема: {{ $ticket->themeTicket }}</h2>
                      <span class="ticket-id">id: {{ $ticket->id }}</span>
                      <span class="ticket-info-manager">Тикет передан в отдел технической поддержки <br>Время работы отдела: Круглосуточно</span>
                       <form action="{{ route('ticket', ['domain' => $subdomain, 'id' => $ticket->id]) }}" method="post">
                           <div class="field">
                               <label for="">Сообщение:</label>
                               <textarea name="message" id="" cols="30" rows="10"></textarea>
                           </div>
                           {{ csrf_field() }}
                           <input type="submit" name="submit" value="Отправить" class="btn btn-primary">
                           <div class="clear"></div>
                       </form>
                   </div>
                   <div class="clear"></div>
                   <div class="list-messages">
                       <table>
                          @foreach($listMessages as $message)
                           <tr @if($message->managerId) style="background-color:#EFF3F6;"  @endif>
                               <td  style="width:20%; padding:10px 0;">
                                  @if($message->managerId)
                                   <img src="/images/woman-with-headset.png" alt="" class="img-author">
                                   @else
                                   <img src="/images/user-picture.png" alt="" class="img-author">
                                   @endif
                               </td>
                               @if($message->managerId)
                               <td style="width:80%; padding:10px 0;"><b>Андрей Александрович</b> <br> <span class="mini-info-message">Служба технической поддержки</span></td>
                               @else
                               <td style="width:80%; padding:10px 0;"><b>Вы</b> <br> <span class="mini-info-message">Отель Марк</span></td>
                               @endif
                           </tr>
                           <tr @if($message->managerId) style="background-color:#EFF3F6;"  @endif>
                               <td style="padding:10px 0;"><span class="time-message">{{ date('d.m.Y H:i', $message->creationDate) }}</span></td>
                               <td><p class="text-message-style">{{ $message->message }}</p></td>
                           </tr>
                           @endforeach
                       </table>
                   </div>
                   <p style="margin-top:15px;">В любое время Вы также можете связаться с нами следующими способами:</p>
                   <ul class="list-telefons-supports">
                       <li><b>Email:</b> hotelsok25@gmail.com</li>
                       <li><b>Телефон:</b>0636108513</li>
<!--                       <li>0636108513</li>-->
                   </ul>
               </div>
                @include('account.blocks.right_sidebar')
                <div class="clear"></div>
            </div>
        </div>
    </div>
</section>


@endsection