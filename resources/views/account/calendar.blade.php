@extends('account.index')
@section('content')
<section id="calendar" onload="">
    <div class="container">
        <div class="row">
            <div class="wrapper-calendars style-page">
                <div class="col-lg-12">
                    <h2>Календарь бронирования номеров</h2>
                    <hr>
                    <div class="alert alert-danger errors-message">
                        
                    </div>
                    <div class="that-calendar">
                        <select class="selectpicker" name="roomId" id="roomId" onchange="Calendar.getCalendarByRoomAboutDate();">
                            @foreach($roomsList as $room)
                                <option value="{{ $room->roomId }}">{{ $room->roomName }}</option>
                            @endforeach
                        </select>
                        <select name="year" id="selectedYear" class="selectpicker year-select-style" onchange="Calendar.getCalendarByRoomAboutDate();">
                            @foreach($listYears as $year)
                                <option value="{{ $year }}" @if($year == $needYear) selected @endif>{{ $year }}</option>
                            @endforeach
                        </select>
                        <div class="wrapper-js-calendar">
                            @include('account.blocks.calendar-block')
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </div>
</section>
<div id="overlay"></div>
<div class="wrapper-popup-calendar">
    
</div>

@endsection