
    @foreach($objects as $object)                   

                       <div class="object">
                        <div class="wrapper-img-search-object">
                            <img src="/images/hotels/{{$object->id}}-{{$object->subdomain}}/{{$object->image}}" alt="">
                        </div>
                        <div class="mini-info-object">
                            <span class="caption-name-object">{{ $object->category->name.' '.$object->name }}
                                @for($i = 0; $i < 5; $i++)
                                    <i class="fa fa-star @if($i <= $object->stars) active-star @endif"></i>
                                @endfor
                            </span>
                            <span class="location-object">г. {{ $object->city->name }}, ул. {{ $object->address }}, {{ mb_substr($object->city->region->name, 0, -2).'ий' }} регион</span>
                        </div>
                        <div class="telefons-object">
                            <ul>
                                <li>{{ $object->reservationPhone }}</li>
                            </ul>
                        </div>
<!--                        <a href="" class="btn-reservation-object">Забронировать</a>-->
                        <div class="clear"></div>
                        <div class="search-objects-rooms">
                            <ul>
                                <li>
                                    <div class="rooms-that-object">
                                        <table>
                                           <tr>
                                               <td>Название номера</td>
                                               <td>Сколько человек?</td>
                                               <td>Что есть в номере?</td>
                                               <td>Доступных</td>
                                               <td>Скидка</td>
                                               <td>Цена за {{ $period }} ноч.</td>
                                               <td></td>
                                           </tr>
                                           @foreach($object->rooms as $room)
                                               @php
                                                    $currentRoom = \App\Models\Room::find($room->typeRoomId);
                                               @endphp
                                            <tr>
                                                <td>{{ $room->name }}</td>
                                                <td>{{ $room->amountPerson }} чел.</td>
                                                <td>
                                                    <a href="javascript:void(0);" onclick="MainFunction.showPopupPluses(this);">Что в номере?</a>
                                                    <ul class="hide">
                                                        @foreach($currentRoom->pluses as $plus)
                                                            <li>{{ $plus->name }}</li>
                                                        @endforeach
                                                    </ul>
                                                </td> <!-- показывать попап плюсов, сделать прелоадер, показывать по 10, добавить сортировку, придумать как показывать количество доступных, возможно даже какой то мини календарь -->
                                                <td>{{ $room->countFreeRoom }}</td>
                                                <td>Discount</td>
                                                <td>{{ $room->price }}</td>
                                                <td><a href="{{ route('reservation-edit', ['objectId' => $object->id, 'dateFrom' => $dateFrom, 'dateTo' => $dateTo]) }}">Создать бронь</a></td>
                                            </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>

    @endforeach               