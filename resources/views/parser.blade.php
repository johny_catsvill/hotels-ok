<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{!! csrf_token() !!}" />
    <link rel="stylesheet" href="/css/font-awesome.css">
    <title>Document</title>
</head>
<body>
    <span>Парсинг...</span>
    
    <div class="field">
        <label for="">Добавить ссылку</label>
        <input type="text" name="link">
    </div>
    
    <a href="javascript:void(0);" onclick="Function.addLinkObject();">Добавить ссылку</a>
    
    <ul class="list-url">
        
    </ul>
    
    <ul class="list-emails"></ul>
    
    
    <a href="javascript:void(0);" class="btn-parser" onclick="Function.parserObjects(this);">Спарсить</a>
    <span class="loading-parser"></span>
    <script src="/js/jquery-3.1.1.min.js"></script>
    <script>
        var Function = {
            parserObjects: function(){
                var btnParser = $('.btn-parser').clone();
                $('.btn-parser').remove();
                var arrayLinks = [];
                var links = $('.list-url').find('li');
                $.each(links, function(indx, element) {
                    arrayLinks.push($(element).text());    
                });
                
                
                $.ajax({
                    type: 'post',
                    url: '/parser',
                    data:{'_token': $('meta[name="csrf-token"]').attr('content'), links:arrayLinks},
                    beforeSend: function() {
                        $('.loading-parser').append('<i class="fa fa-spinner fa-spin" aria-hidden="true"></i>');
                    },
                    success:function(result) {
                        $('.loading-parser').text('Спарсил');
                        $('.list-url').empty();
                        $.each(result, function(indx, element){
                            $('.list-emails').append('<li>'+ element +'</li>');
                        });
                        
                        
                          
                    }
                });
                
            },
            
            addLinkObject:function() {
                var val = $('input[name="link"]').val();
                $('.list-url').append('<li>'+ val +'</li>');
                $('input[name="link"]').val('');
            }
        };
    </script>
</body>
</html>