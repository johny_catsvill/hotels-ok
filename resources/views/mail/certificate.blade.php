<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Акт заселения № {{ $reservation->id }}</title>
</head>
<style>

    html, body {
        margin:0
    }

    #wrapper {
        max-width: 800px;
        min-width: 800px;
    }
    

    
    .header-bg img {
        width: 100%;
        height: 100%;
    }
    
    .content-top table{
        width: 100%;
    }
    
    .logo {
        width: 400px;
        padding: 7px;
        display: inline-block;
    }
    
    .logo img {
        width: 100%;
        height: 100%;
    }
    
    .act {
        text-align: right;
        padding-right: 10px;
    }
    
    .act h4 {
        font-weight: normal;
        margin: 0;
        font-size: 22px;
    }
    
    .act span {
        font-size: 22px;
        padding-top: 5px;
        display: block;
    }
    
    ul {
        list-style: none;
        padding-left: 10px;
    }
    
    .content-middle {
        background-color: #F8F9FA;
    }
    
    .content-middle p {
        font-size: 22px;
        padding-top: 5px;
    }
    
    .caption-act {
        text-align: center;
        padding: 2px;
        font-size: 18px;
        display: block;
    }
    
    .steps {
        text-align: center;
    }
    
    .steps li {
        display: inline-block;
        width: 17.5%;
        margin: 0 6px;
        text-align: center;
        vertical-align: middle;
        line-height: 14px;
        height: 200px;
        background-color: white;
        
    }
    
    .steps li strong {
        font-size: 12px;
        color: white;
        background-color: #769dd5;
        height: 80px;
        display: block;
        padding: 20px 5px 0;
        line-height: 19px;
        text-transform: uppercase;
    }
    
    .steps li span {
        padding: 15px 5px 10px;
        text-align: center;
        display: block;
        font-size: 17px;
        line-height: 20px;
    }

    .mini-info-hotel td {
        padding: 5px 5px 10px;
    }

    .mini-info-hotel tr td:last-child {
        margin-left: 15px;
    }

    .info-project li {
        display: inline-block;
        width:40%;
        vertical-align: top;
    }

    .info-project li:first-child {
        margin-right: 19%;
    }
    
</style>
<body>
    <div id="wrapper">
        {{--<div class="header-bg" style="background: url(/images/bg-mail.jpg);"></div>--}}
        <div class="content-top">
            <table>
                <tr>
                    <td>
                        <div class="logo">
                            <img src="{{ public_path() }}/images/logo-our-admin.png" alt="">
                        </div>
                    </td>
                    <td>
                        <div class="act">
                            <h4><strong>Акт заселения</strong> (ваучер)</h4>
                            <span><strong>№ {{ $reservation->id }}</strong></span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                       <ul>
                            <li>Номер заказа: <strong>{{ $reservation->id }}</strong></li>
                            <li>Дата заказа: <strong>{{ date('d.m.Y',$reservation->creationDate) }}</strong></li>
                        </ul>
                    </td>
                    <td style="text-align:right; padding-right:10px;">
                        <ul>
                            <li>Имя гостя: <br> <strong>{{ $reservation->name }}</strong></li>
                        </ul>
                    </td>
                </tr>
            </table>
        </div>
       <div class="content-middle">
            <p style="text-align:center;"><strong>Уважаемый(ая), {{ $reservation->name }}!</strong></p>
            <span class="caption-act">Это ваучер для Вашего заселения в отель. Пожалуйста, распечатайте и покажите его в отеле.</span>
            <ul class="steps">
                <li><strong>Сведения о бронировании</strong>
                    <span>4 ночи, 1 номер, 2 гостя</span>
                </li>
                <li><strong>Регистрация заезда</strong>
                    <span>{{ date('d.m.Y', $reservation->startReservationDate) }} <br> День(14:00 - 18:00)</span>
                </li>
                <li><strong>Регистрация выезда</strong>
                    <span>{{ date('d.m.Y', $reservation->endReservationDate) }} <br> До 12:00</span>
                </li>
                <li><strong>Общая стоимость номера</strong>
                    <span>{{ $reservation->price }} грн</span>
                </li>
                {{--<li><strong>Предоплата на Hotels-ok (ЧП БАС)</strong>--}}
                    {{--<span>1440,00 грн</span>--}}
                {{--</li>--}}
                <li><strong>К оплате в отеле</strong>
                    <span>{{ $reservation->price }} грн.</span>
                </li>
            </ul>
            <p style="text-align:center; font-size:18px; padding-bottom: 0px;"><strong>Бесплатная отмена бронирования невозможна. Стоимость отмены бронирования </strong>{{ $reservation->price }} грн.</p>
           <ul>
               <li style="display:inline-block; width: 49%; vertical-align:top;">
                   <div class="img-hotel">
                       <img style="display:block; margin-bottom: 40px; width:100%;" src="{{ public_path() }}/images/hotels/2-gostinica-kompas-otel-herson/1511175245oPfr.jpg" alt="">
                       {{--<strong style="display: block; text-transform: uppercase; font-size: 15px;">Важные примечания: 100% предоплата. Отмена невозможна.</strong>--}}
                       {{--<span style="display:block;">Вы гарантировали бронирование полной оплатой за проживание через  HOTELS-OK.COM--}}
{{--Вам не нужно делать доплату в отеле за проживание.</span>--}}
                   </div>
               </li>
               <li style="display:inline-block; width:48%; vertical-align: top;">
                   <div class="description-hotel" style="background-color: white; min-height: 250px; margin-left: 20px;">
                       <span style="display:block; font-weight:bold; text-align: center; font-size: 18px; padding-top: 10px; padding-bottom: 10px;">{{ $reservation->object->name }} <span style="font-weight:normal;">({{ $reservation->object->objectCategory->name }})</span></span>
                       <table class="mini-info-hotel">
                           <tr>
                               <td><img style="margin-left:20px; height: 30%; display: block;" src="{{ public_path() }}/images/address-mail.png" alt=""></td>
                               <td><b>Адрес:</b> {{ $reservation->object->address }}</td>
                           </tr>
                           <tr>
                               <td><img style="margin-left:20px; height: 30%;" src="{{ public_path() }}/images/phone-mail.png" alt=""></td>
                               <td><b>Телефон:</b> (067) 728 47 88</td>
                           </tr>
                           
                           <tr>
                               <td><img style="margin-left:20px; height: 30%;" src="{{ public_path() }}/images/email-mail.png" alt=""></td>
                               <td><b>Email:</b> krasa21333@i.ua</td>
                           </tr>
                           {{--<tr>--}}
                               {{--<td style="vertical-align: top;"><img style="margin-left:20px; height: 30%;" src="{{ public_path() }}/images/location.png" alt=""></td>--}}
                               {{--<td><b>Местоположение:</b>  Из Львова, Ивано-Франковска или--}}
                                   {{--Коломыи до Яремче поездом или автобусом. Далее--}}
                                   {{--на такси или маршруткой до ретсорана “Зелёный гай”. Из Львова,--}}
                                   {{--Ивано-Франковска или Коломыи до Яремче поездом или--}}
                                   {{--автобусом. Далее на такси или маршруткой до ретсорана--}}
                                   {{--“Зелёный гай” Коломыи до Яремче поездом или--}}
                                   {{--автобусом. </td>--}}
                           {{--</tr>--}}
                           <tr>
                               <td><img style="margin-left:20px; height: 30%;" src="{{ public_path() }}/images/gps-mail.png" alt=""></td>
                               <td><b>GPS координаты:</b> {{ $reservation->object->latitudeGps.' '. $reservation->object->longitudeGps }}</td>
                           </tr>
                       </table>
                   </div>
               </li>
           </ul>
            <ul class="info-project">
                <li>
                    <span>Вы всегда сможете изменить или отменить бронирование просто по телефону!
В случае необходимости, мы поможем Вам с поселением в отель.
Работаем круглосуточно: (067) 510 15 04, (044) 364 20 00, 0 (800) 210 017</span>
                </li>
                <li>
                    <span><b>Руководитель отдела бронирования:</b> Татьяна Твердохлебова (096) 340 73 41</span>
                    <span><b>Генеральный директор  HOTELS-OK:</b>  Игорь Довбня (067) 430 42 38</span>
                </li>
            </ul>
           <p style="text-align:center;"><strong>Желаем Вам приятного пребывания в отеле! <br> Ваш  HOTELS-OK</strong></p>
       </div>
    </div>
</body>
</html>