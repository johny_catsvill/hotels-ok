<!-- Inliner Build Version 4380b7741bb759d6cb997545f3add21ad48f010b -->
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta charset="UTF-8">
<title>Document</title>
</head>
<body>
    
    <div class="email-container" style="max-width: 600px; width: 600px; min-height: 300px; border-radius: 8px; text-align: center; color: #313a45; box-shadow: 1px 2px 5px rgba(1,1,1,0.16); font-family: sans-serif; background: white; margin: auto;" align="center">
        <div class="email-header" style="width: 100%; color: white; border-radius: 5px 5px 0 0; background: #313945; padding: 15px 0;">
            <span>hotels-ok.com</span>
        </div>
        <div class="email-container-text" style="font-size: 28px; line-height: 30px; padding-top: 40px; border-bottom-color: #ccc; border-bottom-width: 1px; border-bottom-style: solid; padding-bottom: 50px;">
            <span class="email-welcome-text" style="display: block; padding-bottom: 30px;"><b>Здраствуйте</b><br>{{ $email }}</span>
            <span class="email-thanks" style="display: block; padding-bottom: 30px;">Спасибо, что пользуетесь сервисом бронирования отелей <br> hotels-ok.com!</span>
            <span class="email-confirmed-text" style="display: block; padding-bottom: 30px;">Оставьте отзыв о отеле <br> "Отель Марк" <br> нажав на кнопку ниже.</span>
            <a href="{{ $link }}" class="email-button" style="display: inline-block; color: white; border-radius: 10px; text-decoration: none; font-size: 24px; margin-top: 20px; font-style: italic; background: #769dd5; padding: 15px 27px;">Оставить отзыв</a>
        </div>
        <div class="email-footer" style="text-align: center; background: white; padding: 20px;" align="center">
            <span>Служба поддержки <a href="" style="color: #313a45;">hotels-ok.com</a></span>
        </div>
    </div>
    
    
</body>
</html>
