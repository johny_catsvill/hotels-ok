<!--
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Инструкция от hotels-ok</title>
</head>
<style>
    
    #wrapper {
        width: 600px;
        max-width: 600px;
        min-width: 600px;
    }
    
    #wrapper img {
        width: 100%;
        height: 100%;
    }
    
    #wrapper a {
        display: inline-block;
        margin: 40px auto;
        background-color: #769dd5;
        width: 200px;
        text-align: center;
        padding: 10px;
        color: white;
        border-radius: 3px;
    }
    
</style>
<body>
    <div id="wrapper">
        <img src="{{ $url }}/images/instruction.jpg" alt="">
        <a href="{{ route('regorg') }}">Начать</a>
    </div>
</body>
</html>-->
<!-- Inliner Build Version 4380b7741bb759d6cb997545f3add21ad48f010b -->

<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta charset="UTF-8">
<title>Инструкция от hotels-ok</title>
</head>
<body>
    <div id="wrapper" style="width: 600px; max-width: 600px; min-width: 600px;">
        <img src="http://hotels-ok.com/images/instruction.jpg" alt="" style="width: 100%; height: 100%;"><a href="{{route('partners')}}" style="display: block; width: 200px; text-align: center; color: white; border-radius: 3px; background: #769dd5; margin: 40px auto; padding: 10px;">Перейти на сайт</a>
    </div>
</body>
</html>

