<!DOCTYPE html>
<!--
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body>
    
    <style>
        .email-container {
            max-width: 600px;
            width: 600px;
            min-height: 300px;
            border-radius: 8px;
            margin: auto;
            background-color: white;
            text-align: center;
            color: #313a45;
            box-shadow: 1px 2px 5px rgba(1,1,1,0.16);
            font-family: sans-serif;
        }
        
        .email-header {
            background-color: #313945;
            padding: 15px 0;
            width: 100%;
            color: white;
            border-radius: 5px 5px 0 0;
        }
        
        .email-container-text {
            font-size: 28px;
            line-height: 30px;
            padding-top: 40px;
            border-bottom: 1px solid #ccc;
            padding-bottom: 50px;
        }
        
        .email-container-text span {
            display: block;
            padding-bottom: 30px;
        }
        
        .email-button {
            display: inline-block;
            padding: 15px 27px;
            background-color: #769dd5;
            color: white;
            border-radius: 10px;
            text-decoration: none;
            font-size: 24px;
            margin-top: 20px;
            font-style: italic;
            
        }
        
        .email-footer {
            text-align: center;
            background-color: white;
            padding: 20px;
        }
        
        .email-footer a {
            color: #313a45;
        }
        
    </style>
    
    <div class="email-container">
        <div class="email-header">
            <span>hotels-ok.com</span>
        </div>
        <div class="email-container-text">
            <span class="email-welcome-text"><b>Добро пожаловать</b><br>batenko1@meta.ua</span>
            <span class="email-thanks">Спасибо, что зарегистрировались <br> на hotels-ok.com!</span>
            <span class="email-confirmed-text">Пожалуйста, подтвердите свою почту <br> нажав на кнопку ниже.</span>
            <a href="" class="email-button">Подтвердить мой аккаунт</a>
        </div>
        <div class="email-footer">
            <span>Служба поддержки <a href="">hotels-ok.com</a></span>
        </div>
    </div>
    
    
</body>
</html>-->




<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta charset="UTF-8">
<title>Document</title>
</head>
<body>
    
    <div class="email-container" style="max-width: 600px; width: 600px; min-height: 300px; border-radius: 8px; text-align: center; color: #313a45; box-shadow: 1px 2px 5px rgba(1,1,1,0.16); font-family: sans-serif; background: white; margin: auto;" align="center">
        <div class="email-header" style="width: 100%; color: white; border-radius: 5px 5px 0 0; background: #313945; padding: 15px 0;">
            <span style="color: white;">hotels-ok.com</span>
        </div>
        <div class="email-container-text" style="font-size: 28px; line-height: 30px; padding-top: 40px; border-bottom-color: #ccc; border-bottom-width: 1px; border-bottom-style: solid; padding-bottom: 50px;">
            <span class="email-welcome-text" style="display: block; padding-bottom: 30px;"><b>Добро пожаловать</b><br>{{ $email }}</span>
            <span class="email-thanks" style="display: block; padding-bottom: 30px;">Спасибо, что зарегистрировались <br> на hotels-ok.com!
            @if(isset($password)) <br> Ваш пароль <br> <b>{{ $password }}</b> @endif
            </span>
            <span class="email-confirmed-text" style="display: block; padding-bottom: 30px;">Пожалуйста, подтвердите свою почту <br> нажав на кнопку ниже.</span>
            <a href="{{ $url }}" class="email-button" style="display: inline-block; color: white; border-radius: 10px; text-decoration: none; font-size: 24px; margin-top: 20px; font-style: italic; background: #769dd5; padding: 15px 27px;">Подтвердить мой аккаунт</a>
        </div>
        <div class="email-footer" style="text-align: center; background: white; padding: 20px;" align="center">
            <span>Служба поддержки <a href="" style="color: #313a45;">hotels-ok.com</a></span>
        </div>
    </div>
    
    
</body>
</html>
