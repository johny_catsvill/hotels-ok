<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body>
    
    <style>
        .email-container {
            max-width: 600px;
            width: 600px;
            min-height: 300px;
            border-radius: 8px;
            margin: auto;
            background-color: white;
            text-align: center;
            color: #313a45;
            box-shadow: 1px 2px 5px rgba(1,1,1,0.16);
            font-family: sans-serif;
        }
        
        .email-header {
            background-color: #313945;
            padding: 15px 0;
            width: 100%;
            color: white;
            border-radius: 5px 5px 0 0;
        }
        
        .email-container-text {
            font-size: 28px;
            line-height: 30px;
            padding-top: 40px;
            border-bottom: 1px solid #ccc;
            padding-bottom: 50px;
        }
        
        .email-container-text span {
            display: block;
            padding-bottom: 30px;
        }
        
        .email-button {
            display: inline-block;
            padding: 15px 27px;
            background-color: #769dd5;
            color: white;
            border-radius: 10px;
            text-decoration: none;
            font-size: 24px;
            margin-top: 20px;
            font-style: italic;
            
        }
        
        .email-footer {
            text-align: center;
            background-color: white;
            padding: 20px;
        }
        
        .email-footer a {
            color: #313a45;
        }
        
        .email-none {
            font-size: 20px;
            padding-top: 40px;
        }
        
        .email-confirmed-text {
            font-size: 24px;
        }
        
    </style>
    
    <div class="email-container">
        <div class="email-header">
            <span>hotels-ok.com</span>
        </div>
        <div class="email-container-text">
            <span class="email-welcome-text"><b>Привет</b><br>{{ $email }}</span>
            <span class="email-confirmed-text">Кто-то запросил изменение пароля. <br> Ваш новый пароль {{ $password }}</span>
            <a href="{{ $url }}" class="email-button">Подтвердить</a>
            <span class="email-none">Если вы не запрашивали это, пожалуйста, <br> проигнорируйте это письмо. Ваш пароль не изменится, <br> пока вы не перейдете по ссылке выше <br> и не создадите новую.</span>
        </div>
        <div class="email-footer">
            <span>Служба поддержки <a href="{{ $homepage }}">hotels-ok.com</a></span>
        </div>
    </div>
    
    
</body>
</html>