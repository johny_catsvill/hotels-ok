<!DOCTYPE html>
<html lang="en">
<head class="height-100">
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{!! csrf_token() !!}" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Страница на обслуживании</title>
<!--    <link rel="stylesheet" href="/css/reset.css">-->
<!--    <link rel="stylesheet" href="/css/bootstrap-theme.min.css">-->
<!--    <link rel="stylesheet" href="/css/bootstrap.min.css">-->
    <link rel="stylesheet" href="/css/stub.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500" rel="stylesheet">
</head>
<body style="padding:0;margin:0;" class="height-100">

    


    <section id="wrapper">
        <div class="overlays"></div>
        <div class="timer-for-start">
           <div class="wrapper-for-timer">
               <div class="logo-authentification">
                <a href="{{ route('homepage') }}"><img src="/images/logo-our-admin.png" alt=""></a>
            </div>
            <h4>Здравствуйте! Благодарим Вас за просмотр нашего проекта.</h4>
            <span>Приложение не совсем готово, но мы прилагаем все усилия для завершения работы над ним. <br> Мы запустимся через:</span>
            <div class="timer">
                <ul class="countdown">
                    <li>
                        <span class="days">00</span>
                        <p class="days_ref">days</p>
                    </li>
                    <li class="seperator">:</li>
                    <li>
                        <span class="hours">00</span>
                        <p class="hours_ref">hours</p>
                    </li>
                    <li class="seperator">:</li>
                    <li>
                        <span class="minutes">00</span>
                        <p class="minutes_ref">minutes</p>
                    </li>
                    <li class="seperator">:</li>
                    <li>
                        <span class="seconds">00</span>
                        <p class="seconds_ref">seconds</p>
                    </li>
                </ul>
            </div>
           </div>
            
            <div class="subscription">
                <h4>Если Вы хотите получить уведомления о начале работы проекта, можете оставить адрес своей электронной почты:</h4>
                <div class="auth-field">
                       <input type="text" name="email" data-right="on" id="email-auth" autocomplete="off" onkeyup="MainFunction.emailStub(this);" onfocus="MainFunction.emailStub(this);" onblur="MainFunction.emailStub(this);" >
                       <span class="title-field">Email</span>
                       <span class="error-field"></span>
                       <span class="true-field"></span>
                       
                   </div>
                   <a href="" class="submit-auth" onclick="MainFunction.saveEmailSending(event, this);">Отправить</a> 
            </div>
        </div>
    </section>
</body>
<script type="text/javascript" src="/js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="/js/jquery.downCount.js"></script>
<script type="text/javascript" src="/js/script.js"></script>
<script>
    $('.countdown').downCount({
            date: '11/16/2017 12:00:00',
            offset: +10
        }, function () {
            alert('WOOT WOOT, done!');
        });
</script>
</html>