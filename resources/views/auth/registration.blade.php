@extends('site.startpage')
@section('content')

@section('top-block')
@endsection
<section id="auth">
    <div class="auth-wrapper padding-0px">
        <div class="container-fluid padding-0px">
            <div class="row margin-0px">
                <div class="col-lg-6 padding-0px">
                    <div class="left-img-auth">
                        <img src="/images/bg-registration.jpg" alt="">
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-auth">

                        @if(session('successRegistration'))
                            <div class="success-registration">
                                <h3>{{ __('Регистрация прошла успешно') }}</h3>
                                <img src="/images/message-registration-icon.jpg" alt="">
                                <span>{!! __('На вашу почту отправлено письмо, <br>
пройдите по ссылке для подтверждения') !!}</span>
                            </div>
                            <div class="wrap-success-registration-btn">
                                <a href="{{ route('login') }}" class="btn-auth-success-registration">{{ __('Авторизация') }}</a>
                            </div>
                        @else
                            <h2>{{ __('Создать аккаунт') }}</h2>
                            <ul class="social-buttons">
                                <li><a href="{{ route('socialAuth', 'facebook') }}" class="btn-facebook"><i class="fa fa-facebook" aria-hidden="true"></i> Facebook</a></li>
                                <li><a href="{{ route('socialAuth', 'google') }}" class="btn-google"><i class="fa fa-google-plus" aria-hidden="true"></i> Google</a></li>
                            </ul>
                            <form action="{{ route('registration') }}" method="post">

                            <label for="" class="error-label margin-0px font-weight-normal">
                            <input type="text" name="email" placeholder="Email" value="{{ old('email') }}" class="@if($errors->has('email'))border-red @endif" onfocus="newFunction.removeErrorField(this);">
                            <span class="@if(!$errors->has('email')) hide @endif">@if($errors->has('email')) {{ $errors->first('email') }} @endif</span>
                            </label>
                            <label for="" class="error-label margin-0px font-weight-normal">
                            <input type="text" name="name" placeholder="{{ __('Имя') }}" value="{{old('name')}}" class="@if($errors->has('name'))border-red @endif" onfocus="newFunction.removeErrorField(this);">
                            <span class="@if(!$errors->has('name')) hide @endif">@if($errors->has('name')) {{ $errors->first('name') }} @endif</span>
                            </label>
                            <label for="" class="error-label margin-0px font-weight-normal">
                            <input type="text" name="lastName" value="{{ old('lastName') }}" placeholder="{{ __('Фамилия') }}" class="@if($errors->has('lastName'))border-red @endif" onfocus="newFunction.removeErrorField(this);">
                            <span class="@if(!$errors->has('lastName')) hide @endif">@if($errors->has('lastName')) {{ $errors->first('lastName') }} @endif</span>
                            </label>

                            <label for="" class="error-label margin-0px font-weight-normal">
                                <input type="text" id="phoneCallback" autocomplete="off" name="phone" value="{{ old('phone') }}" placeholder="{{ __('Телефон') }}" class="@if($errors->has('phone'))border-red @endif" onfocus="newFunction.removeErrorField(this);">
                                <span class="@if(!$errors->has('phone')) hide @endif">@if($errors->has('phone')) {{ $errors->first('phone') }} @endif</span>
                            </label>


                            <label for="" class="error-label margin-0px font-weight-normal">
                            <input type="password" name="password" placeholder="Пароль" class="@if($errors->has('password'))border-red @endif" onfocus="newFunction.removeErrorField(this);">
                            <span class="@if(!$errors->has('password'))  hide @endif">@if($errors->has('password')) {{ $errors->first('password') }} @endif</span>
                            </label>
                            <label for="" class="error-label margin-0px font-weight-normal">
                            <input type="password" name="passwordRepeat" placeholder="{{ __('Повторите пароль') }}" class="@if($errors->has('passwordRepeat'))border-red @endif" onfocus="newFunction.removeErrorField(this);">
                            <span class="@if(!$errors->has('passwordRepeat')) hide @endif">@if($errors->has('passwordRepeat')) {{ $errors->first('passwordRepeat') }} @endif</span>
                            </label>
                            <label for="" class="label-checkbox-agree display-inline-block">
                            <table>
                            <tr>
                            <td><input type="checkbox" name="agree" @if(old('agree')) checked @endif onchange="newFunction.removeErrorField(this);"></td>
                            <td>
                            <span>{{ __('Я согласен с условиями') }} <a href="{{ route('public-tender') }}" target="_blank">{{ __('публичной оферты') }}</a> {{ __('и') }} <a href="" target="_blank"> {{ __('соглашения о конфиденциальности') }}</a></span>
                            <span class="error-checkbox @if($errors->has('agree')) color-red  @else hide @endif">@if($errors->has('agree')) {{ $errors->first('agree') }} @endif</span>
                            </td>
                            </tr>
                            </table>
                            </label>
                            {{ csrf_field() }}
                            <input type="submit" name="submit" value="{{ __('Зарегистрироваться') }}">
                            </form>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@include('site.blocks.messages-phone')
<div class="clear"></div>

<style>
    #topMenu {
        box-shadow:  0 -1px 2px 2px #ccc;
    }

    .top-menu ul li {
        padding-bottom: 10px;
    }
</style>
@endsection
