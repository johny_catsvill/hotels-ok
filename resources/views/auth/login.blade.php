@extends('site.startpage')
@section('content')

    @section('top-block')
    @endsection
<section id="auth">
    <div class="auth-wrapper padding-0px">
        <div class="container-fluid padding-0px">
            <div class="row margin-0px">
                <div class="col-lg-6 padding-0px">
                    <div class="left-img-auth">
                        <img src="/images/bg-auth.jpg" alt="">
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-auth">
                        <h2>{{ __('Войдите в свой аккаунт') }}</h2>
                        <ul class="social-buttons">
                            <li><a href="{{ route('socialAuth', 'facebook') }}" class="btn-facebook"><i class="fa fa-facebook" aria-hidden="true"></i> Facebook</a></li>
                            <li><a href="{{ route('socialAuth', 'google') }}" class="btn-google"><i class="fa fa-google-plus" aria-hidden="true"></i> Google</a></li>
                        </ul>
                        @if(session('message'))
                            <span class="error-field message-success-login" style="color:green;">{{ session('message') }}</span>
                        @endif
                        <form action="{{ route('login') }}" method="post">
                            {{ csrf_field() }}
                            <label for="" class="error-label margin-0px font-weight-normal">
                                <input type="text" name="email" placeholder="Email" value="{{old('email')}}" class="@if(session('error-auth'))border-red @endif"
                                       onfocus="newFunction.removeErrorField(this);">
                                <span class="error-field @if(!session('error-auth')) hide @endif">@if(session('error-auth')) {{ session('error-auth') }} @endif</span>
                            </label>
                            <label for="" class="error-label margin-0px font-weight-normal">
                                <input type="password" name="password" placeholder="{{ __('Пароль') }}" value="" class="field-password-auth" onfocus="newFunction.removeErrorField(this);">
                                <span class="error-field @if(!$errors->has('password')) hide @endif">@if($errors->has('password')) {{ $errors->first('password') }} @endif</span>
                            </label>
                            <ul>
                                <li>
                                    <label for="" class="label-checkbox-agree display-inline-block">
                                        <input type="checkbox" name="rememberMe"> <span>{{ __('Запомнить меня') }}</span>
                                    </label>
                                    <a href="{{ route('remember-password') }}" class="display-inline-block btn-remember-password">{{__('Забыли пароль')}}?</a>
                                </li>
                                <li>
                                    <input type="submit" name="submit" value="{{ __('Войти') }}">
                                    <a href="{{ route('registration') }}" class="display-inline-block float-right padding-top-15px">{{ __('Создать аккаунт') }}</a>
                                </li>
                            </ul>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@include('site.blocks.messages-phone')
<div class="clear"></div>

<style>
    #topMenu {
        box-shadow:  0 -1px 2px 2px #ccc;
    }

    .top-menu ul li {
        padding-bottom: 10px;
    }
</style>
@endsection
