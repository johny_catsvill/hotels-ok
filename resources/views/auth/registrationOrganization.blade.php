@extends('site.startpage')
@section('content')

@section('top-block')
@endsection
<section id="auth">
    <div class="auth-wrapper padding-0px">
        <div class="container-fluid padding-0px">
            <div class="row margin-0px">
                <div class="col-lg-6 padding-0px">
                    <div class="left-img-auth">
                        <img src="/images/bg-registration.jpg" alt="">
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-auth">
                        <h2>Создать аккаун организации</h2>
                        <form action="{{ route('regorg') }}" method="post">

                            <label for="" class="error-label margin-0px font-weight-normal">
                                <a class="list-style" href="javascript:void(0);">Выберите тип обьекта <i class="fa fa-angle-down"></i></a>
                                <span class="hide">Вы не указали тип обьекта</span>
                            </label>

                            <label for="" class="error-label margin-0px font-weight-normal">
                                <input type="text" name="name" placeholder="Имя" value="{{old('name')}}" class="@if($errors->has('name'))border-red @endif" onfocus="newFunction.removeErrorField(this);">
                                <span class="@if(!$errors->has('name')) hide @endif">@if($errors->has('name')) {{ $errors->first('name') }} @endif</span>
                            </label>
                            <label for="" class="error-label margin-0px font-weight-normal">
                                <input type="text" name="email" placeholder="Email" value="{{ old('email') }}" class="@if($errors->has('email'))border-red @endif" onfocus="newFunction.removeErrorField(this);">
                                <span class="@if(!$errors->has('email')) hide @endif">@if($errors->has('email')) {{ $errors->first('email') }} @endif</span>
                            </label>
                            <label for="" class="error-label margin-0px font-weight-normal">
                                <input type="password" name="password" placeholder="Пароль" class="@if($errors->has('password'))border-red @endif" onfocus="newFunction.removeErrorField(this);">
                                <span class="@if(!$errors->has('password'))  hide @endif">@if($errors->has('password')) {{ $errors->first('password') }} @endif</span>
                            </label>
                            <label for="" class="error-label margin-0px font-weight-normal">
                                <input type="password" name="passwordRepeat" placeholder="Повторите пароль" class="@if($errors->has('passwordRepeat'))border-red @endif" onfocus="newFunction.removeErrorField(this);">
                                <span class="@if(!$errors->has('passwordRepeat')) hide @endif">@if($errors->has('passwordRepeat')) {{ $errors->first('passwordRepeat') }} @endif</span>
                            </label>
                            <label for="" class="error-label margin-0px font-weight-normal">
                                <input type="text" name="passwordRepeat" placeholder="Телефон" id="phoneCallback" class="@if($errors->has('passwordRepeat'))border-red @endif" onfocus="newFunction.removeErrorField(this);">
                                <span class="@if(!$errors->has('passwordRepeat')) hide @endif">@if($errors->has('passwordRepeat')) {{ $errors->first('passwordRepeat') }} @endif</span>
                            </label>
                            <label for="" class="label-checkbox-agree display-inline-block">
                                <table>
                                    <tr>
                                        <td><input type="checkbox" name="agree" @if(old('agree')) checked @endif onchange="newFunction.removeErrorField(this);"></td>
                                        <td>
                                            <span>Я согласен с условиями <a href="" target="_blank">публичной оферты</a> и <a href="" target="_blank"> соглашения о конфиденциальности</a></span>
                                            <span class="error-checkbox @if($errors->has('agree')) color-red  @else hide @endif">@if($errors->has('agree')) {{ $errors->first('agree') }} @endif</span>
                                        </td>
                                    </tr>
                                </table>
                            </label>
                            {{ csrf_field() }}
                            <input type="submit" name="submit" value="Зарегистрироваться">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@include('site.blocks.messages-phone')
<div class="clear"></div>

<style>
    #topMenu {
        box-shadow:  0 -1px 2px 2px #ccc;
    }

    .top-menu ul li {
        padding-bottom: 10px;
    }
</style>
@endsection
