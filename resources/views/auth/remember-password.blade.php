@extends('site.startpage')
@section('content')

@section('top-block')
@endsection
<section id="auth">
    <div class="auth-wrapper padding-0px">
        <div class="container-fluid padding-0px">
            <div class="row margin-0px">
                <div class="col-lg-6 padding-0px">
                    <div class="left-img-auth">
                        <img src="/images/bg-auth.jpg" alt="">
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-auth">
                        <h2>{{ __('Восстановить пароль') }}</h2>
                        <form action="{{ route('remember-password') }}" method="post">
                            {{ csrf_field() }}
                            <label for="" class="error-label margin-0px font-weight-normal">
                                <input type="text" name="email" placeholder="Email" value="{{old('email')}}"
                                       class="@if($errors->has('email'))border-red @endif" onfocus="newFunction.removeErrorField(this);">
                                <span class="error-field @if(!$errors->has('email')) hide @endif">@if($errors->has('email')) {{ $errors->first('email') }} @endif</span>
                                @if(session('message'))
                                    <span class="error-field" style="color:green;">{{ session('message') }}</span>
                                @endif
                            </label>

                            <ul>
                                <li>
                                    <input type="submit" name="submit" value="{{ __('Войти') }}">
                                </li>
                            </ul>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@include('site.blocks.messages-phone')
<div class="clear"></div>

<style>
    #topMenu {
        box-shadow:  0 -1px 2px 2px #ccc;
    }

    .top-menu ul li {
        padding-bottom: 10px;
    }
</style>
@endsection
