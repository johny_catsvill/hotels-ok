@extends('site.startpage')
@section('content')
@section('top-block')
    <section id="news" class="one-news-h">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>{{ __('Новости') }}</h2>
                    @include('site.blocks.breadcrumbs')
                </div>
            </div>
        </div>
    </section>

    <section id="contactsContent">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    @include('site.blocks.left-sidebar-news')
                    <div class="col-lg-10">
                        <div class="border-left-1px">
                            <div class="one-news-full">
                                <ul>
                                    <li><span class="date-news">{{ $oneNews->date_published }}</span></li>
                                    <li>|</li>
                                    <li><a href="{{ route('news', $oneNews->category->slug) }}" class="category-news">{{ $oneNews->category->name }}</a></li>
                                </ul>
                                <h1>{{ $oneNews->name }}</h1>
                                <img src="{{ asset('/images/news/'.$oneNews->getNewImage('big')) }}" alt="" class="main-img-news">
                                <div class="description-news">
                                    {!! $oneNews->text !!}
                                </div>
                                <h4>{{ __('Популярные новости') }}</h4>
                                <div class="second-rows-news margin-left-0px list-news-by-one-news">
                                    <ul>
                                        @foreach($popularNews as $news)
                                            <li>
                                                <div class="one-news">
                                                    <div class="wrapper-img-news">
                                                        <a href="{{ route('one-news', ['category' => $news->category->slug,'id' => $news->id, 'url' => $news->slug]) }}">
                                                            <img src="/images/news/{{ $news->getNewImage('norm') }}" alt=""></a>
                                                    </div>
                                                    <ul>
                                                        <li><span class="date-news">{{ $news->date_published }}</span></li>
                                                        <li>|</li>
                                                        <li><a href="{{ route('news', $news->category->slug) }}" class="category-news">{{ $news->category->name }}</a></li>
                                                    </ul>
                                                    <a href="{{ route('one-news', ['category' => $news->category->slug,'id' => $news->id, 'url' => $news->slug]) }}" class="link-news">{{ $news->name }}</a>
                                                </div>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@include('site.blocks.messages-phone')
@endsection