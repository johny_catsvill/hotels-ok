@extends('site.startpage')
@section('content')
    <section id="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="list-breadcrumbs reservation-breadcrumbs">
                        <ul>
                            @foreach($breadcrumbs as $key => $breadcrumb)
                                @if($loop->last)
                                    <li><span>{{ $key }}</span></li>
                                @else
                                    <li><a href="{{ $breadcrumb }}">{{ $key }}</a></li>
                                    <li>/</li>
                                @endif
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="wrapperReservationContent">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-xs-12  padding-left-0px desktop-hide">
                    <div class="right-sidebar-reservation">
                        <div class="info-about-room">
                            <ul>
                                @foreach($rooms as $r)
                                    <li><span class="name-room">{{ $r->name }}</span> <span class="price-room">{{ $r->price }} грн</span></li>
                                @endforeach
                            </ul>
                            <hr>
                            <ul class="margin-top-20px">
                                <li><span class="full-price-text display-inline-block">{{ __('Цена') }}:</span> <span class="full-price-room display-inline-block float-right">{{ $fullPrice }} грн</span></li>
                            </ul>
                            <span class="mini-description-reservation">{{ __('В зависимости от условий и периода бронирования с вами может связаться представитель отеля и запросить предоплату для гарантии вашего бронирования.') }}</span>
                        </div>
                        @if(!$clientInfo)
                            <div class="info-about-reservation">
                                <span>{{ __('Зарегистрируйтесь') }}</span>
                                <p>и за каждое бронирование отеля,
                                    вам будут начисляться бонусы, за
                                    которые можно бронировать
                                    отели или покупать
                                    дополнительные услуги</p>
                            </div>
                        @endif
                        <div class="viber-project">
                            <img src="/images/viber.jpg" alt="">
                        </div>
                    </div>
                </div>
                <div class="col-lg-9 col-xs-12 col-sm-8 ">
                    <div class="reservation-content">
                        <div class="main-info-by-reservation">
                            <ul>
                                <li>
                                    <div class="img-object" style="background-image: url('images/hotels/{{ $object->id.'-'.$object->subdomain.'/'.$object->image }}');"></div>
                                </li>
                                <li>
                                    <div class="description-about-object">
                                        <h2>{{ $object->name }} <span>({{ $object->objectCategory->name }})</span></h2>
                                        <span>{{ $object->address }}</span>
                                        <ul>
                                            <li><b>{{ __('Заезд') }}:</b> {{ $formatedDateFrom }}</li>
                                            <li><b>{{ __('Отьезд') }}:</b> {{ $formatedDateTo }}</li>
                                        </ul>
                                        <a href="javascript:void(0);" onclick="Popup.showPopup('conditions-reservation', event);">Условия бронирования и отмены</a>
                                    </div>
                                </li>
                            </ul>
                                <ul class="list-rooms-reservation">
                                    @foreach($rooms as $r)
                                    <li>
                                        <a href="javascript:void(0);" onclick="">{{ $r->name }}</a>
                                        <ul class="dates-reservation">
                                            @foreach($r->datesThisRoom as $date)
                                                <li>
                                                    <span>{{ date('d.m.Y', $date['date']) }}
                                                        <div class="info-price-by-date">
                                                            <ul>
                                                                <li><b>{{ __('Цена') }}:</b> {{ $date['price'] }} грн</li>
                                                                <li><b>{{ __('Скидка') }}:</b> {{ $date['discount'] }}%</li>
                                                                <li><b>{{ __('Количество') }}:</b> {{ $date['countRoom'] }}</li>
                                                            </ul>
                                                        </div>
                                                    </span>
                                                </li>
                                            @endforeach
                                        </ul>
                                        <div class="info-room-reservation">
                                            <div class="what-is-the-room">
                                                <b>{{ __('В номере') }}:</b>
                                                <ul>
                                                    @foreach($r->modelRoom->pluses as $pluse)
                                                        @if($loop->index == 8)
                                                            @break
                                                        @endif
                                                        <li>
                                                            <i class="fa fa-circle" aria-hidden="true"></i>
                                                            <span class="display-inline-block">{{ $pluse->name }}</span>
                                                        </li>
                                                    @endforeach

                                                </ul>
                                            </div>
                                        </div>
                                        <div class="clear"></div>
                                        <div class="other-info-about-room show padding-0px">
                                            <p>{{$r->modelRoom->infoAboutRoom}}</p>
                                            <ul>
                                                @foreach($r->categoryPluses as $category)
                                                    @if(count($category->pluses) > 0)
                                                        <li><b>{{ $category->name }}:</b> {{ $category->pluses->implode('name', ', ') }}</li>
                                                    @endif
                                                @endforeach
                                            </ul>
                                        </div>
                                        <div class="clear"></div>
                                    </li>
                                    @endforeach
                                </ul>


                            <div class="form-for-reservation">
                                <form action="" method="post">
                                    @if($clientInfo)
                                        <a href="javascript:void(0);" class="btn-use-data" onclick="newFunction.insertUserData(this);"
                                           data-fio="{{ $clientInfo->lastName.' '.$clientInfo->name.' '. $clientInfo->middleName }}"
                                           data-email="{{ $userAuth->email }}" data-phone="{{ $userAuth->phone }}">{{ __('Подставить личные данные') }}</a>
                                        <div class="clear"></div>
                                    @endif
                                    <div class="col-lg-6 col-sm-6">
                                        <label for="">
                                            <input type="text" name="fio" placeholder="{{ __('ФИО') }}">
                                            <span class="red">{{ __('Неккорректное значение поля') }}</span>
                                        </label>
                                        <label for="">
                                            <input type="text" name="email" placeholder="Email">
                                            <span class="red">{{ __('Неккорректное значение поля') }}</span>
                                        </label>
                                        <label for="">
                                            <input type="text" name="phone" placeholder="{{ __('Телефон') }}" id="phoneCallback">
                                            <span class="red">{{ __('Неккорректное значение поля') }}</span>
                                        </label>
                                        <label for="">
                                            <input type="text" name="link-referal" placeholder="{{ __('Реферальная ссылка') }}">
                                        </label>
                                    </div>
                                    <div class="col-lg-6 col-sm-6">
                                        <textarea name="text" id="" cols="30" rows="10" placeholder="{{ __('Дополнительные пожелания') }}"></textarea>
                                        <label for="">
                                            <input type="text" name="promo-code" placeholder="{{ __('Промокод') }}">
                                        </label>
                                    </div>

                                    <div class="clear"></div>
                                    <label for="stars5" class="label-checkbox-agree"><input name="agree" checked type="checkbox" id="stars5">
                                        <span>
                                            {{ __('Я согласен с условиями') }} &nbsp;<a href="{{ route('public-tender') }}" target="_blank"> {{ __('публичной оферты') }}</a>&nbsp; {{ __('и') }} &nbsp;
                                            <a target="_blank" href="{{ route('confidentiality') }}"> {{ __('соглашение о конфиденциальности') }}</a>
                                        </span>
                                    </label>
                                        <span class="red error-agree">{{ __('Поле соглашения обязательное к заполнению') }}</span>

                                        @if(isset($userAuth) && $userAuth)
                                            <input type="hidden" name="userId" value="{{ $userAuth->id }}">
                                        @else
                                            <input type="hidden" name="userId" value="">
                                        @endif
                                        <input type="hidden" name="hotelId" value="{{ $hotelId }}">
                                        <input type="hidden" name="arrival" value="{{ $dateFrom }}">
                                        <input type="hidden" name="departure" value="{{ $dateTo }}">
                                    <input type="submit" name="submit" value="Забронировать" onclick="Reservation.saveFullReservation(event);">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-xs-12 col-sm-4 desktop-show padding-left-0px">
                    <div class="right-sidebar-reservation">
                        <div class="info-about-room">
                            <ul>
                                @foreach($rooms as $r)
                                    <li><span class="name-room">{{ $r->name }}</span> <span class="price-room">{{ $r->price }} грн</span></li>
                                @endforeach
                            </ul>
                            <hr>
                            <ul class="margin-top-20px">
                                <li><span class="full-price-text display-inline-block">{{ __('Цена') }}:</span> <span class="full-price-room display-inline-block float-right">{{ $fullPrice }} грн</span></li>
                            </ul>
                            <span class="mini-description-reservation">В зависимости от условий и периода бронирования с вами может связаться представитель отеля и запросить предоплату для гарантии вашего бронирования.</span>
                        </div>
                        @if(!$clientInfo)
                            <div class="info-about-reservation">
                                <span>{{ __('Зарегистрируйтесь') }}</span>
                                <p>и за каждое бронирование отеля,
                                    вам будут начисляться бонусы, за
                                    которые можно бронировать
                                    отели или покупать
                                    дополнительные услуги</p>
                            </div>
                        @endif
                        <div class="viber-project">
                            <img src="/images/viber.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('site.blocks.messages-phone')
    <div class="conditions-reservation popup">
        <a href="javascript:void(0);" onclick="Popup.closePopup(this);" title="Закрыть" class="btn-close-popup">
            <img src="images/close.png" alt="Закрыть окно">
        </a>
        <div class="content-popup">
            <div class="top-content-conditions-reservation">
                <h5>{{ __('Условия бронирования') }}</h5>
                <b class="name-hotel-reservation">{{ $object->objectCategory->name }} {{ $object->name }}</b>
                <table>
                    <tr>
                        <td><b>Отмена бронирования:</b></td>
                        <td>В случае отмены, изменения бронирования или незаезда стоимость заказа не возвращается.</td>
                    </tr>
                    <tr>
                        <td><b>Предоплата:</b></td>
                        <td>100% от стоимости проживания</td>
                    </tr>
                </table>
            </div>
            <div class="wrapper-pluses-reservation">
                <span>Преимущества бронирования</span>
                <ul>
                    <li><i class="fa fa-circle" aria-hidden="true"></i> Коммисия за бронирование не взымается</li>
                    <li><i class="fa fa-circle" aria-hidden="true"></i> Удобные способы оплаты</li>
                    <li><i class="fa fa-circle" aria-hidden="true"></i> Гарантия лучшей цены</li>
                    <li><i class="fa fa-circle" aria-hidden="true"></i> Круглосуточная поддержка по телефону</li>
                </ul>
            </div>
        </div>
    </div>
@endsection