@extends('site.startpage')
@section('content')

    <section id="bgBussiness">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="bussiness-content">
                        <h1>{{ __('Организация деловых поездок') }}</h1>
                        <h2>Комплекс услуг по организации деловых поездок <br> по Украине и за рубежом</h2>
                        <a href="javascript:void(0);" onclick="newFunction.scrollToElement('submitApplication');">Подать заявку</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="bg-bussiness-overlay"></div>
    </section>
    <section id="plusesReservation" class="services-business">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="pluse-reservation-hotel bussiness-pluses">
                        <h2>Наши услуги</h2>
                        <span class="line-blue first-line-caption"></span>
                        <span class="line-blue two-line-caption"></span>
                        <div class="list-pluses">
                            <ul>
                                <li>
                                    <div class="one-pluse">
                                        <img src="/images/pluse-icon-1.png" alt="">
                                        <div class="two-border-pluse"></div>
                                    </div>
                                    <span>Гостиницы</span>
                                    <p>Бронирование гостиниц <br> по всей территории <br> Украины и за рубежом</p>
                                </li>
                                <li>
                                    <div class="one-pluse">
                                        <img src="/images/pluse-icon-2.png" alt="">
                                        <div class="two-border-pluse"></div>
                                    </div>
                                    <span>Конференц сервис</span>
                                    <p>Резервирование <br> конференц-залов для <br> проведения семинаров, <br> конференций,
                                        <br> тренингов</p>
                                </li>
                                <li>
                                    <div class="one-pluse">
                                        <img src="/images/pluse-icon-3.png" alt="">
                                        <div class="two-border-pluse"></div>
                                    </div>
                                    <span>Авиабилеты</span>
                                    <p>Организация <br> бронирования <br> авиабилетов в любую <br> точку мира</p>
                                </li>

                                <li>
                                    <div class="one-pluse">
                                        <img src="/images/pluse-icon-4.png" alt="">
                                        <div class="two-border-pluse"></div>
                                    </div>
                                    <span>Ж/Д билеты</span>
                                    <p>Заказ <br> железнодорожных <br> билетов</p>
                                </li>

                                <li>
                                    <div class="one-pluse">
                                        <img src="/images/pluse-icon-5.png" alt="">
                                        <div class="two-border-pluse"></div>
                                    </div>
                                    <span>Квартиры</span>
                                    <p>Бронирование <br> апартаментов (квартир) <br> по Украине</p>
                                </li>
                                <li>
                                    <div class="one-pluse">
                                        <img src="/images/pluse-icon-6.png" alt="">
                                        <div class="two-border-pluse"></div>
                                    </div>
                                    <span>Визовая поддержка</span>
                                    <p>Помощь в оформлении <br> виз и предоставление <br> дополнительных услуг</p>
                                </li>
                                <li>
                                    <div class="one-pluse">
                                        <img src="/images/pluse-icon-6.png" alt="">
                                        <div class="two-border-pluse"></div>
                                    </div>
                                    <span>Трансферы</span>
                                    <p>Организация <br> транспортного <br> обслуживания по <br> Украине и за рубежом</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="submitApplication">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="form-submit-application">

                        <div class="form-application">
                            <h3>Оставьте заявку</h3>
                            <span class="line-blue first-line-caption"></span>
                            <span class="line-blue two-line-caption"></span>
                            <form action="" method="post">
                                <input type="text" name="name" placeholder="Имя">
                                <input type="text" name="phone" placeholder="Телефон" id="phoneCallback">

                                <input type="text" name="object_name" placeholder="Компания">
                                <input type="text" name="email" placeholder="Email">
                                {{ csrf_field() }}
                                <input type="hidden" name="type" value="bussiness">
                                <input type="hidden" name="city" value="">
                                <textarea name="info" id="" cols="30" rows="10" placeholder="Дополнительная информация"></textarea>
                                <input type="submit" name="submit" value="Подать заявку" onclick="newFunction.addApplication(event, this);">
                            </form>
                        </div>

                        <div class="overlay-application"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="plusesCompany">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="wrapper-pluses-company">
                        <h3>Наши преимущества</h3>
                        <span class="line-blue first-line-caption"></span>
                        <span class="line-blue two-line-caption"></span>
                        <ul>
                            <li>
                                <div class="one-pluse-company">
                                    <div class="wrap-pluse-top">
                                        <div class="pluse-img">
                                            <img src="/images/pluse-project-1.png" alt="">
                                        </div>
                                        <span>Скидки 50% на проживание <br> в отелях Украины</span>
                                    </div>
                                    <p>На нашем сайте постоянно <br> действует акция “Отели за <br> полцены”. Такую скидку отели
                                        <br> предоставляют только если <br> бронировать через сайт</p>
                                </div>
                            </li>
                            <li>
                                <div class="wrap-pluse-top">
                                    <div class="pluse-img">
                                        <img src="/images/pluse-project-2.png" alt="">
                                    </div>
                                    <span>Персональный <br>менеджер</span>
                                </div>
                                <p>Вас будет обслуживать <br> персональный <br> квалифицированный менеджер, <br> который подберет оптимальный
                                    <br> вариант для вашего <br> корпоративного отдыха, учитывая <br> финансовые возможности и
                                    <br> ожидания по комфортности отелей. </p>
                            </li>
                            <li>
                                <div class="wrap-pluse-top">
                                    <div class="pluse-img">
                                        <img src="/images/pluse-project-3.png" alt="">
                                    </div>
                                    <span>Оформление <br>документов <br>для отчетности</span>
                                </div>
                                <p>Получение всех документов, <br> необходимых для отчетности в <br> бухгалтерии. Наша компания
                                    <br> работает по безналичному <br> расчету и является <br> плательщиком НДС. </p>
                            </li>
                            <li>
                                <div class="wrap-pluse-top">
                                    <div class="pluse-img">
                                        <img src="/images/pluse-project-4.png" alt="">
                                    </div>
                                    <span>Возможность любой <br>формы оплаты</span>
                                </div>
                                <p>Вы можете выбрать любую, <br> удобную для вас, форму оплаты <br> отеля (квартиры): карточкой,
                                    <br> банковским переводом или <br> наличными уже по прибытию в <br> отель. </p>
                            </li>
                            <li>
                                <div class="wrap-pluse-top">
                                    <div class="pluse-img">
                                        <img src="/images/pluse-project-5.png" alt="">
                                    </div>
                                    <span>Скидка для групп <br>от 10 человек</span>
                                </div>
                                <p>Скидка на проживание, если в <br> рабочую поездку отправляются <br> более 10 человек. Размер скидки
                                    <br> зависит от отеля, в котором вы <br> решите остановиться, а также от <br> количества бронируемых номеров.</p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="wrapperConfidence">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="confidence">
                        <h2>Нам доверяют</h2>
                        <span class="line-blue first-line-caption"></span>
                        <span class="line-blue two-line-caption"></span>
                        <ul>
                            <li>
                                <img src="/images/logo-big-epicenter.png" alt="">
                            </li>
                            <li>
                                <img src="/images/logo-big-epicenter.png" alt="">
                            </li>
                            <li>
                                <img src="/images/logo-big-epicenter.png" alt="">
                            </li>
                            <li>
                                <img src="/images/logo-big-epicenter.png" alt="">
                            </li>
                            <li>
                                <img src="/images/logo-big-epicenter.png" alt="">
                            </li>
                            <li>
                                <img src="/images/logo-big-epicenter.png" alt="">
                            </li>
                            <li>
                                <img src="/images/logo-big-epicenter.png" alt="">
                            </li>
                            <li>
                                <img src="/images/logo-big-epicenter.png" alt="">
                            </li>
                            <li>
                                <img src="/images/logo-big-epicenter.png" alt="">
                            </li>
                            <li>
                                <img src="/images/logo-big-epicenter.png" alt="">
                            </li>
                            <li>
                                <img src="/images/logo-big-epicenter.png" alt="">
                            </li>
                            <li>
                                <img src="/images/logo-big-epicenter.png" alt="">
                            </li>
                            <li>
                                <img src="/images/logo-big-epicenter.png" alt="">
                            </li>
                            <li>
                                <img src="/images/logo-big-epicenter.png" alt="">
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="reviews">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="reviews-style">
                        <h2>Отзывы компаний</h2>
                        <span class="line-blue first-line-caption"></span>
                        <span class="line-blue two-line-caption"></span>
                        <div class="list-reviews">
                            <ul>
                                <li>
                                    <div class="one-review">
                                        <img src="/images/review-1.png" alt="">
                                        <span>Ирина Белая</span>
                                        <p>Впервые воспользовалась этим сайтом, и не пожалела!! Ребята сработали оперативно, нашли квартиру во Львове по искомым параметрам и по цене приемлемой для нашего скромного бюджета, очень быстро оформили </p>
                                    </div>
                                </li>
                                <li>
                                    <div class="one-review">
                                        <img src="/images/review-2.png" alt="">
                                        <span>Ирина Белая</span>
                                        <p>Во Вторые воспользовалась этим сайтом, и не пожалела!! Ребята сработали оперативно, нашли квартиру во Львове по искомым параметрам и по цене приемлемой для нашего скромного бюджета, очень быстро оформили </p>
                                    </div>
                                </li>
                                <li>
                                    <div class="one-review">
                                        <img src="/images/review-3.png" alt="">
                                        <span>Ирина Белая</span>
                                        <p>В третье воспользовалась этим сайтом, и не пожалела!! Ребята сработали оперативно, нашли квартиру во Львове по искомым параметрам и по цене приемлемой для нашего скромного бюджета, очень быстро оформили </p>
                                    </div>
                                </li>
                                <li>
                                    <div class="one-review">
                                        <img src="/images/review-3.png" alt="">
                                        <span>Ирина Белая</span>
                                        <p>В третье воспользовалась этим сайтом, и не пожалела!! Ребята сработали оперативно, нашли квартиру во Львове по искомым параметрам и по цене приемлемой для нашего скромного бюджета, очень быстро оформили </p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="arrows-reviews-carousel">
                            <a href="javascript:void(0);" onclick="newFunction.prevCarouselSlide();" class="arrow-carousel arrow-left-carousel"><img src="/images/arrow-left-carousel.png" alt=""></a>
                            <a href="javascript:void(0);" onclick="newFunction.nextCarouselSlide();" class="arrow-carousel arrow-right-carousel"><img src="/images/arrow-right-carousel.png" alt=""></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@include('site.blocks.messages-phone')
@endsection