@extends('site.startpage')
@section('content')
@section('top-block')
    <section id="rewards" class="top-header-style">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h1>{{ __('Награды и заслуги') }}</h1>
                    @include('site.blocks.breadcrumbs')
                </div>
            </div>
        </div>
    </section>

    <section id="contactsContent">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    @include('site.blocks.left-sidebar-company')
                    <div class="col-lg-9">
                        <div class="main-content-help-page">
                            <table class="rewardsPage">
                                <tr>
                                    <td class="border-1px-solid position-relative">
                                        <div class="one-rewards">
                                            <b>За сотрудничество</b>
                                            <p>Является одной из крупнейших авиакомпаний в Соединенных Штатах. Самая старая авиакомпания в США которая может.</p>
                                        </div>
                                        <div class="line-right"></div>
                                    </td>
                                    <td>
                                        <div class="date-rewards">
                                            <span>Январь <br> 2018</span>
                                        </div>
                                        <div class="line-bottom"></div>
                                    </td>
                                    <td></td>
                                </tr>

                                <tr>
                                    <td></td>
                                    <td>
                                        <div class="date-rewards">
                                            <span>Февраль <br> 2018</span>
                                        </div>
                                        <div class="line-bottom"></div>
                                    </td>
                                    <td class="border-1px-solid position-relative">
                                        <div class="one-rewards">
                                            <b>За сотрудничество</b>
                                            <p>Является одной из крупнейших авиакомпаний в Соединенных Штатах. Самая старая авиакомпания в США которая может.</p>
                                        </div>
                                        <div class="line-left"></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="border-1px-solid position-relative">
                                        <div class="one-rewards">
                                            <b>За сотрудничество</b>
                                            <p>Является одной из крупнейших авиакомпаний в Соединенных Штатах. Самая старая авиакомпания в США которая может.</p>
                                        </div>
                                        <div class="line-right"></div>
                                    </td>
                                    <td>
                                        <div class="date-rewards">
                                            <span>Январь <br> 2018</span>
                                        </div>

                                    </td>
                                    <td></td>
                                </tr>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@include('site.blocks.messages-phone')
@endsection