@extends('site.startpage')
@section('content')
@section('top-block')
    <section id="questions">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h1>{{ __('Часто задаваемые вопросы') }}</h1>
                    @include('site.blocks.breadcrumbs')
                </div>
            </div>
        </div>
    </section>

    <section id="contactsContent">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    @include('site.blocks.left-sidebar-company')
                    <div class="col-lg-9">
                        <div class="main-content-help-page question-text-page">
                            <span class="caption-question" data-slug="{{ str_slug('Каким способом получить гарантию бронирования?') }}">
                                <i class="fa fa-long-arrow-right" aria-hidden="true"></i> Каким способом получить гарантию бронирования?</span>
                            <div class="wrap-p">
                                <p>После резервирования номера вы получите на электронную почту «Акт о заселении». Если вы забронировали жилье по тарифу с предварительной оплатой, тогда ваучер придет после оплаты.</p>
                            </div>
                            <span class="caption-question" data-slug="{{ str_slug('Какие нужно выполнить требования для  отмены бронирования?') }}">
                                <i class="fa fa-long-arrow-right" aria-hidden="true"></i> Какие нужно выполнить требования для  отмены бронирования?</span>
                            <div class="wrap-p">
                                <p>У вас есть возможность отменить бронь по объективным причинам. Всю информацию по этому вопросу смотрите на странице выбранного отеля  на  портале <b>Hotels-ok</b> либо в «Акте заселения». Чтобы отменить заказ, позвоните по телефону ХХХ, ХХХ.</p>
                            </div>
                            <span class="caption-question" data-slug="{{ str_slug('Предусмотрено ли денежное взыскание за отмену брони?') }}">
                                <i class="fa fa-long-arrow-right" aria-hidden="true"></i> Предусмотрено ли денежное взыскание за отмену брони?</span>
                            <div class="wrap-p">
                                <p>Если вы аннулировали заявку во время бесплатной аннуляции (ее можно посмотреть на сайте отеля или в «Акте заселения»), то штраф не взымается. Мы возвращаем деньги, внесенные за номер в гостинице. Бронирование без предоплаты не предусматривает штраф, независимо от сроков отказа, в случае отмены брони после периода бесплатной аннуляции, необходимо заплатить штраф в размере, что устанавливается непосредственно отелем. Вся сумма оплаты не возвращается клиенту в случае отказа от номера по акции “Скидка 50%”.</p>
                            </div>
                            <span class="caption-question" data-slug="{{ str_slug('Как вернуть назад деньги после отмены брони?') }}">
                                <i class="fa fa-long-arrow-right" aria-hidden="true"></i> Как вернуть назад деньги после отмены брони?</span>
                            <div class="wrap-p">
                                <p>Отказ от номера в период бесплатной аннуляции предусматривает возврат денежных средств в течение 14 дней. Население  Украины может получить деньги на карточку Приват Банка или переводом «Приват мани». Возможны перечисления через другие банки.  Россиияне могут воспользоваться переводом “Золотая корона” либо Western Union.  При оплате через отель напрямую, деньги возвращаются по правилам отеля.</p>
                            </div>
                            <span class="caption-question" data-slug="{{ str_slug('Возможно ли осуществить трансфер в гостиницу/с гостиницы?') }}">
                                <i class="fa fa-long-arrow-right" aria-hidden="true"></i> Возможно ли осуществить трансфер в гостиницу/с гостиницы?</span>
                            <div class="wrap-p">
                                <p>Да, если такой сервис присутствует в  отеле.</p>
                            </div>
                            <span class="caption-question" data-slug="{{ str_slug('Как правильнее доехать  до места назначения?') }}">
                                <i class="fa fa-long-arrow-right" aria-hidden="true"></i> Как правильнее доехать  до места назначения?</span>
                            <div class="wrap-p">
                                <p>Данные  представлены на сайте и в  « Акте заселения». Более подробные сведения можно узнать в отеле.</p>
                            </div>
                            <span class="caption-question" data-slug="{{ str_slug('Как оптимальнее всего совершить предварительную оплату за проживание?') }}">
                                <i class="fa fa-long-arrow-right" aria-hidden="true"></i> Как оптимальнее всего совершить предварительную оплату за проживание?</span>
                            <div class="wrap-p">
                                <p>Оплатить предварительно жилье можно в любом банке страны переводом или с помощью  интернет-платежей «Приват 24» ( для Украины),  с использование карт Visa, Mastercard через онлайн-сервис Platon  (для России и других зарубежных стран). Система безопасности интернет-платежей гарантирует конфиденциальность информации и безопасные платежи. ХХХ не берет дополнительную плату  за регистрацию брони.</p>
                            </div>
                            <span class="caption-question" data-slug="{{ str_slug('Как произвести оплату, если с  картой ничего не получается?') }}">
                                <i class="fa fa-long-arrow-right" aria-hidden="true"></i> Как произвести оплату, если с  картой ничего не получается?</span>
                            <div class="wrap-p">
                                <p>Введите  информацию  вручную и повторите платеж. Срок действия карты и CVV/CVC код - зачастую возникают ошибки при вводе этой числовой информации. Если вы самостоятельно не нашли причину нарушения работы карты банка, позвоните оператору нашего сервиса по телефону ХХХ. Звонки по Украине не оплачиваются.</p>
                            </div>
                            <span class="caption-question" data-slug="{{ str_slug('Как написать  мнение о гостинице?') }}"><i class="fa fa-long-arrow-right" aria-hidden="true"></i> Как написать  мнение о гостинице?</span>
                            <div class="wrap-p">
                                <p>Это можно сделать только, если вы  оставляли бронь через наш сайт. В дату отъезда  на почту  клиента придет письмо с просьбой оставить отзыв.</p>
                            </div>
                            <span class="caption-question" data-slug="{{ str_slug('Какие различия между гостиницами с моментальным  бронированием и  с помощью  оператора?') }}"><i class="fa fa-long-arrow-right" aria-hidden="true"></i> Какие различия между гостиницами с моментальным  бронированием и  с помощью  оператора?</span>
                            <div class="wrap-p">
                                <p>В первом случае заявка отправляется в сам отель, а на почту придет специальный ваучер о поселении. Такие гостиницы на сайте обозначаются иконкой. Второй вариант предусматривает  бронирование  через call-центр  сервиса. Заявка клиента отправляется оператору, он перезванивает через 20 минут и уточняет результат бронирования.</p>
                                <p>Бронирование гостиниц в Украине с через сервис <b>Hotels-ok</b> - отличное решение для позитивного отдыха.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@include('site.blocks.messages-phone')
@endsection

@section('javascript')
    @parent

    <script>
        var slug = '{{ $slug }}';
        if(slug) {
            var el = $('body').find('.caption-question[data-slug="'+ slug +'"]');
            $(el).next('.wrap-p').slideToggle();
        }
    </script>

@endsection