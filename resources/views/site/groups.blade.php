@extends('site.startpage')
@section('content')

    <section id="bgBussiness" class="bg-groups">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="bussiness-content">
                        <h1>{{ __('Бронирование отелей для групп') }}</h1>
                        <h2>{{ __('Сэкономьте свое время, мы всё сделаем за вас!') }}</h2>
                        <a href="javascript:void(0);" onclick="newFunction.scrollToElement('partnersApplication');">{{ __('Подать заявку') }}</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="bg-bussiness-overlay"></div>
    </section>
    <section id="plusesReservation" class="services-business">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="pluse-reservation-hotel bussiness-pluses">
                        <h2>{{ __('Наши услуги') }}</h2>
                        <span class="line-blue first-line-caption"></span>
                        <span class="line-blue two-line-caption"></span>
                        <div class="list-pluses">
                            <ul>
                                <li>
                                    <div class="one-pluse">
                                        <img src="/images/pluse-group-1.png" alt="">
                                        <div class="two-border-pluse"></div>
                                    </div>
                                    <span>Скидка на проживание 3-10% <br>для групп от 10 человек</span>
                                </li>
                                <li>
                                    <div class="one-pluse">
                                        <img src="/images/pluse-group-2.png" alt="">
                                        <div class="two-border-pluse"></div>
                                    </div>
                                    <span>Большой выбор гостиниц <br>для размещения групп</span>
                                </li>
                                <li>
                                    <div class="one-pluse">
                                        <img src="/images/pluse-group-3.png" alt="">
                                        <div class="two-border-pluse"></div>
                                    </div>
                                    <span>Бесплатный и быстрый поиск <br>подходящего отеля или хостела</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="aboutOfService">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>Об услуге</h2>
                    <span class="line-blue first-line-caption"></span>
                    <span class="line-blue two-line-caption"></span>
                    <p>Заполните форму бронирования ниже, и мы бесплатно подберем варианты отелей (хостелов), которые максимально
                        <br> соответствуют вашим пожеланиям. Если у вас планируется поездка, в которую собираются 10 и более человек,
                        <br> воспользуйтесь шансом сэкономить до 10% на стоимости отеля.</p>
                </div>
            </div>
        </div>
    </section>
    <section id="partnersApplication">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>Оставьте заявку</h2>
                    <span class="line-blue first-line-caption"></span>
                    <span class="line-blue two-line-caption"></span>
                    <div class="wrapper-form-partners-application">
                        <form action="" method="post">
                            <input type="text" name="name" placeholder="Имя">
                            <input type="text" name="amountPerson" placeholder="Кол-во людей">
                            <input type="text" name="city" placeholder="Город поездки">
                            <input type="text" name="phone" id="phoneCallback" placeholder="Телефон">
                            <input type="text" name="email" placeholder="Email">
                            {{ csrf_field() }}
                            <input type="submit" name="submit" value="Оставить заявку" onclick="newFunction.addApplicationGroup(event, this);">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="shares" class="shares-groups">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="shares-style">
                        <h2>Специальные предложения для групп от отелей </h2>
                        <span class="line-blue first-line-caption"></span>
                        <span class="line-blue two-line-caption"></span>
                        <div class="list-shares">
                            <ul>
                                <li>
                                    <div class="one-shares">
                                        <img src="/images/primer-shares-1.jpg" alt="">
                                        <div class="shares-price"><span>30%</span></div>
                                        <div class="overlay-share"></div>
                                        <a href="" class="btn-reservation-hotel">Забронировать</a>
                                    </div>
                                    <h4>Барон Мюнхаузен</h4>
                                    <span class="shares-price-hotel">300 грн / сутки</span>
                                    <p>Скидка для групп 30% , а также
                                        возможностью дополнительного
                                        трехразового питания. </p>
                                </li>
                                <li>
                                    <div class="one-shares">
                                        <img src="/images/primer-shares-2.jpg" alt="">
                                        <div class="shares-price"><span>30%</span></div>
                                        <div class="overlay-share"></div>
                                        <a href="" class="btn-reservation-hotel">Забронировать</a>
                                    </div>
                                    <h4>Барон Мюнхаузен</h4>
                                    <span class="shares-price-hotel">300 грн / сутки</span>
                                </li>
                                <li>
                                    <div class="one-shares">
                                        <img src="/images/primer-shares-1.jpg" alt="">
                                        <div class="shares-price"><span>30%</span></div>
                                        <div class="overlay-share"></div>
                                        <a href="" class="btn-reservation-hotel">Забронировать</a>
                                    </div>
                                    <h4>Барон Мюнхаузен</h4>
                                    <span class="shares-price-hotel">300 грн / сутки</span>
                                </li>
                                <li>
                                    <div class="one-shares">
                                        <img src="/images/primer-shares-1.jpg" alt="">
                                        <div class="shares-price"><span>30%</span></div>
                                        <div class="overlay-share"></div>
                                        <a href="" class="btn-reservation-hotel">Забронировать</a>
                                    </div>
                                    <h4>Барон Мюнхаузен</h4>
                                    <span class="shares-price-hotel">300 грн / сутки</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="howAreWorkProject" class="how-are-work-project-group">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>Как это работает</h2>
                    <span class="line-blue first-line-caption"></span>
                    <span class="line-blue two-line-caption"></span>
                    <div class="steps-how-are-work">
                        <div class="arrow-dashed first-arrow-dashed">
                            <img src="/images/arrow-dashed.png" alt="">
                        </div>
                        <div class="arrow-dashed second-arrow-dashed">
                            <img src="/images/arrow-dashed.png" alt="">
                        </div>
                        <ul>
                            <li>
                                <div class="one-step">
                                    <div class="wrapper-img-step">
                                        <img src="/images/how-are-work-step-1.png" alt="">
                                        <div class="first-border-step"><b>1</b>
                                            <div class="two-border-pluse"></div>
                                        </div>
                                    </div>
                                    <span>Вы добавляете свой <br> отель в наш каталог</span>
                                </div>
                            </li>
                            <li>
                                <div class="one-step">
                                    <div class="wrapper-img-step">
                                        <img src="/images/how-are-work-step-2.png" alt="">
                                        <div class="first-border-step"><b>2</b>
                                            <div class="two-border-pluse"></div>
                                        </div>
                                    </div>
                                    <span>Мы рассказываем <br> о вас всей Украине</span>
                                </div>
                            </li>
                            <li>
                                <div class="one-step">
                                    <div class="wrapper-img-step">
                                        <img src="/images/how-are-work-step-3.png" alt="">
                                        <div class="first-border-step"><b>3</b>
                                            <div class="two-border-pluse"></div>
                                        </div>
                                    </div>
                                    <span>Вы получаете <br> бронирования <br> и отзывы</span>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="reviews">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="reviews-style">
                        <h2>Отзывы клиентов</h2>
                        <span class="line-blue first-line-caption"></span>
                        <span class="line-blue two-line-caption"></span>
                        <div class="list-reviews">
                            <ul>
                                <li>
                                    <div class="one-review">
                                        <img src="/images/review-1.png" alt="">
                                        <span>Ирина Белая</span>
                                        <p>Впервые воспользовалась этим сайтом, и не пожалела!! Ребята сработали оперативно, нашли квартиру во Львове по искомым параметрам и по цене приемлемой для нашего скромного бюджета, очень быстро оформили </p>
                                    </div>
                                </li>
                                <li>
                                    <div class="one-review">
                                        <img src="/images/review-2.png" alt="">
                                        <span>Ирина Белая</span>
                                        <p>Во Вторые воспользовалась этим сайтом, и не пожалела!! Ребята сработали оперативно, нашли квартиру во Львове по искомым параметрам и по цене приемлемой для нашего скромного бюджета, очень быстро оформили </p>
                                    </div>
                                </li>
                                <li>
                                    <div class="one-review">
                                        <img src="/images/review-3.png" alt="">
                                        <span>Ирина Белая</span>
                                        <p>В третье воспользовалась этим сайтом, и не пожалела!! Ребята сработали оперативно, нашли квартиру во Львове по искомым параметрам и по цене приемлемой для нашего скромного бюджета, очень быстро оформили </p>
                                    </div>
                                </li>
                                <li>
                                    <div class="one-review">
                                        <img src="/images/review-3.png" alt="">
                                        <span>Ирина Белая</span>
                                        <p>В третье воспользовалась этим сайтом, и не пожалела!! Ребята сработали оперативно, нашли квартиру во Львове по искомым параметрам и по цене приемлемой для нашего скромного бюджета, очень быстро оформили </p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="arrows-reviews-carousel">
                            <a href="javascript:void(0);" onclick="newFunction.prevCarouselSlide();" class="arrow-carousel arrow-left-carousel"><img src="/images/arrow-left-carousel.png" alt=""></a>
                            <a href="javascript:void(0);" onclick="newFunction.nextCarouselSlide();" class="arrow-carousel arrow-right-carousel"><img src="/images/arrow-right-carousel.png" alt=""></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('site.blocks.messages-phone')
@endsection