<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta property="og:locale" content="ru_RU" />
    @if(Route::currentRouteName() == 'homepage')
        <meta property="og:type" content="website" />
    @else
        <meta property="og:type" content="article" />
    @endif
    <meta property="og:title" content="@if($seo) {{ $seo->title }} @endif" />
    <meta property="og:description" content="@if($seo){!! $seo->description !!}@endif" />
    <meta property="og:url" content="{{ Request::url() }}" />
    <meta property="og:site_name" content="{{ __('Сервис бесплатного бронирования в Украине') }}" />
    <meta property="og:image" content="{{ asset('images/logo.png') }}" />

    <meta property="og:image:secure_url" content="" />

    <meta name="twitter:card" content="summary" />
    <meta name="twitter:description" content="@if($seo){!! $seo->description !!}@endif" />
    <meta name="twitter:title" content="@if($seo) {{ $seo->title }} @endif" />
    <meta name="twitter:image" content="{{ asset('images/logo.png') }}" />


    <meta name="csrf-token" content="{!! csrf_token() !!}" />
    <link rel="stylesheet" href="{{ asset('css/bootstrap-theme.min.css') }}">
    @if(Route::currentRouteName() != 'category')
    <link rel="canonical" href="{{ url(Request::path()) }}" />
    @endif
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/font-awesome.css') }}">
    <link rel="stylesheet" href="{{ asset('css/daterangepicker.css') }}">
    <link rel="canonical" href="{{ url(Request::path()) }}">
    <link rel="stylesheet" href="{{ asset('css/reset.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/animate.css') }}">
    <link rel="stylesheet" href="{{ asset('css/jquery-ui.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/media.css') }}">
    <meta name="robots" content="noindex, nofollow">
    @section('meta')
        <meta name="keywords" content="@if($seo){!! $seo->keywords !!}@endif">
        <meta name="description" content="@if($seo){!! $seo->description !!}@endif">
        <title>@if($seo) {{ $seo->title }} @endif</title>
    @endsection
    @yield('meta')
</head>
<body>

@if(Route::currentRouteName() != 'hotel')

<script type="application/ld+json">
{
   "@context":"http://schema.org",
   "@type":"Organization",
   "location":{
      "@type":"Place",
      "address":{
         "@type":"PostalAddress",
         "addressCountry":"Украина",
         "addressLocality":"Парковая дорога, 16а, Киев, 01601, Украина"
      }
   },
   "brand":" Сервис бесплатного бронирования в Украине",
   "logo":"https://www.smileexpo.ru/public/upload/shows/kiev_2018_1516279124406_image.png",
   "telephone":"044 395 0696",
   "name":" Сервис бесплатного бронирования в Украине",
   "url":"https://site.ua/",
   "sameAs":[
      "https://www.facebook.com/ConferenceUkraine/",
      "https://vk.com/bbconfkiev",
      "https://twitter.com/Conf",
      "https://plus.google.com/u/0/+WorldEvents",
      "https://www.instagram.com/events/",
      "https://www.youtube.com/g26YXTF",
      "https://t.me/ConfUA"
   ],
   "department":[
      {
         "@context":"http://schema.org",
         "@type":"Organization",
         "location":{
            "@type":"Place",
            "address":{
               "@type":"PostalAddress",
               "addressLocality":"Парковая дорога, 16а, Киев, 01601, Украина",
               "streetAddress":"Парковая дорога, 16а, Киев, 01601, Украина",
               "postalCode":"01601"
            },
            "geo":{
               "@type":"GeoCoordinates",
               "latitude":"50.4495648",
               "longitude":"30.5403253"
            }
         },
         "telephone":"044 395 0696"
      }
   ]
}
</script>
@endif

<div id="wrapper">

        @php
            /*if(Cache::has('top-menu')){
                echo \Illuminate\Support\Facades\Cache::get('top-menu');
            }
            else {
                Cache::put('top-menu', view('site.blocks.top-menu')->render(), 10);
                echo \Illuminate\Support\Facades\Cache::get('top-menu');
            }*/

        @endphp
        @include('site.blocks.top-menu')
        @yield('top-block')

    @yield('content')
    @php
        /*if(Cache::has('footer')){
            echo \Illuminate\Support\Facades\Cache::get('footer');
        }
        else {
            Cache::put('footer', view('site.blocks.footer')->render(), 1);
            echo \Illuminate\Support\Facades\Cache::get('footer');
        }*/
        echo view('site.blocks.footer')->render();
    @endphp
    <div class="overlay" onclick="Popup.closePopup('nety');"></div>
</div>

<script src="{{ asset('js/jquery-3.1.1.min.js') }}"></script>
<script src="{{ asset('js/jquery-ui.min.js') }}"></script>
<script src="{{ asset('js/jquery.cookie.js') }}"></script>
<script src="{{ asset('js/moment.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/jquery.carouFredSel-6.2.1.js') }}"></script>
<script src="{{ asset('js/script.js') }}"></script>
<script src="{{ asset('js/daterangepicker.js') }}"></script>
<script src="{{ asset('js/jquery.maskedinput.min.js') }}"></script>

<script type="text/javascript">
    $(function(){
        $("#phoneCallback").mask("+38(999) 999-99-99");
        $(".popup-recall input").mask("+38(999) 999-99-99");
//        $("#phoneCallback1").mask("+38(999) 999-99-99");
    });
</script>

@yield('javascript')
@if(isset($page) && ($page == 'startPage' || $page == 'category' || $page == 'item'))
<script>

        function datePicker1(date = false) {

            @if(isset($dateFrom))
                d = '{{$dateFrom}}';
            @endif


            if(date) {
                d = date;
            }


            $('.datePicker1').daterangepicker({
                "startDate": d,
                "minDate": new Date(),
                "linkedCalendars": false,
                "autoApply": true,
                "alwaysShowCalendars": true,
                singleDatePicker:true,
                locale: {
                    format: 'DD.MM.YYYY',
                    cancelLabel: 'Clear'
                }
            }, function(start, end, label) {
                var startDate = start.format('YYYY.MM.DD');
                var currentDate = $('.datePicker1').val();
                var datePicker =  $('.datePicker2').val();

                var d = new Date(startDate);
                d.setDate(d.getDate() + 1);

                if(start.format('DD.MM.YYYY') >= datePicker) {
                    datePicker2(d);
                }

            });

        }

        function datePicker2(date = false) {
            var d = new Date();

            @if(isset($dateTo))
                d = '{{$dateTo}}';
            @elseif(isset($dateFrom))
                d = '{{$dateFrom}}';
            d.setDate(d.getDate() + 1);
            @else
                d.setDate(d.getDate() + 1);
            @endif


            if(date) {
                d = date;
            }


            $('.datePicker2').daterangepicker({
                "startDate": d,
                "minDate": d,
                "linkedCalendars": false,
                "autoApply": true,
                "alwaysShowCalendars": true,
                singleDatePicker:true, // если что убрать этот параметр если нужно показывать двойной календарь
                locale: {
                    format: 'DD.MM.YYYY',
                    cancelLabel: 'Clear'
                }
            }, function (start, end, label) {
                var startDate = start.format('YYYY.MM.DD');
                var d = new Date(startDate);
                var currentDate = $('.datePicker2').val();
                var datePicker =  $('.datePicker1').val();
                d.setDate(d.getDate() - 1);


                if(start.format('DD.MM.YYYY')<= datePicker) {
                    datePicker1(d);
                }

            });

        }

        datePicker1();
        datePicker2();



    
    $('.datePicker').on('apply.daterangepicker', function(ev, picker) {

        var input = ev.currentTarget;
        var inputClass = $(input).attr('class');
        if(inputClass == 'room-datepicker') {
            var dateFrom = picker.startDate.format('DD-MM-YYYY');
            var dateTo = picker.endDate.format('DD-MM-YYYY');
            var objectId = $('input[name="hotelsId"]').val();
            MainFunction.changeDateInObject(objectId, dateFrom, dateTo);
        }
//        
//  console.log(picker.startDate.format('YYYY-MM-DD'));
//  console.log(picker.endDate.format('YYYY-MM-DD'));
        });

    
</script>
@endif

<input type="hidden" name="localization" value="{{ App::getLocale() }}">

<script>
    $('.date-range-picker').daterangepicker({
        "showDropdowns": true,
        opens:'center',
        drops: 'down',
        autoApply: true,
        "locale": {
            format: 'DD.MM.YYYY',
            "daysOfWeek": [
                "Вс",
                "Пн",
                "Вт",
                "Ср",
                "Чт",
                "Пт",
                "Сб"
            ],
            "monthNames": [
                "Январь",
                "Февраль",
                "Март",
                "Апрель",
                "Май",
                "Июнь",
                "Июль",
                "Август",
                "Сентябрь",
                "Октябрь",
                "Ноябрь",
                "Декабрь"
            ],
        }
    });


    $('.date-range-picker').on('apply.daterangepicker', function(ev, picker) {
        newFunction.getBonusList();
        //сделать здесь вызов ajax
    });
</script>

<div class="registration-popup popup">
    <a href="javascript:void(0);" onclick="Popup.closePopupRegistration(this);" class="close-popup-registration">
        <img src="/images/close-popup-registration.png" alt="">
    </a>
    <div class="header-img-registration-popup"></div>
    <h4>{{ __('Акция') }}</h4>
    <span>{!! __('Каждому <b>зарегистрированному пользователю</b> на постоянной основе возвращаем <span>3%</span> от бронирования номеров на нашем сервисе!') !!}</span>
    <a class="registration-btn" href="{{ route('registration') }}">{{ __('Зарегистрироваться') }}</a>
    <div class="wrap-registration-sociallite">
        <h4>{{ __('Зарегистрироваться с помощью') }}</h4>
        <ul>
            <li><a href=""><i class="fa fa-facebook" aria-hidden="true"></i> Facebook</a></li>
            <li><a href=""><i class="fa fa-google-plus" aria-hidden="true"></i> Google</a></li>
        </ul>
    </div>
</div>

<div class="promocode-popup popup">
    <a href="javascript:void(0);" onclick="Popup.closePopupPromocode(this);" class="close-popup-registration">
        <img src="/images/close-popup-registration.png" alt="">
    </a>
    <div class="content-promocode">
        <h3>{{ __('Акция') }}</h3>
        <span>{{ __('Сегодня для Вас разыгрывается') }}</span>
        <div class="wrap-btn-promo">
            <a href="javascript:void(0);" class="promocode-style"><b>{{ __('промокод скидки') }} -</b> <span>{{ $procentPromocode }}%</span></a>
        </div>
        <span>{!! __('на <b>бронирование любого <br> номера</b> на нашем ресурсе') !!}</span>
        <a href="{{ route('quiz') }}" class="btn-rally">{{ __('Разыграть промокод') }}</a>
        <p>{{ __('Подписаться') }}</p>
        <ul>
            <li><a href=""><i class="fa fa-facebook"></i></a></li>
            <li><a href=""><i class="fa fa-google-plus"></i></a></li>
            <li><a href=""><i class="fa fa-instagram"></i></a></li>
            <li><a href=""><i class="fa fa-twitter"></i></a></li>
            <li><a href=""><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
        </ul>
    </div>
</div>
<input type="hidden" name="userIdRegistration" value="{{ isset(Auth::user()->id) ? Auth::user()->id : '' }}">
</body>

</html>
