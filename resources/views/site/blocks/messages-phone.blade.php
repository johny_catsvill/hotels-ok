<div class="messages-phones">
    <ul>
        <li>
            <a class="messages" href="javascript:void(0);" onclick="Popup.showRecallPopup();">
                <img src="/images/phone.png" alt="">
            </a>
        </li>
        <li>
            <a class="phone" href="javascript:void(0);" onclick="Popup.showPopup('popup-reviews-site', event)">
                <img src="/images/message.png" alt="">
            </a>
        </li>
    </ul>
</div>
<div class="popup-recall popup">
    <a href="javascript:void(0);" onclick="Popup.closePopup(this);" title="Закрыть" class="btn-close-popup">
        <img src="/images/close.png" alt="{{ __('Закрыть окно') }}">
    </a>
    <span>{{ __('Оставьте ваш номер телефона и мы перезвоним Вам в течении 20 минут') }}</span>
    <input type="text" name="phone" placeholder="{{ __('Ваш телефон') }}">
    <a href="javascript:void(0);" class="btn-send-call" onclick="newFunction.saveNumberCall();">{{ __('Жду звонка') }}</a>
</div>

<div class="popup-reviews-site popup">
    <a href="javascript:void(0);" onclick="Popup.closePopup(this);" title="Закрыть" class="btn-close-popup">
        <img src="/images/close.png" alt="{{ __('Закрыть окно') }}">
    </a>
    <h3>{{ __('Отзыв о сайте') }}</h3>
    <form action="">
        <input type="hidden" name="url" value="{{ url(Request::path()) }}">
        {{ csrf_field() }}
        <div class="wrapper-category-popup-review">
            <span class="select-input" onclick="newFunction.changeReviewCategory();">{{ __('Выберите категорию') }}</span>
            <div class="list-category-review-popup">
                <ul>
                    <li><a href="javascript:void(0);" onclick="newFunction.selectReviewCategory(this, 'positive');">{{ __('Позитивный') }}</a></li>
                    <li><a href="javascript:void(0);" onclick="newFunction.selectReviewCategory(this, 'negative');">{{ __('Негативный') }}</a></li>
                    <li><a href="javascript:void(0);" onclick="newFunction.selectReviewCategory(this, 'wishes');">{{ __('Пожелания') }}</a></li>
                    <li><a href="javascript:void(0);" onclick="newFunction.selectReviewCategory(this, 'error');">{{ __('Сообщение об ошибке') }}</a></li>
                </ul>
            </div>
        </div>
        <input type="hidden" name="type" value="">
        <span class="red red-error-popup">{{ __('Выберите категорию') }}</span>

        <input type="text" name="name" placeholder="{{ __('Имя') }}">
        <span class="red red-error-popup">{{ __('Укажите Ваше имя') }}</span>

        <input type="text" name="email" placeholder="Email">
        <span class="red red-error-popup">{{ __('Неккоректный email') }}</span>

        <textarea name="review" placeholder="{{ __('Вопрос') }}"></textarea>
        <span class="red red-error-popup">{{ __('Укажите текст сообщения(не меньше 10 символов)') }}</span>

        <a href="javascript:void(0);" class="btn-save-review-site" onclick="newFunction.saveReviewSite(this, event);">{{ __('Отправить') }}</a>
    </form>
</div>

<div class="success-reservation popup @if(Request::is('reservation')) reservation-popup @endif">
    <a href="javascript:void(0);" onclick="Popup.closePopup(this);" class="btn-close-popup">
        <img src="/images/close.png" alt="{{ __('Закрыть окно') }}">
    </a>
    <div class="content-popup">
        <h4>{{ __('Вы успешно отправили заявку!') }}</h4>
        <span>{{ __('Мы с Вами свяжемся в ближайщее время.') }}</span>
    </div>
</div>
