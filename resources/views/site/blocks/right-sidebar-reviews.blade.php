<div class="col-lg-3">
    <div class="reviews-sidebar">
        <div class="viber-project">
            <img src="/images/viber.jpg" alt="">
        </div>
    </div>
    <div class="contacts-project">
        <span>{{ __('Контакты') }}</span>
        <ul>
            <li><img src="/images/phone-contacts.png" alt=""> 0 800 800-000</li>
            <li><img src="/images/message-contacts.png" alt=""> info@hotels-ok.com</li>
            <li><img src="/images/map-contacts.png" alt=""> ул. Иванова, 35</li>
        </ul>
        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d81128.91942808744!2d30.74976742792969!3d50.547586326303396!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sru!2sua!4v1517518256666" width="100%" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>
        <span class="padding-top-25px">{{ __('О нас') }}</span>
        <p> {{ __('Бесплатный сервис бронирования отелей по всей Украине. Наша команда — это более 60 сотрудников, которых объединяет одна цель: помочь вам остановиться в любом уголке нашей страны. Шикарный отель или недорогой хостел? Город-миллионник или небольшой поселок? Отдых или командировка? Вы получите только лучшие предложения отелей.') }}</p>
    </div>
</div>