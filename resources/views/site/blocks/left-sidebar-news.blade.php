<div class="col-lg-2">
    <span class="caption-list">{{ __('Архив') }}</span>
    <ul class="list-link-news">
        @foreach($categoriesYear as $year)
            <li>
                <a href="{{ route('news', $year->year) }}">{{ $year->year }} {{ __('г') }} <span>{{ $year->countNews }}</span></a>
            </li>
        @endforeach
    </ul>

    <span class="caption-list">{{ __('Категории') }}</span>
    <ul class="list-link-news">
        @foreach($categories as $category)
        <li>
            <a @if(Request::is('news/'.$category->slug)) class="active" @endif href="{{ route('news', $category->slug) }}">{{ $category->name }} <span>{{ $category->news_count }}</span></a>
        </li>
        @endforeach
    </ul>
</div>