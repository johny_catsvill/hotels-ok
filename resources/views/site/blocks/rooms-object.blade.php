@if($rooms)
    <div class="list-rooms-hotel">
        <ul>
            @foreach($rooms as $room)
                <li data-room-id="{{ $room->id }}">
                    <div class="one-room-this-hotel">
                        <ul>
                            <li>
                                <div class="one-shares">
                                    <img class="main-img-room" src="/{{ $room->firstImage }}" alt="">
                                    <input type="hidden" name="arrayImages" value="{{ implode(',', $room->images) }}">
                                    @if(isset($room->isDiscount))
                                        <div class="shares-price"><span>{{ $room->isDiscount }}%</span></div>
                                    @endif
                                    <div class="overlay-share"></div>
                                    <a href="javascript:void(0);" class="btn-reservation-hotel" onclick="Popup.showGalleryImgPopup(this);">{{ __('Еще фото') }}</a>
                                </div>
                            </li>
                            <li>
                                <span class="name-room-hotel"><strong>{{ $room->name }}</strong>
                                    <span class="price-room-hotel">
                                        @if($room->priceWithDiscount)
                                            <span class="price-without-discount">{{ $room->priceWithoutDiscount }}</span>
                                            <b> {{ $room->priceWithDiscount }} грн</b>
                                        @else
                                            <b> {{ $room->priceWithoutDiscount }} грн</b>
                                        @endif
                                        / за {{ $period }} дн.
                                    </span>
                                </span>

                                <ul class="list-pluses-room">
                                    @foreach($room->pluses()->where('image', '!=', '')->limit(10)->get() as $pluse)
                                        <li data-title="{{ __($pluse->name) }}"><img src="/images/icon-pluses-room/{{ $pluse->image }}" alt=""></li>
                                    @endforeach
                                    @if($room->amountPerson >= 5)
                                     <li class="float-right" data-title="{{ $room->amountPerson }} {{ trans_choice('localization.countPeople', $room->amountPerson) }}"><img src="/images/icon-people/5.png" alt=""></li>
                                    @else
                                        <li class="float-right" data-title="{{ $room->amountPerson }} {{ trans_choice('localization.countPeople', $room->amountPerson) }}">
                                            <img src="/images/icon-people/{{ $room->amountPerson }}.png" alt=""></li>
                                    @endif
                                    <div class="clear"></div>
                                </ul>
                                <div class="what-is-the-room padding-bottom-20px">
                                    <b>{{ __('В номере') }}</b>
                                    <ul>
                                        @foreach($room->pluses as $pluse)
                                            @if($loop->index == 6)
                                                @break
                                            @endif
                                            <li><i class="fa fa-circle" aria-hidden="true"></i> <span class="display-inline-block">{{ __($pluse->name) }}</span></li>
                                        @endforeach

                                    </ul>
                                </div>
                                <a href="javascript:void(0);" onclick="newFunction.showMoreInfoRoom(this);" class="btn-more-room" data-show="off">{{ __('Подробнее о номере') }}</a>
                                <a href="javascript:void(0);" class="btn-reservation float-right" onclick="Reservation.reservationRoom(this);" data-room-id="{{ $room->id }}">{{ __('Забронировать') }}</a>
                                <a href="javascript:void(0);" class="btn-reservation-success" onclick="Popup.showPopup('popup-reservation-rooms', false);">{{ __('Просмотреть бронь') }}</a>
                            </li>
                        </ul>
                        <div class="clear"></div>
                        <div class="other-info-about-room">
                            <p>{{$room->infoAboutRoom}}</p>
                            <ul>
                                @foreach($room->categoryPluses as $category)
                                    @if(count($category->pluses) > 0)
                                        <li><b>{{ __($category->name) }}:</b> {{ $category->pluses->implode('name', ', ') }}</li>
                                    @endif
                                @endforeach
                            </ul>
                        </div>
                        <div class="clear"></div>
                    </div>
                </li>
            @endforeach
        </ul>
    </div>
    <input type="hidden" name="order" value="">
@else
    <p class="error-find-hotel">{{ __('На выбранные вами даты, свободных номеров нет. Выберите другие даты.') }}</p>
@endif