<ul class="wrapper-list-objects-js">
    @foreach($listObjects as $object)
        @php
            $objectModel = \App\Models\ObjectHotel::find($object->objectId);
            $images = \Illuminate\Support\Facades\Storage::files('/images/hotels/'.$object->objectId.'-'.$object->subdomain.'/images-objects');
            foreach($images as $key => $im) {
                if(strpos($im, '_') !== false) {
                    unset($images[$key]);
                }
            }

        @endphp
        <li class="wrapper-object">
            <div class="one-room-this-hotel">
                <ul>
                    <li>
                        <div class="one-shares">
                            <a target="_blank" href="{{ route('hotel', ['id' => $object->objectId, 'name' => $object->subdomain]) }}" data-img-array="{{ implode(',', $images) }}">
                                <img src="/images/hotels/{{ $object->objectId.'-'.$object->subdomain.'/'.\App\Traits\croppedPhotos::changeImg($object->image) }}" alt="">
                            </a>
                            @if($object->valuePercent)
                                <div class="shares-price"><span>{{ $object->valuePercent }}%</span></div>
                            @endif
                        </div>
                    </li>
                    <li>
                                                <span class="name-room-hotel"><strong>
                                                        <a target="_blank" href="{{ route('hotel', ['id' => $object->objectId, 'name' => $object->subdomain]) }}">{{ $object->objectName }}
                                                            <span>({{ __($objectModel->objectCategory->name) }})</span></a>
                                                    </strong>
                                                    @if($objectModel->reviews->count())
                                                        <span class="price-room-hotel"><b>{{ $objectModel->reviews->avg('avgValue') }}</b> {{ $objectModel->reviews->count() }}
                                                            {{ trans_choice('localization.reviews', $objectModel->reviews->count()) }}</span>
                                                    {{--@else--}}
                                                        {{--<span class="price-room-hotel">Без отзывов</span>--}}
                                                    @endif
                                                </span>
                        <span class="address-object">
                                                    @for($i = 0; $i < 5; $i++)
                                @if($object->stars > $i)
                                    <i class="fa fa-star active-star" aria-hidden="true"></i>
                                @else
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                @endif
                            @endfor

                            <a href="javascript:void(0);" onclick="newFunction.showPopupMoreInfo({{ $object->objectId }}, 4);">{{ $object->address }}
                                <i class="fa fa-map-marker" aria-hidden="true"></i></a>
                        </span>
                        @if(isset($object->newDistance))

                            @if($object->newDistance * 1000 < 1000)
                                <span class="distance-in-metre">{{ __('Находиться') }} в {{ $object->newDistance * 1000 }} м {{ __('от Вас') }}</span>
                            @else
                                <span class="distance-in-metre">{{ __('Находиться') }} в {{ $object->newDistance }} км {{ __('от Вас') }}</span>
                            @endif

                        @endif
                        <ul class="list-pluses-room @if(count($objectModel->objectPluses) == 0) border-top-none margin-0px padding-0px @endif">
                            @foreach($objectModel->objectPluses as $objectPluse)
                                <li data-title="{{ $objectPluse->name }}"><img src="/images/icon-pluses/{{ $objectPluse->image }}" alt=""></li>
                            @endforeach

                            @if(count($objectModel->serviceRelation) > 0)
                                @foreach($objectModel->serviceRelation as $service)
                                    <li class="service-object" data-title="Бронирование без комиссии"><img src="/images/free.png" alt=""></li>
                                    <li class="service-object" data-title="Мгновенное подтверждение"><img src="/images/time.png" alt=""></li>
                                @endforeach
                            @endif
                        </ul>
                        <div class="what-is-the-room">
                            <p>{{ \App\Helpers\Func::getOpening($object->infoAboutObject) }}</p>
                        </div>
                        <table>
                            <tr>
                                <td><div class="wrap-price-hotel">
                                        <b>от {{ round($object->minPrice, 0) }} грн </b><span> / {{ __('сутки') }}</span>
                                    </div></td>
                                <td>
                                    <a class="btn-preview" href="javascript:void(0);" onclick="newFunction.showPopupMoreInfo({{ $object->objectId }}, 1);">Быстрый просмотр</a>
                                </td>
                                <td><a target="_blank" href="{{ route('hotel', ['id' => $object->objectId, 'name' => $object->subdomain]) }}" class="btn-reservation">{{ __('Забронировать') }}</a></td>
                            </tr>
                        </table>

                    </li>
                </ul>
                <div class="clear"></div>
            </div>
        </li>
    @endforeach
</ul>

<script>
    function initMapCategory() {
        console.log('fdf');
        var uluru = {lat: {{ $city->longitudeGps }}, lng: {{ $city->latitudeGps }}};
        var map1 = new google.maps.Map(document.getElementById('map1'), {
            zoom: 8,
            center: uluru
        });
        var infowindowGlobal = new google.maps.InfoWindow();

        @foreach($allObjectsSession as $obj)
            @if($obj->longitudeGps && $obj->latitudeGps)
        var finding = {lat: {{ $obj->longitudeGps }}, lng: {{ $obj->latitudeGps }}};

        var marker{{ $loop->iteration }} = new google.maps.Marker({
            position: finding,
            map: map1,
            title: '{{ $obj->objectName }}'
        });

        var contentString{{ $loop->iteration }} = '<div class="">{{ $obj->infoAboutObject }}</div>';

        var infowindow{{ $loop->iteration }} = new google.maps.InfoWindow({
            content: contentString{{ $loop->iteration }},
            maxWidth: 300
        });

        marker{{ $loop->iteration }}.addListener('click', function() {
            infowindowGlobal.close();
            $(".gm-style-iw").next().click();
            infowindow{{ $loop->iteration }}.open(map1, marker{{ $loop->iteration }});
        });

        @endif
        @endforeach

        ////                var place = autocomplete.getPlace();
        ////                var _coordinates = place.geometry.location; //a google.maps.LatLng object
        //                var _kCord = new google.maps.LatLng(50.4726761, 30.5160402);
        //                var _pCord = new google.maps.LatLng(50.46461, 30.5149273);
        //
        ////                var distance = (google.maps.geometry.spherical.computeDistanceBetween(_kCord, _pCord) / 1000).toFixed(3);
        ////                console.log(distance);

        google.maps.event.trigger(map1, "resize");
    }
</script>
