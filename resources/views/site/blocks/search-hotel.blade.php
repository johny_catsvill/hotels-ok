                    <div class="search-form">
                        <form action="{{ route('category') }}" method="get" name="topFilter">
                            <div class="field-search search-string-style">
                                <input type="text" name="search" autocomplete="off" placeholder="{{ __('Город или отель') }}" value="@if(isset($city) && $city){{ $city->name }}@elseif(isset($region) && $region){{ $region->name }} @endif" onkeyup="Search.timeOutSearchObject(event, this);" onfocus="Search.timeOutSearchObject(event, this);">
                                <div class="result-search"></div>
                                <span class="error-search-home hide">{{ __('Город или регион не выбран!') }}</span>
                            </div>
                            <div class="field-search arrival-style">
                                <input type="text" name="dateFrom" class="datePicker1" placeholder="{{ __('Заезд') }}" value="{{ $dateFrom }}">
                                <i class="fa fa-calendar" aria-hidden="true"></i>
                            </div>
                            <div class="field-search arrival-style">
                                <input type="text" name="dateTo" class="datePicker2" placeholder="{{ __('Выезд') }}" value="{{ $dateTo }}">
                                <i class="fa fa-calendar" aria-hidden="true"></i>
                            </div>
                            <div class="field-search guest-field-style">
                                <span onclick="newFunction.showPopupGuest();">{{ __('Гостей') }}: <span>1</span></span>
                                <div class="block-count-guest">
                                    <ul>
                                        <li>
                                            <a href="javascript:void(0);" onclick="newFunction.deacrementCountGuest();"><i class="fa fa-minus" aria-hidden="true"></i></a>
                                        </li>
                                        <li>
                                            @if(isset($data['guest']))
                                                <input type="text" name="guest" value="{{$data['guest']}}">
                                            @else
                                                <input type="text" name="guest" value="1">
                                            @endif
                                        </li>
                                        <li>
                                            <a href="javacript:void(0);" onclick="newFunction.incrementCountGuest();"><i class="fa fa-plus" aria-hidden="true"></i></a>
                                        </li>
                                        <li class="clear"></li>
                                    </ul>

                                </div>
                                <i class="fa fa-angle-down" aria-hidden="true"></i>
                            </div>
                            @if(isset($city) && $city)
                                <input type="hidden" name="category" class="field-category-search" value="city">
                            @elseif(isset($region) && $region)
                                <input type="hidden" name="category" class="field-category-search" value="region">
                            @endif
                            <input type="hidden" name="countryId" class="js-select-country-id" value="1">
                            @if(isset($city) && $city)
                                <input type="hidden" name="cityId" class="select-category-and-id" value="{{ $city->id }}">
                            @elseif(isset($region) && $region)
                                <input type="hidden" name="regionId" class="select-category-and-id" value="{{ $region->id }}">
                            @else
                                <input type="hidden" class="select-category-and-id" name="" value="">
                            @endif
                                <a href="javascript:void(0);" class="btn-submit-search" onclick="Filter.searchHomeFilter();">{{ __('Поиск') }}</a>
                        </form>
                    </div>