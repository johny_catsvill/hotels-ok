
    <div class="left-sidebar-category">
        <form>
            <div class="stars-filter filter-category">
                <b class="caption-filter" onclick="newFunction.showFilter(this);">{{ __('Рейтинг') }}<i class="fa fa-angle-down arrow-down"></i></b>
                <hr>
                <table>
                    @foreach($stars as $star)
                        <tr>
                            <td><label for="" class="label-checkbox-agree">
                                    <input type="checkbox" @if(is_array($starsArray) && in_array($star->stars, $starsArray)) checked @endif onchange="Filter.categoryFIlter();" name="stars[]" value="{{ $star->stars }}" @if($star->objects_count == 0) disabled class="background-gray" @endif>
                                    <span class="stars-filter-icon">
                                                    @for($i = 0; $i < $star->stars; $i++)
                                            <i class="fa fa-star @if($star->stars != 0) active-star @endif" aria-hidden="true"></i>
                                        @endfor
                                                </span>
                                </label>
                            </td>
                            <td>
                                                <span class="name-filter @if($star->objects_count == 0) color-gray text-decoration-line-through @endif">
                                                    @if($star->stars == 0)
                                                        {{ __('Без звезд') }}
                                                    @else
                                                        {{ $star->stars }} {{ __('звезд') }}
                                                    @endif
                                                </span>
                            </td>
                            <td><span class="count-object-filter">{{ $star->objects_count }}</span></td>
                        </tr>
                    @endforeach
                </table>
            </div>

            <div class="type-object filter-category">
                <b class="caption-filter" onclick="newFunction.showFilter(this);">{{ __('Цена (за ночь)') }} <i class="fa fa-angle-down arrow-down"></i></b>
                <hr>
                <table>
                    @foreach($prices as $key => $price)
                        <tr>
                            <td>
                                <label for="" class="label-checkbox-agree">
                                    <input type="checkbox" onchange="Filter.categoryFIlter();" @if(isset($price['checked'])) checked @endif name="prices[]" value="{{ $key }}" @if(!isset($price['info'])) disabled class="background-gray" @endif>
                                </label>
                            </td>
                            <td>
                                <span class="name-filter @if(!isset($price['info'])) color-gray text-decoration-line-through @endif">{{ __($price['title']) }}</span>
                            </td>
                            <td><span class="count-object-filter">@if(isset($price['info'])) {{ $price['info']->countObject}} @else 0 @endif</span></td>
                        </tr>
                    @endforeach
                </table>
            </div>


            <div class="type-object filter-category">
                <b class="caption-filter" onclick="newFunction.showFilter(this);">{{ __('Типы размещения') }}<i class="fa fa-angle-down arrow-down"></i></b>
                <hr>
                <table>
                    @foreach($typeObjects as $type)
                        <tr>
                            <td>
                                <label for="" class="label-checkbox-agree">
                                    <input type="checkbox" @if(is_array($typeObjectArray) && in_array($type->id, $typeObjectArray)) checked @endif onchange="Filter.categoryFIlter();" name="typeObject[]" value="{{ $type->id }}" @if($type->objects_count == 0) disabled class="background-gray" @endif>
                                </label>
                            </td>
                            <td>
                                <span class="name-filter @if($type->objects_count == 0) color-gray text-decoration-line-through @endif">{{ __( $type->name) }}</span>
                            </td>
                            <td><span class="count-object-filter">{{ $type->objects_count }}</span></td>
                        </tr>
                    @endforeach
                </table>
            </div>
            <div class="type-object filter-category">
                <b class="caption-filter" onclick="newFunction.showFilter(this);">{{ __('Питание') }} <i class="fa fa-angle-down arrow-down"></i></b>
                <hr>
                <table>
                    @foreach($foods as $food)
                        <tr>
                            <td>
                                <label for="" class="label-checkbox-agree">
                                    <input type="checkbox" @if(is_array($foodArray) && in_array($food->id, $foodArray)) checked @endif onchange="Filter.categoryFIlter();" name="food[]" value="{{ $food->id }}" @if($food->objects_count == 0) disabled class="background-gray" @endif>
                                </label>
                            </td>
                            <td>
                                <span class="name-filter @if($food->objects_count == 0) color-gray text-decoration-line-through @endif">{{ __($food->name) }}</span>
                            </td>
                            <td><span class="count-object-filter">{{ $food->objects_count }}</span></td>
                        </tr>
                    @endforeach
                </table>
            </div>
            @if($services)
                <div class="type-object filter-category">
                    <b class="caption-filter" onclick="newFunction.showFilter(this);">{{ __('Условия') }} <i class="fa fa-angle-down arrow-down"></i></b>
                    <hr>
                    <table>
                        @foreach($services as $service)
                            <tr>
                                <td>
                                    <label for="" class="label-checkbox-agree">
                                        <input type="checkbox" @if(is_array($servicesArray) && in_array($service->id, $servicesArray)) checked @endif onchange="Filter.categoryFIlter();" name="services[]" value="{{ $service->id }}" @if($service->service_relation_count == 0) disabled class="background-gray" @endif>
                                    </label>
                                </td>
                                <td>
                                    <span class="name-filter @if($service->service_relation_count == 0) color-gray text-decoration-line-through @endif">{{ __($service->name) }}</span>
                                </td>
                                <td><span class="count-object-filter">{{ $service->service_relation_count }}</span></td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            @endif
            <div class="type-object filter-category">
                <b class="caption-filter" onclick="newFunction.showFilter(this);">{{ __('Услуги') }} <i class="fa fa-angle-down arrow-down"></i></b>
                <hr>
                <table>
                    @foreach($typePluses as $pluse)
                        <tr>
                            <td>
                                <label for="" class="label-checkbox-agree">
                                    <input type="checkbox" @if(is_array($plusesArray) && in_array($pluse->id, $plusesArray)) checked @endif onchange="Filter.categoryFIlter();" name="pluses[]" value="{{ $pluse->id }}" value="{{ $pluse->id }}" @if($pluse->objects_count == 0) disabled class="background-gray" @endif>
                                </label>
                            </td>
                            <td>
                                <span class="name-filter @if($pluse->objects_count == 0) color-gray text-decoration-line-through @endif">{{ __($pluse->name) }}</span>
                            </td>
                            <td><span class="count-object-filter">{{ $pluse->objects_count }}</span></td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </form>
    </div>