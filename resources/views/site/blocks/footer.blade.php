<section id="footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="footer-style">
                    <div class="col-lg-3 col-sm-3">
                        <div class="logo-footer">
                            <a href="{{ route('homepage') }}"><img src="/images/120px-01.png" alt=""></a>
                            <p>{!! __('Бесплатный сервис бронирования отелей по всей Украине. <br> Остановитесь в любом уголке нашей страны!') !!}</p>
                        </div>
                    </div>
                    <div class="col-lg-1 col-sm-0"></div>
                    <div class="col-lg-2 col-sm-2">
                        <h4>{{ __('О нас') }}</h4>
                        <ul>
                            <li class="about-company-footer"><a @if(Request::is('about-company')) class="active" @endif href="{{ route('about-company') }}">{{ __('О компании') }}</a></li>
                            <li class="contacts-footer"><a @if(Request::is('contacts')) class="active" @endif href="{{ route('contacts') }}">{{ __('Контакты') }}</a></li>
                            <li class="faq-footer"><a @if(Request::is('questions')) class="active" @endif href="{{ route('questions') }}">FAQ</a></li>
                            <li class="guarantees-footer"><a @if(Request::is('guarantees')) class="active" @endif href="{{ route('guarantees') }}">{{ __('Гарантии') }}</a></li>
                            <li class="news-footer"><a @if(Request::is('news/*')) class="active" @endif href="{{ route('news') }}">{{ __('Новости') }}</a></li>
                            <li class="reviews-footer"><a @if(Request::is('reviews')) class="active" @endif href="{{ route('reviews') }}">{{ __('Отзывы') }}</a></li>
                        </ul>
                    </div>
                    <div class="col-lg-2 col-sm-3">
                        <h4>{{ __('Услуги') }}</h4>
                        <ul>
                            <li class="hotels-footer"><a href="">{{ __('Гостиницы Украины') }}</a></li>
                            <li class="hotels-footer"><a href="">{{ __('Хостелы Украины') }}</a></li>
                            <li class="hotels-footer"><a href="">{{ __('Квартиры Украины') }}</a></li>
                            <li class="sea-hotel-footer"><a href="">{{ __('Отдых на море') }}</a></li>
                            <li class="mountain-hotel-footer"><a href="">{{ __('Отдых в горах') }}</a></li>
                            <li class="recovery-hotel-footer"><a href="">{{ __('Оздоровление') }}</a></li>
                        </ul>
                    </div>
                    <div class="col-lg-2 col-sm-2">
                        <h4>{{ __('Партнерам') }}</h4>
                        <ul>
                            <li class="partners-footer"><a @if(Request::is('partners')) class="active" @endif href="{{ route('partners') }}">{{ __('Для партнеров') }}</a></li>
                            <li class="bussiness-footer"><a @if(Request::is('bussiness')) class="active" @endif href="{{ route('bussiness') }}">{{ __('Для юридических лиц') }}</a></li>
                            <li class="groups-footer"><a @if(Request::is('groups')) class="active" @endif href="{{ route('groups') }}">{{ __('Для групп') }}</a></li>
                        </ul>
                    </div>
                    <div class="col-lg-2 col-sm-2">
                        <h4 class="contacts-h4">{{ __('Контакты') }}</h4>
                        <p class="mobile-p">{!! __('Бесплатный сервис бронирования отелей по всей Украине. Остановитесь в любом уголке <br> нашей страны!') !!}</p>
                        <ul class="contacts-style">
                            <li>0 800 800-800</li>
                            <li>info@hotels-ok.com</li>
                        </ul>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
<section id="copyright">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="copyright-style">
                    <span>{{ __('Copyright © hotels-ok.com, 2017. Все права защищены') }}</span>
                    <ul>
                        <li><a href=""><i class="fa fa-facebook"></i></a></li>
                        <li><a href=""><i class="fa fa-google-plus"></i></a></li>
                        <li><a href=""><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                        <li><a href=""><i class="fa fa-twitter"></i></a></li>
                        <li><a href=""><i class="fa fa-linkedin"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>