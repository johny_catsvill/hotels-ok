<div class="list-population-hotels-in-object">
    <div class="items-category">
        @foreach($shortInfoAboutObject as $object)
        <div class="item">
            <img src="/images/hotels/{{ $object->id. '-'. $object->url .'/'. $object->image }}" width="100%" height="100%" alt="">
            <div class="price-hotel">
                <span class="new-price-hotel">от {{ $object->minRoomPrice }} грн</span>
            </div>
            <div class="overlay-hotel"></div>
            <a href="/hotel/{{$object->id .'-'. $object->url}}" class="reservation-hotel">{{ __('Открыть') }}</a>
            <div class="name-hotel">
                <span>{{ $object->name }}</span>
                <div class="raiting-hotel">
                    @for($i = 0; $i < $object->stars; $i++)
                    <i class="fa fa-star" aria-hidden="true"></i>
                    @endfor
                </div>
            </div>
            <div class="city-hotel">г. {{ $object->cityName }}</div>
            <div class="pluses-hotel">
                    @foreach($object->arrayPluses as $key => $pluse)
                    <i class="fa fa-{{ trim($key) }}" title="{{ $pluse }}" aria-hidden="true"></i>
                    @endforeach
            </div>
            <!--                                <div class="more-information-about-hostel"></div>-->
        </div>
        @endforeach
        <div class="clear"></div>
    </div>
</div>
