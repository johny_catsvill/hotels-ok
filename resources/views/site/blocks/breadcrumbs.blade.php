
@if( isset($breadcrumbs))

    <div class="list-breadcrumbs">
        <ul>
            @foreach($breadcrumbs as $key => $breadcrumb)
                <li>

                    @if($loop->last)
                        <span>{{ __($key) }}</span>
                    @else
                        <a href="{{ $breadcrumb }}">{{ __($key) }}</a>
                    @endif
                </li>
                @if(!$loop->last)
                    <li>/</li>
                @endif


            @endforeach
        </ul>

    </div>
    @endif