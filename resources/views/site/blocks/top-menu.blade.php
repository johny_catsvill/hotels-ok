<section id="topMenu" class="bottom-shadow">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-xs-9 col-sm-2">
                <div class="logo">
                    <a href="{{ route('homepage') }}"><img src="/images/logo.png" alt=""></a>
                </div>
            </div>
            <div class="col-xs-3 wrapper-bar">
                <div class="bars-top">
                    <a href="javascript:void(0);" onclick="newFunction.showTopMenuMobile(this);" data-active="off">
                        <i></i>
                        <i></i>
                        <i></i>
                    </a>
                </div>
                <div class="clear"></div>
            </div>

            <div class="col-lg-9 col-sm-10 wrapper-top-menu">
                <div class="top-menu">
                    <ul>
                        <li><a @if(Request::is('/')) class="active" @endif href="{{ route('homepage') }}">{{ __('Отели') }}</a></li>
                        <li><a @if(Request::is('about-company')) class="active" @endif href="{{ route('about-company') }}">{{ __('О нас') }}</a></li>
                        <li><a @if(Request::is('news')) class="active" @endif href="{{ route('news') }}">{{ __('Новости') }}</a></li>
                        <li><a @if(Request::is('partners')) class="active" @endif href="{{ route('partners') }}">{{ __('Партнерам') }}</a></li>
                        <li><a @if(Request::is('contacts')) class="active" @endif href="{{ route('contacts') }}">{{ __('Контакты') }}</a></li>
                        <li class="font-bold">
                            <a href="javacript:void(0);"><img src="/images/phone-icon.png" alt=""> 096 500 40 30
                                <i class="fa fa-angle-down"></i>
                                <ul class="sub-menu-phone">
                                    <li>096 500 40 30</li>
                                    <li>096 500 40 30</li>
                                    <li class="clear"></li>
                                </ul>
                            </a>

                        </li>

                        @if(Auth::user())

                            @php
                                $user = \App\Models\User::find(Auth::id());
                            @endphp
                        @endif

                        <li class="text-transform-normal @if(isset($user)) auth-block @else width-auto @endif">
                            @if(isset($user) && $user->clientId)
                                <span class="link-auth">
                                    <img src="/images/user-icon.png" alt=""> <span>{{ str_limit($user->client->name, 10) }}</span>
                                    <ul class="sub-menu-user">
                                        <li><a href="{{ route('user.profile') }}">{{ __('Личный кабинет') }}</a></li>
                                        <li><a href="{{ route('user.reservations') }}">{{ __('Бронирования') }}</a></li>
                                        <li><a href="{{ route('user.bonus') }}">{{ __('Бонусы') }} </a></li>
                                        {{--<li><a href="{{ route('user.settings') }}">{{ __('Настройки') }}</a></li>--}}
                                        <li><a href="{{ route('logout') }}">{{ __('Выход') }}</a></li>
                                    </ul>
                                </span>
                            @elseif(isset($user) && $user->roleId)
                                <a href="{{ route('administrator-home') }}" class="link-auth"><img src="/images/user-icon.png" alt=""> {{ str_limit('Administrator', 10) }}</a>
                            @else
                                <a href="{{ route('login') }}" class="link-auth"><img src="/images/user-icon.png" alt=""> {{ __('Войти') }}</a>
                            @endif
                        </li>
                        <li class="language-list">
                            @if(App::isLocale('ru'))
                                <a href="{{ route('setlocale', ['lang' => 'uk']) }}" class="active font-bold">UK</a>
                            @else
                                <a href="{{ route('setlocale', ['lang' => 'ru']) }}" class="active font-bold">RU
                                </a>
                            @endif
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="overlay-top-menu"></div>
</section>
<div class="mobile-top-menu">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-xs-9 col-sm-2">
                <div class="logo">
                    <a href="{{ route('homepage') }}"><img src="/images/120px-01.png" alt=""></a>
                </div>
            </div>
            <div class="col-xs-3 wrapper-bar">
                <div class="bars-top close-bars-top">
                    <a href="javascript:void(0);" onclick="newFunction.showTopMenuMobile(this);" data-active="on">
                        <i></i>
                        <i></i>
                        <i></i>
                    </a>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </div>
    <div class="block-menu">
        <h4>{{ __('Настройки') }}</h4>
        <ul>
            <li><a href="" data-locale="RU" class="change-locale-btn">{{ __('Сменить язык') }}</a></li>
            <li><a class="auth-mobile-menu" href="{{ route('registration') }}">{{ __('Войдите или создавайте профиль') }}</a></li>
        </ul>
    </div>
    <div class="block-menu">
        <h4>{{ __('Полезные ссылки') }}</h4>
        <ul>
            <li><a class="hotels-mobile-menu" href="{{ route('homepage') }}">{{ __('Отели') }}</a></li>
            <li><a class="about-mobile-menu" href="{{ route('about-company') }}">{{ __('О нас') }}</a></li>
            <li><a class="news-mobile-menu" href="{{ route('news') }}">{{ __('Новости') }}</a></li>
            <li><a class="partners-mobile-menu" href="{{ route('partners') }}">{{ __('Партнерам') }}</a></li>
            <li><a class="contacts-mobile-menu" href="{{ route('contacts') }}">{{ __('Контакты') }}</a></li>
        </ul>
    </div>
    <span>Сервис бесплатного брованировая</span>
    <strong>096 500 40 30</strong>
</div>