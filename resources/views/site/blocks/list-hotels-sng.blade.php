                           @foreach($objects as $object)
                                <div class="item">
                                    <img src="images/{{ $object->image }}" width="100%" height="100%" alt="">
                                    <div class="price-hotel">
                                        <span class="new-price-hotel">от {{ $object->priceRoom }} грн</span>
                                    </div>
                                    <div class="overlay-hotel"></div>
                                    <a href="/hotel/{{ $object->id.'-'.$object->url }}" class="reservation-hotel">{{ __('Открыть') }}</a>
                                    <div class="name-hotel">
                                        <span>{{ $object->name }}</span>
                                    </div>
                                    <div class="city-hotel">г. {{ $object->cityName }}</div>
                            </div>
                            @endforeach    
                            
                        
                    