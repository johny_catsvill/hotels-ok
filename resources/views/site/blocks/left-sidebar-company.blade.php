<div class="col-lg-3 col-sm-3">
    <div class="left-sidebar-help-page">
        <ul>
            <li><a @if(Request::is('how-to-reservation')) class="active" @endif href="{{ route('how-to-reservation') }}">{{ __('Как забронировать') }}</a></li>
            <li><a @if(Request::is('questions')) class="active" @endif href="{{ route('questions-client') }}">{{ __('Часто задаваемые вопросы') }}</a></li>
            <li><a @if(Request::is('about-company')) class="active" @endif href="{{ route('about-company') }}">{{ __('О компании') }}</a></li>
            <li><a @if(Request::is('confidentiality')) class="active" @endif href="{{ route('confidentiality') }}">{{ __('Конфиденциальность') }}</a></li>
            <li><a @if(Request::is('contacts')) class="active" @endif href="{{ route('contacts') }}">{{ __('Контакты') }}</a></li>
            <li><a @if(Request::is('guarantees')) class="active" @endif href="{{ route('guarantees') }}">{{ __('Гарантия безопасности') }}</a></li>
            <li><a @if(Request::is('public-tender')) class="active" @endif href="{{ route('public-tender') }}">{{ __('Публичная оферта') }}</a></li>
            <li><a @if(Request::is('partners')) class="active" @endif href="{{ route('partners') }}">{{ __('Партнеры') }}</a></li>
            <li><a @if(Request::is('rewards')) class="active" @endif href="{{ route('rewards') }}">{{ __('Награды и заслуги') }}</a></li>
            <li><a @if(Request::is('reviews')) class="active" @endif href="{{ route('reviews') }}">{{ __('Отзывы') }}</a></li>
        </ul>
    </div>
</div>