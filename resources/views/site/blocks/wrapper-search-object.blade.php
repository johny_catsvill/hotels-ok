<ul>

    @if($cities)
        @foreach($cities as $city)
            <li>
                <a href="javascript:void(0);" onclick="Search.addTextToField(this);" data-name="{{$city->name}}" data-category="city" data-id="{{$city->id}}">
                    <table>
                        <tr>
                            <td>
                                <img src="images/cities/{{ $city->image }}" alt="">
                            </td>
                            <td><span><b>{{ $city->name }},</b>  {{ $city->region->name }} область, Украина</span></td>
                        </tr>
                    </table>
                </a>
            </li>
        @endforeach
    @endif

    @if($regions)
        @foreach($regions as $region)
                <li>
                    <a href="javascript:void(0);" onclick="Search.addTextToField(this);" data-name="{{ $region->name }} область" data-category="region" data-id="{{$region->id}}">
                        <img src="images/cities/not-img.jpg" alt="">
                        <span><b>{{ $region->name }} область,</b>  Украина</span>
                    </a>
                </li>
        @endforeach
    @endif

    @if($objects)
        @foreach($objects as $object)
                <li>
                    <a href="{{ route('hotel', ['id' => $object->id, 'name' => $object->subdomain]) }}" data-name="{{ $object->name }}" data-category="object" data-id="{{$object->id}}">
                        <table>
                            <tr>
                                <td><img src="{{ asset('images/hotels/'.$object->id.'-'.$object->url.'/'.$object->image) }}" alt=""></td>
                                <td><span><b>{{ $object->name }},</b> {{ $object->city->name }}, {{ $object->city->region->name }} область,  Украина</span></td>
                            </tr>
                        </table>
                    </a>
                </li>
        @endforeach
    @endif

</ul>