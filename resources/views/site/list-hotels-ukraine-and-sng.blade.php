@extends('site.startpage')
@section('content')
@section('top-block')
    <section id="questions" class="hotels-ukraine-list">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h1>{{ __('Гостиницы городов Украины') }}</h1>
                    @include('site.blocks.breadcrumbs')
                </div>
            </div>
        </div>
    </section>
    <section id="contentHotelsList">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-sm-4">
                    <div class="left-sidebar-hotels-list">
                        <div class="search-city-list-hotels">
                            <b class="caption-filter">{{ __('Поиск города') }}</b>
                            <hr>
                            <form action="">
                               <div class="wrap-field-search-city-list-hotels">
                                   <label for="">{{ __('Город') }}</label>
                                   <input type="text" name="city">
                               </div>

                                <div class="wrapper-span-beside-object-distance list-hotels-region">
                                    <label for="">{{ __('Регион') }}</label>
                                    <span class="main-region" onclick="newFunction.listRegions();">{{ __('Выберите область') }}</span>
                                    <i class="fa fa-angle-down"></i>
                                    <div class="list-distance-for-page-category">
                                        <ul>
                                            <li><span onclick="newFunction.selectRegion(this);" data-region="0">{{ __('Выберите область') }}</span></li>
                                            @foreach($regions as $reg)
                                                <li><span onclick="newFunction.selectRegion(this);" data-region="{{ $reg->id }}">{{ $reg->name }} {{ __('область') }}</span></li>
                                            @endforeach
                                        </ul>
                                        <input type="hidden" name="regionSelect" value="">
                                    </div>
                                </div>

                                <a href="javascript:void(0);" onclick="newFunction.showOnlySelectRegionAndCity();" class="btn-search-list-hotels">{{ __('Поиск') }}</a>
                            </form>
                        </div>
                        <div class="viber-project">
                            <img src="/images/viber.jpg" alt="">
                        </div>
                    </div>
                </div>
                <div class="col-lg-9 col-sm-8">
                    <div class="wrap-list-cities">
                        @foreach($regions as $region)
                            @if(!count($region->cities))
                                @continue
                            @endif
                            <div class="list-cities-region" data-region="{{ $region->id }}">
                                <span class="caption-region-block">{{ $region->name }} {{ __('область') }}</span>
                                <ul>
                                    @foreach($region->cities as $city)

                                        @php
                                            $minPrice = round($city->calendarsPrice()->where('event_calendar.date', $currentDate)->min('price'),0);

                                        @endphp
                                        @if($minPrice)
                                            <li>
                                                <div class="one-city">
                                                    <a href="/category/?search={{ $city->name }}&countryId={{ $city->countryId }}&cityId={{ $city->id }}">
                                                        <img src="/images/cities/{{ $city->image }}" alt="">
                                                    </a>
                                                    <div class="wrap-info-city">
                                                        <a href="/category/?search={{ $city->name }}&countryId={{ $city->countryId }}&cityId={{ $city->id }}" class="name-city">{{ $city->name }}</a>
                                                        <span class="count-hotels-city">{{ $city->objects_count }}
                                                            {{ trans_choice('localization.count_objects', $city->objects_count) }}</span>
                                                        <span class="min-price-hotels-city"><b>от {{ $minPrice }} {{ __('грн') }}</b> / {{ __('сутки') }}</span>
                                                    </div>
                                                </div>
                                            </li>
                                        @endif

                                    @endforeach
                                </ul>
                            </div>

                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@include('site.blocks.messages-phone')
@endsection