@extends('site.startpage')
@section('content')
@section('top-block')
    <section id="contacts">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h1>{{ __('Контакты') }}</h1>
                    @include('site.blocks.breadcrumbs')
                </div>
            </div>
        </div>
    </section>

    <section id="contactsContent">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    @include('site.blocks.left-sidebar-company')
                    <div class="col-lg-9 col-sm-9">
                        <div class="main-content-help-page how-to-reservation">
                            <div class="col-lg-6">
                                <h3>{{ __('Контакты') }}</h3>
                                <div class="how-to-reservation-p contacts-ul">
                                    <ul>
                                        <li><img src="/images/phone-contacts.png" alt=""> 0 800 800-000</li>
                                        <li><img src="/images/message-contacts.png" alt=""> info@hotels-ok.com</li>
                                        <li><img src="/images/map-contacts.png" alt=""> ул. Иванова, 35</li>
                                    </ul>
                                </div>
                                <h3>{{ __('Обратная связь') }}</h3>
                                <form action="" method="post" class="contact-form">
                                    {{ csrf_field() }}
                                    <label for="">
                                        <input type="text" name="email" placeholder="Email">
                                    </label>
                                    <label for="">
                                        <input type="text" name="name" placeholder="{{ __('Имя') }}">
                                    </label>
                                    <label for="">
                                        <textarea name="review" id="" cols="30" rows="10" placeholder="{{ __('Сообщение') }}"></textarea>
                                    </label>
                                    <input type="submit" name="submit" value="{{ __('Отправить') }}" onclick="newFunction.saveCallback(event);">
                                    <input type="hidden" name="type" value="positive">
                                </form>
                            </div>
                            <div class="col-lg-6">
                                <div class="map-contacts">
                                    <iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d5080.039136006351!2d30.4903354!3d50.4593603!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sru!2sua!4v1517336605695" width="100%" height="600px" frameborder="0" style="border:0" allowfullscreen></iframe>
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@include('site.blocks.messages-phone')
@endsection