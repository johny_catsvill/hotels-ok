@extends('site.startpage')
@section('content')
@section('top-block')
@endsection
<section id="bgBussiness" class="bg-partners">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="bussiness-content">
                    <h1>{{ __('Добавьте свой отель на Hotels-ok.com') }}</h1>
                    <h2>{{ __('Сервис бронирования №1 в Украине') }}</h2>
                    <a href="javascript:void(0);" onclick="newFunction.scrollToElement('partnersApplication');">Добавить отель</a>
                </div>
            </div>
        </div>
    </div>
    <div class="bg-bussiness-overlay"></div>
</section>
<section id="plusesReservation" class="pluses-for-hotels">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="pluse-reservation-hotel">
                    <h2>Преимущества</h2>
                    <span class="line-blue first-line-caption"></span>
                    <span class="line-blue two-line-caption"></span>
                    <div class="list-pluses">
                        <ul>
                            <li>
                                <div class="one-pluse">
                                    <img src="/images/pluse-partners-1.png" alt="">
                                    <div class="two-border-pluse"></div>
                                </div>
                                <span>Бесплатное <br> размещение <br> на сайте</span>
                            </li>
                            <li>
                                <div class="one-pluse">
                                    <img src="/images/pluse-partners-2.png" alt="">
                                    <div class="two-border-pluse"></div>
                                </div>
                                <span>Оперативная <br> поддержка <br> отелей</span>
                            </li>
                            <li>
                                <div class="one-pluse">
                                    <img src="/images/pluse-partners-3.png" alt="">
                                    <div class="two-border-pluse"></div>
                                </div>
                                <span>Два <br> канала продаж</span>
                            </li>

                            <li>
                                <div class="one-pluse">
                                    <img src="/images/pluse-partners-4.png" alt="">
                                    <div class="two-border-pluse"></div>
                                </div>
                                <span>Продвижение <br> отелей</span>
                            </li>

                            <li>
                                <div class="one-pluse">
                                    <img src="/images/pluse-partners-5.png" alt="">
                                    <div class="two-border-pluse"></div>
                                </div>
                                <span>Обслуживание <br> коорпоративных <br> клиентов</span>
                            </li>
                            <li>
                                <div class="one-pluse">
                                    <img src="/images/pluse-partners-6.png" alt="">
                                    <div class="two-border-pluse"></div>
                                </div>
                                <span>Поселение групп <br> от 10 человек</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="aboutCompany">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2>Преимущества</h2>
                <span class="line-blue first-line-caption"></span>
                <span class="line-blue two-line-caption"></span>
                <p> Бесплатный сервис бронирования отелей по всей Украине. Наша команда — это более 60 сотрудников, которых объединяет одна цель: помочь вам остановиться в любом уголке нашей страны. Шикарный отель или недорогой хостел? Город-миллионник или небольшой поселок? Отдых или командировка? Вы получите только лучшие предложения отелей без лишней траты времени и денег.  Бесплатный сервис бронирования отелей по всей Украине. Наша команда — это более 60 сотрудников, которых объединяет одна цель: помочь вам остановиться в любом уголке нашей страны. Шикарный отель или недорогой хостел? Город-миллионник или небольшой поселок? Отдых или командировка? Вы получите только лучшие предложения отелей без лишней траты времени и денег.
                </p>
            </div>
        </div>
    </div>
</section>
<section id="partnersApplication">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2>Оставьте заявку</h2>
                <span class="line-blue first-line-caption"></span>
                <span class="line-blue two-line-caption"></span>
                <div class="wrapper-form-partners-application">
                    <form action="" method="post">
                        <input type="text" name="name" placeholder="Имя">
                        <input type="text" name="object_name" placeholder="Название отеля, квартиры, хостела">
                        <input type="text" name="city" placeholder="Город">
                        <input type="text" name="phone" id="phoneCallback" placeholder="Телефон">
                        <input type="text" name="email" placeholder="Email">
                        <input type="hidden" name="type" value="hotel">
                        {{ csrf_field() }}
                        <input type="submit" name="submit" value="Добавить отель" onclick="newFunction.addApplication(event, this);">
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="howAreWorkProject">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2>Как это работает</h2>
                <span class="line-blue first-line-caption"></span>
                <span class="line-blue two-line-caption"></span>
                <div class="steps-how-are-work">
                    <div class="arrow-dashed first-arrow-dashed">
                        <img src="/images/arrow-dashed.png" alt="">
                    </div>
                    <div class="arrow-dashed second-arrow-dashed">
                        <img src="/images/arrow-dashed.png" alt="">
                    </div>
                    <div class="arrow-dashed third-arrow-dashed">
                        <img src="/images/arrow-dashed.png" alt="">
                    </div>
                    <ul>
                        <li>
                            <div class="one-step">
                                <div class="wrapper-img-step">
                                    <img src="/images/how-are-work-step-1.png" alt="">
                                    <div class="first-border-step"><b>1</b>
                                        <div class="two-border-pluse"></div>
                                    </div>
                                </div>
                                <span>Вы добавляете свой <br> отель в наш каталог</span>
                            </div>
                        </li>
                        <li>
                            <div class="one-step">
                                <div class="wrapper-img-step">
                                    <img src="/images/how-are-work-step-2.png" alt="">
                                    <div class="first-border-step"><b>2</b>
                                        <div class="two-border-pluse"></div>
                                    </div>
                                </div>
                                <span>Мы рассказываем <br> о вас всей Украине</span>
                            </div>
                        </li>
                        <li>
                            <div class="one-step">
                                <div class="wrapper-img-step">
                                    <img src="/images/how-are-work-step-3.png" alt="">
                                    <div class="first-border-step"><b>3</b>
                                        <div class="two-border-pluse"></div>
                                    </div>
                                </div>
                                <span>Вы получаете <br> бронирования <br> и отзывы</span>
                            </div>
                        </li>
                        <li>
                            <div class="one-step">
                                <div class="wrapper-img-step">
                                    <img src="/images/how-are-work-step-3.png" alt="">
                                    <div class="first-border-step"><b>4</b>
                                        <div class="two-border-pluse"></div>
                                    </div>
                                </div>
                                <span>Оплачиваете комиссию <br> сервиса 10% от стоимости <br> бронирования</span>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="shares">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="shares-style">
                    <h2>Последние забронированные номера</h2>
                    <span class="line-blue first-line-caption"></span>
                    <span class="line-blue two-line-caption"></span>
                    <div class="list-shares">
                        <ul>
                            <li>
                                <div class="one-shares">
                                    <img src="/images/primer-shares-1.jpg" alt="">
                                    <div class="shares-price"><span>30%</span></div>
                                    <div class="overlay-share"></div>
                                    <a href="" class="btn-reservation-hotel">Забронировать</a>
                                </div>
                                <h4>Барон Мюнхаузен</h4>
                                <span class="shares-price-hotel">300 грн / сутки</span>
                            </li>
                            <li>
                                <div class="one-shares">
                                    <img src="/images/primer-shares-2.jpg" alt="">
                                    <div class="shares-price"><span>30%</span></div>
                                    <div class="overlay-share"></div>
                                    <a href="" class="btn-reservation-hotel">Забронировать</a>
                                </div>
                                <h4>Барон Мюнхаузен</h4>
                                <span class="shares-price-hotel">300 грн / сутки</span>
                            </li>
                            <li>
                                <div class="one-shares">
                                    <img src="/images/primer-shares-1.jpg" alt="">
                                    <div class="shares-price"><span>30%</span></div>
                                    <div class="overlay-share"></div>
                                    <a href="" class="btn-reservation-hotel">Забронировать</a>
                                </div>
                                <h4>Барон Мюнхаузен</h4>
                                <span class="shares-price-hotel">300 грн / сутки</span>
                            </li>
                            <li>
                                <div class="one-shares">
                                    <img src="/images/primer-shares-1.jpg" alt="">
                                    <div class="shares-price"><span>30%</span></div>
                                    <div class="overlay-share"></div>
                                    <a href="" class="btn-reservation-hotel">Забронировать</a>
                                </div>
                                <h4>Барон Мюнхаузен</h4>
                                <span class="shares-price-hotel">300 грн / сутки</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="faqProject">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2>Часто задаваемые вопросы</h2>
                <span class="line-blue first-line-caption"></span>
                <span class="line-blue two-line-caption"></span>
                <ul>
                    <li><a href="{{ route('questions-client', str_slug('Каким способом получить гарантию бронирования?')) }}">
                            <img src="{{asset('images/mini-arrow.png')}}" alt=""><span>Каким способом получить гарантию бронирования?</span></a></li>
                    <li><a href="{{ route('questions-client', str_slug('Как написать мнение о гостинице?')) }}">
                            <img src="{{asset('images/mini-arrow.png')}}" alt=""><span>Как написать мнение о гостинице?</span></a></li>
                    <li><a href="{{ route('questions-client', str_slug('Какие нужно выполнить требования для отмены бронирования?')) }}">
                            <img src="{{asset('images/mini-arrow.png')}}" alt=""><span>Какие нужно выполнить требования для отмены бронирования?</span></a></li>
                    <li><a href="{{ route('questions-client', str_slug('Предусмотрено ли денежное взыскание за отмену брони?')) }}">
                            <img src="{{asset('images/mini-arrow.png')}}" alt=""><span>Предусмотрено ли денежное взыскание за отмену брони?</span></a></li>
                    <li><a href="{{ route('questions-client', str_slug('Как вернуть назад деньги после отмены брони?')) }}">
                            <img src="{{asset('images/mini-arrow.png')}}" alt=""><span>Как вернуть назад деньги после отмены брони?</span></a></li>
                    <li><a href="{{ route('questions-client', str_slug('Возможно ли осуществить трансфер в гостиницу/с гостиницы?')) }}">
                            <img src="{{asset('images/mini-arrow.png')}}" alt=""><span>Возможно ли осуществить трансфер в гостиницу/с гостиницы?</span></a></li>
                    <li><a href="{{ route('questions-client', str_slug('Как оптимальнее совершить предварительную оплату за проживание?')) }}">
                            <img src="{{asset('images/mini-arrow.png')}}" alt=""><span>Как оптимальнее всего совершить предварительную оплату за проживание?</span></a></li>
                    <li><a href="{{ route('questions-client', str_slug('Какие различия между гостиницами с моментальным бронированием и с помощью оператора?')) }}">
                            <img src="{{asset('images/mini-arrow.png')}}" alt=""><span>Какие различия между гостиницами с моментальным бронированием и с помощью оператора?</span></a></li>
                </ul>
            </div>
        </div>
    </div>
</section>
<section id="reviews" class="reviews-partners">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="reviews-style">
                    <h2>Что говорят о нас партнеры</h2>
                    <span class="line-blue first-line-caption"></span>
                    <span class="line-blue two-line-caption"></span>
                    <div class="list-reviews">
                        <ul>
                            <li>
                                <div class="one-review">
                                    <img src="/images/review-1.png" alt="">
                                    <span>Ирина Белая</span>
                                    <strong>Администратор отеля ВИВА</strong>
                                    <p>Впервые воспользовалась этим сайтом, и не пожалела!! Ребята сработали оперативно, нашли квартиру во Львове по искомым параметрам и по цене приемлемой для нашего скромного бюджета, очень быстро оформили </p>
                                </div>
                            </li>
                            <li>
                                <div class="one-review">
                                    <img src="/images/review-2.png" alt="">
                                    <span>Ирина Белая</span>
                                    <strong>Администратор отеля ВИВА</strong>
                                    <p>Во Вторые воспользовалась этим сайтом, и не пожалела!! Ребята сработали оперативно, нашли квартиру во Львове по искомым параметрам и по цене приемлемой для нашего скромного бюджета, очень быстро оформили </p>
                                </div>
                            </li>
                            <li>
                                <div class="one-review">
                                    <img src="/images/review-3.png" alt="">
                                    <span>Ирина Белая</span>
                                    <strong>Администратор отеля ВИВА</strong>
                                    <p>В третье воспользовалась этим сайтом, и не пожалела!! Ребята сработали оперативно, нашли квартиру во Львове по искомым параметрам и по цене приемлемой для нашего скромного бюджета, очень быстро оформили </p>
                                </div>
                            </li>
                            <li>
                                <div class="one-review">
                                    <img src="/images/review-3.png" alt="">
                                    <span>Ирина Белая</span>
                                    <strong>Администратор отеля ВИВА</strong>
                                    <p>В третье воспользовалась этим сайтом, и не пожалела!! Ребята сработали оперативно, нашли квартиру во Львове по искомым параметрам и по цене приемлемой для нашего скромного бюджета, очень быстро оформили </p>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="arrows-reviews-carousel">
                        <a href="javascript:void(0);" onclick="newFunction.prevCarouselSlide('list-reviews');" class="arrow-carousel arrow-left-carousel"><img src="/images/arrow-left-carousel.png" alt=""></a>
                        <a href="javascript:void(0);" onclick="newFunction.nextCarouselSlide('list-reviews');" class="arrow-carousel arrow-right-carousel"><img src="/images/arrow-right-carousel.png" alt=""></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@include('site.blocks.messages-phone')
@endsection