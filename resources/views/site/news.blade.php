@extends('site.startpage')
@section('content')
@section('top-block')
    <section id="news">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h1>{{ __('Новости') }}</h1>
                    @include('site.blocks.breadcrumbs')
                </div>
            </div>
        </div>
    </section>

    <section id="contactsContent">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    @include('site.blocks.left-sidebar-news')
                    <div class="col-lg-10">
                        <div class="main-content-help-page ">
                            <div class="main-news">
                                @if($firstNews)
                                    <div class="img-news">
                                        <a href="{{ route('one-news', ['category' => $firstNews->category->slug,'id' => $firstNews->id, 'url' => $firstNews->slug]) }}">
                                            <img src="/images/news/{{ $firstNews->getNewImage('norm') }}" alt="">
                                        </a>
                                    </div>
                                    <div class="news-content">
                                        <a href="{{ route('one-news', ['category' => $firstNews->category->slug,'id' => $firstNews->id, 'url' => $firstNews->slug]) }}" class="caption-news">{{ $firstNews->name }}</a>
                                        <p>{!! strip_tags(str_limit($firstNews->text, 500, '...')) !!}
                                        </p>
                                        <ul>
                                            <li><span class="date-news">{{ $firstNews->date_published }}</span></li>
                                            <li>|</li>
                                            <li><a href="{{ route('news', $firstNews->category->slug) }}" class="category-news">{{ $firstNews->category->name }}</a></li>
                                        </ul>
                                    </div>
                                @endif
                                <div class="clear"></div>
                                </div>
                                @if(count($secondRowsNews))
                                    <div class="second-rows-news">
                                        <ul>
                                            @foreach($secondRowsNews as $secondRowsNew)
                                                <li>
                                                    <div class="one-news">
                                                        <div class="wrapper-img-news">
                                                            <a href="{{ route('one-news', ['category' => $secondRowsNew->category->slug,'id' => $secondRowsNew->id, 'url' => $secondRowsNew->slug]) }}">
                                                                <img src="/images/news/{{ $secondRowsNew->getNewImage('norm') }}" alt="">
                                                            </a>
                                                        </div>
                                                        <ul>
                                                            <li><span class="date-news">{{ $secondRowsNew->date_published }}</span></li>
                                                            <li>|</li>
                                                            <li><a href="{{ route('news', $secondRowsNew->category->slug) }}" class="category-news">{{ $secondRowsNew->category->name }}</a></li>
                                                        </ul>
                                                        <a href="{{ route('one-news', ['category' => $secondRowsNew->category->slug,'id' => $secondRowsNew->id, 'url' => $secondRowsNew->slug]) }}" class="link-news">{{ $secondRowsNew->name }}</a>
                                                    </div>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    <div class="clear"></div>
                                @endif
                                @if(count($threeRowsNews))
                                    <div class="third-rows-news">
                                    <ul>
                                        @foreach($threeRowsNews as $threeRowsNew)
                                        <li>
                                            <div class="third-row-one-new">
                                                <div class="wrapper-third-img-news">
                                                    <a href="{{ route('one-news', ['category' => $threeRowsNew->category->slug,'id' => $threeRowsNew->id, 'url' => $threeRowsNew->slug]) }}">
                                                        <img src="/images/news/{{ $threeRowsNew->getNewImage('norm') }}" alt=""></a>
                                                </div>
                                                <ul>
                                                    <li><span class="date-news">{{ $threeRowsNew->date_published }}</span></li>
                                                    <li>|</li>
                                                    <li><a href="{{ route('one-news', ['category' => $threeRowsNew->category->slug,'id' => $threeRowsNew->id, 'url' => $threeRowsNew->slug]) }}" class="category-news">{{ $threeRowsNew->category->name }}</a></li>
                                                </ul>
                                                <a href="{{ route('one-news', ['category' => $threeRowsNew->category->slug,'id' => $threeRowsNew->id, 'url' => $threeRowsNew->slug]) }}" class="link-news">{{ $threeRowsNew->name }}</a>
                                            </div>
                                        </li>
                                        @endforeach
                                    </ul>
                                </div>
                                    <div class="clear"></div>
                                @endif

                                @if(count($fourRowsNews))
                                    <div class="second-rows-news">
                                        <ul>
                                            @foreach($fourRowsNews as $fourRowsNew)
                                                <li>
                                                    <div class="one-news">
                                                        <div class="wrapper-img-news">
                                                            <a href="{{ route('one-news', ['category' => $fourRowsNew->category->slug,'id' => $fourRowsNew->id, 'url' => $fourRowsNew->slug]) }}">
                                                                <img src="/images/news/{{ $fourRowsNew->getNewImage('norm') }}" alt="">
                                                            </a>
                                                        </div>
                                                        <ul>
                                                            <li><span class="date-news">{{ $fourRowsNew->date_published }}</span></li>
                                                            <li>|</li>
                                                            <li><a href="{{ route('news', $fourRowsNew->category->slug) }}" class="category-news">{{ $fourRowsNew->category->name }}</a></li>
                                                        </ul>
                                                        <a href="{{ route('one-news', ['category' => $fourRowsNew->category->slug,'id' => $fourRowsNew->id, 'url' => $fourRowsNew->slug]) }}" class="link-news">{{ $fourRowsNew->name }}</a>
                                                    </div>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    <div class="clear"></div>
                                @endif

                                @if(count($fiveRowsNews))
                                <div class="third-rows-news">
                                    <ul>
                                        @foreach($fiveRowsNews as $fiveRowsNew)
                                            <li>
                                                <div class="third-row-one-new">
                                                    <div class="wrapper-third-img-news">
                                                        <a href="{{ route('one-news', ['category' => $fiveRowsNew->category->slug,'id' => $fiveRowsNew->id, 'url' => $fiveRowsNew->slug]) }}">
                                                            <img src="/images/news/{{ $fiveRowsNew->getNewImage('norm') }}" alt=""></a>
                                                    </div>
                                                    <ul>

                                                        <li><span class="date-news">{{ $fiveRowsNew->date_published }}</span></li>
                                                        <li>|</li>
                                                        <li><a href="{{ route('news', $fiveRowsNew->category->slug) }}" class="category-news">{{ $fiveRowsNew->category->name }}</a></li>
                                                    </ul>
                                                    <a href="{{ route('one-news', ['category' => $fiveRowsNew->category->slug,'id' => $fiveRowsNew->id, 'url' => $fiveRowsNew->slug]) }}" class="link-news">{{ $fiveRowsNew->name }}</a>
                                                </div>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                                <div class="clear"></div>
                                @endif

                            <div class="clear"></div>
                            <div class="pagination-news">
                                {{ $news->render() }}

                                <span>страница {{ $currentPage }} из {{ $totalPage }}</span>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@include('site.blocks.messages-phone')
@endsection