@extends('site.startpage')
@section('content')
@section('top-block')
    <section id="reviewsPage" class="top-header-style">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h1>{{ __('Отзывы') }}</h1>
                    @include('site.blocks.breadcrumbs')
                </div>
            </div>
        </div>
    </section>
    <section id="reviewsList">
        <div class="container">
            <div class="row">
                <div class="col-lg-9">
                    <div class="list-reviews-page">
                        <ul>
                            @foreach($reviews as $review)
                                <li>
                                    <div class="info-reviews-name-and-date row-review">
                                        <span class="name-author-review">{{ $review->name }}</span>
                                        <span class="date-review">{{ $review->lastUpdater }}</span>
                                        <span class="type-review"><img src="/images/smile.png" alt=""> <span>Отлично</span></span>
                                    </div>
                                    <div class="info-reviews-text row-review">
                                        <p>{{ $review->text }}</p>
                                    </div>
                                    <div class="info-reviews-average-raiting row-review">
                                        <span class="average-raiting-text">{{ $review->raiting }}</span>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                @include('site.blocks.right-sidebar-reviews')
            </div>
        </div>
    </section>

@endsection

@include('site.blocks.messages-phone')
@endsection