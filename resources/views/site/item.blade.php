@extends('site.startpage')
@section('content')
@section('top-block')

@endsection

<section id="breadcrumbs" class="item-breadcrumbs">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                @include('site.blocks.breadcrumbs')
            </div>
        </div>
    </div>
</section>
<section id="contentHotel">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-sm-4 padding-right-0px">
                <div class="left-sidebar-hotel">
                    <div class="map-hotel">
                        <span>{{ __('Отель на карте') }}</span>
                        <hr>
                        <div class="map-wrapper">
                            <div id="mapLeft"></div>
                        </div>
                    </div>
                    <div class="viber-project">
                        <img src="/images/viber.jpg" alt="">
                    </div>
                    @if(count($isDiscount) > 0)
                        <div class="offer-hotel">
                            <span>{{ __('Акции и скидки отеля') }}</span>
                            <hr>
                            <div class="list-shares">
                                <ul>
                                    @foreach($isDiscount as $discountRoom)
                                        <li>
                                            <div class="one-shares">
                                                <img src="/{{ $discountRoom->images[0] }}" alt="">
                                                <div class="shares-price"><span>{{ $discountRoom->isDiscount }}%</span></div>
                                                <div class="overlay-share"></div>
                                                <a href="javascript:void(0);" class="btn-reservation-hotel" data-id="{{ $discountRoom->id }}" onclick="newFunction.showRoom(this);">{{ __('Посмотреть') }}</a>
                                            </div>
                                            <h4><a href="javascript:void(0);" data-id="{{ $discountRoom->id }}" onclick="newFunction.showRoom(this);">{{ $discountRoom->name }}</a></h4>
                                            <span class="shares-price-hotel"><b>{{ $discountRoom->priceWithDiscount }} грн </b> / за {{ $period }} дн.</span>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    @endif
                    @if($lastReview)
                    <div class="last-review-object">
                        <span>{{ __('Отзыв об отеле') }}</span>
                        <hr>
                        <b class="name-review-last">{{ $lastReview->authorName }}</b>
                        <ul>
                            <li class="date-last-review">
                                {{ date('d.m.Y', $lastReview->creationDate) }}
                            </li>
                            <li>
                                    <span class="type-success-review"><img src="/images/smile.png" alt=""> {{ __('Отлично') }}</span>
                            </li>
                        </ul>
                        <p>{{ str_limit($lastReview->positiveText, 250) }} </p>
                        <a href="javascript:void(0);" class="btn-review-show-more" onclick="newFunction.showReview({{ $lastReview->id }});">{{ __('Читать дальше') }}</a>
                    </div>
                    @endif

                    @if($news)
                                <div class="third-rows-news news-item">
                                    <ul>
                                        @foreach($news as $new)
                                            <li>
                                                <div class="third-row-one-new">
                                                    <div class="wrapper-third-img-news">
                                                        <a href="{{ route('one-news', ['category' => $new->category->slug,'id' => $new->id, 'url' => $new->slug]) }}">
                                                            <img src="/images/news/{{ $new->getNewImage('object') }}" alt=""></a>
                                                    </div>
                                                    <ul>
                                                        <li><span class="date-news">{{ $new->date_published }}</span></li>
                                                        <li>|</li>
                                                        <li><a href="{{ route('news', $new->category->slug) }}" class="category-news">{{ $new->category->name }}</a></li>
                                                    </ul>
                                                    <a href="{{ route('one-news', ['category' => $new->category->slug,'id' => $new->id, 'url' => $new->slug]) }}" class="link-news">{{ $new->name }}</a>
                                                </div>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                    @endif


                </div>
            </div>
            <div class="col-lg-9 col-sm-8 padding-left-0px">
                <div class="main-content-hotel">
                    <input type="hidden" name="objectName" value="{{ $object->name }}">
                    <h2>{{ __($object->objectCategories->name) }} {{ $object->name }} <!--<span data-title="Проверенный отель"><img src="/images/checked-hotel.png"></span>--></h2>
                    @if($object->reviews_count)
                        <span class="average-reviews">{{ $object->reviews->avg('avgValue') }}
                            <span class="count-review">{{ $object->reviews_count }} {{ trans_choice('localization.reviews', $object->reviews_count) }}</span>
                        </span>
                    @endif
                    <div class="clear"></div>
                    <div class="stars-hotel display-inline-block padding-right-10px">

                            @for($i = 0; $i < 5; $i++)
                                <i class="fa fa-star @if($object->stars > $i) active-star @endif" aria-hidden="true"></i>
                            @endfor

                    </div>

                    <div class="address-hotel display-inline-block">
                        <span>{{ $object->address }}</span>
                    </div>
                    <div class="min-price-room-hotel display-inline-block">
                        <span>от {{ round($minPriceByRoom , 0) }} грн <span class="text-day">/ {{ __('сутки') }}</span></span>
                    </div>

                    {{--<div class="services-hotel">--}}
                        {{--<ul>--}}
                            {{--<li><img src="/images/bank-card.png" alt="">Без банковских карт</li>--}}
                            {{--<li><img src="/images/cancel-reservation.png" alt="">Условия отмены бронирования</li>--}}
                        {{--</ul>--}}
                    {{--</div>--}}

                    <div class="main-img">
                        <div class="wrap-bg-img-main" style="background-image: url(../{{ $mainImg }});"></div>
                        <img src="/{{ $mainImg }}" alt="" class="main-img-block">
                        <div class="arrows-gallery-hotel">
                            <a href="javascript:void(0);" class="arrow-left-gallery-hotel" onclick="Gallery.prevSlide();">
                                <img src="/images/arrow-left-gallery.png" alt="">
                            </a>
                            <a href="javascript:void(0);" class="arrow-right-gallery-hotel" onclick="Gallery.nextSlide();">
                                <img src="/images/arrow-right-gallery.png" alt="">
                            </a>
                        </div>
                    </div>
                    @if(count($objectImages) > 0)
                    <div class="list-images-hotel">
                        <a href="javascript:void(0);" onclick="newFunction.showAllImagesHotels();" class="btn-show-more-img-hotel-mobile">{{ count($objectImages) + 1 }} {{ __('фото') }}</a>
                        <ul>
                                <li>
                                    <a class="active-gallery-img" href="javascript:void(0);" onclick="Gallery.showThisImg(this);">
                                        <img src="/{{ preg_replace('/_object/', '_object_mini', $mainImg) }}" alt="">
                                    </a>
                                </li>
                                @foreach($objectImages as $image)
                                    <li>
                                        <a href="javascript:void(0);" onclick="Gallery.showThisImg(this);">
                                            <img src="/{{ $image }}" alt="">
                                        </a>
                                    </li>
                                    @if($loop->iteration == 4 && $loop->count >= 5)
                                        <li class="btn-show-more-img-hotel">
                                            <a data-active="off" href="javascript:void(0);" onclick="newFunction.showAllImagesHotels(this);">{{ count($objectImages) + 1 }} {{ __('фото') }} <i class="fa fa-angle-down"></i></a>
                                        </li>
                                    @endif
                                @endforeach
                        </ul>
                    </div>
                    @endif
                    <div class="menu-hotel">
                        <ul>
                            <li><a onclick="newFunction.tabsToogle(this, 1);" href="#poslugi" class="border-left-0px padding-right-0px">{{ __('Услуги') }}</a></li>
                            <li><a onclick="newFunction.tabsToogle(this, 2);" href="#nomera">{{ __('Номера') }}</a></li>
                            <li><a onclick="newFunction.tabsToogle(this, 3);" href="#otzivi">{{ __('Отзывы') }}
                                        <span class="count-reviews-hotel">
                                            @if($object->reviews_count)
                                                ({{ $object->reviews_count }})
                                            @else
                                                0
                                            @endif
                                        </span>
                                </a></li>
                            <li class="btn-show-map-hotel"><a href="javascript:void(0);" onclick="Popup.showPopup('popup-hotel-map', event);">
                                    <img src="/images/map-icon.png" alt=""> <span>{{ __('Показать на карте') }}</span></a></li>
                        </ul>
                    </div>
                    <div class="tabs-menu-hotel">
                        <ul>
                            <li data-tab="1" data-href="#poslugi">
                                <div class="tab-hotel">
                                    <p>{{ $object->infoAboutObject }}</p>
                                    <div class="service-hotel">
                                        <h3 class="caption-hotel">{{ __('Услуги в гостинице') }}</h3>
                                        <ul>
                                            @foreach($object->objectPluses as $pluse)
                                                <li><img src="/images/icon-pluses/{{ $pluse->image }}" alt=""> <span>{{ __($pluse->name) }}</span></li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    <div class="rules-residence">
                                        <h3 class="caption-hotel">{{ __('Порядок проживания') }} в {{ __($object->objectCategories->name) }} {{ $object->name }}</h3>
                                        <span class="mini-caption-hotel padding-top-5px">{{ __('Регистрация') }}</span>
                                        <ul>
                                            <li><i class="fa fa-circle" aria-hidden="true"></i> {{ __('Заезд с') }} {{ $object->checkIn }}</li>
                                            <li><i class="fa fa-circle" aria-hidden="true"></i> {{ __('Отьезд до') }} {{ $object->checkOut }}</li>
                                        </ul>
                                        <span class="mini-caption-hotel">{{ __('Размещение детей и предоставление дополнительных кроватей') }}</span>
                                        <ul>
                                            <li><i class="fa fa-circle" aria-hidden="true"></i>{{ $object->infoExtraPlaces }}</li>
                                        </ul>

                                        <h3 class="caption-hotel">{{ __('Расположение отеля') }}</h3>
                                        <p>{{ $object->locationObject }}</p>
                                        <span class="mini-caption-hotel">GPS {{ __('координаты') }}</span>
                                        <ul>
                                            <li><i class="fa fa-circle" aria-hidden="true"></i> N {{ $object->longitudeGps }} E {{ $object->latitudeGps }}</li>
                                        </ul>
                                        @if(count($object->objectBesides))
                                            <span class="mini-caption-hotel">{{ __('Расстояние к обьекту') }}</span>
                                            <ul>
                                                @foreach($object->objectBesides as $beside)
                                                    <li><i class="fa fa-circle" aria-hidden="true"></i> {{ __('Метро') }} 1.6км</li>
                                                @endforeach
                                            </ul>
                                        @endif
                                        @if($object->infoAboutTravel)
                                                <h3 class="caption-hotel">{{ __('Информация о проезде') }}</h3>
                                                <p>{{ $object->infoAboutTravel }}</p>
                                        @endif
                                        @if($object->restaurantAndBarsInfo)
                                            <h3 class="caption-hotel">{{ __('Рестораны') }}</h3>
                                            <p>{{ $object->restaurantAndBarsInfo }}</p>
                                        @endif
                                        @if($object->infoMedication)
                                            <h3 class="caption-hotel">{{ __('Лечение') }}</h3>
                                            <p>{{ $object->infoMedication }}</p>
                                        @endif
                                    </div>
                                </div>
                            </li>
                            <li data-tab="2" data-href="#nomera">
                                <div class="tab-hotel">
                                    <span>{{ __('Выберите даты проживания в отеле, чтобы просмотреть цены и номера') }}</span>
                                    <form action="" method="post" class="form-find-hotel">
                                        {{ csrf_field() }}
                                        <ul>
                                            <li>
                                                <label for="">
                                                    <input type="text" name="arrival" placeholder="{{ __('Заезд') }}" class="datePicker1">
                                                    <i class="fa fa-calendar" aria-hidden="true"></i>
                                                    <span></span>
                                                </label>
                                            </li>
                                            <li>
                                                <label for="">
                                                    <input type="text" name="departure" placeholder="{{ __('Отьезд') }}" class="datePicker2">
                                                    <i class="fa fa-calendar" aria-hidden="true"></i>
                                                    <span></span>
                                                </label>
                                            </li>
                                            <li class="input-count-guest">
                                                <label for="">
                                                    <input type="text" name="guestNumber" value="1" onclick="newFunction.showPopupGuest();">
                                                    <i class="fa fa-angle-down" aria-hidden="true" onclick="newFunction.showPopupGuest();"></i>
                                                    <span></span>
                                                    <strong class="string-guest">{{ __('Гостей') }}:</strong>
                                                </label>
                                                <div class="block-count-guest">
                                                    <ul>
                                                        <li>
                                                            <a href="javascript:void(0);" onclick="newFunction.deacrementCountGuest();"><i class="fa fa-minus" aria-hidden="true"></i></a>
                                                        </li>
                                                        <li>
                                                            <input type="text" name="guest" value="1" readonly>
                                                        </li>
                                                        <li>
                                                            <a href="javacript:void(0);" onclick="newFunction.incrementCountGuest();"><i class="fa fa-plus" aria-hidden="true"></i></a>
                                                        </li>
                                                        <li class="clear"></li>
                                                    </ul>

                                                </div>
                                            </li>
                                            <li class="btn-search-hotel-on-the-date">
                                                <a href="javascript:void(0);" class="" onclick="Filter.rooms();">{{ __('Поиск') }}</a>
                                            </li>
                                        </ul>
                                        <input type="hidden" name="objectId" value="{{ $object->id }}">
                                    </form>
                                    <hr>
                                    <div class="wrapper-for-room-object">
                                        <div class="loader-inner hide">
                                            <div class="loader-line-wrap">
                                                <div class="loader-line"></div>
                                            </div>
                                            <div class="loader-line-wrap">
                                                <div class="loader-line"></div>
                                            </div>
                                            <div class="loader-line-wrap">
                                                <div class="loader-line"></div>
                                            </div>
                                            <div class="loader-line-wrap">
                                                <div class="loader-line"></div>
                                            </div>
                                            <div class="loader-line-wrap">
                                                <div class="loader-line"></div>
                                            </div>
                                        </div>
                                        <div class="rooms-html">
                                            @include('site.blocks.rooms-object')
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li data-tab="3" data-href="#otzivi">
                                <div class="tab-hotel">
                                    @if(count($object->reviews)> 0)
                                    <div class="reviews-hotel">
                                        <div class="main-info-about-reviews-hotel">
                                            <ul>
                                                <li>
                                                    <div class="average-raiting-hotel">
                                                        <span class="caption-raiting-hotel">{{ __('Оценка отеля') }}</span>
                                                        <div class="raiting-hotel-value">{{ $object->reviews->avg('avgValue') }}</div>
                                                        <span class="base-count-hotel">{{ __('на основании') }} {{ $object->reviews_count }} {{ __('отзывов') }}</span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="average-category-raiting-hotel">
                                                        <ul>
                                                            <li><b>{{ __('Комфорт') }}</b><span>{{ $object->reviews->avg('comfortValue') }}</span></li>
                                                            <li><b>{{ __('Персонал') }}</b><span>{{ $object->reviews->avg('employeeValue') }}</span></li>
                                                            <li><b>{{ __('Услуги') }}</b><span>{{ $object->reviews->avg('serviceValue') }}</span></li>
                                                            <li><b>{{ __('Цена/качество') }}</b><span>{{ $object->reviews->avg('qualityPriceValue') }}</span></li>
                                                            <li><b>{{ __('Чистота') }}</b><span>{{ $object->reviews->avg('cleanValue') }}</span></li>
                                                            <li><b>{{ __('Местоположение') }}</b><span>{{ $object->reviews->avg('locationValue') }}</span></li>
                                                        </ul>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="list-count-reviews-by-category">
                                                        <ul>
                                                            <li><a href="javascript:void(0);" onclick="newFunction.showCategoryReview(this);">{{ __('Все отзывы') }} ({{ $object->reviews_count }})</a></li>
                                                            @foreach($object->categoryReviews as $category)
                                                                <li><a data-id="{{$category->id}}" href="javascript:void(0);" onclick="newFunction.showCategoryReview(this);">{{ $category->nameType }} ({{ $category->reviews()->count() }})</a></li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                            <div class="list-reviews-hotel">
                                                <ul>
                                                    @foreach($object->reviews as $review)
                                                    <li>
                                                        <div class="one-review-hotel" data-review-id="{{ $review->id }}" data-category-review-id="{{ $review->typeReview->id }}">
                                                            <div class="caption-review-hotel">
                                                                <ul>
                                                                    <li>
                                                                        <span class="name-review">{{ $review->authorName }}</span>
                                                                        <span class="date-review">{{ $review->lastUpdater }}, {{ $review->typeReview->nameType }}</span>
                                                                    </li>
                                                                    <li>
                                                                        <span class="average-count-review">{{ $review->avgValue }}</span>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                            <div class="main-content-review-hotel">
                                                                <ul>
                                                                    <li>
                                                                        <div class="average-raiting-current-review">
                                                                            <span>{{ __('Оценки отеля') }}</span>
                                                                            <ul>
                                                                                <li><b>{{ __('Комфорт') }}</b><span>{{ $review->comfortValue }}</span></li>
                                                                                <li><b>{{ __('Персонал') }}</b><span>{{ $review->employeeValue }}</span></li>
                                                                                <li><b>{{ __('Услуги') }}</b><span>{{ $review->serviceValue }}</span></li>
                                                                                <li><b>{{__('Цена/качество')}}</b><span>{{ $review->qualityPriceValue }}</span></li>
                                                                                <li><b>{{ __('Чистота') }}</b><span>{{ $review->cleanValue }}</span></li>
                                                                                <li><b>{{ __('Местоположение') }}</b><span>{{ $review->locationValue }}</span></li>
                                                                            </ul>
                                                                        </div>
                                                                    </li>
                                                                    <li>
                                                                        <div class="text-current-review">
                                                                            <ul>
                                                                                <li>
                                                                                    <span><img src="/images/pluses-review.png" alt="">{{ __('Достоинства') }}</span>
                                                                                    <p>{{ $review->positiveText }}</p>
                                                                                </li>
                                                                                <li>
                                                                                    <span><img src="/images/limitations.png" alt="">{{ __('Недостатки') }}</span>
                                                                                    <p>{{ $review->negativeText }}</p>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                    </div>
                                    @else
                                        <p class="error-find-hotel">К сожалению, на данный момент отзывы отсутствуют.</p>
                                </div>
                                @endif
                            </li>
                        </ul>
                    </div>
                    @if(count($recommendationObjects) > 0)
                        <h3 class="caption-recomendation-hotel">{{ __('Рекомендованные отели') }}</h3>
                        <div class="wrapper-recomendation-hotels">
                        <a href="javascript:void(0);" class="arrow-recomendation arrow-recomendation-left" onclick="newFunction.prevCarouselSlide('recomendation-hotels');">
                            <img src="/images/arrow-recomendation-left.png" alt="">
                        </a>
                        <a href="javascript:void(0);" class="arrow-recomendation arrow-recomendation-right" onclick="newFunction.nextCarouselSlide('recomendation-hotels');">
                            <img src="/images/arrow-recomendation-right.png" alt="">
                        </a>
                        <div class="recomendation-hotels">
                            <ul>
                                @foreach($recommendationObjects as $objectCurrent)
                                <li>
                                    <div class="one-shares">
                                        <img src="/images/hotels/{{$objectCurrent->objectId.'-'.$objectCurrent->subdomain}}/{{$objectCurrent->image}}" alt="">
                                        @if($objectCurrent->valuePercent)
                                            <div class="shares-price"><span>{{ $objectCurrent->valuePercent }}%</span></div>
                                        @endif
                                        <div class="overlay-share"></div>
                                        <a href="{{ route('hotel', ['id' => $objectCurrent->objectId, 'name' => $objectCurrent->subdomain]) }}" class="btn-reservation-hotel">{{ __('Забронировать') }}</a>
                                    </div>
                                    <h4><a href="{{ route('hotel', ['id' => $objectCurrent->objectId, 'name' => $objectCurrent->subdomain]) }}">{{ str_limit($objectCurrent->name, 15) }}</a></h4>
                                    <span class="shares-price-hotel"><b>{{ round($objectCurrent->price, 0) }}</b> грн / {{ __('сутки') }}</span>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</section>
@if($seeLaterHotel)
<section id="seeLaterHotel">
    <h3 class="caption-recomendation-hotel see-later-caption">{{ __('Просмотренные отели') }}</h3>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="see-later-hotel">
                    @if(count($seeLaterHotel) > 4)
                        <a href="javascript:void(0);" class="arrow-recomendation arrow-recomendation-left" onclick="newFunction.prevCarouselSlide('see-later-slider');">
                            <img src="/images/arrow-recomendation-left.png" alt="">
                        </a>
                        <a href="javascript:void(0);" class="arrow-recomendation arrow-recomendation-right" onclick="newFunction.nextCarouselSlide('see-later-slider');">
                            <img src="/images/arrow-recomendation-right.png" alt="">
                        </a>
                    @endif

                    <div class="mobile-arrow-see-later-hotel">
                        <a href="javascript:void(0);" class="arrow-recomendation arrow-recomendation-left" onclick="newFunction.prevCarouselSlide('see-later-slider');">
                            <img src="/images/arrow-recomendation-left.png" alt="">
                        </a>
                        <a href="javascript:void(0);" class="arrow-recomendation arrow-recomendation-right" onclick="newFunction.nextCarouselSlide('see-later-slider');">
                            <img src="/images/arrow-recomendation-right.png" alt="">
                        </a>
                    </div>
                    <div class="see-later-slider">
                        <ul>
                            @foreach($seeLaterHotel as $hotelLatel)
                            <li>
                                <div class="one-shares">
                                    <img src="/images/hotels/{{$hotelLatel->objectId.'-'.$hotelLatel->subdomain}}/{{$hotelLatel->image}}" alt="">
                                    @if($objectCurrent->valuePercent)
                                        <div class="shares-price"><span>{{ $objectCurrent->valuePercent }}%</span></div>
                                    @endif
                                    <div class="overlay-share"></div>
                                    <a href="{{ route('hotel', ['id' => $hotelLatel->objectId, 'name' => $hotelLatel->subdomain]) }}" class="btn-reservation-hotel">{{ __('Забронировать') }}</a>
                                </div>
                                <h4><a href="{{ route('hotel', ['id' => $hotelLatel->objectId, 'name' => $hotelLatel->subdomain]) }}">{{ str_limit($hotelLatel->name, 15) }}</a></h4>
                                <span class="shares-price-hotel">{{ round($hotelLatel->price, 0) }} грн / {{ __('сутки') }}</span>
                            </li>
                             @endforeach
                        </ul>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endif
<div class="user-online hide">
    <span><b>{!! __('5 человек</b> <br> просматривают отель') !!}</b></span>
</div>
<div class="popup-gallery-images-room popup">
    <a href="javascript:void(0);" onclick="Popup.closePopup(this);" title="Закрыть" class="btn-close-popup">
        <img src="/images/close.png" alt="Закрыть окно">
    </a>
    <span></span>
    <div class="main-img-popup-gallery">
        <img src="" alt="">
    </div>
    <a href="javascript:void(0);" class="arrow-gallery-img-popup-left arrow-gallery-popup" onclick="Gallery.prevSlideGallery();">
        <img src="/images/arrow-left-carousel.png" alt="">
    </a>
    <a href="javascript:void(0);" class="arrow-gallery-img-popup-right arrow-gallery-popup" onclick="Gallery.nextSlideGallery();">
        <img src="/images/arrow-right-carousel.png" alt="">
    </a>
    <div class="list-images-current-room">
        <ul>
            <li class="active"><a href="javascript:void(0);"><img src="" alt=""></a></li>
        </ul>
    </div>
</div>
<div class="popup-hotel-map popup">
    <a href="javascript:void(0);" onclick="Popup.closePopup(this);" title="Закрыть" class="btn-close-popup">
        <img src="/images/close.png" alt="{{ __('Закрыть окно') }}">
    </a>
    <h3>{{ $object->objectCategories->name }} {{ $object->name }}</h3>
    <table>
        <tr>
            <td><b>{{ __('Адрес') }}:</b></td>
            <td>{{ $object->address }}</td>
        </tr>
        <tr>
            <td><b>{{ __('Телефон') }}:</b></td>
            <td>
                <ul>
                    <li>+380 67 510-15-04</li>
                    <li>+380 67 510-15-04</li>
                    <li><a href="javascript:void(0);" onclick="Popup.showRecallPopup();">{{ __('Обратный звонок') }}</a></li>
                </ul>
            </td>
        </tr>
        <tr>
            <td><b>Email:</b></td>
            <td>info@hotels-ok.com</td>
        </tr>
    </table>
    <div class="wrapper-hotel-map">
        <div id="map"></div>
    </div>
</div>

<div class="popup-reservation-rooms popup">
    <a href="javascript:void(0);" onclick="Popup.closePopup(this);" title="Закрыть" class="btn-close-popup">
        <img src="/images/close.png" alt="{{ __('Закрыть окно') }}">
    </a>

    <div class="content-reservation">
        <h4>{{ $object->category->name.' '.$object->name }}</h4>
        <table>

        </table>
        
        <ul class="btn-popup-reservation">
            <li><a href="javascript:void(0);" onclick="Reservation.continueReservation();">{{ __('Продолжить') }}</a></li>
            <li><a href="javascript:void(0);" onclick="Reservation.saveReservation();">{{ __('Перейти к брони') }}</a></li>
        </ul>
        
    </div>
    <div class="info-reservation-popup">
        <b>{{ __('Бронирование предусматривает:') }}</b>
        <p>Можно встретить и каменистые пляжи, и густые, нетронутые цивилизацией, леса, и множество чудесных озер! Эстония также прельщает туристов старинными замками, замечательными культовыми сооружениями и величественными крепостями, расположившимися на вершинах холмов.</p>
    </div>

</div>
<table class="hide">
    <tr class="hide clone-reservation">
        <td width="20%"><span class="name-room-for-reservation"></span></td>
        <td class="date-reservation-popup"></td>
        <td>
            <div class="other-style-for-count-room">
                <ul>
                    <li>
                        <a href="javascript:void(0);" onclick="Reservation.increamentReservation('-', this);"><i class="fa fa-minus" aria-hidden="true"></i></a>
                    </li>
                    <li>
                        <input type="text" name="guest" value="1">
                    </li>
                    <li>
                        <a href="javacript:void(0);" onclick="Reservation.increamentReservation('+', this);"><i class="fa fa-plus" aria-hidden="true"></i></a>
                    </li>
                    <li class="clear"></li>
                </ul>

            </div>
        </td>
        <td><a href="javascript:void(0);" class="remove-room-for-reservation" data-room-id="" data-array-id="" onclick="Reservation.removeReservation(this);">
                <img src="/images/remove-reservation.png" alt="">
            </a>
        </td>
    </tr>
</table>


@include('site.blocks.messages-phone')
@section('javascript')
    @parent
    <script>
        function initMap() {
            var uluru = {lat: {{ $object->longitudeGps }}, lng: {{ $object->latitudeGps }} };
            var map1 = new google.maps.Map(document.getElementById('mapLeft'), {
                zoom: 12,
                center: uluru
            });
            var marker = new google.maps.Marker({
                position: uluru,
                map: map1
            });


            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 12,
                center: uluru
            });
            var marker = new google.maps.Marker({
                position: uluru,
                map: map
            });

            google.maps.event.trigger(map, 'resize');

        }

        $(function(){

            var type = window.location.hash.substr(1);

            switch(type) {
                case 'poslugi':
                    $('.menu-hotel ul li').find('a[href="#poslugi"]').closest('li').addClass('active-tab-menu-hotel');
                    $('.tabs-menu-hotel ul').find('li[data-href="#poslugi"]').addClass('active-tab-content');
                    break;
                case 'nomera':
                    $('.menu-hotel ul li').find('a[href="#nomera"]').closest('li').addClass('active-tab-menu-hotel');
                    $('.tabs-menu-hotel ul').find('li[data-href="#nomera"]').addClass('active-tab-content');
                    break;
                case 'otzivi':
                    $('.menu-hotel ul li').find('a[href="#otzivi"]').closest('li').addClass('active-tab-menu-hotel');
                    $('.tabs-menu-hotel ul').find('li[data-href="#otzivi"]').addClass('active-tab-content');
                    break;
                default:
                    $('.menu-hotel ul li').find('a[href="#poslugi"]').closest('li').addClass('active-tab-menu-hotel');
                    $('.tabs-menu-hotel ul').find('li[data-href="#poslugi"]').addClass('active-tab-content');
                    break;
            }

            if ($(window).width() <= '767'){
                $('.btn-show-more-img-hotel').remove();
            }

        });

    </script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAzqr9UAi_u2u9LDTKpQs9ufwDOrEOviuM&callback=initMap"></script>
@endsection
@endsection