@extends('site.startpage')
@section('content')
@section('top-block')
    <section id="contacts" class="feedback">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h1>{{ __('Добавить отзыв') }}</h1>
                    @include('site.blocks.breadcrumbs')
                </div>
            </div>
        </div>
    </section>

    <section id="feedbackWrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-9">
                    <form action="{{ route('feedback') }}" method="post" class="review-form">
                        {{ csrf_field() }}
                        <div class="wrapper-feedback-class margin-top-40px">
                        <div class="wrapper-img-feedback">
                            <img src="/images/hotels/{{ $object->id }}-{{ $object->subdomain }}/{{ $object->image }}" alt="">
                        </div>
                        <div class="mini-content-feedback">
                            <h3>{{ $object->name }} <span>({{ $object->objectCategory->name }})</span></h3>
                            <span class="address">{{ $object->address }}</span>
                            <p>{{ str_limit($object->infoAboutObject, 120) }}</p>
                            <span class="date-arrival"><b>{{ __('Заезд') }}:</b> {{ $formatedDateFrom }}</span>
                            <span class="date-arrival"><b>{{ __('Отьезд') }}:</b> {{ $formatedDateTo }}</span>

                            <span class="fio-feedback"><b>{{ __('ФИО') }}:</b> {{ $reservation->name }}</span>
                        </div>
                        <div class="clear"></div>

                        <div class="main-data-feedback">
                            <div class="span-select-for-category">
                                <span onclick="newFunction.showListCategory(this);"> <span>{{ __('Категория') }}</span><i class="fa fa-angle-down"></i></span>
                                <b class="red red-error-review">{{ __('Выберите категорию') }}</b>
                                <ul>
                                    @foreach($listCategoriesPeople as $category)
                                        <li onclick="newFunction.selectCategoryReview(this);" data-id="{{ $category->id }}">{{ __($category->nameType) }}</li>
                                    @endforeach
                                </ul>
                                <input type="hidden" name="category" value="">
                            </div>
                        </div>

                        <div class="type-feedback type-feedback-block">
                            <span>{{ __('Отзыв об отеле') }}</span>
                            <ul>
                                <li>
                                    <input type="radio" name="typeFeedback" id="positiveTypeFeedback" value="1">
                                    <label for="positiveTypeFeedback"><span>{{ __('Позитивный отзыв') }}</span></label>
                                </li>
                                <li>
                                    <input type="radio" name="typeFeedback" id="negativeTypeFeedback" value="0">
                                    <label for="negativeTypeFeedback"><span>{{ __('Негативный отзыв') }}</span></label>
                                </li>
                            </ul>
                            <b class="red red-error-review">{{ __('Определите категорию отзыва') }}</b>
                        </div>

                        <div class="description-feedback">
                            <ul>
                                <li><textarea name="positiveText" id="" cols="30" rows="10" placeholder="{{ __('Достоинства') }}"></textarea>
                                    <b class="red red-error-review">{{ __('Напишите что Вам понравилось') }}</b>
                                </li>
                                <li><textarea name="negativeText" id="" cols="30" rows="10" placeholder="{{ __('Недостатки') }}"></textarea>
                                    <b class="red red-error-review">{{ __('Расскажите что Вас не устроило') }}</b>
                                </li>
                            </ul>
                        </div>

                        <div class="raiting-object">
                            <ul>
                                <li>
                                    <span>{{ __('Оценка комфорта отеля') }}</span>
                                    <ul>
                                        <li><input type="radio" name="comfortValue" value="1" id="comfort1"><label for="comfort1"><span>1</span></label></li>
                                        <li><input type="radio" name="comfortValue" value="2" id="comfort2"><label for="comfort2"><span>2</span></label></li>
                                        <li><input type="radio" name="comfortValue" value="3" id="comfort3"><label for="comfort3"><span>3</span></label></li>
                                        <li><input type="radio" name="comfortValue" value="4" id="comfort4"><label for="comfort4"><span>4</span></label></li>
                                        <li><input type="radio" name="comfortValue" value="5" id="comfort5"><label for="comfort5"><span>5</span></label></li>
                                        <li><input type="radio" name="comfortValue" value="6" id="comfort6"><label for="comfort6"><span>6</span></label></li>
                                        <li><input type="radio" name="comfortValue" value="7" id="comfort7"><label for="comfort7"><span>7</span></label></li>
                                        <li><input type="radio" name="comfortValue" value="8" id="comfort8"><label for="comfort8"><span>8</span></label></li>
                                        <li><input type="radio" name="comfortValue" value="9" id="comfort9"><label for="comfort9"><span>9</span></label></li>
                                        <li><input type="radio" name="comfortValue" value="10" id="comfort10"><label for="comfort10"><span>10</span></label></li>
                                    </ul>
                                    <b class="red red-error-review">{{ __('Оцените комфорт') }}</b>
                                </li>
                                <li>
                                    <span>{{ __('Оценка персонала') }}</span>
                                    <ul>
                                        <li><input type="radio" name="employeeValue" value="1" id="employeeValue1"><label for="employeeValue1"><span>1</span></label></li>
                                        <li><input type="radio" name="employeeValue" value="2" id="employeeValue2"><label for="employeeValue2"><span>2</span></label></li>
                                        <li><input type="radio" name="employeeValue" value="3" id="employeeValue3"><label for="employeeValue3"><span>3</span></label></li>
                                        <li><input type="radio" name="employeeValue" value="4" id="employeeValue4"><label for="employeeValue4"><span>4</span></label></li>
                                        <li><input type="radio" name="employeeValue" value="5" id="employeeValue5"><label for="employeeValue5"><span>5</span></label></li>
                                        <li><input type="radio" name="employeeValue" value="6" id="employeeValue6"><label for="employeeValue6"><span>6</span></label></li>
                                        <li><input type="radio" name="employeeValue" value="7" id="employeeValue7"><label for="employeeValue7"><span>7</span></label></li>
                                        <li><input type="radio" name="employeeValue" value="8" id="employeeValue8"><label for="employeeValue8"><span>8</span></label></li>
                                        <li><input type="radio" name="employeeValue" value="9" id="employeeValue9"><label for="employeeValue9"><span>9</span></label></li>
                                        <li><input type="radio" name="employeeValue" value="10" id="employeeValue10"><label for="employeeValue10"><span>10</span></label></li>
                                    </ul>
                                    <b class="red red-error-review">{{ __('Оцените персонал') }}</b>
                                </li>
                                <li>
                                    <span>{{ __('Оценка сервиса') }}</span>
                                    <ul>
                                        <li><input type="radio" name="serviceValue" value="1" id="serviceValue1"><label for="serviceValue1"><span>1</span></label></li>
                                        <li><input type="radio" name="serviceValue" value="2" id="serviceValue2"><label for="serviceValue2"><span>2</span></label></li>
                                        <li><input type="radio" name="serviceValue" value="3" id="serviceValue3"><label for="serviceValue3"><span>3</span></label></li>
                                        <li><input type="radio" name="serviceValue" value="4" id="serviceValue4"><label for="serviceValue4"><span>4</span></label></li>
                                        <li><input type="radio" name="serviceValue" value="5" id="serviceValue5"><label for="serviceValue5"><span>5</span></label></li>
                                        <li><input type="radio" name="serviceValue" value="6" id="serviceValue6"><label for="serviceValue6"><span>6</span></label></li>
                                        <li><input type="radio" name="serviceValue" value="7" id="serviceValue7"><label for="serviceValue7"><span>7</span></label></li>
                                        <li><input type="radio" name="serviceValue" value="8" id="serviceValue8"><label for="serviceValue8"><span>8</span></label></li>
                                        <li><input type="radio" name="serviceValue" value="9" id="serviceValue9"><label for="serviceValue9"><span>9</span></label></li>
                                        <li><input type="radio" name="serviceValue" value="10" id="serviceValue10"><label for="serviceValue10"><span>10</span></label></li>
                                    </ul>
                                    <b class="red red-error-review">{{ __('Оцените сервис') }}</b>
                                </li>
                                <li>
                                    <span>{{ __('Оценка чистоты') }}</span>
                                    <ul>
                                        <li><input type="radio" name="cleanValue" value="1" id="cleanValue1"><label for="cleanValue1"><span>1</span></label></li>
                                        <li><input type="radio" name="cleanValue" value="2" id="cleanValue2"><label for="cleanValue2"><span>2</span></label></li>
                                        <li><input type="radio" name="cleanValue" value="3" id="cleanValue3"><label for="cleanValue3"><span>3</span></label></li>
                                        <li><input type="radio" name="cleanValue" value="4" id="cleanValue4"><label for="cleanValue4"><span>4</span></label></li>
                                        <li><input type="radio" name="cleanValue" value="5" id="cleanValue5"><label for="cleanValue5"><span>5</span></label></li>
                                        <li><input type="radio" name="cleanValue" value="6" id="cleanValue6"><label for="cleanValue6"><span>6</span></label></li>
                                        <li><input type="radio" name="cleanValue" value="7" id="cleanValue7"><label for="cleanValue7"><span>7</span></label></li>
                                        <li><input type="radio" name="cleanValue" value="8" id="cleanValue8"><label for="cleanValue8"><span>8</span></label></li>
                                        <li><input type="radio" name="cleanValue" value="9" id="cleanValue9"><label for="cleanValue9"><span>9</span></label></li>
                                        <li><input type="radio" name="cleanValue" value="10" id="cleanValue10"><label for="cleanValue10"><span>10</span></label></li>
                                    </ul>
                                    <b class="red red-error-review">{{ __('Оцените чистоту') }}</b>
                                </li>
                                <li>
                                    <span>{{ __('Оценка цены-качества') }}</span>
                                    <ul>
                                        <li><input type="radio" name="qualityPriceValue" value="1" id="qualityPriceValue1"><label for="qualityPriceValue1"><span>1</span></label></li>
                                        <li><input type="radio" name="qualityPriceValue" value="2" id="qualityPriceValue2"><label for="qualityPriceValue2"><span>2</span></label></li>
                                        <li><input type="radio" name="qualityPriceValue" value="3" id="qualityPriceValue3"><label for="qualityPriceValue3"><span>3</span></label></li>
                                        <li><input type="radio" name="qualityPriceValue" value="4" id="qualityPriceValue4"><label for="qualityPriceValue4"><span>4</span></label></li>
                                        <li><input type="radio" name="qualityPriceValue" value="5" id="qualityPriceValue5"><label for="qualityPriceValue5"><span>5</span></label></li>
                                        <li><input type="radio" name="qualityPriceValue" value="6" id="qualityPriceValue6"><label for="qualityPriceValue6"><span>6</span></label></li>
                                        <li><input type="radio" name="qualityPriceValue" value="7" id="qualityPriceValue7"><label for="qualityPriceValue7"><span>7</span></label></li>
                                        <li><input type="radio" name="qualityPriceValue" value="8" id="qualityPriceValue8"><label for="qualityPriceValue8"><span>8</span></label></li>
                                        <li><input type="radio" name="qualityPriceValue" value="9" id="qualityPriceValue9"><label for="qualityPriceValue9"><span>9</span></label></li>
                                        <li><input type="radio" name="qualityPriceValue" value="10" id="qualityPriceValue10"><label for="qualityPriceValue10"><span>10</span></label></li>
                                    </ul>
                                    <b class="red red-error-review">{{ __('Оцените цену-качества') }}</b>
                                </li>
                                <li>
                                    <span>{{ __('Оценка местоположения') }}</span>
                                    <ul>
                                        <li><input type="radio" name="locationValue" value="1" id="locationValue1"><label for="locationValue1"><span>1</span></label></li>
                                        <li><input type="radio" name="locationValue" value="2" id="locationValue2"><label for="locationValue2"><span>2</span></label></li>
                                        <li><input type="radio" name="locationValue" value="3" id="locationValue3"><label for="locationValue3"><span>3</span></label></li>
                                        <li><input type="radio" name="locationValue" value="4" id="locationValue4"><label for="locationValue4"><span>4</span></label></li>
                                        <li><input type="radio" name="locationValue" value="5" id="locationValue5"><label for="locationValue5"><span>5</span></label></li>
                                        <li><input type="radio" name="locationValue" value="6" id="locationValue6"><label for="locationValue6"><span>6</span></label></li>
                                        <li><input type="radio" name="locationValue" value="7" id="locationValue7"><label for="locationValue7"><span>7</span></label></li>
                                        <li><input type="radio" name="locationValue" value="8" id="locationValue8"><label for="locationValue8"><span>8</span></label></li>
                                        <li><input type="radio" name="locationValue" value="9" id="locationValue9"><label for="locationValue9"><span>9</span></label></li>
                                        <li><input type="radio" name="locationValue" value="10" id="locationValue10"><label for="locationValue10"><span>10</span></label></li>
                                    </ul>
                                    <b class="red red-error-review">{{ __('Оцените местоположение') }}</b>
                                </li>
                            </ul>
                        </div>
                        @if($object->objectCategory->id != 18)

                            <div class="type-feedback is-recommendation-hotel">
                                <ul>
                                    <li>
                                        <input type="radio" name="isRecHotel" id="recommendationHotel" value="1">
                                        <label for="recommendationHotel"><span>{{ __('Рекомендую отель') }}</span></label>
                                    </li>
                                    <li>
                                        <input type="radio" name="isRecHotel" id="noRecommendationHotel" value="0">
                                        <label for="noRecommendationHotel"><span>{{ __('Не рекомендую отель') }}</span></label>

                                    </li>
                                </ul>
                                <b class="red red-error-review">{{ __('Укажите рекомендуете ли Вы отель?') }}</b>
                            </div>
                        @endif
                        

                        <div class="review-about-project">
                            <span>{{ __('Отзыв о') }} {{ env('PROJECT_NAME') }}</span>
                            <div class="raiting-object raiting-project">
                                <ul>
                                    <li>
                                        <span>{{ __('Оценка проекта') }} {{ env('PROJECT_NAME') }}</span>
                                        <ul>
                                            <li><input type="radio" name="projectValue" value="1" id="projectValue1"><label for="projectValue1"><span>1</span></label></li>
                                            <li><input type="radio" name="projectValue" value="2" id="projectValue2"><label for="projectValue2"><span>2</span></label></li>
                                            <li><input type="radio" name="projectValue" value="3" id="projectValue3"><label for="projectValue3"><span>3</span></label></li>
                                            <li><input type="radio" name="projectValue" value="4" id="projectValue4"><label for="projectValue4"><span>4</span></label></li>
                                            <li><input type="radio" name="projectValue" value="5" id="projectValue5"><label for="projectValue5"><span>5</span></label></li>
                                            <li><input type="radio" name="projectValue" value="6" id="projectValue6"><label for="projectValue6"><span>6</span></label></li>
                                            <li><input type="radio" name="projectValue" value="7" id="projectValue7"><label for="projectValue7"><span>7</span></label></li>
                                            <li><input type="radio" name="projectValue" value="8" id="projectValue8"><label for="projectValue8"><span>8</span></label></li>
                                            <li><input type="radio" name="projectValue" value="9" id="projectValue9"><label for="projectValue9"><span>9</span></label></li>
                                            <li><input type="radio" name="projectValue" value="10" id="projectValue10"><label for="projectValue10"><span>10</span></label></li>
                                        </ul>
                                        <b class="red red-error-review">{{ __('Оцените пожалуйста нас') }}</b>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="main-review-feedback">
                            <textarea name="global-review" id="" cols="30" rows="10" class="textarea-global-review" placeholder="{{ __('Оставьте отзыв') }}"></textarea>
                            <b class="red red-error-review">{{ __('Оставьте отзыв о нашем сервисе. Нам важно Ваше мнение!') }}</b>
                        </div>
                            <input type="submit" onclick="newFunction.sendReview(event);" name="submit" value="{{ __('Оставить отзыв') }}" class="btn-submit-review">
                    </div>
                        <input type="hidden" name="email" value="{{ $email }}">
                        <input type="hidden" name="code" value="{{ $code->code }}">
                    </form>
                </div>
                @include('site.blocks.right-sidebar-reviews')
            </div>
        </div>
    </section>

@endsection

@include('site.blocks.messages-phone')
@endsection