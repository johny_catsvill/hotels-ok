@extends('site.startpage')
@section('content')
@section('top-block')
    <section id="topBlock">
        <section id="searchHotel">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="search">
                            @include('site.blocks.search-hotel')
                            <div class="overlay-search"></div>
                        </div>

                    </div>

                </div>
            </div>
        </section>
    </section>
@endsection
    <section id="categoryHome">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <a href="javascript:void(0);" class="btn-mobile-show-categories" onclick="newFunction.showMobileCategory(this);">{{ __('Категории') }}</a>
                <div class="category-list">
                    <ul>
                        <li class="active">
                            <a href="javascript:void(0);" data-tab="1" onclick="newFunction.changeTabCities(this);">{{ __('Гостиницы Украины') }}</a>
                        </li>
                        <li>
                            <a href="javascript:void(0);" data-tab="2" onclick="newFunction.changeTabCities(this);">{{ __('Хостелы Украины') }}</a>
                        </li>
                        <li>
                            <a href="javascript:void(0);" data-tab="3" onclick="newFunction.changeTabCities(this);">{{ __('Квартиры Украины') }}</a>
                        </li>
                        <li>
                            <a href="javascript:void(0);" data-tab="4" onclick="newFunction.changeTabCities(this);">{{ __('Отдых на море') }}</a>
                        </li>
                        <li>
                            <a href="javascript:void(0);" data-tab="5" onclick="newFunction.changeTabCities(this);">{{ __('Отдых в горах') }}</a>
                        </li>
                        <li>
                            <a href="javascript:void(0);" data-tab="6" onclick="newFunction.changeTabCities(this);">{{ __('Оздоровление') }}</a>
                        </li>
                    </ul>
                </div>

            </div>
        </div>
    </div>
    </section>
    <section id="listCities">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="list-cities-category active" data-tab="1">
                            @foreach(array_chunk($citiesHotels, 5) as $cities)

                                <ul class="@if($loop->last) last-list-cities @endif">
                                @foreach($cities as $c)
                                    <li>
                                        <a href="{{ route('category') }}/?search={{ $c->name }}&countryId={{ $c->countryId }}&cityId={{ $c->id }}&typeObject=[2]">{{ $c->name }}
                                            <sup>{{ $c->objects_count}}</sup>
                                            <img src="/images/cities/{{ $c->image }}" alt=""></a>
                                    </li>
                                @endforeach
                                </ul>
                            @endforeach
                    </div>
                    <div class="list-cities-category" data-tab="2">
                        @foreach(array_chunk($citiesHostels, 5) as $cities)

                            <ul class="@if($loop->last) last-list-cities @endif">
                                @foreach($cities as $c)
                                    <li>
                                        <a href="{{ route('category') }}/?search={{ $c->name }}&countryId={{ $c->countryId }}&cityId={{ $c->id }}&typeObject=[16]">{{ $c->name }}
                                            <sup>{{ $c->objects_count}}</sup>
                                            <img src="/images/cities/{{ $c->image }}" alt=""></a>
                                    </li>
                                @endforeach
                            </ul>
                        @endforeach
                    </div>
                    <div class="list-cities-category" data-tab="3">
                        @foreach(array_chunk($citiesApartments, 5) as $cities)

                            <ul class="@if($loop->last) last-list-cities @endif">
                                @foreach($cities as $c)
                                    <li>
                                        <a href="{{ route('category') }}/?search={{ $c->name }}&countryId={{ $c->countryId }}&cityId={{ $c->id }}&typeObject=[18]">{{ $c->name }}
                                            <sup>{{ $c->objects_count}}</sup>
                                            <img src="/images/cities/{{ $c->image }}" alt=""></a>
                                    </li>
                                @endforeach
                            </ul>
                        @endforeach
                    </div>
                    <div class="list-cities-category" data-tab="4">
                        @foreach(array_chunk($citiesCategorySea, 6) as $cities)

                            <ul class="@if($loop->last) last-list-cities @endif">
                                @foreach($cities as $c)
                                    <li>
                                        <a href="{{ route('category') }}/?search={{ $c->name }}&countryId={{ $c->countryId }}&cityId={{ $c->id }}">{{ $c->name }}
                                            <img src="/images/cities/{{ $c->image }}" alt="">
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        @endforeach
                    </div>
                    <div class="list-cities-category" data-tab="5">
                        @foreach(array_chunk($citiesCategoryMountain, 10) as $cities)

                            <ul class="@if($loop->last) last-list-cities @endif">
                                @foreach($cities as $c)
                                    <li>
                                        <a href="{{ route('category') }}/?search={{ $c->name }}&countryId={{ $c->countryId }}&cityId={{ $c->id }}">{{ $c->name }}
                                            <img src="/images/cities/{{ $c->image }}" alt="">
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        @endforeach
                    </div>
                    <div class="list-cities-category" data-tab="6">
                        @foreach(array_chunk($citiesCategoryRecovery, 5) as $cities)

                            <ul class="@if($loop->last) last-list-cities @endif">
                                @foreach($cities as $c)
                                    <li>
                                        <a href="{{ route('category') }}/?search={{ $c->name }}&countryId={{ $c->countryId }}&cityId={{ $c->id }}">{{ $c->name }}
                                            <img src="/images/cities/{{ $c->image }}" alt="">
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        @endforeach
                    </div>

                    <a href="{{ route('hotels-ukraine') }}" class="btn-more-cities">{{ __('Все города Украины') }}</a>
                </div>
            </div>
        </div>
    </section>
    <section id="plusesReservation">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="pluse-reservation-hotel">
                        <h2>{{ __('Преимущества бронирования') }}</h2>
                        <span class="line-blue first-line-caption"></span>
                        <span class="line-blue two-line-caption"></span>
                        <div class="list-pluses">
                            <ul>
                                <li>
                                    <div class="one-pluse animated bounceInUp">
                                        <img src="images/pluse-icon-1.png" alt="">
                                        <div class="two-border-pluse"></div>
                                    </div>
                                    <span>{!! __('Простота <br> бронирования') !!}</span>
                                </li>
                                <li>
                                    <div class="one-pluse animated bounceInUp">
                                        <img src="images/pluse-icon-2.png" alt="">
                                        <div class="two-border-pluse"></div>
                                    </div>
                                    <span>{!! __('Бесплатное <br> бронирование') !!}</span>
                                </li>
                                <li>
                                    <div class="one-pluse animated bounceInUp">
                                        <img src="images/pluse-icon-3.png" alt="">
                                        <div class="two-border-pluse"></div>
                                    </div>
                                    <span>{!! __('Удобный <br> поиск') !!}</span>
                                </li>

                                <li>
                                    <div class="one-pluse animated bounceInUp">
                                        <img src="images/pluse-icon-4.png" alt="">
                                        <div class="two-border-pluse"></div>
                                    </div>
                                    <span>{!! __('Мгновенное <br> подтверждение') !!}</span>
                                </li>

                                <li>
                                    <div class="one-pluse animated bounceInUp">
                                        <img src="images/pluse-icon-5.png" alt="">
                                        <div class="two-border-pluse"></div>
                                    </div>
                                    <span>{!! __('Личный кабинет <br> в системе') !!}</span>
                                </li>
                                <li>
                                    <div class="one-pluse animated bounceInUp">
                                        <img src="images/pluse-icon-6.png" alt="">
                                        <div class="two-border-pluse"></div>
                                    </div>
                                    <span>{!! __('10695 отзывов <br> о гостиницах <br> украины') !!}</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="shares">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="shares-style">
                        <h2>{{ __('Акционные предложения') }}</h2>
                        <span class="line-blue first-line-caption"></span>
                        <span class="line-blue two-line-caption"></span>
                        <div class="list-shares">
                            <ul>
                                @foreach($listPromotialOffers as $object)
                                    <li>
                                        <div class="one-shares">
                                            <img src="/images/hotels/{{ $object->objectId.'-'.$object->subdomain.'/'.\App\Traits\croppedPhotos::changeImg($object->image) }}" alt="">
                                            <div class="shares-price"><span>{{ $object->valuePercent }}%</span></div>
                                            <div class="overlay-share"></div>
                                            <a href="{{ route('hotel', ['id' => $object->objectId, 'url' => $object->subdomain]) }}" class="btn-reservation-hotel">{{ __('Забронировать') }}</a>
                                        </div>
                                        <h4><a href="{{ route('hotel', ['id' => $object->objectId, 'name' => $object->subdomain]) }}">{{ $object->objectName }} </a><span>г. {{ $object->cityName }}</span></h4>
                                        <span class="shares-price-hotel">{{ round($object->price - ($object->price * $object->valuePercent) / 100) }} грн / сутки</span>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="userBlock">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="pluses-user">
                        <h2>{{ __('Преимущества для пользователей') }}</h2>
                        <span class="line-blue first-line-caption"></span>
                        <span class="line-blue two-line-caption"></span>
                        <div class="list-pluses-user">
                            <ul>
                                <li>
                                    <div class="one-pluse-user">
                                        <div class="wrap-img-plus">
                                            <img src="images/pluse-user-1.png" alt="">
                                        </div>
                                        <span>{!! __('Удобная панель <br> пользователя') !!}</span>
                                    </div>
                                </li>
                                <li>
                                    <div class="one-pluse-user">
                                        <div class="wrap-img-plus">
                                            <img src="images/pluse-user-2.png" alt="">
                                        </div>
                                        <span>{!! __('Накопления балов <br> за бронирование') !!}</span>
                                    </div>
                                </li>
                                <li>
                                    <div class="one-pluse-user">
                                        <div class="wrap-img-plus">
                                            <img src="images/pluse-user-3.png" alt="">
                                        </div>
                                        <span>{!! __('Индивидуальные <br> скидки') !!}</span>
                                    </div>
                                </li>
                                <li>
                                    <div class="one-pluse-user">
                                        <div class="wrap-img-plus">
                                            <img src="images/pluse-user-4.png" alt="">
                                        </div>
                                        <span>{!! __('Партнерская <br> программа') !!}</span>
                                    </div>
                                </li>
                                <li>
                                    <div class="one-pluse-user">
                                        <div class="wrap-img-plus">
                                            <img src="images/pluse-user-5.png" alt="">
                                        </div>
                                        <span>{!! __('Поиск ближайших <br> отелей по gps') !!}</span>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="reservationSteps">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="reservation-steps-style">
                        <h2>{{ __('Как забронировать') }}</h2>
                        <span class="line-blue first-line-caption"></span>
                        <span class="line-blue two-line-caption"></span>
                        <div class="list-steps-reservation">
                            <ul>
                                <li>
                                    <div class="one-step">
                                        <div class="wrapper-img-step">
                                            <img src="images/step-1.png" alt="">
                                            <div class="first-border-step"><b>1</b>
                                                <div class="two-border-pluse"></div>
                                            </div>
                                        </div>
                                        <span>{!! __('Выберите любую <br> гостиницу') !!}</span>
                                    </div>
                                </li>
                                <li>
                                    <div class="wrapper-img-step">
                                        <img src="images/step-2.png" alt="">
                                        <div class="first-border-step"><b>2</b>
                                            <div class="two-border-pluse"></div>
                                        </div>
                                    </div>
                                    <span>{!! __('Укажите способ <br> бронирования') !!}</span>
                                </li>
                                <li>
                                    <div class="wrapper-img-step">
                                        <img src="images/step-3.png" alt="">
                                        <div class="first-border-step"><b>3</b>
                                            <div class="two-border-pluse"></div>
                                        </div>
                                    </div>
                                    <span>{!! __('Заполните поля <br> бронирования') !!}</span>
                                </li>
                                <li>
                                    <div class="wrapper-img-step">
                                        <img src="images/step-4.png" alt="">
                                        <div class="first-border-step"><b>4</b>
                                            <div class="two-border-pluse"></div>
                                        </div>
                                    </div>
                                    <span>{!! __('Подтвердите <br> бронирование') !!}</span>
                                </li>
                                <li>
                                    <div class="wrapper-img-step">
                                        <img src="images/step-5.png" alt="">
                                        <div class="first-border-step"><b>5</b>
                                            <div class="two-border-pluse"></div>
                                        </div>
                                    </div>
                                    <span>{!! __('Ожидайте нашего <br> звонка') !!}</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="reviews">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="reviews-style">
                        <h2>{{ __('Отзывы наших клиентов') }}</h2>
                        <span class="line-blue first-line-caption"></span>
                        <span class="line-blue two-line-caption"></span>
                        <div class="list-reviews">
                            <ul>
                                <li>
                                    <div class="one-review">
                                        <img src="images/review-1.png" alt="">
                                        <span>Ирина Белая</span>
                                        <p>Впервые воспользовалась этим сайтом, и не пожалела!! Ребята сработали оперативно, нашли квартиру во Львове по искомым параметрам и по цене приемлемой для нашего скромного бюджета, очень быстро оформили </p>
                                    </div>
                                </li>
                                <li>
                                    <div class="one-review">
                                        <img src="images/review-2.png" alt="">
                                        <span>Ирина Белая</span>
                                        <p>Во Вторые воспользовалась этим сайтом, и не пожалела!! Ребята сработали оперативно, нашли квартиру во Львове по искомым параметрам и по цене приемлемой для нашего скромного бюджета, очень быстро оформили </p>
                                    </div>
                                </li>
                                <li>
                                    <div class="one-review">
                                        <img src="images/review-3.png" alt="">
                                        <span>Ирина Белая</span>
                                        <p>В третье воспользовалась этим сайтом, и не пожалела!! Ребята сработали оперативно, нашли квартиру во Львове по искомым параметрам и по цене приемлемой для нашего скромного бюджета, очень быстро оформили </p>
                                    </div>
                                </li>
                                <li>
                                    <div class="one-review">
                                        <img src="images/review-3.png" alt="">
                                        <span>Ирина Белая</span>
                                        <p>В третье воспользовалась этим сайтом, и не пожалела!! Ребята сработали оперативно, нашли квартиру во Львове по искомым параметрам и по цене приемлемой для нашего скромного бюджета, очень быстро оформили </p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="arrows-reviews-carousel">
                            <a href="javascript:void(0);" onclick="newFunction.prevCarouselSlide('list-reviews');" class="arrow-carousel arrow-left-carousel">
                                <img src="images/arrow-left-carousel.png" alt="">
                            </a>
                            <a href="javascript:void(0);" onclick="newFunction.nextCarouselSlide('list-reviews');" class="arrow-carousel arrow-right-carousel">
                                <img src="images/arrow-right-carousel.png" alt="">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection