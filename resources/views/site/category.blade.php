@extends('site.startpage')
@section('top-block')
    <section id="category">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="category-wrapper">
                        <table>
                            <tr>
                                <td>
                                    <h1>{{ __('Гостиницы Украины') }}</h1>
                                    <ul class="mini-breadcrumbs-category">
                                        <li><a href="/">{{ __('Главная') }}</a></li>
                                        <li>/</li>
                                        <li>{{ __('Гостиницы') }} @if($city){{ \App\Helpers\Func::getDeclineCity($city->name) }} @else {{ __('Украины') }} @endif</li>
                                    </ul>
                                </td>
                                <td>
                                    <span class="count-objects">{{ $countObjectCity }}</span>
                                    <span class="mini-info-category">{{ __('гостиниц') }} в @if($city){{ $city->name }} @else {{ __('Украины') }} @endif</span>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="category-overlay"></div>
    </section>
@endsection

@section('content')

    <section id="categoryForm">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    @include('site.blocks.search-hotel')
                </div>
            </div>
        </div>
    </section>

    <section id="categoryContent">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-sm-4">
                    <a href="javascript:void(0);" onclick="newFunction.currentPosition(event);" class="btn-search-object-beside">{{ __('Поиск ближайщих отелей') }}</a>
                    <div class="wrapper-span-beside-object-distance">
                        <span onclick="newFunction.listDistance();">{{ __('Выберите радиус') }}</span>
                        <i onclick="newFunction.listDistance();" class="fa fa-angle-down"></i>
                        <div class="list-distance-for-page-category">
                            <ul>
                                <li><span data-radius="1" onclick="newFunction.selectDistance(this);">1 км</span></li>
                                <li><span data-radius="3" onclick="newFunction.selectDistance(this);">3 км</span></li>
                                <li><span data-radius="5" onclick="newFunction.selectDistance(this);">5 км</span></li>
                                <li><span data-radius="10" onclick="newFunction.selectDistance(this);">10 км</span></li>
                            </ul>
                            <input type="hidden" name="radius" value="5">
                        </div>
                    </div>
                    <div class="wrap-filters-category wrapper-sidebar-html">
                        @include('site.blocks.left-sidebar-category')
                    </div>

                </div>
                <div class="col-lg-9 col-sm-8">
                    <div class="main-content-category">
                        @include('site.blocks.map')
                        <div class="sort-filter">
                            <form action="">
                                <b>{{ __('Сортировка') }}:</b>
                                <ul>
                                    <li>
                                        <a onclick="Filter.setSortField(this);" @if($sort == 'recommendation') class="active" @endif href="javascript:void(0);"
                                           data-sort="recommendation">{{ __('Рекомендованные') }}</a></li>
                                    <li><a href="javascript:void(0);" @if($sort == 'price-up') class="active" @endif onclick="Filter.setSortField(this);"
                                           data-sort="price-up">{{ __('Сначале дешевые') }}</a></li>
                                    <li><a href="javascript:void(0);" @if($sort == 'price-down') class="active" @endif onclick="Filter.setSortField(this);"
                                           data-sort="price-down">{{ __('Сначала дорогие') }}</a></li>
                                    <li><a href="javascript:void(0);" @if($sort == 'raiting') class="active" @endif onclick="Filter.setSortField(this);"
                                           data-sort="raiting">{{ __('По рейтингу') }}</a></li>
                                    {{--<li>--}}
                                        {{--<a href="javascript:void(0);" @if($sort == 'point-map') class="active" @endif onclick="Filter.setSortField(this);"--}}
                                           {{--data-sort="point-map">{{ __('Расстояние к ориентиру') }}</a>--}}
                                    {{--</li>--}}
                                </ul>
                                <input type="hidden" onchange="FIlter.categoryFIlter();" name="typeSort" value="recommendation">
                            </form>
                        </div>

                        <div class="list-rooms-hotel list-objects">
                            <div class="cities-region-or-city">
                                <b>{{ __('Города') }} {{ preg_replace('/вская/', 'вской', $region->name, -1) }} {{ __('области') }}:</b>
                                <ul>
                                    @foreach($listCitiesByRegion as $cityRegion)
                                        @if(isset($city) && $city && $cityRegion->id == $city->id)
                                            <li><a href="javascript:void(0);" class="active">{{ $cityRegion->name }} ({{ $cityRegion->countObjects }})</a></li>
                                        @else
                                            <li><a href="/category?cityId={{ $cityRegion->id }}&search={{ $cityRegion->name }}">{{ $cityRegion->name }} ({{ $cityRegion->countObjects }})</a></li>
                                        @endif
                                    @endforeach
                                </ul>
                            </div>
                            <div class="wrap-wrap-objects">
                                @if(count($listObjectsAll))
                                    @include('site.blocks.categories-objects', ['listObjects' => $listObjects, 'city' => $city, 'allObjectsSession' => $listObjectsAll])
                                @else
                                    <p>{{ __('На выбранные даты нету отелей в городе') }} <b>{{ $city->name }}</b></p>
                                @endif
                            </div>

                        </div>
                        <div class="wrapper-show-more">

                            @if(count($listObjectsAll) > 10)
                                <a href="javascript:void(0);" class="btn-show-more-object" onclick="Filter.showMoreByFilter(this);">{{ __('Показать еще') }}</a>
                            @endif
                            @include('global.preloader')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <input type="hidden" name="currentPage" value="1">
    @include('site.blocks.messages-phone')

    <div class="loader-inner hide refresh-page">
        <div class="loader-line-wrap">
            <div class="loader-line"></div>
        </div>
        <div class="loader-line-wrap">
            <div class="loader-line"></div>
        </div>
        <div class="loader-line-wrap">
            <div class="loader-line"></div>
        </div>
        <div class="loader-line-wrap">
            <div class="loader-line"></div>
        </div>
        <div class="loader-line-wrap">
            <div class="loader-line"></div>
        </div>
    </div>

    {{--<div class="popup-hotel-map popup">--}}
        {{--<a href="javascript:void(0);" onclick="Popup.closePopup(this);" title="Закрыть" class="btn-close-popup">--}}
            {{--<img src="/images/close.png" alt="{{ __('Закрыть окно') }}">--}}
        {{--</a>--}}
        {{--<h3></h3>--}}
        {{--<table>--}}
            {{--<tr>--}}
                {{--<td><b>{{ __('Адрес') }}:</b></td>--}}
                {{--<td class="address-object"></td>--}}
            {{--</tr>--}}
            {{--<tr>--}}
                {{--<td><b>{{ __('Телефон') }}:</b></td>--}}
                {{--<td>--}}
                    {{--<ul>--}}
                        {{--<li>+380 67 510-15-04</li>--}}
                        {{--<li>+380 67 510-15-04</li>--}}
                        {{--<li><a href="javascript:void(0);" onclick="">{{ __('Обратный звонок') }}</a></li>--}}
                    {{--</ul>--}}
                {{--</td>--}}
            {{--</tr>--}}
            {{--<tr>--}}
                {{--<td><b>Email:</b></td>--}}
                {{--<td>info@hotels-ok.com</td>--}}
            {{--</tr>--}}
        {{--</table>--}}
        {{--<div class="wrapper-hotel-map">--}}
            {{--<div id="map"></div>--}}
        {{--</div>--}}
    {{--</div>--}}
    @section('javascript')
        @parent
        <script>
//            function showMapCategory(element, latitude, longitude) {
//                var el = $(element);
//                var nameObject = el.attr('data-object-name');
//                $('.popup-hotel-map').find('h3').text(nameObject);
//                $('.popup-hotel-map').find('.address-object').text(el.text());
//                latitude = latitude;
//                longitude = longitude;
////                Popup.showPopup('popup-hotel-map', event);
//                setTimeout( function(){initMap(latitude, longitude);} , 400);
//            }
//
            function initMap(latitude, longitude) {
                console.log(typeof latitude, typeof longitude);
                var uluru = {lat: +latitude, lng: +longitude };
                var map = new google.maps.Map(document.getElementById('map'), {
                    zoom: 15,
                    center: uluru
                });
                var marker = new google.maps.Marker({
                    position: uluru,
                    map: map
                });

                google.maps.event.trigger(map, "resize");
            }


            $(function() {
                $('.left-sidebar-category .label-checkbox-agree input').each(function(indx, element) {
                    var el = $(element);
                    if(el.is(':checked')) {
                        el.closest('table').show();
                    }
                });
            });

            //showMapCategory('hz', 49.9893233, 36.2493629);
        </script>
    @endsection
<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyAzqr9UAi_u2u9LDTKpQs9ufwDOrEOviuM&libraries=geometry"></script>
<div class="overlay-category"></div>

<script type="text/javascript">
    if(navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            var latitude = position.coords.latitude;
            var longitude = position.coords.longitude;
            console.log(latitude, longitude);

        });
    }
    else {
        alert("Geolocation API не поддерживается в вашем браузере");
    }
</script>
<div class="quick-view popup">

</div>


@endsection

