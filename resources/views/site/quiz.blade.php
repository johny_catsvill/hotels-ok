@extends('site.startpage')

@section('content')
    @section('top-block')

        <section id="contacts" class="feedback">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h1>{{ __('Тест') }}</h1>
                        @include('site.blocks.breadcrumbs')
                    </div>
                </div>
            </div>
        </section>

        <div id="quizWrapper">
            <div class="container">
                <div class="col-lg-12">
                    @include('site.blocks.left-sidebar-company')
                    <div class="col-lg-9">
                        <div class="main-content-test">
                            <h2>{{ __('Тест') }}</h2>
                            <h3>{{ __('Тест: Как много ты знаешь уникальных мест в Украине?') }}</h3>
                            <div class="wrap-img-test">
                                <div class="wrap-promocode-block">
                                    <h4></h4>
                                    <span>10% кешбек</span>
                                    <p>на бронирование гостиницы <br>
                                        на всей территории Украины</p>
                                    <div class="background-promocode-block"></div>
                                </div>
                                <div class="girl-with-promocode">
                                    <img src="/images/girl-promocode.png" alt="">
                                </div>
                                <img src="/images/test/start.jpg" alt="">
                            </div>
                            <p>
                                {{ __('Проверьте свои знания - пройдите наш простой тест! Ответьте на все вопросы правильно и получите самую большую скидку на бронирование гостиницы!') }}
                            </p>
                            @foreach($questions as $question)
                                <div class="question-wrapper" data-img="{{ $question->image }}" data-iteration="{{ $loop->iteration }}" data-text="{{ $question->name }}">
                                <input type="hidden" name="questionId" value="{{ $question->id }}">
                                <ul>
                                    @foreach($question->answers as $answer)
                                        <li class="@if(!$answer->isTrue) bad-answer @else good-answer @endif">
                                            <div class="procent-question">{{ $answer->procent }}%</div>
                                            <input type="hidden" name="answerId" value="{{ $answer->id }}">
                                            <div class="border-left-hover"></div>
                                            <a onclick="QuizController.selectQuestion(this);" href="javascript:void(0);">{{ $answer->answer }}</a>
                                            @if(!$answer->isTrue)
                                                <div class="wrap-answer-question">
                                                    <span>{{ __('Это неправильный ответ') }}</span>
                                                    <p>{{ $answer->bad_text }}</p>
                                                </div>
                                            @endif
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                            @endforeach
                            <a href="javascript:void(0);" onclick="QuizController.startTest();" class="start-test-btn">{{ __('Начать') }}</a>
                            <a href="javascript:void(0);" onclick="QuizController.nextTest();" class="next-test-btn">{{ __('Далее') }}</a>

                            <div class="wrap-registration-promocode">
                                <h4>Зарегистрируйтесь и <span>получите</span> кешбек <br> в размере <span>10%</span> от бронирования</h4>
                                <form action="">
                                    <ul>
                                        <li>
                                            <input type="text" name="name" placeholder="Введите имя">
                                        </li>
                                        <li>
                                            <input type="text" name="email" placeholder="Ваш email">
                                        </li>
                                        <li>
                                            <a href="">Зарегистрироваться</a>
                                        </li>
                                    </ul>
                                </form>
                                <span>или зарегистрироваться через</span>
                                <ul>
                                    <li><a href=""><i class="fa fa-facebook"></i></a></li>
                                    <li><a href=""><i class="fa fa-google-plus"></i></a></li>
                                    <li><a href=""><i class="fa fa-instagram"></i></a></li>
                                    <li><a href=""><i class="fa fa-twitter"></i></a></li>
                                    <li><a href=""><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                </ul>
                            </div>

                            <ul class="social-test-class">
                                <li><a href=""><i class="fa fa-facebook"></i></a></li>
                                <li><a href=""><i class="fa fa-google-plus"></i></a></li>
                                <li><a href=""><i class="fa fa-instagram"></i></a></li>
                                <li><a href=""><i class="fa fa-twitter"></i></a></li>
                                <li><a href=""><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    @endsection
@endsection