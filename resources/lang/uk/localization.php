<?php

return [
    'count_objects' => '[1] готель|[2, 4] готеля|[5, INF]готелів',
    'reviews' => 'вiдклик|вiдклика|вiдкликiв',
    'countPeople' => '[1] людина|[2, 4] людини|[5, INF] людей'
];