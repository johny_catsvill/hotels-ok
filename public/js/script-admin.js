
var MainFunction = {


    generateCode:function(element) {
        var el = $(element);
        var code = Math.random().toString(36).slice(-12);
        el.siblings('input').val(code.replace(/\./g, ''));
    },



    excelDownload:function() {

        var string = '';
        var status = $('select[name="status"]').val();
        var dateFrom = $('input[name="dateFrom"]').val();
        var dateTo = $('input[name="dateTo"]').val();

        if(status) {
            string += 'status=' + status;
        }



        if(dateFrom) {
            if(string) {
                string += '&dateFrom=' + dateFrom;
            }
            else {
                string += 'dateFrom=' + dateFrom;
            }

        }

        if(dateTo) {
            if(string) {
                string += '&dateTo=' + dateTo;
            }
            else {
                string += 'dateTo=' + dateTo;
            }
        }

        window.open('/excel?' + string, '_blank');


    },

    showMoreInfoReservation:function (element) {
        var el = $(element);

        var active = el.attr('data-active')
        if(active == 'off') {
            el.attr('data-active', 'on');
            el.closest('tr').next().show();
            el.text('Закрыть');
        }
        else {
            el.attr('data-active', 'off');
            el.closest('tr').next().hide();
            el.text('Подробнее');
        }
    },
    
    showRightSidebar: function (element) {
        var thatElement = $(element);
        var power = thatElement.attr('data-power');
        if(power) {
            $('.menu-hover li a').removeClass('loop');
            $('.menu-hover li a span').animate({opacity: 1, left: '55px'});

            $('#sidebar').animate({width: '270px'}).addClass('overflow');
            thatElement.attr('data-power', 0);
        }

    },

    hideRightSidebar: function(element) {
        var thatElement = $(element);
        var power = thatElement.attr('data-power');
        if(power == 0) {
            $('.menu-hover li a').addClass('loop');
            $('.menu-hover li a span').animate({opacity:0, left: '-9999px'});
            $('#sidebar').animate({width: '60px'}).removeClass('overflow');
            thatElement.attr('data-power', 1);
        }
    },

    showPopupPluses:function(element) {
        var el = $(element);
        var list = el.siblings('ul').clone();
        list.removeClass('hide');
        var maskHeight = $(document).height();
        var maskWidth = $(window).width();
        var overlay = $('#overlay');
        var popup = $('.popup-pluses');
        popup.find('ul').remove();
        popup.append(list);
        overlay.css({width: maskWidth, 'height': maskHeight});
        overlay.fadeIn(400, function () {
            popup.fadeIn();
        });
    },

    showWishes:function(element) {
        var el = $(element);
        var wish = el.siblings('p').text();
        var popup = $('.popup-wishes');
        popup.find('p').text(wish);
        var maskHeight = $(document).height();
        var maskWidth = $(window).width();
        var overlay = $('#overlay');
        overlay.css({'width':maskWidth,'height':maskHeight});
        overlay.fadeIn(400, function() {
            popup.fadeIn();
        });

    },

    closePopup:function(element) {
        var el = $(element);
        var popup = el.closest('div');
        var overlay = $('#overlay');
        popup.fadeOut(400, function () {
           overlay.fadeOut();
        });
    },
    
    addBedInTheRoom: function() {
        var btnDelete = false;
        var standartBed = $('.standart-field-bed').clone(),
            tableWithBed = $('.wrapper-beds table'),
            showBed = standartBed.removeClass('standart-field-bed'),
            wrapperBtnDelete = showBed.find('.wrapper-btn-delete-for-room');
        showBed.find('.js-bed-type').attr('name', 'bedTypeId[]');
        showBed.find('.js-count-bed').attr('name', 'countBed[]');
        btnDelete = $('.btn-delete-style-pattern').clone();
        btnDelete.removeClass('btn-delete-style-pattern');
        tableWithBed.append(showBed);
        wrapperBtnDelete.append(btnDelete);
        btnDelete.show();
        showBed.show();
    },
    
    removeThisBed: function(element) {
        var thatElement = $(element);
        thatElement.closest('tr').remove();
    },
    
    changeTypeCancel: function(element) {
        var thatElement = $(element);
        if(thatElement.val() == 3) {
            $('.wrapper-annulation').show();
        }
        else {
            $('.wrapper-annulation').hide();
        }
    },
    
    addObjectAbout: function() {
        var currentTable = $('.object-where-about'),
        standartTr = currentTable.find('.standart-string-add-object').clone(),
        newTr = standartTr.removeClass('standart-string-add-object');    
        currentTable.append(newTr);
    },
    
    deleteAboutObject: function(element) {
        var thatElement = $(element);
        thatElement.closest('tr').remove();
    },
    
    clickByFieldFile:function(element) {
        $('input[name="'+ element +'"]')[0].click();
    },
    
    onchangeImgObject:function(element) {
        var input = $(element)[0];
				if (input.files && input.files[0]) {
					if (input.files[0].type.match('image.*')) {
						var reader = new FileReader();
						reader.onload = function(e) {
							$('#img-preview').attr('src', e.target.result);
//                            console.log(e.target.result);
						}
						reader.readAsDataURL(input.files[0]);
					} else {
						console.log('ошибка, не изображение');
					}
				} else {
					console.log('хьюстон у нас проблема');
				}
    },
    
    files: false,
    
    editGalleryImgObject: function(element) {
        this.saveGalleryImgObject(element, 'images-objects');    
    },
    
    editGalleryImgRoom: function(element) {
      this.saveGalleryImgObject(element, 'images-rooms');  
    },
    
    saveGalleryImgObject: function(element, dir) {
        var that = this;
        
        var wrapperImages = $('#upload_image');
        var blockImg = $('.wrapper-for-img.display-none');
        var images = $(element)[0];
        that.files = images.files;
        var data = new FormData();
        $.each(that.files, function(key, value) {
            data.append(key, value);
        }); 
        var messageErrorBlock = $('.message-errors-upload-image'); 
        data.append('category', dir);
        $.ajax({
            type: 'POST',
            url: '/upload-images',
            data:data,
            cache:false,
            dataType:'json',
            processData:false,
            contentType:false,
            beforeSend: function() {
                $('.gallery_images').text('');
                $('.gallery_images').append('<i class="fa fa-spinner fa-spin"></i>');    
            },
            success: function(respond, textStatus, jqXHR) { 
                $('.gallery_images').text('');
                $('.gallery_images').append('Загрузить фотографии'); 
                wrapperImages.show();
                if(respond.errors.length > 0) {
                    $('#upload_image').removeClass('display-none').show();
                    messageErrorBlock.find('b').text(respond.errors);
                    messageErrorBlock.animate({opacity: 1, top: '7%'}, 500).delay(5000).animate({opacity:0, top: '-200px'}, 300);
                }
                
                    var files_path = respond.files;
                    $.each(files_path, function(key, val) {
                        var cloneBlockImg = blockImg.clone().removeClass('display-none');
                        cloneBlockImg.attr('style', 'background-image: url(..' + val + ');');
                        cloneBlockImg.find('input[name="imgHidden"]').val(val);
                        wrapperImages.append(cloneBlockImg);
                    });    
            }
        });
        
    },
    
    changeDateInfo: function(element, date) {
        var thatElement = $(element);
        $('.link-tab ul li a').removeClass('active');
        thatElement.addClass('active');
        $('.js-tabs').empty();
        $.ajax({
            type: 'POST',
            url: '/',
            data: {'_token': $('meta[name="csrf-token"]').attr('content'), date:date},
            beforeSend: function() {
                $('.js-tabs').append('<i class="fa fa-spinner fa-spin spinner-home" aria-hidden="true"></i>');    
            },
            success:function(result) {
                $('.js-tabs').html(result['html']);
            }
        });
    },
    
    
    removeImgObject: function(element) {
        var thatElement = $(element);
        var imgSrc = thatElement.closest('.wrapper-for-img').find('input[name="imgHidden"]').val();
        
        $.ajax({
            type: 'GET',
            url: '/delete-image-object',
            data: {img:imgSrc},
            success: function(result) {
                if(result) {
                    thatElement.closest('.wrapper-for-img').remove();  
                    var countImg = $('#upload_image').find('.wrapper-for-img');
                    if(!countImg.length) {
                        $('#upload_image').addClass('display-none').hide();
                    }
                }
            }
        });
    },
    
    changeTouristTax: function(element) {
        var thatElement = $(element);
        thatElementValue = thatElement.val();
        console.log(thatElementValue);
        if(thatElementValue == 'IncludedPrice') {
            $('select[name="touristTaxValue"]').attr('disabled', 'disabled');
        }
        else {
            $('select[name="touristTaxValue"]').attr('disabled', false);
        }
    },
    
    sortReview: function(element) {
        var thatElement = $(element);
        var thatElementValue = thatElement.val();
        var page = this.getUrlVar(['page']);
        if(!page) {
            page = 1;
        }
        else {
            page = page['page'];
        }
        document.location.href = '/reviews?page='+ page +'&sort=' + thatElementValue;
    },
    
    getUrlVar: function(){
    var urlVar = window.location.search; // получаем параметры из урла
    var arrayVar = []; // массив для хранения переменных
    var valueAndKey = []; // массив для временного хранения значения и имени переменной
    var resultArray = []; // массив для хранения переменных
    arrayVar = (urlVar.substr(1)).split('&'); // разбираем урл на параметры
    if(arrayVar[0]=="") return false; // если нет переменных в урле
    for (i = 0; i < arrayVar.length; i ++) { // перебираем все переменные из урла
        valueAndKey = arrayVar[i].split('='); // пишем в массив имя переменной и ее значение
        resultArray[valueAndKey[0]] = valueAndKey[1]; // пишем в итоговый массив имя переменной и ее значение
    }
    return resultArray; // возвращаем результат
},
    
    prepareAboutDeleteElement: function(element, e) {
        e.preventDefault();
        var thatElement = $(element);
        $('.prepareBlockDelete').hide();
        thatElement.closest('td').find('.prepareBlockDelete').show();
    },
    
    cancelDeleteElement: function(element) {
        var thatElement = $(element);
        thatElement.closest('td').find('.prepareBlockDelete').hide();
    },
    
    clickAboutCheckbox: function(element) {
        var thatElement = $(element);
        thatElement.closest('tr').find('input[type="checkbox"]')[0].click();
    },
    
    selectRegionAndLoadCity: function(element) {
        var thatElement = $(element);
        var regionId = thatElement.val();
        var wrapperElementSelectCity = $('select[name="cityId"]');
        wrapperElementSelectCity.empty();
        wrapperElementSelectCity.append('<option value="">Выберите город</option>');
        $.ajax({
            type: 'POST',
            url: '/load-city-region',
            data: {regionId:regionId, '_token': $('meta[name="csrf-token"]').attr('content')},
            success:function(result) {
                $.each(result, function(key, value){
                    wrapperElementSelectCity.append('<option value="' + value.id + '">' + value.name + '</option>');
                });
                wrapperElementSelectCity.attr('disabled', false);
            }
        });
    },
    
    
    checkingData: function(e) {
        $('.alert-danger-js ul').empty();
        var errors = false;
        var errorsArray = [];
               
        var name = $('input[name="name"]');
        if(!name.val() && name.val().length < 2) {
            errors = true;
            errorsArray.push("Название объекта является обязательным полем!");
            name.css({border: '1px solid red'});
        }
        
        var phone = $('input[name="phone"]');
        if(!phone.val()) {
            errors = true;
            errorsArray.push("Телефон является обязательным полем!");
            phone.css({border: '1px solid red'});
        }
        
        var objectCategoryId = $('select[name="objectCategoryId"]');

        if(objectCategoryId.val() == 0) {
            errors = true;
            errorsArray.push("Укажите тип объекта! Так Вас будет проще найти при бронировании.");
            objectCategoryId.css({border: '1px solid red'});
        }
        
        var countryId = $('select[name="countryId"]');
        if(countryId.val() == 0) {
            errors = true;
            errorsArray.push("Выберите страну где Вы находитесь!");
            countryId.css({border: '1px solid red'});
        }
        
        var regionId = $('select[name="regionId"]');
        if(regionId.val() == 0) {
            errors = true;
            errorsArray.push("Укажите конкретную область для удобного поиска!");
            regionId.css({border: '1px solid red'});
        }
        
        var indexMail = $('input[name="indexMail"]');
        if(indexMail.val().length != 5) {
            errors = true;
            errorsArray.push("Укажите корректный почтовый индекс!");
            indexMail.css({border: '1px solid red'});
        }
        
        var cityId = $('select[name="cityId"]');
        if(cityId.val() == 0) {
            errors = true;
            errorsArray.push("Выберите город в котором Вы находитесь!");
            cityId.css({border: '1px solid red'});
        }
        
        var street = $('input[name="street"]');
        if(street.val().length < 5) {
            errors = true;
            errorsArray.push("Укажите корректный адрес вашего местоположения!");
            street.css({border: '1px solid red'});
        }
        
        var fax = $('input[name="fax"]');
        if(!fax.val()) {
            errors = true;
            errorsArray.push("Укажите номер Вашего факса!");
            fax.css({border: '1px solid red'});
        }
        
        var numberHouse = $('input[name="numberHouse"]');
        if(!numberHouse.val()) {
            errors = true;
            errorsArray.push("Укажите номер дома! Поле является обязательным");
            numberHouse.css({border: '1px solid red'});
        }
        
        var contactName = $('input[name="contactName"]');
        if(!contactName.val() && contactName.val().length < 3) {
            errors = true;
            errorsArray.push("Укажите контактное лицо! Поле является обязательным.");
            contactName.css({border: '1px solid red'});
        }
        
        var email = $('input[name="email"]');
        var patternEmail = /.+@.+\..+/i;
        if(!email.val() && !patternEmail.test(email.val())) {
            errors = true;
            errorsArray.push("Неккоректный электронный адрес.");
            email.css({border: '1px solid red'});
        }
        
        var reservationPeopleFio = $('input[name="reservationPeopleFio"]');
        if(!reservationPeopleFio.val() && reservationPeopleFio.val().length < 3) {
            errors = true;
            errorsArray.push("Укажите контактное лицо отдела бронирования! Поле является обязательным.");
            reservationPeopleFio.css({border: '1px solid red'});
        }
        
        var reservationPhone = $('input[name="reservationPhone"]');
        if(!phone.val()) {
            errors = true;
            errorsArray.push("Телефон отдела бронирования является обязательным полем!");
            reservationPhone.css({border: '1px solid red'});
        }
        
        var bookkeepingPeopleFio = $('input[name="bookkeepingPeopleFio"]');
        if(!bookkeepingPeopleFio.val() && bookkeepingPeopleFio.val().length < 3) {
            errors = true;
            errorsArray.push("Укажите контактное лицо бухгалтерии! Поле является обязательным.");
            bookkeepingPeopleFio.css({border: '1px solid red'});
        }
        
        var bookkeepingPhone = $('input[name="bookkeepingPhone"]');
        if(!bookkeepingPhone.val()) {
            errors = true;
            errorsArray.push("Телефон бухгалтерии является обязательным полем!");
            bookkeepingPhone.css({border: '1px solid red'});
        }
        
        var legalName = $('input[name="legalName"]');
        if(!legalName.val() || legalName.val().length <= 2) {
            errors = true;
            errorsArray.push("Юридическое название отеля является обязательным полем!");
            legalName.css({border: '1px solid red'});
        }
        
        var typeOwn1 = $('select[name="typeOwn1"]');
        if(!typeOwn1.val()) {
            errors = true;
            errorsArray.push("Форма собственности 1 является обязательным полем!");
            typeOwn1.css({border: '1px solid red'});
        }
        
        var legalAddress = $('textarea[name="legalAddress"]');
        if(!legalAddress.val() && legalAddress.val().length < 3) {
            errors = true;
            errorsArray.push("Юридический адрес является обязательным полем!");
            typeOwn1.css({border: '1px solid red'});
        }
        
        
        
        var egrpou = $('input[name="egrpou"]');
        
        if(egrpou.val() != '' && (parseInt(egrpou.val()) && egrpou.val().length != 8)) {
            errors = true;
            errorsArray.push('ЕГПОУ должен состоять только из цифр, а так же его длина должно состоять из 8 цифр!');
            egrpou.css({border: '1px solid red'});
        }
        
        var identicationCodeLegal = $('input[name="identicationCodeLegal"]');
        
        if(identicationCodeLegal.val().length > 0 && identicationCodeLegal.val().length != 12) {
            errors = true;
            errorsArray.push('Идентификационный номер должен состоять только из цифр, а так же он должен состоять из 12 цифр.');
            identicationCodeLegal.css({border: '1px solid red'});
        }
        
        if(errors) {
            $('html,body').scrollTop(0);
            e.preventDefault();
            $.each(errorsArray, function(indx, element) {

                $('.alert-danger-js ul').append('<li>'+ element +'</li>');
            });
            
            $('.alert-danger-js').show();
            
            
        }
        
    },
    
    checkingDataInfoObject: function(e) {
        
        
        $('.alert-danger-js ul').empty();
        var errors = false;
        
        var errorsArray = [];
        
        var infoAboutObject = $('textarea[name="infoAboutObject"]');
        if(infoAboutObject.val().length <= 100) {
            errors = true;
            errorsArray.push('Информация об объекте должна быть минимум из 100 символов!');
            infoAboutObject.css({border: '1px solid red'});
        }
        
        var checkIn = $('input[name="checkIn"]');
        if(!checkIn.val) {
            errors = true;
            errorsArray.push('Поле "Регистрация заезда" является обязательным');
            checkIn.css({border: '1px solid red'});
        }
        
        var checkOut = $('input[name="checkOut"]');
        if(!checkOut.val) {
            errors = true;
            errorsArray.push('Поле "Регистрация отьезда" является обязательным');
            checkOut.css({border: '1px solid red'});
        }
        
        var countRoom = $('input[name="countRooms"]');
        
        if(countRoom.val() == 0) {
            errors = true;
            errorsArray.push('Укажите корректное количество номеров для продажи!');
            countRoom.css({border: '1px solid red'});
            
        }
        
        var locationObject = $('textarea[name="locationObject"]');
        var locationObjectLength = locationObject.val();
        
        if(!locationObjectLength && locationObjectLength.length <= 100) {
            errors = true;
            errorsArray.push('Поле Месторасположение обязательное к заполнению и должно состоять минимум из 100 символов!');
            locationObject.css({border: '1px solid red'});
        }
        
        var address = $('input[name="address"]');
        var addressLength = address.val();
        if(addressLength.length < 4) {
            errors = true;
            errorsArray.push('Укажите адресс где Вы находитесь!');
            address.css({border: '1px solid red'});
        }
        
        var countPluses = 0;
        $.each($('input[name="pluses[]"]'), function(indx, element) {
            var thatElement = $(element);
            if(thatElement.prop('checked')) {
                countPluses++;
            }
        });
        
        
        var images = $('#upload_image').find('.wrapper-for-img');
        
        var imgPreview = $('#img-preview').attr('src');
        if(!imgPreview) {
            errors = true;
            errorsArray.push('Выберите основное изображения отеля. Она будет основной фотографией Вашего обьекта.');
        }
        
        if(images.length == 0) {
            errors = true;
            errorsArray.push('Нет изображений номера. Пожалуйста добавьте несколько изображений.');
        }
        
        if(countPluses == 0) {
            errors = true;
            errorsArray.push('Укажите несколько услуг в отеле!');
        }
        
        var countFoodTypes = 0;
        $.each($('input[name="foodTypes[]"]'), function(indx, element) {
            var thatElement = $(element);
            if(thatElement.prop('checked')) {
                countFoodTypes++;
            }
        });
        
        if(countFoodTypes == 0) {
            errors = true;
            errorsArray.push('Укажите тип питания в отеле!');
        }
        
        if(errors) {
            e.preventDefault();
            $('html,body').scrollTop(0);
            $.each(errorsArray, function(indx, element) {
                $('.alert-danger-js ul').append('<li>'+ element +'</li>');
            });
            $('.alert-danger-js').show();
            
        }
        
    },
    
    checkingDataRoom: function(e) {
        $('.alert-danger-js ul').empty();
        var errors = false;
        var errorsArray = [];
        
        var name = $('input[name="name"]');
        if(!name.val() && name.val().length < 2) {
            errors = true;
            errorsArray.push('Название объекта является обязательным и должно быть минимум из 2 символов');
            name.css({border: '1px solid red'});
        }
        
        var price = $('input[name="price"]');
        if(!price.val() && isNaN(price.val())) {
            errors = true;
            errorsArray.push('Цена за номер является обязательным полем!');
            price.css({border: '1px solid red'});
        }
        
        var countPluses = 0;
        
        $.each($('input[name="pluse[]"]'), function(indx, element){
            var thatElement = $(element);
            if(thatElement.prop('checked')) {
                countPluses++;
            }
        });
        
        var infoAboutRoom = $('textarea[name="infoAboutRoom"]');
        
        if(infoAboutRoom.val().length <= 30) {
            errors = true;
            errorsArray.push('Описание номера должно состоять минимум из 30 символов и является обязательным полем.');
            infoAboutRoom.css({border: '1px solid red'});
        }
        
        var countRoom = $('input[name="countRoom"]');
        
        if(!countRoom.val() && countRoom.val() == 0) {
            errors = true;
            errorsArray.push('Неккоректное количество номеров. Укажите количество номеров или мест на продажу!');
            countRoom.css({border: '1px solid red'});
        }
        
        var amountPerson = $('input[name="amountPerson"]');
        
        if(!amountPerson.val() && amountPerson.val() == 0) {
            errors = true;
            errorsArray.push('Неккоректное количество мест в номере. Укажите сколько людей могут проживать в данном номере!');
            amountPerson.css({border: '1px solid red'});
        }
        
        if(countPluses == 0) {
            errors = true;
            errorsArray.push('Укажите какие есть услуги в данном номере!');
        }
        
        var images = $('#upload_image').find('.wrapper-for-img');
        
        if(images.length == 0) {
            errors = true;
            errorsArray.push('Нет изображений номера. Пожалуйста добавьте несколько изображений.');
        }
        
        if(errors) {
            $('html,body').scrollTop(0);
            e.preventDefault();
            $.each(errorsArray, function(indx, element) {
                $('.alert-danger-js ul').append('<li>'+ element +'</li>');
            });
            $('.alert-danger-js').show();
            
            
        }
        
    },
    
    timerId: false,

    getObjectsName: function(e, element) {
        if(e.keyCode < 32 && e.keyCode != 8 || element.value.length < 2) return null;
        $('.list-objects-ajax').hide();
        clearTimeout(this.timerId);
        this.timerId = setTimeout(function(){
            var thatElement = $(element);
            var nameObject = thatElement.val();

            var wrapperListObjects = $('.list-objects-ajax ul');
            $.ajax({
                type: 'POST',
                url: '/administrator/get-objects',
                data: {'_token': $('meta[name="csrf-token"]').attr('content'), object:nameObject},
                success: function(result) {

                    wrapperListObjects.html(result['html']);
                    $('.list-objects-ajax').show();
                }
            });
        }, 700);
        
    },
    
    selectObject: function(element, id) {
        var thatElement = $(element);
        var name = thatElement.text();
        var prevObjectId = $('input[name="objectId"]').val();
        
        if(prevObjectId && id != prevObjectId) {
            var link = this.deleteReservation(false);
        }
        
        $('input[name="objectName"]').val(name);
        $('input[name="objectId"]').val(id);
        
        $('.list-objects-ajax').hide();
        var roomSelect = $('#roomId');
        var reservationId = $('input[name="reservationId"]');
        var btnAddRoom = $('.btn-add-room');
        
        $.ajax({
            
            type: 'POST',
            url: '/administrator/create-reservation-by-object',
            data:{'_token': $('meta[name="csrf-token"]').attr('content'), objectId:id, prevObjectId:prevObjectId , dateFrom: $('input[name="dateFrom"]').val(), dateTo: $('input[name="dateTo"]').val(), reservationId:reservationId.val()},
            success:function(result) {

                if(result['reservationId'] > 0) {
                    reservationId.val(result['reservationId']);
                    $('.message-reservation').animate({opacity:1, top:'10%'}, 500).delay(3000).fadeOut();    
                }
                
                btnAddRoom.show();
                
            }
            
        });
    },
    
    getListRoomsByObjectId: function() {
        
        var objectId = $('input[name="objectId"]').val();
        var dateFrom = $('input[name="dateFrom"]').val();
        var dateTo = $('input[name="dateTo"]').val();

        var roomSelect = $('#roomId');
        var that = this;

        roomSelect.empty().append('<option>Выберите комнату</option>');
        $.ajax({
            type: 'POST',
            url: '/administrator/get-list-rooms-by-object',
            data: {'_token': $('meta[name="csrf-token"]').attr('content'), objectId:objectId, dateFrom:dateFrom, dateTo:dateTo},
            success: function(result) {
 
                if(result['listRooms'].length == 0) {
                    $('.message-reservation-error-count-people').animate({opacity:1, top:'25%'}).delay(3000).animate({opacity:0, top: '-10%'});
                }
                else {
                    $.each(result['listRooms'], function(indx, room) {
                        roomSelect.append('<option value="'+ room.id +'">'+ room.name +'</option>');
                    });
                    that.addRoom(); //вызов клонирования комнаты для отображения интерфейса    
                }
                
            }
        });
        
        
    },
    
    addRoom: function() {
        var wrapperPatternRoom = $('.wrapper-room-field').clone();
        wrapperPatternRoom.removeClass('wrapper-room-field').addClass('display-block wrapper-room-field-js');
        wrapperPatternRoom.find('.peopleLive input').attr('name', 'peopleFio');
        wrapperPatternRoom.find('.js-amountPerson input[type="text"]').attr('name', 'countAmountPerson');
        wrapperPatternRoom.find('#roomId').attr('name', 'roomId').removeAttr('id').addClass('js-type-room');
        wrapperPatternRoom.find('#prepaymentSelect').attr('name', 'prepayment');
        wrapperPatternRoom.find('.text-room').attr('name', 'info');
        $('.list-rooms').append(wrapperPatternRoom);
    },
    
    removeThatRoom: function(element) {
        
        if(element) {
            var that = this;
            var thatElement = $(element);
            var reservationId = $('input[name="reservationId"]').val();
            var roomId = thatElement.closest('.wrapper-room-field-js').find('input[name="selectedRoomId"]').val();
            
            $.ajax({
                type: 'POST',
                url: '/administrator/delete-room-from-reservation',
                data: {'_token': $('meta[name="csrf-token"]').attr('content'), roomId: roomId, reservationId:reservationId},
                success:function(result) {
                    if(!result['success']) {
                        thatElement.closest('.wrapper-room-field-js').remove();    
                        that.calculatePrice();
                    }
                    else {
                        $('.message-reservation-error').animate({opacity:1, top:'10%'}, 500).delay(3000).fadeOut();
                    }

                }
            });
        }
        
        
    },
    
    
    changePriceForRoom: function(element) {
        var that = this;
        var thatElement = $(element);
        var roomId = thatElement.val();
        var dateFrom = $('input[name="dateFrom"]').val();
        var dateTo = $('input[name="dateTo"]').val();
        var reservationId = $('input[name="reservationId"]').val();
        var selectedRoomId = thatElement.closest('.wrapper-room-field-js').find('input[name="selectedRoomId"]'); 
        $.ajax({
            type: 'POST',
            url: '/administrator/get-info-room-price',
            data: {'_token': $('meta[name="csrf-token"]').attr('content'), roomId: roomId, dateFrom:dateFrom, dateTo:dateTo, reservationId: reservationId,selectedRoomId:selectedRoomId.val()},
            success:function(result) {
                thatElement.closest('.wrapper-room-field-js').find('input[name="hiddenPrice"]').val(result['fullPriceDiscount']);
                thatElement.closest('.wrapper-room-field-js').find('input[name="priceForDay"]').val(result['priceForDay']);
                thatElement.closest('.wrapper-room-field-js').find('input[name="maxAmountPerson"]').val(result['amountPerson']);
                thatElement.closest('.wrapper-room-field-js').find('.price-for-discount input').val(result['fullPriceDiscount'] + ' UAH');
                thatElement.closest('.wrapper-room-field-js').find('.price input').val(result['fullPrice'] + ' UAH');
                thatElement.closest('.wrapper-room-field-js').find('.val-all-price').text('за ' + result['countDay'] + ' дн. ' +  result['fullPriceDiscount'] + ' UAH');
                that.calculatePrice(result['countDay']);
                selectedRoomId.val(result['selectedRoomId']);
            }
        });    
    },
    
    
    calculatePrice: function(countDay) {
        var allpriceReservation = 0;
        var priceAllRoom = $('input[name="hiddenPrice"]');
        var that = this;
        if(priceAllRoom.length > 0) {
            $.each(priceAllRoom, function(indx, element){
            if($(element).val()) {
                allpriceReservation += +$(element).val(); 
            }
            
        });
    
        $('.val-full-price-reservation').text('Цена за проживания - ' + allpriceReservation + ' UAH');
        }

        this.reloadPrepaymentReservation();
    },

    reloadPrepaymentReservation: function() {
        var listPrepayments = $('select[name="prepayment"]');

        var prepayment = 0;
        $.each(listPrepayments, function(indx, element) {
            var thatElement = $(element);
            var value = thatElement.val();
            switch(+value) {
                case 2:
                    prepayment += +thatElement.closest('.wrapper-room-field-js').find('input[name="priceForDay"]').val();
                    break;
                case 3:
                    prepayment += +thatElement.closest('.wrapper-room-field-js').find('input[name="hiddenPrice"]').val();
                    break;
                default:
                    prepayment += 0;
                    break;
            }
        });
        $('.prepayment-style').find('.prepayment').text(prepayment + ' UAH');
    },
    
    calculateDate:function(dateFrom, dateTo) {
        
        var date1 = this.splitDateArray(dateFrom);
        date1 = new Date(date1[0], parseInt(date1[1]) -1, date1[2]);
        
        var date2 = this.splitDateArray(dateTo);
        date2 = new Date(date2[0], parseInt(date2[1]) -1, date2[2]);
        var Days = Math.floor((date2.getTime() - date1.getTime())/(1000*60*60*24));
        return Days;
    },
    
    splitDateArray:function(date) {
        return date.split('-').reverse();    
    },
    
    validateInfoReservation: function(e,element) {
        e.preventDefault();
        var thatElement = $(element);
        
        var reservationInformation = {};
        
        var roomArray = [];
        
        reservationInformation.dateFrom = $('input[name="dateFrom"]').val();
        reservationInformation.dateTo = $('input[name="dateTo"]').val();
        reservationInformation.objectId = $('input[name="objectId"]').val()
        reservationInformation.reservationId = $('input[name="reservationId"]').val();
        reservationInformation.userId = $('input[name="userId"]').val();
        reservationInformation.telefon = $('input[name="telefon"]').val();
        reservationInformation.email = $('input[name="email"]').val();
        reservationInformation.lastName = $('input[name="lastName"]').val();
        reservationInformation.name = $('input[name="name"]').val();
        reservationInformation.middleName = $('input[name="middleName"]').val();  
        
        var wrapperRooms = $('.wrapper-room-field-js');
        
        $.each(wrapperRooms, function(indx, element) {
            var thatElement = $(element);
            var room = {};
            room.peopleFio = thatElement.find('input[name="peopleFio"]').val();
            room.roomId = thatElement.find('input[name="selectedRoomId"]').val();
            room.prePayment = thatElement.find('#prepaymentSelect').val();
            room.info = thatElement.find('textarea[name="info"]').val();
            room.amountPerson = thatElement.find('input[name="countAmountPerson"]').val();
            roomArray.push(room);
        });
        var reservationFullInformation = {};
        reservationFullInformation.reservationInformation = reservationInformation;
        reservationFullInformation.rooms = roomArray;
        
        $.ajax({
            type: 'POST',
            url: '/administrator/save-full-reservation',
            data: {'_token': $('meta[name="csrf-token"]').attr('content'), reservationInformation: reservationFullInformation},
            beforeSend:function() {
                thatElement.text('<i class="fa fa-spinner fa-spin" aria-hidden="true"></i>');
            },
            success:function(result) {
                console.log(result);
                if(result['success']) {
                    document.location.href = result['link'];
                }
                
            }
        });
        
        
        
    },
    
    
    getLoadClientByTelefon: function(e, element) {
        this.closePopupUser();
        var thatElement = $(element);
        if(e.keyCode < 32 && e.keyCode != 8 || element.value.length < 11) return null;
        clearTimeout(this.timerId);
        var wrapperUserPopup = $('.user-ajax ul');
        wrapperUserPopup.empty();
        var closePopupBtn = $('.style-for-close-popup-user');
        closePopupBtn.hide();
        
        $('.wrapper-telefon-field input[name="userId"]').val('');
        $('input[name="email"]').val('');
        $('input[name="lastName"]').val('');
        $('input[name="name"]').val('');
        $('input[name="middleName"]').val('');
        
        this.timerId = setTimeout(function(){
            $.ajax({
                type: 'POST',
                url: '/administrator/get-load-client-by-telefon',
                data:{'_token': $('meta[name="csrf-token"]').attr('content'), telefon: thatElement.val()},
                success: function(result) {
                    if(result) {

                        wrapperUserPopup.append('<li data-email="'+ result.email +'" data-userId="'+ result.id +'" data-name="'+ result.name +'" data-lastName="'+ result.lastName +'" data-middleName="'+ result.middleName +'"  onclick="MainFunction.selectUserWithTelefon(this);">'+ result.phone +'</li>');
                        $('.user-ajax').show();
                    }
                    else {
                        closePopupBtn.show();
                        wrapperUserPopup.append('<li>Ничего не найдено.<br></li>');
                        $('.user-ajax').show();

                    }
                }
            });
        }, 600);
    },
    
    selectUserWithTelefon:function(element) {
        var thatElement = $(element),
            emailUser = thatElement.attr('data-email'),
            userId = thatElement.attr('data-userId'),
            nameUser = thatElement.attr('data-name'),
            lastNameUser = thatElement.attr('data-lastName'),
            middleNameUser = thatElement.attr('data-middleName'),
            phoneUser = thatElement.text();
        
        $('.wrapper-telefon-field input[name="telefon"]').val(phoneUser);
        $('.wrapper-telefon-field input[name="userId"]').val(userId);
        $('input[name="email"]').val(emailUser);
        $('input[name="lastName"]').val(lastNameUser);
        $('input[name="name"]').val(nameUser);
        $('input[name="middleName"]').val(middleNameUser);
        this.closePopupUser();
        
    },
    
    
    closePopupUser:function() {
        $('.user-ajax').hide();
    },
    
    deleteReservation:function(boolRedirect) {
        var that = this;
        var rooms = $('.wrapper-room-field-js');
        var countRooms = rooms.length;
        var counter = 0;
        var reservationId = $('body').find('input[name="reservationId"]').val();
        $.each(rooms, function(indx, element) {
            var thatElement = $(element);
            var currentRoom = thatElement.find('input[name="selectedRoomId"]');
            that.removeThatRoom(currentRoom);
            counter++;
            
            if(countRooms == counter) {
                if(boolRedirect) {
                    document.location.href = '/administrator/delete-reservation-id/' + reservationId;
                }
            }
        });

        
        
    },
    
    
//    removeReservation:function() {
//        var that = this;
//        var link;
//        var reservationId = $('body').find('input[name="reservationId"]').val();
//        if(reservationId) {
//            var link = $.ajax({
//                type: 'POST',
//                url: '/administrator/delete-reservation-id',
//                async:false,
//                data: {'_token': $('meta[name="csrf-token"]').attr('content'), reservationId: reservationId},
//                success:function(result) {
//
//                    if(result['success']) {
//                        link = result['link'];
//                    }
//                }
//            });
//        }
//
//        return link; //доделать тут
//    },
    

    showObjectForReservation:function(e, element) {
        e.preventDefault();
        var dateFrom = $('input[name="dateFrom"]').val();
        var dateTo = $('input[name="dateTo"]').val();
        var amountPerson = $('input[name="amountPerson"]').val();
        var priceFrom = $('input[name="priceFrom"]').val();
        var priceTo = $('input[name="priceTo"]').val();
        var searchId = $('.select-category-and-id').val();
        var category = $('input[name="category"]').val();
        var wrapper = $('.list-search-objects');
        var discount = $('input[name="isDiscount"]').prop('checked');
        if(discount) {
            discount = 1;
        }
        else {
            discount = 0;
        }
        var page = $('input[name="page"]').val();
        $('.alert-danger').addClass('hide');

        
        wrapper.empty();
        $.ajax({
            type: 'POST',
            url: '/administrator/search-object-by-filters',
            data: {'_token': $('meta[name="csrf-token"]').attr('content'), dateFrom:dateFrom, dateTo:dateTo, amountPerson:amountPerson, priceFrom: priceFrom, priceTo:priceTo, searchId: searchId, category:category, discount:discount, page:page},
            beforeSend:function() {
                $('.loader-inner').removeClass('hide');
            },
            success: function(result) {
                if(result) {
                    wrapper.html(result['html']);

                    if(result['countObjects'] >= 10) {
                        $('.btn-show-more-objects').removeClass('hide');
                    }
                    else {
                        $('.btn-show-more-objects').addClass('hide');
                    }

                    $('.loader-inner').addClass('hide');
                }
                else {
                    $('alert-danger').removeClass('hide');
                }
                
            }
            
        });
        
        
        
        
    },
    
    
    
    timersId: false,
    
    startFunctionChangeFieldSearch: function(element) {
        clearTimeout(this.timersId);
        $('.result-search').hide();
        this.timersId = setTimeout(function(){
            MainFunction.changeFieldSearch(element);
        }, 300);       
    },

    changeFieldSearch: function(element) {
        var el = $(element);
        var elementValue = el.val();
        var resultBlock = $('.result-search');
        var currentUrl = window.location.pathname;
        var categoryCities = $('.cities-search-result');
        var categoryRegions = $('.regions-search-result');
        var categoryObjects = $('.hotels-search-result');
        var showBlocks = 0;
        resultBlock.hide();
        categoryCities.hide();
        categoryRegions.hide();
        categoryObjects.hide();
        $('.tooltip-input').css({visibility:'hidden'});

        
        
    if(elementValue.length > 1) {
            $.ajax({
                type: "POST",
                url: '/administrator/search-object-for-reservation',
                data: {stringSearch: elementValue, '_token': $('meta[name="csrf-token"]').attr('content')},
                success: function (result) {
                    resultBlock.find('ul').empty();
                    if(MainFunction.countOfOject(result['city']) != 0) {
                        var cities = result['city'];
                        categoryCities.show();
                        for(var key in cities) {

                            categoryCities.next().append('<li onclick="MainFunction.addTextToField(this,\'city\', '+ cities[key].id +', '+ cities[key].countryId +');">'+ cities[key].name +'</li>');
                        }
                        showBlocks++;
                    }

                    if(MainFunction.countOfOject(result['region']) != 0) {
                        var regions = result['region'];
                        categoryRegions.show();
                        for(var key in regions) {
                                categoryRegions.next().append('<li onclick="MainFunction.addTextToField(this,\'region\', '+ regions[key].id+', '+ regions[key].countryId +');">'+ regions[key].name +'</li>');
                        }
                        showBlocks++;
                    }

                    if(MainFunction.countOfOject(result['object']) != 0) {
                        var objects = result['object'];
                        categoryObjects.show();
                        for(var key in objects) {
                            var arrayObjects = objects;
                            categoryObjects.next().append('<li onclick="MainFunction.addTextToField(this,\'object\', '+ objects[key].id +', '+ objects[key].countryId +');">'+ objects[key].name +'</li>')
                        }
                        showBlocks++;
                    }

                    if(showBlocks != 0) {
                        resultBlock.fadeIn(300);
                    }
                },
                dataType: "json"
            });


        }
        else {
            resultBlock.hide();
        }


    },
    
    countOfOject: function(obj) {
        var t = typeof(obj);
        var i=0;
        if (t!="object" || obj==null) return 0;
        for (x in obj) i++;
        return i;
    },
    
    addTextToField: function(element, category, id, countryId) {
        var thatElement = $(element);
        var searchField = $('.search-field');
        var resultSearchBlock = $('.result-search');
        var categoryField = $('.hide-field-category');
        var hideInputId = $('.select-category-and-id');
        var hideCountryId = $('.js-select-country-id');
        hideCountryId.val(countryId);
        categoryField.val(category);
        searchField.val(thatElement.text());
        hideInputId.attr('name', category +'Id').val(id);
        resultSearchBlock.fadeOut();

    },
    
    filtersReservationCalendar:function() {
        var status = $('select[name="status"]').val();
        var type = $('select[name="type"]').val();
        var prepayment = $('select[name="prepayment"]').val();
        var dateFrom = $('input[name="dateFrom"]').val();
        var dateTo = $('input[name="dateTo"]').val();
        
        if(status != '' && isNaN(status)) {
            console.log('я сюда зашло'); return false;
        } 
        if(dateFrom && !this.validateDateReservation(dateFrom)) {
            console.log(this.validateDateReservation(dateFrom)); return false;
        } 
        
        if(dateTo && !this.validateDateReservation(dateTo)) {
            return false;
        } 
        
        $('.messageErrorReservation').addClass('display-none');
        $('.list-reservation-content tr.reserv').remove();
        $.ajax({
            type: 'POST',
            url: '/get-reload-reservation-by-filters',
            data: {'_token': $('meta[name="csrf-token"]').attr('content'), status:status, type:type, prepayment:prepayment, dateFrom:dateFrom, dateTo:dateTo},
            beforeSend: function() {
                $('.wrapper-for-reloader').append('<i class="fa fa-spinner fa-spin" aria-hidden="true"></i>').show();
            },
            success:function(result) {
                $('.wrapper-for-reloader').hide();
                if(result['success']) {
                    $('.wrap-reservation').empty();
                    $('.wrap-reservation').append(result['html']);
                }
                else {
                    $('.messageErrorReservation').removeClass('display-none');
                }
                
            }
        });
        
        
    },
    
    validateDateReservation:function (value){
        var arrD = value.split("-");
        arrD[1] -= 1;
        
        var d = new Date(arrD[2], arrD[1], arrD[0]);
        
        if ((d.getFullYear() == arrD[2]) && (d.getMonth() == arrD[1]) && (d.getDate() == arrD[0])) {
            
            return true;
        }
        
        else {
            
        return false;
        }
    },
    
    selectDateForReservation:function(element) {
        var thatElement = $(element);
        var date = thatElement.attr('date-value');
        if(date) {
            $('input[name="dateFrom"]').val(date);
            this.filtersReservationCalendar();
        }
        else {
            $('input[name="dateFrom"]').val('');
            this.filtersReservationCalendar();
        }
        
    },
    
    
    showChangeStatusReservation:function(element) {
        var thatElement = $(element);
        thatElement.closest('td').find('.js-popup-past-reservation').animate({opacity:1});
    },
    
    changeStatusReservation: function(reservationId, statusValue , element) {
        
        var thatElement = $(element);
        
        $.ajax({
            type: 'POST',
            url: '/administrator/change-status-reservation-past',
            data: {'_token': $('meta[name="csrf-token"]').attr('content'), reservationId:reservationId, isPast:statusValue},
            success:function(result) {
                thatElement.closest('tr').remove();
                $('.js-message-success').find('.alert-success').text('Статус брони успешно изменен.');
                $('.js-message-success').animate({opacity:1, top: '25%'}, 400).delay(3000).animate({opacity:0, top: '-10%'});
            }
        });
        
    },
    
    recountPriceForRooms: function() {
        
    },
    
    checkedCountPerson:function(element) {
        $('.info-count-person-room').text('');
        var thatElement = $(element);
        var val = thatElement.val();

        var maxCountPerson = thatElement.closest('.js-amountPerson').find('input[name="maxAmountPerson"]').val();

        if(val > maxCountPerson) {
            thatElement.css({border: '1px solid red'});
            $('.info-count-person-room').text('Макс. вместимость '+ maxCountPerson +' чел.');
        }
        else {
           thatElement.css({border: '1px solid #ccc'}); 
        }
    }, 
    
    showPopupmessageForError: function(element) {
        var thatElement = $(element);
        var id = thatElement.closest('tr').find('span[data-number-problem]').attr('data-number-problem');
        $('input[name="problemId"]').val(id);
        var messagePopup = $('.wrapper-message-about-problem');
        var maskHeight = $(document).height();
		var maskWidth = $(window).width();
        var overlay = $('#overlay');
        overlay.css({'width':maskWidth,'height':maskHeight});
        overlay.fadeIn(400, function() {
            messagePopup.fadeIn();    
        });
        
    },
    
    countLetters:function(element, classElement) {
        var thatElement = $(element);
        var countLetterElement = thatElement.val().length;
        var elementForText = $('.' + classElement);
        elementForText.text(countLetterElement);
    },
    
    closePopupMessageForError:function() {
        $('.wrapper-message-about-problem').fadeOut(400, function() {
            $('#overlay').fadeOut();
        });    
    },
    
    saveError: function(e) {
        e.preventDefault();
        var errorMessage = $('textarea[name="messageError"]');
        var problemId = $('input[name="problemId"]');
        var wrapperErrors = $('.wrapper-for-errors');
        var countError = $('input[name="countErrors"]');
        if(errorMessage && problemId) {
            wrapperErrors.append('<span data-id="'+ problemId.val() +'">'+ errorMessage.val() +'</span>');
            this.closePopupMessageForError();

            var wrapperProblem = $('body').find('[data-number-problem="'+ problemId.val() +'"]').closest('tr');
            wrapperProblem.find('.btn-select-admin').hide();
            wrapperProblem.find('.btn-reload-status').show();
            wrapperProblem.find('.status-select zz').text('Не верно');
            errorMessage.val('');
            problemId.val('');
            countError.val(parseInt(countError.val()) + 1);
            
        }
    },
    
    successStatus: function(element) {
        var thatElement = $(element);
        var wrapper = thatElement.closest('tr');
        var id = wrapper.find('span[data-number-problem]').attr('data-number-problem');
        wrapper.find('.btn-select-admin').hide();
        wrapper.find('.btn-reload-status').show();
        wrapper.find('zz').text('Верно');
    },
    
    changeStatusError:function(element) {
        var thatElement = $(element);
        var wrapper = thatElement.closest('tr');
        var id = wrapper.find('span[data-number-problem]').attr('data-number-problem');
        var wrapperErrors = $('.wrapper-for-errors');
        wrapperErrors.find('span[data-id="'+ id +'"]').remove();
        wrapper.find('.btn-reload-status').hide();
        wrapper.find('.btn-select-admin').show();
        wrapper.find('zz').text('');
        var countError = $('input[name="countErrors"]');
        countError.val(parseInt(countError.val()) - 1);
    },

    saveErrorsForObject:function() {
        var errors = $('.wrapper-for-errors span');
        var objectId = $('input[name="objectId"]').val();
        var objects = [];
        $.each(errors, function (indx, element) {
            var el = $(element);
            var obj = {};
            var text = el.text();
            var errorId = el.attr('data-id');
            obj.id = errorId;
            obj.text = text;
            objects.push(obj);
        });

        $.ajax({
            type: 'post',
            url: '/administrator/save-object-errors',
            data: {'_token': $('meta[name="csrf-token"]').attr('content'), errors:objects, objectId:objectId},
            success:function (result) {
                window.location.href = '/administrator/check-object';
            }
        });

    },

    startScriptErrors:function () {
        var errors = $('.wrapper-for-errors span');
        $.each(errors, function (indx, element) {
            var el = $(element);
            var errorId = el.attr('data-id');
            var wrapperProblem = $('body').find('[data-number-problem="'+ errorId +'"]').closest('tr');
            wrapperProblem.find('.btn-select-admin').hide();
            wrapperProblem.find('.btn-reload-status').show();
            wrapperProblem.find('.status-select zz').text('Не верно');
        });
    },
    
    selectCheckboxPluse:function(element) {
        var thatElement = $(element);
        var checkbox = thatElement.closest('tr').find('input[type="checkbox"]');
        console.log(checkbox.attr('checked'));
        if(checkbox.prop('checked')) {
            checkbox.prop('checked', false);
        }
        else{
            checkbox.prop('checked', true);
        }
    },
    
    changeStatusReservationInAll:function(element) {
        var thatElement = $(element);
        $('.block-for-change-status').show();
    }
    
    
    
}

$(document).ready(function() {
    $(document).mouseup(function (e) {
    var container = $("#sidebar");
    // if (container.has(e.target).length === 0){
    //     if($('.logo-sidebar i').attr('data-power') != 0) {
    //         $('.menu-hover li a').addClass('loop');
    //         $('.menu-hover li a span').animate({opacity:0, left: '-9999px'});
    //         $('#sidebar').animate({width: '60px'});
    //         $('.logo-sidebar i').attr('data-power', 0);
    //     }
    //
    // }
       
    var containerStatusReservation = $('.js-popup-past-reservation');    
        
    if(containerStatusReservation.has(e.target).length === 0) {
        containerStatusReservation.animate({opacity:0}, 50);
    }   
        
    var changeStatusReservationBlock = $('.block-for-change-status');    
        
    if(changeStatusReservationBlock.has(e.target).length === 0) {
        changeStatusReservationBlock.hide();
    }      
        
});
    
    $('.info-object input, .info-object textarea, #addRoom input, #addRoom textarea, #generalInformation input, #generalInformation textarea, #generalInformation select').focus(function(){
        var thatElement = $(this);
        thatElement.css({border: '1px solid #ccc'});
    });
    
    
    
});
