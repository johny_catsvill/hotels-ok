var Calendar =
{

    patternInt: /^[0-9]+$/u,
    patternFloat: /[^\d\.]/g,
    
    showPopupCalendar: function(element) {
        var date = new Date(),
            day = $(element).find('.number-day-calendar span').text(),
            month = $('body').find('.list-month ul li a.active').attr('data-month'),
            roomId = $('#roomId').val(),
            year = $('#selectedYear').val(),
            selectedDate = this.updateDay(day) + '-' + month + '-' + year,
            selectedDateAfter = new Date(year + ',' + month + ',' + day);
        
        if(!this.patternInt.test(roomId) || new Date(selectedDateAfter) == 'Invalid Date') {
                return false;    
        };
        
        selectedDateAfter.setDate(selectedDateAfter.getDate() + 1);
        
        var dd = selectedDateAfter.getDate();
        dd = this.updateDay(dd);
        
        var mm = selectedDateAfter.getMonth() + 1; // месяц 1-12
        mm = this.updateMonth(mm);
        var formatDateAfter = dd + '-' + mm + '-' + selectedDateAfter.getFullYear();
        var selectedDateNewFormat = this.time(year + '-' + month + '-' + day);
        
        if(selectedDateNewFormat >= this.time(date.getFullYear() + '-' + this.updateMonth(date.getMonth() + 1) + '-' + date.getDate())) {
            
            
            var maskHeight = $(document).height();
            var maskWidth = $(window).width();
            var overlay = $('#overlay');
            
            overlay.css({'width':maskWidth,'height':maskHeight});
                $.ajax({
                    type: 'POST',
                    url: '/get-info-room-popup',
                    data: {'_token': $('meta[name="csrf-token"]').attr('content'), roomId:roomId, date:selectedDate},
                    success: function(result){

                        overlay.fadeIn();
                        $('.wrapper-popup-calendar').html(result['html']);
                        $('#datetimepicker1').val(selectedDate);
                        $('#datetimepicker2').val(formatDateAfter);
                        $('#datetimepicker1').datetimepicker({language: 'ru', pickTime:false, minDate: date,defaultDate: selectedDate, format: 'DD-MM-YYYY'});
                        $('#datetimepicker2').datetimepicker({language: 'ru', pickTime:false, minDate: formatDateAfter, defaultDate: formatDateAfter, format: 'DD-MM-YYYY'});
                        $('body').find('.popup-calendar').fadeIn();
                    }
                });
            
        }
    },
    
    updateDay: function(day) {
        if(day < 10) {
            var newDay = '0' + day;
            return newDay;
        }
        return day;
    },
    
    updateMonth:function(month) {
        if(month < 10) {
            var newMonth = '0' + month;
            return newMonth;
        }
        return month;
    },

    closePopupCalendar: function(e) {
        e.preventDefault();
        var popupCallback = $('.popup-calendar');
        popupCallback.fadeOut(300, function () {
            $('#overlay').fadeOut(250);
        });
    },


    sendDataByRoom: function(e) {
        e.preventDefault();
        var roomId = $('#roomId').val(),
            dateFrom = $('input[name="dateFrom"]').val(),
            dateTo = $('input[name="dateTo"]').val(),
            price = $('input[name="price"]').val(),
            countRoom = $('input[name="countRoom"]').val(),
            closeRoom = $('.room-not-available input').prop("checked"),
            discount = $('input[name="discount"]').val();
            var dayWeek = $('input[name="dayWeek[]"]');
        var arrayDayWeek = [];
        $.each(dayWeek, function(indx,element){
            var thatElement = $(element);
            if(thatElement.prop("checked")) {
                arrayDayWeek.push(thatElement.val());
            }
            
        });

        if(closeRoom) {
            closeRoom = 1;
        }
        else {
            closeRoom = 0;
        }
        
        if(!this.patternInt.test(roomId) || !price || !parseFloat(price) || (discount && discount > 100)) {
            $('.errors-message').text('Не корректные данные. Попробуйте обновить страницу!').animate({top:'10%', opacity:1}, 300).delay(2000).animate({top:'-20%', opacity:0});
            return false;
        }
              
            $.ajax({
                type: 'POST',
                url: '/get-calendar-by-room',
                data: {'_token': $('meta[name="csrf-token"]').attr('content'), roomId: roomId, dateFrom:dateFrom, dateTo:dateTo, price:price, countRoom:countRoom, closeRoom:closeRoom, discount:discount, dayWeek:arrayDayWeek},
                success: function(result) {
                    console.log(result);
                    if(result['success']) {
                        Calendar.closePopupCalendar(e);
                        $('.wrapper-js-calendar').html(result['html']);
                    }
                    else {
                        $('.errors-message').text(result['message']).animate({top:'10%', opacity:1}, 300).delay(2000).animate({top:'-20%', opacity:0});
                    }
                    
                }
            });

        
        
        

    },

    reloadMonth: function(month) {
        var roomId = $('#roomId').val(),
        wrapperCalendar = $('.wrapper-js-calendar');
        var currentYear = $('#selectedYear').val();
        
        if(!this.patternInt.test(roomId) || !this.patternInt.test(currentYear)) {
            $('.errors-message').text('Не корректные данные. Попробуйте обновить страницу!').animate({top:'10%', opacity:1}, 300).delay(2000).animate({top:'-20%', opacity:0});
            return false;
        }
        
        
        $.ajax({
            type: 'POST',
            url: '/get-month-info',
            data:{month:month, '_token': $('meta[name="csrf-token"]').attr('content'), roomId: roomId, year:currentYear},
            success: function(result) {
                wrapperCalendar.html(result['html']);
            }
        });

    },
    
    
    getCalendarByRoomAboutDate:function() {
        var currentYear = $('#selectedYear').val(),
            wrapperCalendar = $('.wrapper-js-calendar'),
            currentMonth = $('.list-month ul li a.active').attr('data-month'),
            roomId = $('#roomId').val();
        
        if(!this.patternInt.test(roomId) || !this.patternInt.test(currentYear) || !this.patternInt.test(currentMonth)) {
            $('.errors-message').text('Не корректные данные. Попробуйте обновить страницу!').animate({top:'10%', opacity:1}, 300).delay(2000).animate({top:'-20%', opacity:0});
            return false;
        }
        
        
        $.ajax({
            type: 'POST',
            url: '/get-month-info',
            data: {month:currentMonth, '_token': $('meta[name="csrf-token"]').attr('content'), roomId: roomId, year:currentYear},
            success: function(result) {
                wrapperCalendar.html(result['html']);
            }
        });
        
    },
    
    time: function(date){
        var d = new Date(date);
        return parseInt(d.getTime() / 1000);
    }
    
    
    
    
};