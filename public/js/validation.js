var Validation = {
    
    messages: [],
    elements: [],
    patternString: /^[a-zA-Z0-9а-яёА-ЯЁ\s\-]+$/u,
    patternInt: /^[0-9]+$/u,
    patternEmail: /.+@.+\..+/i,
    
    init: function(data) {
        this.messages.length = 0;
        var countObjects = Object.keys(data).length,
            arrayObjects = data;
        
        
        if(countObjects.lenght == 0) {
            return false;
        }
        
        for(var currentObject in arrayObjects) {

            var currentObject = arrayObjects[currentObject],
                elementName = currentObject.element,
                rules = currentObject.rules; 
            
            var currentElement = this.getElement(elementName);
            var elementName = this.getElementName(currentElement);
            var elementValue = this.getElementValue(currentElement);
            var arrayRules = this.getArrayRulesElement(rules);
            var errorElement = false;
            
            arrayRules.forEach(function(element, index, arrayRule){
                var result = Validation.selectFunctionByRules(element, elementName, elementValue);
                if(!result) {
                    Validation.errorField(currentElement);
                    Validation.showErrors(Validation.messages);
                    errorElement = true;
                }

            });
            
            if(!errorElement) {
                Validation.removeStyleErrorField(currentElement);
            }
            
        }
        
        if(Validation.messages.length == 0) {
            return true;
        }
        return false;
        
        
    },
        
        
    getElement: function(elementName) {
        
        var element = $('body').find('[name="'+ elementName +'"]');    
        return element;
        
    },
    
    getElementValue:function(element) {
        var elementValue = element.val();
        return elementValue;
    },
    
    getElementName: function(element) {
        return element.attr('data-name');    
    },
    
    getArrayRulesElement: function(rules) {
        var arrayRules = rules.split('|');
        return arrayRules;
    },
    
    selectFunctionByRules: function(rule, elementName, elementValue) {
        var thatRules = rule.split(':');
        if(Validation[thatRules[0] + 'Function']) {
            var result = Validation[thatRules[0] + 'Function'](rule, elementName, elementValue);
            return result;
        }
        else {
            console.log('Метод с именем: '+ thatRules[0] +' отсутствует');
        }
        return true;
    },
    
    requiredFunction:function(rule, elementName, elementValue) {
        if(elementValue.length == 0) {
            var arrayMessages = Validation.messages;
            arrayMessages.push("Поле с именем: \"" + elementName + "\" пустое!");
            return false;
        }
        
        return true;
        
    },
    
    stringFunction: function(rule, elementName, elementValue) {
        var pattern = this.patternString;
        if(!pattern.test(elementValue)) {
            var arrayMessages = Validation.messages;
            arrayMessages.push("Поле с именем: \"" + elementName + "\" должно быть текстовое!");
            return false;
        }        
        return true;
    },
    
    minFunction: function(rule, elementName, elementValue) {
        var thatRules = rule.split(':');
        if(elementValue.length < thatRules[1]) {
            var arrayMessages = Validation.messages;
            arrayMessages.push("Поле с именем: \"" + elementName + "\" должно быть минимум из "+ thatRules[1] +"!");
            return false;
        }
        return true;
        
    },
    
    intFunction: function(rule, elementName, elementValue) {
        var pattern = this.patternInt;
        elementValue = elementValue.trim();
        if(!pattern.test(elementValue)) {
            var arrayMessages = Validation.messages;
            arrayMessages.push("Поле с именем: \"" + elementName + "\" должно содержать только лишь цифры!");
            return false;
        }
        return true;
    },
    
    emailFunction: function(rule, elementName, elementValue) {
        var pattern = this.patternEmail;
        if(!pattern.test(elementValue)) {
            var arrayMessages = Validation.messages;
            arrayMessages.push('Неккоректные данные в поле ' + elementName);
            return false;
        }
        return true;
    },
    
    inFunction: function(rule, elementName, elementValue) {
        var thatRules = rule.split(':');
        thatRules = thatRules[1].split(',');
        if(thatRules.indexOf(elementValue) == -1) {
            var arrayMessages = Validation.messages;
            arrayMessages.push('Неккоректные данные в поле ' + elementName);
            return false;   
        }
        return true;
    },
    
    
    errorField: function(element) {
        element.css({border: '1px solid red'});
        $('body').scrollTop(0);
    },
    
    showErrors: function(messages) {
        wrapperMessages = $('body').find('.alert-danger-js');
        wrapperMessages.find('ul').empty();
        for(var i = 0; i < messages.length; i++) {
            wrapperMessages.find('ul').append('<li>'+ messages[i] +'</li>');
        }
        wrapperMessages.show();
    },
    
    removeStyleErrorField: function(element) {
        element.css({border: '1px solid #ccc'});
    }
    
    
    
}