var newFunction = {

    showMobileCategory:function(element) {
        var el = $(element);
        $('.category-list').slideToggle();
    },

    saveInfoUser:function(element) {
        var el = $(element);
        var data = el.closest('form').serialize();
        var wrap = el.closest('.personal-info-user');

        $.ajax({
            type: 'post',
            url: '/account/save-profile',
            data:data,
            beforeSend:function() {
                var clonePreloader = $('.loader-inner').clone();
                var overlay = $('.overlay-save').clone();
                overlay = overlay.addClass('clone').removeClass('hide');
                clonePreloader = clonePreloader.addClass('loader-user clone');
                wrap.append(clonePreloader.removeClass('hide'));
                wrap.append(overlay);
                var message = $('.info-save-message').clone();
                message = message.addClass('clone clone-message');
                wrap.append(message);
            },
            success:function (result) {
                if(result.status) {
                    $('.loader-user').remove();
                    var overlay = $('.overlay-save.clone');
                    var message = $('.clone-message');
                    message.fadeIn(500, function () {
                        setTimeout(function() {
                            $('.clone').remove();
                        }, 2000);
                    });

                }
                else {
                    $('.loader-user').remove();
                    var overlay = $('.overlay-save.clone');
                    var message = $('.clone-message');
                    message.css({'color': 'red'}).text(result.error);
                    message.fadeIn(500, function () {
                        setTimeout(function() {
                            $('.clone').remove();
                        }, 2000);
                    });
                }

            }
        });
    },

    clickByInputImg:function() {
        $('.hidden-field')[0].click();
    },

    showImgUser:function(element) {
        var reader = new FileReader();
        reader.onload = function (e) {
            // get loaded data and render thumbnail.
            console.log(e.target.result);
            $('.block-img-profile img').attr('src', e.target.result);
        };

        // read the image file as a data URL.
        reader.readAsDataURL(element.files[0]);
    },

    getBonusList:function() {
        var date = $('.date-range-picker');
        var cityId = $('input[name="city"]');

        $.ajax({
            type: 'post',
            url: '/account/bonus',
            data: {'_token': $('meta[name="csrf-token"]').attr('content'), date:date.val(), cityId:cityId.val()},
            success:function(result) {
                $('.wrap-table-bonus').html(result['html']);
            }
        });
    },

    showFilter:function(element) {
        var el = $(element);
        var filter = el.siblings('table');
        filter.slideToggle();
    },

    showCategoryReview:function(element) {
        var el = $(element);
        var id = el.attr('data-id');
        if(id) {
            $.each($('.one-review-hotel'), function(indx, element) {
                var eachElement = $(element);
                if(eachElement.attr('data-category-review-id') != id) {
                    eachElement.hide();
                }
                else {
                    eachElement.show();
                }
            });
        }
        else {
            $('.one-review-hotel').show();
        }

    },


    saveCallback:function(e) {
        e.preventDefault();
        var data = $('.contact-form').serialize();

        $('.success-reservation').find('h4').text('Благодарим за Ваш отзыв!');
        $('.success-reservation').find('span').text('Мы перезвоним Вам в ближайщее время!');

        $.ajax({
            type: 'post',
            url: '/send-review',
            data: data,
            success:function (result) {
                $('.contact-form')[0].reset();
                Popup.showPopup('success-reservation', false);
            }
        });
    },

    addApplication:function (e, element) {
        e.preventDefault();
        var el = $(element);
        var data = el.closest('form').serialize();
        $('.success-reservation').find('h4').text('Благодарим за Вашу заявку!');
        $('.success-reservation').find('span').text('Мы перезвоним Вам в ближайщее время!');

        $.ajax({
            type: 'POST',
            url: '/add-application',
            data:data,
            success:function (result) {
                el.closest('form')[0].reset();
                Popup.showPopup('success-reservation', false);
            }
        });
    },

    addApplicationGroup:function (e, element) {
        e.preventDefault();
        var el = $(element);
        var data = el.closest('form').serialize();
        $('.success-reservation').find('h4').text('Благодарим за Вашу заявку!');
        $('.success-reservation').find('span').text('Мы перезвоним Вам в ближайщее время!');

        $.ajax({
            type: 'POST',
            url: '/add-application-group',
            data:data,
            success:function (result) {
                el.closest('form')[0].reset();
                Popup.showPopup('success-reservation', false);
            }
        });
    },


    showTopMenuMobile:function (element) {
        var el = $(element);
        if(el.attr('data-active') == 'off') {
            $('.mobile-top-menu').slideDown();
            $("html,body").css("overflow","hidden");
        }
        else {
            $('.mobile-top-menu').slideUp();
            $("html,body").css("overflow","inherit");

        }

    },

    showPopupGuest:function() {
        var popupGuest = $('.block-count-guest');
        var val = popupGuest.find('input').val();
        if(val.length == 0) {
            popupGuest.find('input').val('1');
        }
        popupGuest.queue(function() {
            $(this).animate({top: '60px', opacity: 1}, 100);
        }).dequeue();

    },

    showFullMapCategory:function() {
        $('.wrap-wrap-objects').hide();
        $('.popup-with-map').find('#map1').empty();
        $('.popup-with-map').fadeIn(100, function() {
            setTimeout(function() {initMapCategory();}, 400);
        });

    },

    selectDistance:function(element) {
        var el = $(element);
        var distance = el.attr('data-radius');
        if(distance > 10) {
            distance = 10;
        }

        el.closest('.wrapper-span-beside-object-distance').find('span:first').text(+ distance + ' км');
        el.closest('.wrapper-span-beside-object-distance').find('input[name="radius"]').val(distance);
        el.closest('.list-distance-for-page-category').fadeOut();
    },

    showListCategory:function(element) {
        var el = $(element);
        console.log('я нажимаю');
        el.siblings('ul').fadeIn();
    },

    selectCategoryReview:function(element) {
        var el = $(element);
        el.closest('.span-select-for-category').find('input').val(el.attr('data-id'));
        el.closest('.span-select-for-category').find('span span').text(el.text());
        el.closest('.span-select-for-category').find('ul').fadeOut(300);
    },

    userLat: false,
    userLng: false,

    currentPosition:function(e) {

        newFunction.userLat = false;
        newFunction.userLng = false;
        var km = $('input[name="radius"]').val();
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                newFunction.userLat = position.coords.latitude;
                newFunction.userLng = position.coords.longitude;

                Filter.generateString();

                $.ajax({
                    type: 'get',
                    url: '/category?' + Filter.stringPath,
                    data: {type: 'nearestObjects', distance: km, userLat: newFunction.userLat, userLng:newFunction.userLng },
                    beforeSend:function() {
                        $('.overlay-category').show();
                        $('.refresh-page').removeClass('hide');
                    },
                    success:function(result) {
                        $('.overlay-category').hide();
                        $('.refresh-page').addClass('hide');
                        $('.list-objects').html(result['htmlObjects']);
                        $('.wrapper-sidebar-html').html(result['filtersHtml']);
                        if(result['cityForSearchString']) {
                            $('input[name="search"]').val(result['cityForSearchString']);
                        }
                    }
                });


            });
        }



        // var R = 6378137;
        // var dLat  = newFunction.radius(50.4723612 - 50.4679305);
        // var dLong = newFunction.radius(30.5202946 - 30.520081);
        // var a = Math.sin(dLat/2) * Math.sin(dLat/2) + Math.cos(newFunction.radius(50.4679305)) * Math.cos(newFunction.radius(50.4723612)) * Math.sin(dLong/2) * Math.sin(dLong/2);
        // var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        // var d = R * c;
        //
        // console.log(d.toFixed(3));
    },

    // radius:function(x) {
    //     return x  *Math.PI/180;
    // },

    listDistance:function () {
        $('.list-distance-for-page-category').fadeIn();
    },

    listRegions:function () {
        $('.list-distance-for-page-category').fadeIn();
    },

    showOnlySelectRegionAndCity:function() {
        var city = $('input[name="city"]').val();
        var region = $('input[name="regionSelect"]').val();
        $('.list-cities-region').each(function (indx, element) {
            var el = $(element);
            if(el.attr('data-region') != region) {
                el.addClass('hide');
            }
            else {
                el.removeClass('hide');
            }

            if(city.length) {
                el.find('.one-city').each(function (indx, element) {
                    var currentCity = $(element);
                    var cityName = currentCity.find('.name-city').text().toLowerCase();
                    if(cityName.indexOf(city) == -1) {
                        currentCity.addClass('hide');
                    }
                    else {
                        currentCity.removeClass('hide');
                    }
                });
            }
            else {
                $('.one-city').removeClass('hide');
            }

            if(region == 0) {
                el.removeClass('hide');
            }

            if(el.find('.one-city').not('.hide').length == 0) {
                el.addClass('hide');
            }

        })
    },

    selectRegion:function (element) {
        var el = $(element);
        $('.main-region').text(el.text());
        $('input[name="regionSelect"]').val(el.data('region'));
        $('.list-distance-for-page-category').fadeOut();
    },

    closeFullMapCategory: function() {
        $('.wrap-wrap-objects').show();
        $('.popup-with-map').fadeOut();
        $('.popup-with-map').find('#map1').remove();
        $('.popup-with-map').append('<div id="map1"></div>');
    },

    insertUserData:function(element) {
        var el = $(element);
        var fio = el.attr('data-fio');
        var phone = el.attr('data-phone');
        var email = el.attr('data-email');
        var form = $('.form-for-reservation');
        form.find('input[name="fio"]').val(fio);
        form.find('input[name="phone"]').val(phone);
        form.find('input[name="email"]').val(email);
    },

    changeReviewCategory:function() {
        var list = $('.list-category-review-popup');
        list.show();
    },

    selectReviewCategory:function(element, name) {
        var el = $(element);
        var list = $('.list-category-review-popup');
        var textarea = $('textarea[name="review"]');
        $('input[name="type"]').val(name);
        $('.popup-reviews-site').find('span.select-input').text(el.text());
        list.hide();

        switch(name){
            case 'positive':
                textarea.attr('placeholder', 'Обрадуйте нас :)');
                break;
            case 'negative':
                textarea.attr('placeholder', 'Мы принимаем правду как есть');
                break;
            case 'wishes':
                textarea.attr('placeholder', 'Нам интересны ваши идеи');
                break;
            case 'error':
                textarea.attr('placeholder', 'Сообщите нам, мы постараемся ее решить');
                break;
        }

    },

    showRoom:function (element) {
        var el = $(element);
        var id = el.attr('data-id');
        $('.menu-hotel').find('li a')[1].click();
        var room = $('li[data-room-id="'+ id +'"]');
        $('html, body').animate({scrollTop:room.offset().top}, 500, function () {
            room.find('.btn-more-room').attr('data-show', 'on').closest('.one-room-this-hotel').find('.other-info-about-room').slideDown();
        });

    },

    sendReview:function(e) {
        $('.red').hide();
        e.preventDefault();
        var form = $('.review-form');
        var data = form.serialize();
        var marker = false;
        var categoryId = form.find('input[name="category"]'),
            typeFeedback = form.find('input[name="typeFeedback"]'),
            positiveText = form.find('[name="positiveText"]'),
            negativeText = form.find('[name="negativeText"]'),
            comfortValue = form.find('[name="comfortValue"]'),
            employeeValue = form.find('[name="employeeValue"]'),
            serviceValue = form.find('[name="serviceValue"]'),
            cleanValue = form.find('[name="cleanValue"]'),
            qualityPriceValue = form.find('[name="qualityPriceValue"]'),
            locationValue = form.find('[name="locationValue"]'),
            isRecHotel = form.find('[name="isRecHotel"]'),
            projectValue = form.find('[name="projectValue"]'),
            globalReview = form.find('[name="global-review"]');

        if(!categoryId.val()) {
            categoryId.closest('.span-select-for-category').find('.red').show();
            marker = true;
        }

        if(!typeFeedback.filter(':checked').val()) {
            typeFeedback.closest('.type-feedback-block').find('.red').show();
            marker = true;
        }

        if(!positiveText.filter(':checked').val()) {
            positiveText.closest('li').find('.red').show();
            marker = true;
        }

        if(!negativeText.filter(':checked').val()) {
            negativeText.closest('li').find('.red').show();
            marker = true;
        }

        if(!comfortValue.filter(':checked').val()) {
            comfortValue.closest('ul').closest('li').find('.red').show();
            marker = true;
        }

        if(!employeeValue.filter(':checked').val()) {
            employeeValue.closest('ul').closest('li').find('.red').show();
            marker = true;
        }

        if(!serviceValue.filter(':checked').val()) {
            serviceValue.closest('ul').closest('li').closest('li').find('.red').show();
            marker = true;
        }

        if(!cleanValue.filter(':checked').val()) {
            cleanValue.closest('ul').closest('li').find('.red').show();
            marker = true;
        }

        if(!qualityPriceValue.filter(':checked').val()) {
            qualityPriceValue.closest('ul').closest('li').find('.red').show();
            marker = true;
        }

        if(!locationValue.filter(':checked').val()) {
            locationValue.closest('ul').closest('li').find('.red').show();
            marker = true;
        }

        if(!isRecHotel.filter(':checked').val()) {
            isRecHotel.closest('.is-recommendation-hotel').find('.red').show();
            marker = true;
        }

        if(!globalReview.val()) {
            globalReview.closest('.main-review-feedback').find('.red').show();
            marker = true;
        }

        if(!projectValue.filter(':checked').val()) {
            projectValue.closest('.raiting-project').find('.red').show();
            marker = true;
        }

        if(marker) {

            $('body, html').animate({
                scrollTop: $('body, html').offset().top
            });

            return false;
        }

        $.ajax({
            type: 'post',
            url: '/save-feedback',
            data: data,
            success:function(result) {
                $('.success-reservation').find('h4').text('Вы успешно оставили отзыв!');
                $('.success-reservation').find('span').text('Спасибо что вы с нами!');
                Popup.showPopup('success-reservation', false);
            }
        });
    },

    showReview:function(reviewId) {
        $('.menu-hotel').find('li a')[2].click();
        var review = $('.one-review-hotel[data-review-id="'+ reviewId +'"]');
        $('html, body').animate({scrollTop:review.offset().top - 30}, 500);
    },

    saveNumberCall:function () {
        var popup = $('.popup-recall');
        var phone = popup.find('input[name="phone"]');
        var number = parseInt(phone.val().replace(/\D+/g,""));
        $('.success-reservation').find('h4').text('Вы успешно отправили заявку!');
        $('.success-reservation').find('span').text('Мы с Вами свяжемся в ближайщее время.');

        if(isNaN(number)) {
            phone.addClass('border-red');
            return false;
        }
        else {
            phone.removeClass('border-red');
        }

        $.ajax({
            type: 'post',
            url: '/send-callback',
            data: {'_token': $('meta[name="csrf-token"]').attr('content'), phone:phone.val()},
            success:function () {
                popup.hide(100, function() {
                    Popup.showPopup('success-reservation', false);
                    phone.val('');
                });
            }
        });
    },

    saveReviewSite:function(element, e) {
        e.preventDefault();
        var el = $(element);
        var data = el.closest('form').serialize();
        $('.red-error-popup').hide();
        var marker = false;

        var category = el.closest('form').find('input[name="type"]');
        var name = el.closest('form').find('input[name="name"]');
        var email = el.closest('form').find('input[name="email"]');
        var text = el.closest('form').find('textarea');

        if(!category.val()) {
            category.next().css('display', 'block');
            marker = true;
        }

        if(!name.val()) {
            name.next().css('display', 'block');
            marker = true;
        }

        var pattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
        if(!email.val() || !pattern.test(email.val())) {
            email.next().css('display', 'block');
            marker = true;
        }

        if(!text.val() || text.val().length < 10) {
            text.next().css('display', 'block');
            marker = true;
        }

        console.log(marker);
        if(!marker) {
            $('.success-review-popup').remove();
            var clonePopup = $('.success-reservation').clone();
            clonePopup.removeClass('reservation-popup').addClass('success-review-popup');
            clonePopup.find('h4').text('Благодарим за Ваш отзыв!');
            clonePopup.find('span').text('Мы очень рады что Вы помогаете сделать наш проект лучше!');
            $('body').append(clonePopup);

            $.ajax({
                type: 'POST',
                url: '/send-review',
                data:data,
                success:function (result) {
                    console.log(el.closest('.popup').length);
                    if(el.closest('.popup').length) {
                        el.closest('.popup').fadeOut(500, function() {
                            Popup.showPopup('success-review-popup', false);
                            $('.popup-reviews-site').find('form')[0].reset();
                            $('.select-input').text('Выберите категорию');
                        });
                    }
                    else {

                        Popup.showPopup('success-review-popup', false);
                        el.closest('form')[0].reset();
                    }

                }
            });
        }


    },

    deacrementCountGuest:function() {
        var popupGuest = $('.block-count-guest');
        var val = popupGuest.find('input').val();
        if(val != 1) {
            popupGuest.find('input').val(+val - 1);
            $('.input-count-guest input[name="guestNumber"]').val(+val - 1);
            $('.guest-field-style span span').text(+val - 1);
        }
    },

    incrementCountGuest:function() {
        var popupGuest = $('.block-count-guest');
        var val = popupGuest.find('input').val();
        popupGuest.find('input').val(+val + 1);
        $('.input-count-guest input[name="guestNumber"]').val(+val + 1);
        $('.guest-field-style span span').text(+val + 1);
    },

    prevCarouselSlide:function(nameElement) {
        var slider = $('.' + nameElement);
        var wrapperSlider = slider.find('ul');
        var countSlide = wrapperSlider.find('li').length;
        if(countSlide < 3) return false;
        var slideWidth = wrapperSlider.find('li').innerWidth();
        wrapperSlider.find('li').eq(-1).clone().prependTo(wrapperSlider);
        wrapperSlider.css({"left":"-"+slideWidth+"px"});
        wrapperSlider.find('li').eq(-1).remove();
        wrapperSlider.animate({left: "0px"}, 200);
    },

    nextCarouselSlide:function(nameElement) {
        var slider = $('.' + nameElement);
        var wrapperSlider = slider.find('ul');
        var countSlide = wrapperSlider.find('li').length;
        if(countSlide < 3) return false;
        var slideWidth = wrapperSlider.find('li').innerWidth();

        wrapperSlider.animate({left: "-"+ slideWidth +"px"}, 200, function(){
            wrapperSlider.find('li').eq(0).clone().appendTo(wrapperSlider);
            wrapperSlider.find("li").eq(0).remove();
            wrapperSlider.css({left: 0});
        });
    },

    changeTabCities:function(element) {

        var el = $(element);
        var tab = el.attr('data-tab');
        $('.category-list li').removeClass('active');
        el.closest('li').addClass('active');
        $('.list-cities-category').removeClass('active');
        $('.list-cities-category[data-tab="'+ tab +'"]').addClass('active');
    },

    showAllImagesHotels:function(element) {
        var el = $(element);
        var active = el.attr('data-active');
        if(active == 'off') {
            el.attr('data-active', 'on');
            $('.list-images-hotel').css({overflow: 'initial', height: 'auto'});
            $('.btn-show-more-img-hotel a').css({backgroundColor:'#CACACA'}).find('i').removeClass('fa-angle-down').addClass('fa-angle-up');
        }
        else {
            el.attr('data-active', 'off');
            $('.list-images-hotel').css({overflow: 'hidden', height: '100px'});
            $('.btn-show-more-img-hotel a').css({backgroundColor:'#0B8FDA'}).find('i').removeClass('fa-angle-up').addClass('fa-angle-down');
        }


    },

    tabsToogle:function(element, tabId) {
        var el = $(element);
        if(!el.hasClass('active-tab-menu-hotel')) {
            $('.menu-hotel ul li').removeClass('active-tab-menu-hotel');
            el.closest('li').addClass('active-tab-menu-hotel');
            $('.tabs-menu-hotel ul li').removeClass('active-tab-content');
            $('.tabs-menu-hotel ul li[data-tab="'+ tabId +'"]').addClass('active-tab-content');
        }
    },

    showMoreInfoRoom:function(element) {
        var el = $(element);
        var showAttr = el.attr('data-show');
        var wrapperShow = el.closest('.one-room-this-hotel').find('.other-info-about-room');
        if(showAttr == 'off') {
            wrapperShow.slideDown();
            el.attr('data-show', 'on');
            el.css({borderBottom: 'none'});
        }
        else {
            wrapperShow.slideUp(300, function() {
                el.attr('data-show', 'off');
                el.css({borderBottom: '1px dashed #0B8FDA'});
            });

        }
    },

    removeErrorField:function(element) {
        $(element).removeClass('border-red').next().addClass('hide');
        if($(element).prop('checked')) {
            $(element).closest('tr').find('.error-checkbox').addClass('hide');
        }
    },

    scrollToElement:function(name) {
        var el = $('#' + name);
        $('html, body').animate({scrollTop: el.offset().top }, 1000);
    },


    showFullReviewPopup:function(element) {
        var el = $(element);
        $('.one-review-popup-block').removeClass('active');
        if(el.attr('data-active') == 'on') {
            el.removeClass('active').attr('data-active', 'off');
        }
        else {
            el.addClass('active').attr('data-active', 'on');
        }

    },

    showPopupMoreInfo:function(objectId, block) {
        if(!objectId) return false;

        $.ajax({
            type: 'post',
            url: '/get-more-info-object',
            data: {'_token': $('meta[name="csrf-token"]').attr('content'), objectId:objectId},
            success:function(result) {
                $('.quick-view').html(result.html);
                Popup.showPopup('quick-view');
                setTimeout( function(){initMap(result.longitude, result.latitude);} , 400);
                $('body').find('.menu-popup ul li a[data-tab='+ block +']')[0].click();
            }
        });

    }

};


var Popup = {

    closePopup: function(element) {
        var thatElement = $(element);
        var popup = $('.popup');

        if(thatElement.closest('.popup').hasClass('reservation-popup')) {
            var userId = $('input[name="userId"]').val();

            if(userId) {
                window.location.href = '/account/reservations';
            }
            else {
                window.location.href = '/registration';
            }
        }

        var overlay = $('.overlay');
        var maskHeight = $(document).height();
        var maskWidth = $(window).width();
        overlay.css({'width':maskWidth,'height':maskHeight});
        popup.fadeOut(300, function() {
            overlay.fadeOut(300);
        });

    },

    showPopup:function(namePopup, e) {

        if(e) {
            e.preventDefault();
        }


        var overlay = $('.overlay');
        var maskHeight = $(document).height();
        var maskWidth = $(window).width();
        overlay.css({'width':maskWidth,'height':maskHeight});
        overlay.fadeIn(300, function() {
            $('.' + namePopup).fadeIn(300);
            if(namePopup == 'map') {
                initMap();
            }
        });
    },

    closePopupRegistration:function(element) {
        var el = $(element);
        el.closest('.popup').animate({'right':'-1000px'}, 1000);
        $.cookie('isRegistration', '1', {expires: 1});
        wrapStartPopupPromocode();
    },

    closePopupPromocode:function(element) {
        var el = $(element);
        var overlay = $('.overlay');
        var maskHeight = $(document).height();
        var maskWidth = $(window).width();
        overlay.css({'width':maskWidth,'height':maskHeight});
        $('.promocode-popup').fadeOut(300, function() {
            overlay.fadeOut(300);
            $.cookie('isPromocode', '1', {expires: 1});
        });
    },

    showGalleryImgPopup:function(element) {
        var thatElement = $(element);
        var popup = $('.popup-gallery-images-room');
        var arrayImg = thatElement.closest('.one-shares').find('input[name="arrayImages"]').val();
        arrayImg = arrayImg.split(',');
        var firstImg = arrayImg[0];
        firstImg = firstImg.replace('_mini', '_big');

        var listImagesPopup = popup.find('.list-images-current-room ul');
        var nameHotel = thatElement.closest('.one-room-this-hotel').find('.name-room-hotel strong').text();
        popup.find('span').text(nameHotel);
        listImagesPopup.empty();
        popup.find('.main-img-popup-gallery img').attr('src', '/' + firstImg);


        if(arrayImg.length < 5) {
            $('.arrow-gallery-popup').hide();
            $('.list-images-current-room').css({margin: 0});
        }
        else {
            $('.arrow-gallery-popup').show();
            $('.list-images-current-room').css({margin: '0 auto'});
        }

        $.each(arrayImg, function (indx, element) {
            if(!indx) {
                listImagesPopup.append('<li class="active" data-number="'+ indx +'"><a href="javascript:void(0);"><img src="/' + element +'" alt=""></a></li>');
            }
            else {
                listImagesPopup.append('<li data-number="'+ indx +'"><a href="javascript:void(0);"><img src="/' + element +'" alt=""></a></li>');
            }

        });

        var overlay = $('.overlay');
        var maskHeight = $(document).height();
        var maskWidth = $(window).width();
        overlay.css({'width':maskWidth,'height':maskHeight});
        overlay.fadeIn(300, function() {
            popup.fadeIn(300);
        });
    },


    showRecallPopup:function() {
        var popupRecall = $('.popup-recall');
        popupRecall.fadeIn();
    }



};


var Search = {

    timerId: false,
    currentCity: '',

    changeListActiveSearch:function(code) {
        var resultSearch = $('.result-search');
        var activeLink = resultSearch.find('a.active');
        var listLink = resultSearch.find('li');
        if(!activeLink.length) {
            var firstLink = $(listLink[0]);
            firstLink.find('a').addClass('active');
        }
        else {

            if(code == 40) {
                if(activeLink.closest('li').next().length) {
                    var index = activeLink.closest('li').next().index() + 1;
                    if(index > 4) {
                        resultSearch.animate({
                            scrollTop:(index) * 50
                        });
                    }
                    resultSearch.find('a').removeClass('active');
                    activeLink.closest('li').next().find('a').addClass('active');
                }
            }

            if(code == 38) {
                if(activeLink.closest('li').prev().length) {
                    var index = activeLink.closest('li').prev().index() + 1;
                    if(index < 4) {
                        resultSearch.animate({
                            scrollTop:(index - 3) * 50
                        });
                    }
                    resultSearch.find('a').removeClass('active');
                    activeLink.closest('li').prev().find('a').addClass('active');
                }
            }


        }
    },


    timeOutSearchObject: function(e, element) {
        var el = $(element).val();
        $('.error-search-home').addClass('hide');
        if(el.length < 2) return false;
        // $('.timeOutSearchObject').addClass('hide');
        // if(e.keyCode < 32 && e.keyCode != 8) return null;

        if(e.keyCode == 40 || e.keyCode ==  38) {
            Search.changeListActiveSearch(e.keyCode);
        }

        if(e.keyCode == 13) {
            $('.result-search').find('a.active')[0].click();
            Filter.searchHomeFilter();
        }


        var el = $(element);

        clearTimeout(this.timerId);
        $('.result-search').hide();
        this.timerId = setTimeout(function(){
            if(this.currentCity == el.val()) {
                $('.result-search').show();
                return false;
            }
            else {
                this.currentCity = el.val();
            }

            Search.searchObject(element);
        }, 300);
    },

    searchObject:function(element) {

        var el = $(element);
        var elementValue = el.val();
        var resultBlock = $('.result-search');

        if(elementValue.length >= 2) {
            $.ajax({
                type: 'POST',
                url: '/find-object',
                data: {search: elementValue, '_token': $('meta[name="csrf-token"]').attr('content')},
                success:function(result) {
                    if(result['countObjects'] > 0) {
                        $('.result-search').css({'height': '246px'});
                        resultBlock.html(result['html']).show();
                    }
                    else {
                        $('.result-search').css({'height': '60px'});
                        resultBlock.html('<p>По даному запросу ничего не найдено</p>').show();
                    }
                }
            });
        }

    },

    addTextToField:function(element) {
        var el = $(element);
        var elementValue = el.attr('data-name');
        var resultBlock = $('.result-search');
        var searchForm = $('.search-form');
        searchForm.find('input[name="search"]').val(elementValue);
        searchForm.find('input[name="category"]').val(el.attr('data-category'));
        searchForm.find('.select-category-and-id').attr('name', el.attr('data-category') + 'Id').val(el.attr('data-id'));
        resultBlock.hide();
    }

};

var Gallery = {

    prevSlide:function() {
        var activeImg = $('.list-images-hotel').find('a.active-gallery-img').closest('li');
        var mainImgBLock = $('.main-img-block');
        if(activeImg.prev().length != 0) {
            var prevImg = activeImg.prev();
            if(prevImg.hasClass('btn-show-more-img-hotel')) {
                prevImg = prevImg.prev();
            }

            var img = prevImg.find('img').attr('src');
            var newImg = img.replace(/_mini/g, '');
            $('.wrap-bg-img-main').css({'background-image': 'url(../' + newImg + ')'});
            mainImgBLock.attr('src', newImg);
            $('.list-images-hotel ul li a').removeClass('active-gallery-img');
            prevImg.find('a').addClass('active-gallery-img');
        }
        else {
            var lastImage = $('.list-images-hotel').find('li');
            lastImage = $(lastImage[lastImage.length - 1]);
            img = lastImage.find('img').attr('src');
            img = img.replace(/_mini/g, '');
            mainImgBLock.attr('src', img);
            $('.wrap-bg-img-main').css({'background-image': 'url(../' + img + ')'});
            lastImage.find('a').addClass('active-gallery-img');
        }

    },

    nextSlide:function () {
        var activeImg = $('.list-images-hotel').find('a.active-gallery-img').closest('li');
        var mainImgBLock = $('.main-img-block');
        if(activeImg.next().length != 0) {
            var nextImg = activeImg.next();

            if(nextImg.hasClass('btn-show-more-img-hotel')) {
                nextImg = nextImg.next();
            }

            var img = nextImg.find('img').attr('src');
            var newImg = img.replace(/_mini/g, '');

            mainImgBLock.attr('src', newImg);
            $('.wrap-bg-img-main').css({'background-image': 'url(../' + newImg + ')'});
            $('.list-images-hotel ul li a').removeClass('active-gallery-img');
            nextImg.find('a').addClass('active-gallery-img');

        }
        else {
            var lastImage = $('.list-images-hotel').find('li');
            lastImage = $(lastImage[0]);
            img = lastImage.find('img').attr('src');
            img = img.replace(/_mini/g, '');
            mainImgBLock.attr('src', img);
            $('.wrap-bg-img-main').css({'background-image': 'url(../' + img + ')'});
            lastImage.find('a').addClass('active-gallery-img');
        }


    },

    nextPhoto:function() {
        var activeImg = $('.list-images-quick-view').find('.active-img');
        if(activeImg.next().length != 0) {
            var nextImg = activeImg.next();
            var nextImgIndex = nextImg.index();
            nextImg.addClass('active-img');
            activeImg.removeClass('active-img');
            var img = nextImg.find('img').attr('src');
            var newImg = img.replace(/_mini/g, '');
            $('.main-img-photo img').attr('src', newImg);
            $('.background-img-photo').css({'background-image': 'url(../' + img + ')'});
            $('.list-images-quick-view').animate({scrollLeft: nextImgIndex * 147}, 500);
        }
        else {
            // var lastImage = $('.list-images-quick-view').find('li');
            // lastImage = $(lastImage[0]);
            // img = lastImage.find('img').attr('src');
            // img = img.replace(/_mini/g, '');
            // $('.main-img-photo img').attr('src', img);
            // $('.background-img-photo').css({'background-image': 'url(../' + img + ')'});
            // lastImage.addClass('active-img');
        }

    },

    prevPhoto:function() {
        var activeImg = $('.list-images-quick-view').find('.active-img');
        if(activeImg.prev().length != 0) {
            var nextImg = activeImg.prev();
            var nextImgIndex = nextImg.index();
            nextImg.addClass('active-img');
            activeImg.removeClass('active-img');
            var img = nextImg.find('img').attr('src');
            var newImg = img.replace(/_mini/g, '');
            $('.main-img-photo img').attr('src', newImg);
            $('.background-img-photo').css({'background-image': 'url(../' + img + ')'});
            $('.list-images-quick-view').animate({scrollLeft: (nextImgIndex - 1) * 147}, 500);
        }
        else {
            // var lastImage = $('.list-images-quick-view').find('li');
            // lastImage = $(lastImage[lastImage.length - 1]);
            // img = lastImage.find('img').attr('src');
            // img = img.replace(/_mini/g, '');
            // $('.main-img-photo img').attr('src', img);
            // $('.background-img-photo').css({'background-image': 'url(../' + img + ')'});
            // lastImage.addClass('active-img');
        }

    },

    prevSlideGallery:function() {
        var gallery = $('.list-images-current-room');
        var activeImg = gallery.find('li.active');
        var mainImgBlock = $('.main-img-popup-gallery').find('img');

        if(activeImg.prev().length != 0) {
            var prevImg = activeImg.prev();
            var widthImg = prevImg.outerWidth() + 10;
            var numberImg = prevImg.data('number');
            var img = prevImg.find('img').attr('src');
            var newImg = img.replace(/_mini/g, '');

            mainImgBlock.attr('src', newImg);
            gallery.find('li').removeClass('active');
            prevImg.addClass('active');
            $('.list-images-current-room ul').animate({left: '-' + widthImg * (numberImg - 3)}).clearQueue();
        }
    },

    nextSlideGallery:function() {
        var gallery = $('.list-images-current-room');
        var activeImg = gallery.find('li.active');
        var mainImgBlock = $('.main-img-popup-gallery').find('img');
        if(activeImg.next().length != 0) {
            var nextImg = activeImg.next();
            var index = nextImg.index();
            var countImg = gallery.find('li').length;
            var widthImg = nextImg.outerWidth() + 10;
            var numberImg = nextImg.data('number');
            var img = nextImg.find('img').attr('src');
            var newImg = img.replace(/_mini/g, '');
            mainImgBlock.attr('src', newImg);
            gallery.find('li').removeClass('active');
            nextImg.addClass('active');


            if(index < countImg - index) {
                $('.list-images-current-room ul').animate({left: '-' + widthImg * numberImg}).clearQueue();
            }

        }
    },

    showThisImg:function (element) {
        var el = $(element);
        var img = el.find('img').attr('src');
        var newImg = img.replace(/_mini/g, '');
        var mainImgBLock = $('.main-img-block');
        $('.list-images-hotel ul li a').removeClass('active-gallery-img');
        mainImgBLock.attr('src', newImg);
        el.addClass('active-gallery-img');
    },


    nextImgGalleryQuickView:function() {

    },

    prevImgGalleryQuickView:function() {

    },


};


var Filter = {

    stringPath: false,

    currentClick: false,

    showMoreByFilter:function(element) {
        var el = $(element);
        Filter.currentClick = el;
        Filter.categoryFIlter();
    },

    searchHomeFilter:function() {
        this.stringPath = '';

        var topFilter = $('form[name="topFilter"] input').filter(function(){
            return !!this.value;
        }).serialize();

        if(topFilter) {
            this.stringPath = topFilter;
        }

        if(!$('.select-category-and-id').val()) {

            $('.error-search-home').removeClass('hide');
            return false;
        }

        var localization = $('input[name="localization"]').val();
        if(localization == 'ru') {
            localization = '';
        }

        window.location.href = localization +"/category?" + this.stringPath;
    },

    generateString: function() {
        this.stringPath = '';

        var topFilter = $('form[name="topFilter"] input').filter(function(){
            return !!this.value;
        }).serialize();

        if(topFilter) {
            this.stringPath = topFilter;
        }

        Filter.priceFilter('prices');


        Filter.sortCheckbox('stars');
        Filter.sortCheckbox('typeObject');
        Filter.sortCheckbox('food');
        Filter.sortCheckbox('services');
        Filter.sortCheckbox('pluses');


        var sortFilter = $('input[name="typeSort"]').val();
        if(sortFilter) {
            if(this.stringPath) {
                this.stringPath = this.stringPath + '&' + 'sort=' + sortFilter;
            }
            else {
                this.stringPath = 'sort' + sortFilter;
            }
        }

    },

    categoryFIlter: function() {
        newFunction.closeFullMapCategory();
        Filter.generateString();

        var html = $('html').html();
        var title = $('html').find('title').text();
        var page = $('input[name="currentPage"]');
        page.val(+page.val() + 1);
        window.history.pushState({"html":html,"pageTitle":title},"", 'category?' + this.stringPath);

        if(Filter.currentClick && Filter.currentClick.hasClass('btn-show-more-object')) {
            var btn = $('.btn-show-more-object').hide();
            $.ajax({
                type: 'get',
                url: '/category?' + Filter.stringPath,
                data:{'page': page.val(), type: 'showMore'},
                beforeSend:function() {
                    $('.show-more-loader').removeClass('hide');
                },
                success:function(result) {
                    $('.list-objects').append(result['html']);
                    $('.show-more-loader').addClass('hide');
                    if(result['countObjects'] >= 10) {
                        btn.show();
                    }
                }
            });
            Filter.currentClick = false;
            return false;
        }

        page.val(1);

        $.ajax({
            type: 'get',
            url: '/category?' + Filter.stringPath,
            data:{'page': page.val()},
            beforeSend:function() {
                $('.overlay-category').show();
                $('.refresh-page').removeClass('hide');
            },
            success:function(result) {
                $('.overlay-category').hide();
                $('.refresh-page').addClass('hide');
                $('.wrap-wrap-objects').html(result['htmlObjects']);
                $('.wrapper-sidebar-html').html(result['filtersHtml']);
                $('input[name="radius"]').val('');
                $('.wrapper-span-beside-object-distance').find('span:first').text('Выберите радиус');
                if(result['countObjects'] < 10) {
                    $('.btn-show-more-object').hide();
                }
                else {
                    $('.btn-show-more-object').show();
                }
            }
        });
    },

    priceFilter:function(name) {
        var stringArray = [];
        var minPrice = false;
        var maxPrice = false;

        var filter = $('input[name="'+ name +'[]"]:checked');
        if(filter.length) {
            filter.each(function(index, element) {
                var el = $(element);
                switch(+el.val()) {
                    case 1:
                        minPrice = 0;
                        maxPrice = 199;
                        stringArray.push(1);
                        break;
                    case 2:
                        stringArray.push(2);
                        if(minPrice === false) {
                            minPrice = 200;
                        }
                        else if(minPrice > 200) {
                            minPrice = 200;
                        }

                        if(!maxPrice) {
                            maxPrice = 399;
                        }
                        else if(maxPrice < 399) {
                            maxPrice = 399;
                        }
                        break;
                    case 3:
                        stringArray.push(3);
                        if(minPrice === false) {
                            minPrice = 400;
                        }
                        else if(minPrice > 400) {
                            minPrice = 400;
                        }

                        if(!maxPrice) {
                            maxPrice = 599;
                        }
                        else if(maxPrice < 599) {
                            maxPrice = 599;
                        }
                        break;
                    case 4:
                        stringArray.push(4);
                        if(minPrice === false) {
                            minPrice = 600;
                        }
                        else if(minPrice > 600) {
                            minPrice = 600;
                        }

                        if(!maxPrice) {
                            maxPrice = 799;
                        }
                        else if(maxPrice < 799) {
                            maxPrice = 799;
                        }
                        break;
                    case 5:
                        stringArray.push(5);
                        if(minPrice === false) {
                            minPrice = 800;
                        }
                        else if(minPrice > 800) {
                            minPrice = 800;
                        }

                        if(!maxPrice) {
                            maxPrice = 999;
                        }
                        else if(maxPrice < 999) {
                            maxPrice = 999;
                        }
                        break;
                    case 6:
                        stringArray.push(6);
                        if(minPrice === false) {
                            minPrice = 1000;
                        }

                        maxPrice = false;
                }

            });
            if(minPrice) {
                if(this.stringPath) {
                    this.stringPath = this.stringPath + '&' + 'minPrice='+ minPrice;
                }
                else {
                    this.stringPath = 'minPrice=' + minPrice;
                }
            }

            if(maxPrice) {
                if(this.stringPath) {
                    this.stringPath = this.stringPath + '&' + 'maxPrice='+maxPrice;
                }
                else {
                    this.stringPath = 'maxPrice=' + maxPrice;
                }
            }
            this.stringPath += '&priceRange=[' + stringArray.join() + ']';
        }
    },


    sortCheckbox:function(name) {
        var string = '';
        var filter = $('input[name="'+ name +'[]"]:checked');
        if(filter.length) {
            string = name +'=[';
            filter.each(function (index, element) {
                if(index === (filter.length - 1)) {
                    string = string + $(element).val() + ']';
                }
                else {
                    string = string + $(element).val() + ',';
                }
            });
        }
        if(string) {
            if(this.stringPath) {
                this.stringPath = this.stringPath + '&' + string;
            }
            else {
                this.stringPath = string;
            }
        }

    },

    setSortField:function(element) {
        var el = $(element);
        var typeSort = el.attr('data-sort');
        $('input[name="typeSort"]').val(typeSort);
        $('.sort-filter ul li a').removeClass('active');
        el.addClass('active');
        Filter.categoryFIlter();
    },


    rooms:function() {
        var data = $('.form-find-hotel').serialize();
        $('.content-reservation table').empty();

        $('.rooms-html').empty();
        $.ajax({
            type:'POST',
            url: '/get-ajax-rooms',
            data:data,
            beforeSend:function() {
                $('.loader-inner').removeClass('hide');
            },
            success:function(result) {
                $('.loader-inner').addClass('hide');
                $('.rooms-html').html(result['html']);
            }
        });
    },




};

var Reservation = {

    stringReservation:[],
    toggle:false,

    reservationRoom:function (element) {
        var el = $(element);
        var roomId = el.data('room-id');
        var roomName = el.closest('.one-room-this-hotel').find('.name-room-hotel strong').text();
        var order = $('input[name="order"]');

        var dateFrom = $('input[name="arrival"]').val();
        var dateTo = $('input[name="departure"]').val();
        var countRoom = 1;
        var that = this;

        if(this.stringReservation.length > 0) {


            var isRoom = $('.content-reservation').find('a[data-room-id="'+ roomId +'"]');
            if(isRoom.length > 0) {
                $.each(this.stringReservation, function (indx, element) {
                    if(element.roomId == roomId) {
                        that.stringReservation[indx].countRoom = +that.stringReservation[indx].countRoom + 1;
                        return false;
                    }
                });

            }
            else {
                var oneOrder = {
                    'roomId':roomId,
                    'countRoom': countRoom,
                    'roomName':roomName
                };

                that.stringReservation.push(oneOrder);
            }


        }
        else {
            var oneOrder = {
                'roomId':roomId,
                'countRoom': countRoom,
                'roomName':roomName
            };

            this.stringReservation.push(oneOrder);
        }

        $('.btn-reservation-success').show();


        $('.content-reservation table').empty();

        $.each(this.stringReservation, function (indx, element) {
            var clone = $('body').find('.clone-reservation').clone();
            clone.removeClass('clone-reservation');
            clone.find('.date-reservation-popup').text(dateFrom + ' - ' + dateTo);
            clone.find('.name-room-for-reservation').text(element.roomName);
            clone.find('input[name="guest"]').val(element.countRoom);
            clone.find('.remove-room-for-reservation').attr('data-array-id', indx);
            clone.find('.remove-room-for-reservation').attr('data-room-id', element.roomId);

            $('.content-reservation table').append(clone.removeClass('hide'));
        });

        Popup.showPopup('popup-reservation-rooms', false);
    },

    removeReservation:function(element) {
        var el = $(element);
        var table = el.closest('table');
        var arrayId = el.data('array-id');
        el.closest('tr').remove();

        this.stringReservation.splice(this.stringReservation.indexOf(arrayId), 1);
        var countRow = table.find('tr').length;
        if(countRow == 0) {
            $('.popup-reservation-rooms').find('.btn-close-popup')[0].click();
            $('.btn-reservation-success').hide();
        }
    },

    increamentReservation:function (type, element) {
        var el = $(element);
        var input = el.closest('.other-style-for-count-room').find('input[name="guest"]');
        var val = input.val();
        if(type == '+') {
            input.val(+val + 1); //доделать ajax
        }
        else {
            if(val > 1) {
                input.val(+val - 1);
            }

        }
    },

    continueReservation:function() {
        var table = $('.content-reservation').find('table');
        var that = this;
        that.stringReservation = [];
        $.each(table.find('tr'), function (indx, element) {
            var el = $(element);

            var oneOrder = {
                'roomId':el.find('.remove-room-for-reservation').data('room-id'),
                'countRoom': el.find('input[name="guest"]').val(),
                'roomName':el.find('.name-room-for-reservation').text()
            };

            that.stringReservation.push(oneOrder);

        });

        if(!this.toggle) {
            $('.popup-reservation-rooms').find('.btn-close-popup')[0].click();
        }


    },

    saveReservation:function () {
        this.toggle = true;
        this.continueReservation();
        var string = '';
        var objectName = $('input[name="objectName"]').val();
        var objectId = $('input[name="objectId"]').val();
        var arrival = $('input[name="arrival"]').val();
        var departure = $('input[name="departure"]').val();

        $.each(this.stringReservation, function (indx, element) {
            delete element.roomName;
        });


        $.ajax({
            type: 'POST',
            url: '/hash',
            data: {'_token': $('meta[name="csrf-token"]').attr('content'), 'arrayRoom':JSON.stringify(this.stringReservation)},
            success:function (result) {

                string = '/reservation?hotel=' + objectName + '&hotelId=' + objectId + '&arrival=' + arrival + '&departure=' + departure + '&hash=' + result;
                window.location.href = string;
            }
        });
    },

    saveFullReservation:function (e) {
        e.preventDefault();
        $('.red').hide();

        var agree = $('input[name="agree"]');
        var error = true;


        var fio = $('input[name="fio"]');
        var email = $('input[name="email"]');
        var phone = $('input[name="phone"]');
        var comment = $('textarea[name="text"]');
        var dateFrom = $('input[name="arrival"]');
        var dateTo = $('input[name="departure"]');
        var hotelId = $('input[name="hotelId"]');
        var userId = $('input[name="userId"]');
        var linkReferal = $('input[name="link-referal"]');
        var url = location.href;
        var newUrl = new URL(url);
        var hash = newUrl.searchParams.get('hash');
        var promoCode = $('input[name="promo-code"]');

        if(!agree.prop('checked')) {
            error = false;
            $('.error-agree').show();
        }

        if(!fio.val()) {
            error = false;
            fio.next('.red').show();
        }

        if(!email.val()) {
            error = false;
            email.next('.red').show();
        }

        if(!phone.val()) {
            error = false;
            phone.next('.red').show();
        }

        if(!error) {
            return false;
        }

        $.ajax({
            type: 'post',
            url: '/save-user-reservation',
            data: {'_token': $('meta[name="csrf-token"]').attr('content'), hash:hash, fio:fio.val(), email:email.val(), phone:phone.val(),
                comment:comment.val(), dateFrom:dateFrom.val(), dateTo:dateTo.val(), hotelId:hotelId.val(), userId:userId.val(), linkReferal:linkReferal.val(), promoCode:promoCode.val()},
            success:function (result) {

                if(result) {
                    Popup.showPopup('success-reservation', false);
                }
                else {
                    //показать попап с ошибкой
                }
            }
        });

    }


};


var UserScripts = {
    getReservations:function () {
        var page = $('input[name="page"]');
        $.ajax({
            type: 'get',
            url: '/account/reservations?page=' + page.val(),
            data: {'_token': $('meta[name="csrf-token"]').attr('content')},
            success:function(result) {
                $('.wrap-resevation').append(result['html']);
                if(result['showMore'] == page.val()) {
                    $('.btn-show-more-reservation').remove();
                }
                else {
                    page.val(+page.val() + 1);
                }



            }
        });

    },

    showDropDown:function (element) {
        var el = $(element);
        el.siblings('.dropdown-class').show();
    },

    getBonuses:function() {
        var cityId = $('input[name="city"]').val();
        var date = $('input[name="date"]').val();
        $.ajax({
            type: 'post',
            url: '/account/bonus',
            data: {cityId:cityId, date:date, '_token': $('meta[name="csrf-token"]').attr('content')},
            beforeSend:function() {
                //добавить прелоадер
            },
            success:function (result) {

            }
        });
    },
};

var QuizController = {

    answersCountTrue: 0,

    nextTest:function() {
         var currentQuestion = $('.question-wrapper.active');
         currentQuestion.removeClass('active');
         var nextQuestion = currentQuestion.next('.question-wrapper');
         nextQuestion.addClass('active');
         var iteration = nextQuestion.attr('data-iteration');
         var img = nextQuestion.attr('data-img');
         $('.next-test-btn').hide();

         if(nextQuestion.length > 0) {
             $('.wrap-img-test > img').attr('src', '/images/test/' + img);
             $('.main-content-test h2').text(iteration + '/10');
             $('.main-content-test h3').text(nextQuestion.attr('data-text'));

         }
         else {
             $('.wrap-img-test > img').attr('src', 'images/test/start.jpg');
             $('.main-content-test h2').text('Поздравляем');
             if(this.answersCountTrue == 10) {
                 $('.main-content-test h3').text('Вы успешно прошли тест');
                 $('.wrap-promocode-block').find('h4').html('Вы имеете отличные знания в области географии. Вы получаете');
             }
             else {
                 $('.main-content-test h3').text('Вы успешно ответили на ' + this.answersCountTrue + ' из  10 вопросов');
                 $('.wrap-promocode-block').find('h4').html('Похоже на то, что Вам необходимо улучшить знания <br>в области географии.  Вы получаете');
             }

             $('.wrap-registration-promocode').show();

             $('.wrap-promocode-block, .girl-with-promocode').show();

         }

    },

    selectQuestion:function(element) {
        var el = $(element);
        var wrap = el.closest('li');
        if(wrap.hasClass('good-answer')) {
            this.answersCountTrue++;
        }

        el.closest('li').addClass('isClick');
        if(!el.closest('.question-wrapper').hasClass('isAnswer')) {
            el.siblings('.wrap-answer-question').show();
            el.closest('.question-wrapper').addClass('isAnswer');
            $('.next-test-btn').css({'display': 'block'});
        }

        el.closest('.question-wrapper').find('li').each(function(indx, element) {
            var elem = $(element);
            if(elem.closest('li').hasClass('bad-answer') && !elem.closest('li').hasClass('isClick')) {
                elem.closest('li').removeClass('bad-answer').addClass('other-answer');
            }
        });

    },

    startTest:function() {
        $($('.question-wrapper')[0]).addClass('active');
        var img = $($('.question-wrapper')[0]).attr('data-img');
        var wrap = $($('.question-wrapper')[0]);
        var iteration = wrap.attr('data-iteration');
        $('.wrap-img-test > img').attr('src', '/images/test/' + img);
        $('.main-content-test > p').hide();
        $('.social-test-class').hide();
        $('.start-test-btn').hide();
        $('.main-content-test h2').text(iteration + '/10');
        $('.main-content-test h3').text(wrap.attr('data-text'));
    }

};


$(document).ready(function() {
    $('html,body').mouseup(function (e) {
        var popupGuest = $(".input-count-guest");
        if (popupGuest.has(e.target).length === 0){
            popupGuest.find('.block-count-guest').animate({top: '50px', opacity:0}, 300);
        }

        var popupGuest2 = $('.guest-field-style');

        if (popupGuest2.has(e.target).length === 0){
            popupGuest2.find('.block-count-guest').animate({top: '50px', opacity:0}, 300);
        }

        var popupReview = $('.wrapper-category-popup-review');
        if (popupReview.has(e.target).length === 0){
            popupReview.find('.list-category-review-popup').hide();
        }

        var listCategoriesPeople = $('.span-select-for-category');
        if (listCategoriesPeople.has(e.target).length === 0){
            listCategoriesPeople.find('ul').hide();
        }

        var resultSearch = $('.search-string-style');
        if(resultSearch.has(e.target).length === 0) {
            resultSearch.find('.result-search').hide();
        }

        var distance = $('.wrapper-span-beside-object-distance');
        if(distance.has(e.target).length === 0) {
            distance.find('.list-distance-for-page-category').hide();
        }

        var dropdownFilter = $('.select-sort-reservation');
        if(dropdownFilter.has(e.target).length === 0) {
            dropdownFilter.closest('li').find('.dropdown-filter-reservation').hide();
        }

        var dropDownCities = $('.filters-balans-user ul li:last-child');
        if(dropDownCities.has(e.target).length === 0) {
            dropDownCities.closest('li').find('.dropdown-filter-reservation').hide();
        }

    });

    $(window).scroll(function () {
        var currentScroll = $(window).scrollTop();
        var footer = $('#footer').offset().top - 820;
        if(parseInt(currentScroll) + 130 > footer) {
            $('.messages-phones').css({bottom: parseInt(currentScroll) - parseInt(footer) + 200 + 'px'});
        }
        else {
            $('.messages-phones').css({bottom: '70px'});
        }

    });


    function hideElements() {
        var limit = 10;
        $('.wrapper-list-objects-js').find('.wrapper-object').each(function(indx, element) {
            if(indx >= limit) {
                $(element).addClass('hide');
            }
        });
    }

    hideElements();

    $('.question-text-page .caption-question').click(function(){
        var el = $(this);
        el.next('.wrap-p').slideToggle();
    });

    var year = 2018;
    var type = 'all';

    $('.select-sort-reservation').click(function () {
        $('.dropdown-filter-reservation').hide();
        var el = $(this);
        el.closest('li').find('.dropdown-filter-reservation').show();
    });

    $('.sort-reservation-user .dropdown-filter-reservation ul li a').click(function(e) {
        e.preventDefault();
        var el = $(this);

        el.closest('.dropdown-filter-reservation').closest('li').find('dz').text(el.text());

        if(el.attr('data-year')) {
            $('input[name="year"]').val(el.attr('data-year'));
            year = el.attr('data-year');
        }

        if(el.attr('data-type')) {
            $('input[name="type"]').val(el.attr('data-type'));
            type = el.attr('data-type');
        }

        var page = $('input[name="page"]');
        page.val(2);
        $('.wrap-resevation').empty();

        $.ajax({
            type: 'post',
            url:'',
            data: {year:year, type:type, '_token': $('meta[name="csrf-token"]').attr('content')},
            beforeSend:function() {
                $('.loader-inner').removeClass('hide');
            },
            success:function (result) {
                $('.loader-inner').addClass('hide');
                $('.wrap-resevation').html(result['html']);
                if(result['showMore'] == page.val()) {
                    $('.btn-show-more-reservation').remove();
                }

                $('.count-reservation-user').text('Найдено '+ result["totalCount"] +' бронирований');
            }
        });


    });

    $('.filters-balans-user .dropdown-filter-reservation ul li a').click(function (e) {
        e.preventDefault();
        var el = $(this);
        el.closest('.dropdown-filter-reservation').hide().siblings('b').text(el.text());
        $('input[name="city"]').val(el.attr('data-id')).trigger('change');

    });


    $('body').on('click', '.menu-popup ul li a', function() {
        var el = $(this);
        $('.menu-popup ul li a').removeClass('active');
        var id = el.attr('data-tab');
        el.addClass('active');
        $('.tabs-popup ul li').removeClass('active');
        $('.tabs-popup ul li[data-tab="'+ id +'"]').addClass('active');
    });

    $('body').on('click', '.price-room-popup-block a', function() {
        var el = $(this);
        if(el.attr('data-active') == 'off') {
            el.attr('data-active', 'on');
            el.addClass('active').find('i').addClass('active').closest('.one-room-popup-block').closest('li').find('.other-content-room-popup-block').slideDown();
        }
        else {
            el.attr('data-active', 'off');
            el.removeClass('active').find('i').removeClass('active').closest('.one-room-popup-block').closest('li').find('.other-content-room-popup-block').slideUp();
        }

    });

    function wrapStartPopupPromocode() {
        var isPromocode = $.cookie('isPromocode');
        $.ajax({
            type:'get',
            url: '/check-promocode',
            success:function(result) {
                if(result == 0 && !$('.registration-popup').is(':visible') && !isPromocode && window.location.pathname != '/quiz') {
                    setTimeout(startPromocodePopup, 20000);
                }
            }
        });
    }

    wrapStartPopupPromocode();

    var isRegistration = $.cookie('isRegistration');
    var userId = $('input[name="userIdRegistration"]').val();
    if(!isRegistration && !userId && !$('.promocode-popup').is(':visible')) {
        setTimeout(startRegistrationPopup, 5000);
    }

    function startRegistrationPopup() {
        $('.registration-popup').animate({'right': 0, 'display': 'block'}, 2000);
    }


    function startPromocodePopup() {
        var overlay = $('.overlay');
        var maskHeight = $(document).height();
        var maskWidth = $(window).width();
        overlay.css({'width':maskWidth,'height':maskHeight});
        overlay.fadeIn(300, function() {
            $('.promocode-popup').fadeIn(300);

        });
    }




    $('.list-tabs-balans-user ul li a').click(function() {
        var el = $(this);
        $('.list-tabs-balans-user ul li').removeClass('active');
        el.closest('li').addClass('active');
        var tab = el.attr('data-tab');
        $('.tabs-info-user ul li').removeClass('active');
        $('.tabs-info-user ul li[data-tab="'+ tab +'"]').addClass('active');
    });


    $('.list-menu-profile-user ul li a').click(function () {
        var el = $(this);
        $('.list-menu-profile-user ul li a').removeClass('active');
        el.addClass('active');
        var block = el.attr('data-block');
        $('html,body').animate({
            scrollTop: $('.' + block).offset().top
        });
    });

    $('body').on('click', '.name-filter', function() {
        var el = $(this);
        el.closest('tr').find('input')[0].click();
    });

    $('body').on('click', '.list-images', function() {
        $($('.menu-popup a[data-tab="2"]')[0]).click();
    });


});