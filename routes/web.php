<?php

//
//Artisan::call('view:clear');
//Artisan::call('cache:clear');
//Artisan::call('config:clear');
/*

|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('oauth/{driver}', 'AuthController@redirectToProvider')->name('socialAuth');
Route::get('oauth/{driver}/callback', 'AuthController@handleProviderCallback');





Route::get('/test-pdf', function() {
//    return view('mail.certificate');
    $reservation = \App\Models\Reservation::find(54);
//    return view('mail.certificate', compact('reservation'));
    $pdf = PDF::loadView('mail.certificate', ['reservation' => $reservation])->setPaper('a4');
    return $pdf->stream('invoice.pdf');
});



Route::get('/setlocale/{locale}', function ($locale) {

    if (in_array($locale, Config::get('app.locales'))) {
        Session::put('locale', $locale);

    }

    $referer = Redirect::back()->getTargetUrl(); //URL предыдущей страницы
    $parse_url = parse_url($referer, PHP_URL_PATH); //URI предыдущей страницы

    //разбиваем на массив по разделителю
    $segments = explode('/', $parse_url);

    //Если URL (где нажали на переключение языка) содержал корректную метку языка
    if (in_array($segments[1], config('app.locales'))) {

        unset($segments[1]); //удаляем метку
    }

    //Добавляем метку языка в URL (если выбран не язык по-умолчанию)

    if ($locale != config('app.locale')){
        array_splice($segments, 1, 0, $locale);
    }

    //формируем полный URL
    $url = Request::root().implode("/", $segments);
    App::setLocale($locale);


    //если были еще GET-параметры - добавляем их
    if(parse_url($referer, PHP_URL_QUERY)){
        $url = $url.'?'. parse_url($referer, PHP_URL_QUERY);
    }
    Artisan::call('view:clear');
    Artisan::call('cache:clear');

    return redirect($url); //Перенаправляем назад на ту же страницу

})->name('setlocale');

Route::get('/interface-parser', [
    'as' => 'interface-parser',
    'uses' => 'ParserController@index'
]);

Route::get('/parser', [
    'as' => 'parser',
    'uses' => 'ParserController@parser'
]);

Route::group([
    'domain' => '{domain}.'.env('APP_URL')], function() { //hotels-ok.com
    Route::match(['get', 'post'], '/auth-object', [
            'as' => 'auth-object',
            'uses' => 'AuthController@auth_object'
        ]);

    Route::group([
    'domain' => '{domain}.'.env('APP_URL'),
    'namespace' => 'ControllersObject',
    'middleware' => 'auth', 'validateUserObject'], function() {


        Route::match(['get', 'post'], '/help', [
            'as' => 'help',
            'uses' => 'HelpController@index'
        ]);

        Route::get('/excel', [
            'as' => 'excel-download',
            'uses' => 'ReservationController@excel'
        ]);

        Route::match(['get', 'post'], '/reference', [
            'as' => 'reference',
            'uses' => 'ReferenceController@index'
        ]);

        Route::get('/delete-ticket/{id}', [
            'as' => 'ticket-delete',
            'uses' => 'HelpController@delete_ticket'
        ]);

        Route::match(['get', 'post'], '/ticket/{id}', [
            'as' => 'ticket',
            'uses' => 'HelpController@ticket'
        ]);
        ;
        Route::match(['get', 'post'], '/', [
            'as' => 'domain-home',
            'uses' => 'AccountController@index'
        ]);

        Route::match(['get', 'post'], '/information', [
            'as' => 'information-object',
            'uses' => 'ObjectController@edit',
            'middleware' => 'byInformationObject'
        ]);


        Route::post('/change-password', [
            'as' => 'change-password',
            'uses' => 'AccountController@change_password'
        ]);

        Route::get('/rooms', [
            'as' => 'rooms',
            'uses' => 'RoomController@list_rooms'
        ]);

        Route::match(['get', 'post'], '/edit-room/{id?}', [
            'as' => 'edit-room',
            'uses' => 'RoomController@edit',
            'middleware' => 'byVerificationRoom'
        ]);

        Route::get('/delete-room/{id}', [
            'as' => 'delete-room',
            'uses' => 'RoomController@delete_room'
        ]);

        Route::get('/activation-sale-room/{id}', [
            'as' => 'activation-sale-room',
            'uses' => 'RoomController@activation_sale_room'
        ]);


        Route::get('/confirmed-account', [
            'as' => 'confirmed-account',
            'uses' => 'ObjectController@confirmed_account'
        ]);

        Route::get('/calendar', [
            'as' => 'calendar',
            'uses' => 'CalendarController@index'
        ]);

        Route::post('/get-calendar-by-room', [
            'as' => 'get-calendar-by-room',
            'uses' => 'CalendarController@get_calendar_by_room',
            //'middleware' => 'byCalendar'
        ]);


        Route::post('/get-reload-reservation-by-filters', [
            'as' => 'get-reload-reservation-by-filters',
            'uses' => 'ReservationController@get_reload_reservation_by_filters'
        ]);

        Route::match(['get', 'post'], '/reservations', [
            'as' => 'reservations',
            'uses' => 'ReservationController@list_reservations'
        ]);

        Route::match(['get', 'post'], '/general-information', [
            'as' => 'general-information',
            'uses' => 'ObjectController@general_information',
            'middleware' => 'byGeneralInformationObject'
        ]);

        Route::get('/reviews', [
            'as' => 'reviews',
            'uses' => 'ReviewController@list_reviews'
        ]);

        Route::post('/get-month-info', [
            'as' => 'get-month-info',
            'uses' => 'CalendarController@get_month_info_by_room'
        ]);

        Route::post('/get-info-room-popup', [
            'as' => 'get-info-room-popup',
            'uses' => 'CalendarController@get_info_room_popup'
        ]);

        Route::match(['get', 'post'], '/get-info-reservation/{id}', [
            'as' => 'get-info-reservation-by-id',
            'uses' => 'ReservationController@get_info_reservation'
        ]);

        Route::match(['get', 'post'], '/settings', [
            'as' => 'settings',
            'uses' => 'AccountController@settings',
            'middleware' => 'bySettingsUser'
        ]);

        Route::get('/analytics', [
            'as' => 'analytics',
            'uses' => 'AnalyticsController@index'
        ]);




    });
});




Route::group(['prefix' => App\Http\Middleware\Locale::getLocale(), 'middleware' => 'clientPart'], function() { //клиентская часть -------------------------------------------------------


    Route::get('/check-promocode', [
        'uses' => 'HomepageController@checkPromocode'
    ]);

    Route::get('/quiz', [ //опросник
        'as' => 'quiz',
        'uses' => 'QuizController@quiz'
    ]);

    Route::post('/get-more-info-object', [
        'uses' => 'ObjectController@getMoreInfoObject'
    ]);

    Route::post('/subscribe', [
        'as' => 'subscribe',
        'uses' => 'HomepageController@viewHomepage'
    ]);

    Route::post('/get-ajax-rooms', [
        'as' => 'get-ajax-rooms',
        'uses' => 'ObjectController@get_ajax_rooms'
    ]);

    Route::get('/bussiness', [
        'as' => 'bussiness',
        'uses' => 'AboutCompanyController@bussiness'
    ]);

    Route::get('/groups', [
        'as' => 'groups',
        'uses' => 'AboutCompanyController@groups'
    ]);

        Route::get('/news/{category?}', [
            'as' => 'news',
            'uses' => 'NewsController@index'
        ]);

        Route::get('/news/{category}/{id}-{slug}', [
            'as' => 'one-news',
            'uses' => 'NewsController@one_news'
        ]);

        Route::get('/rewards', [
            'as' => 'rewards',
            'uses' => 'AboutCompanyController@rewards'
        ]);

        Route::match(['get', 'post'], '/hotels-world', [
            'as' => 'hotels-world',
            'uses' => 'ObjectController@hotels_world'
        ]);

        Route::get('/about-company', [
            'as' => 'about-company',
            'uses' => 'AboutCompanyController@aboutCompany'
        ]);
//
        Route::match(['get', 'post'], '/category', [
            'as' => 'category',
            'uses' => 'CategoryController@viewCategoryPage'
        ]);

        Route::get('/hotels/{id}-{nameHotel}', [
            'as' => 'hotel',
            'uses' => 'ObjectController@viewHotel'
        ])->where([
                'id' => '[0-9]+',
                'nameHotel' => '[a-zA-Z0-9-]+'
            ]);


        Route::post('/get-list-rooms-object-ajax', [
            'as' => 'get-list-rooms-object-ajax',
            'uses' => 'ObjectController@get_list_rooms_object_ajax'
        ]);

        Route::get('/reservation', [
            'as' => 'reservation',
            'uses' => 'ReservationController@reservationHotel',
            'middleware' => 'byReservation'
        ]);

        Route::post('/reservation', [
            'as' => 'reservation_store',
            'uses' => 'ReservationController@save',
        //    'middleware' => 'validateDataReservation'
        ]);


        Route::get('/guarantees', [
            'as' => 'guarantees',
            'uses' => 'AboutCompanyController@guarantees'
        ]);

        Route::get('/how-to-reservation', [
            'as' => 'how-to-reservation',
            'uses' => 'AboutCompanyController@how_to_reservation'
        ]);

        Route::get('/questions/{question?}', [
            'as' => 'questions-client',
            'uses' => 'AboutCompanyController@questions'
        ]);

        Route::post('/get-list-type-object', [
            'as' => 'get-list-type-object',
            'uses' => 'ObjectController@getListTypeObject'
        ]);


        Route::match(['get', 'post'],'/contacts', [
            'as' => 'contacts',
            'uses' => 'AboutCompanyController@call_company'
        ]);

        Route::get('/public-tender', [
            'as' => 'public-tender',
            'uses' => 'AboutCompanyController@public_tender'
        ]);

        Route::get('/confidentiality', [
            'as' => 'confidentiality',
            'uses' => 'AboutCompanyController@confidentiality'
        ]);


        Route::get('/partners', [
            'as' => 'partners',
            'uses' => 'PartnerController@partnerLending'
        ]);

        Route::post('/send-callback', [
           'as' => 'send-callback',
            'uses' => 'CallbackController@sendRequestCallback'
        ]);

    Route::post('/send-review', [
        'as' => 'send-review',
        'uses' => 'CallbackController@sendReview'
    ]);

        Route::match(['get', 'post'], '/hotels-ukraine', [
           'as' => 'hotels-ukraine',
            'uses' => 'ObjectController@hotelsUkraine'
        ]);


        Route::match(['get', 'post'], '/login', [
            'as' => 'login',
            'uses' => 'AuthController@login',
//            'loginedUser' => 'LoginedUser'
        ]);
//
        Route::match(['get', 'post'], '/registration', [
            'as' => 'registration',
            'uses' => 'AuthController@registration'
        ]);
//
//        //Роут регистрации организации
        Route::match(['get', 'post'], '/regorg', [
            'as' => 'regorg',
            'uses' => 'AuthController@registrationOrganization'
        ]);
//
        Route::get('auth/activate','AuthController@activate');


        Route::get('/feedback', [
            'as' => 'feedback',
            'uses' => 'ReviewController@feedback'
        ]);

        Route::post('/save-feedback', [
            'as' => 'save-feedback',
            'uses' => 'ReviewController@saveFeedback'
        ]);

        Route::get('/reviews', [
            'as' => 'reviews',
            'uses' => 'ReviewController@reviews'
        ]);



        Route::any('/', [
            'as' => 'homepage',
            'uses' => 'HomepageController@viewHomepage'
        ]);

        Route::get('/activate-password', [
            'as' => 'activate-password',
            'uses' => 'AuthController@activate_password'
        ]);


        Route::match(['get', 'post'],'/remember-password', [
            'as' => 'remember-password',
            'uses' => 'AuthController@remember_password'
        ]);

        Route::post('/find-object', [
            'as' => 'find-object',
            'uses' => 'ObjectController@find_object'
        ]);

        Route::post('/save-user-reservation', [
            'as' => 'save-user-reservation',
            'uses' => 'ReservationController@save_user_reservation'
        ]);

});


Route::group(['as' => 'user.', 'prefix' => 'account', 'namespace' => 'ControllersUser', 'middleware' => ['auth', 'validateUser']], function() {

    Route::get('/', [
        'as' => 'profile',
        'uses' => 'ProfileController@profile'
    ]);

    Route::post('/save-profile', [
        'as' => 'save-profile',
        'uses' => 'ProfileController@saveProfile'
    ]);

    Route::match(['get', 'post'], '/reservations', [
        'as' => 'reservations',
        'uses' => 'ReservationController@reservations'
    ]);

    Route::match(['get', 'post'],'/bonus', [
        'as' => 'bonus',
        'uses' => 'BonusController@index'
    ]);

    Route::get('/settings', [
        'as' => 'settings',
        'uses' => 'SettingController@setting'
    ]);

    Route::get('/feedback', [
        'as' => 'feedback',
        'uses' => 'FeedbackController@feedback'
    ]);

    Route::get('/promotional-offers', [
        'as' => 'offers',
        'uses' => 'ObjectController@offers'
    ]);



//    Route::match(['get', 'post'], '/', [
//        'as' => 'user-home',
//        'uses' => 'HomeController@index'
//    ]);

});

Route::post('upload-image', function(
    \Illuminate\Http\Request $request,
    Illuminate\Contracts\Validation\Factory $validator
) {
    $v = $validator->make($request->all(), [
        'upload' => 'required|image',
    ]);

    $funcNum = $request->input('CKEditorFuncNum');

    if ($v->fails()) {
        return response(
            "<script>
                window.parent.CKEDITOR.tools.callFunction({$funcNum}, '', '{$v->errors()->first()}');
            </script>"
        );
    }

    $image = $request->file('upload');
    $image->store('/images/news');
    //$url = asset('storage/uploads/'.$image->hashName());

    return response()->json([
        'uploaded' => 1,
        'fileName' => 'img',
        'url' => '/images/news/'.$image->hashName()
    ]);

    return response(
        "<script>
            window.parent.CKEDITOR.tools.callFunction({$funcNum}, '/images/news/{$image->hashName()}', 'Изображение успешно загружено');
        </script>"
    );
});



Route::group(['prefix' => 'administrator' ,'namespace' => 'ControllersAdmin', 'middleware' => ['auth', 'validateAdmin']], function(){


    Route::get('/questions', [
        'as' => 'questions',
        'uses' => 'QuestionAnswerController@listQuestions'
    ]);

    Route::match(['get', 'post'], '/edit-questions/{id?}', [
        'as' => 'edit-question',
        'uses' => 'QuestionAnswerController@editQuestion'
    ]);

    Route::get('/delete-question/{id}', [
        'as' => 'delete-question',
        'uses' => 'QuestionAnswerController@deleteQuestion'
    ]);

    Route::match(['get', 'post'], '/edit-answer/{id?}', [
        'as' => 'edit-answer',
        'uses' => 'QuestionAnswerController@editAnswer'
    ]);

    Route::get('/delete-answer/{id}', [
        'as' => 'delete-answer',
        'uses' => 'QuestionAnswerController@deleteAnswer'
    ]);

    Route::match(['get', 'post'], '/', [
        'as' => 'administrator-home',
        'uses' => 'AdminController@index'
    ]);

    Route::get('/users', [
        'as' => 'users',
        'uses' => 'UserController@users'
    ]);


    Route::get('/user-roles', [
        'as' => 'user-roles',
        'use' => 'UserController@users'
    ]);

    Route::post('/save-object-errors', [
        'as' => 'save-object-errors',
        'uses' => 'ObjectController@save_errors'
    ]);

    Route::get('/discounts', [
        'as' => 'discounts',
        'uses' => 'DiscountController@discounts'
    ]);

    Route::match(['get', 'post'], '/edit-discount/{id?}', [
        'as' => 'edit-discount',
        'uses' => 'DiscountController@editDiscount'
    ]);

    Route::post('/delete-discount/{id}', [
        'as' => 'delete-discount',
        'uses' => 'DiscountController@deleteDiscount'
    ]);

    Route::get('/news', [
        'as' => 'news-admin',
        'uses' => 'NewsController@news'
    ]);

    Route::match(['get', 'post'], '/edit-news/{id?}', [
        'as' => 'edit-news-admin',
        'uses' => 'NewsController@edit_news'
    ]);

    Route::get('/reviews-project', [
        'as' => 'reviews-project',
        'uses' => 'ReviewSiteController@reviews_project'
    ]);

    Route::match(['get', 'post'], '/add-review-project/{id?}', [
        'as' => 'add-review-project',
        'uses' => 'ReviewSiteController@addReviewProject'
    ]);

    Route::post('/delete-review-project/{id}', [
        'as' => 'delete-review-project',
        'uses' => 'ReviewSiteController@deleteReviewProject'
    ]);


    Route::get('/wishes', [
        'as' => 'wishes',
        'uses' => 'WishesController@wishes'
    ]);


    Route::get('/delete-news/{id}', [
        'as' => 'delete-news',
        'uses' => 'NewsController@delete_news'
    ]);

    Route::get('/pages/', [
        'as' => 'pages',
        'uses' => 'PageController@page'
    ]);

    Route::match(['get', 'post'], '/edit-page/{id?}', [
        'as' => 'edit-page',
        'uses' => 'PageController@edit_page'
    ]);

    Route::match(['get', 'post'], '/delete-page/{id?}', [
        'as' => 'delete-page',
        'uses' => 'PageController@delete_page'
    ]);


    Route::get('/news-category', [
        'as' => 'news-category-admin',
        'uses' => 'NewsController@news_category'
    ]);

    Route::match(['get', 'post'], '/edit-category-news/{id?}',  [
        'as' => 'edit-category-news',
        'uses' => 'NewsController@edit_category_news'
    ]);

    Route::get('/delete-category-news/{id}', [
        'as' => 'delete-category-news',
        'uses' => 'NewsController@delete_category_news'
    ]);

    Route::match(['get', 'post'], '/application-object', [
        'as' => 'application-object',
        'uses' => 'ObjectController@application_object'
    ]);

    Route::post('/save-application-object', [
        'as' => 'save-application-object',
        'uses' => 'ObjectController@save_application_object'
    ]);

    Route::get('/check-object', [
        'as' => 'check-object',
        'uses' => 'ObjectController@check_object'
    ]);

    Route::match(['get', 'post'], '/checked-object-id/{id}', [
        'as' => 'checked-object-id',
        'uses' => 'ObjectController@checked_object_id'
    ]);

    Route::get('/confirmed-object/{id}', [
        'as' => 'confirmed-object-admin',
        'uses' => 'ObjectController@confirmed_object'
    ]);

    Route::get('/helps', [
        'as' => 'help-admin',
        'uses' => 'HelpController@index'
    ]);

    Route::match(['get', 'post'], '/ticket/{id}', [
        'as' => 'ticket-admin',
        'uses' => 'HelpController@ticket'
    ]);


    Route::match(['get', 'post'], '/reservation-admin', [
        'as' => 'reservation-admin',
        'uses' => 'ReservationController@index'
    ]);

    Route::match(['get', 'post'], '/reservation-edit/', [
        'as' => 'reservation-edit',
        'uses' => 'ReservationController@edit'
    ]);

    Route::post('/get-objects', [
        'as' => 'get-objects',
        'uses' => 'ObjectController@getObjects'
    ]);

    Route::post('/create-reservation-by-object', [
        'as' => 'create-reservation-by-object',
        'uses' => 'RoomController@create_reservation_by_object'
    ]);


    Route::post('/get-load-client-by-telefon', [
        'as' => 'get-load-client-by-telefon',
        'uses' => 'UserController@get_load_client_by_telefon'
    ]);

    Route::post('/get-info-room-price', [
        'as' => 'get-info-room-price',
        'uses' => 'ReservationController@get_info_room_price'
    ]);

    Route::post('/save-full-reservation', [
        'as' => 'save-full-reservation',
        'uses' => 'ReservationController@save_full_reservation'
    ]);

    Route::get('/change-status-reservation/{id}/{statusId}', [
        'as' => 'change-status-reservation',
        'uses' => 'ReservationController@change_status_reservation'
    ]);

    Route::post('/delete-room-from-reservation', [
        'as' => 'delete-room-from-reservation',
        'uses' => 'ReservationController@delete_room_from_reservation'
    ]);

    Route::get('/delete-reservation-id/{id}', [
        'as' => 'delete-reservation-id',
        'uses' => 'ReservationController@delete_reservation'
    ]);

    Route::post('/get-list-rooms-by-object', [
        'as' => 'get-list-rooms-by-object',
        'uses' => 'RoomController@get_list_rooms_by_object'
    ]);

    Route::match(['get', 'post'], '/info-by-reservation/{id}', [
        'as' => 'info-by-reservation',
        'uses' => 'ReservationController@info_reservation'
    ]);



    ///

    /* Роуты для работы с плюсами обьекта */

        Route::get('/services-object', [
            'as' => 'list-service-object',
            'uses' => 'AdminController@list_services'
        ]);

        Route::match(['get', 'post'], '/edit-service/{id?}', [
            'as' => 'edit-service',
            'uses' => 'AdminController@edit_service'
        ]);

        Route::match(['get', 'post'], '/delete_service/{id}', [
            'as' => 'delete-service',
            'uses' => 'AdminController@delete_service'
        ]);

/* Роуты для работы с ближайщими обьектами */

        Route::get('/list-objects-beside', [
            'as' => 'list-objects-beside',
            'uses' => 'AdminController@list_objects_beside'
        ]);

        Route::match(['get', 'post'], '/edit-object-beside/{id?}', [
            'as' => 'edit-object-beside',
            'uses' => 'AdminController@edit_object_beside'
        ]);

        Route::match(['get', 'post'], '/delete-object-beside/{id}', [
            'as' => 'delete-object-beside',
            'uses' => 'AdminController@delete_object_beside'
        ]);

/* Роуты для работы с городами системы */

        Route::get('/list-cities', [
            'as' => 'list-cities',
            'uses' => 'CityController@list_cities'
        ]);

        Route::match(['get', 'post'], '/edit-city/{id?}', [
            'as' => 'edit-city',
            'uses' => 'CityController@edit_city'
        ]);

        Route::get('/delete-city/{id}', [
            'as' => 'delete-city',
            'uses' => 'CityController@delete_city'
        ]);

/* Роуты для работы со странами */

        Route::get('/list-countries', [
            'as' => 'list-countries',
            'uses' => 'CountryController@list_countries'
        ]);

        Route::match(['get', 'post'], '/edit-country/{id?}', [
            'as' => 'edit-country',
            'uses' => 'CountryController@edit_country'
        ]);

        Route::get('/delete-country/{id}', [
            'as' => 'delete-country',
            'uses' => 'CountryController@delete_country'
        ]);

/* Роуты для работы с регионами */

        Route::get('/list-regions', [
            'as' => 'list-regions',
            'uses' => 'RegionController@list_regions'
        ]);

        Route::match(['get', 'post'], '/edit-region/{id?}', [
            'as' => 'edit-region',
            'uses' => 'RegionController@edit_region'
        ]);

        Route::get('/delete-region/{id}', [
            'as' => 'delete-region',
            'uses' => 'RegionController@delete_region'
        ]);

/* Роуты для работы со связями категорий плюсов */

        Route::get('/category-pluses-room', [
            'as' => 'category-pluses-room',
            'uses' => 'CategoryPlusesController@list_category_pluses'
        ]);

        Route::match(['get', 'post'], '/edit-category-pluses/{id?}', [
            'as' => 'edit-category-pluses',
            'uses' => 'CategoryPlusesController@edit_category_pluses'
        ]);

        Route::get('/delete-category-pluses/{id}', [
            'as' => 'delete-category-pluses',
            'uses' => 'CategoryPlusesController@delete_category_pluses'
        ]);

/* Роуты для работы с плюсами комнаты */

        Route::get('/list-pluses-room', [
            'as' => 'list-pluses-room',
            'uses' => 'PlusesRoomController@list_pluses_room'
        ]);

        Route::match(['get', 'post'], 'edit-pluse-room/{id?}', [
            'as' => 'edit-pluse-room',
            'uses' => 'PlusesRoomController@edit_pluse_room'
        ]);

        Route::get('/delete-pluse-room/{id}', [
            'as' => 'delete-pluse-room',
            'uses' => 'PlusesRoomController@delete_pluse_room'
        ]);

/* Роуты для работы с обратными звонками */

        Route::get('/list-callbacks', [
            'as' => 'list-callbacks',
            'uses' => 'CallbackController@list_callbacks'
        ]);

    Route::match(['get', 'post'], '/edit-callback/{id}', [
        'as' => 'edit-callback',
        'uses' => 'CallbackController@edit_callback'
    ]);

        Route::post('/delete-callback/{id}', [
            'as' => 'delete-callback',
            'uses' => 'CallbackController@delete_callback'
        ]);

        Route::match(['get', 'post'], '/search-object', [
            'as' => 'search-object',
            'uses' => 'ReservationController@searchObject'
        ]);

        Route::post('/search-object-for-reservation', [
            'as' => 'search-object-for-reservation',
            'uses' => 'ReservationController@search_object_for_reservation'
        ]);


        Route::post('/search-object-by-filters', [
            'as' => 'search-object-by-filters',
            'uses' => 'ReservationController@search_object_by_filters'
        ]);


        Route::post('/change-status-reservation-past', [
            'as' => 'change-status-reservation-past',
            'uses' => 'ReservationController@change_status_reservation_past'
        ]);

        Route::get('/promocodes', [
            'as' => 'promocodes',
            'uses' => 'DiscountController@promocodes'
        ]);

        Route::match(['get', 'post'], '/edit-promocode/{id?}', [
            'as' => 'edit-promocode',
            'uses' => 'DiscountController@editPromocode'
        ]);

        Route::post('/delete-promocode/{id}', [
            'as' => 'delete-promocode',
            'uses' => 'DiscountController@deletePromocode'
        ]);


});


Route::post('/upload-images', [
    'as' => 'upload-images',
    'uses' => 'ImageController@upload_images'
]);

Route::get('/delete-image-object', [
    'as' => 'delete-img-object',
    'uses' => 'ImageController@delete_img_object'
]);

Route::get('/logout', [
    'as' => 'logout',
    'uses' => 'AuthController@logout'
]);

Route::post('/load-city-region', [
    'as' => 'load-city-region',
    'uses' => 'CityController@load_city_region'
]);

Route::post('/add-application', [
    'as' => 'add-application',
    'uses' => 'CallbackController@add_application'
]);

Route::post('/add-application-group', [
    'as' => 'add-application-group',
    'uses' => 'CallbackController@add_application_group'
]);

Route::post('/hash', function (\Illuminate\Http\Request $request) {
    return base64_encode($request->arrayRoom);
});

