<?php

use Illuminate\Database\Seeder;
use App\Models\ObjectHotel;
use App\Models\City;
use App\Models\EventCalendar;
use App\Models\Room;
use Illuminate\Support\Facades\DB;
class DatabaseSeeder extends Seeder
{
    use \App\Traits\timeTraits;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        $objects = ObjectHotel::all();
//        foreach($objects as $object) {
//            $parentCityId = City::find($object->cityId)->parentId;
//            if($parentCityId) {
//                $object->parentCityId = $parentCityId;
//            }
//            else {
//                $object->parentCityId = $object->cityId;
//            }
//
//            $object->save();
//        }
//
//        dd('success');

//        $eventCalendars = EventCalendar::where('regionId', 0)->get();
//        foreach($eventCalendars as $calendar) {
//            $room = Room::find($calendar->typeRoomId);
//            $object = ObjectHotel::find($room->objectId);
//            $calendar->cityIdDaugher = $object->cityId;
//            $city = City::find($object->cityId);
//            $calendar->regionId =  $city->regionId;
//            $calendar->objectCategoryId = $object->objectCategoryId;
//            $calendar->save();
//        }


//        $cities = DB::connection('test')->table('cities')->select('*')->get();
//        foreach ($cities as $city) {
//            $currentCity = City::find($city->id);
//            $currentCity->image = $city->img;
//            $currentCity->save();
//        }
//
//        dd('success');


        //php artisan db:seed --class=DatabaseSeeder


        $dateFrom = '13.09.2018';
        $dateTo = '31.10.2018';
        $countRoom = 3;
        $room = new Room;
        $eventCalendar = new EventCalendar;
        echo date('d.m.Y H:s', time());
        $listObjects = DB::table('objects')->select('id', 'name')->get();
        foreach($listObjects as $object) {
            $listRooms = DB::table('rooms')->select('id', 'price', 'amountPerson')->where('objectId', $object->id)->get();
            foreach($listRooms as $roomCurrent) {
//                dd($roomCurrent);
                $roomId = $roomCurrent->id;
                $dateArray = $this->getDateTimeStartStopByDays($dateFrom, $dateTo);
                array_pop($dateArray);
                foreach($dateArray as $key => $date) {
//                    $countRoomReservation = $eventCalendar->getCountReservationRoomByDate($key, $roomId);
//                    $countFreeRoom = $countRoom - $countRoomReservation->countRoom;

//                    if($countFreeRoom && $countFreeRoom >= $countRoom) {
                        DB::beginTransaction();
                        try {
                            //$listRoomsByTypeRoom = $room->getListRoomsByTypeRoomId($roomId, $countRoom, $key);
//                            $eventCalendar->removeRowFromCalendar($key, $roomId); //тут доделать


                            $object = ObjectHotel::find($object->id);
                            $cityIdDaugher = $object->cityId;
                            $city = City::find($object->cityId);
                            $amountPersonRoom = $roomCurrent->amountPerson;
                            $regionId = $city->regionId;

                            if($object->parentCityId) {
                                $parentCityId = $object->parentCityId;
                            }
                            else {
                                $parentCityId = $object->cityId;
                            }
                            $objectCategoryId = $object->objectCategoryId;


                            $result = $eventCalendar->createEventByRoom($roomId, $key, $roomCurrent->price, 0, 0, [],
                                $cityIdDaugher, $regionId, $objectCategoryId, $parentCityId, $object->id, $amountPersonRoom, $countRoom);

                        }
                        catch(QueryException $e) {
                            DB::rollBack();
                            $errors = true;
                        }

                        DB::commit();
//                    }

                }
            }
            echo $object->id.'<br>';
        }
        echo date('d.m.Y H:s', time());
        echo 'Все получилось';


//        $eventCalendars = EventCalendar::where('id', '>=', 1800000)->get();
//        foreach ($eventCalendars as $calendar) {
//            $amountPersonRoom = Room::find($calendar->typeRoomId)->amountPerson;
//            $calendar->amountPersonRoom = $amountPersonRoom;
//            $calendar->save();
//
//        }

        echo 'success';



    }
}
