<?php

use Illuminate\Database\Seeder;
use App\Models\ObjectHotel;
use App\Models\Room;
use Illuminate\Support\Facades\Storage;
class PhotosSeeder extends Seeder
{
    use \App\Traits\croppedPhotos;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        $objects = ObjectHotel::first();
//        foreach ($objects as $object) {
//            $path = '/images/hotels/'.$object->id.'-'.$object->subdomain.'/';
//            $images = Storage::allFiles($path);
//            foreach ($images as $image) {
//                $hzBred = explode('/', $image);
//                $img = end($hzBred);
//                $bredImg = explode('.', $img);
//                $nameImg = $bredImg[0];
//                $type = $bredImg[1];
//                $pathImages = preg_replace('/'.$img.'/', '', $image);
//
//                $category = 'images-objects';
//                if(strpos($image, 'images-rooms') !== false) {
//                    $category = 'images-rooms';
//                }
//
//                $this->cropPhoto('/'.$pathImages, $img, $nameImg, $type, $category);
//            }
//
//            echo $object->id;
//        }

        $objects = ObjectHotel::all();
        foreach ($objects as $object) {
            $path = '/images/hotels/'.$object->id.'-'.$object->subdomain.'/';
            $images = Storage::allFiles($path);
            foreach ($images as $image) {
                $hzBred = explode('/', $image);
                $img = end($hzBred);
                $bredImg = explode('.', $img);
                $nameImg = $bredImg[0];
                $type = $bredImg[1];
                $pathImages = preg_replace('/'.$img.'/', '', $image);

                $category = 'images-objects';
                if(strpos($image, 'images-rooms') !== false) {
                    $category = 'images-rooms';
                }

                $this->cropPhoto('/'.$pathImages, $img, $nameImg, $type, $category);
            }

            echo $object->id;
        }
    }
}
