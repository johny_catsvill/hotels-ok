<?php
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 23.07.2017
 * Time: 7:42
 */

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use DB;
use App\Helpers\Validate;
use Exception;

class EventCalendar extends Model
{
    protected $table = 'event_calendar';

    protected $fillable = ['*'];

    public $timestamps = false;


    public function createEventByRoom($typeRoomId, $date, $price, $closeRoom, $discount = 0, $dayWeek,
                                      $cityIdDaugher = false, $regionId = false, $objectCategoryId = false, $parentCityId = false, $objectId = false, $amountPersonRoom = false, $countRoom) { //нужно доделать для пользователей являющимися обьктом

        $arrayRow = [];
        
        if(!$typeRoomId && !Validate::unsignedInt($typeRoomId)) throw new exception('25');
        if(!$date && !Validate::unsignedInt(strtotime($date))) throw new exception('27');
        if(!$price && !Validate::unsignedFloat($price)) throw new exception('28');
        if($discount && !Validate::unsignedFloat($discount)) throw new exception('29');
        if(!Validate::logic($closeRoom)) throw new exception('30');
        if($dayWeek && !is_array($dayWeek)) throw new exception('Invalid array');
          
        $dateArray = explode('-', $date);
        $newDay = $dateArray[2];
        $newMonth = $dateArray[1];
        $newYear = $dateArray[0];

        //$week = ["пн","вт","ср","чт","пт","сб", "вс"];
        $dayWeekNum = date("w", mktime(0, 0, 0, $newMonth, $newDay, $newYear));

        if($dayWeek && !in_array($dayWeekNum, $dayWeek)) return false;
        $dayWeekName = '';
        switch($dayWeekNum)
        {
            case  0:
                $dayWeekName = 'Воскресенье';
                break;
            case  1:
                $dayWeekName = 'Понедельник';
                break;
            case  2:
                $dayWeekName = 'Вторник';
                break;
            case  3:
                $dayWeekName = 'Среда';
                break;
            case  4:
                $dayWeekName = 'Четверг';
                break;
            case  5:
                $dayWeekName = 'Пятница';
                break;
            case  6:
                $dayWeekName = 'Суббота';
                break;
        }

        for($i = 0; $i < $countRoom; $i++) {
            $currentDay = [
                'date' => strtotime($date),
                'price' => $price,
                'day' => $newDay,
                'month' => $newMonth,
                'year' => $newYear,
                'dayWeekNum' => $dayWeekNum,
                'dayWeekName' => $dayWeekName,
                'closeRoom' => $closeRoom,
                'typeRoomId' => $typeRoomId,
                'valuePercent' => $discount,
                'cityIdDaugher' => $cityIdDaugher,
                'regionId' => $regionId,
                'objectCategoryId' => $objectCategoryId,
                'cityId' => $parentCityId,
                'objectId' => $objectId,
                'amountPersonRoom' => $amountPersonRoom
            ];
            array_push($arrayRow, $currentDay);
        }
        
        
        $result = DB::table('event_calendar')->insert($arrayRow);
    }
    
    public function getListInfoByRoomIdAndMonth($roomId, $month, $year) {
        
        $listInfoRooms = DB::table('event_calendar as ec')->select('ec.price', 'ec.day', 'ec.date', 'ec.closeRoom', DB::raw('COUNT(ec.date) as countRoom'), 'ec.valuePercent',
            DB::raw('(SELECT COUNT(e.reservationId) FROM event_calendar as e where e.date = ec.date AND e.reservationId != 0) as countReservation'))
            ->where('ec.typeRoomId', $roomId)
            ->where('ec.month', $month)
            ->where('ec.year', $year)
            ->groupBy('ec.date')
            ->get();

        return $listInfoRooms;
    }
    
    public function getInfoRoomForCalendarPopup($roomId, $dateUnix) {
        
        if(!Validate::unsignedInt($roomId)) throw new Exception('Неккоректный id');
        if(!Validate::unsignedInt($dateUnix)) throw new Exception('Неккоректная дата');
        
        $roomInfo = DB::table('event_calendar as ec')->select('ec.price', 'ec.typeDiscounts', 'ec.valuePercent', 'ec.closeRoom', DB::raw('count(ec.id) as countRoom'))
            ->where('ec.typeRoomId', $roomId)
            ->where('ec.date', $dateUnix)
            ->where('ec.reservationId', 0)
            ->get()
            ->first();
        return $roomInfo;
        
    }


    public function getCountReservationRoomByDate($date, $roomId) {
        
        if(!Validate::unsignedInt($roomId)) throw new Exception('Неккоректный id');
        if(!Validate::unsignedInt(strtotime($date))) throw new Exception('Неккоректная дата');
        
        
        $countRoom = DB::table('event_calendar')
            ->select(DB::raw('COUNT(id) as countRoom'))
            ->where('date', strtotime($date))
            ->whereNotNull('reservationId')
            ->where('reservationId', '<>', 0)
            ->first();
        return $countRoom;
        
    }
    
    public function removeRowFromCalendar($date, $roomId) {
        
        if(!Validate::unsignedInt($roomId)) throw new Exception('Неккоректный id');
        if(!Validate::unsignedInt(strtotime($date))) throw new Exception('Неккоректная дата');
        
        DB::table('event_calendar')
            ->where('date', strtotime($date))
            ->where('typeRoomId', $roomId)
            ->where('reservationId', 0)
            ->delete();
    }
    
    
    public function updateSaleByRoomId($roomId, $bool) {
        if(!Validate::unsignedInt($roomId)) throw new Exception('Неккоректный id');
        if(!Validate::logic($bool)) throw new Exception('Неккоректное значение');
        
        DB::table('event_calendar')
            ->where('typeRoomId', $roomId)
            ->where('reservationId', 0)
            ->update([
                'closeRoom' => $bool
            ]);
        
    }
    
    
    
    public function removeRoomOnTheDate($dateArray, $roomId) {
        DB::table('event_calendar')->where('reservationId', 0)->whereIn('date', $dateArray)->where('typeRoomId', $roomId)->delete();
    }

    public function getListPriceAndCountObject($dateFromStr, $guest = 1, $city = false, $region = false,
                                               $starsArray = false, $foodArray = false, $servicesArray = false, $plusesArray = false, $typeObjectArray = false) {

        $sqlTableString = 'event_calendar AS ec, objects';
        $sqlGuest = ' and ec.amountPersonRoom >= '. $guest;

        $sqlCity = '';
        if($city) {
            $sqlCity = ' and ec.cityIdDaugher = '. $city->id;
        }

        $sqlRegion = '';
        if($region) {
                $sqlRegion = ' and ec.regionId = '. $region->id;

        }

        $sqlStars = '';
        if($starsArray) {
            $sqlStars = ' and objects.stars in ('. implode(",", $starsArray) .')';
        }

        $sqlFood = '';
        if($foodArray) {
            $sqlTableString.= ', food_types_relation_object';
            $sqlFood = ' and food_types_relation_object.objectId = objects.id and food_types_relation_object.foodTypeId in ('. implode(",", $foodArray) .') ';
        }

        $sqlServices = '';
        if($servicesArray) {
            $sqlTableString.= ', objects_relation_objects_servises';
            $sqlServices = ' and objects_relation_objects_servises.objectId = objects.id and objects_relation_objects_servises.objectServiceId in ('. implode(",", $servicesArray) .') ';
        }

        $sqlPluses = '';
        if($plusesArray) {
            $sqlTableString.= ', objects_objects_types_pluses';
            $sqlPluses = ' and objects_objects_types_pluses.objectId = objects.id and objects_objects_types_pluses.objectTypePlusesId in ('. implode(",", $plusesArray) .')';
        }

        $sqlTypeObject = '';
        if($typeObjectArray) {
            $sqlTypeObject = ' and objects.objectCategoryId in ('. implode(",", $typeObjectArray) .') ';
        }

        $sql = 'SELECT COUNT(table1.objectId) as countObject, table1.rangePrice FROM (SELECT ec.objectId AS objectId, CASE
                    WHEN ec.price > 0 AND ec.price <= 200 THEN 1
                    WHEN ec.price > 200 AND ec.price <= 400  THEN 2
                    WHEN ec.price > 400 AND ec.price <= 600  THEN 3
                    WHEN ec.price > 600 AND ec.price <= 800  THEN 4
                    WHEN ec.price > 800 AND ec.price <= 1000  THEN 5
                    ELSE 6
                    END AS rangePrice 
                    FROM '. $sqlTableString .'  WHERE objects.id = ec.objectId and ec.date = '. $dateFromStr . $sqlGuest . $sqlCity. $sqlRegion . $sqlStars . $sqlFood . $sqlServices . $sqlPluses. $sqlTypeObject .' 
                    and ec.reservedRoomId = 0 and ec.reservationId = 0 and ec.closeRoom = 0 
                    GROUP BY ec.objectId) AS table1 GROUP BY table1.rangePrice';

        return DB::select($sql);
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    


}