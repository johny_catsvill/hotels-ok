<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Callback extends Model
{
    protected $table = 'callback_telefons';
    public $timestamps = false;

    public function setCallbackUser($telefonUser) {
        $insertTelefon = DB::table('callback_telefons')
            ->insert(
                [
                    'deleted' => 0,
                    'telefon' => $telefonUser,
                    'creationDate' => time()
                ]
            );
        return $insertTelefon;
    }
    
    public function getListCallbacks() {
        
        $listCallbacks = DB::table('callback_telefons')->select('id', 'telefon', 'status')->where('deleted', 0)->get();
        return $listCallbacks;
        
    }

    public function saveReviewUser($data) {
        DB::table('callback_reviews')->insert([
            'creationDate' => time(),
            'name' => $data['name'],
            'email' => $data['email'],
            'review' => $data['review'],
            'type' => $data['type'],
            'url' => $data['url'],
        ]);
    }
    
}
