<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Star extends Model
{
    protected $table = 'star_objects';

    public function objects() {
        return $this->hasMany('App\Models\ObjectHotel', 'stars', 'stars');
    }

    public function getListStarsAndCountObject($city = false, $region = false, $dateFromStr, $guest = 1, $search = false, $minPrice = false, $maxPrice = false, $typeObjectArray = false,
                                               $foodArray = false, $servicesArray = false, $plusesArray = false) {

        $sqlTable =  'event_calendar, objects';

        $sqlCity = '';
        if($city) {
            $sqlCity = ' and event_calendar.cityIdDaugher = '. $city->id;
        }

        $sqlRegion = '';
        if($region) {
            $sqlRegion = ' and event_calendar.regionId = '. $region->id;
        }

        $sqlGuest = ' and event_calendar.amountPersonRoom = '. $guest;

        $sqlTypeObject = '';
        if($typeObjectArray) {
            $sqlTypeObject = ' and objects.objectCategoryId in( '. implode(",", $typeObjectArray) . ')';
        }

        $sqlMinPrice = '';
        if($minPrice) {
            $sqlMinPrice = ' and event_calendar.price >= '. $minPrice;
        }

        $sqlMaxPrice = '';
        if($maxPrice) {
            $sqlMaxPrice = ' and event_calendar.price <= '. $maxPrice;
        }

        $sqlFood = '';
        if($foodArray) {
            $sqlTable.= ', food_types_relation_object';
            $sqlFood = ' and food_types_relation_object.objectId = objects.id and food_types_relation_object.foodTypeId in ('. implode(",", $foodArray) .')';
        }

        $sqlServices = '';
        if($servicesArray) {
            $sqlTable.= ', objects_relation_objects_servises';
            $sqlServices = ' and objects_relation_objects_servises.objectId = objects.id and objects_relation_objects_servises.objectServiceId in ('. implode(",", $servicesArray) .')';
        }

        $sqlPluses = '';
        if($plusesArray) {
            $sqlTable.= ', objects_objects_types_pluses';
            $sqlPluses = ' and objects_objects_types_pluses.objectId = objects.id and objects_objects_types_pluses.objectTypePlusesId in ('. implode(",", $plusesArray  ) .')';
        }

        $sql = 'SELECT star_objects.`stars`, 
                        (SELECT COUNT(DISTINCT objects.id)
                        FROM
                          '. $sqlTable .'
                        WHERE event_calendar.date = '. $dateFromStr .' 
                          AND event_calendar.`amountPersonRoom` >= '. $guest .' AND event_calendar.`objectId` = objects.id
                          '. $sqlCity . $sqlRegion . $sqlTypeObject . $sqlMinPrice . $sqlMaxPrice . $sqlFood . $sqlServices . $sqlPluses .'
                          AND `event_calendar`.`closeRoom` = 0 AND `event_calendar`.`reservedRoomId` = 0 AND `event_calendar`.`reservationId` = 0
                          AND objects.`stars` = star_objects.`stars`
                           LIMIT 1) AS objects_count
                        FROM star_objects order by star_objects.stars desc';

        return DB::select($sql);
    }

}
