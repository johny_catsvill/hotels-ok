<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Clients extends Model
{
    protected $table = 'clients';
    protected $guarded = [];
    public $timestamps = false;
    
    public function user()
    {
//        return $this->hasOne('App\Models\User', 'clientId', 'id');
        // первый параметр это мы указываем с какой таблицей делаем обратную связь
        // вторым параметром указывается локальный ключ текущей модели а тоесть модели Clients
        // третьим параметром указывается внешний ключ с которой идет связь а то есть с таблицей user
        return $this->belongsTo('App\Models\User', 'id', 'clientId');
    }
    
    
    public function getTelefonByValue($telefon) {
        $client = DB::table('clients as c')->select('u.id', 'c.phone', 'c.name', 'c.lastName', 'c.middleName', 'u.email')
            ->join('users as u', 'u.clientId', '=', 'c.id')
            ->where('c.phone', $telefon)->whereNull('u.objectId')->first();
        return $client;
    }
    
    
}
