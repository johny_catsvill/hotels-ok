<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PlusesCategory extends Model
{
    protected $table = 'rooms_categories_pluses';

    public function pluses() {
        return $this->hasMany('App\Models\PlusesRoom', 'roomCategoryId', 'id');
    }

}
