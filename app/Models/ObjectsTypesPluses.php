<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Helpers\ValidateQuery;
use App\Helpers\Validate;
use Exception;

class ObjectsTypesPluses extends Model
{
    protected $table = 'objects_types_pluses';
    protected $fillable = [
        'name', 'class'
    ];
    public $timestamps = false;

    public function objectTypesPlusesRelation() {
        return $this->belongsToMany('App\Models\ObjectHotel', 'objects_objects_types_pluses', 'objectTypePlusesId' ,'objectId');
    }


    public function getListPlusesAndCountObject($city = false, $region = false, $dateFromStr, $guest = 1, $search = false, $starsArray = false, $minPrice = false, $maxPrice = false,
                                                $typeObjectArray = false, $foodArray = false, $servicesArray = false) {

        $sqlTable =  'event_calendar, objects, objects_objects_types_pluses';

        $sqlCity = '';
        if($city) {
            $sqlCity = ' and event_calendar.cityIdDaugher = '. $city->id;
        }

        $sqlRegion = '';
        if($region) {
            $sqlRegion = ' and event_calendar.regionId = '. $region->id;
        }

        $sqlGuest = ' and event_calendar.amountPersonRoom = '. $guest;

        $sqlTypeObject = '';
        if($typeObjectArray) {
            $sqlTypeObject = ' and event_calendar.objectCategoryId in( '. implode(",", $typeObjectArray) . ')';
        }

        $sqlMinPrice = '';
        if($minPrice) {
            $sqlMinPrice = ' and event_calendar.price >= '. $minPrice;
        }

        $sqlMaxPrice = '';
        if($maxPrice) {
            $sqlMaxPrice = ' and event_calendar.price <= '. $maxPrice;
        }

        $sqlFood = '';
        if($foodArray) {
            $sqlTable.= ', food_types_relation_object';
            $sqlFood = ' and food_types_relation_object.objectId = objects.id and food_types_relation_object.foodTypeId in ('. implode(",", $foodArray) .')';
        }

        $sqlServices = '';
        if($servicesArray) {
            $sqlTable.= ', objects_relation_objects_servises';
            $sqlServices = ' and objects_relation_objects_servises.objectId = objects.id and objects_relation_objects_servises.objectServiceId in ('. implode(",", $servicesArray) .')';
        }

        $sqlStars = '';
        if($starsArray) {
            $sqlStars = ' and objects.stars in ('. implode(",", $starsArray) .')';
        }


        $sql = 'SELECT objects_types_pluses.name, objects_types_pluses.id, 
                        (SELECT COUNT(DISTINCT objects.id)
                        FROM
                          '. $sqlTable .'
                        WHERE event_calendar.date = '. $dateFromStr .' 
                          AND event_calendar.`amountPersonRoom` >= '. $guest .' AND event_calendar.`objectId` = objects.id
                          '. $sqlCity. $sqlRegion . $sqlTypeObject . $sqlMinPrice . $sqlMaxPrice . $sqlFood . $sqlServices . $sqlStars .'
                          AND `event_calendar`.`closeRoom` = 0 AND `event_calendar`.`reservedRoomId` = 0 AND `event_calendar`.`reservationId` = 0
                          AND objects.id = objects_objects_types_pluses.objectId 
                          and objects_objects_types_pluses.objectTypePlusesId = objects_types_pluses.id 
                           LIMIT 1) AS objects_count
                        FROM objects_types_pluses';

        return DB::select($sql);
    }


    
    
    
}
