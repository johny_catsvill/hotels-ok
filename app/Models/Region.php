<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Region extends Model
{
    
    protected $guarded = [];
    public $timestamps = false;

    public function country() {
        return $this->belongsTo('App\Models\Country', 'countryId', 'id');
    }
    
    
    public function listRegions() {
        
        $listRegions = DB::table('regions as r')->select('r.id', 'r.name', 'r.url', 'c.name as countryName')->join('countries as c', 'r.countryId', '=', 'c.id')->get();
        return $listRegions;
    }

    public function cities() {
        return $this->hasMany('App\Models\City', 'regionId', 'id');
    }
    
    
}
