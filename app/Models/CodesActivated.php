<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CodesActivated extends Model
{
    protected $table = 'codes_activated';
    protected $fillable = ['userId', 'code'];
    public $timestamps = false;
}
