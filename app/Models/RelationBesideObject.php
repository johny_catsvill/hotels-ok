<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RelationBesideObject extends Model
{
    
    protected $table = 'objects_beside';
    protected $guarded = [];
    public $timestamps = false;
    
    
}
