<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class City extends Model
{

    protected $table = 'cities';
    protected $guarded = [];
    public $timestamps = false;


    public function region() {
        return $this->belongsTo('App\Models\Region','regionId');
    }

    public function objects() {
        return $this->belongsTo('App\Models\ObjectHotel', 'id', 'cityId');
    }

    public function objectsCities() {
        return $this->hasMany('App\Models\ObjectHotel', 'parentCityId', 'id');
    }

    public function calendarsPrice() {
        return $this->belongsTo('App\Models\EventCalendar', 'id', 'cityIdDaugher');
    }



    public function children()
    {
        return $this->hasMany('App\Models\City', 'parentId');
    }

    public function getListCitiesUkraine($type) {

        $sqlType = '';

        switch($type) {
            case 'hotels':
                $sqlType = ' AND objects.`objectCategoryId` NOT IN (18,16) ';
                break;
            case 'hostel':
                $sqlType = ' AND objects.objectCategoryId = 16 ';
                break;
            case 'apartment':
                $sqlType = ' And objects.objectCategoryId = 18 ';
                break;
        }

        $sql = 'SELECT COUNT(*) as objects_count, cities.id, cities.`name`, cities.countryId, cities.image 
                FROM objects, cities
                 WHERE objects.parentCityId = cities.id AND cities.parentId 
                 IS NULL '. $sqlType .' 
                 GROUP BY objects.parentCityId order by cities.name';
        $hotels = DB::select($sql);
        return $hotels;
    }

    public function getListCitiesCategory($category) {

        $sqlCategory = '';

        switch($category) {
            case 'sea':
                $sqlCategory = ' And categories_travel_cities.categoryTravelId = 4';
            break;
            case 'mountain':
                $sqlCategory = ' And categories_travel_cities.categoryTravelId = 5';
                break;
            case 'recovery':
                $sqlCategory = ' And categories_travel_cities.categoryTravelId = 6';
                break;
        }

        $sql = 'SELECT cities.id, cities.`name`, cities.countryId, cities.image 
                FROM objects, cities, categories_travel_cities
                 WHERE categories_travel_cities.citiesId = cities.id 
                 '. $sqlCategory .'
                 GROUP BY cities.id  ORDER BY cities.name';
        $hotels = DB::select($sql);
        return $hotels;
    }

    public function getListCitiesByRegion($regionId, $dateFromStr){

        $sql = 'SELECT cities.name, cities.id,
                (SELECT COUNT(distinct(objects.id)) FROM objects, rooms WHERE objects.`cityId` = cities.id and objects.id = rooms.objectId) AS countObjects FROM cities WHERE cities.`regionId` = '. $regionId . ' HAVING countObjects > 0';

        return DB::select($sql);

}



}
