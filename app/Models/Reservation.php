<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Helpers\Validate;
use Exception;
use Auth;

class Reservation extends Model

{



    protected $table = 'reservations';
    protected $guarded = [];
    public $timestamps = false;

    public function bonus() {
        return $this->belongsTo('App\Models\Bonus', 'bonusId', 'id');
    }
    
    public function getInfoByObjectReservation($objectId, $hotelsName, $roomId, $period, $countPeriod) {
        
        $reservationInfo = DB::select("SELECT *, SUM(priceWithDiscount) as fullPrice, COUNT(countEcDate) AS countDate FROM (SELECT r.id AS roomId, ec.date, o.id AS objectId, o.image, o.subdomain, o.url, o.address, c.name AS cityName, r.name AS roomName, o.name as objectName,
        IF(ec.`valuePercent` IS NULL, ec.price, ec.price - (ec.price * ec.valuePercent) / 100) AS priceWithDiscount, COUNT(DISTINCT(ec.date)) AS countEcDate
        FROM rooms AS r
        JOIN objects AS o ON r.`objectId` = o.`id`
        JOIN cities AS c ON o.`cityId` = c.id
        JOIN event_calendar AS ec ON r.id = ec.`typeRoomId`
        WHERE r.`objectId` = ". $objectId ." AND r.id = ". $roomId ." AND ec.`closeRoom` = 0 AND ec.`reservationId` = 0 AND ec.`date` IN (". implode(',', $period) .")
        GROUP BY ec.date) AS table_q
        HAVING countDate >= ".$countPeriod);
        return $reservationInfo;
    }
    
    
    public function createPreReservation($objectId, $userId, $dateFrom, $dateTo, $type = 0) {
        
        $reservationId = DB::table('reservations')->insertGetId([
            'createUserId' => $userId,
            'objectId' => $objectId,
            'startReservationDate' => strtotime($dateFrom),
            'endReservationDate' => strtotime($dateTo),
            'statusId' => 1,
            'type' => $type
        ]);
        return $reservationId;
    }
    
    public function preReservationRoom($roomId, $fullPrice, $fullPriceDiscount, $reservationId, $priceForDay, $amountPerson) {
        $roomId = DB::table('reservation_relation_room')->insertGetId([
            'roomId' => $roomId,
            'price' => $fullPriceDiscount,
            'priceWithoutDiscount' => $fullPrice,
            'reservationId' => $reservationId,
            'priceForDay' => $priceForDay,
            'amountPerson' => $amountPerson
        ]);
        return $roomId;
    }
    
    public function reservedRoomEventCalendarByPeriod($roominfo, $reservationId, $relationRoomEventCalendarId = 0) {
        foreach($roominfo as $room) {
            DB::table('event_calendar')->where('id', $room->id)->update(['reservationId' => $reservationId, 'reservedRoomId' => $relationRoomEventCalendarId]);
        }
        
    }
    
    public function fullEditReservationById($arrayInformation, $price, $prepayment) { //этот метод использовать
        DB::table('reservations')
            ->where('id', $arrayInformation['reservationId'])
            ->update([
                'startReservationDate' => strtotime($arrayInformation['dateFrom']),
                'endReservationDate' => strtotime($arrayInformation['dateTo']),
                'userId' => $arrayInformation['userId'],
                'name' => $arrayInformation['name'],
                'visitorEmail' => $arrayInformation['email'],
                'visitorPhone' => $arrayInformation['telefon'],
                'price' => $price,
                'prePayment' => $prepayment,
                'statusId' => 4,
                'creationDate' => time()
            ]);
    }
    
    
    public function getListReservation($id = false, $dateFrom = false, $dateTo = false, $statusId = false, $prepayment = false, $type = 0, $isPast = false) {
        
         if($id && !Validate::unsignedFloat($id)) throw new exception('Invalid id');
        
        if($statusId && ($statusId != 3 && $statusId !=2)) throw new Exception();
        if($type && ($type != 1 && $type != 0)) throw new Exception();
        if($prepayment && ($prepayment != 1 && $prepayment != 0)) throw new Exception();

        if($dateFrom && !Validate::unsignedInt($dateFrom)) throw new Exception();
        if($dateTo && !Validate::unsignedInt($dateTo)) throw new Exception();
        
        $listReservation = DB::table('reservations as res')
            ->select('res.lastUpdater', 'res.id', 'res.name', 'o.name as objectName',
           'res.startReservationDate', 'res.endReservationDate', 'res.visitorPhone',
           'res.price', 'res.prePayment', 'sr.name as statusName', 'sr.id as statusId', 'c.name as managerName', 'c.lastName as managerLastName', 'res.creationDate',
           DB::raw('(select group_concat(r.name) from reservation_relation_room as rrr join reservations as res1 on res1.id = rrr.reservationId join rooms as r on r.id = rrr.roomId where res1.id = res.id AND rrr.reservationId = res.id) as roomsName'))
            ->leftJoin('users as u', 'u.id', '=', 'res.createUserId')
            ->join('status_reservation as sr', 'sr.id', '=', 'res.statusId')
            ->join('objects as o', 'o.id', '=', 'res.objectId')
            ->leftJoin('clients as c', 'c.id', '=', 'u.clientId')
            ->orderBy('res.id', 'desc');
        
        if($id) {
           $listReservation->where('res.objectId', $id);
        }

        if($dateFrom) {
            $listReservation->where('res.startReservationDate', '>=', $dateFrom);
        }
        
        if($dateTo) {
            $listReservation->where('res.startReservationDate', '<=', $dateTo);
        }
        
        if($statusId) {
            $listReservation->where('res.statusId', $statusId);
        }
        
        if($prepayment) {
            $listReservation->where('res.prePayment', '>', 0);
        }
        
        if($type) {
            $listReservation->where('res.type', $type);
        }
        
        $user = Auth::user();
        
        if($user->objectId) {
            $listReservation->where('res.statusId', '!=', 1)->where('res.statusId', '!=', 4);
        }
        
        if($isPast) {
            $listReservation->where('isPast', 0)->where('res.endReservationDate', '>', $dateTo);
        }

        if($isPast === 0) {
            $listReservation->where('isPast', 1);
        }

        $listReservation = $listReservation->get(); 

        return $listReservation;
        
    }
    
    
    public function deleteRoomFromReservation($reservationId, $roomId) {
        
        DB::table('reservation_relation_room')->where('reservationId', $reservationId)->where('id', $roomId)->delete();
        
    }
    
    public function deleteReservRoomInEventCalendar($reservationId, $roomId, $objectId) {
        DB::table('event_calendar')->where('reservationId', $reservationId)->where('reservedRoomId', $roomId)->where('objectId', $objectId)->update([
            'reservationId' => 0,
            'reservedRoomId' => 0
        ]);
    }
    
    public function deleteReservationById($reservationId) {
        $result = DB::table('reservations')->where('id', $reservationId)->update([
            'statusId' => 2
        ]);
        return $result;
    }
    
    public function getInfoByReservationId($id) {
        $result = DB::table('reservations as r')->select('r.creationDate', 'c.name as cityName', 'regions.name as regionName', 'r.lastUpdater',
            'r.id', 'r.startReservationDate', 'r.endReservationDate', 'r.name', 'r.visitorPhone',
            'r.visitorEmail', 'r.userId', 'r.userId', 'r.price', 'r.prePayment', 'o.name as objectName', 'oc.name as categoryName', 'sr.name as statusName', 'sr.id as statusId',
            DB::raw('COUNT(rrr.id) as countRoom'), DB::raw('GROUP_CONCAT(rrr.peopleFio) as guests'), 'r.objectId', DB::raw('sum(rrr.amountPerson) as countPeople'))
            ->join('objects as o', 'o.id', '=', 'r.objectId')
            ->join('objects_categories as oc', 'oc.id', '=', 'o.objectCategoryId')
            ->join('status_reservation as sr', 'sr.id', '=', 'r.statusId')
            ->join('reservation_relation_room as rrr', 'rrr.reservationId', '=', 'r.id')
            ->join('cities as c', 'c.id', '=', 'o.cityId')
            ->join('regions', 'regions.id', '=', 'c.regionId')
            ->leftJoin('users as u', 'u.id', '=', 'r.userId')
            ->where('r.id', $id)
            ->first();
        return $result;
    }

    public function getInfoRoomsByReservationId($id) {
        $result = DB::table('reservation_relation_room as rrr')
            ->select('rrr.peopleFio', 'rrr.price', 'r.name', 'rrr.id', 'rrr.priceForDay', 'r.amountPerson', 'rrr.amountPerson as roomAmountPerson', 'r.id as roomId', 'rrr.prepaymentId', 'rrr.priceWithoutDiscount', 'rrr.info')
            ->join('rooms as r', 'r.id', '=', 'rrr.roomId')
            ->where('rrr.reservationId', $id)
            ->get();
        return $result;
    }
    
    public function getListReservationByDate($date, $day, $objectId) {
        $reservations = DB::table('reservations as r')
            ->select('r.startReservationDate', 'r.endReservationDate', 'r.id', 'r.name', 'r.id',
                     DB::raw('(select count(rrr.id) from reservations as res join reservation_relation_room as rrr ON rrr.reservationId = res.id where res.objectId = '. $objectId .' and r.id = res.id group by res.id)  as countRoom'),
                    DB::raw('(select sum(rrr.amountPerson) from reservations as res join reservation_relation_room as rrr on rrr.reservationId = res.id where res.objectId = '. $objectId .' and r.id = res.id group by res.id) as countPerson'))
//            ->join('reservation_relation_room as rrr', 'rrr.reservationId', '=', 'r.id')
            ->whereBetween('r.creationDate', [$date, $day])
            ->whereNotNull('r.id')
            ->where('r.objectId', $objectId)
            ->having('countRoom', '>', 0)
            ->get();
        return $reservations;
    }
    
    public function getListReservationArrivalByDate($date, $day, $objectId) {
        $reservations = DB::table('reservations as r')
            ->select(DB::raw('COUNT(rrr.id) as countRoom'), DB::raw('sum(rrr.amountPerson) as countPerson'), 'r.startReservationDate', 'r.endReservationDate', 'r.id', 'r.name', 'r.id')
            ->join('reservation_relation_room as rrr', 'rrr.reservationId', '=', 'r.id')
            ->where('r.startReservationDate', $date)
            ->where('r.objectId', $objectId)
            ->whereNotNull('r.id')
            ->groupBy('r.id')
            ->having('countRoom', '>', 0)
            ->get();
        return $reservations;
    }
    
    public function getListReservationDepartureByDate($date, $day, $objectId) {
        $reservations = DB::table('reservations as r')
            ->select(DB::raw('COUNT(rrr.id) as countRoom'), DB::raw('sum(rrr.amountPerson) as countPerson'), 'r.startReservationDate', 'r.endReservationDate', 'r.id', 'r.name', 'r.id')
            ->join('reservation_relation_room as rrr', 'rrr.reservationId', '=', 'r.id')
            ->where('r.endReservationDate', $date)
            ->where('r.objectId', $objectId)
            ->whereNotNull('r.id')
            ->having('countRoom', '>', 0)
            ->groupBy('r.id')
            ->get();
        return $reservations;
    }
    
    
    
    
    public function getListObjectsByAdmin($amountPerson = false, $periods, $countPeriod, $priceFrom = false, $priceTo = false, $category, $searchId, $discount = false, $page) {
        
        $priceFromSql = '';
        $priceToSql = '';
        if($priceFrom) {
            $priceFromSql = 'AND ec.price >= '.$priceFrom;
        }
        
        if($priceTo) {
            $priceToSql = 'AND ec.price <= '.$priceTo;
        }


        

        $discountSql = '';
        if($discount) {
            $discountSql = 'AND ec.valuePercent > 0';
        }
        
        $amountPersonSql = '';
        if($amountPerson) {
            $amountPersonSql = 'AND r.amountPerson >= '.$amountPerson;
        }
        
        
        
        $query = DB::table('objects as o')->select('o.id', 'o.name as objectName', 'o.subdomain', 'o.image', 'o.address', 'g.reservationPhone', 'o.stars', 'c.name as cityName', 'reg.name as regionName',
            DB::raw('(SELECT COUNT(DISTINCT(ec.date)) as countRowDate FROM event_calendar AS ec JOIN rooms AS r ON r.id = ec.typeRoomId 
WHERE r.objectId = o.id AND ec.date IN ('. implode(',', $periods) .') AND ec.reservationId = 0 
AND ec.closeRoom = 0 '. $priceFromSql .' '. $priceToSql .' '. $discountSql .' '. $amountPersonSql .' GROUP BY r.id having countRowDate >= '. $countPeriod .' LIMIT 1) as countDate'))
            ->leftJoin('general_infromation_objects as g', 'g.objectId', '=', 'o.id')
            ->join('cities as c', 'c.id', '=', 'o.cityId')
            ->join('regions as reg', 'reg.id', '=', 'c.regionId')
            ->leftJoin('confirmed_object as co', 'co.objectId', '=', 'o.id');
            //->where('co.confirmedAccount', 1); потом вернуть
        
        switch($category) {
            case 'region': 
                $query->where('reg.id', $searchId);
                break;
            case 'city':
                $query->where('c.id', $searchId);
                break;
            case 'object':
                $query->where('o.id', $searchId);
                break; 
        }
        
        $objects = $query->havingRaw('countDate IS NOT NULL')->limit(10)->get();
        
        return $objects;
        
        
    }
    
    
    public function getListPastReservation($currentDate) {

        $reservations = DB::table('reservations as s')
            ->select('s.startReservationDate', 's.endReservationDate', 's.visitorPhone', 's.name', 'o.name as objectName', 's.id')
            ->join('objects as o', 'o.id', '=', 's.objectId')
            ->where('s.statusId', 3)
            ->where('s.isPast', 0)
            ->where('s.endReservationDate', '<=', $currentDate)
            ->get();
        return $reservations;
    }
    
    
    public function changeIsPastByReservationId($reservationId, $isPast) {
        
        DB::table('reservations')->where('id', $reservationId)->update([
            'isPast' => $isPast 
        ]);
        
    }
    
    
    public function getMiniInfoByReservationId($reservationId) {
        
        $reservation = DB::table('reservations as r')
            ->select('o.name as objectName', 'r.visitorEmail', 'o.id', 'r.userId', 'r.bonusId', 'r.price')
            ->join('objects as o', 'o.id', '=', 'r.objectId')
            ->where('r.id', $reservationId)
            ->first();
        return $reservation;
        
    }

    
   public function deletePreReservationRoom($id) {

       DB::table('reservation_relation_room')->where('id', $id)->delete();
//       DB::table('relation_room_reservation_calendar')->whereIn('id', $id)->delete();
       
   } 
    
    
   public function changeStatusReservation($reservationId, $statusId) {
       DB::table('reservations')->where('id', $reservationId)->update([
           'statusId' => $statusId
       ]);
   } 
    
    public function getListRelationRoomByReservationId($reservationId) {
        $listRooms = DB::table('reservation_relation_room')->select('id', 'roomId')->where('reservationId', $reservationId)->get()->toArray();
        return $listRooms;
    }
    
    
    public function object() {
        return $this->hasOne(ObjectHotel::class, 'id', 'objectId');
    }

    public function user() {
        return $this->hasOne(User::class, 'id', 'userId');
    }
    
    
    
    
    
    
    
    
    
}
