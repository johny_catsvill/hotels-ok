<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReviewCategory extends Model
{
    protected $table = 'categories_users_for_reviews';

    public function reviews() {
        return $this->hasMany('App\Models\Review', 'typeUserReviewId', 'id');
    }


}
