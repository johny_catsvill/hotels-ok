<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ObjectPlusesRelation extends Model
{
    //
    
    protected $table = 'objects_objects_types_pluses';
    protected $guarded = [];
    public $timestamps = false;

    public function objectPluses() {
        return $this->hasMany('App\Models\ObjectsTypesPluses', 'id', 'objectTypePlusesId');
    }
}
