<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Helpers\Validate;
use Exception;
use Illuminate\Support\Facades\Storage;

class Room extends Model
{

    protected $guarded = [];
    public $timestamps = false;
    
    public function object() {
        return $this->belongsTo('App\Models\ObjectHotel');
    }

    public function eventCalendar() {
        return $this->hasMany('App\Models\EventCalendar', 'typeRoomId', 'id');
    }
    
    public function pluses() {
        return $this->belongsToMany('App\Models\PlusesRoom', 'rooms_relation_rooms_categories_pluses', 'roomId', 'roomPluseId');
    }

    
    public function getListBedsForRoom() {
        $listBeds = DB::table('bed_types')->select('id', 'name')->get();
        return $listBeds;
    }
    
    public function createRelationBedTypeAndRoom($roomId, $arrayBedType, $arrayCountBed) {
        $dataBed = [];
        for($i = 0; $i < count($arrayBedType); $i++) {
            if($arrayBedType[$i]) {
                $bedAndCountRoom = [
                    'roomId' => $roomId,
                    'bedTypeId' => $arrayBedType[$i],
                    'countBed' => $arrayCountBed[$i]
                ];
                array_push($dataBed, $bedAndCountRoom);
            }
            
        }
        
        if(count($dataBed) > 0) {
            DB::table('bed_types_relation_room')->insert($dataBed);
        }
        
    }
    
    public function deleteBedRelationThatRoom($roomId) {
        $result = DB::table('bed_types_relation_room')->where('roomId', $roomId)->delete();
        return $result;
    }
    
    public function getListBedsByRoomId($roomId) {
        $listBeds = DB::table('bed_types_relation_room')->where('roomId', $roomId)->get();
        return $listBeds;
    }
    
    public function deleteRelationBedTypeAndRoom($roomId) {
        DB::table('bed_types_relation_room')->where('roomId', $roomId)->delete();
    }

    public function getListRoomByObjectId($objectId)
    {
        $query = DB::table('rooms AS r')
            ->select('r.id AS roomId', 'r.name AS roomName', 'r.amountPerson', 'r.bedType', 'r.price');
        $query->where('r.objectId', $objectId);

        $roomsList = $query->groupBy('r.objectId', 'r.id')->get();

        return $roomsList;
    }
    
    public function getListRoomsInReservationByObjectId($objectId, $period) {
        
        $listRooms = DB::table('event_calendar as ec')->select('r.id', 'r.name', 'r.amountPerson', DB::raw('GROUP_CONCAT(DISTINCT ec.date) as listDate'))
            ->join('rooms as r', 'r.id', '=', 'ec.typeRoomId')
            ->where('r.objectId', $objectId)
            ->whereIn('ec.date', $period)
            ->where('ec.reservationId', 0)
            ->where('ec.closeRoom', 0)
            ->groupBy('r.id')
            ->get();
        return $listRooms;
    }
    
    public function getInfoPriceByRooms($objectId, $roomId, $period) {
        dd($roomId, $objectId, $period);
        $query = DB::table('event_calendar as ec')
            ->select('ec.price', 'ec.valuePercent', 'r.price as roomPriceStandart', 'r.id')
            ->join('rooms as r', 'r.id', '=', 'ec.roomId')
            ->where('r.objectId', $objectId)->where('r.id', $roomId);
        $periodString = implode(",", $period);
        
        $query->whereIn('ec.date', $periodString);
        $listRoomsPrice = $query->get();
        return $listRoomsPrice;
        
    }
    
    public function createRelationReservationAndRoom($rooms, $reservationId) {
        
        $arrayRoomsInsert = [];
        foreach($rooms as $room) {
            $thatArray = [
                'roomId' => $room['roomId'],
                'peopleFio' => $room['peopleFio'],
                'prePaymentId' => $room['prePaymentId'],
                'info' => $room['info'],
                'price' => $room['price'],
                'reservationId' => $reservationId
            ];
            array_push($arrayRoomsInsert, $thatArray);
        }
        
        DB::table('reservation_relation_room')->insert($arrayRoomsInsert);
    }
    
    public function getPriceInfoByRoomIdInPeriod($roomId, $period, $periodCount) {
        
        
        $room = DB::table('event_calendar as ec')
            ->select('ec.date', 'ec.price', 'ec.amountPersonRoom', 'ec.valuePercent as discount', 'ec.id', DB::raw('COUNT(DISTINCT(ec.date)) as countUniqueRow'))
            ->join('rooms as r', 'r.id', '=', 'ec.typeRoomId')
            ->where('ec.typeRoomId', $roomId)
            ->where('ec.reservationId', 0)
            ->whereIn('ec.date',$period)
            ->groupBy('ec.date')
//            ->having(DB::raw('SUM(countUniqueRow)'), '>=', $periodCount) // тут если че вернуть на единицу
            ->get();
        
        return $room;
    }
    

    
    public function updateRoomsByReservationId($reservationId, $roomsArray) {
        foreach($roomsArray as $room) {
    
            DB::table('reservation_relation_room')
                ->where('id', $room['roomId'])
                ->where('reservationId', $reservationId)
                ->update([
                    'peopleFio' => $room['peopleFio'],
                    'prePaymentId' => $room['prePayment'],
                    'info' => $room['info']
                ]);
        }
        
    }
    
    public function getListPriceByRooms($roomId, $reservationId) {
            
        $roomPrice = DB::table('reservation_relation_room')
            ->select('price', 'prePaymentId', 'priceForDay')
            ->where('id', $roomId) // коммент проверки комнаты
            ->where('reservationId', $reservationId)
            ->first();
        return $roomPrice;
    }
    
    public function getInfoRoom($roomId) {
        $room = DB::table('rooms')->select('*')->where('id',$roomId)->first();
        return $room;
    }


    public function getListRoomsByDate($roomObject, $dateFrom, $dateTo, $period, $object, $person = 1) {
        $roomObject = new Room;

        $rooms = $roomObject->with(['eventCalendar' => function($query) use($dateFrom, $dateTo) {
            $query->where('event_calendar.date','>=', $dateFrom)->where('event_calendar.date', '<', $dateTo)->groupBy('event_calendar.typeRoomId')->groupBy('event_calendar.date');
        }])->has('eventCalendar', '>=', $period)->where('objectId', $object->id)->where('rooms.amountPerson', '>=', $person)->get();
        foreach ($rooms as $room) {
            $images = Storage::files('images/hotels/'. $object->id .'-'. $object->subdomain.'/images-rooms/'.$room->id);

            foreach($images as $key => $img) {
                if(strpos($img, 'mini') === false) {
                    unset($images[$key]);
                }
            }
            $images = array_values($images);
            if(isset($images[0])) {
                $firstImg = $images[0];
            }
            else {
                $firstImg = '';
            }
            $firstImg = preg_replace('/_mini/', '_norm', $firstImg);
            $room->firstImage = $firstImg;

            $room->images = $images;

            $priceWithDiscount = 0;
            $priceWithoutDiscount = 0;

            $categoryPluses = new \App\Models\PlusesCategory();
            $room->categoryPluses = $categoryPluses->with(['pluses' => function($query) use($room) {
                $query->roomPluses($room);
            }])->get();

//            dd($room->eventCalendar);
            foreach($room->eventCalendar as $index => $calendar) {

                if($index == 0) {
                    if(!$this->minPriceByRoom) {
                        $this->minPriceByRoom = $calendar->price;
                    }
                    else {
                        if($this->minPriceByRoom > $calendar->price) {
                            $this->minPriceByRoom = $calendar->price;
                        }
                    }
                }

                if($calendar->valuePercent) {
                    if($index == 0) {
                        array_push($this->isDiscount, $room);
                        $room->isDiscount = $calendar->valuePercent;
                    }
                    $priceWithDiscount += $calendar->price - ($calendar->price * $calendar->valuePercent / 100);
                }
                else {
                    $priceWithoutDiscount += $calendar->price;
                }
            }
            $room->priceWithDiscount = round($priceWithDiscount,0);
            $room->priceWithoutDiscount = round($priceWithoutDiscount, 0);
        }

        return ['rooms' => $rooms, 'minPrice' => $this->minPriceByRoom];
    }

    public function getListRoomsByDateInSearchAdmin($periods, $object, $priceFrom, $priceTo, $discount, $amountPerson) {

        $priceFromSql = '';

        if($priceFrom) {
            $priceFromSql = ' AND ec.price >= '.$priceFrom;
        }


        $priceToSql = '';
        if($priceTo) {
            $priceToSql = ' AND ec.price <= '. $priceTo;
        }

        $discountSql = '';
        if($discount) {
            $discountSql = ' AND ec.valuePercent > 0';
        }

        $amountPersonSql = false;
        if($amountPerson) {
            $amountPersonSql = ' AND r.amountPerson >= '.$amountPerson;
        }

        $datesSql = '';
        $pricesSql = '';
        $discountsSql = '';
        $i = 0;
        foreach($periods as $key => $p) {

            $i++;

            $datesSql.= '(SELECT COUNT(event_calendar.date) FROM event_calendar WHERE event_calendar.`objectId` = '. $object->id .' 
            AND r.id = event_calendar.`typeRoomId` AND event_calendar.date = '. $p .' GROUP BY event_calendar.typeRoomId) AS calendar'.$i. ',';

            $pricesSql.= '(SELECT event_calendar.price FROM event_calendar WHERE event_calendar.`objectId` = '. $object->id .' AND r.id = event_calendar.`typeRoomId` 
            AND event_calendar.date = '. $p .' GROUP BY event_calendar.typeRoomId) AS price'.$i. ',';

            $discountsSql.= '(SELECT event_calendar.`valuePercent` FROM event_calendar WHERE event_calendar.`objectId` = '. $object->id .' AND r.id = event_calendar.`typeRoomId` 
            AND event_calendar.date = '. $p .' GROUP BY event_calendar.typeRoomId) AS discount'.$i;

            if($i != count($periods)) {
                $discountsSql.= ',';
            }
        }


        $sql = 'SELECT ec.`typeRoomId`, min(ec.price) as minPrice, r.`amountPerson`, r.`name`, '. $datesSql.$pricesSql.$discountsSql .' 
                FROM event_calendar AS ec 
                JOIN rooms AS r ON r.id = ec.`typeRoomId`
                WHERE ec.objectId = '. $object->id .' AND ec.date IN ('. implode(',', $periods) .') 
                AND ec.reservationId = 0 AND ec.closeRoom = 0 '. $priceFromSql.$priceToSql.$discountSql.$amountPersonSql .' 
                GROUP BY ec.typeRoomId';
        $listRooms = DB::select($sql);
        return $listRooms;
    }

    public function getListRoomsReservation($objectId, $periods, $rooms, $dateFrom, $dateTo, $isClosed = false) {

        $i = 0;
        $datesSql = '';
        $pricesSql = '';
        $discountsSql = '';
        foreach($periods as $key => $p) {

            $i++;
            $datesSql.= '(SELECT COUNT(event_calendar.date) FROM event_calendar WHERE event_calendar.`objectId` = '. $objectId .' 
            AND r.id = event_calendar.`typeRoomId` AND event_calendar.date = '. $p .' GROUP BY event_calendar.typeRoomId) AS calendar'.$i. ',';

            $pricesSql.= '(SELECT event_calendar.price FROM event_calendar WHERE event_calendar.`objectId` = '. $objectId .' AND r.id = event_calendar.`typeRoomId` 
            AND event_calendar.date = '. $p .' GROUP BY event_calendar.typeRoomId) AS price'.$i. ',';

            $discountsSql.= '(SELECT event_calendar.`valuePercent` FROM event_calendar WHERE event_calendar.`objectId` = '. $objectId .' AND r.id = event_calendar.`typeRoomId` 
            AND event_calendar.date = '. $p .' GROUP BY event_calendar.typeRoomId) AS discount'.$i;

            if($i != count($periods)) {
                $discountsSql .= ',';
            }
        }

        $roomsArray = '';
        if(is_array($rooms)) {
            $roomsArray = implode(',', $rooms);
        }
        else {
            $roomsArray = $rooms;
        }

        $closedSql = '';

        if(!$isClosed) {
            $closedSql = ' AND ec.closeRoom = 0 ';
        }

        $sql = 'SELECT ec.`typeRoomId`, ec.amountPersonRoom, ec.valuePercent, ec.price, r.name, '. $datesSql. $pricesSql. $discountsSql .' 
                FROM event_calendar AS ec 
                JOIN rooms AS r ON r.id = ec.`typeRoomId`
                WHERE ec.objectId = '. $objectId .' AND ec.date IN ('. implode(',', $periods) .') 
                AND ec.reservationId = 0 '. $closedSql .' and ec.typeRoomId IN('. $roomsArray .') 
                GROUP BY ec.typeRoomId';

        $listRooms = DB::select($sql);
        return $listRooms;



    }


    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
