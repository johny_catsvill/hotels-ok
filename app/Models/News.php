<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    public $timestamps = false;

    public function category() {
        return $this->belongsTo('App\Models\CategoryNews', 'category_id',  'id');
    }

    public function getNewImage($category) {
        $imageName = $this->image;

        $newImage = explode('.', $imageName);

        $type = $newImage[1];

        return $newImage[0].'_'.$category.'.'.$type;
    }

}
