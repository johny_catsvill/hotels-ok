<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Helpers\ValidateQuery;

class ObjectsCategories extends Model
{
    protected $table = 'objects_categories';
    protected $fillable = ['*'];

    public function getObjectsCategoriesListByCountObject($countryId = false, $cityId = false,
                                                          $regionId = false, $dateFrom = false, $dateTo = false, $person = false)
    {
        $query = DB::table('objects_categories AS oc')
            ->select('oc.id', 'oc.name', DB::raw('count(DISTINCT o.id) as countObject'))
            ->join('objects AS o', 'oc.id', '=', 'o.objectCategoryId')
            ->join('rooms AS r', 'o.id', '=', 'r.objectId')
            ->leftJoin('confirmed_object as co', 'co.objectId', '=', 'o.id')
            ->where('co.confirmedAccount', 1);

        $query = ValidateQuery::validateQueryForFilters($query, $countryId, $cityId,
            $regionId, $dateFrom, $dateTo, $person);

        $objectsCategories = $query->groupBy('oc.id')->get();
        return $objectsCategories;
    }
    
    
    public function getListCategoriesObject() {
        $listCategoriesObject = DB::table($this->table)->select('name', 'id')->get();
        return $listCategoriesObject;
    }

    public function objects() {
        return $this->hasMany('App\Models\ObjectHotel', 'objectCategoryId', 'id');
    }

    public function getListTypeObjectsAndCountObject($city = false, $region = false, $dateFromStr = false, $guest = 1, $search = false, $starsArray = false, $minPrice = false, $maxPrice = false, $foodArray = false, $servicesArray = false, $plusesArray = false) {


        $sqlTable =  'event_calendar, objects';

        $sqlCity = '';
        if($city) {
            $sqlCity = ' and event_calendar.cityIdDaugher = '. $city->id;
        }

        $sqlRegion = '';
        if($region) {
            $sqlRegion = ' and event_calendar.regionId = '. $region->id;
        }

        $sqlGuest = ' and event_calendar.amountPersonRoom = '. $guest;

        $sqlStars = '';
        if($starsArray) {
            $sqlStars = ' and objects.stars in ('. implode(",", $starsArray) .')';
        }

        $sqlMinPrice = '';
        if($minPrice) {
            $sqlMinPrice = ' and event_calendar.price >= '. $minPrice;
        }

        $sqlMaxPrice = '';
        if($maxPrice) {
            $sqlMaxPrice = ' and event_calendar.price <= '. $maxPrice;
        }

        $sqlFood = '';
        if($foodArray) {
            $sqlTable.= ', food_types_relation_object';
            $sqlFood = ' and food_types_relation_object.objectId = objects.id and food_types_relation_object.foodTypeId in ('. implode(",", $foodArray) .')';
        }

        $sqlServices = '';
        if($servicesArray) {
            $sqlTable.= ', objects_relation_objects_servises';
            $sqlServices = ' and objects_relation_objects_servises.objectId = objects.id and objects_relation_objects_servises.objectServiceId in ('. implode(",", $servicesArray) .')';
        }

        $sqlPluses = '';
        if($plusesArray) {
            $sqlTable.= ', objects_objects_types_pluses';
            $sqlPluses = ' and objects_objects_types_pluses.objectId = objects.id and objects_objects_types_pluses.objectTypePlusesId in ('. implode(",", $plusesArray) .') ';
        }

        $sql = 'SELECT objects_categories.name, objects_categories.id,
                        (SELECT COUNT(DISTINCT objects.id)
                        FROM
                          '. $sqlTable .'
                          where objects.id =  event_calendar.`objectId`
                        And event_calendar.date = '. $dateFromStr .'
                          AND event_calendar.`amountPersonRoom` >= '. $guest .'
                          '. $sqlCity. $sqlRegion . $sqlStars . $sqlMinPrice . $sqlMaxPrice . $sqlFood . $sqlServices . $sqlPluses .'
                          AND `event_calendar`.`closeRoom` = 0 AND `event_calendar`.`reservedRoomId` = 0 AND `event_calendar`.`reservationId` = 0
                          AND event_calendar.objectCategoryId = objects_categories.id
                           LIMIT 1) AS objects_count
                        FROM objects_categories';

        return DB::select($sql);

    }
    
    
    
    
    
}
