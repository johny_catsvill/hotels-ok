<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class CategoriesTravel extends Model
{
    const CITY_LIMIT = 7;
    const REGION_LIMIT = 7;
    const OBJECT_LIMIT = 7;

    
    public function getListCategoriesTravel()
    {
        $listCityAndCategoryCity = DB::table('categories_travel AS ct')->select('ct.id', 'ct.name', 'ct.url',
            DB::raw('GROUP_CONCAT(c.name SEPARATOR ",") AS cityName'),
            DB::raw('GROUP_CONCAT(c.url SEPARATOR ",") AS cityUrl'))
            ->join('categories_travel_cities AS ctc', 'ctc.categoryTravelId', '=', 'ct.id')
            ->join('cities AS c', 'c.id', '=', 'ctc.citiesId')
            ->groupBy('ct.id')
            ->groupBy('ct.name')
            ->groupBy('ct.url')
            ->limit(6)
            ->get();
        return $listCityAndCategoryCity;
    }

    public function getListCities() {

        $listCities = DB::select("SELECT c.id, c.name AS cityName, c.url, c.countryId,
                            ct.`name` AS categoryName, ct.url as categoryLink,
                                (SELECT COUNT(o.id) FROM objects as o LEFT JOIN confirmed_object as co ON co.objectId = o.id WHERE o.`cityId` = c.id AND co.confirmedAccount = 1) AS countObject
                                FROM cities AS c
                                INNER JOIN categories_travel_cities AS ctc ON c.id = ctc.`citiesId`
                                INNER JOIN categories_travel AS ct ON ctc.`categoryTravelId` = ct.`id`
                                ORDER BY cityName ASC ");
        return $listCities;
    }

    public function getListCitiesAndRegionsAndObjectsAboutSearch($string) {
        $resultSearch = DB::table('cities as c')
        ->select('c.name', 'c.url', 'c.id', 'c.countryId',
            DB::raw('("city") as currentTable'))
        ->where('c.name', 'LIKE', '%'. $string .'%')->limit(self::CITY_LIMIT);

        $resultSearchSubqueryRegions = DB::table('regions as r')
            ->select('r.name', 'r.url', 'r.id', 'r.countryId',
                DB::raw('("region") as currentTable'))
            ->where('r.name', 'LIKE', '%'. $string .'%')->limit(self::REGION_LIMIT);

        $resultSearchSubqueryObjects = DB::table('objects as o')
            ->select('o.name', 'o.url', 'o.id', 'o.countryId',
                DB::raw('("object") as currentTable'))
            ->leftJoin('confirmed_object as co', 'co.objectId', '=', 'o.id')
            ->where('co.confirmedAccount', 1)
            ->where('o.name', 'LIKE', '%'. $string .'%')->limit(self::OBJECT_LIMIT);

        $resultSearch = $resultSearch->union($resultSearchSubqueryRegions)->union($resultSearchSubqueryObjects)->get();
        return $resultSearch;
    }
    
    public function getListRegions($countryId) {
        $listRegions = DB::table('regions')->select('id', 'name')->where('countryId', $countryId)->get();
        return $listRegions;
    }



}
