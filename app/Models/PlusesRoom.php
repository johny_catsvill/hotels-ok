<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;


class PlusesRoom extends Model
{

    protected $table = 'rooms_pluses';

    public function scopeRoomPluses($query, $room) {
        return $query->join('rooms_relation_rooms_categories_pluses', 'rooms_relation_rooms_categories_pluses.roomPluseId', '=', 'rooms_pluses.id')
            ->where('rooms_relation_rooms_categories_pluses.roomId', $room->id);
    }


    public function roomCategoryPluseRelation() {
        return $this->belongsToMany('App\Models\Room', 'rooms_relation_rooms_categories_pluses', 'roomPluseId', 'id');
    }



    public function getListCategoryPluses() {
        
        $listCategories = DB::table('rooms_categories_pluses as rcp')->select('rcp.id', 'rcp.name', 'rcp.deleted', DB::raw('COUNT(rp.id) as countPluses'))
            ->leftJoin('rooms_pluses as rp', 'rcp.id', '=', 'rp.roomCategoryId')
            ->groupBy('rcp.id')
            ->get();
        return $listCategories;
    }
    
    public function getCategoryPlusesById($id) {
        $categoryPluses = DB::table('rooms_categories_pluses')->select('id', 'name')->where('id', $id)->get()->first();
        return $categoryPluses;
    }
    
    public function createCategoryPluses($data) {
        DB::table('rooms_categories_pluses')->insert([
            'name' => $data['name']
        ]);
    }
    
    public function updateCategoryPluses($data) {
        DB::table('rooms_categories_pluses')->where('id', $data['categoryPlusesId'])->update([
            'name' => $data['name']
        ]);
    }
    
    public function deleteCategoryPluses($id) {
        DB::table('rooms_categories_pluses')->where('id', $id)->delete();
    }
    
    public function getListPlusesRoom() {
        $listPlusesRoom = DB::table('rooms_pluses as rp')->select('rp.id', 'rp.name as pluseName', 'rcp.name as categoryName', 'rp.image')
            ->join('rooms_categories_pluses as rcp', 'rp.roomCategoryId', '=', 'rcp.id')->orderBy('image', 'desc')
            ->get();
        return $listPlusesRoom;
            
    }
    
    public function createPluseRoom($data, $image) {
        DB::table('rooms_pluses')->insert([
            'name' => $data['name'],
            'roomCategoryId' => $data['roomCategoryId'],
            'class' => $image
        ]);
    }
    
    public function updatePluseRoomById($data, $image) {
        DB::table('rooms_pluses')->where('id', $data['pluseRoomId'])->update([
            'name' => $data['name'],
            'roomCategoryId' => $data['roomCategoryId'],
            'image' => $image
        ]);
    }
    
    public function getInfoPluseRoomById($id) {
        $pluseRoom = DB::table('rooms_pluses')->select('*')->where('id', $id)->get()->first();
        return $pluseRoom;
    }
    
    public function deletePluseRoomById($id) {
        DB::table('rooms_pluses')->where('id', $id)->delete();
    }
    
    public function deleteRelationCategoryPlusesAndRoom($id) {
        DB::table('rooms_relation_rooms_categories_pluses')->where('roomCategoryPluseId', $id)->delete();
    }
    
    public function getListCategoriesPlusesForRoom() {
        $listCategoriesPluses = DB::table('rooms_categories_pluses')->select('name', 'id')->get();
      
        return $listCategoriesPluses;
    }
    
    public function getListPlusesForRoom() {
        $listPluses = DB::table('rooms_pluses')->select('name', 'id', 'roomCategoryId')->get();
        
        return $listPluses;    
    }
    
    public function createRelationPlusesAndRoom($roomId, $arrayPluses) {
        $dataArrayPluses = [];
        foreach($arrayPluses as $pluse) {
            $thatArray = ['roomPluseId' => $pluse, 'roomId' => $roomId];
            array_push($dataArrayPluses, $thatArray);
        }
        DB::table('rooms_relation_rooms_categories_pluses')->insert($dataArrayPluses);
    }
    
    public function deletePluseRelationThatRoom($roomId) {
        $result = DB::table('rooms_relation_rooms_categories_pluses')->where('roomId', $roomId)->delete();
        return $result;
    }
    
    public function getListPlusesByRoomId($roomId) {
        $listPluses = DB::table('rooms_relation_rooms_categories_pluses')->select('roomPluseId')->where('roomId', $roomId)->get();
        return $listPluses;
    }
    
    
    
    
}
