<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Exception;
use App\Helpers\Validate;

class BesideObject extends Model
{
    //
    
    protected $table = 'objects_beside_types';
//    protected $guarded = [];
    protected $fillable = ['*'];
    public $timestamps = false;
    
    public function getListBesideObjects() {
        
        $listBesideObjects = DB::table('objects_beside_types')->select('id', 'name', 'deleted')->get();
        return $listBesideObjects;
        
    }
    
    public function getListBesideByObjectId($objectId) {
        
        $listObjects = DB::table('objects_beside as ob')
            ->select('ob.id', 'ob.name', 'ob.typeBesideId', 'ob.distance', 'obt.name as besideTypeName')
            ->join('objects_beside_types as obt', 'obt.id', '=', 'ob.typeBesideId')
            ->where('objectId', $objectId)
            ->get();
        return $listObjects;
    }
    
}
