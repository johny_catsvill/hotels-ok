<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Helpers\Validate;
use Exception;
use Validator;
//use App\Helpers\Func;

class Review extends Model
{

    public $timestamps = false;
    public function typeReview() {
        return $this->hasOne('App\Models\ReviewCategory', 'id', 'typeUserReviewId');
    }


    public function getListCategoriesPeople() {
        $list = DB::table('categories_users_for_reviews')->select('id', 'nameType')->get();
        return $list;
    }
    
    public function getListReviewsByObjectId($id, $page = 1, $countElement, $sort) {
        if(!Validate::id($id)) throw new Exception('invalid id');
        if(!Validate::int($page)) throw new Exception('invalid page');
        if(!Validate::int($countElement)) throw new Exception('invalid countElement');
        if(!strlen($sort) > 0) throw new Exception('invalid sort');


        
        $countReviews = $page * $countElement - $countElement;
        $reviews = DB::table('reviews as r')->select('r.positiveText', 'r.negativeText', 'r.comfortValue', 'r.employeeValue', 'r.serviceValue', 'r.cleanValue', 'r.locationValue', 'r.qualityPriceValue', 'r.authorName', 'r.creationDate as creationDate', /*'countries.name as countryName',*/ 'r.reservationId', 'cufr.nameType', DB::raw('SUM(r.`comfortValue` + r.`employeeValue` + r.`serviceValue` + r.`cleanValue` + r.`qualityPriceValue` + r.locationValue) / 6 AS avgValue'), 'countries.name as countryName')
            ->leftJoin('categories_users_for_reviews as cufr', 'r.typeUserReviewId', '=', 'cufr.id')
            ->leftJoin('clients as c', 'r.clientId', '=', 'c.id')
            ->leftJoin('countries', 'c.countryId', '=', 'countries.id')
            ->where('r.objectId', $id)
            ->groupBy('r.id');
            
        switch($sort) {
            case 'new':
                $reviews->orderBy('r.creationDate', 'desc');
                break;
            case 'positive': 
                $reviews->orderBy('positiveText', 'desc');
                break;
            case 'negative':
                $reviews->orderBy('negativeText', 'desc');
                break;
        }
    
        $r = $reviews->limit($countElement)->offset($countReviews)->get();

        return $r;
            
    }
    
    
    public function getListReviewsByDate($currentDate, $day, $objectId) {
        
        $reviews = DB::table('reviews as r')
            ->select('r.creationDate', DB::raw('SUM(r.`comfortValue` + r.`employeeValue` + r.`serviceValue` + r.`cleanValue` + r.`qualityPriceValue` + r.locationValue) / 6 AS avgValue'), 'r.authorName')
            ->where('r.objectId', $objectId)
            ->whereBetween('r.creationDate', [$currentDate, $day])
            ->whereNotNull('r.id')
            ->having('avgValue', '>', 0)
            ->get();
        return $reviews;
        
    }
    
    public function createQueryForReview($objectId, $code, $emailUser, $reservationId) {
        DB::table('code_reviews_user')->insert([
            'objectId' => $objectId,
            'emailUser' => $emailUser,
            'code' => $code,
            'reservationId' => $reservationId
        ]);
    }
    
    public function getInfoByCodeReview($email, $code) {
        $validator = Validator::make(['email' => $email, 'code' => $code], [
            'email' => 'required|email',
            'code' => 'required|min:10'
        ]);
        
        if($validator->fails()) {
            throw new exception('Invalid data');
        }
        
        
        $travel = DB::table('code_reviews_user as cru')
            ->select('cru.id', 'cru.objectId', 'cru.reservationId', 'cru.isReview', 'r.name as userName', 'r.lastName')
            ->join('reservations as r', 'r.id', '=', 'cru.reservationId')
            ->where('cru.emailUser', $email)
            ->where('cru.code', $code)
            ->first();
        return $travel;
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}

