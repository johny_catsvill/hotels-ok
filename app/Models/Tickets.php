<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Helpers\Validate;
use Exception;


class Tickets extends Model
{
    public function getListTickets($objectId) {
        
        if(!Validate::id($objectId)) throw new Exception('Exception: invalid id');
        
        $tickets = DB::table('tickets as t')
            ->select('t.id', 'themeTicket', DB::raw('(select lastUpdater from messages where ticketId = t.id order by lastUpdater desc limit 1) as dateLastMessages'), DB::raw('(select reading from messages where ticketId = t.id order by lastUpdater desc limit 1) as reading'))
            ->where('objectId', $objectId)
            ->get();
//        dd($tickets);
        return $tickets;
        
    }
    
    public function createTicket($theme, $objectId) {
        
        if(!Validate::id($objectId)) throw new Exception('Exception: invalid id');
        if(!$theme) throw new Exception('Invalid theme');
        
        $ticketId = DB::table('tickets')->insertGetId([
            'objectId' => $objectId,
            'themeTicket' => $theme
        ]);
        return $ticketId;
        
    }
    
    public function createMessage($message, $ticketId, $objectId) {
        if(!Validate::id($ticketId)) throw new Exception('Exception: invalid id');
        if(!Validate::id($objectId)) throw new Exception('Exception: invalid id');
        if(!$message) throw new Exception('Invalid message');
        
        DB::table('messages')->insert([
            'creationDate' => time(),
            'ticketId' => $ticketId,
            'message' => $message,
            'authorId' => $objectId
        ]);
        
    }
    
    public function createMessageAdmin($message, $ticketId, $managerId) {
        if(!Validate::id($ticketId)) throw new Exception('Exception: invalid id');
        if(!Validate::id($managerId)) throw new Exception('Exception: invalid id');
        if(!$message) throw new Exception('Invalid message');
        
        DB::table('messages')->insert([
            'creationDate' => time(),
            'ticketId' => $ticketId,
            'message' => $message,
            'managerId' => $managerId
        ]);
        
    }
    
    
    public function getInfoByTicketId($ticketId, $objectId) {
        if(!Validate::id($ticketId)) throw new Exception('Exception: invalid id');
        if(!Validate::id($objectId)) throw new Exception('Exception: invalid id');
        
        $ticket = DB::table('tickets')->select('themeTicket', 'id')->where('id', $ticketId)->where('objectId', $objectId)->first();
        
        return $ticket;
    }
    
    public function getInfoByTicketIdAdmin($ticketId) {
        if(!Validate::id($ticketId)) throw new Exception('Exception: invalid id');
        
        $ticket = DB::table('tickets')->select('themeTicket', 'id')->where('id', $ticketId)->first();
        
        return $ticket;
    }
    
    
    public function getListMessagesByTicketId($ticketId) {
        if(!Validate::id($ticketId)) throw new Exception('Exception: invalid id');
        
        $messages = DB::table('messages')->select('creationDate', 'message', 'authorId', 'managerId')->where('ticketId', $ticketId)->
            orderBy('creationDate', 'desc')->get();
        
        return $messages;
    }
    
    
    public function getListTicketsForAdmin() {
        $tickets = DB::table('tickets as t')->select('t.id', 't.themeTicket', 'o.organizationName as objectName', DB::raw('(select creationDate from messages where ticketId = t.id order by creationDate desc limit 1) as dateLastMessages'))
            ->join('objects as o', 'o.id', '=', 't.objectId')
            ->orderBy('t.lastUpdater', 'desc')
            ->get();
        
        return $tickets;
    }
    
    public function updateMessageAttrRead($ticketId, $objectId = false) {
        if(!Validate::id($ticketId)) throw new Exception('Exception: invalid id');
        $query = DB::table('messages')->where('ticketId', $ticketId);
        if($objectId) {
            
            $query->where('managerId', 10); //изменить потом на нормальное
        }
        
        $query->update(['reading' => 1]);
        
    }
    
    public function deleteTicket($ticketId) {
        if(!Validate::id($ticketId)) throw new Exception('Exception: invalid id');
        DB::table('messages')->where('ticketId', $ticketId)->delete();
        DB::table('tickets')->where('id', $ticketId)->delete();
    }
    
    
}
