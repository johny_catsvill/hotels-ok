<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Helpers\ValidateQuery;
use App\Helpers\Validate;

class FoodTypes extends Model
{
    protected $table = 'food_types';

    protected $fillable = ['*'];

    public function objects() {
        return $this->belongsToMany('App\Models\ObjectHotel', 'food_types_relation_object', 'foodTypeId', 'objectId');
    }


    
    public function getListFoodTypes() {
        $listFoodTypes = DB::table('food_types')->select('id', 'name')->get();
        return $listFoodTypes;
    }
    
    public function insertFoodType($arrayFoodTypes) {
        DB::table('food_types_relation_object')->insert($arrayFoodTypes);
    }
    
    public function removeFoodTypesObject($objectId) {
        if(!Validate::id($objectId)) throw new Exception('Exception: invalid id');
        DB::table('food_types_relation_object')->where('objectId', $objectId)->delete();
    }
    
    public function getListFoodTypesObject($objectId) {
        if(!Validate::id($objectId)) throw new Exception('Exception: invalid id');
        $listFoodTypesObject = DB::table('food_types_relation_object')->select('foodTypeId')->where('objectId', $objectId)->get();
        return $listFoodTypesObject;
    }


    public function getListFoodAndCountObject($city = false, $region = false, $dateFromStr = false, $guest = 1, $search = false, $starsArray = false, $minPrice = false, $maxPrice = false,
                                              $typeObjectArray = false, $servicesArray = false, $plusesArray = false) {

        $sqlTable =  'event_calendar, objects, food_types_relation_object';

        $sqlCity = '';
        if($city) {
            $sqlCity = ' and event_calendar.cityIdDaugher = '. $city->id;
        }

        $sqlRegion = '';
        if($region) {
            $sqlRegion = ' and event_calendar.regionId = '. $region->id;
        }


        $sqlGuest = ' and event_calendar.amountPersonRoom = '. $guest;

        $sqlTypeObject = '';
        if($typeObjectArray) {
            $sqlTypeObject = ' and objects.objectCategoryId in( '. implode(",", $typeObjectArray) . ')';
        }

        $sqlMinPrice = '';
        if($minPrice) {
            $sqlMinPrice = ' and event_calendar.price >= '. $minPrice;
        }

        $sqlMaxPrice = '';
        if($maxPrice) {
            $sqlMaxPrice = ' and event_calendar.price <= '. $maxPrice;
        }

        $sqlServices = '';
        if($servicesArray) {
            $sqlTable.= ', objects_relation_objects_servises';
            $sqlServices = ' and objects_relation_objects_servises.objectId = objects.id and objects_relation_objects_servises.objectServiceId in ('. implode(",", $servicesArray) .')';
        }

        $sqlStars = '';
        if($starsArray) {
            $sqlStars = ' and objects.stars in ('. implode(",", $starsArray) .')';
        }

        $sqlPluses = '';
        if($plusesArray) {
            $sqlTable.= ', objects_objects_types_pluses';
            $sqlPluses = ' and objects_objects_types_pluses.objectId = objects.id and objects_objects_types_pluses.objectTypePlusesId in ('. implode(",", $plusesArray) .')';
        }


        $sql = 'SELECT food_types.name, food_types.id, 
                        (SELECT COUNT(DISTINCT objects.id)
                        FROM
                          '. $sqlTable .'
                        WHERE event_calendar.date = '. $dateFromStr .' 
                          AND event_calendar.`amountPersonRoom` >= '. $guest .' AND event_calendar.`objectId` = objects.id
                          '. $sqlCity . $sqlRegion . $sqlTypeObject . $sqlMinPrice . $sqlMaxPrice . $sqlPluses . $sqlServices . $sqlStars .'
                          AND `event_calendar`.`closeRoom` = 0 AND `event_calendar`.`reservedRoomId` = 0 AND `event_calendar`.`reservationId` = 0
                          AND objects.id = food_types_relation_object.objectId 
                          and food_types_relation_object.foodTypeId = food_types.id 
                           LIMIT 1) AS objects_count
                        FROM food_types';

        return DB::select($sql);
    }
    
}
