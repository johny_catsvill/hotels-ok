<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Helpers\ValidateQuery;
use App\Helpers\CategoryHelper;
use App\Traits\timeTraits;
use App\Helpers\Validate;
use Exception;

class ObjectHotel extends Model
{
    use timeTraits;
    
    protected $table = 'objects';
    protected $guarded = [];
    public $timestamps = false;
    
    const COUNTOBJECT = 7;
    
//    public function room() {
//        return $this->hasMany('App\Models\Room', 'objectId', 'id');
//    }
//    
//    public function city() {
//        return $this->hasOne('App\Models\City', 'id', 'countryId');
//    }
//    
//    public function relationPluses() {
//        return $this->hasMany('App\Models\ObjectCategoryPluses', 'objectId', 'id');
//    }


    public function city() {
        return $this->belongsTo('App\Models\City', 'cityId');
    }

    public function category() {
        return $this->belongsTo('App\Models\ObjectsCategories', 'objectCategoryId');
    }


    public function getListObjectsByParameters($cityId = false, $limit, $dateFrom, $objectIds = false, $currentObjectId = false)
    {

        $sqlCity = '';
        if($cityId) {
           $sqlCity = ' And event_calendar.cityIdDaugher =  '. $cityId;
        }

        $sqlObjectIds = '';
        $orderBy = '';
        if($objectIds) {
            $sqlObjectIds = ' and objects.id in ('.implode(',',$objectIds).')';
        }
        else {
            $orderBy = ' ORDER BY RAND()';
        }

        $sqlObjectCurrent = '';

        if($currentObjectId) {
            $sqlObjectCurrent = ' And objects.id <> '. $currentObjectId;
        }

    $sql = 'select objects.id as objectId, objects.name, objects.`subdomain`, rooms.id, event_calendar.`valuePercent`, event_calendar.price, objects.image
            from objects, rooms, event_calendar where objects.id = rooms.`objectId` 
            and rooms.id = event_calendar.`typeRoomId` and event_calendar.date = '. $dateFrom . $sqlCity . $sqlObjectIds . $sqlObjectCurrent .'
            group by objects.`id` '. $orderBy .'
            limit '. $limit;

        return DB::select($sql);

}



    public function rooms() {
        return $this->hasMany('App\Models\Room', 'objectId');
    }

    public function categoryReviews() {
        return $this->belongsToMany('App\Models\ReviewCategory', 'reviews', 'objectId', 'typeUserReviewId');
    }

    public function reviews() {
        return $this->hasMany('App\Models\Review', 'objectId');
    }

    public function objectPluses() {
        return $this->belongsToMany('App\Models\ObjectsTypesPluses', 'objects_objects_types_pluses', 'objectId', 'objectTypePlusesId');
    }

    public function objectBesides() {
        return $this->belongsToMany('App\Models\BesideObject', 'objects_beside', 'objectId', 'typeBesideId');
    }

    public function objectCategories() {
        return $this->belongsTo('App\Models\ObjectsCategories', 'objectCategoryId', 'id');
    }

    public function objectCategory() {
        return $this->hasOne('App\Models\ObjectsCategories', 'id', 'objectCategoryId');
    }

    public function serviceRelation() {
        return $this->belongsToMany('App\Models\ObjectServise', 'objects_relation_objects_servises', 'objectId' ,'objectServiceId');
    }

    public function getListObjectsWithoutCityAndCategory($presentDay, $arrayIdCities) {
        $sql = 'SELECT MAX(event_calendar.`valuePercent`) as valuePercent, event_calendar.`price`, objects.name AS objectName, cities.name AS cityName, 
                objects.id AS objectId, objects.`subdomain`, objects.image 
                FROM cities, event_calendar, objects
                WHERE objects.`parentCityId` = cities.id AND cities.id = event_calendar.`cityId` AND event_calendar.date = '. $presentDay .' AND objects.id = event_calendar.`objectId`
                AND cities.id IN ('. implode(',', $arrayIdCities) .')
                GROUP BY cities.id
                LIMIT 4';

        return DB::select($sql);
    }


    public function getInfoByObjectId($objectId) {
        if(!Validate::id($objectId)) throw new Exception('Exception: invalid id');
        $infoObject = DB::table('objects as o')->select('o.name', 'o.phone', 'o.email', 'o.indexMail', 'o.street', 'o.numberHouse', 'o.numberApartment', 'o.fax', 'o.contactName', 'o.objectCategoryId', 'o.cityId as cityId', 'r.id as regionId', 'c.id as countryId')
            ->leftJoin('cities', 'o.cityId', '=', 'cities.id')
            ->leftJoin('regions as r', '.cities.regionId', '=', 'r.id')
            ->leftJoin('countries as c', 'r.countryId', '=', 'c.id')
            ->where('o.id', $objectId)
            ->get()
            ->first();
        return $infoObject;
    }

    public function getImageAttribute($image) {

        if($image) {
            $newImage = explode('.', $image);
            $type = $newImage[1];
            return $newImage[0].'_object.'.$type;
        }

    }
    
    public function updateOrInsertRowInfoByObjectId($data, $objectId, $type) {
        
        $data['reservationPhone'] = preg_replace('/[^0-9]/', '', $data['reservationPhone']);
        
        if($data['bookkeepingPhone']) {
            $data['bookkeepingPhone'] = preg_replace('/[^0-9]/', '', $data['reservationPhone']);
        }
        if($data['reservationPhoneSms']) {
            $data['reservationPhoneSms'] = preg_replace('/[^0-9]/', '', $data['reservationPhoneSms']);    
        }
        
        if($data['reservationFax']) {
            $data['reservationFax'] = preg_replace('/[^0-9]/', '', $data['reservationFax']);    
        }
        
        if($data['bookkeepingFax']) {
            $data['bookkeepingFax'] = preg_replace('/[^0-9]/', '', $data['bookkeepingFax']);    
        }

        
        if(!Validate::id($objectId)) throw new Exception('Exception: invalid id');
        if(strlen($type) > 0 && ($type != 'insert' && $type != 'update')) throw new Exception('Exception: invalide type');
        if(!strlen($data['reservationPeopleFio']) > 0) throw new Exception('Exception: invalide reservation FIO');
        if(!Validate::phone($data['reservationPhone'])) throw new Exception('Exception: invalide reservation phone');
        if(!strlen($data['bookkeepingPeopleFio']) > 0) throw new Exception('Exception: invalide bookkeping FIO');
        if(!Validate::phone($data['bookkeepingPhone']) > 0) throw new Exception('Exception: invalide bookkeping phone');
        if(!strlen($data['legalName']) > 0) throw new Exception('Exception: invalide legalName');
        if(!strlen($data['typeOwn1']) > 0) throw new Exception('Exception: invalide typeOwn1');
        if($data['checkingAccount'] && !Validate::unsignedInt($data['checkingAccount'])) throw new Exception('Exception: invalide checkingAccount');
        if($data['egrpou'] && !Validate::unsignedInt($data['egrpou'])) throw new Exception('Exception: invalide egrpou');
        if($data['identicationCodeLegal'] && !Validate::unsignedInt($data['identicationCodeLegal'])) throw new Exception('Exception: invalide identicationCodeLegal');
        
        
        $data = Validate::nohtml($data);
        Validate::quote($data);

        $arrayData = [
                'reservationPeopleFio' => $data['reservationPeopleFio'],
                'reservationPeoplePosition' => $data['reservationPeoplePosition'],
                'reservationPhone' => $data['reservationPhone'],
                'reservationPhoneSms' => $data['reservationPhoneSms'],
                'reservationFax' => $data['reservationFax'],
                'reservationEmail' => $data['reservationEmail'],
                'bookkeepingPeopleFio' => $data['bookkeepingPeopleFio'],
                'bookkeepingPhone' => $data['bookkeepingPhone'],
                'bookkeepingFax' => $data['bookkeepingFax'],
                'bookkeepingEmail' => $data['bookkeepingEmail'],
                'legalName' => $data['legalName'],
                'typeOwn1' => $data['typeOwn1'],
                'typeOwn2' => $data['typeOwn2'],
                'checkingAccount' => $data['checkingAccount'],
                'additionalAccount' => $data['additionalAccount'],
                'legalAddress' => $data['legalAddress'],
                'egrpou' => $data['egrpou'],
                'identicationCodeLegal' => $data['identicationCodeLegal'],
                'numberNds' => $data['numberNds']
            ];
            
        if($type == 'insert') {
            $arrayData['objectId'] = $objectId;
            DB::table('general_infromation_objects')->$type($arrayData);
        }
        else {
            DB::table('general_infromation_objects')->where('objectId', $objectId)->$type($arrayData);
        }
        
        
    }
    
    
    public function getListObjectsByName($objectName) {
        $listObjects = DB::table('objects as o')
            ->select('o.id', 'o.name', 'c.name as cityName', 'r.name as regionName')
            ->join('cities as c', 'c.id', '=', 'o.cityId')
            ->join('regions as r', 'r.id', '=', 'c.regionId')
            ->leftJoin('confirmed_object as co', 'co.objectId', '=', 'o.id')
            ->where('o.name', 'like', '%'. $objectName .'%')
            ->get();
        return $listObjects;
    }
    
    public function queryForChangeStatusConfirmedAccount($objectId) {
        if(!Validate::id($objectId)) throw new Exception('Exception: invalid id');
        DB::table('confirmed_object')->insert([
            'creationDate' => time(),
            'objectId' => $objectId
        ]);
    }
    
    public static function getInfoByConfirmedAccount($objectId) {
        if(!Validate::id($objectId)) throw new Exception('Exception: invalid id');
        
        $isConfirmed = DB::table('confirmed_object')->select('confirmedAccount', 'objectId')
            ->where('objectId', $objectId)
            ->first();
        return $isConfirmed;
        
    }
    
    public function getListTypeObject() {
        $listTypeObject = DB::table('objects_categories')->select('id', 'name')->get();
        return $listTypeObject;
    }
    
    
    public function getListObjectsAdmin() {
        
        $objects = DB::table('objects as o')
            ->select('o.id', 'o.organizationName', 'o.name', 'o.lastUpdater', 'co.creationDate', 'co.confirmedAccount')
            ->leftJoin('confirmed_object as co', 'co.objectId', '=', 'o.id')
            ->get();
        
        return $objects;
        
    }
    
    public function getInfoByObjectIdAdmin($objectId) {
        $object = DB::table('objects as o')->select('o.id as objectId', 'o.name', 'o.url', 'o.image', 'o.phone', 'o.email', 'o.fax', 'o.stars', 'o.checkIn',
            'o.checkOut', 'o.infoAboutObject', 'o.locationObject', 'o.infoAboutTravel', 'o.infoExtraPlaces', 'o.infoAdditionalPayments',
            'o.restaurantAndBarsInfo', 'o.infoMedication', 'c.name as cityName', 'o.street', 'o.numberHouse', 'o.numberApartment',
            'o.subdomain', 'o.longitudeGps', 'o.latitudeGps', 'countries.name as countryName', 'oc.name as categoryName', 'o.indexMail', 'o.contactName', 'gio.*')
            ->leftJoin('cities as c', 'c.id', '=', 'o.cityId')
            ->leftJoin('countries', 'countries.id', '=', 'c.countryId')
            ->leftJoin('general_infromation_objects as gio', 'gio.objectId', '=', 'o.id')
            ->join('objects_categories as oc', 'oc.id', '=', 'o.objectCategoryId')
            ->where('o.id', $objectId)
            ->first();
        
        return $object;
    }
    
    public function confirmedObjectAdmin($objectId) {
        if(!Validate::id($objectId)) throw new Exception('Exception: invalid id');
        
        DB::table('confirmed_object')->where('objectId', $objectId)->update([
            'confirmedAccount' => 1,
            'creationDate' => time()
        ]);
    }
    
    
    public function createApplicationForObject($data) {
        DB::table('application_object')->insert([
            'creationDate' => time(),
            'updateDate' => time(),
            'name' => $data['name'],
            'object_name' => $data['object_name'],
            'email' => $data['email'],
            'is_send' => $data['is_send'],
            'phone' => $data['phone'],
            'city' => $data['city'],

        ]);
    }
    
    public function getListApplicationObject() {
        $listApplication = DB::table('application_object')->select('id', 'updateDate', 'name', 'email', 'phone', 'city', 'object_name', 'is_send')->get();
        return $listApplication;
    }
    
    
   public function getListObjects($city = false, $region = false, $dateFromStr, $dateToStr, $guest = 1, $minPrice = false, $maxPrice = false, $typeObjectArray = false, $foodArray = false,
                                  $servicesArray = false, $plusesArray = false, $starsArray = false, $sort = false, $countPeriod) {
        $sqlTable = 'objects, event_calendar';

        $sqlRaiting = '';
       switch ($sort) {
           case 'recommendation':
               $sort = '';
               break;
           case 'price-up':
               $sort = ' order by table1.minPrice asc';
               break;
           case 'price-down':
               $sort =' order by table1.minPrice desc';
               break;
           case 'raiting':
               $sqlTable.= '';
               $sort = ' order by table1.avgValue desc';
               break;
           default:
               $sort = '';
               break;
       }

        $sqlCity = '';
        if($city) {
            $sqlCity = ' and event_calendar.cityIdDaugher = '. $city->id;
        }

        $sqlRegion = '';
        if($region) {
            $sqlRegion = ' and event_calendar.regionId = '.$region->id;
        }
        $sqlMinPrice = '';
        if($minPrice) {
            $sqlMinPrice = ' and event_calendar.price >= '. $minPrice;
        }

        $sqlMaxPrice = '';
        if($maxPrice) {
            $sqlMaxPrice = ' and event_calendar.price <= '. $maxPrice;
        }

       $sqlTypeObject = '';
       if($typeObjectArray && is_array($typeObjectArray)) {
           $sqlTypeObject = ' and objects.objectCategoryId in( '. implode(",", $typeObjectArray) . ')';
       }

       $sqlFood = '';
       if($foodArray && is_array($foodArray)) {
           $sqlTable.= ', food_types_relation_object';
           $sqlFood = ' and food_types_relation_object.objectId = objects.id and food_types_relation_object.foodTypeId in ('. implode(",", $foodArray) .')';
       }

       $sqlServices = '';
       if($servicesArray && is_array($servicesArray)) {
           $sqlTable.= ', objects_relation_objects_servises';
           $sqlServices = ' and objects_relation_objects_servises.objectId = objects.id and objects_relation_objects_servises.objectServiceId in ('. implode(",", $servicesArray) .')';
       }

       $sqlPluses = '';
       if($plusesArray && is_array($plusesArray)) {
           $sqlTable.= ', objects_objects_types_pluses';
           $sqlPluses = ' and objects_objects_types_pluses.objectId = objects.id and objects_objects_types_pluses.objectTypePlusesId in ('. implode(",", $plusesArray) .')';
       }

       $sqlStars = '';
       if($starsArray && is_array($starsArray)) {
           $sqlStars = ' and objects.stars in ('. implode(",", $starsArray) .')';
       }

        $sql = 'SELECT table1.objectId, table1.minPrice, table1.image, table1.objectName, table1.address, table1.infoAboutObject, table1.stars, table1.subdomain, table1.valuePercent, 
                table1.longitudeGps, table1.latitudeGps
                FROM (
                SELECT objects.id AS objectId, COUNT(event_calendar.`typeRoomId`) AS countCalendar, 
                MIN(event_calendar.`price`) AS minPrice, objects.image, objects.name as objectName, objects.address, objects.infoAboutObject, 
                objects.stars, objects.subdomain, event_calendar.valuePercent, objects.avgValue, objects.longitudeGps, objects.latitudeGps
                FROM '. $sqlTable .'
                WHERE event_calendar.date >= '.$dateFromStr .' AND event_calendar.date < '. $dateToStr .'
                AND event_calendar.`amountPersonRoom` >= '. $guest . $sqlCity . $sqlRegion . $sqlMinPrice . $sqlMaxPrice . $sqlTypeObject . $sqlFood . $sqlServices . $sqlPluses . $sqlStars .'
                AND event_calendar.`objectId` = objects.id '. $sqlRaiting .'
                AND event_calendar.`closeRoom` = 0 AND event_calendar.`reservationId` = 0 AND event_calendar.`reservedRoomId` = 0
                GROUP BY event_calendar.`typeRoomId`
                ) AS table1 WHERE table1.countCalendar >= '. $countPeriod . ' GROUP BY table1.objectId '. $sort;

        return DB::select($sql);

   }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
