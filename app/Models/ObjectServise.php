<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Helpers\ValidateQuery;

class ObjectServise extends Model
{
    protected $table = 'objects_servises';

    public function getObjectsServisesListByCountObject($countryId = false, $cityId = false,
                                                        $regionId = false, $dateFrom = false, $dateTo = false, $person = false)
    {
        $query = DB::table('objects_servises AS os')
            ->select('os.id', 'os.name', DB::raw('count(DISTINCT o.id) as countObject'))
            ->join('objects_relation_objects_servises AS oros', 'os.id', '=', 'oros.objectServiceId')
            ->join('objects AS o', 'oros.objectId', '=', 'o.id')
            ->join('rooms AS r', 'o.id', '=', 'r.objectId')
            ->leftJoin('confirmed_object as co', 'co.objectId', '=', 'o.id')
            ->where('co.confirmedAccount', 1);

        $query = ValidateQuery::validateQueryForFilters($query, $countryId, $cityId,
            $regionId, $dateFrom, $dateTo, $person);

        $objectsServises = $query->groupBy('os.id')->get();
        return $objectsServises;
    }
    public function serviceRelation() {
        return $this->belongsToMany('App\Models\ObjectHotel', 'objects_relation_objects_servises', 'objectServiceId' ,'objectId');
    }
    
    
    
}
