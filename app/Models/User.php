<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Helpers\Validate;

class User extends Model
{
    protected $table = 'users';
    protected $guarded = [];
    public $timestamps = false;
    public $remember_token=false;
    
    public function client()
    {
        return $this->hasOne('App\Models\Clients', 'id', 'clientId');
    }
    
    public function object() {
        return $this->hasOne('App\Models\ObjectHotel', 'id', 'objectId');
    }
    
    public function getInfoByUserId($userId) {
        $user = DB::table('users as u')->select('u.email', 'u.phone')->where('id', $userId)->first();
        return $user;
    }
    
    public function updateInfoUser($data, $userId) {
        DB::table('users as u')->where('id', $userId)->update([
            'u.phone' => $data['phone'],
            'u.email' => $data['email']
        ]);
    }
    
    public function changePasswordAccount($password, $userId) {
        if(!$password) throw new exception('Invalid data');
        if(!Validate::id($userId)) throw new exception('Invalid userId');
        DB::table('users')->where('id', $userId)->update([
            'password' => bcrypt($password)
        ]);
    }
    
    public function createActivatePasswordRow($userId, $code) {
        if(!$code) throw new exception('Invalid data');
        if(!Validate::id($userId)) throw new exception('Invalid userId');
        
        DB::table('password_activated')->insert([
            'userId' => $userId,
            'code' => $code
        ]);   
    }
    
    public function getInfoUserPassword($userId, $code) {
        if(!$code) throw new exception('Invalid data');
        if(!Validate::id($userId)) throw new exception('Invalid userId');
        
        $result = DB::table('password_activated')->select('id')->where('userId', $userId)->where('code', $code)->first();
        
        return $result;
    }
    
    public function removeInfoUserPassword($userId, $code) {
        if(!$code) throw new exception('Invalid data');
        if(!Validate::id($userId)) throw new exception('Invalid userId');
        
        DB::table('password_activated')->where('userId', $userId)->where('code', $code)->delete();
    }
    
    
    public function removeNoActiveUser() {
        
        $currentDayMinusWeek = strtotime(date('d.m.Y')) - (86400 * 7);
        
        DB::table('users')->where('activated', 0)->where('creationDate', '<=', $currentDayMinusWeek)->delete();

    }
    
    public function createSubscribe($email) {
        DB::table('email_user_subscribe')->insert([
            'email' => $email,
            'creationDate' => time()
        ]);
    }
    
    
    
    
}
