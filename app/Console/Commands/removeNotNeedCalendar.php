<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\EventCalendar;
use Mail;

class removeNotNeedCalendar extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'removeNotNeedCalendar:user';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Удаление не нужных дат календаря';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(EventCalendar $eventCalendar)
    {
        $today = strtotime(date('d.m.Y'));
        dd($today);
    }
}
