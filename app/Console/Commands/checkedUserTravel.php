<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Reservation;
use Illuminate\Support\Facades\DB;
use Mail;

class checkedUserTravel extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'checkedTravel:user';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Проверка пользователя на прибытие в отель спустя 3 дня';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $threeDays = 86400*3;
        $currentDate = time() - $threeDays;
        $listReservation = Reservation::where('isComment', 0)->where('isPast', 1)
            ->where('endReservationDate', '<', $currentDate)
            ->where('statusId', 3)
            ->get();


        foreach ($listReservation as $reservation) {

            $codeReview = DB::table('code_reviews_user')->where('reservationId', $reservation->id)->first();
            $link = url('/'). '/feedback/?email='.$reservation->visitorEmail.'&code='.$codeReview->code;
            Mail::send('mail.review-user', array('link' => $link, 'email' => $reservation->visitorEmail), function($message) use ($reservation)
            {
                $message->to($reservation->visitorEmail)->subject('Отзыв о проживании');
            });

        }

        echo 'success';

    }
}
