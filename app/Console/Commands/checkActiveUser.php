<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\User;

class checkActiveUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'checkActive:user';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check User on active/';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(User $userModel)
    {
        $userModel->removeNoActiveUser();
        echo 'Я все сделал)';
    }
}
