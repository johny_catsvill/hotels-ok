<?php

namespace App\Providers;

use App\Models\ObjectHotel;
use Illuminate\Support\Facades\Artisan;
use Request;
use Illuminate\Support\ServiceProvider;
use View;
use App\Models\Wishe;
use Illuminate\Support\Facades\URL;
use App\Models\Page;
use App\Models\City;
use App;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {

//        if(strpos(Request::url(), '/uk') && App::getLocale() != 'uk') {
//            App::setLocale('uk');
//            return redirect('/setlocale/uk');
//        }
//        else{
//            return redirect('/setlocale/ru');
//        }

        if(Request::has('destroy')) {
            Artisan::call('cache:clear');
            Artisan::call('view:clear');
        }

        View::share('seo', $this->getSeoPage());
        View::share('procentPromocode', $this->procentPromocode());
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    public function getSeoPage() {

        $data = false;

        $arr = explode('/', Request::path()); // для сео новостей. так как из за пагинации сео не подгружаются
        $page = Page::where('slug', $arr[0])->first();
        if($page) {
            return $page;
        }


        return $data;

    }

    public function procentPromocode() {
        $temp = App\Models\Bonus::find(2)->procent;
        return $temp;
    }

}
