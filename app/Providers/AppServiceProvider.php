<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use DB;
use Blade;
use Illuminate\Support\Facades\Auth;
use App\Models\ObjectHotel;
use App\Models\Tickets;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {



        Blade::directive('messages', function() {
            $objectId = Auth::user()->objectId;
            $ticketsModel = new Tickets();
            $tickets = $ticketsModel->getListTickets($objectId);
            $count = 0;
            foreach($tickets as $ticket) {
                if(!$ticket->reading) {
                    $count++;
                }
            }
            return $count;
        });
        
        
        Blade::directive('countDayInPeriod', function($timestampStart = false, $timestampStop = false){
            try {
                $start = new \DateTime();
                $start->setTimestamp(strtotime($timestampStart));
                $stop = new\ DateTime();
                $stop->setTimestamp(strtotime($timestampStop));

                $tmp = $start->getTimestamp();
                $result = array();

                while ($stop->getTimestamp() >= $tmp) {
                    $date = new \DateTime();
                    $date->setTimestamp($tmp);

                    $result[$date->format("Y-m-d")] = array(
                        'startTime' => $date->format("H:i"),
                        'stopTime'  => $date->setTime(23, 59, 59)->format('H:i')
                    );

                    $date->setTime(0, 0, 0)->modify("+1 day");
                    $tmp = $date->getTimestamp();
                }
                if (isset($date)) {
                    if ($start->format('Ymd') == $stop->format('Ymd')) {
                        $result[$start->format('Y-m-d')]['stopTime'] = $stop->format('H:i');
                    } elseif ($stop->format('H:i') != $result[$start->format('Y-m-d')]['stopTime']) {
                        $result[$stop->format('Y-m-d')]['stopTime'] = $stop->format('H:i');
                    }
                }
                $count = count($result);
                return $count;
            } catch (Exception $e) {
                $count = count($result);
                return $count;
            }
        });
        
        
        
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
