<?php
/**
 * Created by PhpStorm.
 * User: Nick
 * Date: 21.04.2017
 * Time: 10:05
 */

namespace App\Helpers;


class ValidateQuery
{
    public static function validateQueryForFilters($query, $countryId, $cityId,
                                                   $regionId, $dateFrom, $dateTo, $person)
    {
        if ($countryId) {
            $query->where('o.countryId', $countryId);
        }
        if ($cityId) {
            $query->where('o.cityId', $cityId);
        }
        if ($regionId) {
            $query->where('o.regionId', $regionId);
        }
        if ($person) {
            $query->where('r.amountPerson', '>=', $person);
        }
        return $query;
    }

}