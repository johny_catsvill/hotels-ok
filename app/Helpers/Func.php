<?php

namespace App\Helpers;

class Func
{
    
    public static $arrayMonths = [
        '01' => 'Января',
        '02' => 'Февраля',
        '03' => 'Марта',
        '04' => 'Апреля',
        '05' => 'Мая',
        '06' => 'Июня',
        '07' => 'Июля',
        '08' => 'Августа',
        '09' => 'Сентября',
        '10' => 'Октября',
        '11' => 'Ноября',
        '12' => 'Декабря',
    ];

    public $days = [
        '0' => 'Воскресенья',
        '1' => 'Понедельник',
        '2' => 'Вторник',
        '3' => 'Среда',
        '4' => 'Четверг',
        '5' => 'Пятница',
        '6' => 'Суббота',
    ];

    public static function getDay($time) {
        $date = date('d.m.Y', $time);
        $dateExplode = explode('.', $date);
        return $dateExplode[0].' '. Func::$arrayMonths[$dateExplode[1]];
    }
    
    public static function arrayMergeTwoFunction($array1, $array2)
    {
        $newArray1 = explode(',', $array1);
        $newArray2 = explode(',', $array2);

        $newArrayMerge = array_combine($newArray1, $newArray2);
        return $newArrayMerge;
    }

    public function translit($s) {
        $s = (string) $s; // преобразуем в строковое значение
        $s = strip_tags($s); // убираем HTML-теги
        $s = str_replace(array("\n", "\r"), " ", $s); // убираем перевод каретки
        $s = preg_replace("/\s+/", ' ', $s); // удаляем повторяющие пробелы
        $s = trim($s); // убираем пробелы в начале и конце строки
        $s = function_exists('mb_strtolower') ? mb_strtolower($s) : strtolower($s); // переводим строку в нижний регистр (иногда надо задать локаль)
        $s = strtr($s, array('а'=>'a','б'=>'b','в'=>'v','г'=>'g','д'=>'d','е'=>'e','ё'=>'e','ж'=>'j','з'=>'z',
            'и'=>'i','й'=>'y','к'=>'k','л'=>'l','м'=>'m','н'=>'n','о'=>'o','п'=>'p','р'=>'r','с'=>'s','т'=>'t',
            'у'=>'u','ф'=>'f','х'=>'h','ц'=>'c','ч'=>'ch','ш'=>'sh','щ'=>'shch',
            'ы'=>'y','э'=>'e','ю'=>'yu','я'=>'ya','ъ'=>'','ь'=>''));
        $s = preg_replace("/[^0-9a-z-_ ]/i", "", $s); // очищаем строку от недопустимых символов
        $s = str_replace(" ", "-", $s); // заменяем пробелы знаком минус
        return $s; // возвращаем результат
    }

    public function countPercent($currentValue, $maxValue) {
        return ($currentValue * 100) / $maxValue;
    }
    
    public function translateToDate($time) {
        $data = [];
        $newDate = date('d.m.Y', $time);
        $newDateArray = explode('.', $newDate);
        $newDateWithMonth = $newDateArray[0].' '.Func::$arrayMonths[$newDateArray[1]].' '.$newDateArray[2].' год';
        return $newDateWithMonth;
    }
    
    
    public function explodeArrays($array) {
        return explode(',', $array);
    }
    

    public static function getDeclineCity($city = '')
    {

        $ending =
            [
                //группа с окончанием А
                '[к][а]' => 'ки',
                '[а]' => 'ы',

                '[в]' => 'ва',
                '[д]' => 'да',

                //группа с окончанием Е
                '[ь][е]' => 'ья',
                '[о][е]' => 'ого',

                '[и]' => 'ок',

                //группа с окончанием Й
                '[и][й]' => 'ого',
                '[а][й]' => 'ая',

                '[к]' => 'ка',
                '[н]' => 'на',
                '[о]' => 'а',
                '[р]' => 'ра',
                '[т]' => 'та',

                '[е][ц]' => 'ца',

                '[ь]' => 'я',

                //группа с окончанием Ы
                '[с][ы]' => 'с',
                '[ы]' => 'ов',

                //группа с окончанием Я
                '[ы][я]' => 'ыи',
                '[и][я]' => 'ии',
            ];

        foreach ($ending as $key => $val)
        {
            if(preg_match("/([а-яА-ЯёЁ]+)($key$)/u", $city))
            {
                $city =  preg_replace("/([а-яА-ЯёЁ]+)($key$)/u", '${1}'.$val, $city);
                return $city;
            }
        }
        return $city;
    }

    // Принимает первым параметром строку
    // Вторым количество до какого максимального количества слов обрезать
    public static function getOpening($full, $words = 100, $str = 180) {
        $string = preg_replace('/\s|(&nbsp;)/m',' ',$full);
        $string = strip_tags( html_entity_decode($string) );
        $string = preg_replace('/\s|(&nbsp;)/m',' ',$string);
        $string = trim($string);
        return self::truncateStr($string, $words, $str);
    }

    // Принимает первым параметром строку
    // Вторым количество до какого максимального количества слов обрезать
    // Третий параметр максимальное количество символов в строке до которого обрезать
    // и является преоритетнее второго параметра
    public static function truncateStr($string, $count, $str, $suffix = '...')
    {
        $words = preg_split('/(\s+)/u', trim($string), null, PREG_SPLIT_DELIM_CAPTURE);
        $result = [];
        $i = 0;
        foreach ($words as $key => $word)
        {
            if($i <= $str)
            {
                $i += strlen($word);
                $result[$key] = $word;
            }
        }
        array_pop($result);
        $string = implode('', array_slice($result, 0, ($count * 2) - 1)) . $suffix;
        $string = ($string == $suffix) ? '' : $string;

        return $string;
    }
        
        
        
}
    

