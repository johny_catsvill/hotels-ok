<?php

namespace App\Http\Middleware;

use Closure;
use Validator;

class ByCalendar
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $requestAll = $request->all();

        $validator = Validator::make($requestAll, [
            'roomId' => 'required|integer|exists:rooms,id',
        ]);



        if($validator->fails()) {
            return redirect()->route('calendar');
        }

        return $next($request);
    }
}
