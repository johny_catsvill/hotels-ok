<?php

namespace App\Http\Middleware;

use Closure;
use Validator;

class ByVerificationRoom
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        if($request->isMethod('post')) {
            
            $requestAll = $request->all();

            $messages = [
                'name.required' =>                      'Поле "Имя комнаты" является обязательным!',
                'countRoom.required' =>                 'Поле "Количество комнат" является обязательным!',
                'countRoom.integer' =>                  'Поле "Количество комнат" должно состоять из цифр!',
                'typePayment.required' =>               'Поле "Тип оплаты" является обязательным!',
                'typePayment.in' =>                     'Неверное значение для поля "Тип оплаты"!',
                'price.required' =>                     'Поле "Стоимость" является обязательным!',
                'price.integer' =>                      'Поле "Стоимость" должно состоять из цифр!',
                'amountPerson.required' =>              'Поле "Количество мест в номере" является обязательным',
                'amountPerson.integer' =>               'Поле "Количество мест в номер" должно состоять из цифр!',
                'pluse.array' =>                        'Выберите несколько плюсов!',
                'pluse.required' =>                     'Поле с услугами является обязательным!',
                'infoAboutRoom.required' =>             'Поле "Информация о комнате должно быть обязательным"',
                'infoAboutRoom.min' =>                  'Поле "Информация о комнате должно состоять минимум из 30 символов!"',
//                'infoAboutRoom.max' =>                  'Поле "Информация о комнате должно состоять максиум до 300 символов!"',
                'bedTypeId.required' =>                 'Поле "Тип кровати" является обязательным!',
                'countBed.required' =>                 'Поле "Количество кроватей" является обязательным!'
            ];
            
            $validator = Validator::make($requestAll, [
                'name' => 'required|string',
                'countRoom' => 'required|integer',
                'typePayment' => 'required|in:priceAboutRoom,priceAboutPlace',
                'price' => 'required|integer',
                'amountPerson' => 'required|integer',
                'pluse' => 'required|array',
                'bedTypeId' => 'required|array',
                'countBed' => 'required|array',
                'infoAboutRoom' => 'required|min:30'
            ], $messages);
            
            if($validator->fails()) {
                return redirect()->back()->withInput()->withErrors($validator->errors());
            }
            
        }
        
        
        return $next($request);
    }
}
