<?php

namespace App\Http\Middleware;

use App\Helpers\CategoryHelper;
use App\Helpers\Validate;
use Closure;
use Illuminate\Support\Facades\Validator;

class ByCategory
{
    public function handle($request, Closure $next)
    {
        return $next($request);
    }
}
