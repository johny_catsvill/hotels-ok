<?php

namespace App\Http\Middleware;

use Closure;
use Validator;

class ValidateDataReservation
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        
        $requestAll = $request->all();
//        dd($requestAll);
        $validator = Validator::make($requestAll, [
            
            'hotelsId' => 'required|integer',
            'roomId' => 'required|integer',
            'dateFrom' => 'required|date_format:"d.m.Y"',  
            'dateTo' => 'required|date_format:"d.m.Y"|after:tomorrow',  
            'nameUser' => 'required|string|min:2',
            'lastNameUser' => 'required|string|min:2',
            'emailUser' => 'required|email|min:3',
            'telUser' => 'required|min:7',
            'agreeUser' => 'required',
            
        ]);
        
        
        
       if($validator->fails()) {
//           dd($validator->errors());
           return redirect()->back()->withInput()->withErrors($validator)->with('message', 'Неправильно заполнены поля, попробуйте еще раз!');
       }
        
        return $next($request);
    }
}
