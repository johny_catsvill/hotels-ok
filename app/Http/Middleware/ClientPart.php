<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\Models\ObjectHotel;

class ClientPart
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
//        $user = Auth::user();
//        if(isset($user->objectId) && $user->objectId) {
//            $objectId = Auth::user()->objectId;
//            $subdomain = ObjectHotel::find($objectId)->subdomain;
//            if($objectId) {
//                return redirect()->route('domain-home', $subdomain);
//            }
//        }
        
        return $next($request);
    }
}
