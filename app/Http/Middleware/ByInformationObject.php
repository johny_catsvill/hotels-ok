<?php

namespace App\Http\Middleware;

use Closure;
use Validator;
use App\Traits\timeTraits;

class ByInformationObject
{
    use timeTraits;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->isMethod('post')) {
            
            
            $requestAll = $request->all();
            $requestAll['checkIn'] = $this->translateToSeconds($requestAll['checkIn']);
            $requestAll['checkOut'] = $this->translateToSeconds($requestAll['checkOut']);
//            dd($requestAll);
            if($request->hasFile('main_img')) {
                $file = $request->file('main_img');
                if($file->getClientSize() > 5 * 1024 * 1024) {
                    return redirect()->back()->withInput()->withErrors('Слишком большое заглавное изображение! Максимальный размер 5мб. ');
                }   
            }
            
            $messages = [
//                'name.required' =>                      'Поле «Имя отеля» обязательное к заполнению!',
//                'name.min' =>                           'Поля «Имя отеля» должно состоять минимум из 2 символов.',
//                'name.max' =>                           'Поля «Имя отеля» должно состоять максимально из 20 символов.',
                'address.required' =>                   'Поле «Адресс» обязательное к заполнению!',
                'stars.required' =>                     'Поле «Количество звёзд объекта» обязательное!',
                'countRooms.required' =>                'Поле «Количество комнат» обязательное к заполнению!',
                'countRooms.integer' =>                 'Поле «Количество комнат» должно состоять только из чисел.',
                'checkIn.required' =>                   'Поле «Регистрация заезда» обязательное к заполнению!',
                'checkOut.required' =>                  'Поле «Регистрация отьезда» обязательное к заполнению!',
                'receptionRoundClock.required' =>       'Поле «Стойка регистрации» обязательное!',
                'prePayment.required' =>                'Поле «Поселение без предоплаты» обязательное!',
                'prePayment.integer' =>                 'Поле «Поселение без предоплаты» должно состоять только из чисел.',
                'typePayment.required' =>               'Поле «Способ оплаты» обязательное!',
                'touristTax.required' =>                'Поле «Туристический сбор» обязательное!',
//                'touristTaxValue.between' =>            'Значение "Туристического сбора" не должно быть меньше нуля, а так же превышать 1%.',
                'pluses.array' =>                      'Выберите услуги которые присутствуют на Вашем объекта. Это крайне важно для большего шанса аренды именно у Вас!',
                'infoAboutObject.required' =>           'Поле «Описание объекта» обязательное к заполнению!',
                'infoAboutObject.min' =>                'Поле «Описание объекта» должно состоять минимум из 100 символов.',
                'longitudeGps.required' =>              'Укажите адресс объекта для определения координат.',
                'locationObject.required' =>            'Поле «Месторасположение» обязательное к заполнению!',
                'latitudeGps.required' =>               'Укажите адресс объекта для определения координат.',
                'foodTypes.required' =>                 'Укажите типы питания в объекте!'
                
            ];
           
            $validator = Validator::make($requestAll, [
//                'name' =>                           'required|string|min:2|max:20',
//                'cityId' =>                         'required|integer|exists:cities,id',   /* метод проверяет на существовании записи в БД с таким идентификатором */ 
                'address' =>                        'required|string|min:2',
                'stars' =>                          'required|integer',
                'countRooms' =>                     'required|integer',
                'checkIn' =>                        'required|integer',
                'checkOut' =>                       'required|integer',
                'receptionRoundClock' =>            'required|boolean',
                'prePayment' =>                     'required|integer',
                'typePayment' =>                    'required|in:payHotelsOk,creditData,payInHotel',
                'touristTax' =>                     'required|in:IncludedPrice,addPrice',
//                'touristTaxValue' =>                'required_if:touristTax,addPrice|between:0,1',
                'pluses' =>                         'array',
                'infoAboutObject' =>                'required|min:100',
                'locationObject' =>                 'required',
                'typeObject' =>                     'array',
                'nameTypeObjectBeside' =>           'array',
                'distanceBesideObject' =>           'array',
                'longitudeGps' =>                   'required',
                'latitudeGps' =>                    'required',
                'foodTypes' =>                      'required|array'
                
            ], $messages);
            
            if($validator->fails()) {
                return redirect()->back()->withInput()->withErrors($validator->errors());
            }
            
            
            
            
            
        }
        
        return $next($request);
    }
}
