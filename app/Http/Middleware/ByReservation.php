<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Validator;

class ByReservation
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $requestAll = $request->all();
        if(isset($requestAll['arrival']) && isset($requestAll['departure'])) {

        $validator = Validator::make($requestAll, [
            'hotelId' => 'required|integer',
            'hotel' => 'required|string',
            'hash' => 'required',
            'arrival' => 'required|date_format:"d.m.Y"',
            'departure' => 'required|date_format:"d.m.Y"|after:tomorrow'
        ]);

        }
        else {
            return redirect()->back()->withErrors(['Некорректная информация. Попробуйте еще раз']);
        }
        

       if($validator->fails()) {
           return redirect()->back();
       }
        
        if($requestAll['arrival'] < date("d.m.Y")) {
           return redirect()->back();
       }
        
        return $next($request);
    }
}
