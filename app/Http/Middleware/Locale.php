<?php

namespace App\Http\Middleware;

use Closure;
use App;
use Session;
use Request;

class Locale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public static function getLocale()
    {

        $uri = Request::path();


        $segmentsURI = explode('/',$uri);

        if (!empty($segmentsURI[0]) && in_array($segmentsURI[0], config('app.locales'))) {

            if ($segmentsURI[0] != config('app.locale')) return $segmentsURI[0];

        }
        return null;
    }


    public function handle($request, Closure $next)
    {


        $locale = self::getLocale();

        if($locale) App::setLocale($locale);


        else App::setLocale(config('app.locale'));
        Session::put('locale', $locale);

        return $next($request);
    }
}
