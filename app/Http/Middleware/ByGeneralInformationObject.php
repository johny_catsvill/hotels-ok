<?php

namespace App\Http\Middleware;

use Closure;
use Validator;

class ByGeneralInformationObject
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        if($request->isMethod('post')) {
            $requestAll = $request->all();
//            dd($requestAll);
            
            $messages = [
                'name.required' =>                          'Поле "Название объекта" обязательное!',
                'name.min' =>                               'Поле "Название объекта" должно состоять минимум из 2 цифр!',
                'objectCategoryId.required' =>              'Поле "Тип объекта" обязательное!',
                'objectCategoryId.exists' =>                'Неккоректное значение поля "Тип объекта"',
                'indexMail.required' =>                         'Поле "Индекс" обязательное!',
//                'index.numeric' =>                          'Поле "Индекс" должно содержать только лишь цифры!',
                'indexMail.min' =>                              'Поле "Индекс" должно состоять минимум из 5 цифр!',
                'indexMail.numeric' =>                              'Поле "Индекс" должно состоять только из цифр!',
                'street.required' =>                        'Поле "Улица" обязательное',
                'numberHouse.required' =>                   'Поле "Номер дома" обязательное',
                'cityId.required' =>                          'Поле "Город" обязательное!',
                'cityId.exists' =>                            'Неккоректное значение поля "Город"!',
                'numberApartment.numeric' =>                'Поле "Номер квартиры" должно состоять из цифр!',
                'phone.required' =>                         'Поле "Телефон" обязательное!',
                'phone.numeric' =>                          'Поле "Телефон" должно содержать только лишь цифры!',
                'phone.min' =>                              'Поле "Телефон" должно состоять минимум из 10р!',
                'contactName.required' =>                   'Поле "Контактное лицо" обязательное!',
                'fax.numeric' =>                            'Поле "Факс" должно содержать только лишь цифры!',
                'email.required' =>                         'Поле "Почтовый адрес" обязательное!',
                'email.email' =>                            'Неккоректные данные в поле "Email"!',
                'reservationPeopleFio.required' =>          'Поле "Отдел Бронирования -> ФИО" обязательное!',
                'reservationPeopleFio.min' =>               'Поле "Отдел Бронирования -> ФИО" должно быть минимум из 6 цифр!',
                'reservationPhone.required' =>              'Поле "Отдел Бронирования -> Телефон" обязательное!',
                'reservationPhone.numeric' =>               'Поле "Отдел Бронирования -> Телефон" должно содержать только лишь цифры!',
                'reservationPhone.min' =>                   'Поле "Отдел Бронирования -> Телефон" должно состоять минимум из 10 цифр!',
                'reservationPhoneSms.numeric' =>            'Поле "Отдел бронирования -> Телефон для смс" должно содержать только лишь цифры!',
                'reservationPhoneSms.min' =>                'Поле "Отдел бронирования -> Телефон для смс" должно состоять минимум из 10 цифр!',
                'reservationFax.numeric' =>                 'Поле "Отдел бронирования -> Факс" должно содержать только лишь цифры!',
                'reservationEmail.email' =>                 'Неккоректные данные в поле "Отдел бронирования -> Email"!',
                'bookkeepingPeopleFio.required' =>          'Поле "Бухгалтерия -> ФИО" обязательное!',
                'bookkeepingPhone.required' =>              'Поле "Бухгалтерия -> Телефон" обязательно!',
                'bookkeepingPhone.numeric' =>               'Поле "Бухгалтерия -> Телефон" должно состоять из цифр!',
                'bookkeepingPhone.min' =>                   'Поле "Бухгалтерия -> Телефон" должно состоять минимум из 10 цифр!',
                'bookkeepingFax.numeric' =>                 'Поле "Бухгалтерия -> Факс" должно состоять из цифр!',
                'bookkeepingEmail.email' =>                 'Неккоректные данные в поле "Бухгалтерия -> Email"!',
                'legalName.required' =>                     'Поле "Юридическое название отеля" обязательное!',
                'typeOwn1.required' =>                      'Поле "Форма собственности 1" обязательное!',
                'typeOwn1.in' =>                            'Неккоректное значение в поле "Форма собственности 1"!',
                'typeOwn2.in' =>                            'Неккоректное значение в поле "Форма собственности 2"!',
                'checkingAccount.required' =>               'Поле "Расчетный счет" обязательное!',
                'checkingAccount.numeric' =>                'Поле "Расчетный счет" должно состоять только из цифр!',
                'additionalAccount.integer' =>              'Поле "Дополнительный счет" должно состоять только из цифр!',
                'legalAddress.required' =>                  'Поле "Юридический адрес" является обязательным!',
                'egrpou.digits' =>                            'Поле "ЕГРПОУ" должно состоять из 8 цифр!',
                'numberNds.digits' =>                        'Поле "Номер свидетельства плательщика НДС" должно состоять из 12 цифр!'
            ];
            
            $requestAll['phone'] = preg_replace('/[^0-9]/', '', $requestAll['phone']);
            $requestAll['fax'] = preg_replace('/[^0-9]/', '', $requestAll['fax']);
            $requestAll['reservationPhone'] = preg_replace('/[^0-9]/', '', $requestAll['reservationPhone']);
            $requestAll['reservationPhoneSms'] = preg_replace('/[^0-9]/', '', $requestAll['reservationPhoneSms']);
            $requestAll['reservationFax'] = preg_replace('/[^0-9]/', '', $requestAll['reservationFax']);
            $requestAll['bookkeepingPhone'] = preg_replace('/[^0-9]/', '', $requestAll['bookkeepingPhone']);
            $requestAll['bookkeepingFax'] = preg_replace('/[^0-9]/', '', $requestAll['bookkeepingFax']);

//            dd($requestAll);
            
            $validator = Validator::make($requestAll, [
                'name' =>                       'required|string|min:2',
                'objectCategoryId' =>           'required|numeric|exists:objects_categories,id',
                'indexMail' =>                     'required|min:5|numeric',
                'street' =>                     'required|string',
                'numberHouse' =>                'required',
                'cityId' =>                     'required|integer|exists:cities,id',
                'numberApartment' =>            $requestAll['numberApartment'] != null ? 'sometimes|numeric' : '',
                'phone' =>                      'required|numeric|min:10',
                'contactName' =>                'required|string',
                'fax' =>                        $requestAll['fax'] != null ? 'sometimes|numeric' : '',
                'email' =>                      'required|email',
                'reservationPeopleFio' =>       'required|string|min:6',
                'reservationPeoplePosition' =>  $requestAll['reservationPeoplePosition'] != null ? 'sometimes|string|min:2' : '',
                'reservationPhone' =>           'required|numeric|min:10',
                'reservationPhoneSms' =>        $requestAll['reservationPhoneSms'] != null ? 'sometimes|numeric|min:10' : '',
                'reservationFax' =>             $requestAll['reservationFax'] != null ? 'sometimes|numeric|min:2': '',
                'reservationEmail' =>           $requestAll['reservationEmail'] != null ? 'sometimes|email' : '',
                'bookkeepingPeopleFio' =>       'required|string',
                'bookkeepingPhone' =>           'required|numeric|min:10',
                'bookkeepingFax' =>             $requestAll['bookkeepingFax'] != null ? 'sometimes|numeric|min:2' : '',
                'bookkeepingEmail' =>           $requestAll['bookkeepingEmail'] != null ? 'sometimes|email' : '',
                'legalName' =>                  'required|string|min:2',
                'typeOwn1' =>                   'required|in:tov,fop',
                'typeOwn2' =>                   $requestAll['typeOwn2'] != null ? 'sometimes|in:tov,fop' : '',
                //sometimes -> валидирует если поле присутсвует, ураа епт!
                'checkingAccount' =>            $requestAll['checkingAccount'] != null ? 'sometimes|numeric|min:2' : '',
                'additionalAccount' =>          $requestAll['additionalAccount'] != null ? 'sometimes|numeric|min:2' : '',
                'legalAddress' =>               'required|string',
                'egrpou' =>                     $requestAll['egrpou'] != null ? 'digits:8': '',
                'numberNds' =>                  $requestAll['numberNds'] != null ? 'digits:8' : ''
                
                
                
                
                
                
            ], $messages);
            
            if($validator->fails()) {
                return redirect()->back()->withInput()->withErrors($validator->errors());
            }
            
            
        }
        
        
        
        
        
        
        return $next($request);
    }
}
