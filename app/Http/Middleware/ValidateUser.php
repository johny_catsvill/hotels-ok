<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class ValidateUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        $user = Auth::user();
        
        if(!$user || $user->objectId || !$user->clientId || $user->roleId == 1) return redirect()->route('homepage');
        
        
        
        
        return $next($request);
    }
}
