<?php

namespace App\Http\Middleware;

use Closure;
use Validator;

class BySettingsUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
//        if($request->isMethod('post')) {
//            $requestAll = $request->all();
//            $validator = Validator::make($requestAll, [
////                'password' => 'sometimes|alpha_num',
////                'repeatPassword' => 'sometimes|same:password|alpha_num',
//                'email' => 'required|email|exists:users,!email',
//                'phone' => 'required|numeric|min:10'
//            ]);
//
//
//
//           if($validator->fails()) {
//               return redirect()->back()->withErrors($validator->errors());
//           }
//            
//            
//        }
        
        
        return $next($request);
    }
}
