<?php

namespace App\Http\Controllers;

use App\Models\Bonus;
use App\Models\ObjectHotel;
use App\Models\Promocode;
use App\Models\Reservation;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Clients;
use Auth;
use DB;
use App\Models\Room;
use App\Traits\timeTraits;
use App\Helpers\Func;

class ReservationController extends Controller
{

    use timeTraits;

    public function reservationHotel(Request $request, Reservation $reservation, ObjectHotel $objectHotel, Room $roomModel) {

        $objectId = $request->hotelId;
        $rooms = json_decode(base64_decode($request->hash));
        $dateFrom = $request->arrival;
        $explodeDateFrom = explode('.', $dateFrom);
        $dateTo = $request->departure;
        $explodeDateTo = explode('.', $dateTo);
        $nameHotel = $request->hotel;
        $marker = false; //пометка если данные заказа не являются массивов
        $error = true;
        if(!is_array($rooms)) {

            $rooms = false;
            $marker = true;
            abort(404);
        }

        $fullPrice = 0;

        $roomsIdArray = [];
        foreach($rooms as $room) {
            array_push($roomsIdArray, $room->roomId);
        }


        if(Auth::user()) {
            $userAuth = User::find(Auth::id());
            $clientInfo = $userAuth->client;
        }
        else {
            $clientInfo = false;
            $userAuth = false;
        }

        if($dateFrom >= $dateTo) {
            abort(404);
        }

        $func = new Func();

//        setlocale(LC_TIME, 'Russian');
//        Carbon::setLocale('ru_RU');
//        $formatedDateFrom = new Carbon($dateFrom);
//        $formatedDateFrom = $formatedDateFrom->format('l, d F Y');
//        $formatedDateTo = new Carbon($dateTo);
//        $formatedDateTo = $formatedDateTo->format('l, d F Y');
        $dayNameFrom = date("w", mktime(0,0,0, $explodeDateFrom[1], $explodeDateFrom[0], $explodeDateFrom[2]));
        $dayNameTo = date("w", mktime(0,0,0, $explodeDateTo[1], $explodeDateTo[0], $explodeDateTo[2]));
        $formatedDateFrom = $func->days[$dayNameFrom].', '. $explodeDateFrom[0].' '. Func::$arrayMonths[$explodeDateFrom[1]].' '. $explodeDateFrom[2];
        $formatedDateTo = $func->days[$dayNameTo].', '. $explodeDateTo[0].' '. Func::$arrayMonths[$explodeDateTo[1]].' '. $explodeDateTo[2];

        $object = $objectHotel->findOrFail($objectId);

        $period = $this->getDateTimeStartStopByDays($dateFrom, $dateTo);
        array_pop($period);

        foreach($period as $key => $p) {
            $period[$key] = strtotime($key);
        }

        $datesPeriod = array_values($period);
        $countPeriod = count($period);

        $currentRooms = $roomModel->getListRoomsReservation($object->id, $period, $roomsIdArray, $dateFrom, $dateTo); //вытягиваю количество доступных комнат

        if(count($currentRooms) == count($rooms)) {
            foreach($currentRooms as $key => $r) {
                $newArrayRoom = (array)$r;
                $r->modelRoom = Room::find($r->typeRoomId);
                $cloneRoomModel = $r->modelRoom;
                $categoryPluses = new \App\Models\PlusesCategory();
                $r->categoryPluses = $categoryPluses->with(['pluses' => function($query) use($cloneRoomModel) {
                    $query->roomPluses($cloneRoomModel);
                }])->get();

                $arrayCalendarCount = [];
                $currentRooms[$key]->datesThisRoom = [];
                $currentRooms[$key]->price = 0;
                for($i = 1; $i <= $countPeriod; $i++) {
                    array_push($arrayCalendarCount, $newArrayRoom['calendar'.$i]);

                    $currentPrice = ($newArrayRoom['price'.$i] - ($newArrayRoom['price'.$i] * $newArrayRoom['discount'.$i]) / 100) * $rooms[$key]->countRoom;
                    $currentRooms[$key]->price += $currentPrice;

                    $currentDate = [
                        'date' => $datesPeriod[$i - 1],
                        'price' => $currentPrice,
                        'discount' => $newArrayRoom['discount'.$i],
                        'countRoom' => $rooms[$key]->countRoom
                    ];

                    array_push($currentRooms[$key]->datesThisRoom, $currentDate);
                }

                $fullPrice += $currentRooms[$key]->price;


                $minCountFreeRoom = min($arrayCalendarCount); //получаю минимальное число доступных номеров

                if($minCountFreeRoom < $rooms[$key]->countRoom) { // проверка есть ли столько штук сколько выбрал пользователь

                    $error = false;
                    //break;
                }


            }

        }
        else {
            $error = false;
        }

        if(!$error) {
            //надо вернуть как нить ошибку говоря о том что несколько номеров уже недоступно
        }


        $city = $object->city;

        $this->breadcrumbs += [
            'Гостиницы '.$city->name.'а' => '/category?cityId='.$city->id.'&search='.$city->name,
            $object->objectCategory->name.' '. $object->name => route('hotel', ['id' => $object->id, 'url' => $object->subdomain]),
            'Бронирование' => ''
        ];

        return view('site.reservation', [
            'object' => $object,
            'dateFrom' => $dateFrom,
            'dateTo' => $dateTo,
            'page' => $this->page['reservation'],
            'rooms' => $currentRooms,
            'breadcrumbs' => $this->breadcrumbs,
            'clientInfo' => $clientInfo,
            'userAuth' => $userAuth,
            'fullPrice' => $fullPrice,
            'formatedDateFrom' => $formatedDateFrom,
            'formatedDateTo' => $formatedDateTo,
            'hotelId' => $object->id,
        ]);
    }

    public function typePrepayment($roomPrice) {
        
        $pricePrepayment = 0;
        
        switch(+$roomPrice->prePaymentId) {
            case 2:
                $pricePrepayment += $roomPrice->priceForDay;
                break;
            case 3:
                $pricePrepayment += $roomPrice->price;
                break;
            default:
                $pricePrepayment += 0;
        }
        
        return $pricePrepayment;
        
    }

    public function save_user_reservation(Request $request, Room $roomModel) {

        $error = false;

        if($request->isMethod('post')) {
            $data = $request->all();
            $name = $data['fio'];
            $email = $data['email'];
            $phone = $data['phone'];
            $comment = $data['comment'];
            //$link_referal = $data['linkReferal'];
            $userId = $data['userId'];
            $dateFrom = $data['dateFrom'];
            $dateTo = $data['dateTo'];
            $hotelId = $data['hotelId'];
            $promoCode = $data['promoCode'];
            $rooms = json_decode(base64_decode($data['hash']));

            $object = ObjectHotel::find($hotelId);

            if(!$object) {
                return 0;
            }

            if(strtotime($dateFrom) > strtotime($dateTo)) {
                return 0;
            }

            $user = User::find($userId);

            if(!$user) {
                $userId = 0;
            }
            else {
                $userId = $user->id;
            }


            $period = $this->getDateTimeStartStopByDays($dateFrom, $dateTo);
            foreach($period as $date => $datePeriod) {
                $period[$date] = strtotime($date);
            }
            array_pop($period);

            $periodCount = count($period);

            $fullPrice = 0;
            $fullPriceWithoutDiscount = 0;

            DB::beginTransaction();
            try {


                $reservation = new Reservation();
                $reservation->objectId = $object->id;
                if($userId) {
                    $reservation->userId = $userId;
                }

                $reservation->startReservationDate = strtotime($dateFrom);
                $reservation->endReservationDate = strtotime($dateTo);
//                $reservation->referal_link = $link_referal;
                $procentBonus = 0;
                if($userId) {
                    $reservation->bonusId = 1;
                    $procentBonus = Bonus::find(1)->procent;
                }

                if($promoCode) {
                    $isPromoCode = Promocode::where('code', $promoCode)->first();
                    if($isPromoCode) {
                        $reservation->bonusId = 2;
                        $procentBonus = Bonus::find(2)->procent;
                    }

                }

                $reservation->type = 1;
                $reservation->creationDate = time();
                $reservation->visitorPhone = $phone;
                $reservation->comment = $comment;
                $reservation->visitorEmail = $email;
                $reservation->name = $name;
                $reservation->statusId = 4; // в ожидании
                $reservation->save();



                $roomsIdArray = [];
                foreach($rooms as $room) {
                    array_push($roomsIdArray, $room->roomId);
                }

                $currentRooms = $roomModel->getListRoomsReservation($object->id, $period, $roomsIdArray, $dateFrom, $dateTo);


                if(count($currentRooms) == count($rooms)) {

                    $fullPriceWithoutDiscountReservation = 0;

                    foreach($currentRooms as $key => $r) {
                        $fullPriceRoom = 0;
                        $fullPriceWithoutDiscount = 0;
                        $newArrayRoom = (array)$r;

                        $arrayCalendarCount = [];
                        $currentRooms[$key]->price = 0;
                        $currentRooms[$key]->priceCountRoom = 0;
                        $priceForDay = 0;

                        for($i = 1; $i <= $periodCount; $i++) {

                            if($i == 1) {
                                $priceForDay = ($newArrayRoom['price'.$i] - ($newArrayRoom['price'.$i] * $newArrayRoom['discount'.$i]) / 100);
                            }

                            $fullPriceWithoutDiscount += $newArrayRoom['price'.$i];
                            $fullPriceWithoutDiscountReservation += $newArrayRoom['price'.$i] * $rooms[$key]->countRoom;
                            $currentPrice = ($newArrayRoom['price'.$i] - ($newArrayRoom['price'.$i] * $newArrayRoom['discount'.$i]) / 100);
                            $currentRooms[$key]->price += $currentPrice;
                            $currentRooms[$key]->priceCountRoom += $currentPrice  * $rooms[$key]->countRoom;

                            array_push($arrayCalendarCount, $newArrayRoom['calendar'.$i]);
                        }

                        $minCountFreeRoom = min($arrayCalendarCount); //получаю минимальное число доступных номеров

                        if($minCountFreeRoom < $rooms[$key]->countRoom) { // проверка есть ли столько штук сколько выбрал пользователь

                            $error = false;
                            //break;
                        }
                        $fullPriceRoom += $currentRooms[$key]->price;

                        //добавляю скидку от бонусов
                        //$fullPriceRoom  = $fullPriceRoom - ($fullPriceRoom * $procentBonus / 100);

                        $fullPrice += $currentRooms[$key]->priceCountRoom;

                        for($z = 0; $z < $rooms[$key]->countRoom; $z++) {

                            DB::table('reservation_relation_room')->insert([
                                'roomId' => $r->typeRoomId,
                                'price' => $fullPriceRoom,
                                'priceWithoutDiscount' =>  $fullPriceWithoutDiscount,
                                'amountPerson' => $r->amountPersonRoom,
                                'reservationId' => $reservation->id,
                                'priceForDay' => $priceForDay
                            ]);
                        }

                    }

                }
                else {
                    $error = false; // придумать общий вывод ошибок
                }


                $reservation->price = $fullPrice;
                $reservation->priceWithoutDiscount = $fullPriceWithoutDiscountReservation;
                $reservation->save();

            }

            catch(QueryException $e) {
                DB::rollBack();
                $errors = true;
            }

            DB::commit();

            return 1;


        }

    }
    
    
}
