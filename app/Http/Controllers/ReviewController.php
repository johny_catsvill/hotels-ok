<?php

namespace App\Http\Controllers;

use App\Models\ObjectHotel;
use App\Models\Reservation;
use App\Models\SiteReview;
use Illuminate\Http\Request;
use App\Models\Review;
use Illuminate\Support\Facades\DB;
use App\Helpers\Func;

class ReviewController extends Controller
{
    
    public function feedback(Request $request, Review $reviewModel) {

        $this->breadcrumbs += ['Добавить отзыв' => route('feedback')];

        $listCategoriesPeople = $reviewModel->getListCategoriesPeople();

        $data = $request->all();
        if(!isset($data['code']) || !isset($data['email'])) {
            abort(404);
        }

        $code = DB::table('code_reviews_user')->where('code', $data['code'])->where('emailUser', $data['email'])->where('isReview', 0)->first();
        if(!$code) {
            abort(404);
        }
        $reservation = Reservation::where('id', $code->reservationId)->where('isPast', 1)->where('isComment', 0)->first();
        if(!$reservation) {
            abort(404);
        }

        $object = ObjectHotel::find($reservation->objectId);
        $explodeDateFrom = explode('.', date('d.m.Y',$reservation->startReservationDate));
        $explodeDateTo = explode('.', date('d.m.Y', $reservation->endReservationDate));

        $func = new Func();
        $dayNameFrom = date("w", mktime(0,0,0, $explodeDateFrom[1], $explodeDateFrom[0], $explodeDateFrom[2]));
        $dayNameTo = date("w", mktime(0,0,0, $explodeDateTo[1], $explodeDateTo[0], $explodeDateTo[2]));
        $formatedDateFrom = $func->days[$dayNameFrom].', '. $explodeDateFrom[0].' '. Func::$arrayMonths[$explodeDateFrom[1]].' '. $explodeDateFrom[2];
        $formatedDateTo = $func->days[$dayNameTo].', '. $explodeDateTo[0].' '. Func::$arrayMonths[$explodeDateTo[1]].' '. $explodeDateTo[2];



        return view('site.feedback', [
            'breadcrumbs' => $this->breadcrumbs,
            'page' => $this->page['object'],
            'listCategoriesPeople' => $listCategoriesPeople,
            'object' => $object,
            'formatedDateFrom' => $formatedDateFrom,
            'formatedDateTo' => $formatedDateTo,
            'reservation' => $reservation,
            'code' => $code,
            'email' => $data['email']
        ]); 

    }

    public function saveFeedback(Request $request) {
        if($request->isMethod('post')) {
            $data = $request->all();
            $code = DB::table('code_reviews_user')->where('code', $data['code'])->where('emailUser', $data['email'])->where('isReview', 0)->first();
            if(!$code) {
                return 0;
            }
            $reservation = Reservation::where('id', $code->reservationId)->where('isPast', 1)->where('isComment', 0)->first();
            if(!$reservation) {
                return 0;
            }



            DB::beginTransaction();

            try {

                $review = new Review();
                $review->creationDate = time();
                $review->positiveText = $data['positiveText'];
                $review->negativeText = $data['negativeText'];
                $review->comfortValue = $data['comfortValue'];
                $review->employeeValue = $data['employeeValue'];
                $review->serviceValue = $data['serviceValue'];
                $review->cleanValue = $data['cleanValue'];
                $review->qualityPriceValue = $data['qualityPriceValue'];
                $review->locationValue = $data['locationValue'];
                $review->isRecommendation = $data['isRecHotel'];
                $review->typeUserReviewId = $data['category'];
                $review->clientId = $reservation->user->clientId; //как нибудь правильно назвать переменнные
                $review->authorName = $reservation->user->client->name;
                $review->objectId = $reservation->objectId;
                $review->reservationId = $reservation->id;
                $review->avgValue = ($data['comfortValue'] + $data['employeeValue'] + $data['serviceValue'] + $data['cleanValue'] + $data['qualityPriceValue'] + $data['locationValue']) / 6;
                $review->save();

                $reservation->isComment = 1;
                $reservation->save();


                $siteReview = new SiteReview();
                $siteReview->name = $reservation->user->client->name;
                $siteReview->raiting = $data['projectValue'];
                $siteReview->text = $data['global-review'];
                $siteReview->save();

                DB::table('code_reviews_user')->where('code', $data['code'])->where('emailUser', $data['email'])->where('isReview', 0)->update([
                    'isReview' => 1
                ]);
            }
            catch(QueryException $e) {
                DB::rollBack();
            }

            DB::commit();



            return 1;

        }
    }
    
    
    public function reviews() {

        $this->breadcrumbs += ['Отзывы о нас' => route('feedback')];
        $reviews = SiteReview::orderBy('lastUpdater', 'desc')->where('active', 1)->get();

        return view('site.reviews', [
            'breadcrumbs' => $this->breadcrumbs,
            'searchShow' => $this->search['hide'],
            'page' => $this->page['object'],
            'reviews' => $reviews
        ]); 
    }
    
    
    
}
