<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AboutCompanyController extends Controller
{
    public function aboutCompany(){

        $this->breadcrumbs += ['О нас' => route('about-company')];
        $this->data['title'] = '';
        $this->data['keywords'] = '';
        $this->data['description'] = '';
        
        
        return view('site.aboutCompany', [
            'title' => $this->data['title'],
            'keywords' => $this->data['keywords'],
            'description' => $this->data['description'],
            'page' => $this->page['about'],
            'searchShow' => $this->search['hide'],
            'breadcrumbs' => $this->breadcrumbs
        ]);
    }
    
    
    public function guarantees() {
        $this->breadcrumbs += ['Гарантии безопасности' => route('guarantees')];
        $this->data['title'] = 'Гарантии безопасности Hotels-ok';
        $this->data['keywords'] = '';
        $this->data['description'] = '';
        
        
        return view('site.guarantees', [
            'title' => $this->data['title'],
            'keywords' => $this->data['keywords'],
            'description' => $this->data['description'],
            'page' => $this->page['about'],
            'searchShow' => $this->search['hide'],
            'breadcrumbs' => $this->breadcrumbs
        ]);
    }
    
    public function how_to_reservation() {
        $this->breadcrumbs += ['Как забронировать?' => route('how-to-reservation')];
        $this->data['title'] = 'Как забронировать';
        $this->data['keywords'] = '';
        $this->data['description'] = '';
        
        
        return view('site.how-to-reservation', [
            'title' => $this->data['title'],
            'keywords' => $this->data['keywords'],
            'description' => $this->data['description'],
            'page' => $this->page['about'],
            'searchShow' => $this->search['hide'],
            'breadcrumbs' => $this->breadcrumbs
        ]);
    }
    
    public function questions(Request $request, $slug = false) {
        $this->breadcrumbs += ['Часто задаваемые вопросы' => route('questions')];

        
        
        return view('site.questions', [
            'page' => $this->page['about'],
            'searchShow' => $this->search['hide'],
            'breadcrumbs' => $this->breadcrumbs,
            'slug' => $slug
        ]);
    }


    public function rewards() {
        $this->breadcrumbs += ['Награды и заслуги' => route('rewards')];
        $this->data['title'] = 'Награды и заслуги';
        $this->data['keywords'] = '';
        $this->data['description'] = '';

        return view('site.rewards', [
            'title' => $this->data['title'],
            'keywords' => $this->data['keywords'],
            'description' => $this->data['description'],
            'page' => 'rewards',
            'breadcrumbs' => $this->breadcrumbs
        ]);
    }
    
    public function call_company() {
        $this->breadcrumbs += ['Контакты' => route('contacts')];
        $this->data['title'] = 'Контакты';
        $this->data['keywords'] = '';
        $this->data['description'] = '';
        
        
        return view('site.contacts', [
            'title' => $this->data['title'],
            'keywords' => $this->data['keywords'],
            'description' => $this->data['description'],
            'page' => $this->page['about'],
            'breadcrumbs' => $this->breadcrumbs
        ]);

    }
    
    public function public_tender() {
        $this->breadcrumbs += ['Публичная оферта' => route('public-tender')];
        $this->data['title'] = 'Контакты';
        $this->data['keywords'] = '';
        $this->data['description'] = '';
        
        
        return view('site.call-company', [
            'title' => $this->data['title'],
            'keywords' => $this->data['keywords'],
            'description' => $this->data['description'],
            'page' => $this->page['about'],
            'searchShow' => $this->search['hide'],
            'breadcrumbs' => $this->breadcrumbs
        ]);
    }
    
    public function confidentiality() {
        $this->breadcrumbs += ['Конфиденциальность' => route('confidentiality')];
        $this->data['title'] = 'Контакты';
        $this->data['keywords'] = '';
        $this->data['description'] = '';
        
        
        return view('site.confidentiality', [
            'title' => $this->data['title'],
            'keywords' => $this->data['keywords'],
            'description' => $this->data['description'],
            'page' => $this->page['about'],
            'searchShow' => $this->search['hide'],
            'breadcrumbs' => $this->breadcrumbs
        ]);
    }


    public function bussiness() {
        return view('site.bussiness', [
            'title' => $this->data['title'],
            'keywords' => $this->data['keywords'],
            'description' => $this->data['description'],
            'page' => 'bussiness'
        ]);
    }

    public function groups() {
        return view('site.groups', [
            'title' => $this->data['title'],
            'keywords' => $this->data['keywords'],
            'description' => $this->data['description'],
            'page' => 'groups'
        ]);
    }
    
    
    
    
    
}
