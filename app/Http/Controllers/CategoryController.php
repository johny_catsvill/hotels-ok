<?php

namespace App\Http\Controllers;


use App\Helpers\Func;
use App\Models\FoodTypes;
use App\Models\ObjectsTypesPluses;
use App\Models\ObjectHotel;
use App\Models\ObjectServise;
use App\Models\ObjectsCategories;
use App\Models\Page;
use App\Traits\shortcodeTraits;
use Illuminate\Http\Request;
use App\Models\City;
use DB;
use App\Traits\timeTraits;
use App\Models\Star;
use App\Models\Region;
use App\Models\EventCalendar;


class CategoryController extends Controller
{

    use timeTraits, shortcodeTraits;

    public function viewCategoryPage(Request $request,
                                     Func $funcHelper, ObjectsCategories $objectsCategories, ObjectHotel $object,
                                     ObjectServise $objectServise, ObjectsTypesPluses $objectsTypesPluses, Star $star, FoodTypes $foodTypes, EventCalendar $eventCalendar, City $cityModel)
    {
        $zoom = 7;
        $city = false;
        $region = false;
        $guest = 1;
        $search = false;
        $starsArray = false;
        $minPrice = 0;
        $maxPrice = 0;
        $typeObjectArray = false;
        $foodArray = false;
        $servicesArray = false;
        $plusesArray = false;
        $sort = 'recommendation';

            $data = $request->all();
            if(isset($data['dateFrom']) && $data['dateFrom'] && is_numeric(strtotime($data['dateFrom']))) {
                $dateFrom = $data['dateFrom'];
            }
            else {
                $dateFrom  = date('d.m.Y');
            }

            if(isset($data['dateTo']) && $data['dateTo'] && is_numeric(strtotime($data['dateTo']))) {
                $dateTo = $data['dateTo'];
            }
            else {
                $dateTo = strtotime(date('d.m.Y')) + 86400;
                $dateTo = date('d.m.Y', $dateTo);
            }

            $dateFromStr = strtotime($dateFrom);
            $dateToStr = strtotime($dateTo);

            if($dateFromStr > $dateToStr) {
                abort(404);
            }


            $page = 1;

            if(isset($data['page']) && $data['page'] && is_numeric($page)) {
                $page = preg_replace("/[^,.0-9]/", '', $data['page']);
            }

            if(isset($data['cityId']) && $data['cityId'] && is_numeric($data['cityId'])) {
                $city = City::find(preg_replace("/[^,.0-9]/", '', $data['cityId']));
            }

            if(isset($data['regionId']) && $data['regionId'] && is_numeric($data['regionId'])) {
                $region = Region::find(preg_replace("/[^,.0-9]/", '', $data['regionId']));
            }
            else {
                if($city) {
                    $region = Region::find($city->regionId);
                }
            }

            if(!$city && !$region) {
                return redirect()->route('homepage'); // редирект на главную
            }

            if(isset($data['guest']) && $data['guest'] && is_numeric($data['guest'])) {
                $guest = preg_replace("/[^,.0-9]/", '', $data['guest']);
            }

            if(isset($data['search']) && $data['search']) {
                $search = preg_replace('/[a-zа-яё\d]+/iu','',$data['search']);
            }

            if(isset($data['stars']) && $data['stars']) {
                $starsArray = explode(',', $data['stars']);
                foreach ($starsArray as $key => $starFor) {
                    $starsArray[$key] = preg_replace("/[^,.0-9]/", '', $starFor);
                }
            }

            if(isset($data['minPrice']) && $data['minPrice'] && is_numeric($data['minPrice'])) {
                $minPrice = preg_replace("/[^,.0-9]/", '', $data['minPrice']);
            }

            if(isset($data['maxPrice']) && $data['maxPrice'] && is_numeric($data['maxPrice'])) {
                $maxPrice = preg_replace("/[^,.0-9]/", '', $data['maxPrice']);
            }

            if(isset($data['typeObject']) && $data['typeObject'] ) {
                $typeObjectArray = explode(',', $data['typeObject']);
                foreach ($typeObjectArray as $key => $typeObjectFor) {
                    $typeObjectArray[$key] = preg_replace("/[^,.0-9]/", '', $typeObjectFor);
                }
            }

            if(isset($data['food']) && $data['food']) {
                $foodArray = explode(',', $data['food']);
                foreach ($foodArray as $key => $foodFor) {
                    $foodArray[$key] = preg_replace("/[^,.0-9]/", '', $foodFor);
                }
            }

            if(isset($data['services']) && $data['services']) {
                $servicesArray = explode(',', $data['services']);
                foreach ($servicesArray as $key => $serviceFor) {
                    $servicesArray[$key] = preg_replace("/[^,.0-9]/", '', $serviceFor);
                }
            }

            if(isset($data['pluses']) && $data['pluses']) {

                $plusesArray = explode(',', $data['pluses']);
                foreach ($plusesArray as $key => $pluseFor) {
                    $plusesArray[$key] = preg_replace("/[^,.0-9]/", '', $pluseFor);
                }
            }

            if(isset($data['sort']) && $data['sort']) {
                $sort = $data['sort'];
            }



        $period = $this->getDateTimeStartStopByDays($dateFrom, $dateTo);
        array_pop($period);
        $countPeriod = count($period);

        $userLat = false;
        $userLng = false;

        if(isset($city->id)) {
            $countObjectCity = ObjectHotel::where('cityId', $city->id)->has('rooms')->count();
        }
        else {
           $city = City::where('regionId', $region->id)->whereNull('parentId')->first();
            $countObjectCity = ObjectHotel::where('cityId', $city->id)->has('rooms')->count();
        }


        if(!$request->ajax()) {
            $listObjects = $object->getListObjects($city, $region, $dateFromStr, $dateToStr, $guest, $minPrice, $maxPrice, $typeObjectArray, $foodArray, $servicesArray, $plusesArray,
                $starsArray, $sort, $countPeriod);
            session(['listObjects' => $listObjects]);
            $minPriceAllObject = collect($listObjects)->min('minPrice');
        }



        if($request->ajax()) { //проверка на ajax и запись в сессию всех обьектов
            if(!isset($data['type'])) {
                $listObjects = $object->getListObjects($city, $region, $dateFromStr, $dateToStr, $guest, $minPrice, $maxPrice, $typeObjectArray, $foodArray, $servicesArray, $plusesArray,
                    $starsArray, $sort, $countPeriod);
                session(['listObjects' => $listObjects]);
            }

            if(isset($data['type']) && $data['type'] == 'nearestObjects') {
                $listObjects = $object->getListObjects(false, false, $dateFromStr, $dateToStr, $guest, $minPrice, $maxPrice, $typeObjectArray, $foodArray, $servicesArray, $plusesArray,
                    $starsArray, $sort, $countPeriod);
                session(['listObjects' => $listObjects]);
                $userLat = $data['userLat'];
                $userLng = $data['userLng'];
                $distanceData = $data['distance'] * 1000;

                if($userLat && $userLng) {
                    $listObjectCoordinates = session('listObjects');
                    foreach($listObjectCoordinates as $key => $obj) {
                        if(!$obj->longitudeGps || !$obj->latitudeGps) {
                            unset($listObjectCoordinates[$key]);
                            continue;
                        }
                        $distance = $this->calculate_distance($userLat, $userLng, $obj->longitudeGps, $obj->latitudeGps);
                        $newDistance = round(($distance / 1000) ,2);
                        $obj->newDistance = $newDistance;
                        if($distanceData <= $newDistance * 1000) {
                            unset($listObjectCoordinates[$key]);
                        }
                    }

                    $collection = collect($listObjectCoordinates);
                    $listObjectCoordinates = $collection->sortBy('newDistance');
                    $listObjectCoordinates = $listObjectCoordinates->toArray();
                    session(['listObjects' => $listObjectCoordinates]);
                }
            }
        }

        $arr = array_chunk(session('listObjects'), 10);

        if($request->ajax()) {
            if(isset($data['type']) && $data['type'] == 'showMore') {
                if(isset($arr[$page -1])) {
                    $listObjects = $arr[$page - 1];
                    $returnHTML = view('site.blocks.categories-objects')->with(['listObjects' => $listObjects, 'city' => $city, 'allObjectsSession' => session('listObjects')])->render();
                    return response()->json(array('html' => $returnHTML, 'countObjects' => count($listObjects)));
                }

                return 0;

            }
        }

        $stars = $star->getListStarsAndCountObject($city, $region, $dateFromStr, $guest, $search, $minPrice, $maxPrice, $typeObjectArray, $foodArray, $servicesArray, $plusesArray);
        $typeObjects = $objectsCategories->getListTypeObjectsAndCountObject($city, $region, $dateFromStr, $guest, $search, $starsArray, $minPrice, $maxPrice, $foodArray, $servicesArray, $plusesArray);
        $prices = $eventCalendar->getListPriceAndCountObject($dateFromStr, $guest, $city, $region, $starsArray, $foodArray, $servicesArray, $plusesArray, $typeObjectArray);
        $typePluses = $objectsTypesPluses->getListPlusesAndCountObject($city, $region, $dateFromStr, $guest, $search, $starsArray, $minPrice, $maxPrice, $typeObjectArray, $foodArray, $servicesArray);
        $foods = $foodTypes->getListFoodAndCountObject($city, $region, $dateFromStr, $guest, $search, $starsArray, $minPrice, $maxPrice, $typeObjectArray, $servicesArray, $plusesArray);

        $pricesArray = [
            1 => ['title' => 'до 200 грн', 'min' => 0, 'max' => 199],
            2 => ['title' =>'от 200 до 400 грн', 'min' => 200, 'max' => 399],
            3 => ['title' => 'от 400 до 600 грн', 'min' => 400, 'max' => 599],
            4 => ['title' => 'от 600 до 800 грн', 'min' => 600, 'max' => 799],
            5 => ['title' => 'от 800 до 1000 грн', 'min' => 800, 'max' => 999],
            6 => ['title' => 'выше 1000 грн', 'min' => 1000, 'max' => 0]
        ];

        foreach ($pricesArray as $key => $p) {
            foreach ($prices as $pricesDb) {
                if($key == $pricesDb->rangePrice) {
                    $pricesArray[$key]['info'] = $pricesDb;
                }
            }
        }

        if(isset($data['priceRange'])) {
           $priceRange = explode(',', $data['priceRange']);
           foreach ($priceRange as $price) {
               $price = preg_replace("/[^,.0-9]/", '', $price);
               if(array_key_exists($price, $pricesArray)) {
                   $pricesArray[$price]['checked'] = 1;
               }
           }
        }

        if($region) {
            $listCitiesByRegion = $cityModel->getListCitiesByRegion($region->id, $dateFromStr);
        }
        else {
            $regionId = $city->regionId;
            $region = Region::find($regionId);
            $listCitiesByRegion = $cityModel->getListCitiesByRegion($region->id, $dateFromStr);
        }

        if($request->ajax()) { //ajax для фильтров
            if($arr) {
                $listObjects = $arr[0];
            }
            else {
                $listObjects = [];
            }

            $objectsHtml = view('site.blocks.categories-objects')->with(['listObjects' => $listObjects, 'city' => $city, 'allObjectsSession' => session('listObjects')])->render();
            $filtersHtml = view('site.blocks.left-sidebar-category')
                ->with(['stars' => $stars, 'typeObjects' => $typeObjects, 'prices' => $pricesArray, 'typePluses' => $typePluses, 'foods' => $foods, 'starsArray' => $starsArray,
                    'typeObjectArray' => $typeObjectArray, 'foodArray' => $foodArray, 'servicesArray' => false, 'plusesArray' => $plusesArray, 'services' => false,
                    'minPrice' => $minPrice, 'maxPrice' => $maxPrice])
                ->render();
            $cityForSearchString = '';
            if(isset($listObjects[0]) && isset($data['type']) && $data['type'] == 'nearestObjects') {
                $ob = ObjectHotel::find($listObjects[0]->objectId);
                $cityForSearchString = $ob->city->name;
            }

            return response()->json(array('htmlObjects' => $objectsHtml, 'filtersHtml' => $filtersHtml, 'countObjects' => count($listObjects), 'cityForSearchString' => $cityForSearchString));
        }

        $listObjectsAll = session('listObjects');
        if($arr) {
            $listObjects = $arr[0];
        }
        else {
            $listObjects = [];
        }


        $this->breadcrumbs += ['Гостиницы' => route('category')];




        $page = Page::where('slug', 'category')->first();

        $seo = false;

        if($page) {
            $seo = (object)[];
            $seo->title = $this->parseStringSeo($page->title, $city->name, $minPriceAllObject);
            $seo->keywords = $this->parseStringSeo($page->keywords, $city->name, $minPriceAllObject);
            $seo->description = $this->parseStringSeo($page->description, $city->name, $minPriceAllObject);
        }




        return view('site.category', [
            'title' => $this->data['title'],
            'keywords' => $this->data['keywords'],
            'description' => $this->data['description'],
            'page' => 'category',
            'dateFrom' => $dateFrom,
            'dateTo' => $dateTo,
            'stars' => $stars,
            'city' => $city,
            'typeObjects' => $typeObjects,
            'typePluses' => $typePluses,
            'listObjects' => $listObjects,
            'services' => false,
            'foods' => $foods,
            'starsArray' => $starsArray,
            'typeObjectArray' => $typeObjectArray,
            'foodArray' => $foodArray,
            'servicesArray' => false,
            'plusesArray' => $plusesArray,
            'sort' => $sort,
            'minPrice' => $minPrice,
            'maxPrice' => $maxPrice,
            'breadcrumbs' => $this->breadcrumbs,
            'prices' => $pricesArray,
            'countObjectCity' => $countObjectCity,
            'listCitiesByRegion' => $listCitiesByRegion,
            'region' => $region,
            'listObjectsAll' => $listObjectsAll,
            'seo' => $seo
        ]);


    }


public function calculate_distance ($φA, $λA, $φB, $λB) {

    // перевести координаты в радианы
    $lat1 = $φA * M_PI / 180;
    $lat2 = $φB * M_PI / 180;
    $long1 = $λA * M_PI / 180;
    $long2 = $λB * M_PI / 180;

    // косинусы и синусы широт и разницы долгот
    $cl1 = cos($lat1);
    $cl2 = cos($lat2);
    $sl1 = sin($lat1);
    $sl2 = sin($lat2);
    $delta = $long2 - $long1;
    $cdelta = cos($delta);
    $sdelta = sin($delta);

    // вычисления длины большого круга
    $y = sqrt(pow($cl2 * $sdelta, 2) + pow($cl1 * $sl2 - $sl1 * $cl2 * $cdelta, 2));
    $x = $sl1 * $sl2 + $cl1 * $cl2 * $cdelta;


    $ad = atan2($y, $x);
    $dist = $ad * 6372795;

    return $dist;
}

}
