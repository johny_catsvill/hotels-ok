<?php

namespace App\Http\Controllers;

use App\Models\Page;
use App\Models\Review;
use App\Traits\shortcodeTraits;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\ObjectHotel;
use App\Models\Room;

use App\Models\City;
use App\Models\Region;
use Illuminate\Support\Facades\Storage;
use App\Models\RoomCategoryPluses;
use DB;
use App\Traits\timeTraits;
use Illuminate\Support\Facades\Cache;
use App\Models\News;


class ObjectController extends Controller
{

    use timeTraits, shortcodeTraits;
    protected $isDiscount = [];
    protected $minPriceByRoom = 0;

    public function viewHotel(Request $request, ObjectHotel $objectModel, Room $room, $id, $nameHotel)
    {

        $seeLaterHotel = false;

        $object = $objectModel->withCount('reviews')->with('categoryReviews')
            ->where('id', $id)->first();


        $object->checkIn = $this->translateToHours($object->checkIn);
        $object->checkOut = $this->translateToHours($object->checkOut);

        $mainImg = 'images/hotels/'.$id.'-'.$nameHotel.'/'.$object->image;
        $objectImages = Storage::allFiles('images/hotels/'.$id.'-'.$nameHotel.'/images-objects');

        foreach($objectImages as $key => $imgObject) {
            if(strpos($imgObject, 'object_mini') === false) {
                unset($objectImages[$key]);
            }
        }


        if (!$request->session()->has('viewedObject')) {
            $request->session()->push('viewedObject', $object->id);
        }
        else {
            $arrayViewedObject = session('viewedObject');
            if(!in_array($object->id, $arrayViewedObject)) {
                array_unshift($arrayViewedObject, $object->id);
                $request->session()->push('viewedObject', $object->id);
            }

        }

        if($request->session()->has('viewedObject')) {
            $arrayObjects = session('viewedObject');
        }

        $notNeedKeyObject = array_search($object->id, $arrayObjects);
        if($notNeedKeyObject !== false) {
            unset($arrayObjects[$notNeedKeyObject]);
        }


        $dateFrom = strtotime(date('d.m.Y'));
        $dateTo = $dateFrom + 86400;
        $countPeriod = 1;

        $minPriceAndRooms = $this->roomsData($room, $dateFrom, $dateTo, $countPeriod, $object);
        $this->minPriceByRoom = $minPriceAndRooms['minPrice'];
        $rooms = $minPriceAndRooms['rooms'];

        $recommendationObjects = $object->getListObjectsByParameters($object->cityId, 5, $dateFrom, false, $object->id);

        foreach ($recommendationObjects as $key => $recommendationObject) {
            $img = explode('.', $recommendationObject->image);
            if(isset($img[1])) {
                $recommendationObjects[$key]->image = $img[0].'_home.'.$img[1];
            }
            else {
                $recommendationObjects[$key]->image = $img[0].'_home.jpg';
            }

        }

        if(count($arrayObjects)) {
            $seeLaterHotel = $object->getListObjectsByParameters(false, 10, $dateFrom, $arrayObjects);
            foreach ($seeLaterHotel as $key => $seeLater) {
                //этот принципе изменить добавля картинку если ее нету
                if($seeLater->image) {
                    $img = explode('.', $seeLater->image);
                    $seeLaterHotel[$key]->image = $img[0].'_home.'.$img[1];
                }
                else {
                    $seeLaterHotel[$key]->image = '';
                }

            }
        }

        $zoom = 8;

        $news = News::where('date_published', '<', Carbon::now())->orderBy('date_published')->limit(2)->get();

        $page = Page::where('slug', 'objects')->first();

        $seo = false;

        if($page) {
            $seo = (object)[];
            $seo->title = $this->parseStringSeo($page->title, $object->city->name, $this->minPriceByRoom, $object->address, $object->objectCategory->name.' '.$object->name);
            $seo->keywords = $this->parseStringSeo($page->keywords, $object->city->name, $this->minPriceByRoom, $object->address, $object->objectCategory->name.' '.$object->name);
            $seo->description = $this->parseStringSeo($page->description, $object->city->name, $this->minPriceByRoom, $object->address, $object->objectCategory->name.' '.$object->name);
        }


        $lastReview = Review::where('avgValue', '>=', 8)->orderBy('id', 'desc')->where('objectId', $object->id  )->first();
        $this->breadcrumbs += [
            'Гостиницы' => url('category?cityId='.$object->city->id.'&search='.$object->city->name), $object->category->name.' '.$object->name => ''
            ];

        return view('site.item', [
            'page' => 'item',
            'object' => $object,
            'mainImg' => $mainImg,
            'objectImages' => $objectImages,
            'rooms' => $rooms,
            'recommendationObjects' => $recommendationObjects,
            'seeLaterHotel' => $seeLaterHotel,
            'breadcrumbs' => $this->breadcrumbs,
            'dateFrom' => date('d.m.Y', $dateFrom),
            'dateTo' => date('d.m.Y', $dateTo),
            'period' => $countPeriod,
            'isDiscount' => $this->isDiscount,
            'lastReview' => $lastReview,
            'minPriceByRoom' => $this->minPriceByRoom,
            'seo' => $seo,
            'news' => $news
        ]);
    }


    public function roomsData($roomObject, $dateFrom, $dateTo, $period, $object, $person = 1) {

        return $roomObject->getListRoomsByDate($roomObject, $dateFrom, $dateTo, $period, $object, $person = 1); //если что вернуть назад
    }

    public function hotelsUkraine(Request $request)
    {
        $currentDate = strtotime(date('d.m.Y'));
        $page = 'hotelsUkraine';
        $breadcrumbs = $this->breadcrumbs += [
            'Главная' => route('homepage'),
            'Гостиницы городов Украины' => route('hotels-ukraine')
        ];

        $regions = Cache::remember('regions', 60, function () {
            $regions = Region::with(['cities' => function($query) {
                $query->withCount('objects')->has('objects', '>=', 1);
            }])
                ->has('cities', '>=', 1)
                ->get();
            return $regions;
        });


        if (Cache::has('test')) {
            $view = Cache::get('test');
        }
        else {
            $view = view('site.list-hotels-ukraine-and-sng', compact('page', 'breadcrumbs', 'regions', 'currentDate'))->render();
            Cache::put('test', $view, 120);

        }

        return $view;

    }


    public function get_ajax_rooms(Request $request, ObjectHotel $objectHotel, Room $room) {
        if($request->isMethod('post')) {
            $data = $request->all();
            $dateFrom = $data['arrival'];
            $dateTo = $data['departure'];
            $person = $data['guestNumber'];
            $period = $this->getDateTimeStartStopByDays($dateFrom, $dateTo);

            array_pop($period);
            $countPeriod = count($period);
            $object = $objectHotel->find($data['objectId']);

            $minPriceAndRooms = $this->roomsData($room, strtotime($dateFrom), strtotime($dateTo), $countPeriod, $object, $person);
            $rooms = $minPriceAndRooms['rooms'];
            $returnHTML = view('site.blocks.rooms-object')->with(['rooms' => $rooms, 'period' => $countPeriod, 'dateFrom' => $dateFrom, 'dateTo' => $dateTo, 'object' => $object])->render();
            return response()->json(array('html' => $returnHTML));


        }
    }

    public function hotels_world()
    {
        return view('stub');
    }


    public function find_object(Request $request)
    { //метод для поиска обьектов в строке поиска
        if ($request->isMethod('post')) {

            $searchString = mb_ereg_replace('%[^a-zа-я\d]%i', '', $request->search);

            if (strlen($searchString) < 2) {
                return false;
            }

            $cities = City::where('name', 'LIKE', '%'. $searchString .'%')->with('region')->limit(7)->get();
            $regions = Region::where('name', 'Like', '%'. $searchString .'%')->limit(7)->get();
            $objects = ObjectHotel::where('name', 'like', '%'. $searchString .'%')->with('city.region')->limit(7)->get();
            $returnHTML = view('site.blocks.wrapper-search-object')->with([
                'cities' => $cities,
                'regions' => $regions,
                'objects' => $objects
            ])->render();
            return response()->json(array('html' => $returnHTML, 'countObjects' => count($cities) + count($regions) + count($objects)));
        }

        return false;

    }

    public function getMoreInfoObject(Request $request, Room $room) {
        if($request->isMethod('post')) {
            $objectId = $request->objectId;
                $object = ObjectHotel::find($objectId);
                $mainImg = 'images/hotels/'.$object->id.'-'.$object->subdomain.'/'.$object->image;

                $objectImages = Storage::allFiles('images/hotels/'.$object->id.'-'.$object->subdomain.'/images-objects');
                foreach($objectImages as $key => $imgObject) {
                    if(strpos($imgObject, 'object_mini') === false) {
                        unset($objectImages[$key]);
                    }
                }

                $oldMainImg = $mainImg;
                $reviews = Review::where('objectId', $object->id)->get();
                $mainImg = preg_replace('/./', '_object_mini.', $mainImg);

                $dateFrom = strtotime(date('d.m.Y'));
                $dateTo = $dateFrom + 86400;
                $countPeriod = 1;

                $minPriceAndRooms = $this->roomsData($room, $dateFrom, $dateTo, $countPeriod, $object);

                $returnHTML = view('popups.quick-view')
                    ->with([
                        'object' => $object,
                        'mainImg' => $mainImg,
                        'objectImages' => $objectImages,
                        'oldMainImg' => $oldMainImg,
                        'reviews' => $reviews,
                        'minPrice' => $minPriceAndRooms['minPrice'],
                        'rooms' => $minPriceAndRooms['rooms']
                    ])->render();

                return response()->json(['html' => $returnHTML, 'latitude' => $object->latitudeGps, 'longitude' => $object->longitudeGps]);

        }
    }

}
