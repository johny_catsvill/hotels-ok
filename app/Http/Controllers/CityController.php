<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\City;

class CityController extends Controller
{
    
    public function load_city_region(Request $request, City $cityModel) {
        
        if($request->isMethod('post')) {
            
            $regionId = $request->regionId;
            $listCitiesByRegion = City::all()->where('regionId', $regionId);
            return $listCitiesByRegion;
            
        }
        
    }
    
    
}
