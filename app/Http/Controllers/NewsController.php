<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\News;
use App\Models\CategoryNews;
use DB;
use Carbon\Carbon;

class NewsController extends Controller
{
    public function index(Request $request, $categoryOrYear = false) {

        $seo = false;


        $categoriesYear = DB::table('news')->select('year', DB::raw('count(id) as countNews'))->groupBy('year')->get();
        $categories = CategoryNews::withCount('news')->where('active', 1)->get();
        if($categoryOrYear) {
            $category = CategoryNews::where('slug', $categoryOrYear)->first();

            if($category) {
                $seo = (object)[];
                $seo->title = $category->name;
                $seo->keywords = $category->keywords;
                $seo->description = $category->description;
            }

            $news = News::where('active', 1)->where('date_published', '<', Carbon::now()->format('Y-m-d H:i'));
            if($category) {
                $this->breadcrumbs += ['Новости' => route('news'), $category->name => route('news', $category->slug)];
                $news = $news->where('category_id', $category->id);
            }
            else {
                if(is_integer(+$categoryOrYear)) {
                    $this->breadcrumbs += ['Новости' => route('news'), $categoryOrYear.' г.' => route('news', $categoryOrYear)];
                    $news = $news->where('year', $categoryOrYear);
                }
            }

            $news = $news->paginate(11);

        }
        else {

            $news = News::where('active', 1)->where('date_published', '<=', Carbon::now()->format('Y-m-d H:i'))->paginate(11);
        }

        if(!count($news)) {
            abort(404);
        }


        $currentPage = $news->currentPage();
        $totalPage = $news->lastPage();

        if($request->page > $totalPage) {
            abort(404);
        }

        $firstNews = $news->first();

        $secondRowsNews = $news->only([2,3,4]);
        $threeRowsNews = $news->only([5,6]);
        $fourRowsNews = $news->only([7,8,9]);
        $fiveRowsNews = $news->only([10, 11]);



        $data = [
            'page' => 'news',
            'breadcrumbs' => $this->breadcrumbs,
            'categories' => $categories,
            'categoriesYear' => $categoriesYear,
            'firstNews' => $firstNews,
            'secondRowsNews' => $secondRowsNews,
            'threeRowsNews' => $threeRowsNews,
            'fourRowsNews' => $fourRowsNews,
            'fiveRowsNews' => $fiveRowsNews,
            'news' => $news,
            'currentPage' => $currentPage,
            'totalPage' => $totalPage

        ];

        if($seo) {
            array_push($data, ['seo' => $seo]);
        }

        return view('site.news', $data);
    }


    public function one_news(Request $request, $category, $id, $slug) {


        $categoriesYear = DB::table('news')->select('year', DB::raw('count(id) as countNews'))->groupBy('year')->get();

        $categories = CategoryNews::withCount('news')->where('active', 1)->get();

        $oneNews = News::where('id', $id)->where('slug', $slug)->firstOrFail();
        $oneNews->views++;
        $oneNews->save();

        $this->breadcrumbs += [__('Новости') => route('news'), $oneNews->name => ''];

        $popularNews = News::orderBy('views', 'desc')->where('id', '!=', $oneNews->id)->take(3)->get();

        $seo = (object)[];
        $seo->title = $oneNews->name;
        $seo->keywords = $oneNews->keywords;
        $seo->description = $oneNews->description;


        return view('site.one-news', [
            'page' => 'news',
            'breadcrumbs' => $this->breadcrumbs,
            'categoriesYear' => $categoriesYear,
            'categories' => $categories,
            'oneNews' => $oneNews,
            'popularNews' => $popularNews,
            'seo' => $seo
        ]);
    }



}
