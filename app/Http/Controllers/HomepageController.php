<?php

namespace App\Http\Controllers;

use App\Models\ObjectHotel;
use App\Models\City;
use App\Models\User;
use Illuminate\Support\Facades\Cache;
use DB;
use Auth;


class HomepageController extends Controller
{
    public function viewHomepage(ObjectHotel $object, City $city)
    {

        if (Cache::has('home')) {
            $view = Cache::get('home');
        }
        else {

            $citiesHotels = Cache::remember('citiesHotels', 60, function () use($object, $city) {
                return $city->getListCitiesUkraine('hotels');
            });

            $citiesHostels = Cache::remember('citiesHostels', 60, function () use($object, $city) {
                return $city->getListCitiesUkraine('hostel');
            });

            $citiesApartments = Cache::remember('citiesApartments', 60, function () use($object, $city) {
                return $city->getListCitiesUkraine('apartment');
            });

            $citiesCategorySea = Cache::remember('citiesCategorySea', 60, function () use($object, $city) {
                return $city->getListCitiesCategory('sea');
            });

            $citiesCategoryMountain = Cache::remember('citiesCategoryMountain', 60, function () use($object, $city) {
                return $city->getListCitiesCategory('mountain');
            });
            $citiesCategoryRecovery = Cache::remember('citiesCategoryRecovery', 60, function () use($object, $city) {
                return $city->getListCitiesCategory('recovery');
            });


            $presentDay = strtotime(date('d-m-Y')); // дата для вывода скидки с текущего дня
            $endDay = $presentDay + 86400;

            $arrayIdCities = [];
            foreach($citiesHotels as $oneCity) {
                array_push($arrayIdCities, $oneCity->id);
            }


            $listPromotialOffers = $object->getListObjectsWithoutCityAndCategory($presentDay, $arrayIdCities);

            $view = view('site.home',
                [
                    'page' => $this->page['start'],
                    'dateFrom' => date('d-m-Y'),
                    'dateTo' => date('d-m-Y', $endDay),
                    'listPromotialOffers' => $listPromotialOffers,
                    'citiesHotels' => $citiesHotels,
                    'citiesHostels' => $citiesHostels,
                    'citiesApartments' => $citiesApartments,
                    'citiesCategorySea' => $citiesCategorySea,
                    'citiesCategoryMountain' => $citiesCategoryMountain,
                    'citiesCategoryRecovery' => $citiesCategoryRecovery
                ]
            )->render();

            Cache::put('home', $view, 10);
        }

        return $view;

    }

    public function checkPromocode() {
        $user = User::find(Auth::id());
        if($user && $user->clientId && $user->client->isPromocode) {
            return 1;
        }
        return 0;
    }

}
