<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Models\ObjectHotel;
use Auth;
use DB;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;




    public $search =
        [
            'hide' => 0,
            'show' => 1
        ];

    public $page =
        [
            'reservation' => 'reservationPage',
            'about' => 'aboutCompany',
            'category' => 'categoryPage',
            'start' => 'startPage',
            'object' => 'objectPage',
        ];


    public $data =
        [
            'title' => '',
            'keywords' => '',
            'description' => ''
        ];

    public $breadcrumbs =
        [
            'Главная' => '/'
        ];
    
    protected function getDomainObject() {
        $user = Auth::user();
        $subdomain = ObjectHotel::find($user->objectId)->subdomain;
        return $subdomain;
    }

    
    public function confirmedAccount() {
        $isConfirmed = 0;
        $objectId = Auth::user()->objectId;
        $isConfirmedAccount = ObjectHotel::getInfoByConfirmedAccount($objectId);
        $errors = DB::table('messages_for_objects')->where('objectId', $objectId)->get();


//        if(isset($isConfirmedAccount) && $isConfirmedAccount->confirmedAccount == 0 && $isConfirmedAccount->objectId) {
//            $isConfirmed = 1;
//        }
//        elseif(isset($isConfirmedAccount) && $isConfirmedAccount->confirmedAccount == 1) {
//            $isConfirmed = 2;
//        }
//
//        if(count($errors) > 0) {
//            return $isConfirmed;
//        }
        
        return $isConfirmedAccount->confirmedAccount;
           
    }
    

}
