<?php

namespace App\Http\Controllers\ControllersAdmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\Func;
use App\Models\Region;
use App\Models\Country;

class RegionController extends Controller
{
    
    public function list_regions(Region $region) {
    
        $this->breadcrumbs += ['Список регионов' => route('list-regions')];
        
        $listRegions = $region->listRegions();
        
        return view('account.admin_layouts.regions.list-regions', [
            'breadcrumbs' => $this->breadcrumbs,
            'listRegions' => $listRegions
        ]);
        
    }
    
    public function edit_region(Request $request, Region $region, Func $func, $id = false) {

        $this->breadcrumbs += ['Добавление региона' => route('edit-region')];
        $listCountries = Country::all();
        $region = false;
        
        if($request->isMethod('post')) {
            $requestAll = $request->all();
            
            if($requestAll['regionId']) {
                $region = Region::find($requestAll['regionId']);
                
                if($region) {
                    $region->name = $requestAll['name'];
                    $region->url = $func->translit($requestAll['name']);
                    $region->countryId = $requestAll['countryId'];
                    $region->save();
                    return redirect()->route('list-regions')->with('message', 'Регион успешно обновлен');
                }
                else {
                    return redirect()->back()->withInput()->withErrors('Неккоретные данные! Попробуйте еще раз!');
                }
                
            }
            
            else {
                Region::create([
                    'name' => $requestAll['name'],
                    'url' => $func->translit($requestAll['name']),
                    'countryId' => $requestAll['countryId']
                ]);
                return redirect()->route('list-regions')->with('message', 'Регион успешно добавлен');

            }
            
        }
        
        
        
        if($id) {
            $region = Region::find($id);
            if(!$region) {
                return redirect()->route('list-regions')->withErrors('Региона с таким идентификатором не существует!');
            }
        }
        
        return view('account.admin_layouts.regions.edit-region', [
            'breadcrumbs' => $this->breadcrumbs,
            'region' => $region,
            'listCountries' => $listCountries
        ]);
        
    }
    
    public function delete_region($id) {
        if($id) {
            $region = Region::find($id);
            $region->delete();
            return redirect()->back()->with('message', 'Регион успешно удален!');
        }
    }
    
    
}
