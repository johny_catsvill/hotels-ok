<?php

namespace App\Http\Controllers\ControllersAdmin;

use App\Models\Bonus;
use App\Models\Clients;
use App\Models\EventCalendar;
use App\Models\User;
use App\Traits\timeTraits;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Reservation;
use App\Models\Room;
use DB;
use Session;
use App\Models\ObjectHotel;
use App\Models\CategoriesTravel;
use Mail;
use App\Models\Review;
use App\Helpers\GenerateCode;

class ReservationController extends Controller
{

    use timeTraits;
    
    public function index(Request $request, Reservation $reservationModel) {


        $this->breadcrumbs += ['Бронирование' => route('reservation-admin')];
        
        $reservations = $reservationModel->getListReservation(false, false, time(), false, false, 0, 1);

        $reservationsPast = $reservationModel->getListPastReservation(strtotime(date('d.m.Y')));

        $reservationsActive = $reservationModel->getListReservation(false, false, false, 3, false, 0, 0);

        
        return view('account.admin_layouts.reservation.reservation', [
            'breadcrumbs' => $this->breadcrumbs,
            'reservations' => $reservations,
            'reservationsPast' => $reservationsPast,
            'reservationsActive' => $reservationsActive
        ]);
    }
    
    public function edit(Request $request, Reservation $reservationModel, ObjectHotel $objectHotelModel, Room $roomModel) {
        $this->breadcrumbs += ['Создание брони' => route('reservation-edit')];
        
        $object = false;
        $dateFrom = false;
        $dateTo = false;
        $reservation = false;
        $roomsReservation = false;
        
        $requestAll = $request->all();
        
        if(isset($requestAll['objectId']) && !isset($requestAll['reservationId'])) {
            $object = $objectHotelModel->getInfoByObjectId($requestAll['objectId']);
            
        }
        
        if(isset($requestAll['dateFrom']) && !isset($requestAll['reservationId'])) {
            $dateFrom = $requestAll['dateFrom'];
        }
        
        if(isset($requestAll['dateTo']) && !isset($requestAll['reservationId'])) {
            $dateTo = $requestAll['dateTo'];
        }
        
        if(isset($requestAll['reservationId'])) {
            $reservation = $reservationModel->getInfoByReservationId($requestAll['reservationId']);
            $roomsReservation = $reservationModel->getInfoRoomsByReservationId($requestAll['reservationId']);
            
            $listDaysInPeriod = $this->getDateTimeStartStopByDays(date('d.m.Y', $reservation->startReservationDate), date('d.m.Y', $reservation->endReservationDate));
            array_pop($listDaysInPeriod);
            foreach($listDaysInPeriod as $date => $datePeriod) {
                $listDaysInPeriod[$date] = strtotime($date);
            }
            
            
            
            $periodCount = count($listDaysInPeriod); 
            $listRooms = $roomModel->getListRoomsInReservationByObjectId($reservation->objectId, $listDaysInPeriod);
            $reservation->listRooms = $listRooms;

            
            foreach($listRooms as $key => $r) {
                
                $roomDate = explode(',', $r->listDate);
                if(count($roomDate) != $periodCount) {
                    unset($listRooms[$key]);
                }
            }
            
            
        }
        
        
        
        return view('account.admin_layouts.reservation.edit', [
            'breadcrumbs' => $this->breadcrumbs,
            'object' => $object,
            'dateFrom' => $dateFrom,
            'dateTo' => $dateTo,
            'reservation' => $reservation,
            'roomsReservation' => $roomsReservation
        ]);
        
    }
    
    public function get_info_room_price(Request $request, Room $roomModel, Reservation $reservationModel) {
        
        if($request->isMethod('post')) {
            $requestAll = $request->all();
            $dateFrom = $requestAll['dateFrom'];
            $dateTo = $requestAll['dateTo'];
            $roomId = $requestAll['roomId'];
            $reservationId = $requestAll['reservationId'];
            $reservation = Reservation::find($reservationId);
            $reservedRoom = $requestAll['selectedRoomId'];
            try {
                if($reservedRoom) {
                    $reservationModel->deletePreReservationRoom($reservedRoom);
                }


                $period = $this->getDateTimeStartStopByDays($dateFrom, $dateTo);
                foreach($period as $date => $datePeriod) {
                    $period[$date] = strtotime($date);
                }

                $amountPerson = 0;
                $roomMiniInfo = $roomModel->getInfoRoom($roomId);

                if($roomMiniInfo) {
                    $amountPerson = $roomMiniInfo->amountPerson;
                }

                array_pop($period);
                $periodCount = count($period);
                $roomInfo = $roomModel->getListRoomsReservation($reservation->objectId, $period, $roomId, $dateFrom, $dateTo);

                $fullPrice = 0;
                $fullPriceDiscount = 0;
                $priceForDay = 0;
                $selectedRoomId = 0;

                $r = (array)$roomInfo[0];
                for($i = 1; $i <= $periodCount; $i++) {
                    $fullPrice += $r['price'.$i];
                    $currentPrice = ($r['price'.$i] - ($r['price'.$i] * $r['discount'.$i]) / 100);
                    $fullPriceDiscount += $currentPrice;
                }


                $selectedRoomId = $reservationModel->preReservationRoom($roomId, $fullPrice, $fullPriceDiscount, $reservationId, $priceForDay, $roomMiniInfo->amountPerson);

                
            }

            catch(QueryException $e) {
                DB::rollBack();
            }

            DB::commit();
            
            
            return [
                'fullPrice' => $fullPrice,
                'fullPriceDiscount' => $fullPriceDiscount,
                'countDay' => $periodCount,
                'selectedRoomId' => $selectedRoomId,
                'priceForDay' => $priceForDay,
                'amountPerson' => $amountPerson
            ];
            
            
        }
        
    }
    
    
    public $fullPrice = 0;
    public $prepaymentPrice = 0;
    
    public function save_full_reservation(Request $request, Room $roomModel, Reservation $reservationModel) {
        
        $errors = false;

        if($request->isMethod('post')) {
            $requestAll = $request->all();
            
            $reservationInformation = $requestAll['reservationInformation'];
            $infoAboutUserReservation = $reservationInformation['reservationInformation'];

            $roomsReservation = $reservationInformation['rooms'];

            DB::beginTransaction();
            try {
                $roomModel->updateRoomsByReservationId($infoAboutUserReservation['reservationId'], $roomsReservation);
                foreach($roomsReservation as $currentRoom) {
                    $roomPrice = $roomModel->getListPriceByRooms($currentRoom['roomId'], $infoAboutUserReservation['reservationId']);
                    $this->fullPrice += $roomPrice->price;
                    $this->prepaymentPrice += $this->typePrepayment($roomPrice);
                }
                
                $reservationModel->fullEditReservationById($infoAboutUserReservation, $this->fullPrice, $this->prepaymentPrice);
                Session::flash('message', 'Бронь успешно сохранена!');
                
                
            }
            catch(QueryException $e) {
                DB::rollBack();
                $errors = true;
            }
            
            DB::commit();
            
            if(!$errors) {
                return ['link' => route('reservation-admin'), 'success' => true];
            }
            
            return ['success' => false];
            
            
        }
        
    }
    
    public function typePrepayment($roomPrice) {
        
        $pricePrepayment = 0;
        
        switch(+$roomPrice->prePaymentId) {
            case 2:
                $pricePrepayment += $roomPrice->priceForDay;
                break;
            case 3:
                $pricePrepayment += $roomPrice->price;
                break;
            default:
                $pricePrepayment += 0;
        }
        
        return $pricePrepayment;
        
    }
    
    
    public function delete_room_from_reservation(Request $request, Reservation $reservationModel) {
        
        if($request->isMethod('post')) {
            $requestAll = $request->all();
            $errors = false;
            
            DB::beginTransaction();
            try {
//                $roomInfoReservation = $reservationModel->getIdByRoomReservation($requestAll['reservationId'], $requestAll['roomId']);
                $reservation = Reservation::find($requestAll['reservationId']);
                $reservationModel->deleteRoomFromReservation($requestAll['reservationId'], $requestAll['roomId']);
                $reservationModel->deleteReservRoomInEventCalendar($requestAll['reservationId'], $requestAll['roomId'], $reservation->objectId);
            }
            catch(QueryException $e) {
                DB::rollBack();
                $errors = true;
            }
            
            DB::commit();
            
            return ['success' => $errors];    
            
        }
        
    }
    
    
    public function delete_reservation(Request $request, Reservation $reservationModel, $reservationId) {
        if($request->isMethod('get')) {
            $requestAll = $request->all();
            $errors = false;
            
            DB::beginTransaction();
            try {
                $reservationModel->deleteReservationById($reservationId);
            }
            catch(QueryException $e) {
                DB::rollBack();
                $errors = true;
            }
            
            DB::commit();
            
            if(!$errors) {
                return redirect()->route('reservation-admin')->with('message', 'Бронь успешно удалена!');
            }
            
            
        }
    }
    
    
    public function info_reservation(Request $request, Reservation $reservationModel, Room $roomModel, $id = false) {
        
        $this->breadcrumbs += ['Информация по броне' => route('info-by-reservation', $id)];
        
        if(!$id) return redirect()->back()->withError('Где индетификатор брони епт!');
        
        $reservation = $reservationModel->getInfoByReservationId($id);
        
        $roomsReservation = $reservationModel->getInfoRoomsByReservationId($id);

        return view('account.info_by_reservation', [
            'breadcrumbs' => $this->breadcrumbs,
            'reservation' => $reservation,
            'roomsReservation' => $roomsReservation
            
        ]);
        
    }
    
    public function searchObject(Request $request, ObjectHotel $objectHotelModel, Room $roomModel) {
        
        $this->breadcrumbs += ['Поиск обьектов' => route('search-object')];
        
        return view('account.search-object', [
            'breadcrumbs' => $this->breadcrumbs
        ]);
        
    }
    
    public function search_object_for_reservation(Request $request, CategoriesTravel $categoriesTravel) {
        if(isset($_POST['stringSearch'])) {
            $count = 0;
            $searchValue = $_POST['stringSearch'];
            $resultSearch = $categoriesTravel->getListCitiesAndRegionsAndObjectsAboutSearch($searchValue);
            $list = array('city' => array(), 'region' => array(), 'object' => array());
            foreach($resultSearch as $result) {
                switch ($result->currentTable) {
                    case 'city':
                        $list['city'][$count] = ['id' => $result->id, 'url' => $result->url,
                            'name' => $result->name, 'countryId' => $result->countryId];
                        break;
                    case 'region':
                        $list['region'][$count] = ['id' => $result->id, 'url' => $result->url,
                            'name' => $result->name, 'countryId' => $result->countryId];
                        break;
                    case 'object':
                        $list['object'][$count] = ['id' => $result->id, 'url' => $result->url,
                            'name' => $result->name, 'countryId' => $result->countryId];
                        break;
                }
                $count++;
            }
            return $list;
        }
    }
    
    public function search_object_by_filters(Request $request, Reservation $reservationModel, Room $roomModel) {
        if($request->isMethod('post')) {
            $requestAll = $request->all();
            $amountPerson = $requestAll['amountPerson'];
            $dateFrom = $requestAll['dateFrom'];
            $dateTo = $requestAll['dateTo'];
            $priceFrom = $requestAll['priceFrom'];
            $priceTo = $requestAll['priceTo'];
            $category = $requestAll['category'];
            $searchId = $requestAll['searchId'];
            $discount = $requestAll['discount'];
            $page = $requestAll['page'];

            $periods = $this->getDateTimeStartStopByDays($dateFrom, $dateTo);
            array_pop($periods);
            
            $countPeriod = count($periods);
            foreach($periods as $key => $p) {
                $periods[$key] = strtotime($key);
            }
            
            
            
            $objects = $reservationModel->getListObjectsByAdmin($amountPerson, $periods, $countPeriod, $priceFrom, $priceTo, $category, $searchId, $discount, $page);
            if($objects) {
                
                foreach($objects as $key => $object) {
                    $object = ObjectHotel::find($object->id);
                    $roomsObject = $roomModel->getListRoomsByDateInSearchAdmin($periods, $object, $priceFrom, $priceTo, $discount, $amountPerson);

                    $rooms = [];
                    foreach($roomsObject as $k => $room) {
                        $roomsObject[$k]->price = 0;
                        $newArrayRoom = (array)$room;
                        $arrayCalendarCount = [];
                        for($i = 1; $i <= $countPeriod; $i++) {
                            array_push($arrayCalendarCount, $newArrayRoom['calendar'.$i]);

                            $currentPrice = ($newArrayRoom['price'.$i] - ($newArrayRoom['price'.$i] * $newArrayRoom['discount'.$i]) / 100);
                            $roomsObject[$k]->price += $currentPrice;
                        }

                        $room->countFreeRoom = min($arrayCalendarCount);
                        array_push($rooms, $room);
                    }

                    $object->rooms = $rooms;
                    $objects[$key] = $object;
                }
                $returnHTML = view('account.search-object-block')->with(['objects' => $objects, 'period' => $countPeriod, 'dateFrom' => $dateFrom, 'dateTo' => $dateTo])->render();

                return response()->json(array('success' => true, 'html' => $returnHTML, 'countObjects' => count($objects)));
            }
                
            
            return false;
        }
    }
    
    
    
    public function change_status_reservation_past(Request $request, Reservation $reservationModel, Review $reviewModel) {
        if($request->isMethod('post')) {
            $requestAll = $request->all();
            $reservationModel->changeIsPastByReservationId($requestAll['reservationId'], $requestAll['isPast']);

            
            $reservationInfo = $reservationModel->getMiniInfoByReservationId($requestAll['reservationId']);


            if($requestAll['isPast'] && $reservationInfo->userId && $reservationInfo->bonusId) {
                $user = User::find($reservationInfo->userId);
                if($user) {
                    $client = Clients::find($user->clientId);
                    $bonus = Bonus::find($reservationInfo->bonusId);
                    if($client && $bonus) {
                        $client->balans += $reservationInfo->price * $bonus->procent / 100;
                        $client->save();
                    }
                }
            }



            $code = GenerateCode::generateCode(10);

            $reviewModel->createQueryForReview($reservationInfo->id, $code, $reservationInfo->visitorEmail, $requestAll['reservationId']);

            return 1;
        }
    }
    
    public function change_status_reservation(Request $request, Reservation $reservationModel, $reservationId, $statusId) {

        $error = true;
        $messagesError = [];

        if($reservationId && $statusId) {

            $reservation = Reservation::find($reservationId);
            $listRooms = $reservationModel->getListRelationRoomByReservationId($reservationId);

            if($statusId == 3) {
                DB::beginTransaction();

                try {

                    $reservationModel->changeStatusReservation($reservationId, $statusId);

                    foreach($listRooms as $room) {
                        $eventCalendarRoom = $this->findEventCalendarByRoomId($room->roomId, $reservation->objectId, $reservation->startReservationDate, $reservation->endReservationDate);
                        if($eventCalendarRoom) {
                            $this->saveRoomInEventCalendar($room->roomId, $room->id, $reservation->objectId, $reservation->startReservationDate, $reservation->endReservationDate, $reservation->id);
                        }
                        else {
                            $error = false;
                            array_push($messagesError, 'В календаре нету дат для комнаты '. $room->roomId);
                        }

                    }

                }
                catch(QueryException $e) {
                    DB::rollBack();
                    return redirect()->back()->withErrors([$e]);
                }

                DB::commit();


                Mail::send('mail.certificate', ['reservation' => $reservation], function($message)
                {
                    $message->from('info@hotels-ok.com');
                    $message->to('batenko4@gmail.com')->subject('Ticket reservation');
                });


                return redirect()->route('reservation-admin')->with('message', 'Статус успешно изменен! Даты в календаре успешно обновлены!');
            }

            if($statusId = 2) {

                DB::beginTransaction();

                try {
                    $reservationModel->changeStatusReservation($reservationId, $statusId);

                    $this->removeFromCalendarByReservation($reservationId);
                }

                catch(QueryException $e) {
                    DB::rollBack();
                    return redirect()->back()->withErrors([$e]);
                }

                DB::commit();
                return redirect()->route('reservation-admin')->with('message', 'Статус успешно изменен на отменен! Обидно');
            }


        }
        
    }

    public function saveRoomInEventCalendar($roomId, $reservedRoomId, $objectId, $dateFrom, $dateTo, $reservationId) {

        $counter = 0;

        $periods = $this->getDateTimeStartStopByDays(date('d.m.Y', $dateFrom), date('d.m.Y', $dateTo));
        array_pop($periods);

        $countPeriod = count($periods);

        foreach($periods as $key => $p) {
            $periods[$key] = strtotime($key);
            $calendar = EventCalendar::where('typeRoomId', $roomId)->where('date', strtotime($key))->where('objectId', $objectId)->first();
            if($calendar) {
                $counter++;
                $calendar->reservationId = $reservationId;
                $calendar->reservedRoomId = $reservedRoomId;
                $calendar->save();
            }
        }

        if($counter < $countPeriod) {
            throw new exception('На комнату'. $roomId. 'нет дат в календаре! Звоните администраторам отеля, пусть выставляют!');
        }

    }

    public function findEventCalendarByRoomId($roomId, $objectId, $dateFrom, $dateTo) {

        $error = true;
        $arrayCalendarCount = [];

        $periods = $this->getDateTimeStartStopByDays(date('d.m.Y', $dateFrom), date('d.m.Y', $dateTo));
        array_pop($periods);

        $countPeriod = count($periods);
        foreach($periods as $key => $p) {
            $periods[$key] = strtotime($key);
        }

        $room = new Room;
        $currentRoom = $room->getListRoomsReservation($objectId, $periods, $roomId, $dateFrom, $dateTo);

        $currentRoom = (array)$currentRoom[0];

        for($i = 1; $i <= $countPeriod; $i++) {
            array_push($arrayCalendarCount, $currentRoom['calendar'.$i]);
        }

        $minCountFreeRoom = min($arrayCalendarCount); //получаю минимальное число доступных номеров

        if($minCountFreeRoom > 0) { // проверка есть ли столько штук сколько выбрал пользователь
            $error = true;
        }
        else {
            $error = false;
        }

        return $error;


    }

    public function removeFromCalendarByReservation($reservationId) {

        EventCalendar::where('reservationId', $reservationId)->update([
            'reservationId' => 0,
            'reservedRoomId' => 0
        ]);
    }

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
