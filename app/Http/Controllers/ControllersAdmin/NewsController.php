<?php

namespace App\Http\Controllers\ControllersAdmin;

use App\Traits\croppedPhotos;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\News;
use App\Models\CategoryNews;
use Validator;

class NewsController extends Controller
{

    use croppedPhotos;

    public function news() {
        $news = News::all();
        return view('account.admin_layouts.news.index', compact('news'));
    }

    public function news_category() {
        $categories = CategoryNews::all();
        return view('account.admin_layouts.news.category-news.index', compact('categories'));
    }

    public function edit_category_news(Request $request, $id = false) {

        $category = false;

        if($request->isMethod('post')) {
            $data = $request->all();
            if($id) {
                $category = CategoryNews::find($id);
                $category->name = $data['name'];
                $category->position = $data['position'];
                $category->slug = $data['slug'];
                $category->keywords = $data['keywords'];
                $category->description = $data['description'];

                if(isset($data['active'])) {
                    $category->active = 1;
                }
                else {
                    $category->active = 0;
                }
                $category->save();
                return redirect()->route('news-category-admin')->with('message', 'Категория успешно обновлена');

            }

            $category = new CategoryNews;
            $category->name = $data['name'];
            $category->position = $data['position'];
            $category->keywords = $data['keywords'];
            $category->description = $data['description'];

            if(isset($data['active'])) {
                $category->active = 1;
            }
            else {
                $category->active = 0;
            }

            $category->slug = $data['slug'];

            $category->save();

            return redirect()->route('news-category-admin');

        }

        if($id) {
            $category = CategoryNews::find($id);
        }

        return view('account.admin_layouts.news.category-news.edit-category-news', compact('category'));
    }

    public function delete_category_news(Request $request, $id) {

        if($id) {
            $category = CategoryNews::find($id);
            $category->delete();
            return redirect()->back()->with('message', 'Категория успешно удалена');
        }

        return redirect()->back();

    }

    public function edit_news(Request $request, $id = false) {

        $news = false;

        if($id) {
            $news = News::find($id);
        }

        if($request->isMethod('post')) {
            $data = $request->all();

            $validator = Validator::make($data, [
                'name' => 'required',
                'slug' => 'required',
                'category_id' => 'required',
                'text' => 'required'
            ]);

            if($validator->fails()) {
                return redirect()->back()->withErrors($validator->errors())->withInput();
            }

            if($id) {
                $news = News::find($id);
            }
            else {
                $news = new News;
            }

            $news->name = $data['name'];
            $news->slug = $data['slug'];
            $news->keywords = $data['keywords'];
            $news->description = $data['description'];
            $news->category_id = $data['category_id'];
            $news->text = $data['text'];
            $news->date_published = $data['date_published'];
            $news->year = $data['year'];

            if(isset($data['active'])) {
                $news->active = 1;
            }
            else {
                $news->active = 0;
            }

            if($request->hasFile('image')) {
                $image = $request->file('image');

                if($image->getClientOriginalName() != $news->image) {
                    $this->removePhoto($news->image, 'images-news');
                }

                $news->image = $news->id.'.'. $image->getClientOriginalExtension();
                $image->move(public_path() . '/images/news/', $news->image);
                $this->cropPhoto('/images/news/', $news->image, $news->id, $image->getClientOriginalExtension(), 'images-news');
            }



            $news->save();

            return redirect()->route('news-admin')->with('message', 'Новость успешно создана!');
        }


        $categories = CategoryNews::where('active', 1)->get();

        if(old()) {
            $news = (object)old();
        }



        return view('account.admin_layouts.news.edit-news', compact('categories', 'news'));

    }

    public function delete_news($id) {
        if($id) {
            $news = News::find($id);
            $news->delete();
            return redirect()->back()->with('message', 'Новость успешно удалена');
        }

        return redirect()->back()->withErrors(['Статьи с таким идентификатором не существует']);

    }

}
