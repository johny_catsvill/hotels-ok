<?php

namespace App\Http\Controllers\ControllersAdmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\SiteReview;


class ReviewSiteController extends Controller
{
    public function reviews_project() {

        $reviews = SiteReview::all();
        return view('account.admin_layouts.reviews.index', compact('reviews'));
    }

    public function addReviewProject(Request $request, $id = false) {

        $review = false;

        if($request->isMethod('post')) {
            $data = $request->all();

            if(isset($data['reviewId']) && $data['reviewId']) {
                $review = SiteReview::find($data['reviewId']);

                $message = 'Отзыв успешно обновлен!';
            }
            else {
                $review = new SiteReview();
                $message = 'Отзыв успешно создан!';
            }

            $review->name = $data['name'];
            $review->raiting = $data['raiting'];
            $review->text = $data['text'];

            if(isset($data['active']) && $data['active'] == 'on') {
                $review->active = 1;
            }
            else {
                $review->active = 0;
            }

            $review->save();

            return redirect()->route('reviews-project')->with('message', $message);


        }

        if($id) {
            $review = SiteReview::find($id);
        }


        return view('account.admin_layouts.reviews.edit', compact('review'));
    }


    public function deleteReviewProject($id) {
        $review = SiteReview::find($id);
        $review->delete();
        return redirect()->route('reviews-project')->with('message', 'Отзыв о проекте был успешно удален!');
    }

}
