<?php

namespace App\Http\Controllers\ControllersAdmin;

use App\Helpers\Func;
use App\Helpers\GenerateCode;
use App\Models\City;
use App\Models\CodesActivated;
use App\Models\ObjectsCategories;
use App\Models\User;
use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ObjectHotel;
use App\Helpers\Validate;
use Storage;
use App\Traits\timeTraits;
use App\Models\ObjectsTypesPluses;
use App\Models\BesideObject;
use Mail;
use DB;
use Auth;
use Validator;


class ObjectController extends Controller
{
    use timeTraits;
    
    public function getObjects(Request $request, ObjectHotel $objectHotelModel) {

        if($request->isMethod('post')) {
            $requestAll = $request->all();
            
            $listObjects = $objectHotelModel->getListObjectsByName($requestAll['object']);

            $returnHTML = view('account.admin_layouts.blocks.block-list-objects')->with(['listObjects' => $listObjects])->render();
            return response()->json(array('success' => true, 'html' => $returnHTML));
        }
    }
    
    public function check_object(Request $request, ObjectHotel $objectHotelModel) {
        $this->breadcrumbs += ['Проверка обьекта' => route('check-object')];


        $listObjects = $objectHotelModel->getListObjectsAdmin();

        return view('account.admin_layouts.check-object.check-object', [
            'breadcrumbs' => $this->breadcrumbs,
            'listObjects' => $listObjects
        ]);

    }
    
    
    public function checked_object_id(Request $request, ObjectHotel $objectHotelModel, ObjectsTypesPluses $objectPlusesModel, BesideObject $besideObjectModel, $id) {
        
        if(!$id && !Validate::id($id)) return redirect()->back();
        
        $this->breadcrumbs += ['Обьект №'.$id => route('checked-object-id', $id)];

        $object = $objectHotelModel->getInfoByObjectIdAdmin($id);
        $currentObject = ObjectHotel::find($id);

        $errors = DB::table('messages_for_objects')->where('objectId', $currentObject->id)->get();

        $galleryImages = Storage::files('/images/hotels/'.$id.'-'.$object->subdomain.'/images-objects/');
        foreach($galleryImages as $key => $imgObject) {
            if(strpos($imgObject, 'category') === false) {
                unset($galleryImages[$key]);
            }
        }
        
        $object->checkIn = $this->translateToHours($object->checkIn);
        $object->checkOut = $this->translateToHours($object->checkOut);


        $listBesideObject = $besideObjectModel->getListBesideByObjectId($object->id);



        
        return view('account.admin_layouts.check-object.object', [
            'breadcrumbs' => $this->breadcrumbs,
            'object' => $object,
            'listBesideObject' => $listBesideObject,
            'galleryImages' => $galleryImages,
            'currentObject' => $currentObject,
            'id' => $id,
            'errors' => $errors
        ]);
        
        
        
    }
    
    public function confirmed_object(ObjectHotel $objectHotelModel, $id) {
        if(!$id && !Validate::id($id)) return redirect()->back();
        $objectHotelModel->confirmedObjectAdmin($id);
        DB::table('messages_for_objects')->where('objectId', $id)->delete();
        return redirect()->route('check-object')->with('message', 'Аккаунт успешно подтвержден');
        
    }
    
    public function application_object(Request $request, ObjectHotel $objectHotelModel) {
        $this->breadcrumbs += ['Заявки обьектов' => route('application-object')];
        $listApplicationObject = $objectHotelModel->getListApplicationObject();
        $categoriesObjects = ObjectsCategories::all();
        $cities = City::orderBy('name')->get();

        return view('account.admin_layouts.application-object', [
            'breadcrumbs' => $this->breadcrumbs,
            'listApplicationObject' => $listApplicationObject,
            "categoriesObjects" => $categoriesObjects,
            'cities' => $cities
        ]);
    }
    
    
    public function save_application_object(Request $request, ObjectHotel $objectHotelModel, Func $functionHelper) {
        
        if($request->isMethod('post')) {
            
            $requestAll = $request->all();
            if($requestAll['submitCreate']) {

                $messages = [
                    'typeObject.required' =>                                    'Поле «Тип объекта» обязательное к заполнению!',
                    'name.required' =>                                          'Поле «Имя организации» обязательное к заполнению!',
                    'name.min' =>                                               'Поле «Имя организации» должно состоять минимум из 2 символов!',
                    'email.required' =>                                         'Поле «Электронный адрес» обязательное к заполнению!',
                    'email.email' =>                                            'Некорректный электронный адрес!',
                    'email.unique' =>                                           'К сожалению пользователь с таким электронным адресом уже зарегистрирован в системе',
                    'password.required' =>            'Поле "Пароль" обязательное к заполнению',
                    'password.min' =>                 'Поле "Пароль" должно состоять минимум из 8 символов',
                    'password.same' =>                'Поле "Повторите пароль" должно состоять минимум из 8 символов и быть таким же как и значение поля "Пароль"!',
                    'phone.required' =>                                         'Поле "Телефон" обязательное к заполнению!',
                    'phone.min' =>                                              'Поле "Телефон" должно состоять из 10 символов!',

                ];

                $validator = Validator::make($requestAll, [
                    'typeObject' => 'required|integer',
                    'city' => 'required',
                    'name' => 'required|string|min:2',
                    'email' => 'required|email|unique:users,email',
                    'phone' => 'required|min:10',

                ], $messages);

                if($validator->fails()) {
                    return redirect()->back()->withInput()->withErrors($validator->errors());
                }

                DB::beginTransaction();
                try {
                    $object = new ObjectHotel;

                    $city = City::find($requestAll['city']);
                    $object->organizationName = $requestAll['object_name'];
                    $object->email = $requestAll['email'];
                    $object->cityId = $requestAll['city'];
                    $object->parentCityId = $city->parentId;
                    $object->objectCategoryId = $requestAll['typeObject'];
                    $object->subdomain = $functionHelper->translit($requestAll['object_name']);
                    $object->save();

                    $password = GenerateCode::generateCode(10);

                    $user = new User();
                    $user->email = $requestAll['email'];
                    $user->phone = $requestAll['phone'];
                    $user->password = bcrypt($password);
                    $user->objectId = $object->id;
                    $user->creationDate = strtotime(date('d.m.Y'));
                    $user->save();

                    $codeActivate = GenerateCode::generateCode(8);

                    $code = new CodesActivated();
                    $code->userId = $user->id;
                    $code->code = $codeActivate;
                    $code->save();

                    $url = url('/').'/auth/activate?id='.$user->id.'&code='.$codeActivate;

                }
                catch(QueryException $e) {
                    DB::rollBack();
                    return redirect()->back()->withErrors(['Есть непредвиденная ошибка. Попробуй еще раз и внимательнее!']);
                }

                //DB::commit();

                Mail::send('mail.confirmation-account', array('url' => $url, 'email' => $requestAll['email'], 'password' => $password), function($message) use ($requestAll)
                {
                    $message->from('info@hotels-ok.com');
                    $message->to($requestAll['email'])->subject('Регистрация организации в hotels-ok.com');
                });

                return redirect()->route('application-object')->with('message', 'Обьект был успешно создан! На почту отправлено письмо с подтверждением!');

            }
            else {
                $requestAll['is_send'] = 1;
                $objectHotelModel->createApplicationForObject($requestAll);
                $url = public_path();
                Mail::send('mail.instruction', array('url' => $url), function($message) use ($requestAll)
                {
                    $message->from('info@hotels-ok.com');
                    $message->to($requestAll['email'])->subject('Инструкция от Hotels-ok');
                });
                return redirect()->back()->with('message', 'Заявка успешно сохранена!');
            }

        }
    }

    public function save_errors(Request $request) {

        if($request->isMethod('post')) {
            $data = $request->all();
            $errors = $data['errors'];
            $objectId = $data['objectId'];
            $object = ObjectHotel::find($objectId);

            DB::table('messages_for_objects')->where('objectId', $objectId)->delete();

            foreach ($errors as $error) {
                $errorArray = [
                    'creationDate' => time(),
                    'creationUserId' => Auth::id(),
                    'objectId' => $objectId,
                    'errorId' => $error['id'],
                    'message' => $error['text']

                ];

                DB::table('messages_for_objects')->insert($errorArray);
            }
            DB::table('confirmed_object')->where('objectId', $objectId)->update([
                'confirmedAccount' => 0
            ]);


            session()->flash('message', 'Ошибки успешно отправлены!');
        }


    }
    
    
    
    
    
}
