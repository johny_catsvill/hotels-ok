<?php

namespace App\Http\Controllers\ControllersAdmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Question;
use App\Models\Answer;

class QuestionAnswerController extends Controller
{
    public function listQuestions() {

        $listQuestions = Question::withCount('answers')->get();
        return view('account.admin_layouts.questions.index', compact('listQuestions'));
    }

    public function editQuestion(Request $request, $id = false) {

        $question = false;
        $answers = false;
        if($request->isMethod('post')) {
            $data = $request->all();
            if($data['question_id']) {
                $question = Question::find($data['question_id']);
                $question->name = $data['name'];
                if ($request->hasFile('image')) {

                    $file=$request->file('image');

                    if($file->getClientOriginalName() != $question->image) {
                        Storage::delete('/images/test/'.$question->image);
                    }

                    $file->move(public_path('\images\test'),$file->getClientOriginalName());
                    $question->image = $file->getClientOriginalName();
                }

                $question->save();
                return redirect()->route('questions')->with('message', 'Вопрос успешно обновлен');
            }
            else {
                $question = new Question();
                $question->name = $data['name'];

                if ($request->hasFile('image')) {
                    $file=$request->file('image');
                    $file->move(public_path('\images\test'),$file->getClientOriginalName());
                    $question->image = $file->getClientOriginalName();
                }

                $question->save();
                return redirect()->route('questions')->with('message', 'Вопрос успешно создан');
            }
        }

        if($id) {
            $question = Question::find($id);
            $answers = Answer::where('question_id', $question->id)->get();
        }

        return view('account.admin_layouts.questions.edit', compact('question', 'answers'));

    }

    public function deleteQuestion(Request $request, $id) {

        if($id) {
            Answer::where('question_id', $id)->delete();
            Question::where('id', $id)->delete();
            return redirect()->back()->with('message', 'Вопрос и ответы были успешно удаленны');
        }


    }

    public function editAnswer(Request $request, $id = false) {
        $answer = false;
        $listQuestions = Question::all();
        if($request->isMethod('post')) {
            $data = $request->all();

            if($data['answer_id']) {
                $answer = Answer::find($data['answer_id']);

                if(isset($data['isTrue'])) {
                    $answer->isTrue = $data['isTrue'];
                }

                $answer->good_text = $data['good_text'];
                $answer->bad_text = $data['bad_text'];
                $answer->procent = $data['procent'];
                $answer->question_id = $data['question_id'];
                $answer->answer = $data['answer'];
                $answer->save();
                return redirect()->route('edit-question', $answer->question_id)->with('message', 'Ответ успешно обновлен');
            }
            else {
                $answer = new Answer();

                if(isset($data['isTrue'])) {
                    $answer->isTrue = $data['isTrue'];
                }

                $answer->good_text = $data['good_text'];
                $answer->bad_text = $data['bad_text'];
                $answer->procent = $data['procent'];
                $answer->question_id = $data['question_id'];
                $answer->answer = $data['answer'];
                $answer->save();
                return redirect()->route('edit-question', $answer->question_id)->with('message', 'Ответ успешно добавлен');
            }

        }

        if($id) {
            $answer = Answer::find($id);
        }

        return view('account.admin_layouts.answers.edit', compact('answer', 'listQuestions'));

    }

    public function deleteAnswer($id) {
        if($id) {
            Answer::where('id', $id)->delete();
            return redirect()->back()->with('message', 'Ответ был успешно удален');
        }
    }

}
