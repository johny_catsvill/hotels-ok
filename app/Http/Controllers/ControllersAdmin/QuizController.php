<?php

namespace App\Http\Controllers\ControllersAdmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class QuizController extends Controller
{
    public function quiz() {
        return view('account.quiz.list');
    }

    public function edit(Request $request, $id = false) {
        return view('account.quiz.edit');
    }

    public function delete(Request $request, $id) {

    }
}
