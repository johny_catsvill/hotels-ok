<?php

namespace App\Http\Controllers\ControllersAdmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\City;
use App\Models\Region;
use App\Models\Country;
use Validator;
use App\Helpers\Func;

class CityController extends Controller
{
    
    public function list_cities() {
        $this->breadcrumbs += ['Список городов системы' => route('list-cities')];
        $listCities = City::all();
        
        return view('account.admin_layouts.cities.list-cities', [
            'breadcrumbs' => $this->breadcrumbs,
            'listCities' => $listCities,
        ]);
        
        
    }
    
    public function edit_city(Request $request, Func $func, $id = false) {
        
        $this->breadcrumbs += ['Добавление города' => route('edit-city')];
        $listRegions = Region::all();
        $listCountries = Country::all();
        $city = false;
        
        if($request->isMethod('post')) {
            $requestAll = $request->all();
            if($requestAll['cityId']) {
                $city = City::find($requestAll['cityId']);
                if($city) {
                    $city->name = $requestAll['name'];
                    $city->url = $func->translit($requestAll['name']);
                    $city->phoneCode = $requestAll['phoneCode'];
                    $city->countryId = $requestAll['countryId'];
                    $city->regionId = $requestAll['regionId'];
                    $city->latitudeGps = $requestAll['latitudeGps'];
                    $city->longitudeGps = $requestAll['longitudeGps'];
                    
                    $city->save();    
                    return redirect()->route('list-cities')->with('message', 'Город был успешно обновлен!');    
                }
                else {
                    return redirect()->back()->withInput()->withErrors('Такого города нет в системе!');
                }
            }
            else {
                $validator = Validator::make($requestAll, [
                    'name' => 'required|string|unique:cities,name',
                    'phoneCode' => 'required',
                    'countryId' => 'required|integer|exists:countries,id',
                    'regionId' => 'required|integer|exists:regions,id',
                    'latitudeGps' => 'required',
                    'longitudeGps' => 'required'
                ]);
                
                if($validator->fails()) {
                    return redirect()->back()->withInput()->withErrors('Некорректные данные, попробуйте еще раз!');
                }
                
                City::create([
                    'name' => $requestAll['name'],
                    'url' => $func->translit($requestAll['name']),
                    'phoneCode' => $requestAll['phoneCode'],
                    'countryId' => $requestAll['countryId'],
                    'regionId' => $requestAll['regionId'],
                    'latitudeGps' => $requestAll['latitudeGps'],
                    'longitudeGps' => $requestAll['longitudeGps']
                ]);
                
                return redirect()->route('list-cities')->with('message', 'Город был успешно добавлен в базу данных!');
                 
            }
            
        }
        
        if($id) {
            $city = City::find($id);
            
            if(!$city) {
                return redirect()->route('list-cities')->withErrors('Города с таким идентификатором нет в системе! Пожалуйста выберите город из списка или создайте новый.');    
            }
        }
        
        return view('account.admin_layouts.cities.edit-city', [
            'breadcrumbs' => $this->breadcrumbs,
            'city' => $city,
            'listRegions' => $listRegions,
            'listCountries' => $listCountries
        ]);
        
    }
    
    public function delete_city($id = false) {
        if($id) {
            $city = City::find($id);
            if($city) {
                $city->delete();
                return redirect()->back()->with('message', 'Город был успешно удален из базы данных!');
            }
            else {
                return redirect()->route('list-cities')->withErrors('Город с таким идентификатором отсутсвует в базе данных!');
            }
        }
    }
    
    
}
