<?php

namespace App\Http\Controllers\ControllersAdmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\PlusesRoom;

class CategoryPlusesController extends Controller
{
    
    
    public function list_category_pluses(PlusesRoom $plusesRoomModel) {
        
        $this->breadcrumbs += ['Список категории плюсов' => route('category-pluses-room')];
        
        $listCategoriesPluses = $plusesRoomModel->getListCategoryPluses();
        
        return view('account.admin_layouts.category-pluses.list-category-pluses', [
            'breadcrumbs' => $this->breadcrumbs,
            'listCategoriesPluses' => $listCategoriesPluses
        ]);
          
    }
    
    public function edit_category_pluses(Request $request, PlusesRoom $plusesRoomModel, $id = false) {
        
        $this->breadcrumbs += ['Список категории плюсов' => route('edit-category-pluses')];
        
        $categoryPluses = false;
        
        if($request->isMethod('post')) {
            $requestAll = $request->all();
            if($requestAll['categoryPlusesId']) {
                $categoryPlusesId = $plusesRoomModel->getCategoryPlusesById($requestAll['categoryPlusesId']);
                if($categoryPlusesId) {
                    $plusesRoomModel->updateCategoryPluses($requestAll);
                    return redirect()->route('category-pluses-room')->with('message', 'Категория успешно обновлена!');
                }
                else {
                    return redirect()->back()->withInput()->withErrors('Записи с таким идентификатором не существует.');
                }
            }
            else {
                
                $plusesRoomModel->createCategoryPluses($requestAll);
                return redirect()->route('category-pluses-room')->with('message', 'Категория успешно добавлена!');
                
            }
        }
        
        if($id) {
            $categoryPluses = $plusesRoomModel->getCategoryPlusesById($id);
        }
        
        
        return view('account.admin_layouts.category-pluses.edit-category-pluses', [
            'breadcrumbs' => $this->breadcrumbs,
            'categoryPluses' => $categoryPluses
        ]);
        
    }
    
    public function delete_category_pluses(PlusesRoom $plusesRoomModel, $id = false) {
        if($id) {
            $categoryPlusesId = $plusesRoomModel->getCategoryPlusesById($id);
            if($categoryPlusesId) {
                $plusesRoomModel->deleteRelationCategoryPlusesAndRoom($id);
                $plusesRoomModel->deleteCategoryPluses($id);
                return redirect()->back()->with('message', 'Категория успешно удалена! А так же все связи');
            }    
        }

        return redirect()->route('category-pluses-room')->withErrors('Ошибка удаления категории! Попробуйте заново.');  
    }
}
