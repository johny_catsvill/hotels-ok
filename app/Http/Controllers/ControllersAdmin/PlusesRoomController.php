<?php

namespace App\Http\Controllers\ControllersAdmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\PlusesRoom;

class PlusesRoomController extends Controller
{
    
    public function list_pluses_room(Request $request, PlusesRoom $plusesRoomModel) {
        
        $this->breadcrumbs += ['Список плюсов комнаты' => route('list-pluses-room')];
        
        $listPlusesRoom = $plusesRoomModel->getListPlusesRoom();
        
        return view('account.admin_layouts.pluses-room.list-pluses-room', [
            'breadcrumbs' => $this->breadcrumbs,
            'listPlusesRoom' => $listPlusesRoom
        ]);
        
    }
        
    public function edit_pluse_room(Request $request, PlusesRoom $plusesRoomModel, $id = false) {
    
        $this->breadcrumbs += ['Добавление плюса комнаты' => route('edit-pluse-room')];
        
        $pluseRoom = false;
        
        $listCategoriesPlusesRoom = $plusesRoomModel->getListCategoryPluses();
        if($request->isMethod('post')) {
            $requestAll = $request->all();
            if(!$requestAll['roomCategoryId']) {
                return redirect()->back()->withErrors('Вы не выбрали категорию. Без нее создания плюса комнаты невозможно!');    
            }

            $image = '';
            if($request->hasFile('image')) {
                $file = $request->file('image');
                $name = $file->getClientOriginalName();
                $file->move(public_path().'/images/icon-pluses-room/',$file->getClientOriginalName());
            }
            
            if($requestAll['pluseRoomId']) {
                $pluseRoom = $plusesRoomModel->getInfoPluseRoomById($requestAll['pluseRoomId']);

                if($pluseRoom) {
                    $plusesRoomModel->updatePluseRoomById($requestAll, $name);
                    return redirect()->route('list-pluses-room')->with('message', 'Запись успешно обновлена!');
                }
                else {
                    return redirect()->back()->withInput()->withErrors('Записи с таким идентификатором не существует! Попробуйте заново.');
                }
            }
            else {
                $plusesRoomModel->createPluseRoom($requestAll, $name);
                return redirect()->route('list-pluses-room')->with('message', 'Запись успешно добавлена!');
            }
        }
        
        if($id){
            $pluseRoom = $plusesRoomModel->getInfoPluseRoomById($id);
        }
        
        return view('account.admin_layouts.pluses-room.edit-pluse-room', [
            'breadcrumbs' => $this->breadcrumbs,
            'pluseRoom' => $pluseRoom,
            'listCategoriesPlusesRoom' => $listCategoriesPlusesRoom
            
        ]);
        
    }
        
    public function delete_pluse_room(PlusesRoom $plusesRoomModel, $id = false) {
        if($id) {
            $pluseRoom = $plusesRoomModel->getInfoPluseRoomById($id);
            if($pluseRoom) {
                $plusesRoomModel->deletePluseRoomById($id);
                return redirect()->back()->with('message', 'Запись успешно удалена!');
            }
        }
        
        return redirect()->route('list-pluses-room')->withErrors('Записи с таким идентификатором не существует.');
    }
    
    
    
    
    
    
    
}
