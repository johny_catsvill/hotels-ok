<?php

namespace App\Http\Controllers\ControllersAdmin;

use App\Traits\timeTraits;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Room;
use App\Models\Reservation;
use Auth;

class RoomController extends Controller
{

    use timeTraits;
    
    public function create_reservation_by_object(Request $request, Room $room, Reservation $reservationModel) {
        if($request->isMethod('post')) {
            $requestAll = $request->all();
 
            $reservationId = 0;

            if(!isset($requestAll['reservationId'])) {
                $userId = Auth::id();
                $reservationId = $reservationModel->createPreReservation($requestAll['objectId'], $userId, $requestAll['dateFrom'], $requestAll['dateTo']);
            }
            else {
                $reservation = Reservation::find($requestAll['reservationId']);
                $reservation->objectId = $requestAll['objectId'];
                $reservation->save();
                return ['reservationId' => $reservation->id];
            }
            

            return ['reservationId' => $reservationId];
        }
    }
    
    public function get_list_rooms_by_object(Request $request, Room $roomModel) {
        if($request->isMethod('post')) {
            $requestAll = $request->all();
            $objectId = $requestAll['objectId'];
            $dateFrom = $requestAll['dateFrom'];
            $dateTo = $requestAll['dateTo'];
            
            $listDaysInPeriod = $this->getDateTimeStartStopByDays($dateFrom, $dateTo);
            
            array_pop($listDaysInPeriod);
            foreach($listDaysInPeriod as $date => $datePeriod) {
                $listDaysInPeriod[$date] = strtotime($date);
            }
            
            $periodCount = count($listDaysInPeriod); 
            $listRooms = $roomModel->getListRoomsInReservationByObjectId($objectId, $listDaysInPeriod);
            
            foreach($listRooms as $key => $r) {
                
                $roomDate = explode(',', $r->listDate);
                if(count($roomDate) != $periodCount) {
                    unset($listRooms[$key]);
                }
            }
            
            return ['listRooms' => $listRooms];
            
        }
    }

    
    
}
