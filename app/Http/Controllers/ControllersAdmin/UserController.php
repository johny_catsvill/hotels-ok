<?php

namespace App\Http\Controllers\ControllersAdmin;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Clients;

class UserController extends Controller
{
    
    public function get_load_client_by_telefon(Request $request, Clients $clientsModel) {
        
        if($request->isMethod('post')) {
            $requestAll = $request->all();
            
            $clientInfo = $clientsModel->getTelefonByValue($requestAll['telefon']);
            if($clientInfo) {
                return response()->json($clientInfo);
            }
            
            
            
            
        }
        
    }

    public function users(Request $request) {


        $objects = User::whereNotNull('objectId')->with('object')->get();
        $profiles = User::whereNotNull('clientId')->get();
        return view('account.admin_layouts.users.index', compact('objects', 'profiles'));
    }
    
}
