<?php

namespace App\Http\Controllers\ControllersAdmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Wishe;

class WishesController extends Controller
{
    public function wishes() {


        $wishes = Wishe::all();
        return view('account.admin_layouts.wishes.index', compact('wishes'));
    }
}
