<?php

namespace App\Http\Controllers\ControllersAdmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Callback;

class CallbackController extends Controller
{
    
    public function list_callbacks(Request $request, Callback $callback) {
        $this->breadcrumbs += ['Список звонков' => route('list-callbacks')];
        
        $listCallbacks = $callback->getListCallbacks();
        
        return view('account.admin_layouts.callback.list-callbacks', [
            'breadcrumbs' => $this->breadcrumbs,
            'listCallbacks' => $listCallbacks
        ]);
        
    }

    public function edit_callback(Request $request, $id) {

        $callback = Callback::find($id);

        if($request->isMethod('post')) {
            $data = $request->all();
            $callback->status = $data['status'];
            $callback->save();
            return redirect()->route('list-callbacks')->with('message', 'Статус успешно изменен');
        }

        return view('account.admin_layouts.callback.edit-callback', compact('callback'));

    }
    
    public function delete_callback($id) {

        if($id) {
            $callback = Callback::find($id);
            $callback->deleted = 1;
            $callback->save();
            return redirect()->back()->with('message', 'Телефон успешно удален');
        }

        return redirect()->back()->withErrors(['Не существующий идентификатор']);

    }
    
    
}
