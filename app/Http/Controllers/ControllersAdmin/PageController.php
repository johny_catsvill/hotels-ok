<?php

namespace App\Http\Controllers\ControllersAdmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Page;


class PageController extends Controller
{
    public function page() {
        $pages = Page::all();

        return view('account.admin_layouts.pages.index', compact('pages'));
    }

    public function edit_page(Request $request, $id = false) {

        $page = false;

        if($request->isMethod('post')) {

            $data = $request->all();

            if($id) {
                $page = Page::find($id);
            }
            else {
                $page = new Page;
            }

            $page->title = $data['title'];
            $page->slug = $data['slug'];
            $page->name = $data['name'];
            $page->keywords = $data['keywords'];
            $page->description = $data['description'];

            $page->save();
            return redirect()->route('pages')->with('message', 'Страница успешно добавлена');
        }

        if($id) {
            $page = Page::find($id);
        }


        return view('account.admin_layouts.pages.edit-page', compact('page'));

    }

    public function delete_page($id) {
        if($id) {
            $page = Page::find($id);
            if($page) {
                $page->delete();
                return redirect()->back()->with('message', 'Страница успешно удалена');
            }

            return redirect()->back()->withErrors(['Не существует страницы с таким идентификатором']);
        }

        return redirect()->back()->withErrors(['Некорректное удаление']);
    }

}
