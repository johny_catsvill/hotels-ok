<?php

namespace App\Http\Controllers\ControllersAdmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Tickets;

class HelpController extends Controller
{
    public function index(Tickets $ticketsModel) {
        
        $this->breadcrumbs += ['Список тикетов' => route('help-admin')];
        
        $tickets = $ticketsModel->getListTicketsForAdmin();
        
        return view('account.admin_layouts.help.help', [
            'breadcrumbs' => $this->breadcrumbs,
            'tickets' => $tickets
        ]);
        
    }
    
    public function ticket(Request $request, Tickets $ticketsModel, $id) {
        
        if(!$id) return redirect()->back()->withErrors('Неккоректный тикет');
        $this->breadcrumbs += ['Тикет №'.$id => route('ticket-admin', $id)];
        
        if($request->isMethod('post')) {
            $message = $request->message;
            $ticketsModel->createMessageAdmin($message, $id, 10);
            return redirect()->route('ticket-admin', $id);
        }
        
        $ticket = $ticketsModel->getInfoByTicketIdAdmin($id);
        
        $listMessages = $ticketsModel->getListMessagesByTicketId($id);
        
        return view('account.admin_layouts.help.ticket', [
            'breadcrumbs' => $this->breadcrumbs,
            'ticket' => $ticket,
            'listMessages' => $listMessages
        ]);
        
    }
}
