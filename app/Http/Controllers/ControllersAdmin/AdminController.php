<?php

namespace App\Http\Controllers\ControllersAdmin;

use App\Models\Reservation;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use DB;
use App\Models\ObjectsTypesPluses;
use App\Models\BesideObject;
use File;
use Carbon\Carbon;


class AdminController extends Controller
{
    //
    
    public function list_services(ObjectsTypesPluses $objectServices) {
        
        $this->breadcrumbs += ['Список услуг обьектов' => route('list-service-object')];
        
        
//        $listServices = $objectServices->getListServicesObjects();
        $listServices = ObjectsTypesPluses::all();
        
        
        
        return view('account.admin_layouts.services.list-services', [
            'breadcrumbs' => $this->breadcrumbs,
            'listServices' => $listServices,
        ]);
        
    }
    
    public function edit_service(Request $request, $id = false) {
        
        
        $this->breadcrumbs += ['Добавление услуги' => route('edit-service')];
        
        $pluse = false;
        $arrayValidate = [];
        $requestAll = $request->all();
        
        if($request->isMethod('post')) {
            
            if($requestAll['service_id']) {
                $requestAll['service_id'] = intval($requestAll['service_id']);
                $arrayValidate = [
                'name_service' => 'required|string|min:2',
                'service_id' => 'required|int'
                ];
            }
            else {
                $arrayValidate = [
                    'name_service' => 'required|string|min:2|unique:objects_types_pluses,name',
                ];
            }
            
            $validator = Validator::make($requestAll, $arrayValidate);

            if($validator->fails()) {
                return redirect()->back()->withInput()->withErrors('Неправильно введенны данные услуги! Попробуйте еще раз!');
            }
            
            else {

                if($request->hasFile('image')) {
                    $file = $request->file('image');
                    $name = $file->getClientOriginalName();
                    $file->move(public_path().'/images/icon-pluses/',$file->getClientOriginalName());
                }
                
                if($requestAll['service_id']) {
                    $pluseModel = ObjectsTypesPluses::find($requestAll['service_id']);
                    $pluseModel->name = $requestAll['name_service']; 
                    $pluseModel->image = $name;
                    $pluseModel->save();
                    return redirect()->route('list-service-object')->with('message', 'Услуга успешно обновлена!');
                }
                else {
                    ObjectsTypesPluses::create([
                        'name' => $requestAll['name_service'],
                        'class' => $requestAll['nameClass_service'],
                        'image' => $name
                    ]); 
                }
            }
            
            
            
            
            return redirect()->route('list-service-object')->with('message', 'Услуга успешно добавлена!');
        }
        
        if($id) {
            $pluse = ObjectsTypesPluses::find($id);
        }
        
        
        
        
        return view('account.admin_layouts.services.edit-services', [
            'breadcrumbs' => $this->breadcrumbs,
            'pluse' => $pluse
        ]); 
        
    }
    
    public function delete_service($id) {
        $deleteRow = ObjectsTypesPluses::where('id', $id)->delete();
        if($deleteRow) {
            return redirect()->back()->with('message', 'Услуга успешно удалена!');
        }
    }
    
    public function list_objects_beside(Request $request, BesideObject $besideObject) {
        
        $this->breadcrumbs += ['Список ближайщих обьектов' => route('list-objects-beside')];
        
        $listBesideObjects = $besideObject->getListBesideObjects();
        
        return view('account.admin_layouts.beside-objects.list-beside-objects', [
            'breadcrumbs' => $this->breadcrumbs,
            'listBesideObjects' => $listBesideObjects
        ]);
        
    }
    
    public function edit_object_beside(Request $request, BesideObject $besideObject, $id = false) {
        
        $this->breadcrumbs += ['Добавить ближайщий обьект' => route('edit-object-beside')];
        $typeObject = false;
        
        if($request->isMethod('post')) {
            
            $requestAll = $request->all();
            if($id) {
                $typeObject = BesideObject::find($id);
                $typeObject->name = $requestAll['name_typeObject'];
                $typeObject->save();
                return redirect()->route('list-objects-beside')->with('message', 'Тип ближайщего обьекта успешно обновлен');
            }
            
            BesideObject::create([
                'name' => $requestAll['name_typeObject']
            ]);
            return redirect()->route('list-objects-beside')->with('message', 'Тип ближайщего обьекта успешно добавлен!');
        }
        
        if($id) {
            $typeObject = BesideObject::find($id);
        }
        
        return view('account.admin_layouts.beside-objects.edit-beside-object', [
            'breadcrumbs' => $this->breadcrumbs,
            'typeObject' => $typeObject,
        ]);  
    }
    
    public function delete_object_beside($id) {
        $deleteRow = BesideObject::where('id', $id)->delete();
        if($deleteRow) {
            return redirect()->back()->with('message', 'Тип ближащего обьекта успешно удален!');
        }
        
    }
    
    public function index(Request $request, Reservation $reservationModel) {

        $currentMonth = date('m');


        $m = [
            '01' => 'Январь',
            '02' => 'Февраль',
            '03' => 'Март',
            '04' => 'Апрель',
            '05' => 'Май',
            '06' => 'Июнь',
            '07' => 'Июль',
            '08' => 'Август',
            '09' => 'Сентябрь',
            '10' => 'Октябрь',
            '11' => 'Ноябрь',
            '12' => 'Декабрь'
        ];

        $startDate = strtotime('01.'.$currentMonth.'.'.date('Y'));
        $endDate = Carbon::now()->addMonth()->format('m');
        $endDate = strtotime('01.'.$endDate.'.'.date('Y'));

        $arrayReservation = [];

        $monthName = $m[$currentMonth];
        $countDay = date("t", strtotime("2018-".$currentMonth));

        for($i = 1; $i <= $countDay; $i++) {

            $dateString = strtotime($i.'.'.$currentMonth.'.'.date('Y'));
            $countReservations = Reservation::select(DB::raw('Count(id) as countObjects'))
                ->where('startReservationDate', $dateString)
                ->groupBy('startReservationDate')
                ->first();

            if(count($countReservations)) {
                array_push($arrayReservation, $countReservations->countObjects);
            }
            else {
                array_push($arrayReservation, 0);
            }


        }


        return view('account.admin_layouts.home', [
            'breadcrumbs' => $this->breadcrumbs,
            'countDay' => $countDay,
            'monthName' => $monthName,
            'currentMonth' => $currentMonth,
            'arrayReservation' => $arrayReservation
        ]);
        
        
    }
    
    
    
    
    
    
    
    
    
    
    
}
