<?php

namespace App\Http\Controllers\ControllersAdmin;

use App\Models\Bonus;
use App\Models\Promocode;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DiscountController extends Controller
{
    public function discounts() {
        $listBonuses = Bonus::all();
        return view('account.admin_layouts.bonuses.list-bonuses', compact('listBonuses'));
    }

    public function editDiscount(Request $request, $id = false) {
        $bonus = false;

        if($id) {
            $bonus = Bonus::find($id);
        }

        if($request->isMethod('post')) {
            $data = $request->all();
            if($data['discountId']) {
                $bonus = Bonus::find($data['discountId']);
                $bonus->name = $data['name'];
                $bonus->procent = $data['procent'];
                $bonus->save();
                return redirect()->route('discounts')->with('message', 'Бонус успешно обновлен!');
            }
            else {
                $bonus = new Bonus;
                $bonus->name = $data['name'];
                $bonus->procent = $data['procent'];
                $bonus->save();
                return redirect()->route('discounts')->with('message', 'Бонус успешно создан!');
            }


        }


        return view('account.admin_layouts.bonuses.edit', compact('bonus'));



    }

    public function deleteDiscount(Request $request, $id) {

        if($request->isMethod('post')) {
            $bonus = Bonus::find($id);
            $bonus->delete();
            return redirect()->back()->with('message', 'Бонус успешно удален');
        }

    }

    public function promocodes() {
        $listPromocodes = Promocode::all();
        return view('account.admin_layouts.promocodes.list-promocodes', compact('listPromocodes'));

    }

    public function editPromocode(Request $request, $id = false) {
        $promocode = false;
        if($id) {
            $promocode = Promocode::find($id);
        }

        if($request->isMethod('post')) {
            $data = $request->all();
            if($data['codeId']) {
                $promocode = Promocode::find($id);
                $promocode->code = $data['code'];
                $promocode->name = $data['name'];
                $promocode->published_at = $data['published_at'];
                $promocode->save();
                return redirect()->route('promocodes')->with('message', 'Промокод успешно обновлен');
            }
            else {
                $promocode = new Promocode();
                $promocode->code = $data['code'];
                $promocode->name = $data['name'];
                $promocode->published_at = $data['published_at'];
                $promocode->save();
                return redirect()->route('promocodes')->with('message', 'Промокод успешно создан');
            }


        }

        return view('account.admin_layouts.promocodes.edit', compact('promocode'));

    }

    public function deletePromocode(Request $request, $id) {
        if($request->isMethod('post')) {
            $promocode = Promocode::find($id);
            $promocode->delete();
            return redirect()->back()->with('message', 'Промокод успешно удален');
        }
    }
}
