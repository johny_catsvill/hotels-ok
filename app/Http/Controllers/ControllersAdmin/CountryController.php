<?php

namespace App\Http\Controllers\ControllersAdmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Country;
use Validator;

class CountryController extends Controller
{
    
    public function list_countries() {
        
        $this->breadcrumbs += ['Список стран системы' => route('list-countries')];
        
        $listCountries = Country::all();
        
        return view('account.admin_layouts.countries.list-countries', [
            'breadcrumbs' => $this->breadcrumbs,
            'listCountries' => $listCountries,
        ]);
        
    }
    
    public function edit_country(Request $request, $id = false) {
        
        $this->breadcrumbs += ['Добавление страны' => route('edit-country')];
        $country = false;
        
        if($request->isMethod('post')) {
            $requestAll = $request->all();
            
            $validator = Validator::make($requestAll, [
                'name' => 'required|string|min:2',
                'code' => 'required|string',
                'currencyCode' => 'required',
                'currencyName' => 'required|string',
                'currencyShortName' => 'required',
                'phoneCode' => 'required'
            ]);
            
            if($validator->fails()) {
                return redirect()->back()->withInput()->withErrors('Неккоректная информация! Попробуйте еще раз');
            }
            
            
            if($requestAll['countryId']) {
                $country = Country::find($requestAll['countryId']);
                if($country) {
                    $country->name = $requestAll['name'];
                    $country->code = $requestAll['code'];
                    $country->currencyCode = $requestAll['currencyCode'];
                    $country->currencyName = $requestAll['currencyName'];
                    $country->currencyShortName = $requestAll['currencyShortName'];
                    $country->phoneCode = $requestAll['phoneCode'];
                    $country->save();
                    return redirect()->route('list-countries')->with('message', 'Страна успешно обновлена!');
                }
                else {
                    return redirect()->back()->withInput()->withErrors('Неккоректная информация! Возможно при обновлении информации произошла ошибка. Попробуйте еще раз!');
                }
            }
            else {
                Country::create([
                    'name' => $requestAll['name'],
                    'code' => $requestAll['code'],
                    'currencyCode' => $requestAll['currencyCode'],
                    'currencyName' => $requestAll['currencyName'],
                    'currencyShortName' => $requestAll['currencyShortName'],
                    'phoneCode' => $requestAll['phoneCode']
                ]);
                return redirect()->route('list-countries')->with('message', 'Страна успешно добавлена!');
            }
        }
        
        
        if($id) {
            $country = Country::find($id);    
        }
        
        
        return view('account.admin_layouts.countries.edit-country', [
            'breadcrumbs' => $this->breadcrumbs,
            'country' => $country,
        ]);
        
    }
    
    public function delete_country($id = false) {
        if($id) {
            $country = Country::find($id);
            if($country) {
                $country->delete();
                return redirect()->back()->with('message', 'Страна успешно удалена из базы данных!');
            }
        }
    }
    
    
    
}
