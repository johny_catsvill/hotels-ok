<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Sunra\PhpSimple\HtmlDomParser;
use App\Models\ObjectHotel;
use App\Models\User;
use App\Models\ObjectPlusesRelation;
use App\Models\Room;
use App\Models\PlusesRoom;
use App\Models\EventCalendar;
use App\Helpers\Func;
use DB;

class ParserController extends Controller
{
    


//    public function parser(Room $room, EventCalendar $eventCalendar) {
//
//        $dateFrom = '23.11.2017';
//        $dateTo = '01.05.2018';
//        $countRoom = 1;
//        echo date('d.m.Y H:s', time());
//        $listObjects = DB::table('objects')->select('id', 'name')->where('id', '>' ,881)->get();
//        foreach($listObjects as $object) {
//            $listRooms = DB::table('rooms')->select('id', 'price')->where('objectId', $object->id)->get();
//            foreach($listRooms as $roomCurrent) {
//                $roomId = $roomCurrent->id;
//                $dateArray = $this->getDateTimeStartStopByDays($dateFrom, $dateTo);
//                array_pop($dateArray);
//                foreach($dateArray as $key => $date) {
////                    $countRoomReservation = $eventCalendar->getCountReservationRoomByDate($key, $roomId);
////                    $countFreeRoom = $countRoom - $countRoomReservation->countRoom;
//
////                    if($countFreeRoom && $countFreeRoom >= $countRoom) {
//                        DB::beginTransaction();
//                        try {
//                            $listRoomsByTypeRoom = $room->getListRoomsByTypeRoomId($roomId, $countRoom, $key);
////                            $eventCalendar->removeRowFromCalendar($key, $roomId); //тут доделать
//
//                            $result = $eventCalendar->createEventByRoom($roomId, $listRoomsByTypeRoom, $key, $roomCurrent->price, 0, 0, []);
//
//                        }
//                        catch(QueryException $e) {
//                            DB::rollBack();
//                            $errors = true;
//                        }
//
//                        DB::commit();
////                    }
//
//                }
//            }
//            echo $object->id.'<br>';
//        }
//        echo date('d.m.Y H:s', time());
//        echo 'Все получилось';
//
//
//    }
    
    
    
    
    
    
//    public function index() {
//        return view('parser');
//    }
//    
//    public function translit($s) {
//        $s = (string) $s;
//        $s = strip_tags($s);
//        $s = str_replace(array("\n", "\r"), " ", $s);
//        $s = preg_replace("/\s+/", ' ', $s);
//        $s = trim($s);
//        $s = function_exists('mb_strtolower') ? mb_strtolower($s) : strtolower($s);
//        $s = strtr($s, array('а'=>'a','б'=>'b','в'=>'v','г'=>'g','д'=>'d','е'=>'e','ё'=>'e','ж'=>'j','з'=>'z',
//            'и'=>'i','й'=>'y','к'=>'k','л'=>'l','м'=>'m','н'=>'n','о'=>'o','п'=>'p','р'=>'r','с'=>'s','т'=>'t',
//            'у'=>'u','ф'=>'f','х'=>'h','ц'=>'c','ч'=>'ch','ш'=>'sh','щ'=>'shch',
//            'ы'=>'y','э'=>'e','ю'=>'yu','я'=>'ya','ъ'=>'','ь'=>''));
//        $s = preg_replace("/[^0-9a-z-_ ]/i", "", $s);
//        $s = str_replace(" ", "-", $s);
//        return $s;
//    }
//
//    public function watermark_img($img, $type) {
//        $watermark = public_path().'/images/watermark.png';
//
//        $wh = getimagesize($watermark);
//        $fh = getimagesize($img);
//        $rwatermark = imagecreatefrompng($watermark);
//        if($type == 'jpeg' || $type == 'jpg') {
//            $rfile = imagecreatefromjpeg($img);
//        }
//        else {
//            $rfile = imagecreatefrompng($img);
//        }
//
//          imagecopy($rfile, $rwatermark, 10, $fh[1] - $wh[1] - 100, 0, 0, $wh[0], $wh[1]);
//          imagejpeg($rfile, $img, '100');
//          imagedestroy($rfile);
//        return true;
//    }
//
//    public function translateToSeconds($time) {
//        $time = str_replace('.', ':', $time);
//        $timePattern = explode(':', $time);
//        $seconds = (($timePattern[0] * 60) * 60) + ($timePattern[1] * 60);
//        return $seconds;
//    }
//
//    public function parser(Request $request) {
//
////        if(!$request->isMethod('post')) return false;
//
//        $roomModel = new Room;
//        $plusesRoomModel = new PlusesRoom;
//
//        $listCategoriesObject = DB::table('objects_categories')->select('name', 'id')->get();
//        $listServices = DB::table('objects_types_pluses')->select('name', 'id')->get();
//        $listPlusesRoom = DB::table('rooms_pluses')->select('name', 'id')->get();
//
////        $arrayLinks = $request->links;
//
//        $arrayLinks = [
//            ''
//                      ];
//
//        $arrayLinks = array_unique($arrayLinks);
//
//        $arrayObjectsName = [];
//        foreach($arrayLinks as $linkObject) {
//        $dom = HtmlDomParser::file_get_html($linkObject);
//            if(!$dom) continue;
//        $addressHotel = trim($dom->find('.hotel-line > .hotel-adress > span', 0)->text());
//        $city = explode(',', $addressHotel);
//        if(!$city[0]) {
//            continue;
//        }
//        $city = DB::table('cities')->select('id')->where('name', $city[0])->first();
//        if(!$city) continue;
////        dd($city);
//
//        $nameHotel = trim($dom->find('span.fn', 0)->text());
//
//        $stars = count($dom->find('.icon-star_new'));
//
//        $subdomain = $this->translit($nameHotel);
//        $categoryObjectId = 1;
//        foreach($listCategoriesObject as $categoryObject) {
//            $search = strpos($nameHotel, $categoryObject->name);
//            if($search === 0) {
//                $categoryObjectId = $categoryObject->id;
//                $nameHotel = trim(preg_replace('/'.$categoryObject->name.'/', '', $nameHotel));
//
//            }
//        }
//
//        $email = $subdomain.'-'. str_random(4) .'@meta.ua';
//        array_push($arrayObjectsName, $email.' - '.$nameHotel);
//
//        $object = ObjectHotel::create([
//            'organizationName' => $nameHotel,
//            'email' => $email,
//            'objectCategoryId' => $categoryObjectId,
//            'subdomain' => $subdomain,
//            'linkParser' => $linkObject
//        ]);
//
//        $user = User::create([
//            'email' => $email,
//            'phone' => '+38(123) 456-78-90',
//            'password' => bcrypt('12345678'),
//            'objectId' => $object->id,
//            'creationDate' => strtotime(date('d.m.Y')),
//            'activated' => 1
//        ]);
//
//        if(!file_exists(public_path().'/images/hotels/'.$object->id.'-'.$subdomain)) {
//            $path = public_path().'/images/hotels/'.$object->id.'-'.$subdomain;
//            mkdir($path, 0777);
//            mkdir($path.'/images-objects', 0777);
//            mkdir($path.'/images-rooms', 0777);
//        }
//
//        if($dom->find('.metro_gallery_block input', 0)) {
//            $galleryImages = $dom->find('.metro_gallery_block input', 0)->value;
//        }
//        else {
//            $galleryImages = [];
//        }
//
//        $mainImgObject = '';
//
//        if(count($galleryImages) != 0) {
//            $galleryImages = explode(',', $galleryImages);
//            foreach($galleryImages as $key => $img) {
//                if($img) {
//                    $nameImg = time().str_random(4).'.jpg';
//                    if($key != 0) {
//                        if(file_get_contents('https:'.$img)) {
//                            file_put_contents($path.'/images-objects/'.$nameImg, file_get_contents('https:'.$img));
//                            $galleryImages[$key] = $nameImg;
//    //                        $this->watermark_img($path.'/images-objects/'.$nameImg, 'jpg');
//                        }
//
//                    }
//                    else {
//                        $mainImgObject = $nameImg;
//                        file_put_contents($path.'/'.$nameImg, file_get_contents('https:'.$img));
//                    }
//
//                }
//                else {
//                    unset($galleryImages[$key]);
//                }
//
//            }
//        }
//
//
//
//        $description = trim($dom->find('#hotel-description-panel-content', 0)->text());
//            if($dom->find('.padT10 p strong', 0)) {
//                $countRooms = $dom->find('.padT10 p strong', 0)->text();
//                $removeCountRooms = $dom->find('.padT10 p', 0)->innertext = ' ';
//            }
//            else {
//                $countRooms = 1;
//            }
//
//
//
//
//        $services = [];
//        $servicesId = [];
//        $servicesHtml = explode(',',$dom->find('.hotel-service-content-line-right', 0)->text());
//        foreach($servicesHtml as $service) {
//            array_push($services, trim($service));
//        }
//
//        $listContentHotel = $dom->find('li #accor-living_in_hotel-content .hotel-service-content-line-right');
//        $str = $listContentHotel[0]->text();
//        $str = preg_replace("/[^0-9]/", '', $str);
//
//        $checkIn = substr($str,0, 2);
//        $checkOut = substr($str, -4,2);
//
//        $children = 'Нету информации о размещении детей или дополнительных местах.';
//
//        if(isset($listContentHotel[1])) {
//            $children = trim($listContentHotel[1]->text());
//        }
//
//        $restoraunt = '';
//
//
//        if($dom->find('#accor-restaurants_and_bars-content .hotel-service-content-line')) {
//            $restoraunt = trim($dom->find('#accor-restaurants_and_bars-content .hotel-service-content-line', 0)->text());
//        }
//
//        $location = 'Месторасположения отеля не указано.';
//
//        if($dom->find('#accor-BHotel_placement-content .hotel-service-content-line',0)) {
//            $location = trim($dom->find('#accor-BHotel_placement-content .hotel-service-content-line', 0)->text());
//        }
//
//            if($dom->find('.padB10 .dispIB', 0)) {
//                $longitude = trim(preg_replace("/N/", '', $dom->find('.padB10 .dispIB', 0)->text()));
//            }
//            else {
//                $longitude = '';
//            }
//
//            if($dom->find('.padB10 .dispIB', 1)) {
//                $latitude = trim(preg_replace("/E/", '', $dom->find('.padB10 .dispIB', 1)->text()));
//            }
//            else {
//                $latitude = '';
//            }
//
//
//
//
//        $infoAboutTravel = '';
//
//        if($dom->find('#accor-way_to_hotel-content .hotel-service-content-line', 0)) {
//            $infoAboutTravel = trim($dom->find('#accor-way_to_hotel-content .hotel-service-content-line', 0)->text());
//        }
//
//        $objectHotel = ObjectHotel::find($object->id);
//
//        $objectHotel->name = $nameHotel;
//        $objectHotel->url = $subdomain;
//        $objectHotel->address = $addressHotel;
//        $objectHotel->stars = $stars;
//        $objectHotel->countRooms = $countRooms;
//        $objectHotel->checkIn = $this->translateToSeconds($checkIn.':00');
//        $objectHotel->checkOut = $this->translateToSeconds($checkOut.':00');
//        $objectHotel->receptionRoundClock = 1;
//        $objectHotel->prePayment = 0;
//        $objectHotel->typePayment = 'payHotelsOk';
//        $objectHotel->touristTax = 'IncludedPrice';
//        $objectHotel->infoAboutObject = $description;
//        $objectHotel->locationObject = $location;
//        $objectHotel->infoAboutTravel = $infoAboutTravel;
//        $objectHotel->infoExtraPlaces = $children;
//        $objectHotel->restaurantAndBarsInfo = $restoraunt;
//        $objectHotel->longitudeGps = $longitude;
//        $objectHotel->latitudeGps = $latitude;
//        $objectHotel->image = $mainImgObject;
//        $objectHotel->cityId = $city->id;
//
//        $objectHotel->save();
//
//        foreach($listServices as $s) {
//            foreach($services as $key => $serviceHtml) {
//
//                if($s->name == $serviceHtml) {
//                    array_push($servicesId, ['objectId' => $object->id, 'objectTypePlusesId' =>$s->id]);
//                    unset($services[$key]);
//                }
//
//            }
//        }
//
//        ObjectPlusesRelation::insert($servicesId);
//
//        $roomsHtml = $dom->find('#tableScroll .room-table');
//        $rooms = [];
//        foreach($roomsHtml as $room) {
//            $plusesRoom = [];
//            $typeRoom = $room->find('tr', 0);
//            $typeRoom = $typeRoom->find('td', 1);
//            $typeRoom = $typeRoom->find('td[align=right]', 2);
//            $typeRoom = trim($typeRoom->text());
//            if(strpos($typeRoom, 'за место')) {
//                $typeRoom = 'priceAboutPlace';
//            }
//            else {
//                $typeRoom = 'priceAboutRoom';
//            }
//            $name = trim($room->find('caption', 0)->text());
//            $countPeople = $room->find('.people-container-tooltip', 0)->text();
//            $countPeople = preg_replace('/[^0-9]/', '', trim($countPeople));
//            $price = preg_replace('/[^0-9]/', '',trim($room->find('.price', 0)->text()));
//            $text = 'Хороший и крайне уютный номер для отдыха.';
//            if($room->find('.roomDescription', 0)) {
//                $text = preg_replace('/Описание номера: /', '', $room->find('.roomDescription', 0)->text());
//            }
//
//            $plusesHtml = $room->find('.widest-add-info table tr td');
//            $plusesRoomBd = [];
//            foreach($plusesHtml as $key => $pluse) {
//                if($key % 2) {
//                    $pluses = explode(',', $pluse->text());
//                    foreach($pluses as $p) {
//                        array_push($plusesRoom, trim($p));
//                    }
//
//                }
//            }
//
//            foreach($listPlusesRoom as $pluseRoomBd) {
//                foreach($plusesRoom as $key => $p) {
//                    if($pluseRoomBd->name == $p) {
//                        array_push($plusesRoomBd, $pluseRoomBd->id);
//                        unset($plusesRoom[$key]);
//                    }
//                }
//            }
//
//            $roomBD = Room::create([
//                'name' =>                       $name,
//                'countRoom' =>                  1,
//                'typePayment' =>                $typeRoom,
//                'price' =>                      $price,
//                'amountPerson' =>               $countPeople,
//                'infoAboutRoom' =>              $text,
//                'objectId' =>                   $object->id
//            ]);
//
//            $arrayRooms = [];
//
//            for($i = 0; $i < 1; $i++) {
//
//                $currentRoom = ['roomId' => $roomBD->id];
//                array_push($arrayRooms, $currentRoom);
//            }
//
//            $roomModel->createListRoomsByTypeRoom($arrayRooms);
//
//            $imagesRoom = $room->find('.widest-add-info .menu-horiz img');
//            $images = [];
//
//            if(!file_exists($path.'/images-rooms/'.$roomBD->id)) {
//                mkdir($path.'/images-rooms/'.$roomBD->id, 0777);
//            }
//
//            DB::table('list_rooms_type_room')->insert([
//                'roomId' => $roomBD->id
//            ]);
//
//            $plusesRoomModel->createRelationPlusesAndRoom($roomBD->id, $plusesRoomBd);
//
//            DB::table('bed_types_relation_room')->insert([
//                'roomId' => $roomBD->id,
//                'bedTypeId' => 1,
//                'countBed' => 1
//            ]);
//
//            $mainImgRoom = $room->find('.room-photo-container img', 0)->src;
//            $mainImgRoomName = time().str_random(4).'.jpg';
//            if($mainImgRoom != 'img/no-photo.gif') {
//                $mainImgSrc = str_replace('mx.jpg', 'z600.jpg', $mainImgRoom);
//                file_put_contents($path.'/images-rooms/'.$roomBD->id.'/'.$mainImgRoomName, file_get_contents('https:'.$mainImgSrc));
//                array_push($images, $mainImgRoomName);
//            }
//
//
//
//            if(is_array($imagesRoom)) {
//               foreach($imagesRoom as $img) {
//                   if(strpos($img->src, 'mx.jpg')) {
//                       $imgSrc = str_replace('mx.jpg', 'z600.jpg', $img->src);
//                       if(!file_get_contents('https:'.$imgSrc)) continue;
//                        $nameImg = time().str_random(4).'.jpg';
//                        file_put_contents($path.'/images-rooms/'.$roomBD->id.'/'.$nameImg, file_get_contents('https:'.$imgSrc));
//                        array_push($images, $nameImg);
//                   }
//
//                }
//            }
//
//
//
//
//
//
//        }
//
//        $dom->clear();
//        unset($dom);
//    }
//        echo '<pre>';
//        print_r($arrayObjectsName);
//        echo '</pre>';
//
//    }
}
