<?php

namespace App\Http\Controllers;

use App\Models\Question;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class QuizController extends Controller
{
    public function quiz(Request $request) {

        $questions = Question::with('answers')->get();
        return view('site.quiz', compact('questions'));
    }
}
