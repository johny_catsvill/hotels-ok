<?php

namespace App\Http\Controllers\ControllersUser;

use App\Models\Reservation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Models\User;
use App\Models\City;

class BonusController extends Controller
{
    public function index(Request $request) {

        $page = 'bonus';
        $user = User::find(Auth::id());
        $dateFrom = date('d.m.Y');
        $dateTo = date('d.m.Y', strtotime($dateFrom) + 86400);
        $listReservations = Reservation::whereNotNull('bonusId')->where('isPast', 1)->where('userId', $user->id)->get();

        if($request->ajax()) {
            $data = $request->all();
            $date = explode('-', $data['date']);
            $dateFrom = trim($date[0]);
            $dateTo = trim($date[1]);
            $listReservations = Reservation::whereNotNull('bonusId')->where('isPast', 1)->whereHas('object', function ($query) use ($data) {
                if($data['cityId']) {
                    $query->where('cityId', $data['cityId']);
                }

            })
                ->where('startReservationDate', '>=', strtotime($dateFrom))
                ->where('endReservationDate', '<', strtotime($dateTo))
                ->where('userId', $user->id)
                ->get();

            $returnHTML = view('user.blocks.table-bonus')->with([
                'listReservations' => $listReservations
            ])->render();

            return response()->json(array('html' => $returnHTML));
        }

        $cities = City::all();
        return view('user.bonus', compact('page', 'user', 'cities', 'listReservations', 'dateFrom', 'dateTo'));

    }
}
