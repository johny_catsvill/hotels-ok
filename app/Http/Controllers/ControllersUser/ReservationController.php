<?php

namespace App\Http\Controllers\ControllersUser;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Auth;
use App\Models\Reservation;

class ReservationController extends Controller
{
    public function reservations(Request $request) {

        if($request->isMethod('post')) {
            $data = $request->all();
            $year = strtotime('31.12.'.$data['year']);
            $type = $data['type'];
        }


        $page = 'reservation';
        $user = User::find(Auth::id());
        $reservations = Reservation::where('userId', $user->id);

        if(isset($year) && $year) {
            $reservations->where('startReservationDate', '<', $year);
        }

        if(isset($type) && $type) {

            switch($type) {
                case 'wait':
                    $type = 4;
                    break;
                case 'cancel':
                    $type = 2;
                    break;
                case 'active':
                    $type = 3;
                    break;
                default:
                    $type = '';
                    break;
            }

            if($type) {
                $reservations->where('statusId', $type);
            }

        }

        $reservations = $reservations->paginate(10);
        $showMore = $reservations->lastPage() !== 1;
        $totalCount = $reservations->total();



        if($request->ajax()) {
            $html = view('user.blocks.reservation', compact('reservations'))->render();
            return response()->json([
                'html' => $html,
                'showMore' => $reservations->lastPage(),
                'totalCount' => $totalCount
            ]);
        };

        return view('user.reservations', compact('page', 'reservations', 'showMore'));
    }

}
