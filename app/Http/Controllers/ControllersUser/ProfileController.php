<?php

namespace App\Http\Controllers\ControllersUser;

use App\Models\City;
use App\Models\SocialAccount;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Clients;
use Auth;
use Hash;
use Schema;

class ProfileController extends Controller
{

    public function profile() {
        $page = 'user';
        $cities = City::orderBy('name')->get();
        $client = Clients::find(Auth::user()->clientId);
        $socialAccountFacebook = SocialAccount::where('userId', Auth::id())->where('type', 'facebook')->first();
        $socialAccountGoogle = SocialAccount::where('userId', Auth::id())->where('type', 'google')->first();

        return view('user.profile', compact('page', 'cities', 'client', 'socialAccountFacebook', 'socialAccountGoogle'));
    }

    public function saveProfile(Request $request) {
        if($request->isMethod('post')) {
            $data = $request->all();
            $client = Clients::find(Auth::user()->clientId);
            unset($data['_token']);
            foreach($data as $key => $row) {
                if($row && (Schema::hasColumn('clients', $key) || Schema::hasColumn('users', $key))) {

                    if($key == 'password') {

                        if($data['oldPassword']) {

                            $oldPasswordUser = $client->user->password;
                            if(Hash::check($data['oldPassword'], $oldPasswordUser)) {
                                $user = User::find($client->user->id);
                                $user->password = bcrypt($row);
                                $user->save();
                            }
                            else {
                                return ['status' => 0, 'error' => 'Неверный старый пароль'];
                            }
                        }

                    }
                    else {
                        $client->{$key} = $row;
                    }

                }
            }

            $client->save();
            return ['status' => 1];
        }
    }

}
