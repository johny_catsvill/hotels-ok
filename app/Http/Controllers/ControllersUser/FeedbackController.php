<?php

namespace App\Http\Controllers\ControllersUser;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FeedbackController extends Controller
{
    public function feedback(Request $request) {
        $page = 'feedback';
        return view('user.feedback', compact('page'));
    }
}
