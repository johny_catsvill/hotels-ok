<?php

namespace App\Http\Controllers\ControllersUser;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ObjectController extends Controller
{
    public function offers(Request $request) {
        return view('user.offers');
    }
}
