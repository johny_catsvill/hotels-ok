<?php

namespace App\Http\Controllers\ControllersObject;

use App\Traits\croppedPhotos;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ObjectsTypesPluses;
use App\Models\BesideObject;
use App\Models\User;
use Auth;
use App\Models\ObjectHotel;
use App\Traits\timeTraits;
use App\Models\RelationBesideObject;
use DB;
use App\Models\City;
use Storage;
use App\Models\ObjectPlusesRelation;
use App\Helpers\Func;
use App\Models\Country;
use App\Models\Region;
use App\Models\ObjectsCategories;
use App\Models\FoodTypes;

class ObjectController extends Controller
{
    use timeTraits, croppedPhotos;
    
    public function edit(Request $request, ObjectsTypesPluses $objectPlusesModel, ObjectHotel $object, BesideObject $besideObjectModel, FoodTypes $foodTypesModel) {

        $this->breadcrumbs += ['Информация об объекте' => route('information-object', $this->getDomainObject())];

        $listTypesObjects = BesideObject::all()->where('deleted', 0);
        $userId = Auth::id();
        $objectId = User::find($userId)->objectId;
        $objectHotel = $object->find($objectId);
        $listPluses = ObjectsTypesPluses::all();
        $listObjectsBeside = $besideObjectModel->getListBesideByObjectId($objectId);
        $listPlusesObject = $objectHotel->objectPluses;
        $objectUrl = $objectHotel->subdomain;
        $pathImages = '/images/hotels/' . $objectId. '-'. $objectUrl.'/images-objects/';
        $listImagesObject = Storage::files($pathImages);
        foreach($listImagesObject as $key => $imgObject) {
            if(strpos($imgObject, 'category') === false) {
                unset($listImagesObject[$key]);
            }
        }


        $listFoodTypesObject = $foodTypesModel->getListFoodTypesObject($objectId);


        $objectHotel->checkIn = $this->translateToHours($objectHotel->checkIn);
        $objectHotel->checkOut = $this->translateToHours($objectHotel->checkOut);

        $listFoodTypes = $foodTypesModel->getListFoodTypes();

        foreach($listPluses as $pluse) {
            if(!$listPlusesObject) break;
            foreach($listPlusesObject as $pluseObject) {
                if($pluse->id == $pluseObject->id) {
                    $pluse->active = 'on';
                }
            }
        }
        
        foreach($listFoodTypes as $foodType) {
            if(!$listFoodTypesObject) break;
            foreach($listFoodTypesObject as $foodTypeObject) {
                if($foodType->id == $foodTypeObject->foodTypeId) {
                    $foodType->active = 'on';
                }
            }
        }
        
        $listStars = [
            '0' => 'Без звезд',
            '1' => 'Одна звезда',
            '2' => 'Две звезды',
            '3' => 'Три звезды',
            '4' => 'Четыре звезды',
            '5' => 'Пять звезд'
        ];
        
        
        if($request->isMethod('post')) {
            
            $requestAll = $request->all();
            $arrayObjectBeside = [];
            $arrayObjectPluses = [];
            if(!file_exists(public_path().$pathImages)) {
                //потом протестировать на еще одном обьекте
                mkdir($pathImages, 0700);
                mkdir($pathImages.'/images-objects', 0777);
                mkdir($pathImages.'/images-rooms', 0777);
            }
            
            if($requestAll['pluses']) {
                foreach($requestAll['pluses'] as $pluse) {
                    array_push($arrayObjectPluses, [
                        'objectId' => $objectId,
                        'objectTypePlusesId' => $pluse
                    ]);    
                }
                
            }
            $arrayFoodTypes = [];
            if($requestAll['foodTypes']) {
                foreach($requestAll['foodTypes'] as $foodType) {
                    array_push($arrayFoodTypes, [
                        'objectId' => $objectId,
                        'foodTypeId' => $foodType
                    ]);
                }
            }
            
            
            
            for($i = 1; $i < count($requestAll['typeObject']); $i++) {
                array_push($arrayObjectBeside, [
                    'name' => $requestAll['nameTypeObjectBeside'][$i], //доделать здесь, придумать правильный способ реализации добавления сервисов
                    'objectId' => $objectId,
                    'typeBesideId' => $requestAll['typeObject'][$i],
                    'distance' => $requestAll['distanceBesideObject'][$i]
                ]);
            }
            
            
            DB::beginTransaction();
            try {
                
                $foodTypesModel->removeFoodTypesObject($objectId);
                $relationBesideObjects = RelationBesideObject::where('objectId', $objectId)->delete();
                ObjectPlusesRelation::where('objectId', $objectId)->delete();    

//                $objectHotel->name = $requestAll['name'];
                $objectHotel->address = $requestAll['address'];
                $objectHotel->stars = $requestAll['stars'];
                $objectHotel->countRooms = $requestAll['countRooms'];
                $objectHotel->checkIn = $this->translateToSeconds($requestAll['checkIn']);
                $objectHotel->checkOut = $this->translateToSeconds($requestAll['checkOut']);
                $objectHotel->receptionRoundClock = $requestAll['receptionRoundClock'];
                $objectHotel->prePayment = $requestAll['prePayment'];
                $objectHotel->typePayment = $requestAll['typePayment'];
                $objectHotel->touristTax = $requestAll['touristTax'];
                $objectHotel->touristTaxValue = $requestAll['touristTaxValue'];
                $objectHotel->infoAboutObject = $requestAll['infoAboutObject'];
                $objectHotel->locationObject = $requestAll['locationObject'];
                $objectHotel->infoAboutTravel = $requestAll['infoAboutTravel'];
                $objectHotel->infoExtraPlaces = $requestAll['infoExtraPlaces'];
                $objectHotel->infoAdditionalPayments = $requestAll['infoAdditionalPayments'];
                $objectHotel->restaurantAndBarsInfo = $requestAll['restaurantAndBarsInfo'];
                $objectHotel->infoMedication = $requestAll['infoMedication'];
                $objectHotel->longitudeGps = $requestAll['longitudeGps'];
                $objectHotel->latitudeGps = $requestAll['latitudeGps'];
                $objectHotel->countryId = 1; // это сделано лишь для записи идентификатора страны, так как нигде мы его сейчас не берем, потом убрать и сделать нормально. Обязательно!
                if($request->hasFile('main_img')) {
                    $mainImg = $request->file('main_img');
                    if($objectHotel->image) {
                        $imgName = explode('.', $objectHotel->image);
                        $type = $imgName[1];
                        foreach ($this->sizesImages as $key => $size) {
                            Storage::delete('/images/hotels/'.$objectId.'-'.$objectHotel->subdomain.'/'.$imgName[0].'_'.$key.'.'.$type);
                        }

                    }

                    $mainImg->move(public_path().'/images/hotels/'.$objectId.'-'.$objectHotel->subdomain.'/', 'main.'. $mainImg->getClientOriginalExtension());

                    foreach ($this->sizesImages as $key => $imgCrop) {
                        $this->cropPhoto('/images/hotels/'.$objectId.'-'.$objectHotel->subdomain.'/', 'main.'.$mainImg->getClientOriginalExtension(), 'main', $mainImg->getClientOriginalExtension());
                    }

                    $objectHotel->image = 'main.'. $mainImg->getClientOriginalExtension();
                }
                
                $objectHotel->save();

                RelationBesideObject::insert($arrayObjectBeside);
                ObjectPlusesRelation::insert($arrayObjectPluses);
                $foodTypesModel->insertFoodType($arrayFoodTypes);
                
            }
            catch(QueryException $e) {
                DB::rollBack();
            }
            
            DB::commit();
            
            return redirect()->route('information-object', $this->getDomainObject())->with('message', 'Информация успешно обновлена!');
        }
        
        if(old()) {

            $objectHotel = (object)old();
            
            foreach($listPluses as $pluse) {
                if(!isset($objectHotel->pluses)) break;
                foreach($objectHotel->pluses as $pluseObject) {
                    if($pluse->id == $pluseObject) {
                        $pluse->active = 'on';
                    }
                }
            }
            
            foreach($listFoodTypes as $foodType) {
                if(!isset($objectHotel->foodTypes)) break;
                foreach($objectHotel->foodTypes as $foodTypeObject) {
                    if($foodType->id == $foodTypeObject) {
                        $foodType->active = 'on';
                    }
                }
            }
            
            
            $listObjectsBeside = [];
            for($i = 1; $i < count(old('typeObject')); $i++) {
                array_push($listObjectsBeside, [
                    'name' => old('nameTypeObjectBeside')[$i], //доделать здесь, придумать правильный способ реализации добавления сервисов
                    'objectId' => $objectId,
                    'typeBesideId' => old('typeObject')[$i],
                    'distance' => old('distanceBesideObject')[$i]
                ]);
            }
            
            for($i = 0; $i < count($listObjectsBeside); $i++) {
                $listObjectsBeside[$i] = (object)$listObjectsBeside[$i];  
            }
            

            
            $objectHotel->image = old('old_img');
            $objectHotel->id = $objectId;
            
            //реализовать момент с старыми данными
        }
    
        
        return view('account.info_hotel', [
            'breadcrumbs' => $this->breadcrumbs,
            'objectHotel' => $objectHotel,
            'listPluses' => $listPluses,
            'listPlusesObject' => $listPlusesObject,
            'listObjectsBeside' => $listObjectsBeside,
            'listStars' => $listStars,
            'listImagesObject' => $listImagesObject,
            'listTypesObjects' => $listTypesObjects,
            'subdomain' => $this->getDomainObject(),
            'isConfirmed' => $this->confirmedAccount(),
            'listFoodTypes' => $listFoodTypes
        ]);
        
    }
    
    public function general_information(Request $request, ObjectHotel $objectHotelModel, ObjectsCategories $objectCategoriesModel, Func $funcHelper, Country $countyModel, City $cityModel) {
        
        $this->breadcrumbs += ['Общие сведения' => route('general-information', $this->getDomainObject())];
        $user = Auth::user();
        $objectId = $user->objectId;
        $listCountries = Country::all();  
        $listRegions = Region::all();
        $listCategoriesObject = $objectCategoriesModel->getListCategoriesObject();
        $listCities = false;
        
        if($request->isMethod('post')) {
            $requestAll = $request->all();

            $object = ObjectHotel::find($objectId);
            if($object) {
                DB::beginTransaction();
                try {
                    $object->name = $requestAll['name'];
                    $object->url = $funcHelper->translit($requestAll['name']);
                    $object->cityId = $requestAll['cityId'];
                    $object->indexMail = $requestAll['indexMail'];
                    $object->street = $requestAll['street'];
                    $object->numberHouse = $requestAll['numberHouse'];
                    $object->numberApartment = $requestAll['numberApartment'];
                    $object->phone = $requestAll['phone'];
                    $object->fax = $requestAll['fax'];
                    $object->contactName = $requestAll['contactName'];
                    $object->email = $requestAll['email'];
                    $object->save();
                    
                    $generalInfoObject = DB::table('general_infromation_objects')->where('objectId', $objectId)->first();
                    if($generalInfoObject) {
                        $objectHotelModel->updateOrInsertRowInfoByObjectId($requestAll, $objectId, 'update');
                    }
                    else {
                        $objectHotelModel->updateOrInsertRowInfoByObjectId($requestAll, $objectId, 'insert');
                    }
                    
                }
                catch(QueryException $e) {
                    DB::rollback();
                }
                
                DB::commit();
                return redirect()->back()->with('message', 'Общие сведения об объекте успешно сохранены!');
            }
            else {
                return redirect()->back()->withInput()->withErrors('У вас нет прав для обновления информации об этом объекте');
            }
        }
        
        if(old()) {
            $object = (object)old();
            $listCities = City::all()->where('regionId', $object->regionId);
        }
        else {
            $object = $objectHotelModel->getInfoByObjectId($objectId); 
            $objectGeneralInfo = DB::table('general_infromation_objects')->where('objectId', $objectId)->first();
            if(count($objectGeneralInfo) > 0) {
                foreach($objectGeneralInfo as $key => $objectInfo) {
                    $object->$key = $objectInfo;
                }   
            }
            if(isset($object->cityId) && $object->cityId) {
                $regionId = City::find($object->cityId)->regionId;
                $listCities = City::all()->where('regionId', $regionId);
            }
        }
          
        
        return view('account.general-information', [
            'breadcrumbs' => $this->breadcrumbs,
            'object' => $object,
            'listCountries' => $listCountries,
            'listRegions' => $listRegions,
            'listCities' => $listCities,
            'listCategoriesObject' => $listCategoriesObject,
            'subdomain' => $this->getDomainObject(),
            'isConfirmed' => $this->confirmedAccount()
        ]);
        
    }
    
    
    public function confirmed_account(ObjectHotel $objectHotelModel) {
        $user = Auth::user();
        if($user->objectId) {
            $objectId = $user->objectId;
            $confirmed = DB::table('confirmed_object')->where('objectId', $objectId)->first();
            if($confirmed) {

                DB::table('confirmed_object')->where('objectId', $objectId)->update([
                    'confirmedAccount' => 2
                ]);
            }
            else {
                $objectHotelModel->queryForChangeStatusConfirmedAccount($objectId);
            }

            return redirect()->back()->with('message', 'Заявка на проверку успешно отправлена. В течении нескольких часов Вы получите ответ!');
        }
        
        return abort(404);
    }
    
    
    
    
    
    
}
