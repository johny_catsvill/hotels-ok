<?php

namespace App\Http\Controllers\ControllersObject;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Room;
use App\Models\PlusesRoom;
use Storage;
use Auth;
use App\Models\ObjectHotel;
use DB;
use Validator;
use App\Helpers\Validate;
use App\Models\EventCalendar;

class RoomController extends Controller
{
    public function list_rooms(Room $roomModel) {
        
        $this->breadcrumbs += ['Список комнат' => route('rooms', $this->getDomainObject())];
        
        
        $user = Auth::user();
        $object = ObjectHotel::find($user->objectId);
        $pathImages = '/images/hotels/' . $object->id. '-'. $object->subdomain.'/images-rooms/';
        $this->deleteImgNoRelationRoom($pathImages);
        
        $listRoomsObject = $roomModel::all()->where('objectId', $object->id);
//        dd($listRoomsObject);
        foreach($listRoomsObject as $room) {
            if(Storage::files($pathImages.$room->id)) {
                $room->img = Storage::files($pathImages.$room->id)[0];    
            }
            else {
                $room->img = '';
            }
            
            
        }
        
        return view('account.list_rooms', [
            'breadcrumbs' => $this->breadcrumbs,
            'listRoomsObject' => $listRoomsObject,
            'subdomain' => $this->getDomainObject(),
            'isConfirmed' => $this->confirmedAccount()
        ]);
        
        
    }
    
    
    public function edit(Request $request, Room $roomModel, PlusesRoom $plusesRoom, $domain, $id = false) {
        
        $this->breadcrumbs += ['Добавление комнаты' => route('edit-room', $this->getDomainObject())];
        $error = false;
        $room = false;
        $listImagesObject = false;
        $listBedsThatRoom = false;
        $listPlusesThatRoom = false;
        $arrayCategoriesAndPlusesRoom = [];
        $user = Auth::user();
        $object = ObjectHotel::find($user->objectId);
        $caption = 'Добавление номера';
        
        
        $pathImages = '/images/hotels/' . $object->id. '-'. $object->subdomain.'/images-rooms/';
        
        $listCategoriesPlusesRoom = $plusesRoom->getListCategoriesPlusesForRoom();
        $listPlusesForRoom = $plusesRoom->getListPlusesForRoom();
        
        $listBedsForRoom = $roomModel->getListBedsForRoom();
        
        
        foreach($listCategoriesPlusesRoom as $category) {
            $category->pluses = [];
            foreach($listPlusesForRoom as $pluse) {
                if($category->id == $pluse->roomCategoryId) {
                    array_push($category->pluses, $pluse);
                    unset($pluse);
                }
            }
        }
        
        if($request->isMethod('post')) {
            $requestAll = $request->all();
            if($requestAll['roomId']) {
                $room = Room::where('id', $requestAll['roomId'])->where('objectId', $user->objectId)->get()->first();
                if($room) {
                    DB::beginTransaction();
                    
                    try {
                        $room->name =                                   $requestAll['name'];
                        $room->countRoom =                              $requestAll['countRoom'];
                        $room->typePayment =                            $requestAll['typePayment'];
                        $room->price =                                  $requestAll['price'];
                        $room->amountPerson =                           $requestAll['amountPerson'];
                        $room->infoAboutRoom =                          $requestAll['infoAboutRoom'];
                        $room->morePlace =                          $requestAll['morePlace'];
                        $room->save();
                        
                        //удаление связей плюсов обновляемой комнаты
                        
                        $plusesRoom->deletePluseRelationThatRoom($room->id);
                        
                        //создание связей плюсов обновляемой комнаты
                        
                        $plusesRoom->createRelationPlusesAndRoom($room->id, $requestAll['pluse']);
                        
                        //удаление связей кроватей обновляемой комнаты
                        
                        $roomModel->deleteRelationBedTypeAndRoom($room->id);
                        
                        //создание связей кроватей обновляемой комнаты
                        
                        $roomModel->createRelationBedTypeAndRoom($room->id, $requestAll['bedTypeId'], $requestAll['countBed']);
                        
                        $pathImagesRoom = $pathImages.$room->id;
                        
                    }
                    catch(QueryException $e) {
                        DB::rollBack();
                        $error = true;
                    }

                    DB::commit();
                    
                    if(!$error) {
                        $galleryImgRoom = Storage::files($pathImages);

                        foreach($galleryImgRoom as $img) {
                            $imgName = str_replace($pathImages, "", '/'.$img);
                            rename(public_path().'/'.$img, public_path().$pathImagesRoom.'/'.$imgName);
                        }
                    }

                    return redirect()->route('rooms', $this->getDomainObject())->with('message', 'Информация о комнате, была успешно обновлена!');
                    
                }
                else {
                    return redirect()->back()->withInput()->withErrors('Ошибка редактирования комнаты! Попробуйте еще раз.');
                }
                
            }
            else {
    
                DB::beginTransaction();
                try {

                    $room = Room::create([
                        'name' =>                       $requestAll['name'],
                        'countRoom' =>                  $requestAll['countRoom'],
                        'typePayment' =>                $requestAll['typePayment'],
                        'price' =>                      $requestAll['price'],
                        'amountPerson' =>               $requestAll['amountPerson'],
                        'infoAboutRoom' =>              $requestAll['infoAboutRoom'],
                        'objectId' =>                   $object->id,
                        'morePlace' =>                   $requestAll['morePlace']
    ]);

                    $roomId = $room->id;
                    
                    $arrayRooms = [];
                    
                    for($i = 0; $i < $requestAll['countRoom']; $i++) {
                        
                        $currentRoom = ['roomId' => $roomId];
                        array_push($arrayRooms, $currentRoom);
                    }
                    
                    $roomModel->createListRoomsByTypeRoom($arrayRooms);
                    
                    
                    $plusesRoom->createRelationPlusesAndRoom($roomId, $requestAll['pluse']);
                    $roomModel->createRelationBedTypeAndRoom($roomId, $requestAll['bedTypeId'], $requestAll['countBed']);

                    $pathImagesRoom = $pathImages.$roomId;

}

                catch(QueryException $e) {
                    DB::rollBack();
                    $error = true;
                }

                DB::commit();

                if(!$error) {

                    if(!file_exists($pathImagesRoom)) {
                        mkdir(public_path().$pathImagesRoom, 0777);
                    }

                    $galleryImgRoom = Storage::files($pathImages);

                    foreach($galleryImgRoom as $img) {
                        $imgName = str_replace($pathImages, "", '/'.$img);
                        rename(public_path().'/'.$img, public_path().$pathImagesRoom.'/'.$imgName);
                    }

}

                return redirect()->route('rooms', $this->getDomainObject())->with('message', 'Комната успешно добавлена');
            }
            
            
            
        }
        
        
        
        if($id) {
            
            $room = Room::find($id);
            $user = Auth::user();
            $listBedsThatRoom = $roomModel->getListBedsByRoomId($room->id);
            $listPlusesThatRoom = $plusesRoom->getListPlusesByRoomId($room->id);

            foreach($listCategoriesPlusesRoom as $category) {
                foreach($category->pluses as $pluse) {
                    foreach($listPlusesThatRoom as $pluseThatRoom) {
                        if($pluse->id == $pluseThatRoom->roomPluseId) {
                            $pluse->active = 'active';
                            unset($pluseThatRoom);
                        }
                    }
                }
            }
                
                
            if($room && $room->objectId == $user->objectId) {
                $listImagesObject = Storage::files($pathImages.'/'.$id);

                foreach($listImagesObject as $key => $imgObject) {
                    if(strpos($imgObject, 'norm') === false) {
                        unset($listImagesObject[$key]);
                    }
                }

                $caption = 'Редактирование номера';
            }
            else {
                return redirect()->route('rooms', $this->getDomainObject())->withErrors('Комната с таким идентификатором отсутсвует!');
            }
             
        }
        
        
        if(old()) {
        
            $room = (object)old();

            $room->id = $room->roomId;
            if(isset($room->pluse)) {
                $room->pluse = (object)$room->pluse;
            }
            else {
                $room->pluse = false;
            }
            
            
            foreach($listCategoriesPlusesRoom as $category) {
                foreach($category->pluses as $pluse) {
                    if($room->pluse) {
                        foreach($room->pluse as $pluseThatRoom) {
                            if($pluse->id == $pluseThatRoom) {
                                $pluse->active = 'active';
                                unset($pluseThatRoom);
                            }
                        }
                    }
                }
            }
            
            $listBedsThatRoom = [];
            if(!isset($room->bedTypeId)) {
                $room->bedTypeId = false;
            } 
            if($room->bedTypeId) {
                for($i = 0; $i < count($room->bedTypeId); $i++) {
                    $arrBed = [];
                    $arrBed['bedTypeId'] = $room->bedTypeId[$i];
                    $arrBed['countBed'] = $room->countBed[$i];
                    $arrBed = (object)$arrBed;
                    array_push($listBedsThatRoom, $arrBed);
                }
            }
            
        }
        
        if(!$id && !old()) {
            $this->deleteImgNoRelationRoom($pathImages);
        }
        


        return view('account.add_room', [
            'breadcrumbs' => $this->breadcrumbs,
            'room' => $room,
            'caption' => $caption,
            'listBedsForRoom' => $listBedsForRoom,
            'listImagesObject' => $listImagesObject,
            'listCategoriesPlusesRoom' => $listCategoriesPlusesRoom,
            'listBedsThatRoom' => $listBedsThatRoom,
            'listPlusesThatRoom' => $listPlusesThatRoom,
            'subdomain' => $this->getDomainObject(),
            'isConfirmed' => $this->confirmedAccount()
        ]);
        
    }
    
    
    public function delete_room(ObjectHotel $objectHotelModel, Room $roomModel, PlusesRoom $plusesRoomModel, $domain, $id) {
        //здесь доделать и протестить (использовать валидатор данных)
        $error = false;
        
        if($id) {
            $user = Auth::user();
            $object = ObjectHotel::find($user->objectId);
            $pathImages = '/images/hotels/' . $object->id. '-'. $object->subdomain.'/images-rooms/';
            $room = $roomModel::find($id);
            if($room->objectId == $user->objectId) {
                DB::beginTransaction();
                try {
                    $resultBedRelation = $roomModel->deleteBedRelationThatRoom($room->id); //protestit
                    $resultPluseRelation = $plusesRoomModel->deletePluseRelationThatRoom($room->id); //protestit
//                    $roomModel->deleteRoom($room->id, $object->id);
                    Room::where('id', $room->id)->where('objectId', $user->objectId)->delete();   
                    
                }
                catch(QueryException $e) {
                    DB::rollBack();
                    $error = true;
                }

                DB::commit();
                
                if(!$error) {
                    Storage::deleteDirectory($pathImages.$room->id);
                }
            
                return redirect()->back()->with('message','Комната была успешно удалена!');
            }
            else {
                return redirect()->route()->withErrors('Ошибка удаление комнаты текущего объекта! Попробуйте позже или напишите администратору.');
            }
        }
        else {
            return redirect()->route('rooms')->withErrors('Ошибка удаление комнаты. Попробуйте позже или напишите администратору.');
        }
    }
    
    public function activation_sale_room(Room $roomModel, EventCalendar $eventCalendarModel, $domain, $id) {
        
        if($id) {
            
            $user = Auth::user();
            $objectId = $user->objectId;
            
            if(!$objectId || !Validate::id($id)) return abort(404);
            
            $room = Room::where('id', $id)->where('objectId', $objectId)->get()->first();
            if($room) {
                if($room->activeSale == 1) {
                    $room->activeSale = 0;
                    $room->save();
                    $eventCalendarModel->updateSaleByRoomId($id, 1);
                    return redirect()->route('rooms', $this->getDomainObject())->with('message', 'Комната успешно снята с продажи!');
                }
                else {
                    $room->activeSale = 1;
                    $room->save();
                    $eventCalendarModel->updateSaleByRoomId($id, 0);
                    return redirect()->route('rooms', $this->getDomainObject())->with('message', 'Комната успешно установлена на продажу!');
                }
            }
            else {
                return redirect()->route('rooms', $this->getDomainObject())->withErrors('Комнаты с таким идентификатором не существует!');
            }
            
        }
        return redirect()->route('route', $this->getDomainObject())->withErrors('Неккоректный идентификатор комнаты');
        
    }
    
    public function deleteImgNoRelationRoom($pathImagesRoom) {
        $files = Storage::files($pathImagesRoom);
        foreach($files as $file) {
            Storage::delete($file);
        }
        
    }
    
    
    
}
