<?php

namespace App\Http\Controllers\ControllersObject;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Models\Tickets;
use DB;

class HelpController extends Controller
{
    
    public function index(Request $request, Tickets $ticketsModel) {
        $this->breadcrumbs += ['Помощь и поддержка' => route('help', $this->getDomainObject())];
        $objectId = Auth::user()->objectId;
        $errors = false;
        
        
        if($request->isMethod('post')) {
            $requestAll = $request->all();
            $theme = $requestAll['themeTicket'];
            $message = $requestAll['message'];
            
            DB::beginTransaction();
                try {
                    
                    $ticketId = $ticketsModel->createTicket($theme, $objectId);
                    $ticketsModel->createMessage($message, $ticketId, $objectId);

                }
                catch(QueryException $e) {
                    DB::rollBack();
                    $errors = true;
                }

                DB::commit();

            if(!$errors) {
                return redirect()->route('ticket', ['domain' => $this->getDomainObject(), 'id' => $ticketId]);
            }
            return redirect()->back()->withErrors('Ошибка создания тикета, попробуйте еще раз позже!');
            
        }
        
        
        $tickets = $ticketsModel->getListTickets($objectId);
        
        
        return view('account.helps', [
            'breadcrumbs' => $this->breadcrumbs,
            'subdomain' => $this->getDomainObject(),
            'isConfirmed' => $this->confirmedAccount(),
            'tickets' => $tickets
        ]);
        
        
    }
    
    public function ticket(Request $request, Tickets $ticketsModel, $domain, $id) {
        
        if(!$id) return redirect()->back()->withErrors('Неккоректный тикет');
        
        $this->breadcrumbs += ['Тикет №'. $id => route('ticket', ['domain' => $this->getDomainObject(), 'id' => $id])];
        $objectId = Auth::user()->objectId;
        
        if($request->isMethod('post')) {
            $message = $request->message;
            $ticketsModel->createMessage($message, $id, $objectId);
            return redirect()->route('ticket', ['domain' => $this->getDomainObject(), 'id' => $id]);
        }
        
        $ticket = $ticketsModel->getInfoByTicketId($id, $objectId);
        
        $listMessages = $ticketsModel->getListMessagesByTicketId($id);
        
        $ticketsModel->updateMessageAttrRead($id, $objectId);
        
        
        return view('account.ticket', [
            'breadcrumbs' => $this->breadcrumbs,
            'subdomain' => $this->getDomainObject(),
            'isConfirmed' => $this->confirmedAccount(),
            'ticket' => $ticket,
            'listMessages' => $listMessages
        ]);
        
    }
    
    public function delete_ticket(Request $request, Tickets $ticketsModel, $domain, $id) {
        
            if($id) {
                $objectId = Auth::user()->objectId;
                $ticket = $ticketsModel->getInfoByTicketId($id, $objectId);
                if($ticket) {
                    $ticketsModel->deleteTicket($ticket->id);
                    return redirect()->back();
                }
                return redirect()->back();
            }
            return redirect()->back();
        
    }
    
    
    
}
