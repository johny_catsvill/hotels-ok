<?php

namespace App\Http\Controllers\ControllersObject;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReferenceController extends Controller
{
    public function index() {
        
        $this->breadcrumbs += ['Справка' => route('reference', $this->getDomainObject())];
        return view('account.reference', [
            'breacrumbs' => $this->breadcrumbs,
            'subdomain' => $this->getDomainObject(),
            'isConfirmed' => $this->confirmedAccount()
        ]);
    }
}
