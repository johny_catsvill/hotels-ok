<?php

namespace App\Http\Controllers\ControllersObject;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Review;
use Auth;
use App\Helpers\Func;
use App\Helpers\Paginate;
use App\Models\ObjectHotel;


class ReviewController extends Controller
{
    
    const countElementsOnPage = 7;
    
    public function list_reviews(Request $request, Review $reviewModel, Func $funcModel, ObjectHotel $objectHotel) {
        
        return view('stub-admin');

        $this->breadcrumbs += ['Отзывы по комнатам' => route('reviews', $this->getDomainObject())];
        $objectId = Auth::user()->objectId;
        $page = 1;
        $requestAll = $request->all();
        if(isset($requestAll['page'])) {
            $page = intval($requestAll['page']);
        }
        
        $reviewsSort = 'all';
        
        if(isset($requestAll['sort'])) {
            $reviewsSort = $requestAll['sort'];
        }
        
        
        $countReviewsByObject = count(Review::all()->where('objectId', $objectId));
        $averageRaiting = $objectHotel->getInfoObjectById($objectId);
        
        $averageRaiting = $averageRaiting->raitingValue;
        
        $totalValueByCategoryForObject = $objectHotel->getTotalValueByCategoryForObject($objectId);
        
        $listReviews = $reviewModel->getListReviewsByObjectId($objectId, $page, self::countElementsOnPage, $reviewsSort);
        
        $pagination = Paginate::pagination($countReviewsByObject, $page, self::countElementsOnPage, $reviewsSort, $this->getDomainObject());

        
        return view('account.reviews', [
            'breadcrumbs' => $this->breadcrumbs,
            'listReviews' => $listReviews,
            'funcModel' => $funcModel,
            'pagination' => $pagination,
            'subdomain' => $this->getDomainObject(),
            'countReviews' => $countReviewsByObject,
            'totalValueByCategoryForObject' => $totalValueByCategoryForObject,
            'isConfirmed' => $this->confirmedAccount(),
            'averageRaiting' => $averageRaiting
        ]);
        
    }
    
}
