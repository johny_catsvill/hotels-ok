<?php

namespace App\Http\Controllers\ControllersObject;

use App\Models\ObjectHotel;
use App\Traits\timeTraits;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Calendar;
use App\Models\EventCalendar;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Models\Room;
use DB;
use App\Helpers\Validate;
use Exception;

class CalendarController extends Controller
{

    use timeTraits;

    public $m = [
        '01' => 'Январь',
        '02' => 'Февраль',
        '03' => 'Март',
        '04' => 'Апрель',
        '05' => 'Май',
        '06' => 'Июнь',
        '07' => 'Июль',
        '08' => 'Август',
        '09' => 'Сентябрь',
        '10' => 'Октябрь',
        '11' => 'Ноябрь',
        '12' => 'Декабрь'
    ];
    
    public $y = [
        '2017', '2018', '2019', '2020', '2021', '2022', '2023'
    ];

    public function index(Room $room, EventCalendar $eventCalendar) {

        $this->breadcrumbs += ['Календарь бронирования номеров' => route('calendar', $this->getDomainObject())];

        $userId = Auth::id();
        $objectId = User::find($userId)->objectId;
        $roomsList = $room->getListRoomByObjectId($objectId);;
        $needMonth = date('m');
        $needYear = date('Y');
        if(count($roomsList) > 0) {
            $calendar = $this->createCalendar($eventCalendar, $roomsList[0]->roomId, $needMonth, $needYear);    
        }
        else {
            $calendar = '';
        }

        
        return view('account.calendar',
            [
                'breadcrumbs' => $this->breadcrumbs,
                'subdomain' => $this->getDomainObject(),
                'roomsList' => $roomsList,
                'needMonth' => $needMonth,
                'needYear' => $needYear,
                'listMonths' => $this->m,
                'calendar' => $calendar,
                'listYears' => $this->y,
            'isConfirmed' => $this->confirmedAccount()

            ]);

    }

    public function get_calendar_by_room(Request $request, Room $room, EventCalendar $eventCalendar)
    {
        $userId = Auth::id();
        $objectId = User::find($userId)->objectId;
        $errors = false;
        
        if($request->isMethod('post')) {
            $requestAll = $request->all();
            
            
            if($requestAll['roomId'] && $requestAll['countRoom']) {
                
                $countRoom = $requestAll['countRoom'];
                if(!Validate::unsignedInt($requestAll['roomId'])) throw new exception('Неккоректный id');
                if(!Validate::unsignedInt($requestAll['countRoom'])) throw new exception('Неккоректное количество номеров');
                $dateFrom = $requestAll['dateFrom'];
                $dateTo = $requestAll['dateTo'];
                $price = $requestAll['price'];
                $discount = $requestAll['discount'];
                $isCloseRoom = $requestAll['closeRoom'];
                $dayWeek = false;
                
                if(isset($requestAll['dayWeek'])) {
                    $dayWeek = $requestAll['dayWeek'];
                }
                
                if(!$dateFrom && !Validate::unsignedInt(strtotime($dateFrom))) throw new exception('Неккоректная дата');
                if(!$dateTo && !Validate::unsignedInt(strtotime($dateTo))) throw new exception('Неккоректная дата');
                if(!$price && !Validate::unsignedFloat($price)) throw new exception('Некорректная цена');
                if($discount && !Validate::unsignedFloat($discount)) throw new exception('Неккоректная скидка');
                if(!Validate::logic($isCloseRoom)) throw new exception('Invalid close room');
                if($dayWeek && !is_array($dayWeek)) throw new exception('Invalid array');
                $roomId = $requestAll['roomId'];

                
                $room = Room::find($roomId);
                $object = ObjectHotel::find($room->objectId);
                $cityParentId = $object->cityId;

                if($object->city->parentId) {
                    $cityParentId = $object->city->parentId;
                }
                if($objectId == $room->objectId && $room->countRoom >= $countRoom) {
                    $dateArray = $this->getDateTimeStartStopByDays($dateFrom, $dateTo);
                    array_pop($dateArray);

                    foreach($dateArray as $key => $date) {
                        $periods = [];
                        array_push($periods, strtotime($key));

                        $countRoomReservation = $room->getListRoomsReservation($object->id, $periods, $roomId, $dateFrom, $dateTo, true);

                        if(isset($countRoomReservation[0]->calendar1)) {
                            $countRoomCalendar = $countRoomReservation[0]->calendar1;
                        }
                        else {
                            $countRoomCalendar = 0;
                        }

                        $countFreeRoom = $room->countRoom - $countRoomCalendar;

                        //$room->count = 7; $calendar-> 4; $room->6;

                        if($countRoomCalendar > $countRoom) {
                            //удаляем несколько записей

                            $countRoomForDelete = $countRoomCalendar - $countRoom;
                            $calendars = EventCalendar::where('typeRoomId', $room->id)
                                ->where('objectId', $object->id)
                                ->where('date', strtotime($key))
                                ->limit($countRoomForDelete)
                                ->orderBy('id');

                            if(is_array($dayWeek)) {
                                $calendars->whereIn('dayWeekNum', $dayWeek);
                            }

                            $calendars->delete();

                        }
                        elseif($countRoomCalendar == $countRoom) {
                            //обновляем записи, не удаляем
                            $calendars = EventCalendar::where('typeRoomId', $room->id)
                                ->where('objectId', $object->id)
                                ->where('date', strtotime($key));

                            if(is_array($dayWeek)) {
                                $calendars->whereIn('dayWeekNum', $dayWeek);
                            }

                            $calendars->update([
                                'valuePercent' => $discount,
                                'price' => $price,
                                'closeRoom' => $isCloseRoom
                            ]);
                        }
                        elseif ($room->countRoom >= $countRoom) {
//                            $needRoomCount = $room->countRoom - $countRoom;
                            $needRoomCount =  $countRoom - $countRoomCalendar;

                            if($countFreeRoom && $countFreeRoom >= $needRoomCount) {
                                DB::beginTransaction();
                                try {

                                    $result = $eventCalendar->createEventByRoom($room->id, $key, $price, $isCloseRoom, $discount, $dayWeek,
                                        $object->cityId, $object->city->regionId, $object->objectCategoryId, $cityParentId, $object->id, $room->amountPerson, $needRoomCount);

                                }
                                catch(QueryException $e) {
                                    DB::rollBack();
                                    $errors = true;
                                }

                                DB::commit();


                            }
                            else {
                                return ['success' => false, 'message' => 'Количество номеров для бронирования превышает количество доступных номеров!'];
                            }

                        }


                    }
                    if(!$errors) {
                        $currentDateArray = explode('-', $requestAll['dateFrom']);
                        $needMonth = $currentDateArray[1];
                        $needYear = $currentDateArray[2];
                        $calendar = $this->createCalendar($eventCalendar, $requestAll['roomId'], $needMonth, $needYear);   
                        $returnHTML = view('account.blocks.calendar-block')->with(['needMonth' => $needMonth, 'listMonths' => $this->m, 'calendar' => $calendar])->render();
                        return response()->json(array('success' => true, 'html' => $returnHTML));
                    }
                }

                return ['success' => false, 'message' => 'Количество номеров для бронирования превышает общее количество номера в отеле!'];
            }

            if($requestAll['roomId'] && $requestAll['countRoom'] == 0) {
                $dateFrom = $requestAll['dateFrom'];
                $dateTo = $requestAll['dateTo'];
                $dateArray = $this->getDateTimeStartStopByDays($dateFrom, $dateTo);
                array_pop($dateArray);
                foreach($dateArray as $key => $d) {
                    $dateArray[$key] = strtotime($key);
                }
                $eventCalendar->removeRoomOnTheDate($dateArray, $requestAll['roomId']);
                $currentDateArray = explode('-', $requestAll['dateFrom']);
                $needMonth = $currentDateArray[1];
                $needYear = $currentDateArray[2];
                $calendar = $this->createCalendar($eventCalendar, $requestAll['roomId'], $needMonth, $needYear);   
                $returnHTML = view('account.blocks.calendar-block')->with(['needMonth' => $needMonth, 'listMonths' => $this->m, 'calendar' => $calendar])->render();
                return response()->json(array('success' => true, 'html' => $returnHTML));
            }
            return ['success' => false, 'message' => 'Ошибка. Некорректные данные'];
        }
        
    }

    

    public function get_month_info_by_room(Request $request, EventCalendar $eventCalendar) {

        if($request->isMethod('post')) {
            $requestAll = $request->all();
            
            $roomId = $requestAll['roomId'];
            $month = $requestAll['month'];
            $year = $requestAll['year'];
            
            if(!Validate::unsignedInt($roomId)) throw new exception('Неккоректный id');
            if(!Validate::int($year)) throw new exception('Неккоректный год');
            if(!checkdate(1, $month, $year)) throw new exception('Неккоректная дата');
            
            $calendar = $this->createCalendar($eventCalendar, $requestAll['roomId'], $requestAll['month'], $requestAll['year']);
            $returnHTML = view('account.blocks.calendar-block')->with(['listMonths' => $this->m, 'calendar' => $calendar, 'needMonth' => $requestAll['month']])->render();
            return response()->json(array('success' => true, 'html' => $returnHTML));
        }

    }
    
    
    public function createCalendar($eventCalendar, $roomId, $month, $year) {
        $infoCalendarByDays = $eventCalendar->getListInfoByRoomIdAndMonth($roomId, $month, $year);
        $infoCalendarByDays = $this->reloadKeyArrayRoom($infoCalendarByDays);
//        dump($infoCalendarByDays);
        $calendar = '<table><tr><th>понедельник</th><th>вторник</th><th>среда</th><th>четверг</th><th>пятница</th><th>суббота</th><th>воскресенье</th></tr><tr>';
        
        $standartPriceRoom = Room::find($roomId)->price;

        $d = date('01-'. $month .'-Y');
        $currentDate = date('d');
        
        $dayWeekNum = date("w", mktime(0, 0, 0, $month, '01', $year)) -1;

        if($dayWeekNum < 0) {
            $dayWeekNum = 6;
        }
        for($i = 0; $i < $dayWeekNum; $i++) {
            $calendar.= '<td></td>';
        }
        
        $countDay  = date('t',mktime(0,0,0,$month,1,$year));
        $startDay = 0;
        for($i = 0; $i < $countDay; $i++) {
            $startDay++;
            $calendar.= '<td onclick="Calendar.showPopupCalendar(this);"><a href="javascript:void(0);"><span class="number-day-calendar">';         
            
                
            if(strtotime(date($startDay.'-'.$month.'-'.$year)) < strtotime(date('d-m-Y'))) {
                //если выбраная дата меньше текущей даты то просто выводим серым цветом
                $calendar.= '<span>'.$startDay.'</span>';
            }
            elseif(date('m') == $month && date('Y') == $year) {
                
                //если текущий месяц совпадает с выбраным и текущий год совпадает с выбранным то...
                if($startDay < $currentDate) {
                    //если день(итерации) меньше чем текущий день то просто выводим
                    $calendar.= '<span>'. $startDay .'</span>';
                }
                elseif($startDay == $currentDate && date('m') == $month && date('Y') == $year) {
                    //если день (итерации) равен текущему дню то отметить зеленым цветом
                    $calendar.= '<span class="current-day">'. $startDay .'</span>';
                }
                else {
                    
                    if(isset($infoCalendarByDays[$startDay]) && $infoCalendarByDays[$startDay]->closeRoom) {
                        
                        $calendar.= '<span class="close-day">'. $startDay .'</span>';  
                    }
                    else {
                        //если день (итерации) больше чем текущий день и номер свободен то синим
                        $calendar.= '<span class="active-day">'. $startDay .'</span>';    
                    }
                    
                }    
            }
            else {
                if(isset($infoCalendarByDays[$startDay]) && $infoCalendarByDays[$startDay]->closeRoom) {
                        $calendar.= '<span class="close-day">'. $startDay .'</span>';  
                }
                else {
                    //если день (итерации) больше чем текущий день и номер свободен то синим
                    $calendar.= '<span class="active-day">'. $startDay .'</span>';    
                }
            }      
            
            
            $currentDateSecond = strtotime(date('d-m-Y'));
            
            if(isset($infoCalendarByDays[$startDay]) && $currentDateSecond <= $infoCalendarByDays[$startDay]->date) {
                $calendar.= '<b>'. round($infoCalendarByDays[$startDay]->price, 2) .' UAH</b></span><ul class="info-calendar-day"><li>Доступно: '. $infoCalendarByDays[$startDay]->countRoom .'</li><li class="color-red">Забронировано: '. $infoCalendarByDays[$startDay]->countReservation .'</li><li class="color-green">Свободно: '. ($infoCalendarByDays[$startDay]->countRoom - $infoCalendarByDays[$startDay]->countReservation) .'</li></ul>';
                if($infoCalendarByDays[$startDay]->valuePercent) {
                    $calendar.= '<span class="is-discount-in-day">'. $infoCalendarByDays[$startDay]->valuePercent .'%</span>';    
                }
                
            }
            else {
//                dd($currentDateSecond, $infoCalendarByDays[$startDay]->date, $startDay);
                $calendar.= '<b>'. $standartPriceRoom .' UAH</b></span><ul class="info-calendar-day"></ul></span>';
            }
            
            $calendar.= '<div class="list-info-room"></div></a></td>';

            $dateWeek = date("w", mktime(0, 0, 0, $month, $i, $year));
            if($startDay == $countDay) {

                if($dateWeek < 6) { 
                    for($z = $dateWeek; $z < 6; $z++) {
                        $calendar.= '<td><a href="javascript:void(0);"><div class="list-info-room"></div></a></td>';

                    }
                }
            }
            if ($dateWeek % 7 == 6) { // вс, последний день - перевод строки
                $calendar.= '</tr><tr>';
                
            }
            
               
        }
         
        
        $calendar.= '</tr></table>';
        return $calendar;
        
    }
    
    public function reloadKeyArrayRoom($arrayRoomEvent) {
        
        $newArray = [];
        
        foreach($arrayRoomEvent as $roomEvent) {
            $newArray[$roomEvent->day] = $roomEvent;
        }
        return $newArray;
        
    }
    
    public function get_info_room_popup(Request $request, EventCalendar $eventCalendar) {
        if($request->isMethod('post')) {
            $requestAll = $request->all();
            
            if(!Validate::unsignedInt($requestAll['roomId'])) throw new exception('Неккоректный id');
            $date = strtotime($requestAll['date']);
            if(!Validate::unsignedInt($date)) throw new exception('Неккоректная дата');
            
            $roomInfoForCalendarPopup = $eventCalendar->getInfoRoomForCalendarPopup($requestAll['roomId'], $date);
            
            if(!$roomInfoForCalendarPopup) {
                $price = Room::find($requestAll['roomId'])->price;
            }
            else {
                $price = $roomInfoForCalendarPopup->price;
            }
            
            $returnHTML = view('account.blocks.popup_calendar')->with(['roomInfo' => $roomInfoForCalendarPopup, 'price' => $price])->render();
            return response()->json(array('success' => true, 'html' => $returnHTML));
        }
    }

}
