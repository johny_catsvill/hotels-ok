<?php

namespace App\Http\Controllers\ControllersObject;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Models\User;
use App\Models\Reservation;
use App\Models\Room;
use App\Helpers\Validate;
use Exception;
use Excel;

class ReservationController extends Controller
{
    //
    
    
    public function list_reservations(Request $request, Reservation $reservationModel) {
//        return view('stub-admin');

        $this->breadcrumbs += ['Список броней' => route('reservations', $this->getDomainObject())];
        $user = Auth::id();
        $objectId = User::find($user)->objectId;
        if(!$objectId) {
            return redirect()->route('logout');
        }
        
        $yesterday = date('d-m-Y', strtotime(date('d.m.Y')) - 86400);
        $tommorrow = date('d-m-Y', strtotime(date('d.m.Y')) + 86400);
        $today = date('d-m-Y');
        
        $reservations = $reservationModel->getListReservation($objectId);

        return view('account.reservation', [
            'breadcrumbs' => $this->breadcrumbs,
            'subdomain' => $this->getDomainObject(),
            'reservations' => $reservations,
            'yesterday' => $yesterday,
            'tommorrow' => $tommorrow,
            'today' => $today,
            'isConfirmed' => $this->confirmedAccount()
        ]);
        
    }
    
    
    public function get_info_reservation(Request $request, Reservation $reservationModel, Room $roomModel, $domain, $id) {
        
        if($id) {
            
            $this->breadcrumbs += ['Детальная информация по броне' => route('get-info-reservation-by-id', ['subdomain' => $this->getDomainObject(), 'id' => $id])];
            
            $user = Auth::id();
            $objectId = User::find($user)->objectId;
            
            if($objectId) {

                $reservation = $reservationModel->getInfoByReservationId($id);


                if($reservation->objectId == $objectId) {
                    
                    $roomsReservation = $reservationModel->getInfoRoomsByReservationId($id);
                    
                    return view('account.info-reservation', [
                        'breadcrumbs' => $this->breadcrumbs,
                        'subdomain' => $this->getDomainObject(),
                        'reservation' => $reservation,
                        'roomsReservation' => $roomsReservation,
                        'isConfirmed' => $this->confirmedAccount()
                    ]);
                    
                }
                else {
                    abort(404);
                }
            }
            
            return redirect()->back();
            
        }
        
        return redirect()->back();
        
    }
    
    
    public function get_reload_reservation_by_filters(Request $request, Reservation $reservationModel) {
        
        if($request->isMethod('post')) {
            
            $user = Auth::id();
            $objectId = User::find($user)->objectId;
            if(!$objectId) {
                return redirect()->route('logout');
            }
            
            $requestAll = $request->all();
            
            $status = $requestAll['status'];
            $type = $requestAll['type'];
            $prepayment = $requestAll['prepayment'];
            $dateFrom = strtotime($requestAll['dateFrom']);
            $dateTo = strtotime($requestAll['dateTo']);

            if($status && ($status != 3 && $status !=2)) throw new Exception();
            if($type && ($type != 1 && $type != 0)) throw new Exception();
            if($prepayment && ($prepayment != 1 && $prepayment != 0)) throw new Exception();
            
            if($dateFrom && !Validate::unsignedInt($dateFrom)) throw new Exception();
            if($dateTo && !Validate::unsignedInt($dateTo)) throw new Exception();
            

            
            $reservations = $reservationModel->getListReservation($objectId, $dateFrom, $dateTo, $status, $prepayment, $type);
            if(count($reservations) > 0) {
                $returnHTML = view('account.blocks.list-reservation')->with(['reservations' => $reservations, 'subdomain' => $this->getDomainObject()])->render();

                return response()->json(array('success' => true, 'html' => $returnHTML));    
            }
            
            return response()->json(array('success' => false));

        }
        
        
    }

    public function excel(Request $request, Reservation $reservationModel) {

        $data = $request->all();

        if($data) {

            $objectId = Auth::user()->objectId;

            $dateFrom = strtotime($data['dateFrom']);
            $dateTo = strtotime($data['dateTo']);
            $status = false;
            $prepayment = false;
            $type = false;

            $reservations = $reservationModel->getListReservation($objectId, $dateFrom, $dateTo, $status, $prepayment, $type);

            Excel::create('reservations', function ($excel) use ($reservations) {
                $excel->setTitle('Ваши бронирования');

                $excel->sheet('Reservations', function($sheet) use($reservations) {

                    $sheet->setPageMargin(0.25);

                    $sheet->setWidth(array(
                        'A'     =>  15,
                        'B'     =>  35,
                        'C'     =>  25,
                        'D'     =>  25,
                        'E'     =>  45,
                        'F'     =>  20,
                        'G'     =>  20,
                        'H'     =>  20,
                        'I'     =>  20,
                        'J'     =>  20
                    ));

                    $sheet->row(1, array(
                        'test1', 'test2'
                    ));

                    $sheet->row(2, array(
                        'test3', 'test4'
                    ));

                    $sheet->row(3, array(
                        'Иден. брони', 'Имя гостя', 'Заезд', 'Отьезд', 'Название номера', 'Контактный телефон', 'Дата бронирования', 'Статус', 'Итоговая цена', 'Комиссия'
                    ));

                    $currentRow = 4;
                    foreach ($reservations as $reservation) {

                        $status = '';
                        if($reservation->statusId == 3) {
                            $status = 'Активна';
                        }
                        elseif($reservation->statusId == 2) {
                            $status = 'Отменено';
                        }

                        $sheet->row($currentRow, array(
                            $reservation->id, $reservation->name, date('d.m.Y', $reservation->startReservationDate),
                            date('d.m.Y', $reservation->endReservationDate), $reservation->roomsName, $reservation->visitorPhone,
                            date('d.m.Y', $reservation->creationDate), $status, $reservation->price. ' UAH', '181.80 UAH'
                        ));
                        $currentRow++;
                    }

//                    $sheet->cells('A:J', function($cells) {
//
//                        $cells->setAlignment('center');
//
//                    }); //выравнивание по центру

                });

            })->download('xls');
        }
        else {
            abort(404);
        }


        dd($data);
    }
    
    
    

}
