<?php

namespace App\Http\Controllers\ControllersObject;

use Illuminate\Http\Request;
use Auth;
use App\Models\User;
use App\Models\Clients;
use App\Http\Controllers\Controller;
use App\Models\ObjectHotel;
use App\Models\Reservation;
use App\Models\Review;

class AccountController extends Controller {
    public function index(Request $request, Reservation $reservationModel, Review $reviewModel, ObjectHotel $objectHotelModel) {
        
        $request->session()->forget('webmID');
        $currentDate = strtotime(date('d.m.Y'));
        if($request->isMethod('post')) {
            $requestAll = $request->all();
            $currentDate = $requestAll['date'];
        }
        
        
        $user = User::find(Auth::id());
        $object = ObjectHotel::find($user->objectId);
        
         
        $day = $currentDate + (24 * 3600 - 1);
        $yesterday = $currentDate - 24 * 3600;
        $tommorrow = $currentDate + 24 * 3600;
        $reservations = $reservationModel->getListReservationByDate($currentDate, $day, $object->id);
        $reservationsArrival = $reservationModel->getListReservationArrivalByDate($currentDate, $day, $object->id);
        $reservationsDeparture = $reservationModel->getListReservationDepartureByDate($currentDate, $day, $object->id);
        
        $reviews = $reviewModel->getListReviewsByDate($currentDate, $day, $object->id);
        
        if($request->isMethod('post')) {
            $returnHTML = view('account.blocks.info-about-object')->with([
                'reservations' => $reservations, 
                'reservationsArrival' => $reservationsArrival, 
                'reservationsDeparture' => $reservationsDeparture,
                'reviews' => $reviews, 'subdomain' => $this->getDomainObject()])->render();
            return response()->json(array('html' => $returnHTML));
        }
        
        $this->breadcrumbs += ['Главная' => '/'];

        return view('account.home', 
                    [
                        'breadcrumbs' => $this->breadcrumbs,
                        'subdomain' => $this->getDomainObject(),
                        'object'  => $object,
                        'yesterday' => $yesterday,
                        'currentDate' => $currentDate,
                        'tommorrow' => $tommorrow,
                        'reservations' => $reservations,
                        'reservationsArrival' => $reservationsArrival,
                        'reservationsDeparture' => $reservationsDeparture,
                        'reviews' => $reviews,
                        'isConfirmed' => $this->confirmedAccount()
                    ]);

   
    }
    
    public function settings(Request $request, User $userModel) {
        
        $this->breadcrumbs += ['Настройки' => route('settings', $this->getDomainObject())];
        $userId = Auth::id();
        $infoUser = $userModel->getInfoByUserId($userId);

        if($request->isMethod('post')) {
            $requestAll = $request->all();
            $userModel->updateInfoUser($userId, $requestAll);
            return redirect()->route('settings', $this->getDomainObject())->with('message', 'Информация успешно обновлена!');
        }
        
        
        return view('account.settings', [
            'breadcrumbs' => $this->breadcrumbs,
            'subdomain' => $this->getDomainObject(),
            'infoUser' => $infoUser,
            'isConfirmed' => $this->confirmedAccount()
        ]);
        
    }
    
    public function change_password(Request $request, User $userModel) {
        if($request->isMethod('post')) {
            $requestAll = $request->all();
            if($requestAll['password'] == $requestAll['repeatPassword']) {
                
                $userId = Auth::id();
                
                $userModel->changePasswordAccount($requestAll['password'], $userId);
                
                return redirect()->back()->with('message', 'Пароль успешно изменен!');
            }
            return redirect()->back()->withErrors('Данные пароли не совпадают');
        }
        return abort(404);
    }

    

    
    
    
    
    
    
}
