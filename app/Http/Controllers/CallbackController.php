<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Callback;
use DB;
class CallbackController extends Controller
{
    public function sendRequestCallback(Request $request, Callback $callback) {
        if($request->ajax()) {
            $phoneCallback = $request->phone;
            $phoneCallbackNew = preg_replace('![^0-9]+!', '', $phoneCallback);
            $callback->setCallbackUser($phoneCallbackNew);
        }
    }

    public function sendReview(Request $request, Callback $callback) {
        if($request->ajax()){
            $data = $request->all();
            $callback->saveReviewUser($data);
        }
    }

    public function add_application(Request $request) {

        if($request->isMethod('post')) {

            $data = $request->all();

            $info = '';

            if(isset($data['info'])) {
                $info = $data['info'];
            }

            DB::table('application_object')->insert([
                'creationDate' => time(),
                'name' => $data['name'],
                'email' => $data['email'],
                'phone' => $data['phone'],
                'object_name' => $data['object_name'],
                'type_application' => $data['type'],
                'info' => $info
            ]);

            return 1;

        }

    }

    public function add_application_group(Request $request) {

        if($request->isMethod('post')) {

            $data = $request->all();

            DB::table('application_groups')->insert([
                'creationDate' => time(),
                'name' => $data['name'],
                'email' => $data['email'],
                'phone' => $data['phone'],
                'city' => $data['city'],
                'amountPerson' => $data['amountPerson']
            ]);

            return 1;

        }

    }

}
