<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PartnerController extends Controller
{
    public function partnerLending(){

        return view('site.partner', [
            'data' => $this->data,
            'page' => 'partners'
        ]);
    }
}
