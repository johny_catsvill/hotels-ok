<?php

namespace App\Http\Controllers;

use App\Helpers\Func;
use Illuminate\Http\Request;
use App\Models\ObjectHotel;
use App\Models\CategoriesTravel;
class ErrorsController extends Controller
{
    
    
    public function error404(Request $request, ObjectHotel $object, CategoriesTravel $categoriesTravel) {
//        return view('mail.certificate');
        $this->data['title'] = 'Страница не найдена';
        $this->data['keywords'] = 'Страница не найдена';
        $this->data['description'] = 'Страница не найдена';
        
        $listCategoryAndCitiesTravel = array();
        for($i = 1; $i < 6; $i++) {
            $listCities = $categoriesTravel->getListCities($i);
            if($listCities) {
                $countObject = $listCities[0]->countObject;
                $key = $listCities[0]->categoryName;
                $categoryLink = $listCities[0]->categoryLink;
                $listCategoryAndCitiesTravel[$key]['cities'] = $listCities;
                $listCategoryAndCitiesTravel[$key]['categoryId'] = $i;
                $listCategoryAndCitiesTravel[$key]['countObjectCategory'] = $countObject;
                $listCategoryAndCitiesTravel[$key]['categoryLink'] = $categoryLink;    
            }
            
        }

        $endDay = strtotime(date('d.m.Y')) + 86400;
        return view('404', [
            'title' => $this->data['title'],
            'keywords' => $this->data['keywords'],
            'description' => $this->data['description'],
            'page'=> $this->page,
            'dateFrom' => date('d.m.Y'),
            'dateTo' =>date('d.m.Y', $endDay),
            'searchShow' => $this->search['show'],
            'listCategoryAndCitiesTravel' => $listCategoryAndCitiesTravel
        ]);
    }
    
}
