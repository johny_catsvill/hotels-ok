<?php

namespace App\Http\Controllers;

use App\Models\SocialAccount;
use Illuminate\Http\Request;
use Validator;
use App\Helpers\GenerateCode;
use App\Models\User;
use App\Models\CodesActivated;
use App\Models\Clients;
use App\Models\ObjectHotel;
use Mail;
use Auth;
use Session;
use App\Helpers\Func;
use Hash;
use DB;
use Artisan;
use App\Models\ObjectsCategories;
use Laravel\Socialite;
//use Cookie;

class AuthController extends Controller
{
    //
    
    public function login(Request $request) {

        $this->breadcrumbs += ['Авторизация' => '/login/'];
        $remember = false;
        if(Auth::id()) {
            $user = Auth::user();
            
            if($user->objectId) {
                $subdomain = ObjectHotel::find($user->objectId)->url;
                return redirect()->route('domain-home', $subdomain);
            }
            elseif($user->roleId) {
                return redirect()->route('administrator-home');
            }
           return redirect()->route('user.profile');
        }
        if($request->isMethod('post')) {

            $requestAll = $request->all();
            if(isset($requestAll['remember_me'])) {
                $remember = true;
            }

            $validator = Validator::make($requestAll, [
                'email' => 'required|email',
                'password' => 'required'
            ]);
            
            
            $user = User::where('email', $requestAll['email'])->where('activated', 1)->first();
            if(!$user) {
                return redirect()
                    ->back()->withInput()
                    ->with('error-auth', 'Некорректные данные для входа. Возможно пользователь не существует или не был активирован!  Проверьте почту и попробуйте еще раз!');
            }

            if(Hash::check($requestAll['password'], $user->password)) {

                $code = GenerateCode::generateCode(22);
                $user->hash = $code;
                $user->save();

                
                if($user->objectId) {

                    $subdomain = ObjectHotel::find($user->objectId)->subdomain;


//                    setcookie("webmID", $code, time()+3600, "/", '.hotels-ok.com');
                    setcookie("webmID", $code, time()+3600, "/", '.hotels-ok');
//                    return ['error' => false, 'link' => route('auth-object', $subdomain)];
                    return redirect()->route('auth-object', $subdomain);
                }
                
                elseif($user->roleId === 1) {
                    Auth::loginUsingId($user->id, true);
                    return redirect()->route('administrator-home');
                }
                else {
                    Auth::loginUsingId($user->id, true);
                    Artisan::call('view:clear');
                    Artisan::call('cache:clear');
                    return redirect()->route('user.profile');
                }
            }
            else {
                return redirect()->back()->withInput()
                    ->with('error-auth', 'Некорректные данные для входа. Возможно пользователь не существует или не был активирован!  Проверьте почту и попробуйте еще раз!');
            }
        }
        
        return view('auth.login', [
            'page' => $this->page,
            'searchShow' => $this->search['hide'],
            'breadcrumbs' => $this->breadcrumbs
        ]);
            
    }
    
    public function registration(Request $request, User $user) {

        $requestAll = $request->all();
        $this->breadcrumbs += ['Регистрация пользователя' => route('registration')];

        if(!Auth::id()) {
            if($request->isMethod('post')) {

            $validator = Validator::make($requestAll, [
                'email' => 'required|email|unique:users,email',
                'name' => 'required|string|min:2',
                'lastName' => 'required|string|min:2',
                'password' => 'required|string|min:8',
                'passwordRepeat' => 'required|same:password',
                'agree' => 'required|accepted',
                'phone' => 'required'
            ]);
            
            if($validator->fails()) {
                return redirect()->back()->withErrors($validator->errors())->withInput();
            }
                
            $code = GenerateCode::generateCode(8);

            DB::beginTransaction();
            try {
                $client = new Clients;
                $client->name = $requestAll['name'];
                $client->lastName = $requestAll['lastName'];
                $client->phone = $requestAll['phone'];
                $client->save();

                $user =  new User;
                $user->email = $requestAll['email'];
                $user->password = bcrypt($requestAll['password']);
                $user->clientId = $client->id;
                $user->creationDate = strtotime(date('d.m.Y'));
                $user->save();

                $codeActivate = new CodesActivated;
                $codeActivate->userId = $user->id;
                $codeActivate->code = $code;
                $codeActivate->save();

            }
            catch(QueryException $e) {
                DB::rollBack();
                return redirect()->back();
            }

            DB::commit();

            
            $url = url('/').'/auth/activate?id='.$user->id.'&code='.$code;      
            Mail::send('mail.confirmation-account', array('url' => $url, 'email' => $requestAll['email']), function($message) use ($requestAll)
            {    
                $message->from('info@hotels-ok.com');
                $message->to($requestAll['email'])->subject('Registration hotels-ok');
            });

            return redirect()->route('registration')->with('successRegistration', 'На вашу почту отправлено письмо, <br>
пройдите по ссылке для подтверждения');
            
        }
        
        return view('auth.registration', [
                                          'page' => $this->page,
                                          'searchShow' => $this->search['hide'],
                                         'breadcrumbs' => $this->breadcrumbs
                                         ]);
        }
        else {
           $user = Auth::user();
            if($user->roleId) {
                return redirect()->route('administrator-home')->with('message', 'Вы уже авторизированы!');
            }
            elseif($user->objectId) {

                route('domain-home', $user->object->subdomain);
            }
            else {
                return redirect()->route('user.profile');
            }

        }
        
    }
    
    public function registrationOrganization(Request $request, Func $functionHelper) {
        
        
        $this->data['title'] = 'Регистрация организации';
        $this->data['keywords'] = '';
        $this->data['description'] = '';
        $this->breadcrumbs += ['Регистрация организации' => '/regorg'];
        
        if(!Auth::id()) {
            $listTypeObject = ObjectsCategories::all();

            if($request->isMethod('post')) {
                $requestAll = $request->all();
                
                $messages = [
                'typeObject.required' =>                                    'Поле «Тип объекта» обязательное к заполнению!',
                'name.required' =>                                          'Поле «Имя организации» обязательное к заполнению!',
                'name.min' =>                                               'Поле «Имя организации» должно состоять минимум из 2 символов!',
                'email.required' =>                                         'Поле «Электронный адрес» обязательное к заполнению!',
                'email.email' =>                                            'Некорректный электронный адрес!',
                'email.unique' =>                                           'К сожалению пользователь с таким электронным адресом уже зарегистрирован в системе',
                'password.required' =>            'Поле "Пароль" обязательное к заполнению',
                'password.min' =>                 'Поле "Пароль" должно состоять минимум из 8 символов',
                'password.same' =>                'Поле "Повторите пароль" должно состоять минимум из 8 символов и быть таким же как и значение поля "Пароль"!',
                'phone.required' =>                                         'Поле "Телефон" обязательное к заполнению!',
                'phone.min' =>                                              'Поле "Телефон" должно состоять из 10 символов!',
                
            ];
                
                $validator = Validator::make($requestAll, [
                    'typeObject' => 'required|integer',
                    'name' => 'required|string|min:2',
                   'email' => 'required|email|unique:users,email',
                    'password' => 'required|string|min:8|same:repeatPassword',
                    'phone' => 'required|min:10',
                    
                ], $messages);

                if($validator->fails()) {
                    return redirect()->route('regorg')->withInput()->withErrors($validator->errors());
                }                


                $object = new ObjectHotel;

                $object->organizationName = $requestAll['name'];
                $object->email = $requestAll['email'];
                $object->objectCategoryId = $requestAll['typeObject'];
                $object->subdomain = $functionHelper->translit($requestAll['name']);
                $object->save();

                $user = new User;
                $user->email = $requestAll['email'];
                $user->phone = $requestAll['phone'];
                $user->password = bcrypt($requestAll['password']);
                $user->objectId = $object->id;
                $user->creationDate = strtotime(date('d.m.Y'));
                $user->save();

                $codeActivate = GenerateCode::generateCode(8);

                $code = new CodesActivated;
                $code->userId = $user->id;
                $code->code = $codeActivate;
                $code->save();

                $url = url('/').'/auth/activate?id='.$user->id.'&code='.$codeActivate;
                Mail::send('mail.confirmation-account', array('url' => $url, 'email' => $requestAll['email']), function($message) use ($requestAll)
                {   
                    $message->from('info@hotels-ok.com');
                    $message->to($requestAll['email'])->subject('Регистрация организации в hotels-ok.com');
                });
                
                return redirect()->with('message', 'Вы успешно зарегистрировались. На вашу почту отправлено письмо с подтверждением!');
                
            }
            
            return view('auth.registrationOrganization', [
                                                      'page' => $this->page,
                                                      'searchShow' => $this->search['hide'],
                                                     'breadcrumbs' => $this->breadcrumbs,
                                                        'title' => $this->data['title'],
                                                        'keywords' => $this->data['keywords'],
                                                        'description' => $this->data['description'],
                                                        'listTypeObject' => $listTypeObject
                                                     ]);
            
        }
        else {
            $user = Auth::user();
            if($user->roleId) {
                return redirect()->route('administrator-home')->with('message', 'Вы уже авторизированы!');
            }
            return false;
        }
        
        
    }
    
    public function activate(Request $request) {
        
        $result = CodesActivated::where('userId',$request->id)
            ->where('code',$request->code)
            ->first();
        
        if($result) {    
            
            $result->delete();          
            User::find($request->id)
                    ->update([                   
                        'activated'=>1,
                    ]);
            
            return redirect()->route('login')->with(['message' => 'Аккаунт успешно активирован. Для работы с аккаунтом авторизируйтесь!']);
        }
        
        abort(404);
        
    }
    
    public function logout() {
        Auth::logout();
        Artisan::call('view:clear');
        Artisan::call('cache:clear');
//        return redirect('http://hotels-ok.com/login');
        return redirect()->route('login');
    }

    
    
    public function remember_password(Request $request, User $userModel) {
        $this->breadcrumbs += ['Восстановление пароля' => route('remember-password')];

        if($request->isMethod('post')) {
            $email = $request->email;
            $requestAll = $request->all();
            $validator = Validator::make($requestAll, [
                'email' => 'required|email',
            ]);

           if($validator->fails()) {
               return redirect()->back()->withInput()->withErrors(['email' => 'Неккоректный email']);
           }
            
            $isEmailIsset = User::where('email', $email)->first();
            if($isEmailIsset) {
                $pass = GenerateCode::generateCode(8);
                $activateCode = GenerateCode::generateCode(12);
                
                $url = url('/').'/activate-password?id='.$isEmailIsset->id.'&code='.$activateCode;  
                
                Mail::send('mail.reset-password', array('password' => $pass, 'url' => $url, 'homepage' => route('homepage'), 'email' => $email), function($message) use ($email)
                {       
                    $message->from('info@hotels-ok.com');
                    $message->to($email)->subject('Восстановление пароля');
                });

                $isEmailIsset->newPassword = bcrypt($pass);
                $isEmailIsset->save();

                $userModel->createActivatePasswordRow($isEmailIsset->id, $activateCode);
                return redirect()->back()->with('message', 'Для изменения пароля зайдите на почту и перейдите по ссылке!');
            }
            return redirect()->back()->withInput()->withErrors(['email' => 'Пользователя с таким email не существует!']);
            
        }
        
        return view('auth.remember-password', [
            'breadcrumbs' => $this->breadcrumbs,
            'page' => $this->page,
        ]);
        
    }
    
    
    public function activate_password(Request $request, User $userModel) {
        $result = $userModel->getInfoUserPassword($request->id, $request->code);
        
        if($result) {    
            
            $userModel->removeInfoUserPassword($request->id, $request->code); 
            $user = User::find($request->id);
            $newPassword = $user->newPassword;
            $user->password = $newPassword;
            $user->save();
            
            return redirect()->route('login')->with(['message' => 'Пароль успешно изменен. Для работы с аккаунтом авторизируйтесь!']);
        }
        
        abort(404);
    }
    
        public function auth_object() {
            if(isset($_COOKIE['webmID'])) {

                $ssid = $_COOKIE['webmID'];
                $user = User::where('hash', $ssid)->first();

                if($user) {

                    $subdomain = ObjectHotel::find($user->objectId)->subdomain;
                    setcookie("webmID", '', time()-3600, '/', '.hotels-ok'); //hotels-ok.com

                    Auth::loginUsingId($user->id, true);

                    return redirect()->route('domain-home', $subdomain);    
                }
                
            }

            return redirect()->route('login');
        }


        public function redirectToProvider($type) {
            return Socialite::driver($type)->redirect();
        }

        public function handleProviderCallback($type) {
            $socialAccount = Socialite::driver($type)->user();
            $id = $socialAccount->getId();

            $profile = SocialAccount::where('socialId', $id)->where('type', $type)->first();

            if($profile) {
                Auth::loginUsingId($profile->userId);
                return redirect()->route('profile');
            }
            else {

                $password = GenerateCode::generateCode(10);

                $client = new Clients;
                $client->name = $socialAccount->getName();
                $client->save();

                $user =  new User;
                $user->email = $socialAccount->getEmail();
                $user->password = bcrypt($password);
                $user->clientId = $client->id;
                $user->activated = 1;
                $user->creationDate = time();
                $user->save();

                $newSocialAccount = new SocialAccount();
                $newSocialAccount->creationDate = time();
                $newSocialAccount->userId = $user->id;
                $newSocialAccount->name = $socialAccount->getName();
                $newSocialAccount->email = $socialAccount->getEmail();
                $newSocialAccount->avatar = $socialAccount->getAvatar();
                $newSocialAccount->socialId = $socialAccount->getId();
                $newSocialAccount->type = $type;

                $newSocialAccount->save();

                return redirect()->route('profile');
            }


        }


    
    
    
    
}
