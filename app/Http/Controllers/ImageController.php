<?php

namespace App\Http\Controllers;

use App\Traits\croppedPhotos;
use Illuminate\Http\Request;
use Storage;
use Auth;
use App\Models\ObjectHotel;


class ImageController extends Controller
{

    use croppedPhotos;

    public function upload_images(Request $request) {

        $data = array();
        if($request->isMethod('post')) {
            $user = Auth::user();
            $objectId = $user->objectId;
            $objectSubdomain = ObjectHotel::find($objectId)->subdomain;
            
            if(!file_exists(public_path().'/images/hotels/'.$objectId.'-'.$objectSubdomain)) {
                $path = public_path().'/images/hotels/'.$objectId.'-'.$objectSubdomain;
                mkdir($path, 0777);
                mkdir($path.'/images-objects', 0777);
                mkdir($path.'/images-rooms', 0777);
            }

            $category = $_POST['category'];
            $pathImages = '/images/hotels/'. $objectId .'-'. $objectSubdomain .'/'. $_POST['category']. '/';        
            
            $error = [];
            $files = array();
            $type = false;
            $metka = false;
            $validation = [
                'maxSize' => 5 * 1024 * 1024,
                'types' => array('image/jpeg', 'image/jpg', 'image/png')
            ];
            
            foreach($_FILES as $file){
                $type = false;
                $newNameImg = time().str_random(4);
                
                if(!in_array($file['type'], $validation['types'])) {
                    $metka = true;
                    array_push($error, 'Одно из изображений имеет неверный формат! Допустимые форматы: JPG, PNG, GIF.');
                }
                elseif($file['size'] > $validation['maxSize']) {
                    $metka = true;
                    array_push($error, 'Одно из изображений имеет размер больше 2 мб! Размер загружаемого файла не должен превышать 5 мб.');
                }
                
                else if(!$metka){
                    if($file['type'] == 'image/jpeg') $type = 'jpeg';
                    if($file['type'] == 'image/jpg') $type = 'jpg';
                    if($file['type'] == 'image/png') $type = 'png';

                    $imgNameWithoutType = $newNameImg;
                    $newNameImg = $newNameImg.'.'.$type;
                    if(move_uploaded_file($file['tmp_name'], public_path().$pathImages.$newNameImg)) {

                        $this->cropPhoto($pathImages, $newNameImg, $imgNameWithoutType, $type, $category);
//                        $nameImg = $this->watermark_img(public_path().$pathImages.$newNameImg, $pathImages, $type);
                        //$nameImg = $this->watermark_img(public_path().$pathImages.$newNameImg, $type);
                        if($category == 'images-objects') {
                            $files[] = $pathImages.$imgNameWithoutType.'_category.'.$type;
                        }
                        else {
                            $files[] = $pathImages.$imgNameWithoutType.'_norm.'.$type;
                        }

                           
                    }
                    
                }

            }
            $data = array('errors' => $error,'files' => $files);
            echo json_encode($data);
            
        }
        
    }
    
    public function delete_img_object(Request $request) {

        $sizes = array_merge($this->sizesImages, $this->sizesImagesRoom);

        if($request->isMethod('get')) {

            $img = $_GET['img'];
            $arrayImg = explode('/', $img);
            $nameImg = explode('_', end($arrayImg));

            $type = explode('.', $nameImg[1]);

            $newImg = preg_replace('/'.end($arrayImg).'/', '', $img);

            Storage::delete($newImg.$nameImg[0] .'.'.$type[1]);

            foreach ($sizes as $key => $size) {
                Storage::delete($newImg.$nameImg[0] .'_'.$key.'.'.$type[1]);
            }

            echo true;
        }
    }
    
//    public function watermark_text($oldImage, $newImage, $type) {
//        $text = 'hotels-ok.com';
//        $newImage = $newImage.time().str_random(4).'.png';
//        $font_size = 28;
//        $param = getimagesize($oldImage);
//        $width = $param[0];
//        $height = $param[1];
//        
//        switch($type) {
//            case 'png':
//                $img_src = imagecreatefrompng($oldImage);
//                $image = imagecreate($width, $height);
//                $bgc = imagecolorallocate($image, 255, 255, 255);
//                break;
//            default:
//                $img_src = imagecreatefromjpeg($oldImage);
//                $image = imagecreatetruecolor($width, $height);
//                break;
//        }
//        
//        
//        imagecopyresampled($image, $img_src, 0, 0, 0, 0, $width, $height, $width, $height);
//        $white = imagecolorallocate($image, 255, 255, 255);
//        
//        imagettftext($image, $font_size, 0, 30, $height - 50, $white, public_path().'/fonts/10469.ttf', $text);
//        imagejpeg($image, public_path().'/'.$newImage, 100);
//        imagedestroy($image);
//        unlink($oldImage);
//        return $newImage;
//    }
    
    public function watermark_img($img, $type) {
        $watermark = public_path().'/images/watermark.png';

        $wh = getimagesize($watermark);
        $fh = getimagesize($img);
        $rwatermark = imagecreatefrompng($watermark); 
        if($type == 'jpeg' || $type == 'jpg') {
            $rfile = imagecreatefromjpeg($img);
        }
        else {
            $rfile = imagecreatefrompng($img);
        }

          imagecopy($rfile, $rwatermark, 50, $fh[1] - $wh[1] - 70, 0, 0, $wh[0], $wh[1]);
          imagejpeg($rfile, $img, '100'); 
          imagedestroy($rfile);
        return true;
    }
    
}
