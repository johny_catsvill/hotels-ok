<?php

namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [

        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
        \Illuminate\Foundation\Http\Middleware\ValidatePostSize::class,
        \App\Http\Middleware\TrimStrings::class,
        \Illuminate\Foundation\Http\Middleware\ConvertEmptyStringsToNull::class,
        \Illuminate\Session\Middleware\StartSession::class,
        \App\Http\Middleware\Locale::class,

        // сюда добавить класс для создания сессии
        //
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web' => [
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            // \Illuminate\Session\Middleware\AuthenticateSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \App\Http\Middleware\VerifyCsrfToken::class,
            \Illuminate\Routing\Middleware\SubstituteBindings::class,
        ],

        'api' => [
            'throttle:60,1',
            'bindings',
        ],
    ];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => \Illuminate\Auth\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'bindings' => \Illuminate\Routing\Middleware\SubstituteBindings::class,
        'can' => \Illuminate\Auth\Middleware\Authorize::class,
        'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,
        'byCategory' => \App\Http\Middleware\ByCategory::class,
        'byReservation' => \App\Http\Middleware\ByReservation::class,
        'validateDataReservation' => \App\Http\Middleware\ValidateDataReservation::class,
        'byInformationObject' => \App\Http\Middleware\ByInformationObject::class,
        'byVerificationRoom' => \App\Http\Middleware\ByVerificationRoom::class,
        'byGeneralInformationObject' => \App\Http\Middleware\ByGeneralInformationObject::class,
        'validateAdmin' => \App\Http\Middleware\ValidateAdmin::class,
        'bySettingsUser' => \App\Http\Middleware\BySettingsUser::class,
        'validateUserObject' => \App\Http\Middleware\ValidateUserObject::class,
        'validateUser' => \App\Http\Middleware\ValidateUser::class,
        'clientPart' => \App\Http\Middleware\ClientPart::class,
        'loginedUser' => \App\Http\Middleware\LoginedUser::class,
    ];
}
