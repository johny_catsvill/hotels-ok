<?php

namespace App\Traits;
use Exception;

trait shortcodeTraits {

    public function parseStringSeo($title, $city, $price, $street = false, $objectName = false) {

        $shortCodes = [$city => 'city', round($price).' грн' => 'price', $street => 'street', $objectName => 'objectName'];

        foreach ($shortCodes as $key => $shortCode) {
            $title = preg_replace('/%'.$shortCode.'%/', $key, $title);
        }

        return $title;

    }
}