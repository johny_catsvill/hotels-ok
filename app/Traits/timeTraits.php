<?php

namespace App\Traits;

trait timeTraits {


    public function translateToSeconds($time) {
        $time = str_replace('.', ':', $time);
        $timePattern = explode(':', $time);
        $seconds = (($timePattern[0] * 60) * 60) + ($timePattern[1] * 60);
        return $seconds;
    }
    
    public function translateToHours($time) {
        $newTime = date("H:i", mktime(0, 0, $time));
        return $newTime;
    }


    public function getDateTimeStartStopByDays($timestampStart, $timestampStop)
    {
        try {
            $start = new \DateTime();
            $start->setTimestamp(strtotime($timestampStart));
            $stop = new\ DateTime();
            $stop->setTimestamp(strtotime($timestampStop));

            $tmp = $start->getTimestamp();
            $result = array();

            while ($stop->getTimestamp() >= $tmp) {
                $date = new \DateTime();
                $date->setTimestamp($tmp);

                $result[$date->format("Y-m-d")] = array(
                    'startTime' => $date->format("H:i"),
                    'stopTime'  => $date->setTime(23, 59, 59)->format('H:i')
                );

                $date->setTime(0, 0, 0)->modify("+1 day");
                $tmp = $date->getTimestamp();
            }
            if (isset($date)) {
                if ($start->format('Ymd') == $stop->format('Ymd')) {
                    $result[$start->format('Y-m-d')]['stopTime'] = $stop->format('H:i');
                } elseif ($stop->format('H:i') != $result[$start->format('Y-m-d')]['stopTime']) {
                    $result[$stop->format('Y-m-d')]['stopTime'] = $stop->format('H:i');
                }
            }
            return $result;
        } catch (Exception $e) {
            return array();
        }
    }
    
    
}