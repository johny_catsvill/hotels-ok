<?php

namespace App\Traits;

use Intervention\Image\ImageManagerStatic as Image;
use Storage;

trait croppedPhotos {

    public $sizesImages = [
        'home' => ['width' => 255, 'height' => 185],
        'category' => ['width' => 285, 'height' => 205],
        'object_mini' => ['width' => 145, 'height' => 93],
        'object' => ['width' => 840, 'height' => 605]
    ];

    public $sizesImagesRoom = [
        'norm' => ['width' => 300, 'height' => 205],
        'big' => ['width' => 720, 'height' => 400],
        'mini' => ['width' => 150, 'height' => 100]
    ];

    public $sizesImagesNews = [
        'norm' => ['width' => 290, 'height' => 245],
        'big' => ['width' => 900, 'height' => null],
        'object' => ['width' => 255, 'height' => null]
    ];


    public function cropPhoto($pathImages, $newNameImg, $imgNameWithoutType, $type, $category = 'images-objects') {

        if($category == 'images-objects') {
            $sizes = $this->sizesImages;
        }
        elseif($category == 'images-news') {
            $sizes = $this->sizesImagesNews;
        }
        else {
            $sizes = $this->sizesImagesRoom;
        }

        foreach ($sizes as $key => $size) {
            $img = Image::make(public_path().$pathImages.$newNameImg);

//            $img->resize($size['width'], $size['height'], function ($constraint) {
//                $constraint->aspectRatio();
//                $constraint->upsize();
//            });

            $img->fit($size['width'], $size['height'], function ($constraint) {
                $constraint->upsize();
            });

            $img->save(public_path().$pathImages.$imgNameWithoutType.'_'.$key.'.'.$type, 100);
        }
    }

    public static function changeImg($image) {
        if($image) {
            $newImage = explode('.', $image);

            $type = $newImage[1];
            return $newImage[0].'_home.'.$type;
        }

    }

    public function removePhoto($nameImage, $category) {

        if($category == 'images-objects') {
            $sizes = $this->sizesImages;
        }
        elseif('images-news') {
            $sizes = $this->sizesImagesNews;
        }
        else {
            $sizes = $this->sizesImagesRoom;
        }

        Storage::delete('/images/news/'.$nameImage);

        foreach ($sizes as $size) {

        }

    }



}